#include "cgicc/CgiDefs.h" 
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/mem/AutoReference.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Integer32.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "xdata/Double.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/shared/LUMITopics.hh"
#include "interface/bril/shared/CompoundDataStreamer.h"
#include "bril/vdmmonitor/VDMScan.h"
#include "bril/vdmmonitor/Events.h"
#include "bril/vdmmonitor/exception/Exception.h"
#include "eventing/core/PublisherReadyEvent.h"
#include "b2in/nub/exception/Exception.h"
#include <cstdint>

XDAQ_INSTANTIATOR_IMPL(bril::vdmmonitor::VDMScan)

namespace bril {

  namespace vdmmonitor {

    VDMScan::VDMScan(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
      
      xgi::framework::deferredbind(this,this,&bril::vdmmonitor::VDMScan::Default, "Default");     
      b2in::nub::bind(this, &bril::vdmmonitor::VDMScan::onMessage);
      
      m_busName.fromString("brildata");//default
      m_dipbusName.fromString("slimbus");
      m_diptopic.fromString("urn:dip:data");
      m_signalTopic.fromString("NB4");
      m_csvoutDir.fromString("/tmp");
      m_dipRoot.fromString("dip/CMS/");
      m_withcsv = false;
      
      m_fillnum = 0;
      m_runnum = 0;
      m_lsnum = 0;
      m_nbnum = 0;
      m_tssec = 0;
      m_tsmsec = 0;
      m_evtcanpublish = false;
      m_dipcanpublish = false;
      m_canpublish = false;
      m_evtpublishsubmitted = false;
      m_dippublishsubmitted = false;
      m_ip = 5;
      m_ipstr = "5";
      m_currentScanActive = false;
      m_previousScanActive = false;
      m_acqflag = false;
      m_bsrlqBunched_canpublish = true;
      m_bsrlfracBunched_canpublish = true;
      initCache();

      m_vdmscanTopic.fromString("dip/acc/LHC/Beam/AutomaticScanIP5");
      m_bstarTopics.fromString("dip/acc/LHC/Beam/BetaStar/Bstar5,dip/acc/LHC/RunControl/RunConfiguration");
      m_luminousRegionTopics.fromString("dip/CMS/LHC/LuminousRegion");      
      m_bpmTopics.fromString("dip/acc/LHC/Beam/BPM/LSS5L/B1_DOROS,dip/acc/LHC/Beam/BPM/LSS5L/B2_DOROS,dip/acc/LHC/Beam/BPM/LSS5R/B1_DOROS,dip/acc/LHC/Beam/BPM/LSS5R/B2_DOROS");
      m_atlasTopics.fromString("dip/TNproxy/ATLAS/LHC/BPTX1/IntensitiesPerBunch,dip/TNproxy/ATLAS/LHC/BPTX2/IntensitiesPerBunch,dip/TNproxy/ATLAS/LHC/Luminosity");
      m_bsrlTopics.fromString("dip/acc/LHC/Beam/BSRL/Beam1,dip/acc/LHC/Beam/BSRL/Beam2");
      m_outTopics.fromString("vdmflag,vdmscan,atlasbeam,bsrlfracBunched,bsrlqBunched,luminousregion");
      m_republishEvtTopics.fromString("beam,bunchlength,bcm1flumi,bcm1futca_lumi,hfoclumi,hfetlumi,pltlumizero,pltslinklumi");//for IP5, there should also be per channel
      m_outDIPTopics.fromString("LHC/AutomaticScan");
      //
      // register configuration parameters
      //
      getApplicationInfoSpace()->fireItemAvailable("busName",&m_busName);
      getApplicationInfoSpace()->fireItemAvailable("dipbusName",&m_dipbusName);
      getApplicationInfoSpace()->fireItemAvailable("dipTopic",&m_diptopic);
      getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
      getApplicationInfoSpace()->fireItemAvailable("outDir",&m_csvoutDir);    
      getApplicationInfoSpace()->fireItemAvailable("withcsv",&m_withcsv); 
      getApplicationInfoSpace()->fireItemAvailable("vdmscanTopic",&m_vdmscanTopic);
      getApplicationInfoSpace()->fireItemAvailable("betastarTopics",&m_bstarTopics);
      getApplicationInfoSpace()->fireItemAvailable("luminousRegionTopics",&m_luminousRegionTopics);
      getApplicationInfoSpace()->fireItemAvailable("atlasTopics",&m_atlasTopics);
      getApplicationInfoSpace()->fireItemAvailable("bpmTopics",&m_bpmTopics);
      getApplicationInfoSpace()->fireItemAvailable("bsrlTopics",&m_bsrlTopics);
      getApplicationInfoSpace()->fireItemAvailable("outTopics",&m_outTopics);
      getApplicationInfoSpace()->fireItemAvailable("republishEvtTopics",&m_republishEvtTopics);
      getApplicationInfoSpace()->fireItemAvailable("outDIPTopics",&m_outDIPTopics);
      
      getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
      this->addActionListener(this);
      
      toolbox::net::UUID uuid;
      m_processuuid = uuid.toString();
      unsigned int instanceid = getApplicationDescriptor()->getInstance();
      std::stringstream ss;
      ss<<instanceid;
      m_instanceidstr = ss.str();
      m_bsrlfracBunched_timerurn = "urn:VDMScan_bsrlfracBunched_timer"+m_processuuid+"_"+m_instanceidstr;
      m_bsrlqBunched_timerurn = "urn:VDMScan_bsrlqBunched_timer"+m_processuuid+"_"+m_instanceidstr;

      toolbox::net::URN memurn("toolbox-mem-pool",std::string("VDMScan_")+m_processuuid+"_"+m_instanceidstr+std::string("_mem"));
      toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
      m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
      
      //configure workloops
      m_wl_writecsv = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_processuuid+"_"+m_instanceidstr+"_writecsv","waiting");
      m_wl_evtpublish = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_processuuid+"_"+m_instanceidstr+"_evtpublish","waiting");
      m_wl_dippublish = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_processuuid+"_"+m_instanceidstr+"_dippublish","waiting");
      m_as_writecsv = toolbox::task::bind(this,&bril::vdmmonitor::VDMScan::do_writeVDMToCSV,"writecsv");
      m_as_evtpublish = toolbox::task::bind(this,&bril::vdmmonitor::VDMScan::do_publishToEventing,"evtpublish");
      m_as_dippublish = toolbox::task::bind(this,&bril::vdmmonitor::VDMScan::do_publishToDIP,"dippublish");

      m_bsrlfracBunched_timerurn = "urn:VDMScan_bsrlfracBunched_timer_"+m_processuuid+"_"+m_instanceidstr;
      m_bsrlqBunched_timerurn = "urn:VDMScan_bsrlqBunched_timer_"+m_processuuid+"_"+m_instanceidstr;
    }
    
    void VDMScan::actionPerformed(xdata::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event "+e.type());
      if( e.type() == "urn:xdaq-event:setDefaultValues" ){    
	m_ipstr =  m_vdmscanTopic.value_.substr(m_vdmscanTopic.value_.size()-1,1);
	m_ip = std::stoi(m_ipstr);
	std::array<std::string,5> keywords={"betastarTopics","luminousRegionTopics","bpmTopics","atlasTopics","bsrlTopics"};
	std::stringstream ss;
	xdata::Serializable* p=0;
	for( const auto& k: keywords ){	
	    p = getApplicationInfoSpace()->find(k);
	    std::list< std::string > ms = toolbox::parseTokenList(dynamic_cast<xdata::String*>(p)->value_,",");
	    m_dipsubs.insert(std::end(m_dipsubs),std::begin(ms),std::end(ms));	    
	}
	ss<<"DIP subs: ";
	for( const auto& s : m_dipsubs ){
	  ss<<s<<" ";
	}
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	ss.str("");
	
	std::list< std::string > os;
	//p = getApplicationInfoSpace()->find("outTopics");	
	//os = toolbox::parseTokenList(dynamic_cast<xdata::String*>(p)->value_,",");
	os = toolbox::parseTokenList(m_outTopics.value_,",");
	m_evtpubs.insert(std::end(m_evtpubs),std::begin(os),std::end(os));
	ss<<"EVT pubs: ";
	for( const auto& p : m_evtpubs ){
	  ss<<p<<" ";
	}	
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	ss.str("");
	//p = getApplicationInfoSpace()->find("outDIPTopics");	
	//os = toolbox::parseTokenList(dynamic_cast<xdata::String*>(p)->value_,",");
	os = toolbox::parseTokenList(m_outDIPTopics.value_,",");
	m_dippubs.insert(std::end(m_dippubs),std::begin(os),std::end(os));
	ss<<"DIP pubs: ";
	for( const auto& p : m_dippubs ){
	  ss<<p<<" ";
	}	
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	ss.str("");

	os = toolbox::parseTokenList(m_republishEvtTopics.value_,",");
	for( const auto& t : os){
	  if(t.find("%")!=std::string::npos){
	    std::vector<std::string> parseresult = gettruenames(t);
	    for( const auto& truename: parseresult ){
	      m_repubs.push_back( truename );
	    }
	  }else{
	    m_repubs.push_back( t );
	  }	  
	}
	ss<<"EVT repubs: ";
	for( const auto& p: m_repubs ){
	  ss<<p<<":";
	  std::string topicprefix="scan"+m_ipstr+"_";
	  std::string outtopicname = topicprefix+p;
	  ss<<outtopicname<<" ";
	  toolbox::mem::Reference* t1 = 0;
	  xdata::Properties t2;
	  m_republishCache.insert( std::make_pair(outtopicname,std::make_pair(t1,t2) ) );
	}	
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	ss.str("");
	
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	toolbox::TimeInterval vsec(3,0);//3 sec later to subscribe to all topics
	toolbox::TimeVal startT = now + vsec;
	std::string startsubtimer_name = "VDMScan_timer_"+m_processuuid+"_"+m_instanceidstr;  
	toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( startsubtimer_name );
	t->schedule( this, startT, (void*)0, "startsubscriptions" );
	
	m_wl_writecsv->activate();
	m_wl_evtpublish->activate();
	m_wl_dippublish->activate();
	this->getEventingBus(m_busName.value_).addActionListener(this);     
	this->getEventingBus(m_dipbusName.value_).addActionListener(this);   
      }
    }
    
    void VDMScan::timeExpired( toolbox::task::TimerEvent& e ){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "timeExpired");
      std::string name = e.getTimerTask()->name;
      std::stringstream ss;
      if( name.find("startsubscriptions")!=std::string::npos ){	
	try{
	  ss<<"subscribing to "<<m_signalTopic.toString()<<" on "<<m_busName.value_;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  this->getEventingBus(m_busName.value_).subscribe(m_signalTopic.value_);	  
	}catch(eventing::api::exception::Exception& e){
	  ss<<"Failed to subscribe to eventing "<<m_busName.value_<<" "<<e.what();
	  LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	  XCEPT_RETHROW(bril::vdmmonitor::exception::Exception,ss.str(),e);     
	}
	
	for( std::vector<std::string>::iterator it=m_repubs.begin(); it!=m_repubs.end(); ++it){
	  ss<<"subscribing to "<<*it<<" on "<<m_busName.value_;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  try{
	    this->getEventingBus(m_busName.value_).subscribe(*it);	  
	  }catch(eventing::api::exception::Exception& e){
	    ss<<"Failed to subscribe to eventing "<<m_busName.value_<<" "<<*it<<e.what();
	    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	    XCEPT_RETHROW(bril::vdmmonitor::exception::Exception,ss.str(),e);     
	  }
	}
	
	try{
	  ss<<"subscribing to "<<m_vdmscanTopic.toString()<<" on "<<m_busName.value_;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  this->getEventingBus(m_dipbusName.value_).subscribe(m_vdmscanTopic.value_);	  
	}catch(eventing::api::exception::Exception& e){
	  ss<<"Failed to subscribe to eventing "<<m_dipbusName.value_<<" "<<e.what();
	  LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	  XCEPT_RETHROW(bril::vdmmonitor::exception::Exception,ss.str(),e);     
	}
	try{
	  for(std::vector<std::string>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
	    ss<<"subscribing to "<<*it<<" on "<<m_dipbusName.value_;
	    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	    ss.str("");
	    this->getEventingBus(m_dipbusName.value_).subscribe(*it);
	  }
	}catch(eventing::api::exception::Exception& e){
	  ss<<"Failed to subscribe to eventing "<<m_dipbusName.value_<<" "<<e.what();
	  LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	  XCEPT_RETHROW(bril::vdmmonitor::exception::Exception,ss.str(),e);     
	}
	
	if( m_withcsv==true ){	
	  std::string fname = m_csvoutDir.value_+std::string("/vdm_")+std::string(m_processuuid)+std::string(".csv");
	  ss<<"write VDM file header to "<<fname;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  m_vdmfile.open(fname.c_str(),std::fstream::out|std::fstream::app);	
	  writeVDMHeader(m_vdmfile);
	}
      }//end "startsubscriptions" timer
    }
    
    void VDMScan::cleanRepubCache(){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering cleanRepubCache");
      {
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
        for( auto& item: m_republishCache ){
	  if(item.second.first){
	    item.second.first->release();
	    item.second.first=0;
	  }
	  //if(item.second.second){
	  item.second.second.clear();
	  //}
        }
      }//end scope guard
  }
    
    void VDMScan::writeVDMHeader( std::ostream& ss ){
      ss<<"fill,run,ls,nb,sec,msec"<<",";
      ss<<"acqflag"<<",";
      ss<<"step"<<",";
      ss<<"beam"<<",";
      ss<<"ip"<<",";
      ss<<"scanstatus"<<",";
      ss<<"plane"<<",";
      ss<<"progress"<<",";
      ss<<"nominal_separation"<<",";
      ss<<"read_nominal_B1sepPlane"<<",";
      ss<<"read_nominal_B1xingPlane"<<",";
      ss<<"read_nominal_B2sepPlane"<<",";
      ss<<"read_nominal_B2xingPlane"<<",";
      ss<<"set_nominal_B1sepPlane"<<",";
      ss<<"set_nominal_B1xingPlane"<<",";
      ss<<"set_nominal_B2sepPlane"<<",";
      ss<<"set_nominal_B2xingPlane"<<",";
      ss<<"bpm_5LDOROS_B1Names"<<",";
      ss<<"bpm_5LDOROS_B1hPos"<<",";
      ss<<"bpm_5LDOROS_B1vPos"<<",";
      ss<<"bpm_5LDOROS_B1hErr"<<",";
      ss<<"bpm_5LDOROS_B1vErr"<<",";
      ss<<"bpm_5RDOROS_B1Names"<<",";
      ss<<"bpm_5RDOROS_B1hPos"<<",";
      ss<<"bpm_5RDOROS_B1vPos"<<",";
      ss<<"bpm_5RDOROS_B1hErr"<<",";
      ss<<"bpm_5RDOROS_B1vErr"<<",";
      ss<<"bpm_5LDOROS_B2Names"<<",";
      ss<<"bpm_5LDOROS_B2hPos"<<",";
      ss<<"bpm_5LDOROS_B2vPos"<<",";
      ss<<"bpm_5LDOROS_B2hErr"<<",";
      ss<<"bpm_5LDOROS_B2vErr"<<",";
      ss<<"bpm_5RDOROS_B2Names"<<",";
      ss<<"bpm_5RDOROS_B2hPos"<<",";
      ss<<"bpm_5RDOROS_B2vPos"<<",";
      ss<<"bpm_5RDOROS_B2hErr"<<",";
      ss<<"bpm_5RDOROS_B2vErr"<<",";
      ss<<"atlas_totInst"<<",";
      ss<<"nominal_separation_plane"<<",";
      ss<<"bstar5"<<",";
      ss<<"ip5xingHmurad"<<",";
      ss<<"scanname";
      ss<<std::endl;      
    }

    std::vector<std::string> VDMScan::gettruenames( const std::string& pattern ){
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Entering gettrunames");   
      std::vector< std::string > result;
      size_t firstmark_pos = pattern.find_first_of('%');
      if( firstmark_pos!=std::string::npos ){
	size_t lastmark_pos = pattern.find_last_of('%');
	std::string begStr = pattern.substr( firstmark_pos+1, lastmark_pos-firstmark_pos-1 );
	std::string endStr = pattern.substr( lastmark_pos+1 );
	int begint;
	std::stringstream convertbeg( begStr );
	if( !(convertbeg >> begint) ){
	  LOG4CPLUS_ERROR( getApplicationLogger(), "gettruenames Failed to convert "+begStr+" to number in "+pattern );
	  XCEPT_RAISE( bril::vdmmonitor::exception::Exception, "Wrong range topic format" );
	}

	int endint;
	std::stringstream convertend( endStr );
	if( !(convertend >> endint) ){
	  LOG4CPLUS_ERROR( getApplicationLogger(), "gettruenames Failed to convert "+endStr+" to number in "+pattern );
	  XCEPT_RAISE( bril::vdmmonitor::exception::Exception, "Wrong range topic format" );
	}

	for(int id=begint; id<=endint; ++id){
	  std::string t = pattern.substr(0,firstmark_pos);
	  std::stringstream topicnamess;
	  topicnamess << t << id;
	  result.push_back( topicnamess.str() );
	}
      }
      std::stringstream ss;
      ss<<"Pattern "<<pattern<<" is parsed to ";
      for(auto const& elem: result){
	ss<<elem<<" ";
      }
      LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());   
      return result;
    }

    void VDMScan::actionPerformed( toolbox::Event& e ){
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received toolbox event " + e.type());   
      if ( e.type() == "eventing::api::BusReadyToPublish" ){//this app publishes only to bril eventing
	std::string busname = (static_cast<eventing::api::Bus*> (e.originator() ) )->getBusName();
	if( busname==m_busName.value_ ){
	  m_evtcanpublish = true;
	}else if( busname==m_dipbusName.value_ ){
	  m_dipcanpublish = true;
	}
      }else if( e.type() == "urn:bril-vdmmonitor-event:ScanActive" ){
	//get ip
	int thisip = static_cast<ScanActiveEvent&>(e).ip;
	if( thisip == m_ip ){
	  m_previousScanActive = false;
	  m_currentScanActive = true;
	}
      }else if( e.type() == "urn:bril-vdmmonitor-event:ScanOff" ){	
	int thisip = static_cast<ScanOffEvent&>(e).ip;
	if( thisip == m_ip ){
	  m_currentScanActive = false;
	  m_previousScanActive = false;
	  m_acqflag = false;
	  initCache();
	}
      }
    }

    void VDMScan::Default( xgi::Input* in, xgi::Output* out ) {
      *out<<cgicc::h3("VDM Scan")<<std::endl;
      std::string url = "/";
      url += getApplicationDescriptor()->getURN();
      *out<<"<div class=\"xdaq-tab-wrapper\" id=\"vdmPanel\">"<<std::endl;      
      *out<<"<div class=\"xdaq-tab\" title=\"status\">" <<std::endl;
      *out<<cgicc::table().set("class","xdaq-table").set("style","width: 100%;")<<std::endl;
      *out<<cgicc::thead()<<std::endl;
      *out<<cgicc::tr()<<std::endl;
      *out<<cgicc::th("IP")<<std::endl;
      *out<<cgicc::th("Scan Active")<<std::endl;
      *out<<cgicc::th("Acquisition Flag")<<std::endl;      
      *out<<cgicc::tr()<<std::endl;
      *out<<cgicc::thead()<<std::endl;
      *out<<cgicc::tbody()<<std::endl;
      *out<<cgicc::tr()<<std::endl;
      std::stringstream ss;
      ss<<m_ip;
      *out<<cgicc::td( ss.str() )<<std::endl;
      ss.str("");
      ss<<m_currentScanActive;
      *out<<cgicc::td( ss.str() )<<std::endl;
      ss.str("");
      ss<<m_acqflag;
      *out<<cgicc::td( ss.str() )<<std::endl;
      ss.str("");      
      *out<<cgicc::tr()<<std::endl;
      *out<<cgicc::tbody() <<std::endl;
      *out<<cgicc::table() <<std::endl;
      *out<<"</div>";
      *out<<"</div>";

      *out<<"<div class=\"xdaq-tab-wrapper\" id=\"vdmparam\">"<<std::endl;     
      *out<<"<div class=\"xdaq-tab\" title=\"parameters\">" <<std::endl;
      *out<<cgicc::table().set("class","xdaq-table").set("style","width: 100%;")<<std::endl;
      *out<<cgicc::thead()<<std::endl;
      *out<<cgicc::tr()<<std::endl;
      std::array<std::string,21> fields={"fill","run","ls","nb","sec","msec","beam","status","plane","nominal_sep","nominal_sep_plane","step","progress","r_B1sep","r_B1xing","r_B2sep","r_B2xing","s_B1sep","s_B1xing","s_B2sep","s_B2xing"};
      for( const auto& f : fields){
	*out<<cgicc::th(f)<<std::endl;
      }
      *out<<cgicc::tr()<<std::endl;
      *out<<cgicc::thead()<<std::endl;
      *out<<cgicc::tbody()<<std::endl;
      *out<<cgicc::tr()<<std::endl;
      
      ss<<m_fillnum;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_runnum;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_lsnum;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_nbnum;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_tssec;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_tsmsec;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.beam;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.stat;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.plane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.nominal_separation;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.nominal_separation_plane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.step;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.progress;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.read_nominal_B1sepPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.read_nominal_B1xingPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.read_nominal_B2sepPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.read_nominal_B2xingPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.set_nominal_B1sepPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.set_nominal_B1xingPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.set_nominal_B2sepPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");
      ss<<m_scandatacache.set_nominal_B2xingPlane;
      *out<<cgicc::td(ss.str())<<std::endl;
      ss.str("");

      *out<<cgicc::tr()<<std::endl;
      *out<<cgicc::tbody() <<std::endl;
      *out<<cgicc::table() <<std::endl;
      *out<<"</div>";
      *out<<"</div>";
      
    }

    bool VDMScan::do_publishToEventing(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publishToEventing");
      usleep(600000);
      for( const auto& outtopic : m_evtpubs ){
	publishToEventing(outtopic);
      }
      do_republish();
      return false;
    }
    
    bool VDMScan::do_publishToDIP(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publishToDIPEventing");
      usleep(600000);
      for( const auto& outtopic : m_dippubs ){
	publishToDIPEventing(outtopic);
      }
      return false;
    }

    void VDMScan::do_dippublish_automaticscan( const std::string& diptopic ){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"do_dippublish_automaticscan "+diptopic);
      std::stringstream ss;
      xdata::Table::Reference dipmessage( new xdata::Table );
      try{
	dipmessage->addColumn("Sequence","string");
	dipmessage->addColumn("Delay1","int 32");
	xdata::String strval("ENABLE");
	dipmessage->setValueAt(0,"Sequence",strval);
	xdata::Integer32 val=0;
	dipmessage->setValueAt(0,"Delay1",val);
      }catch( xdata::exception::Exception& e){
	ss<<"Failed to create AutomaticScan message table "<<e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	XCEPT_DECLARE_NESTED( bril::vdmmonitor::exception::Exception,err,ss.str(), e);
	notifyQualified("error",err);
      }
      std::string topicnameH( m_dipRoot.value_ + diptopic );
      do_publish_to_dipbus( topicnameH,dipmessage );
    }
    
    void VDMScan::do_publish_to_dipbus(const std::string& dipname, xdata::Table::Reference dipmessage){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_dippublish_to_dipbus "+dipname);
      std::stringstream ss;
      xdata::exdr::AutoSizeOutputStreamBuffer outBuffer; 
      xdata::exdr::Serializer serializer; 
      toolbox::mem::Reference* bufRef=0;
      xdata::Properties plist;
      plist.setProperty( "urn:dipbridge:dipname" , dipname );
      try{
	serializer.exportAll(&(*dipmessage),&outBuffer);
	char* buf = outBuffer.getBuffer();
	size_t bufsize = outBuffer.tellp();    
	bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,bufsize);
	bufRef->setDataSize(bufsize);	
	memcpy( bufRef->getDataLocation(), buf, bufsize );
      }catch(xdata::exception::Exception& e){
	ss<<"Failed to serialize "<<dipname<<" "<<e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	if(bufRef) bufRef->release(); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_INFO(getApplicationLogger(),"Publishing "+m_diptopic.value_+" with "+dipname+" to "+m_dipbusName.toString());
      try{
	this->getEventingBus(m_dipbusName.value_).publish(m_diptopic.value_,bufRef,plist);
      }catch(xcept::Exception& e){
	ss<<"Failed to publish "+m_diptopic.value_<<" with "<<dipname<<" to bus "<<m_dipbusName.toString()<<" "<<e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	if(bufRef) bufRef->release(); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
    }
    
    bool VDMScan::do_writeVDMToCSV(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_writeVDMToCSV");
      usleep(400000);
      if( !m_acqflag ) return false;
      writeVDMToCSV(m_vdmfile);
      m_vdmfile.flush();
      return false;
    }
    
    void VDMScan::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist) {
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering onMessage callback");
      toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if ( action == "notify" && ref!=0 ){      	
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
	std::stringstream msg;
	if( topic == m_signalTopic.toString() ){
	  //parse timing signal message header to get timing info
	  interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
	  unsigned int oldrunnum = m_runnum;
	  unsigned int oldlsnum = m_lsnum;
	  unsigned int oldnbnum = m_nbnum;

	  m_fillnum = thead->fillnum;
	  m_runnum = thead->runnum;
	  m_lsnum = thead->lsnum;
	  m_nbnum = thead->nbnum;	  
	  m_tssec = thead->timestampsec;
	  m_tsmsec = thead->timestampmsec;	  
	  if(oldrunnum==0 && oldlsnum==0 && oldnbnum==0) {
	    LOG4CPLUS_INFO(getApplicationLogger(),"discard the first NB4 message");
	    return;
	  }
	  msg<<"Fill "<<m_fillnum<<" Run "<<m_runnum<<" ls "<<m_lsnum<<" nb "<<m_nbnum;
	  LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
	  msg.str("");
	  do_publish_vdmscanflag();//always publishes vdmscanflag for the current IP
	  
	  if( m_withcsv==true ){
	    m_wl_writecsv->submit(m_as_writecsv);
	  }
	  if( m_evtcanpublish ){
	    m_wl_evtpublish->submit(m_as_evtpublish);
	    m_evtpublishsubmitted = true;
	  }
	  if( m_dipcanpublish ){
	    m_wl_dippublish->submit(m_as_dippublish);
	    m_dippublishsubmitted = true;
	  }
	  m_canpublish = m_evtpublishsubmitted&&m_evtpublishsubmitted;
	}else if( topic.find("dip/")!=std::string::npos ){
	  if(!m_canpublish) return;
	  handleDipMessage(topic,ref,plist);
	}else if( std::find(m_repubs.begin(),m_repubs.end(),topic) != m_repubs.end() ){
	  if( !m_canpublish ) return;	  
	  if( !plist.hasProperty("DATA_VERSION") || !plist.hasProperty("PAYLOAD_DICT") ){
	    LOG4CPLUS_ERROR(getApplicationLogger(), "Received data to republish has no property DATA_VERSION or PAYLOAD_DICT "+topic);
	    return;
	  }
	  std::string topicprefix="scan"+m_ipstr+"_";
	  std::string topicname = topicprefix+topic;      
	  {        
	  toolbox::task::Guard<toolbox::BSem> g(m_applock);
	  if( m_republishCache.find(topicname)!=m_republishCache.end() ) {
	    //std::pair< toolbox::mem::Reference*,xdata::Properties >& item=m_republishCache[topicname];
	    if( m_republishCache[topicname].first ){
	      msg<<topicname<<" has early cached unpublished value, clean it "<<m_republishCache[topicname].first;
	      LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
	      msg.str("");
	      m_republishCache[topicname].first->release();
	      m_republishCache[topicname].first = 0;
	      m_republishCache[topicname].second.clear();
	    }
	  }
          }//end scope guard 1
	  toolbox::mem::Reference* newref = ref->duplicate();
	  {        
	  toolbox::task::Guard<toolbox::BSem> g(m_applock);
	  m_republishCache[topicname].first = newref;
	  m_republishCache[topicname].second.setProperty( "DATA_VERSION",plist.getProperty("DATA_VERSION") );
	  m_republishCache[topicname].second.setProperty( "PAYLOAD_DICT",plist.getProperty("PAYLOAD_DICT") );
	  }//end scope guard 2
	}
      }
    }

    void VDMScan::do_republish(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_republish");
      std::stringstream ss;
      if( m_currentScanActive&&m_acqflag ){
        {        
	  toolbox::task::Guard<toolbox::BSem> g(m_applock);
	for( auto& item: m_republishCache ){ //loop over cache
	  std::string topic = item.first;
	  toolbox::mem::Reference* bufRef = item.second.first;
	  if( !bufRef ){
	    ss<<topic<<" has empty data, do nothing";
	    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	    ss.str("");
	    continue;
	  }
	  ss<<"do_republish Publishing "<<topic;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  //xdata::Properties& plist = item.second.second;
	  try{
	    this->getEventingBus(m_busName.value_).publish(topic,bufRef,item.second.second);
	    item.second.first = 0;
	    item.second.second.clear();	    
	  }catch(xcept::Exception& e){
	    ss<<"Failed to publish "<<topic<<" to "<<m_busName.toString();
	    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	    if(bufRef)  {
	      bufRef->release();
	      bufRef = 0;
	    }
	    XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	    this->notifyQualified("fatal",myerrorobj);
	  }
	}
	}//end scope guard
	
      }else{
	LOG4CPLUS_INFO(getApplicationLogger(),"do_republish Outside scan, cleanRepubCache");
	cleanRepubCache();//clean accumulated unpublished data
      }
    }
    
    void VDMScan::publishToDIPEventing(const std::string& topicname){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering publishToDIPEventing "+topicname);
      if( topicname.find("AutomaticScan")!=std::string::npos ){
	if( m_ip==5 && m_currentScanActive ){//only do this for ip5
	  do_dippublish_automaticscan(topicname);
	}
      }
    }

    void VDMScan::publishToEventing(const std::string& topicname){      
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering publishToEventing "+topicname);
      //data only meaning excluding all the flags
      if( topicname.find("vdmscan")!=std::string::npos ){//can be vdmscan or vdmscan1 etc
	if( m_currentScanActive&&m_acqflag ){
	  do_publish_vdmscandata();
	}
      }
      if( topicname.find("atlasbeam")!=std::string::npos ){
	if( m_currentScanActive&&m_acqflag ){
	  do_publish_atlasbeam();
	}	
      }
      if( topicname.find("luminousregion")!=std::string::npos ){
	if( m_currentScanActive&&m_acqflag ){
	  do_publish_luminousregion();
	}	
      }
      if( topicname.find("bsrl")!=std::string::npos ){
	if( m_currentScanActive && !m_previousScanActive ){
	  toolbox::task::Timer* t = 0;
	  std::string timername;
	  bool canpublish = false;
	  std::string myname;
	  if( topicname.find("fracBunched")!=std::string::npos ){
	    myname = "fracBunched";
	    timername =  m_bsrlfracBunched_timerurn;
	    canpublish = m_bsrlfracBunched_canpublish;
	  }else{
	    myname = "qBunched";
	    timername =  m_bsrlqBunched_timerurn;
	    canpublish = m_bsrlqBunched_canpublish;
	  }

	  if( !toolbox::task::TimerFactory::getInstance()->hasTimer( timername ) ){
	    t = toolbox::task::TimerFactory::getInstance()->createTimer( timername );
	  }else{
	    t = toolbox::task::TimerFactory::getInstance()->getTimer( timername );
	  }
	  if( canpublish ){
	    const std::string taskname("unblock_bsrl"+myname);
	    toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	    toolbox::TimeInterval vsec(300,0);//5 minutes
	    toolbox::TimeVal startT = now + vsec;
	    //start timer task: at the end of time set m_bsrlfracBunched_canpublish to true
	    if( t->isActive() ){
	      t->stop();
	    }
	    LOG4CPLUS_DEBUG(getApplicationLogger(), "start timer for "+taskname);
	    t->start();
	    t->schedule( this, startT, (void*)0, taskname );
	  }
	  if( myname == "fracBunched" ){
	    do_publish_bsrlfracBunched();// at the end of succesful publishing set m_bsrlfracBunched_canpublish to false
	  }else{
	    do_publish_bsrlqBunched();
	  }
	}
      }
    }
        
    void VDMScan::do_publish_bsrlfracBunched(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_bsrlfracBunched");
      if(!m_bsrlfracBunched_canpublish) return;
      std::stringstream ss;     
      std::string topicname(interface::bril::bsrlfracBunchedT::topicname());
      std::string topicprefix="scan"+m_ipstr+"_";
      topicname = topicprefix+topicname;      
      toolbox::mem::Reference* bufRef=0;
      std::string pdict = interface::bril::bsrlfracBunchedT::payloaddict();
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
      plist.setProperty("PAYLOAD_DICT",pdict);
      
      size_t totalsize = interface::bril::bsrlfracBunchedT::maxsize();
      bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
      bufRef->setDataSize(totalsize);
      unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
      interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
      header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_bsrl_acqStamp,0);
      header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
      header->setFrequency(300);
      header->setTotalsize(totalsize);
      
      interface::bril::shared::CompoundDataStreamer tc(pdict);
      tc.insert_field(header->payloadanchor, "postfracghostBunched", &m_bsrl_post_fraction_ghost_bunched[0] );
      tc.insert_field(header->payloadanchor, "postfracsatBunched", &m_bsrl_post_fraction_sat_bunched[0] );      
      try{
	ss<<"do_publish_bsrlfracBunched publishing bsrlfracBunched to "<<m_busName.toString()<<" "<<m_bsrl_post_fraction_ghost_bunched[0]<<" "<<m_bsrl_post_fraction_ghost_bunched[1]<<" "<<m_bsrl_post_fraction_sat_bunched[0]<<" "<<m_bsrl_post_fraction_sat_bunched[1];
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");
        this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
      }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_busName.toString();
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef)  bufRef->release();
        XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
      m_bsrlfracBunched_canpublish = false;
    }
    
    void VDMScan::do_publish_bsrlqBunched(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_bsrlqBunched");
      if(!m_bsrlqBunched_canpublish) return;
      std::stringstream ss;      
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
      std::string topicname = interface::bril::bsrlqBunchedT::topicname();
      std::string topicprefix="scan"+m_ipstr+"_";
      topicname = topicprefix+topicname;      
      toolbox::mem::Reference* qbufRef=0;
      for(int beamidx=0; beamidx<2; ++beamidx){
        for(int pieceid=0; pieceid<3; ++pieceid){
	  size_t qsize = interface::bril::bsrlqBunchedT::maxsize();
	  qbufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,qsize);
	  qbufRef->setDataSize(qsize);
	  interface::bril::shared::DatumHead* qheader = (interface::bril::shared::DatumHead*)(qbufRef->getDataLocation());
	  qheader->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_bsrl_acqStamp,0);
	  int ichannel = (beamidx+1)*10+(pieceid+1);
	  qheader->setResource(interface::bril::shared::DataSource::DIP,0,ichannel,interface::bril::shared::StorageType::FLOAT);
	  qheader->setFrequency(300);
	  qheader->setTotalsize(qsize);
	  interface::bril::bsrlqBunchedT* fullobj = (interface::bril::bsrlqBunchedT*)(qbufRef->getDataLocation());
	  for(int i=0; i<11880; ++i){
	    fullobj->payload()[i] = m_bsrl_post_q_bunched[beamidx][pieceid][i];
	  }
	  try{
	    ss<<"do_publish_bsrlqBunched publishing bsrlqBunched to "<<m_busName.toString();
	    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	    ss.str("");
	    this->getEventingBus(m_busName.value_).publish(topicname,qbufRef,plist);
	  }catch(xcept::Exception& e){
	    ss<<"Failed to publish "<<topicname<<" beam "<<beamidx+1<<" piece "<<pieceid+1<<" to "<<m_busName.value_;
	    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	    if(qbufRef) qbufRef->release();
	    XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	    this->notifyQualified("fatal",myerrorobj);
	  }
	  LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
        }
      }
      m_bsrlqBunched_canpublish = false;
    }

    void VDMScan::do_publish_atlasbeam(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_atlasbeam");
      std::stringstream ss;            
      std::string topicname = interface::bril::atlasbeamT::topicname();
      std::string topicprefix="scan"+m_ipstr+"_";
      topicname = topicprefix+topicname;
      toolbox::mem::Reference* bufRef=0;
      try{
        std::string pdict = interface::bril::atlasbeamT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        size_t totalsize = interface::bril::atlasbeamT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);
	
        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.insert_field( header->payloadanchor, "intensity1", &m_atlasBeamIntensity[1] );
        tc.insert_field( header->payloadanchor, "intensity2", &m_atlasBeamIntensity[2] );
        tc.insert_field( header->payloadanchor, "bxintensity1", &m_atlasBXIntensity[1][0] );
        tc.insert_field( header->payloadanchor, "bxintensity2", &m_atlasBXIntensity[2][0] );
	ss<<"Publishing "<<topicname<<" to "<<m_busName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        this->getEventingBus(m_busName).publish(topicname,bufRef,plist);
      }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_busName.toString()<<" "<<e.what();
        LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
        if(bufRef) bufRef->release(); 
        XCEPT_DECLARE_NESTED(vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
    }
    
    void VDMScan::do_publish_luminousregion(){      
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_luminousregion");
      std::stringstream ss;    
      std::string topicname = interface::bril::luminousregionT::topicname();
      std::string topicprefix="scan"+m_ipstr+"_";
      topicname = topicprefix+topicname;
      toolbox::mem::Reference* bufRef=0;
      std::string pdict = interface::bril::luminousregionT::payloaddict();
      size_t totalsize = interface::bril::luminousregionT::maxsize();
     
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
      plist.setProperty("PAYLOAD_DICT",pdict);
      bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
      bufRef->setDataSize(totalsize);
      unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
      interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
      header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);
      header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
      header->setFrequency(4);
      header->setTotalsize(totalsize);
      interface::bril::shared::CompoundDataStreamer tc(pdict);
      tc.insert_field( header->payloadanchor, "centroid", &m_luminous_region_centroid[0] );
      tc.insert_field( header->payloadanchor, "size", &m_luminous_region_size[0] );
      tc.insert_field( header->payloadanchor, "tilt", &m_luminous_region_tilt[0] );
      try{
	ss<<"Publishing "<<topicname<<" to "<<m_busName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");
	this->getEventingBus(m_busName).publish(topicname,bufRef,plist);
      }catch(xcept::Exception& e){
	ss<<"Failed to publish "<<topicname<<" to "<<m_busName.value_<<" "<<e.what();
	if(bufRef)  bufRef->release();
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
    }
    
    void VDMScan::handleDipMessage( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist ){
      if( topic.find("dip/acc/LHC/Beam/AutomaticScanIP") != std::string::npos ){
	do_processScanData( topic, ref, plist );	
      }else if( topic.find("dip/acc/LHC/Beam/BPM") != std::string::npos ){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Processing BPM");
	if( !m_currentScanActive ) return;
	do_processBPMData( topic, ref, plist);
      }else if( topic=="dip/acc/LHC/Beam/BetaStar/Bstar5" ){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Proccessing Bstar5");
	if( !m_currentScanActive ) return;
	xdata::Table table;
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
	xdata::exdr::Serializer serializer;
	try{
	  serializer.import(&table, &inBuffer);
	}catch (xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}
	m_bstar5 = 0;
        if( table.begin() == table.end() ){
          LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
          return;
        }	
	xdata::Serializable* p = 0;
	try{
	  p = table.getValueAt(0,"payload");
	  m_bstar5 = dynamic_cast<xdata::Integer32*>(p)->value_;
	}catch(xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}	
      }else if( topic=="dip/acc/LHC/RunControl/RunConfiguration" ){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Processing RunConfiguration");
	if( !m_currentScanActive ) return;
	xdata::Table table;
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
	xdata::exdr::Serializer serializer;
	try{
	  serializer.import(&table, &inBuffer);
	}catch (xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}
	m_ip5xingHmurad = 0;    
        if( table.begin() == table.end() ){
          LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
          return;
        }	
	xdata::Serializable* p = 0;
	try{
	  p = table.getValueAt(0,"IP5-XING-H-MURAD");
	  std::string ip5xingHmurad_str = dynamic_cast<xdata::String*>(p)->value_;
	  std::stringstream cs(ip5xingHmurad_str);
	  int s = 0;
	  m_ip5xingHmurad = cs >> s ? s : 0;
	}catch(xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}	
      }else if( topic=="dip/CMS/LHC/LuminousRegion"){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Processing LuminousRegion");
	if( !m_currentScanActive ) return;
	xdata::Table table;
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
	xdata::exdr::Serializer serializer;
	try{
	  serializer.import(&table, &inBuffer);
	}catch (xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}	
	std::fill(m_luminous_region_tilt.begin(), m_luminous_region_tilt.end(), -1.0);	
	std::fill(m_luminous_region_centroid.begin(), m_luminous_region_centroid.end(), -1.0);  
	std::fill(m_luminous_region_size.begin(), m_luminous_region_size.end(), -1.0);
        if( table.begin() == table.end() ){
          LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
          return;
        }
	xdata::Serializable* p = 0;
	try{
	  p = table.getValueAt(0,"Tilt");
	  xdata::Vector<xdata::Float>* p_tilt = dynamic_cast<xdata::Vector<xdata::Float>*>( p );
	  for( size_t i =0; i<p_tilt->elements(); ++i ){
	    xdata::Float* val = dynamic_cast<xdata::Float*>(p_tilt->elementAt(i));
	    m_luminous_region_tilt[i] = val->value_;
	  }
	  p = table.getValueAt(0,"Centroid");
	  xdata::Vector<xdata::Float>* p_centroid = dynamic_cast<xdata::Vector<xdata::Float>*>( p );
	  for( size_t i =0; i<p_centroid->elements(); ++i ){
	    xdata::Float* val = dynamic_cast<xdata::Float*>(p_centroid->elementAt(i));
	    m_luminous_region_centroid[i] = val->value_;
	  }
	  p = table.getValueAt(0,"Size");
	  xdata::Vector<xdata::Float>* p_size = dynamic_cast<xdata::Vector<xdata::Float>*>( p );
	  for( size_t i =0; i<p_size->elements(); ++i ){
	    xdata::Float* val = dynamic_cast<xdata::Float*>(p_size->elementAt(i));
	    m_luminous_region_size[i] = val->value_;
	  }	  
	}catch(xdata::exception::Exception& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}	
      }else if( topic.find("dip/TNproxy/ATLAS/LHC") != std::string::npos ){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Processing atlas data");
	if( !m_currentScanActive ) return;
	do_processAtlasData( topic, ref, plist);
      }else if( topic.find("dip/acc/LHC/Beam/BSRL/Beam") != std::string::npos ){
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Processing bsrl data");
	if( !m_currentScanActive ) return;
	do_processBSRLData( topic, ref, plist);
      }
    }

    void VDMScan::do_processScanData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist ){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_processScanData");
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;      
      try{
	serializer.import(&table, &inBuffer);
      }catch (xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	      
      
      if( table.begin() == table.end() ){
	LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
	return;
      }
      xdata::Serializable* p = 0;
      try{
	p = table.getValueAt(0,"Active");
	xdata::Boolean* p_scan_flag = dynamic_cast< xdata::Boolean* >(p);
	bool scanflag = p_scan_flag->value_;
	std::stringstream ss;
	if( !m_currentScanActive && scanflag ){	
	  ss<<"Firing ScanActiveEvent IP "<<m_ip;	  
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  bril::vdmmonitor::ScanActiveEvent e(m_ip);
	  this->fireEvent(e);	  
	}
	if( m_currentScanActive && !scanflag){
	  ss<<"Firing ScanOffEvent IP "<<m_ip;	  
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  ss.str("");
	  bril::vdmmonitor::ScanOffEvent e(m_ip);
	  this->fireEvent(e);
	  return;
	}
	if( !scanflag ) {
	  return;
	}	

	p = table.getValueAt(0,"Acquisition_Flag");
	xdata::Boolean* p_acqflag = dynamic_cast< xdata::Boolean* >(p);
        m_acqflag =  p_acqflag->value_;	
	p = table.getValueAt(0,"Beam");
	xdata::Integer32* p_beam = dynamic_cast< xdata::Integer32* >(p);
	m_scandatacache.beam = p_beam->value_;

	p = table.getValueAt(0,"LumiScan_Status");
	xdata::String* p_status = dynamic_cast< xdata::String* >(p);
	m_scandatacache.stat = p_status->value_;

	p = table.getValueAt(0,"Plane");
	xdata::String* p_plane = dynamic_cast< xdata::String* >(p);
	m_scandatacache.plane = p_plane->value_;

	p = table.getValueAt(0,"Nominal_Separation_Plane");
	xdata::String* p_nominal_separation_plane = dynamic_cast< xdata::String* >(p);
	m_scandatacache.nominal_separation_plane = p_nominal_separation_plane->value_;

	p = table.getValueAt(0,"Step");
	xdata::Integer32* p_step = dynamic_cast< xdata::Integer32* >(p);
	m_scandatacache.step = p_step->value_;

	p = table.getValueAt(0,"Step_Progress");
	xdata::Integer32* p_progress = dynamic_cast< xdata::Integer32* >(p);
	m_scandatacache.progress = p_progress->value_;

	p = table.getValueAt(0,"Nominal_Separation");
	xdata::Double* p_nominal_separation = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.nominal_separation = (float)p_nominal_separation->value_;

	p = table.getValueAt(0,"Read_Nominal_Displacement_B1_sepPlane");
	xdata::Double* p_read_nominal_B1sepPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.read_nominal_B1sepPlane = (float)p_read_nominal_B1sepPlane->value_;

	p = table.getValueAt(0,"Read_Nominal_Displacement_B1_xingPlane");
	xdata::Double* p_read_nominal_B1xingPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.read_nominal_B1xingPlane = (float)p_read_nominal_B1xingPlane->value_;

	p = table.getValueAt(0,"Read_Nominal_Displacement_B2_sepPlane");
	xdata::Double* p_read_nominal_B2sepPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.read_nominal_B2sepPlane = (float)p_read_nominal_B2sepPlane->value_;

	p = table.getValueAt(0,"Read_Nominal_Displacement_B2_xingPlane");
	xdata::Double* p_read_nominal_B2xingPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.read_nominal_B2xingPlane = (float)p_read_nominal_B2xingPlane->value_;

	p = table.getValueAt(0,"Set_Nominal_Displacement_B1_sepPlane");
	xdata::Double* p_set_nominal_B1sepPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.set_nominal_B1sepPlane = (float)p_set_nominal_B1sepPlane->value_;

	p = table.getValueAt(0,"Set_Nominal_Displacement_B1_xingPlane");
	xdata::Double* p_set_nominal_B1xingPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.set_nominal_B1xingPlane = (float)p_set_nominal_B1xingPlane->value_;

	p = table.getValueAt(0,"Set_Nominal_Displacement_B2_sepPlane");
	xdata::Double* p_set_nominal_B2sepPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.set_nominal_B2sepPlane = (float)p_set_nominal_B2sepPlane->value_;

	p = table.getValueAt(0,"Set_Nominal_Displacement_B2_xingPlane");
	xdata::Double* p_set_nominal_B2xingPlane = dynamic_cast< xdata::Double* >(p);
	m_scandatacache.set_nominal_B2xingPlane = (float)p_set_nominal_B2xingPlane->value_;	
	
	p = table.getValueAt(0,"Scan_Name");
	xdata::String* p_scan_name = dynamic_cast< xdata::String* >(p);
	m_scandatacache.scanname = p_scan_name->value_;

      }catch(xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	      
    }

    void VDMScan::do_publish_vdmscanflag(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_vdmscanflag");
      std::stringstream ss;
      std::string scan_topicname = "vdmscanflag";
      std::string acq_topicname = "vdmflag";
      if( m_ip!=5 ){
	scan_topicname = scan_topicname+m_ipstr;
	acq_topicname = acq_topicname+m_ipstr;
      }
      toolbox::mem::Reference* bufRef=0;
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
      try{        
	size_t totalsize = interface::bril::vdmscanflagT::maxsize();
	bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
	bufRef->setDataSize(totalsize);
	unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
	interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
	header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);
	header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
	header->setFrequency(4);
	header->setTotalsize(totalsize);
	bool vdmscanflag = m_currentScanActive;
	((interface::bril::vdmscanflagT*)buf)->payload()[0]=vdmscanflag;
	ss<<"Publishing SCAN flag "<<scan_topicname<<" = "<<vdmscanflag<<" to "<<m_busName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");
	this->getEventingBus(m_busName.value_).publish(scan_topicname,bufRef,plist);
      }catch(xcept::Exception& e){
	ss<<"Failed to publish "<<scan_topicname<<" to "<<m_busName.value_+" "+e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	if(bufRef) bufRef->release(); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
      bufRef=0;
      try{
	size_t totalsize = interface::bril::vdmflagT::maxsize();
	bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
	bufRef->setDataSize(totalsize);
	unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
	interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
	header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);
	header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
	header->setFrequency(4);
	header->setTotalsize(totalsize);
	bool vdmacqflag=m_acqflag;
	((interface::bril::vdmflagT*)buf)->payload()[0]=vdmacqflag;
	ss<<"Publishing ACQ flag "<<acq_topicname<<"= "<< vdmacqflag<<" to "<<m_busName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");
	this->getEventingBus(m_busName.value_).publish(acq_topicname,bufRef,plist);  
      }catch(xcept::Exception& e){
	ss<<"Failed to publish "<<acq_topicname<<" to "<<m_busName.value_<<" "<<e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	if(bufRef) bufRef->release(); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
    }        
    
    void VDMScan::do_publish_vdmscandata(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_vdmscandata");
      std::stringstream ss;
      std::string topicname(interface::bril::vdmscanT::topicname());
      if( m_ip!=5 ){
	std::string topicprefix="scan"+m_ipstr+"_";
	topicname = topicprefix+topicname;      
      }
      toolbox::mem::Reference* bufRef=0;
      try{
        std::string pdict = interface::bril::vdmscanT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if( !m_acqflag ){
	  plist.setProperty("NOSTORE","1");
        }
        size_t totalsize = interface::bril::vdmscanT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
	header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);
        header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);       
        interface::bril::shared::CompoundDataStreamer tc(pdict);
	tc.insert_field(header->payloadanchor, "ip", &m_ip);
        tc.insert_field(header->payloadanchor, "step", &m_scandatacache.step);
        tc.insert_field(header->payloadanchor, "progress", &m_scandatacache.progress);
        tc.insert_field(header->payloadanchor, "beam", &m_scandatacache.beam);
        tc.insert_field(header->payloadanchor, "plane", m_scandatacache.plane.c_str());
        tc.insert_field(header->payloadanchor, "nominal_sep_plane", m_scandatacache.nominal_separation_plane.c_str());
        tc.insert_field(header->payloadanchor, "stat", m_scandatacache.stat.c_str());
        tc.insert_field(header->payloadanchor, "sep", &m_scandatacache.nominal_separation);
        tc.insert_field(header->payloadanchor, "r_sepP1", &m_scandatacache.read_nominal_B1sepPlane);
        tc.insert_field(header->payloadanchor, "r_xingP1", &m_scandatacache.read_nominal_B1xingPlane);
        tc.insert_field(header->payloadanchor, "r_sepP2", &m_scandatacache.read_nominal_B2sepPlane);
        tc.insert_field(header->payloadanchor, "r_xingP2", &m_scandatacache.read_nominal_B2xingPlane);
        tc.insert_field(header->payloadanchor, "s_sepP1", &m_scandatacache.set_nominal_B1sepPlane);
        tc.insert_field(header->payloadanchor, "s_xingP1", &m_scandatacache.set_nominal_B1xingPlane);
        tc.insert_field(header->payloadanchor, "s_sepP2", &m_scandatacache.set_nominal_B2sepPlane);
        tc.insert_field(header->payloadanchor, "s_xingP2", &m_scandatacache.set_nominal_B2xingPlane);
        tc.insert_field(header->payloadanchor, "5ldoros_b1names",m_l_bpmdatacache[1].Names.c_str());
        tc.insert_field(header->payloadanchor, "5ldoros_b1hpos", &m_l_bpmdatacache[1].hPos);
        tc.insert_field(header->payloadanchor, "5ldoros_b1vpos",&m_l_bpmdatacache[1].vPos);
        tc.insert_field(header->payloadanchor, "5ldoros_b1herr",&m_l_bpmdatacache[1].hErr);
        tc.insert_field(header->payloadanchor, "5ldoros_b1verr",&m_l_bpmdatacache[1].vErr);
        tc.insert_field(header->payloadanchor, "5rdoros_b1names",m_r_bpmdatacache[1].Names.c_str());
        tc.insert_field(header->payloadanchor, "5rdoros_b1hpos", &m_r_bpmdatacache[1].hPos);
        tc.insert_field(header->payloadanchor, "5rdoros_b1vpos", &m_r_bpmdatacache[1].vPos);
        tc.insert_field(header->payloadanchor, "5rdoros_b1herr", &m_r_bpmdatacache[1].hErr);
        tc.insert_field(header->payloadanchor, "5rdoros_b1verr", &m_r_bpmdatacache[1].vErr);
        tc.insert_field(header->payloadanchor, "5ldoros_b2names",m_l_bpmdatacache[2].Names.c_str() );
        tc.insert_field(header->payloadanchor, "5ldoros_b2hpos", &m_l_bpmdatacache[2].hPos);
        tc.insert_field(header->payloadanchor, "5ldoros_b2vpos", &m_l_bpmdatacache[2].vPos);
        tc.insert_field(header->payloadanchor, "5ldoros_b2herr", &m_l_bpmdatacache[2].hErr);
        tc.insert_field(header->payloadanchor, "5ldoros_b2verr", &m_l_bpmdatacache[2].vErr);
        tc.insert_field(header->payloadanchor, "5rdoros_b2names", m_r_bpmdatacache[2].Names.c_str());
        tc.insert_field(header->payloadanchor, "5rdoros_b2hpos", &m_r_bpmdatacache[2].hPos);
        tc.insert_field(header->payloadanchor, "5rdoros_b2vpos", &m_r_bpmdatacache[2].vPos);
        tc.insert_field(header->payloadanchor, "5rdoros_b2herr", &m_r_bpmdatacache[2].hErr);
        tc.insert_field(header->payloadanchor, "5rdoros_b2verr", &m_r_bpmdatacache[2].vErr);
	if( pdict.find("vdmname")!=std::string::npos ){
	  tc.insert_field(header->payloadanchor, "vdmname",m_scandatacache.scanname.c_str() );	  
	}
        if( pdict.find("bstar")!=std::string::npos ){
	  tc.insert_field(header->payloadanchor, "bstar5", &m_bstar5 );
        }
        if( pdict.find("Hmurad")!=std::string::npos ){
	  tc.insert_field(header->payloadanchor, "xingHmurad", &m_ip5xingHmurad );
        }
	ss<<"Publishing "<<topicname<<" to "<<m_busName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");      
        this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
      }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_busName.value_<<" "<<e.what();
        LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
        if(bufRef) bufRef->release();
        XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"done");
    }
    
    void VDMScan::do_processBPMData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_processBPMData");
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;
      try{
	serializer.import(&table, &inBuffer);
      }catch (xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	
      if( table.begin() == table.end() ){
	LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
	return;
      }
      xdata::Serializable* p = 0;
      try{
	p = table.getValueAt(0,"bpmNames");	
	xdata::Vector<xdata::String>* p_bpmNames = dynamic_cast< xdata::Vector<xdata::String>* >(p);
	p = table.getValueAt(0,"horizontalPos");
	xdata::Vector<xdata::Double>* p_hPos = dynamic_cast< xdata::Vector<xdata::Double>* >(p);
	p = table.getValueAt(0,"verticalPos");
	xdata::Vector<xdata::Double>* p_vPos = dynamic_cast< xdata::Vector<xdata::Double>* >(p);
	p = table.getValueAt(0,"errorH");
	xdata::Vector<xdata::Double>* p_hErr = dynamic_cast< xdata::Vector<xdata::Double>* >(p);
	p = table.getValueAt(0,"errorV");
	xdata::Vector<xdata::Double>* p_vErr = dynamic_cast< xdata::Vector<xdata::Double>* >(p);
	int beamid = 0;
	if( topic.find("B1_DOROS") != std::string::npos ){
	  beamid = 1;
	}else if( topic.find("B2_DOROS") != std::string::npos ){
	  beamid = 2;
	}
	std::string searchtarget = std::string("LSS")+m_ipstr;
	if( topic.find( searchtarget+"L" ) != std::string::npos ){
	  //fill m_l_bpmdatacache		  
	  BPMData& bpmdata = m_l_bpmdatacache[beamid]; 	
	  bpmdata.Names = p_bpmNames->elementAt(0)->toString();
	  xdata::Double* p_hPos_val = dynamic_cast<xdata::Double*>(p_hPos->elementAt(0));
	  bpmdata.hPos = (float)p_hPos_val->value_;
	  xdata::Double* p_vPos_val = dynamic_cast<xdata::Double*>(p_vPos->elementAt(0));
	  bpmdata.vPos = (float)p_vPos_val->value_;
	  xdata::Double* p_hErr_val = dynamic_cast<xdata::Double*>(p_hErr->elementAt(0));
	  bpmdata.hErr = (float)p_hErr_val->value_;
	  xdata::Double* p_vErr_val = dynamic_cast<xdata::Double*>(p_vErr->elementAt(0));
	  bpmdata.vErr = (float)p_vErr_val->value_;
	}else if( topic.find( searchtarget+"R" ) != std::string::npos ) {
	  //fill m_r_bpmdatacache	
	  BPMData& bpmdata = m_r_bpmdatacache[beamid]; 
	  bpmdata.Names = p_bpmNames->elementAt(0)->toString();
	  xdata::Double* p_hPos_val = dynamic_cast<xdata::Double*>(p_hPos->elementAt(0));
	  bpmdata.hPos = (float)p_hPos_val->value_;
	  xdata::Double* p_vPos_val = dynamic_cast<xdata::Double*>(p_vPos->elementAt(0));
	  bpmdata.vPos = (float)p_vPos_val->value_;
	  xdata::Double* p_hErr_val = dynamic_cast<xdata::Double*>(p_hErr->elementAt(0));
	  bpmdata.hErr = (float)p_hErr_val->value_;
	  xdata::Double* p_vErr_val = dynamic_cast<xdata::Double*>(p_vErr->elementAt(0));
	  bpmdata.vErr = (float)p_vErr_val->value_;
	}
      }catch(xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	
    }
    
    void VDMScan::do_processAtlasData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist ){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_processAtlasData");
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;
      try{
	serializer.import(&table, &inBuffer);
      }catch (xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	
      if( table.begin() == table.end() ){
	LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
	return;
      }
      xdata::Serializable* p = 0;      
      if( topic.find("Luminosity")!=std::string::npos ){
	try{
	  p = table.getValueAt(0,"Lumi_TotInst");
	  m_atlas_totInst = dynamic_cast<xdata::Float*>(p)->value_;
	}catch( xdata::exception::Exception& e ){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}
      }else if( topic.find("IntensitiesPerBunch")!=std::string::npos ){
	int beamid = 0;
	if( topic.find("1")!=std::string::npos ){
	  beamid = 1;
	}else if( topic.find("2")!=std::string::npos ){
	  beamid = 2;
	}
	try{
	  p = table.getValueAt(0,"Sum");
	  m_atlasBeamIntensity[beamid] = dynamic_cast<xdata::Float*>(p)->value_;
	  p = table.getValueAt(0,"Values");
	  xdata::Vector< xdata::Float >* p_bxintensity = dynamic_cast< xdata::Vector<xdata::Float>* >(p);
	  for( size_t i = 0; i<MAXNBX; ++i ){
	    float bxintensity_val = dynamic_cast< xdata::Float* >(p_bxintensity->elementAt(i))->value_;
	    m_atlasBXIntensity[beamid][i] = bxintensity_val;
	  }
	}catch( xdata::exception::Exception& e ){
	  LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	  ref->release();
	}
      }
    }

    void VDMScan::do_processBSRLData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist ){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_processBSRLData");
      std::string beamidstr = topic.substr(topic.size()-1,1);
      int beamid = std::stoi( beamidstr );
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;
      try{
	serializer.import(&table, &inBuffer);
      }catch (xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }	
      if( table.begin() == table.end() ){
	LOG4CPLUS_WARN(getApplicationLogger(),toolbox::toString("DIP notification table is empty for this topic %s.", topic.c_str()));
	return;
      }
      xdata::Serializable* p = 0;
      try{
	int beamidx = beamid-1;//beam1 pos =0, beam2 pos=1
	p = table.getValueAt(0,"acqStamp");
	std::uint64_t val = dynamic_cast<xdata::UnsignedInteger64*>(p)->value_;
	m_bsrl_acqStamp = val/1000000000;
	p = table.getValueAt(0,"post_fraction_ghost_bunched");
	double post_fraction_ghost_bunched = dynamic_cast<xdata::Double*>(p)->value_;
	m_bsrl_post_fraction_ghost_bunched[beamidx] = (float)post_fraction_ghost_bunched;
	p = table.getValueAt(0,"post_fraction_sat_bunched");
        double post_fraction_sat_bunched = dynamic_cast<xdata::Double*>(p)->value_;
	m_bsrl_post_fraction_sat_bunched[beamidx] = (float)post_fraction_sat_bunched;
	p = table.getValueAt(0,"post_q_bunched");
	xdata::Vector< xdata::Float >* post_q_bunched = dynamic_cast< xdata::Vector<xdata::Float>* >(p);
	for( int pieceid=0; pieceid<3; ++pieceid) {
	  for( int i=0; i<11880; ++i){
	    unsigned int myid = pieceid*11880+i;
	    xdata::Float* v = dynamic_cast<xdata::Float*>(post_q_bunched->elementAt(myid));
	    m_bsrl_post_q_bunched[beamidx][pieceid][i] = v->value_;
	  }
        }
      }catch( xdata::exception::Exception& e ){
	LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
	ref->release();
      }
    }

    void VDMScan::initCache(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering initCache");
      m_scandatacache.reset();
      //bpmdata
      BPMData bd;     
      std::array<int,2> beams={1,2};
      for(auto& beamid : beams){	
	if( m_l_bpmdatacache.empty() ){
	  m_l_bpmdatacache.insert(std::pair<int,BPMData>(beamid,bd));
	}else{
	  m_l_bpmdatacache[beamid].reset();
	}
	if( m_r_bpmdatacache.empty() ){
	  m_r_bpmdatacache.insert(std::pair<int,BPMData>(beamid,bd));
	}else{
	  m_r_bpmdatacache[beamid].reset();
	}
      }
      
      //atlas data
      for(auto& beamid : beams){
	if( m_atlasBeamIntensity.empty() ){
	  m_atlasBeamIntensity.insert(std::pair<int,float>(beamid,0.));
	}else{
	  m_atlasBeamIntensity[beamid] = 0.;
	}
	std::array<float,MAXNBX> a;
	a.fill(0.);
	if( m_atlasBXIntensity.empty() ) {
	  m_atlasBXIntensity.insert(std::pair<int,std::array<float,MAXNBX> >(beamid,a));
	}else{
	  m_atlasBXIntensity[beamid] = a;
	}
      }
      m_atlas_totInst = 0;
      //bsrl data
      m_bsrl_acqStamp = 0;
      m_bsrl_post_fraction_ghost_bunched.fill(0.);
      m_bsrl_post_fraction_sat_bunched.fill(0.);
      memset(m_bsrl_post_q_bunched,0, sizeof(float)*2*3*11880 );     
      //luminous region
      m_luminous_region_tilt.fill(-1.);
      m_luminous_region_centroid.fill(-1.);
      m_luminous_region_size.fill(-1.);
      //bstar data
      m_bstar5 = 0;
      m_ip5xingHmurad = 0;    
    }

    void VDMScan::writeVDMToCSV( std::ostream& ss ){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering writeVDMToCSV");
      std::array<int,2> beams{1,2};
      ss<<m_fillnum<<","<<m_runnum<<","<<m_lsnum<<","<<m_nbnum<<","<<m_tssec<<","<<m_tsmsec<<",";
      ss<<m_acqflag<<",";
      ss<<m_scandatacache.step<<",";
      ss<<m_scandatacache.beam<<","; //beam
      ss<<m_ip<<",";
      ss<<m_scandatacache.stat<<",";
      ss<<m_scandatacache.plane<<",";
      ss<<m_scandatacache.progress<<",";
      ss<<m_scandatacache.nominal_separation<<",";
      ss<<m_scandatacache.read_nominal_B1sepPlane<<",";
      ss<<m_scandatacache.read_nominal_B1xingPlane<<",";
      ss<<m_scandatacache.read_nominal_B2sepPlane<<",";
      ss<<m_scandatacache.read_nominal_B2xingPlane<<",";
      ss<<m_scandatacache.set_nominal_B1sepPlane<<",";
      ss<<m_scandatacache.set_nominal_B1xingPlane<<",";
      ss<<m_scandatacache.set_nominal_B2sepPlane<<",";
      ss<<m_scandatacache.set_nominal_B2xingPlane<<",";
      for( const auto& beamidx: beams ){
	BPMData& lbpm = m_l_bpmdatacache[beamidx];
	BPMData& rbpm = m_r_bpmdatacache[beamidx];
	ss<<lbpm.Names<<",";
	ss<<lbpm.hPos<<",";
	ss<<lbpm.vPos<<",";
	ss<<lbpm.hErr<<",";
	ss<<lbpm.vErr<<",";
	ss<<rbpm.Names<<",";
	ss<<rbpm.hPos<<",";
	ss<<rbpm.vPos<<",";
	ss<<rbpm.hErr<<",";
	ss<<rbpm.vErr<<",";
      }
      ss<<m_atlas_totInst<<",";
      ss<<m_scandatacache.nominal_separation_plane<<",";
      ss<<m_bstar5<<",";
      ss<<m_ip5xingHmurad<<",";
      ss<<m_scandatacache.scanname;
      ss<<std::endl;
    }

    VDMScan::~VDMScan(){
      for( auto& item: m_republishCache ){
	if(item.second.first){
	  item.second.first->release();
	}
	item.second.second.clear();
	//if(item.second.second){
	//  delete item.second.second;
	//}
      }//end for
    }

  } // end ns vdmmonito
} // end ns bril 
