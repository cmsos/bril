#ifndef _bril_vdmmonitor_RandGenerator_h_
#define _bril_vdmmonitor_RandGenerator_h_
namespace bril{
  namespace vdmmonitor{
     float box_muller(float m, float s);
  }
}
#endif
