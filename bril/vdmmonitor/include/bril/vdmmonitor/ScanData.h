#ifndef _bril_vdmmonitor_ScanData_h_
#define _bril_vdmmonitor_ScanData_h_
namespace bril{
  namespace vdmmonitor{
    class ScanData{
    public:
    ScanData():fillnum(0),runnum(0),step(0),ip(0),progress(0),beam(0),nominal_separation(0),read_nominal_B1sepPlane(0),read_nominal_B1xingPlane(0),read_nominal_B2sepPlane(0),read_nominal_B2xingPlane(0),set_nominal_B1sepPlane(0),set_nominal_B1xingPlane(0),set_nominal_B2sepPlane(0),set_nominal_B2xingPlane(0), read_beta_star(0),read_crossing_angle(0) { }
      unsigned int fillnum;
      unsigned int runnum;      
      int step;
      unsigned char ip;
      int progress;
      unsigned char beam;      
      char plane[20];
      char stat[20];
      float nominal_separation; 
      float read_nominal_B1sepPlane;
      float read_nominal_B1xingPlane;
      float read_nominal_B2sepPlane;
      float read_nominal_B2xingPlane;
      float set_nominal_B1sepPlane;
      float set_nominal_B1xingPlane;
      float set_nominal_B2sepPlane;
      float set_nominal_B2xingPlane;
      int read_beta_star; // is in cm, e.g. 80 or 311 cm
      int read_crossing_angle; // is in uRad, e.g. 150 or 170 uRad
      float b1_bxlength[3564]; //b1length:float:3564
      float b2_bxlength[3564]; //b2length:float:3564
    };
  }//end ns vdmmonitor
}//end ns bril

#endif
