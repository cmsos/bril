#ifndef _bril_vdmmonitor_Events_h_
#define _bril_vdmmonitor_Events_h_

#include <string>
#include "toolbox/Event.h"

namespace bril{
  namespace vdmmonitor{

    class StepOnEvent : public toolbox::Event{
      // cms scan true, previous vdmflag=false, current vdmflag=true, starting accumulating data
    public:
      StepOnEvent();
      ~StepOnEvent();
    };

    class StepOffEvent : public toolbox::Event{
      // cms scan true, previous vdmflag=true, current vdmflag=false, stopping accumulating data, start analyzing
    public:
      StepOffEvent();
      ~StepOffEvent();
    };

    class PlaneOffEvent : public toolbox::Event{
      // current plane differs from previous plane, start fitting shape for this plane
    public:
      PlaneOffEvent();
      ~PlaneOffEvent();
    };

    class CMSOffEvent : public toolbox::Event{
      // previous cms scan=true, current cms scan=false, clean all caches
    public:
      CMSOffEvent();
      ~CMSOffEvent();
    };

    class ScanActiveEvent : public toolbox::Event{
    public:
      explicit ScanActiveEvent(int ip);
      ~ScanActiveEvent();
      int ip;
    private:
      ScanActiveEvent();
    };

    class ScanOffEvent : public toolbox::Event{
    public:
      explicit ScanOffEvent(int ip);
      ~ScanOffEvent();
      int ip;
    private:
      ScanOffEvent();
    };
  }//ns vdmmonitor
}//ns bril

#endif
