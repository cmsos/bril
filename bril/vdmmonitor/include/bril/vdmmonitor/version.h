// $Id$
#ifndef _bril_brilvdmmonitor_version_h_
#define _bril_brilvdmmonitor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILVDMMONITOR_VERSION_MAJOR 4
#define BRIL_BRILVDMMONITOR_VERSION_MINOR 0
#define BRIL_BRILVDMMONITOR_VERSION_PATCH 19
#define BRILVDMMONITOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILVDMMONITOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILVDMMONITOR_VERSION_MAJOR,BRIL_BRILVDMMONITOR_VERSION_MINOR,BRIL_BRILVDMMONITOR_VERSION_PATCH)
#ifndef BRIL_BRILVDMMONITOR_PREVIOUS_VERSIONS
#define BRIL_BRILVDMMONITOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILVDMMONITOR_VERSION_MAJOR,BRIL_BRILVDMMONITOR_VERSION_MINOR,BRIL_BRILVDMMONITOR_VERSION_PATCH)
#else
#define BRIL_BRILVDMMONITOR_FULL_VERSION_LIST BRIL_BRILVDMMONITOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILVDMMONITOR_VERSION_MAJOR,BRIL_BRILVDMMONITOR_VERSION_MINOR,BRIL_BRILVDMMONITOR_VERSION_PATCH)
#endif
namespace brilvdmmonitor{
  const std::string project = "bril";
  const std::string package = "brilvdmmonitor";
  const std::string versions = BRIL_BRILVDMMONITOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ vdmmonitor";
  const std::string description = "Monitor vdm scans and fire monitoring data";
  const std::string authors = "Zhen Olena";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
