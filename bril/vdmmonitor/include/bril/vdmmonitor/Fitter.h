#ifndef _bril_vdmmonitor_Fitter_h_
#define _bril_vdmmonitor_Fitter_h_
#include <vector>
#include <numeric>

namespace bril{
  namespace vdmmonitor{

    typedef std::vector<float> vecF;
    typedef std::vector< std::vector<float> > vecVecF;
    void fit_vector(const vecF&  sep, const vecVecF&  All_shapes_X, const vecVecF&  All_shapes_X_err, const vecVecF& b1_currents, const vecVecF& b2_currents, vecF& bx_b1_b2_product, vecF&  bxwidth, vecF&  bxwidth_err, vecF& bxpeak, vecF& bxpeak_err, vecF& bxmean, vecF& bxmean_err, float& avgwidth, float& avgwidth_err, float& avgpeak, float& avgpeak_err, float& avgmean, float& avgmean_err);  /* with const and & it will pass a handle, but preventprotectdata from copying.  With & only it can will change (add/rewrite) the value in the vector */
  }
}
#endif
