#include <iostream>
#include "bril/timesource/TCDSHWReceiver.h"
#include "bril/timesource/tcdsinfo.h"
#include <unistd.h>
#include <stdexcept>

int main(){
  bril::timesource::TCDSHWReceiver r("file://./cfg/connections_rarp.xml", "bcm1f.crate1.amc5");
  r.setup_board();
  usleep(100000);
  uint32_t cdce_ready = r.cdce_locked();
  if( !cdce_ready ){
    throw std::runtime_error( "CDCE is not locked (Missing TCDS backplane clock?)");
  }
  usleep(100000);
  uint32_t tcds_ready = r.tcds_ready();
  if( !tcds_ready ){
    throw std::runtime_error( "TCDS receiver is not ready" );
  }

  int nc = 100;
  while ( --nc ) {
    bril::timesource::tcdsinfo_t tcds = r.read_tcds();
    std::cout << "Fill: " << tcds.lhc_fill << ", Run: " << tcds.cms_run << ", LS: " << tcds.lumi_section << ", NB: " << tcds.lumi_nibble << std::endl;
    usleep( 100000 );
   }
}
