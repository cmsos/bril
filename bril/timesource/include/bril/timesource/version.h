// $Id$
#ifndef _bril_timesource_version_h_
#define _bril_timesource_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILTIMESOURCE_VERSION_MAJOR 4
#define BRIL_BRILTIMESOURCE_VERSION_MINOR 0
#define BRIL_BRILTIMESOURCE_VERSION_PATCH 3
#define BRILTIMESOURCE_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILTIMESOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILTIMESOURCE_VERSION_MAJOR,BRIL_BRILTIMESOURCE_VERSION_MINOR,BRIL_BRILTIMESOURCE_VERSION_PATCH)
#ifndef BRIL_BRILTIMESOURCE_PREVIOUS_VERSIONS
#define BRIL_BRILTIMESOURCE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILTIMESOURCE_VERSION_MAJOR,BRIL_BRILTIMESOURCE_VERSION_MINOR,BRIL_BRILTIMESOURCE_VERSION_PATCH)
#else
#define BRIL_BRILTIMESOURCE_FULL_VERSION_LIST BRIL_BRILTIMESOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILTIMESOURCE_VERSION_MAJOR,BRIL_BRILTIMESOURCE_VERSION_MINOR,BRIL_BRILTIMESOURCE_VERSION_PATCH)
#endif
namespace briltimesource{
  const std::string project = "bril";
  const std::string package = "briltimesource";
  const std::string versions = BRIL_BRILTIMESOURCE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ timesource";
  const std::string description = "brildaq nibble frequency clocks. There are simulated clock and real clock following tcds signals.";
  const std::string authors = "Z. Xie";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
