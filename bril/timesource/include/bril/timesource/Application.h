#ifndef _bril_timesource_Application_h_
#define _bril_timesource_Application_h_
#include <string>
#include <vector>
#include <deque>
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/task/TimerListener.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "eventing/api/Member.h"
#include "xdata/InfoSpace.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Properties.h"
#include "xdata/String.h"
#include "xdata/Float.h"
#include "toolbox/BSem.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/EventDispatcher.h"
#include "bril/timesource/Common.h"

namespace mem{
  class MemoryPoolFactory;
}
namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{
  namespace timesource{
    
    class NBTimer;
    /**
     * main app. send start run/stop signal; manage nb timer workloops; monitoring; read global configuration
     * In simulation setup, main app generates runs on timer
     * In real mode, main app listens to tcds mon for start/stop run signal 
     * control nibble frequency timer instances
     **/
    class Application : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox callback
      virtual void actionPerformed(toolbox::Event& e);
      // run duration expired callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
      void addActionListener(toolbox::ActionListener* l);

      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

    protected:  
      // send runstart signal 
      void do_runstart();
      void do_publish_timesignal(const std::string& topicname, unsigned int frequency);
      void do_publish_tcds(bool nostore);
      
      bool watching(toolbox::task::WorkLoop* wl);

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      xdata::Boolean m_simSource;           // if sim mode
      xdata::UnsignedInteger m_instanceid;
      //configurable parameter used in case of real mode
      xdata::String m_tcdsBus;              // tcds bus name in real mode
      xdata::String m_tcdsTopic;            // tcds topic in real mode
      xdata::String m_outputBuses;
      std::set<std::string> m_outbuses;
      std::set<std::string> m_unreadybuses;

      //configurable parameter used in case of sim mode
      xdata::UnsignedInteger m_swInitRun;   // loopback initial run number
      xdata::UnsignedInteger m_swInitFill;  // loopback initial fill number
      xdata::UnsignedInteger m_swLSPerRun;  // number of lumi section per run
      xdata::UnsignedInteger m_swNRuns;     // number of runs 
      xdata::UnsignedInteger m_swRunPerFill;// number of runs per fill
      xdata::Vector<xdata::Properties> m_swNBTimers;// nibble frequency timer config
      xdata::Vector<xdata::String> m_swNBTimerStrs;

      xdata::UnsignedInteger m_currentfill;  // current fill number
      xdata::UnsignedInteger m_currentrun;   // current run number
      xdata::UnsignedInteger m_currentls;    // current ls number
      xdata::UnsignedInteger m_currentnb;    // current nb number
            
      xdata::Boolean m_cmson;       
      xdata::Float m_deadfrac; 
      xdata::UnsignedInteger m_ndeadfrac;   //
      xdata::UnsignedInteger32 m_ncollidingbx; //The number of BXs in the current lumi nibble with crossing/colliding bunch pairs in CMS. 
      xdata::UnsignedInteger32 m_norbs; // orbits per nibble
      xdata::UnsignedInteger32 m_nbperls; // nberls
      unsigned int m_nruns;

      std::string m_runtimername;            // runtimer name (sim)
      std::string m_extratimername;          // extra tcds fallback timer name(real)
      toolbox::task::Timer* m_runtimer;      // runtimer instance(sim)
      toolbox::task::Timer* m_extratimer;
      std::vector< NBTimer* > m_nbtimerstore;// nibble frequency timers (sim)
      toolbox::BSem m_applock;
      toolbox::EventDispatcher m_dispatcher;

      // monitoring
      std::string m_monURN;
      xdata::InfoSpace* m_monInfoSpace;
      std::list<std::string> m_monItemList;
      std::map<std::string, xdata::Serializable*> m_monconfig_table;
      std::map<std::string, xdata::Serializable*> m_monstatus_table;
      unsigned int m_lasttcdssec;
      bool m_hastcds;
      unsigned m_fakenb4counts;
      toolbox::task::WorkLoop* m_tcdswatcher;
      toolbox::task::ActionSignature* m_as_tcdswatcher;

      // counters
      std::deque<unsigned int> m_nbx_moving_window;
      xdata::UnsignedInteger32 m_moving_window_size;
    private:
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
