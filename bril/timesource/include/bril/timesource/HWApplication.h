#ifndef _bril_timesource_HWApplication_h_
#define _bril_timesource_HWApplication_h_
#include <string>
#include <vector>
#include <deque>
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/task/TimerListener.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "eventing/api/Member.h"
#include "xdata/InfoSpace.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Boolean.h"
#include "xdata/Properties.h"
#include "xdata/String.h"
#include "xdata/Float.h"
#include "toolbox/BSem.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/EventDispatcher.h"
#include "bril/timesource/tcdsinfo.h"


namespace mem{
  class MemoryPoolFactory;
}
namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{
  namespace timesource{
    enum CLOCKTYPE {HWCLOCKSOURCE=1,SWCLOCKSOURCE,DUMMYCLOCKSOURCE};
    class TCDSHWReceiver;    
    class HWApplication : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      HWApplication (xdaq::ApplicationStub* s);
      // destructor
      ~HWApplication ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox callback
      virtual void actionPerformed(toolbox::Event& e);
      // initialize hw callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);      
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

    protected:  
      //void do_publish_timesignal(const std::string& topicname, unsigned int frequency);
      //void do_publish_tcds(bool nostore);
      
      // configuration params
      xdata::String m_tcdsBus;              // tcds bus name in real mode
      xdata::String m_tcdsTopic;            // tcds topic in real mode
      xdata::String m_outputBuses;
      xdata::UnsignedInteger32 m_moving_window_size;
      xdata::String m_hwconfig;
      xdata::String m_boardname;
      xdata::String m_topicsuffix;
      xdata::UnsignedInteger32 m_fakeerrorinterval;
      xdata::UnsignedInteger32 m_fakeerrorduration;
      xdata::UnsignedInteger32 m_pause_swclock_duration;
      std::set<std::string> m_outbuses;
      std::set<std::string> m_unreadybuses;          
      
      // common cache
      bril::timesource::CLOCKTYPE m_next_publishfrom;
      bril::timesource::tcdsinfo_t m_current_clockinfo;

      // sw tcds data cache
      xdata::UnsignedInteger32 m_current_tcds_fill;
      xdata::Boolean m_cmson;       
      xdata::Float m_deadfrac; 
      xdata::UnsignedInteger m_ndeadfrac;   //
      xdata::UnsignedInteger32 m_ncollidingbx; //The number of BXs in the current lumi nibble with crossing/colliding bunch pairs in CMS. 
      xdata::UnsignedInteger32 m_norbs; // orbits per nibble
      xdata::UnsignedInteger32 m_nbperls; // nberls
      xdata::UnsignedInteger32 m_swclock_threshold_msec;
      unsigned int m_last_swclock_message;
      unsigned int m_pause_swclock_count;
      // hw data cache
      bool m_hwready;
      //xdata::UnsignedInteger32 m_currentfill;
      //xdata::UnsignedInteger32 m_currentrun;
      //xdata::UnsignedInteger32 m_currentls;
      //xdata::UnsignedInteger32 m_currentnb;
      //unsigned int m_last_successfulpoll;
      //unsigned int m_last_publish;
      unsigned int m_hwclock_success;
      unsigned int m_hwpollinterval;
      
      // averaging moving window counters
      std::deque<unsigned int> m_nbx_moving_window;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      std::string m_init_timername;
      toolbox::task::Timer* m_initializehw_timer; 
      
      std::string m_check_timername;
      toolbox::task::Timer* m_check_timer; 

      std::string m_dummyclock_timername;
      toolbox::task::Timer* m_dummyclock_timer; 

      toolbox::BSem m_applock;

      toolbox::task::WorkLoop* m_hwpoller;
      toolbox::task::ActionSignature* m_as_hwpoller;

      bril::timesource::TCDSHWReceiver* m_tcdsreceiver;

    private:
      bool polling( toolbox::task::WorkLoop* wl );
      void do_publish_timesignal(const std::string& topicname, unsigned int frequency,const bril::timesource::tcdsinfo_t& clockinfo, const toolbox::TimeVal& t, bril::timesource::CLOCKTYPE clocktype);
      void do_publish_tcds( const std::string& topicname, const bril::timesource::tcdsinfo_t& clockinfo, const toolbox::TimeVal& t,bril::timesource::CLOCKTYPE clocktype); 
      HWApplication(const HWApplication&);
      HWApplication& operator=(const HWApplication&);
    };
  }
}
#endif
