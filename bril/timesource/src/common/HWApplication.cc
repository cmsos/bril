#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "eventing/api/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/mem/AutoReference.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Properties.h"
#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "bril/timesource/HWApplication.h"
#include "bril/timesource/exception/Exception.h"
#include "interface/bril/TCDSTopics.hh"
#include "bril/timesource/TCDSHWReceiver.h"
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL (bril::timesource::HWApplication)

bril::timesource::HWApplication::HWApplication (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this), m_applock(toolbox::BSem::FULL)
{
  xgi::framework::deferredbind(this,this,&bril::timesource::HWApplication::Default,"Default");
  b2in::nub::bind(this, &bril::timesource::HWApplication::onMessage);

  m_tcdsBus.value_ = "";
  m_tcdsTopic.value_ = "";
  m_fakeerrorinterval = 0;
  m_fakeerrorduration = 16;
  m_pause_swclock_count = 0;
  m_pause_swclock_duration = 0;     
  m_current_tcds_fill = 0;
  m_cmson = false;
  m_deadfrac = 0.;
  m_ndeadfrac = 0;
  m_ncollidingbx = 0;
  m_norbs = 0;
  m_nbperls = 0;
  m_swclock_threshold_msec = 3000;//sw clock cut off time
  m_last_swclock_message = 0;

  m_hwready = false;
  //m_last_successfulpoll = 0;
  //m_last_publish = 0;
  m_hwclock_success = 0;
  m_hwpollinterval = 91000;// 1/4 of a nibble 364msec/4*1000 usec
  m_moving_window_size = 12;

  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  std::string memPoolName = getApplicationDescriptor()->getClassName() + std::string("_memPool");
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);

  try{
    getApplicationInfoSpace()->fireItemAvailable("tcdsBus",&m_tcdsBus);
    getApplicationInfoSpace()->fireItemAvailable("tcdsTopic",&m_tcdsTopic);
    getApplicationInfoSpace()->fireItemAvailable("outputBuses",&m_outputBuses);
    getApplicationInfoSpace()->fireItemAvailable("movingWindowSize",&m_moving_window_size); 
    getApplicationInfoSpace()->fireItemAvailable("hwconfig",&m_hwconfig);
    getApplicationInfoSpace()->fireItemAvailable("boardname",&m_boardname);
    getApplicationInfoSpace()->fireItemAvailable("topicsuffix",&m_topicsuffix);
    getApplicationInfoSpace()->fireItemAvailable("fakeerrorinterval",&m_fakeerrorinterval);
    getApplicationInfoSpace()->fireItemAvailable("fakeerrorduration",&m_fakeerrorduration);
    getApplicationInfoSpace()->fireItemAvailable("swclockThresholdMsec",&m_swclock_threshold_msec);    
    getApplicationInfoSpace()->fireItemAvailable("pauseSwclockDuration",&m_pause_swclock_duration);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool = m_poolFactory->createPool(urn,allocator);
  }catch(xdata::exception::Exception& e){
    std::string msg = "Failed to setup infospace ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::timesource::exception::HWApplication, msg, e);
  }catch(toolbox::mem::exception::Exception& e){
    std::string msg = "Failed to create memory pool ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::timesource::exception::HWApplication, msg, e);
  }  
  xdata::UnsignedInteger instanceid = getApplicationDescriptor()->getInstance();
  m_init_timername = getApplicationDescriptor()->getURN()+"_initializehw_timer_"+instanceid.toString();
  m_initializehw_timer = toolbox::task::getTimerFactory()->createTimer( m_init_timername );
  m_check_timername = getApplicationDescriptor()->getURN()+"_check_timer_"+instanceid.toString();
  m_check_timer = toolbox::task::getTimerFactory()->createTimer( m_check_timername );
  m_dummyclock_timername = getApplicationDescriptor()->getURN()+"_dummyclock_timer_"+instanceid.toString();
  m_dummyclock_timer = toolbox::task::getTimerFactory()->createTimer( m_dummyclock_timername );
  
  m_hwpoller = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_hwpoller_"+instanceid.toString(),"waiting");
  m_as_hwpoller = toolbox::task::bind(this,&bril::timesource::HWApplication::polling,"polling");  
}

bril::timesource::HWApplication::~HWApplication(){
  if(m_tcdsreceiver) delete m_tcdsreceiver;
}

void bril::timesource::HWApplication::Default (xgi::Input * in, xgi::Output * out){ 
}

void bril::timesource::HWApplication::actionPerformed(toolbox::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received toolbox::Event " << e.type());
  if( e.type() == "eventing::api::BusReadyToPublish"){
    std::stringstream msg;
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    if( m_unreadybuses.find(busname)!=m_unreadybuses.end() ){
      m_unreadybuses.erase(busname);
    }
    if(m_unreadybuses.size()!=0) return;    
  }
}

void bril::timesource::HWApplication::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received xdata::Event " << e.type());
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    m_outbuses = toolbox::parseTokenSet(m_outputBuses,",");
    m_unreadybuses = m_outbuses;
    for(std::set<std::string>::iterator it=m_outbuses.begin(); it!=m_outbuses.end(); ++it){
      this->getEventingBus(*it).addActionListener(this);
    }
    // subscribe to software tcds topic
    try{
      LOG4CPLUS_INFO(getApplicationLogger(),"subscribing to "+m_tcdsTopic.value_);
      this->getEventingBus(m_tcdsBus.value_).subscribe(m_tcdsTopic.value_);
    }catch(eventing::api::exception::Exception& e){
      std::stringstream msg;
      msg<<"Failed to subscribe to "<<m_tcdsBus.toString()<<" "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::timesource::exception::Exception,msg.str(),e);
    }
    // start initialize hw timer 
    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    toolbox::TimeInterval delta(5,0);
    start += delta;
    m_initializehw_timer->schedule(this,start,(void*)0, m_init_timername);
    
  }
}

void bril::timesource::HWApplication::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify" && ref){
    if(!m_hwready) return;
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    std::stringstream msg;
    if(topic == m_tcdsTopic.toString()){
      
      //parse tcds message table
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;
      xdata::UnsignedInteger32 current_tcds_run = 0;
      xdata::UnsignedInteger32 current_tcds_ls = 0;
      xdata::UnsignedInteger32 current_tcds_nb = 0;
      try{
	unsigned int previous_fill = m_current_tcds_fill.value_;
	serializer.import(&table, &inBuffer);
	xdata::Serializable* p = 0;
	p =  table.getValueAt(0,"FillNumber");
	if(p){
	  m_current_tcds_fill = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_;
	  p = 0;
	}
	p = table.getValueAt(0,"RunNumber");
	if(p){
	  current_tcds_run = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_;
	  p = 0;
	}
	p = table.getValueAt(0,"SectionNumber");
	if(p){
	  current_tcds_ls = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_ ;
	  p = 0;
	}
	p = table.getValueAt(0,"NibbleNumber");
	if(p){
	  current_tcds_nb = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_ ;
	  p = 0;
	}
	p = table.getValueAt(0,"CMSRunActive");
	if(p){
	  m_cmson = dynamic_cast<xdata::Boolean*>( p )->value_ ;	  
	  p = 0;
	}
	p = table.getValueAt(0,"NumBXBeamActive");
	if(p){
	  unsigned int ncollidingbx = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	  ncollidingbx = ncollidingbx/4096;
	  p = 0;
	  if( m_current_tcds_fill.value_ != previous_fill ){//this is a new fill , clear the moving window and trust/publish the new value
	    m_ncollidingbx = ncollidingbx;
	    m_nbx_moving_window.clear();
	  }else{ //this is ongoing fill, push ncollidingbx into the moving window
	    m_nbx_moving_window.push_back(ncollidingbx);
	    if( m_nbx_moving_window.size()>m_moving_window_size.value_ ){  //if the queue is full, rotate the queue and check if all elements are equal. 
	      m_nbx_moving_window.pop_front();
	      if( std::equal( m_nbx_moving_window.begin()+1, m_nbx_moving_window.end(), m_nbx_moving_window.begin() ) ){
		m_ncollidingbx = ncollidingbx; //if all elements in the window are equal, this value is stable enough to be trusted/published, else keep publishing the old value m_ncollidingbx 		  
	      }else{
		msg.clear(); msg.str("");	  
		msg<<"tcds fill "<<m_current_tcds_fill.value_<<" nbx moving window elements are not equal , FLIPPING ";
		LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
	      }
	    }  
	  }	    
	}
	p = table.getValueAt(0,"DeadtimeBeamActive");
	if(p){
	  float dtbeam = dynamic_cast<xdata::Float*>(p)->value_/100. ;
	  if(m_ncollidingbx.value_>0 && m_cmson.value_){
	    m_deadfrac.value_ = m_deadfrac.value_ + dtbeam;
	    m_ndeadfrac.value_ = m_ndeadfrac.value_ + 1;//to be used to make average deadfrac when publishing.
	  }
	  p = 0;	    
	}
	
	p = table.getValueAt(0,"NumOrbits");
	if(p){
	  m_norbs = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	  p = 0;
	}
	p = table.getValueAt(0,"NumNibblesPerSection");
	if(p){
	  m_nbperls = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	  p = 0;
	}
	
	msg<<"TCDS "<<m_current_tcds_fill.value_<<" "<<current_tcds_run.toString()<<" "<<current_tcds_ls.toString()<<" "<<current_tcds_nb.toString()<<" : deadfrac "<<m_deadfrac.toString()<<" ncollidingbx "<<m_ncollidingbx.toString()<<" norbits "<<m_norbs.toString()<<" nbperls "<<m_nbperls.toString();	
	LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());	 
	
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	++m_pause_swclock_count;
	if(m_pause_swclock_count < m_pause_swclock_duration ||  !m_pause_swclock_duration){
	  m_last_swclock_message = now.sec()*1000+now.millisec();
	}else{
	  //std::cout << "NOT UPDATING" << std::endl;
	  if(m_pause_swclock_count>=2*m_pause_swclock_duration){
	    m_pause_swclock_count = 0;
	  }
	}
	bril::timesource::tcdsinfo_t thistcdsinfo(m_current_tcds_fill.value_, current_tcds_run.value_, current_tcds_ls.value_, current_tcds_nb.value_);
	do_publish_timesignal("NB1"+m_topicsuffix.value_,1,thistcdsinfo,now,SWCLOCKSOURCE);
	if( current_tcds_nb%4==0 ){//
	  do_publish_timesignal("NB4"+m_topicsuffix.value_,4,thistcdsinfo,now,SWCLOCKSOURCE);
	  do_publish_tcds(interface::bril::tcdsT::topicname()+m_topicsuffix.value_,thistcdsinfo,now,SWCLOCKSOURCE);
	}
      }catch (xdata::exception::Exception& err){
	LOG4CPLUS_ERROR(getApplicationLogger(),"Failed to deserialize incoming tcds table"); 
	if(ref!=0) ref->release();
      }
    }
  }
}

void bril::timesource::HWApplication::timeExpired(toolbox::task::TimerEvent& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"Enter timeExpired");
  std::stringstream msg;
  std::string taskname = e.getTimerTask()->name;
  toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

  if( taskname == m_init_timername  ){
    try{
      m_tcdsreceiver = new bril::timesource::TCDSHWReceiver(m_hwconfig.value_,m_boardname.value_,&getApplicationLogger(),m_fakeerrorinterval.value_,m_fakeerrorduration.value_);
      m_tcdsreceiver->setup_board();
    } catch ( bril::timesource::exception::SystemBoardError& e ){
      XCEPT_RAISE(bril::timesource::exception::SystemBoardError,e.what());
    }
    usleep(100000);
    uint32_t cdce_ready = m_tcdsreceiver->cdce_locked();
    if( !cdce_ready ){
      msg<<"CDCE is not locked (Missing TCDS backplane clock?)";
      XCEPT_RAISE(bril::timesource::exception::SystemBoardError,msg.str());
    }
    
    m_hwready = true;
    
    // start hardware poller
    m_hwpoller->activate();
    m_hwpoller->submit(m_as_hwpoller);
    
    // start check timer
    toolbox::TimeInterval interval( 1,456 );//check every 4 nibbles
    m_check_timer->scheduleAtFixedRate(now,this,interval,(void*)0,m_check_timername);

    // start dummy clock timer
    unsigned int nibble_usec = 364000;
    toolbox::TimeInterval dummyclock_interval( 0,nibble_usec );//dummy clock fire every 1 nibble
    m_dummyclock_timer->scheduleAtFixedRate(now,this,dummyclock_interval,(void*)0,m_dummyclock_timername);
    
  }else if( taskname == m_check_timername ){
    //check hw clock polling
    if( m_hwclock_success > 2 ){ //hw good
      if (m_next_publishfrom != bril::timesource::HWCLOCKSOURCE) {
      	LOG4CPLUS_INFO( getApplicationLogger(),"Switching to HW" );
      }
      m_next_publishfrom = bril::timesource::HWCLOCKSOURCE;
    }else {
      unsigned int deltaT_msec = now.sec()*1000+now.millisec() - m_last_swclock_message;
      if( deltaT_msec < m_swclock_threshold_msec.value_ ){
	m_next_publishfrom = bril::timesource::SWCLOCKSOURCE;
	LOG4CPLUS_INFO( getApplicationLogger(),"Switching to SW" );
      }else{
	m_next_publishfrom = bril::timesource::DUMMYCLOCKSOURCE;
	LOG4CPLUS_INFO( getApplicationLogger(),"Switching to DUMMY" );
      }
    }    
    
    /**
    unsigned int delta = nowmsec - m_last_successfulpoll;
    if( delta > m_hwpollinterval*4 && m_last_successfulpoll!=0 ){
      msg<<"hardware stuck for "<<delta<<" msec";
      LOG4CPLUS_ERROR( getApplicationLogger(),msg.str() );
      msg.str(""); msg.clear();
    }
    delta = nowmsec - m_last_publish;
    if( delta > m_hwpollinterval*20 && m_last_publish!=0 ){
      msg<<"publishing stuck for "<<delta<<" msec";
      LOG4CPLUS_ERROR( getApplicationLogger(),msg.str() );
      }*/
  }else if( taskname == m_dummyclock_timername ){
    bril::timesource::tcdsinfo_t newtcdsinfo = m_current_clockinfo;
    ++newtcdsinfo;
    do_publish_timesignal("NB1"+m_topicsuffix.value_,1,newtcdsinfo,now,DUMMYCLOCKSOURCE);
    if( newtcdsinfo.lumi_nibble%4==0 ){
      do_publish_timesignal("NB4"+m_topicsuffix.value_,4,newtcdsinfo,now,DUMMYCLOCKSOURCE);
      do_publish_tcds("tcds"+m_topicsuffix.value_,newtcdsinfo,now,DUMMYCLOCKSOURCE);
    }
  }
}

bool bril::timesource::HWApplication::polling( toolbox::task::WorkLoop* wl ){ 
  LOG4CPLUS_DEBUG(getApplicationLogger(),"Enter polling");
  std::stringstream msg;
  /**
  unsigned int tcdsready = 0;
  try{
    tcdsready = m_tcdsreceiver->tcds_ready();
  }catch(...){
    LOG4CPLUS_ERROR( getApplicationLogger(),"TCDS not ready??" );
    m_hwclock_success = 0;
    usleep(m_hwpollinterval);
    return true;
  }
  */
  unsigned int tcdsready = 1; // remove this if above block is uncommented.
  if(tcdsready){
    bril::timesource::tcdsinfo_t tcds;
    try{
      tcds = m_tcdsreceiver->read_tcds();
    }catch( bril::timesource::exception::TCDSReadError& e ){
      m_hwclock_success = 0;
      LOG4CPLUS_ERROR( getApplicationLogger(), std::string("caught timesource::exception::TCDSReadError ")+e.what() );
      usleep(m_hwpollinterval);
      return true;
    }catch( bril::timesource::exception::TCPConnectError& e  ){//
      m_hwclock_success = 0;
      LOG4CPLUS_ERROR( getApplicationLogger(), std::string("caught timesource::exception::TCPConnectError " )+e.what() );
      usleep(m_hwpollinterval);
      return true;
    }catch( bril::timesource::exception::UnknownError& e  ){//
      m_hwclock_success = 0;
      LOG4CPLUS_ERROR( getApplicationLogger(), std::string("caught timesource::exception::UnknownError ")+e.what() );
      usleep(m_hwpollinterval);
      return true;
    }
    msg << "Pollout : " << tcds.lhc_fill << " , " << tcds.cms_run << " , " << tcds.lumi_section << " , " << tcds.lumi_nibble;
    LOG4CPLUS_DEBUG( getApplicationLogger(),msg.str() );
    msg.str("");msg.clear();
    toolbox::TimeVal t=toolbox::TimeVal::gettimeofday();
    //m_last_successfulpoll = t.sec()*1000+t.millisec();    
    unsigned int nbnum = tcds.lumi_nibble;
    
    if( nbnum!=m_current_clockinfo.lumi_nibble ){
      t = toolbox::TimeVal::gettimeofday();
      bril::timesource::tcdsinfo_t tcdsinfo(tcds.lhc_fill,tcds.cms_run,tcds.lumi_section,tcds.lumi_nibble);     
      msg << "HW: "<<tcdsinfo.toString();
      LOG4CPLUS_DEBUG( getApplicationLogger(),msg.str() );
      do_publish_timesignal("NB1"+m_topicsuffix.value_,1,tcdsinfo,t,HWCLOCKSOURCE);
      if( nbnum%4==0 ){
	do_publish_timesignal("NB4"+m_topicsuffix.value_,4,tcdsinfo,t,HWCLOCKSOURCE);
	do_publish_tcds(interface::bril::tcdsT::topicname()+m_topicsuffix.value_,tcdsinfo,t,HWCLOCKSOURCE);
      }
    }
  }else{
    m_hwclock_success = 0;
    LOG4CPLUS_ERROR( getApplicationLogger(),"TCDS not ready??" );
    usleep(m_hwpollinterval);
    return true;
  }
  if( m_hwclock_success<10000 ){
    m_hwclock_success++;
  }
  usleep(m_hwpollinterval);
  return true;
}

void bril::timesource::HWApplication::do_publish_timesignal(const std::string& topicname, unsigned int frequency,const tcdsinfo_t& clockinfo, const toolbox::TimeVal& t, bril::timesource::CLOCKTYPE clocktype){
  if(clocktype != m_next_publishfrom){
    return;
  }
  std::stringstream msg;
  toolbox::mem::Reference* bufRef = 0;
  size_t msgsize = sizeof(interface::bril::shared::DatumHead);
  try{
    bufRef = m_poolFactory->getFrame(m_memPool,msgsize);
    bufRef->setDataSize(msgsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(clockinfo.lhc_fill,clockinfo.cms_run,clockinfo.lumi_section,clockinfo.lumi_nibble,t.sec(),t.millisec());
    header->setResource(interface::bril::shared::DataSource::TCDS,0,0,0);
    header->setFrequency(frequency);
    header->setTotalsize(msgsize);
    for(std::set<std::string>::iterator it=m_outbuses.begin();it!=m_outbuses.end();++it){
      msg<<"Publish "<<topicname<<" to "<<*it<<" : "<<clockinfo.toString()<<" "<<t.sec()<<"."<<t.millisec()<<" nbx "<<m_ncollidingbx.value_;
      LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
      this->getEventingBus(*it).publish(topicname, bufRef->duplicate(), plist);
    }
    if(bufRef){
      bufRef->release(); bufRef = 0;
    }    
  }catch(xcept::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef) bufRef->release();
    //notify qualified
  }

  if( frequency==1 ){
    // clock itself should not update cached values
    m_current_clockinfo = clockinfo;
  }
  
}

void bril::timesource::HWApplication::do_publish_tcds( const std::string& topicname, const bril::timesource::tcdsinfo_t& clockinfo, const toolbox::TimeVal& t, bril::timesource::CLOCKTYPE clocktype){
  if(clocktype != m_next_publishfrom){
    return;
  }
  std::stringstream msg;
  toolbox::mem::Reference* bufRef = 0;
  size_t msgsize = interface::bril::tcdsT::maxsize();
  std::string payloaddict = interface::bril::tcdsT::payloaddict();
  try{
    bufRef = m_poolFactory->getFrame(m_memPool,msgsize);
    bufRef->setDataSize(msgsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT", payloaddict );
    if( m_ncollidingbx.value_<1 ){
      plist.setProperty("NOSTORE", "1");
    }
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(clockinfo.lhc_fill,clockinfo.cms_run,clockinfo.lumi_section,clockinfo.lumi_nibble,t.sec(),t.millisec());
    header->setResource(interface::bril::shared::DataSource::TCDS,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(msgsize);
    interface::bril::shared::CompoundDataStreamer tc(payloaddict); 
    tc.insert_field( header->payloadanchor, "norb", &m_norbs.value_ );
    tc.insert_field( header->payloadanchor, "nbperls", &m_nbperls.value_ );
    tc.insert_field( header->payloadanchor, "cmson", &m_cmson.value_ );
    float mydeadfrac = 1.0;
    if( m_cmson.value_ && m_ncollidingbx.value_>0 && m_ndeadfrac.value_>0 ){
      mydeadfrac = m_deadfrac.value_/float(m_ndeadfrac.value_);
    }
    tc.insert_field( header->payloadanchor, "deadfrac", &mydeadfrac );    
    tc.insert_field( header->payloadanchor, "ncollidingbx", &m_ncollidingbx.value_ );    
    for(std::set<std::string>::iterator it=m_outbuses.begin();it!=m_outbuses.end();++it){
      msg<<"Publish "<<topicname<<" to "<<*it<<" "<<m_next_publishfrom<<" : "<<clockinfo.toString()<<" "<<t.sec()<<"."<<t.millisec()<<" cmson "<<m_cmson.value_<<" nbx "<<m_ncollidingbx.value_<<" deadfrac "<<mydeadfrac<<" averaged over "<<m_ndeadfrac.value_;
      LOG4CPLUS_INFO(getApplicationLogger(),msg.str());

      this->getEventingBus(*it).publish(topicname, bufRef->duplicate(), plist);
      msg.str("");msg.clear();      
    }
    if(bufRef){
      bufRef->release(); bufRef = 0;
    }
    m_applock.take();
    m_deadfrac = 0.0;
    m_ndeadfrac = 0;    
    m_applock.give();
  }catch(xcept::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef) bufRef->release();
  }
}
