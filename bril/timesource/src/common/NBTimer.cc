#include "bril/timesource/Common.h"
#include "bril/timesource/NBTimer.h"
#include "bril/timesource/exception/Exception.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xcept/tools.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/TCDSTopics.hh"
namespace bril{
  namespace timesource{
    NBTimer::NBTimer(xdaq::Application* app,const std::string& signaltopic,unsigned int nbfrequency,const std::set<std::string>& buses,xdata::InfoSpace* monInfoSpace):eventing::api::Member(app),m_app(app),m_signaltopic(signaltopic),m_nbfrequency(nbfrequency),m_buses(buses),m_monInfoSpace(monInfoSpace){
      m_timername = "timer_"+m_signaltopic;
      m_timer = 0;
      m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
      for(std::set<std::string>::const_iterator bb=buses.begin();bb!=buses.end();++bb){
	if(bb!=buses.begin()){m_busstr+=",";}
	m_busstr += *bb;
      }
      std::string memPoolName = m_signaltopic + std::string("_memPool");
      toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
      try{
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	m_memPool = m_poolFactory->createPool(urn,allocator);
	m_app->getApplicationInfoSpace()->addItemChangedListener("startrun",this);
      }catch(xcept::Exception & e){
	std::string msg("Failed to listen to app infospace");
	LOG4CPLUS_ERROR(m_app->getApplicationLogger(), stdformat_exception_history(e));
	XCEPT_RETHROW(bril::timesource::exception::NBTimer, msg, e);
      }
      initparams();
    }

    void NBTimer::actionPerformed(xdata::Event& e){
      std::stringstream msg;
      msg<<"NBTimer received xdata::Event "<<e.type()<<std::endl;	
      LOG4CPLUS_DEBUG(m_app->getApplicationLogger(), msg.str());
      msg.str("");      
      if( e.type() == "ItemChangedEvent"){
	std::string item = dynamic_cast<xdata::ItemEvent&>(e).itemName();
	if(item == "startrun"){
	  xdata::Serializable * runnum = dynamic_cast<xdata::ItemEvent&>(e).item();	  
	  m_currentrun = dynamic_cast<xdata::UnsignedInteger*>(runnum)->value_;	 
	  initparams();
	  //do_publish();
	  //if(m_nbfrequency==4){
	  //  do_publish_faketcds();
	  //}
	  toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	  toolbox::TimeInterval duration((double)m_nbfrequency*(s_nbduration));
	  try{
	    m_timer = toolbox::task::getTimerFactory()->createTimer(m_timername);
	    m_timer->scheduleAtFixedRate(start,this,duration,(void*)0,m_timername);
	  }catch(toolbox::task::exception::Exception& e){
	    std::string msg = "Failed to start "+m_timername;
	    LOG4CPLUS_FATAL (m_app->getApplicationLogger(),msg+" "+stdformat_exception_history(e));
	  }
	}
      }else{
	msg << "Failed to process unknown event type '"<<e.type()<<"'";
	LOG4CPLUS_ERROR(m_app->getApplicationLogger(), msg.str());
      }
    }

    void NBTimer::actionPerformed(toolbox::Event& e){
      std::stringstream msg;
      msg<< "NBTimer received toolbox::Event "<<e.type()<<std::endl;	
      LOG4CPLUS_DEBUG(m_app->getApplicationLogger(), msg.str());	
      msg.str("");
      if( e.type() == "urn:bril-timesource-event:RunStop" ){
	if(m_timer){
	  try{
	    m_timer->stop();
	    toolbox::task::getTimerFactory()->removeTimer(m_timername);
	    m_timer = 0;
	  }catch(toolbox::task::exception::Exception& e){
	    std::string msg("Failed to stop "+m_timername);
	    LOG4CPLUS_FATAL(m_app->getApplicationLogger(),stdformat_exception_history(e));	    
	  }
	}
      }
    }
    
    void NBTimer::timeExpired(toolbox::task::TimerEvent& e){
      m_nnbs += m_nbfrequency;
      if(m_nnbs>s_nbperls){
	++m_currentls;
	m_nnbs=0;
      }
      if(m_nnbs == 0){
	m_currentnb = 1;
      }else{	
	m_currentnb = m_nnbs;
      }
      if(m_monInfoSpace){
	try{
	  xdata::UnsignedInteger* currentls = dynamic_cast<xdata::UnsignedInteger*>(m_monInfoSpace->find("currentls"));
	  m_monInfoSpace->lock();
	  *currentls = m_currentls;
	  m_monInfoSpace->unlock();
	}catch(xdata::exception::Exception& e){
	  std::string msg("Failed to find currentls in monInfoSpace ");
	  LOG4CPLUS_FATAL(m_app->getApplicationLogger(),stdformat_exception_history(e));
	}
	try{
	  xdata::UnsignedInteger* currentnb = dynamic_cast<xdata::UnsignedInteger*>(m_monInfoSpace->find("currentnb"));
	  m_monInfoSpace->lock();
	  *currentnb = m_currentnb;
	  m_monInfoSpace->unlock();
	}catch(xdata::exception::Exception& e){
	  std::string msg("Failed to find currentnb in monInfoSpace ");
	  LOG4CPLUS_FATAL(m_app->getApplicationLogger(),stdformat_exception_history(e));
	} 
	do_publish();
	if(m_nbfrequency==4){// only NB4 timer publishes also fake tcds topic
	  do_publish_faketcds();
	}
      }
    }
    
    void NBTimer::do_publish(){
      std::stringstream msg;
      xdata::Serializable* currentfill=m_app->getApplicationInfoSpace()->find("currentfill");
      if(currentfill!=0){
	xdata::UnsignedInteger* c = dynamic_cast<xdata::UnsignedInteger*>(currentfill);
	m_currentfill = c->value_;
      }
      msg <<"fill "<<m_currentfill<<" run "<<m_currentrun<<" ls "<<m_currentls<<" nb "<<m_currentnb;
      LOG4CPLUS_INFO(m_app->getApplicationLogger(),msg.str());
      toolbox::mem::Reference* bufRef=0;
      try{
	xdata::Properties plist;
	plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
	toolbox::TimeVal t = toolbox::TimeVal::gettimeofday(); 
	bufRef = m_poolFactory->getFrame(m_memPool,sizeof(interface::bril::shared::DatumHead));
	interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
	header->setTime(m_currentfill,m_currentrun,m_currentls,m_currentnb,t.sec(),t.millisec());
	header->setResource(interface::bril::shared::DataSource::TCDS,0,0,0);
	header->setTotalsize(sizeof(interface::bril::shared::DatumHead));
	header->setFrequency(m_nbfrequency);
	for(std::set<std::string>::iterator it=m_buses.begin();it!=m_buses.end();++it){
	  this->getEventingBus(*it).publish(m_signaltopic,bufRef->duplicate(),plist);
	  msg.str("");
	  msg << "publish to "<<*it<<":"<<m_signaltopic;
	  LOG4CPLUS_DEBUG(m_app->getApplicationLogger(),msg.str());
	}
	if(bufRef){
	  bufRef->release(); bufRef = 0;
	}
      }catch(xcept::Exception& e){
	std::string msg("Failed to publish to topic "+m_signaltopic);
	LOG4CPLUS_ERROR(m_app->getApplicationLogger(),stdformat_exception_history(e));
	if(bufRef){
	  bufRef->release(); bufRef = 0;
	}
      }
    }

    void NBTimer::initparams(){
      m_currentnb = 0;
      m_currentls = 1;
      m_nnbs = 0;
    }

    void NBTimer::do_publish_faketcds(){
      std::stringstream msg;
      toolbox::mem::Reference* bufRef = 0;
      toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
      size_t msgsize = interface::bril::tcdsT::maxsize();
      std::string payloaddict = interface::bril::tcdsT::payloaddict();
      std::string topicname = interface::bril::tcdsT::topicname();
      try{
	bufRef = m_poolFactory->getFrame(m_memPool,msgsize);
	bufRef->setDataSize(msgsize);
	xdata::Properties plist;
	plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
	plist.setProperty("PAYLOAD_DICT", payloaddict );
	interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
	header->setTime(m_currentfill,m_currentrun,m_currentls,m_currentnb,t.sec(),t.millisec());
	header->setResource(interface::bril::shared::DataSource::TCDS,0,0,interface::bril::shared::StorageType::COMPOUND);
	header->setFrequency(4);
	header->setTotalsize(msgsize);
	unsigned int norbs = 4096;
	unsigned int nbperls = 64;
	bool cmson = false;
	float deadfrac = 1;
	unsigned int ncollidingbx = 0;
	interface::bril::shared::CompoundDataStreamer tc(payloaddict); 
	tc.insert_field( header->payloadanchor, "norb", &norbs );
	tc.insert_field( header->payloadanchor, "nbperls", &nbperls );
	tc.insert_field( header->payloadanchor, "cmson", &cmson );
	tc.insert_field( header->payloadanchor, "deadfrac", &deadfrac );
	tc.insert_field( header->payloadanchor, "ncollidingbx", &ncollidingbx );
	msg<<"Publish fake "<<topicname<<" cmson "<<cmson<<" deadfrac "<<deadfrac<<" ncollidingbx "<<ncollidingbx;	
	LOG4CPLUS_INFO(m_app->getApplicationLogger(), msg.str());
	for(std::set<std::string>::iterator it=m_buses.begin();it!=m_buses.end();++it){
	  this->getEventingBus(*it).publish(topicname, bufRef->duplicate(), plist);
	}
	if(bufRef){
	  bufRef->release();
	  bufRef = 0;
	}
      }catch(xcept::Exception& e){
	LOG4CPLUS_ERROR(m_app->getApplicationLogger(),stdformat_exception_history(e));
	if(bufRef){
	  bufRef->release();
	  bufRef = 0;
	}
      }
    }
  }}
