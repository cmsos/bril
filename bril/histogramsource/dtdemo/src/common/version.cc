#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xgi/version.h"
#include "toolbox/version.h"
#include "bril/histogramsource/dtdemo/version.h"
#include "bril/histogramsource/dtdemo/version.h"

GETPACKAGEINFO(histogramsourcedtdemo)

void histogramsourcedtdemo::checkPackageDependencies() {
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xgi);
  CHECKDEPENDENCY(toolbox);
}

std::set<std::string, std::less<std::string>> histogramsourcedtdemo::getPackageDependencies() {
    std::set<std::string, std::less<std::string>> dependencies;
    ADDDEPENDENCY(dependencies,config);
    ADDDEPENDENCY(dependencies,xcept);
    ADDDEPENDENCY(dependencies,xdaq);
    ADDDEPENDENCY(dependencies,xgi);
    ADDDEPENDENCY(dependencies,toolbox);
    return dependencies;
}