#include "bril/histogramsource/dtdemo/Application.h"
#include "bril/histogramsource/generic/ReadoutInterface.h"

XDAQ_INSTANTIATOR_IMPL(bril::histogramsourcedtdemo::Application)

namespace bril{
    namespace histogramsourcedtdemo{

        Application::Application(xdaq::ApplicationStub * s) : bril::histogramsourcegeneric::Application::Application(s) {
        };

        void Application::ConfigureHardware() {
            // this stuff below can be done in generic
            // to do so one would have to move the data delay register to the ipbus interface, to make it not dtdemo specific
            // set readout interface
            xdata::Vector<xdata::Properties> config;
            if (m_config_interface_type == "tcp") {
                config = m_config_tcp_boards;
            } else if (m_config_interface_type == "ipbus") {
                config = m_config_ipbus_boards;
            }

            // now write delays
            for (size_t element = 0; element < config.elements(); element++) {
                std::set<std::string> t_channel_prefix_set = toolbox::parseTokenSet (config[element].getProperty ("board-channel-prefixes"), ",");
                std::set<std::string> t_data_delays = toolbox::parseTokenSet (config[element].getProperty ("data-delays"), ",");
                std::set<std::string>::iterator it_ch_pre;
                std::set<std::string>::iterator it_data_delays;
                for(it_ch_pre = t_channel_prefix_set.begin(), it_data_delays = t_data_delays.begin() ; it_ch_pre != t_channel_prefix_set.end() && it_data_delays != t_data_delays.end() ; it_ch_pre++, it_data_delays++) {
                    // FIXME this all has to be moved to generic
                    // and use the prefixes, like this:
                    //m_backend_interface[element]->write(*it_ch_pre+".data_latency, std::stoi(*it_data_delays));
                    // instead hard code
                    m_backend_interface[element]->write("lumi.csr.conf.data_latency", std::stoi(*it_data_delays));
                }
            }
        }
    }
}

