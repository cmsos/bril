#ifndef _otsource_Application_h_
#define _otsource_Application_h_

#include "bril/histogramsource/generic/Application.h"
#include "interface/bril/HistogramOtTopics.hh"

namespace bril{
    namespace histogramsourceot {

        class Application: public bril::histogramsourcegeneric::Application {

            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);

            protected:
                // these methods are detector specific
                std::string getApplicationSummary() { return std::string("Histogram Source OT"); }
                uint32_t getHistogramMaxsize() { return interface::bril::ot_rawhistT::maxsize(); }
                std::string getHistogramTopicname() { return interface::bril::ot_rawhistT::topicname(); }
		std::string getHistogramPayloadDict() { return interface::bril::ot_rawhistT::payloaddict(); }
                void ConfigureHardware();

        };

    }
}

#endif
