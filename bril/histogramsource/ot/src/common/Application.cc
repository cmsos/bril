#include "bril/histogramsource/ot/Application.h"
#include "bril/histogramsource/generic/ReadoutInterface.h"

XDAQ_INSTANTIATOR_IMPL(bril::histogramsourceot::Application)

namespace bril{
    namespace histogramsourceot{

        Application::Application(xdaq::ApplicationStub * s) : bril::histogramsourcegeneric::Application::Application(s) {

        };

        void Application::ConfigureHardware() {
            ;
        }
    }
}

