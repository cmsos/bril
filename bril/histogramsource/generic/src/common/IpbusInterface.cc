#include <bril/histogramsource/generic/IpbusInterface.h>

#include <cstring>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdexcept>

namespace bril{
    namespace histogramsourcegeneric {
        IpbusInterface::IpbusInterface(uhal::HwInterface hw_interface, uint32_t histogram_readout_interval_usec) : ReadoutInterface(histogram_readout_interval_usec), m_hw_interface(hw_interface) {
            m_initialized = true;
            m_configured = checkConnection();
        }

        IpbusInterface::~IpbusInterface() {
            ;
        }

        bool IpbusInterface::checkConnection() {
            //
            bool result;
            // open
            try {
                // dummy command
                std::vector<uint32_t> data;
                result = true;
            } catch (const std::exception& e) {
                throw("Failed to establish connection");
                result = false;
            }
            return result;
        }

        uint32_t IpbusInterface::read(std::string reg_name) {
            uint32_t result = 0;
            try {
                auto data = m_hw_interface.getNode(reg_name).read();
                m_hw_interface.dispatch();
                result = data.value();
            } catch (uhal::exception::NoBranchFoundWithGivenUID &e) {
                throw std::runtime_error(e.what());
            }
            return result;
        }

        std::vector<uint32_t> IpbusInterface::readBlock(std::string reg_name, uint32_t size) {
            std::vector<uint32_t> result;
            try {
                auto data = m_hw_interface.getNode(reg_name).readBlock(size);
                m_hw_interface.dispatch();
                result = data.value();
            } catch (uhal::exception::NoBranchFoundWithGivenUID &e) {
                throw std::runtime_error(e.what());
            }
            return result;
        }

        void IpbusInterface::write(std::string reg_name, uint32_t data) {
            try {
                m_hw_interface.getNode(reg_name).write(data);
                m_hw_interface.dispatch();
            } catch (uhal::exception::NoBranchFoundWithGivenUID &e) {
                throw std::runtime_error(e.what());
            }
        }

        void IpbusInterface::writeBlock(std::string reg_name, std::vector<uint32_t> data) {
            try {
                m_hw_interface.getNode(reg_name).writeBlock(data);
                m_hw_interface.dispatch();
            } catch (uhal::exception::NoBranchFoundWithGivenUID &e) {
                throw std::runtime_error(e.what());
            }
        }

        std::vector<std::pair<std::string, std::string>> IpbusInterface::getConfiguration() {
            std::vector<std::pair<std::string, std::string>> result;
            result.push_back(std::make_pair("Interface type", "IPbus"));
            result.push_back(std::make_pair("ID", m_hw_interface.id()));
            result.push_back(std::make_pair("URI", m_hw_interface.uri()));
            result.push_back(std::make_pair("Histogram readout interval (us)", std::to_string(m_histogram_readout_interval_usec)));
            return result;
        }

    }
}