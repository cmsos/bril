#include "bril/histogramsource/generic/Application.h"
#include "bril/histogramsource/generic/SimpleTcpInterface.h"
#include "bril/histogramsource/generic/IpbusInterface.h"
#include "bril/histogramsource/generic/exception/Exception.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "histograminterface/BrilHistogram.h"
#include "uhal/ConnectionManager.hpp"
#include "b2in/nub/Method.h"
#include "toolbox/mem/AutoReference.h"
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL(bril::histogramsourcegeneric::Application)

namespace bril{
    namespace histogramsourcegeneric{

        Application::Application(xdaq::ApplicationStub * s): xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this) {
            // set icon
            s->getDescriptor()->setAttribute("icon", "/bril/histogramsource/generic/html/histogram.png");
	        s->getDescriptor()->setAttribute("icon16x16", "/bril/histogramsource/generic/html/histogram.png");

            // web part
            xgi::framework::deferredbind(this, this, &bril::histogramsourcegeneric::Application::Default, "Default");
            LOG4CPLUS_INFO(this->getApplicationLogger(), toolbox::toString("Load URL %s in a browser", getApplicationContext()->getContextDescriptor()->getURL().c_str()));

            // set shutdown listener
            m_shutdown_request = false;
            toolbox::getRuntime()->addShutdownListener( this );

            // set configuration
            try {
                this->getApplicationInfoSpace()->fireItemAvailable("interface_type", &m_config_interface_type );
                this->getApplicationInfoSpace()->fireItemAvailable("tcp_boards", &m_config_tcp_boards );
                this->getApplicationInfoSpace()->fireItemAvailable("ipbus_general", &m_config_ipbus_general );
                this->getApplicationInfoSpace()->fireItemAvailable("ipbus_boards", &m_config_ipbus_boards );
                this->getApplicationInfoSpace()->fireItemAvailable("do_histogram_reset", &m_do_histogram_reset );
                this->getApplicationInfoSpace()->fireItemAvailable("backend_poll_interval", &m_config_backend_poll_interval );
                this->getApplicationInfoSpace()->fireItemAvailable("push_interval", &m_config_push_interval );
                this->getApplicationInfoSpace()->fireItemAvailable("use_external_tcds_info", &m_config_use_external_tcds_info );
                this->getApplicationInfoSpace()->fireItemAvailable("frequency_nb", &m_config_frequency_nb );
                this->getApplicationInfoSpace()->fireItemAvailable("eventinginput", &m_config_eventing_input );
                this->getApplicationInfoSpace()->fireItemAvailable("eventingoutput", &m_config_eventing_output );
                this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
            } catch(xdata::exception::Exception& e) {
                LOG4CPLUS_FATAL(getApplicationLogger(), __func__ << "Failed to set infospace" );
                XCEPT_RAISE(bril::histogramsourcegeneric::exception::applicationException, "");
            }

            // memory pool
            try {
                toolbox::mem::CommittedHeapAllocator * allocator = new toolbox::mem::CommittedHeapAllocator(0x4000000);
                toolbox::net::URN mem_urn( "mem", getApplicationDescriptor()->getURN() );
                m_memory_pool = toolbox::mem::getMemoryPoolFactory()->createPool(mem_urn, allocator);
            } catch(toolbox::task::exception::Exception & e) {
                LOG4CPLUS_FATAL(getApplicationLogger(), __func__ << "Failed to allocate a memory pool" );
                XCEPT_RAISE(bril::histogramsourcegeneric::exception::applicationException, "");
            }

            // some variables

            // done
            LOG4CPLUS_INFO(this->getApplicationLogger(),  "Done init");
        }

        Application::~Application() {
            for (auto board : m_backend_interface) delete board;
            m_backend_interface.clear();
            delete m_readhist_wl;
            delete m_publishing_wl;
        }


        void Application::Default(xgi::Input * in, xgi::Output * out) {
            //*out << "<script src=\"/bril/histogramsource/generic/html/homepage.js\"></script>";
            *out << "<h2 align=\"center\">" << this->getApplicationSummary() << "</h2>";
            *out << cgicc::br();

            if (m_backend_interface.size() > 0) {
                // out
                *out << "<h3>Back-End Settings</h3>" << std::endl;

                // begin table
                *out << cgicc::div() << std::endl;
                uint16_t board_id = 0;
                for (auto board : m_backend_interface) {
                    *out << cgicc::table().set("class", "xdaq-table-vertical").set("style", "display: inline-block") << std::endl;
                    *out << cgicc::tbody() << std::endl;
                    *out << cgicc::tr() << "<th align=\"center\" colspan=2>" << "Board " << board_id << "</th>" << cgicc::tr() << std::endl;
                    for (auto parameter : board->getConfiguration()) {
                        *out << cgicc::tr();
                        *out << cgicc::th(parameter.first.c_str());
                        *out << cgicc::td();
                        *out << parameter.second.c_str();
                        *out << cgicc::td();
                        *out << cgicc::tr() << std::endl;
                    }

                    // end table
                    *out << cgicc::tbody() << std::endl;
                    *out << cgicc::table();

                    // white space between tables
                    *out << "   ";

                    board_id++;
                }
                *out << cgicc::div() << std::endl;
                *out << cgicc::br();

            }



            // back-end status
            *out << "<h3>Back-End Status</h3>" << std::endl;

            // begin table
            *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
            *out << cgicc::tbody() << std::endl;

            *out << cgicc::tr();
            *out << cgicc::th("Board");
            for (uint16_t board_id = 0; board_id < m_backend_interface.size(); board_id++) {
                *out << cgicc::th();
                *out << board_id;
                *out << cgicc::th();
            }
            *out << cgicc::tr() << std::endl;

            *out << cgicc::tr();
            *out << cgicc::th("Initialized");
            for (auto board : m_backend_interface) {
                *out << cgicc::td();
                if (board->isInitialized()) *out << cgicc::p().set( "style", "color: green")  << "True" << cgicc::p();
                else *out << cgicc::p().set( "style", "color: red")  << "False" << cgicc::p();
                *out << cgicc::td();
            }
            *out << cgicc::tr() << std::endl;

            *out << cgicc::tr();
            *out << cgicc::th("Configured");
            for (auto board : m_backend_interface) {
                *out << cgicc::td();
                if (board->isConfigured()) *out << cgicc::p().set( "style", "color: green")  << "True" << cgicc::p();
                else *out << cgicc::p().set( "style", "color: red")  << "False" << cgicc::p();
                *out << cgicc::td();
            }
            *out << cgicc::tr() << std::endl;

            // end table
            *out << cgicc::tbody() << std::endl;
            *out << cgicc::table();
            *out << cgicc::br();


            // read-out status
            *out << "<h3>Read-out Status</h3>" << std::endl;

            // begin table
            *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
            *out << cgicc::tbody() << std::endl;

            *out << cgicc::tr();
            *out << cgicc::th("Channel \\ Algo");
            for (auto ch_algo : m_backend_channel_algo_vec) {
                *out << cgicc::th();
                *out << ch_algo.first << " \\ " << ch_algo.second;
                *out << cgicc::th();
            }
            *out << cgicc::tr() << std::endl;

            for (auto entry : this->getReadoutStatus()) {
                    *out << cgicc::tr();
                    *out << cgicc::th(entry.first);
                    for (auto ch_algo_val : entry.second) {
                        *out << cgicc::td();
                        *out << ch_algo_val;
                        *out << cgicc::td();
                    }
                    *out << cgicc::tr() << std::endl;
            }

            // end table
            *out << cgicc::tbody() << std::endl;
            *out << cgicc::table();
            *out << cgicc::br();

        }

        void Application::actionPerformed( xdata::Event& e ) {
            LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received xdata event " << e.type() );
            try
            {
                if ( e.type() == "urn:xdaq-event:setDefaultValues" ) //this is used as an initialization signal
                {
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": setDefaultValues " );

                    // set eventing input if needed
                    if (m_config_use_external_tcds_info) {
                        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": using external TCDS info " );
                        b2in::nub::bind(this, &bril::histogramsourcegeneric::Application::onMessage);
                        for (size_t element = 0; element < m_config_eventing_input.elements(); element++) {
                            std::string t_bus = m_config_eventing_input[element].getProperty ("bus");
                            std::string t_topics = m_config_eventing_input[element].getProperty ("topics");
                            auto t_topicNames = toolbox::parseTokenSet (t_topics, ",");

                            for(auto t_topic : t_topicNames) {
                                try {
                                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": subscribing to the topic " << t_topic );
                                    this->getEventingBus(t_bus).subscribe(t_topic);
                                } catch (eventing::api::exception::Exception& e) {
                                    LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": failed to subscribe to the topic " << t_topic );
                                    notifyQualified("error", e);
                                }
                            }
                        }
                    }

                    // set eventing output
                    for (size_t element = 0; element < m_config_eventing_output.elements(); element++) {
                        // process topic
                        std::string t_topic = m_config_eventing_output[element].getProperty ("topic");
                        m_topicoutqueues.insert(std::make_pair(t_topic, new toolbox::squeue<toolbox::mem::Reference*>));

                        // buses
                        std::string t_buses = m_config_eventing_output[element].getProperty ("buses");
                        std::set<std::string> t_set = toolbox::parseTokenSet (t_buses, ",");
                        m_topic_bus_map[t_topic] = t_set;

                        // now add listeners for the buses
                        for(auto t_bus : t_set) {
                            // check not already set
                            if (std::find(m_unready_buses.begin(), m_unready_buses.end(), t_bus) == m_unready_buses.end()) {
                                try {
                                    this->getEventingBus(t_bus).addActionListener(this);
                                    m_unready_buses.insert(t_bus);
                                } catch (eventing::api::exception::Exception& e) {
                                    LOG4CPLUS_FATAL( getApplicationLogger(), __func__ << ": Failed to set topic " << t_topic << ", bus " << t_bus );
                                    XCEPT_RAISE(bril::histogramsourcegeneric::exception::applicationException, "");
                                }

                            }
                        }
                        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": created topic " << t_topic );
                    }

                    // set readout interface
                    xdata::Vector<xdata::Properties> config;
                    if (m_config_interface_type == "tcp") {
                        config = m_config_tcp_boards;
                        for (size_t element = 0; element < m_config_tcp_boards.elements(); element++) {
                            // back-end instance
                            std::string t_server = m_config_tcp_boards[element].getProperty ("server");
                            std::string t_port = m_config_tcp_boards[element].getProperty ("port");
                            m_backend_interface.push_back(new SimpleTcpInterface(t_server.c_str(), t_port.c_str(), m_config_backend_poll_interval));
                        }
                    } else if (m_config_interface_type == "ipbus") {
                        config = m_config_ipbus_boards;
                        // log level
                        uhal::setLogLevelTo(uhal::Warning());
                        // create connection manager
                        std::string t_connection_file = m_config_ipbus_general.getProperty ("connection_file");
                        uhal::ConnectionManager* ipbus_connection_manager = new uhal::ConnectionManager(t_connection_file);
                        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": created cm " << t_connection_file.c_str() );
                        // create boards
                        for (size_t element = 0; element < m_config_ipbus_boards.elements(); element++) {
                            // back-end instance
                            std::string t_device = m_config_ipbus_boards[element].getProperty ("device");
                            m_backend_interface.push_back(new IpbusInterface(ipbus_connection_manager->getDevice(t_device.c_str()),m_config_backend_poll_interval));
                        }
                    }
                    else {
                        LOG4CPLUS_FATAL( getApplicationLogger(), __func__ << ": Wrong interface type. Available: tcp, ipbus " );
                        XCEPT_RAISE(bril::histogramsourcegeneric::exception::applicationException, "");
                    }

                    // channel map
                    for (size_t element = 0; element < config.elements(); element++) {
                        std::set<std::string> t_channel_prefix_set = toolbox::parseTokenSet (config[element].getProperty ("board-channel-prefixes"), ",");
                        std::set<std::string> t_channel_set = toolbox::parseTokenSet (config[element].getProperty ("channel-ids"), ",");
                        std::set<std::string> t_algo_set = toolbox::parseTokenSet (config[element].getProperty ("algo-ids"), ",");
                        std::set<std::string>::iterator it_ch_pre;
                        std::set<std::string>::iterator it_ch;
                        std::set<std::string>::iterator it_algo;
                        for(it_ch_pre = t_channel_prefix_set.begin(), it_ch = t_channel_set.begin(), it_algo = t_algo_set.begin() ; it_ch_pre != t_channel_prefix_set.end() && it_ch != t_channel_set.end() && it_algo != t_algo_set.end() ; it_ch_pre++, it_ch++, it_algo++) {
                            auto pair = std::make_pair(std::stoi(*it_ch), std::stoi(*it_algo));
                            // channel prefix
                            m_backend_board_channel_prefix_map[element].push_back (*it_ch_pre);
                            // board map
                            m_backend_board_channel_algo_map[element].push_back (pair);
                            // add to vec
                            m_backend_channel_algo_vec.push_back (pair);
                            // init counter
                            m_readout_status[pair] = readout_status_init;
                            // init tcds info
                            m_tcds_external_queue.insert(std::make_pair(pair, new toolbox::squeue<tcds_external_t>));
                        }
                    }

                    // workloops
                    toolbox::net::URN m_readhist_urn( "readhist", getApplicationDescriptor()->getURN() );
                    m_readhist_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( m_readhist_urn.toString(), "polling" );

                    toolbox::net::URN m_publishing_urn( "publishing", getApplicationDescriptor()->getURN() );
                    m_publishing_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( m_publishing_urn.toString(), "waiting" );

                    //
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": setDefaultValues finished " );
                }
                else if ( e.type() == "ItemChangedEvent" )
                {
                    std::string item = static_cast<xdata::ItemEvent&>( e ).itemName();
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Parameter " << item << " changed." );

                }
            }
            catch ( xcept::Exception& e )
            {
                notifyQualified( "error", e );
            }

        }

        void Application::actionPerformed( toolbox::Event& e ) {
            LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received toolbox event " << e.type() );
            try
            {
                if ( e.type() == "eventing::api::BusReadyToPublish" )
                {
                    // get busname
                    std::string busname = static_cast<eventing::api::Bus*>( e.originator() )->getBusName();
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Bus \"" << busname << "\" is ready." );
                    // remove bus from unready
                    m_unready_buses.erase(busname);

                    // if still some unready - return
                    if (!m_unready_buses.empty()) return;
                    else LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": All buses are ready." );

                    // subsystem specific configure
                    this->ConfigureHardware();

                    // if not external tcds info - start workloops
                    if(!m_config_use_external_tcds_info) resetAndStartWorkLoops();

                }
                if ( e.type() == "Shutdown" )
                {
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Shutting down" );
                    m_shutdown_request = true;
                    usleep( 500000 );
                }
            }
            catch ( xcept::Exception& e )
            {
                notifyQualified( "error", e );
            }

        }

        void Application::resetHistograms() {
            try {
                LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Resetting histogram" );
                // first put all histograms in reset state
                for (uint16_t board_id = 0; board_id < m_backend_interface.size(); board_id++) {
                    for (uint16_t board_channel_id = 0; board_channel_id < m_backend_board_channel_algo_map[board_id].size(); board_channel_id++) {
                        m_backend_interface[board_id]->resetHistogram(m_backend_board_channel_prefix_map[board_id].at(board_channel_id), 0x1);
                    }
                }
                // sleep
                usleep( m_config_backend_poll_interval );
                // now release all histogram resets
                for (uint16_t board_id = 0; board_id < m_backend_interface.size(); board_id++) {
                    for (uint16_t board_channel_id = 0; board_channel_id < m_backend_board_channel_algo_map[board_id].size(); board_channel_id++) {
                        m_backend_interface[board_id]->resetHistogram(m_backend_board_channel_prefix_map[board_id].at(board_channel_id), 0x0);
                    }
                }
            } catch (std::runtime_error &e) {
                LOG4CPLUS_FATAL( getApplicationLogger(), __func__ << ": Failed to reset: " << e.what() );
                XCEPT_RAISE(bril::histogramsourcegeneric::exception::readoutException, e.what());
            }
        }

        void Application::resetAndStartWorkLoops() {
            // if reset is enabled
            if (m_do_histogram_reset) resetHistograms();

            // activate read hist workloop
            toolbox::task::ActionSignature* readhist_as = toolbox::task::bind( this, &bril::histogramsourcegeneric::Application::readHistogram, "readhist" );
            m_readhist_wl->activate();
            m_readhist_wl->submit( readhist_as );
            // axctivate publishing workloop
            toolbox::task::ActionSignature* publishing_as = toolbox::task::bind( this, &bril::histogramsourcegeneric::Application::publishing, "publishing" );
            m_publishing_wl->activate();
            m_publishing_wl->submit( publishing_as );
        }

        void Application::resynchroniseHistograms() {
            LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Resynchronising histograms by performing a reset" );

            // reset all histograms
            resetHistograms();

            // reset nstable histograms
            for (auto ch_algo : m_backend_channel_algo_vec) m_readout_status[ch_algo].nstablehistograms = 0;
        }

        bool Application::readHistogram(toolbox::task::WorkLoop* wl) {
            try {
                // read from the backend
                for (uint16_t board_id = 0; board_id < m_backend_interface.size(); board_id++) {
                    for (uint16_t board_channel_id = 0; board_channel_id < m_backend_board_channel_algo_map[board_id].size(); board_channel_id++) {
                        std::vector<uint32_t> histogram;
                        try {
                            histogram = m_backend_interface[board_id]->readHistogram(m_backend_board_channel_prefix_map[board_id].at(board_channel_id));
                        } catch (std::runtime_error &e) {
                            LOG4CPLUS_FATAL( getApplicationLogger(), __func__ << ": Failed to read histogram: " << e.what() );
                            XCEPT_RAISE(bril::histogramsourcegeneric::exception::readoutException, e.what());
                        }
                        auto ch_algo = m_backend_board_channel_algo_map[board_id].at(board_channel_id);
                        if (histogram.size() > 0) {
                            toolbox::mem::Reference* hist_ref = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memory_pool, this->getHistogramMaxsize());
                            hist_ref->setDataSize(this->getHistogramMaxsize());
                            interface::bril::shared::DatumHead* hist_header = (interface::bril::shared::DatumHead*)(hist_ref->getDataLocation());
                            hist_header->setTotalsize(this->getHistogramMaxsize());
                            // we need to reset everything if fail has been detected
                            if (!processHistogram(ch_algo, histogram, hist_header)) {
                                resynchroniseHistograms();
                                return true;
                            }
                            //push to output
                            try {
                                LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": publishing topicname " << this->getHistogramTopicname() );
                                m_topicoutqueues[getHistogramTopicname()]->push(hist_ref);
                            } catch ( ... ) {
                                LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Failed pushing to the queue" );
                                hist_ref->release();
                                //notifyQualified("error", e);
                            }
                            // update pull counter
                            m_readout_status[ch_algo].npulls_static = m_readout_status[ch_algo].npulls_current;
                            m_readout_status[ch_algo].npulls_current = 0;
                        } else {
                            // only update pull counter
                            m_readout_status[ch_algo].npulls_current = m_readout_status[ch_algo].npulls_current + 1;
                        }
                    }
                }
            }
            catch ( xcept::Exception& e )
            {
                notifyQualified( "error", e );
            }

            usleep(m_config_backend_poll_interval);

            // done
            return !m_shutdown_request;
        }

        bool Application::processHistogram(std::pair<uint16_t, uint16_t> ch_algo, std::vector<uint32_t> histogram, interface::bril::shared::DatumHead* histogram_header) {
            LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << "New histogram, number of words: " << histogram.size() << ", header " << std::hex << histogram.at(0) << std::dec );
            bool success = false;
            try {
                auto histogram_obj = std::make_unique<BrilHistogram>(histogram);
                auto new_fill = histogram_obj->GetLhcFill();
                auto new_run = histogram_obj->GetCmsRun();
                auto new_section = histogram_obj->GetLumiSection();
                auto new_nibble = histogram_obj->GetLumiNibble();
                auto temp_readout_status = m_readout_status[ch_algo];
                // using external tcds from brildata
                if (m_config_use_external_tcds_info) {
                    // extract external
                    tcds_external_t tcds_external_temp = tcds_external_init;
                    if (temp_readout_status.nstablehistograms == 0) while(!m_tcds_external_queue[ch_algo]->empty()) tcds_external_temp = m_tcds_external_queue[ch_algo]->pop(); // first histogram should contain relevant tcds info, so clear the queue
                    else tcds_external_temp = m_tcds_external_queue[ch_algo]->pop();
                    // now check
                    if (temp_readout_status.nstablehistograms > 0) {
                        bool is_nibble_reset_internal = (new_section == 1 && new_nibble == m_config_frequency_nb);
                        uint32_t nibble_step_internal = (new_section*64 + new_nibble) - (temp_readout_status.current_section_internal*64 + temp_readout_status.current_nibble_internal);
                        bool is_correct_increment_internal = (nibble_step_internal == m_config_frequency_nb);
                        if ( !is_nibble_reset_internal && !is_correct_increment_internal) {
                            if (nibble_step_internal == 0) {
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " INTERNAL Detected duplicate histogram" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nduplicate++;
                            } else if ( nibble_step_internal > m_config_frequency_nb ) {
                                uint32_t nskipped = (nibble_step_internal-m_config_frequency_nb)/m_config_frequency_nb;
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " INTERNAL Detected " << nskipped << " skipped histogram(s)" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nskipped += nskipped;
                            } else {
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " INTERNAL Detected unidentified internal TCDS info mismatch" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nunknown++;
                            }
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tParameters in format (prev:current): ");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tINTERNAL Lumi Section (" << temp_readout_status.current_section_internal << ":" << new_section << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tINTERNAL Lumi Nibble (" << temp_readout_status.current_nibble_internal << ":" << new_nibble << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Fill (" << temp_readout_status.current_fill << ":" << tcds_external_temp.current_fill << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Run (" << temp_readout_status.current_run << ":" << tcds_external_temp.current_run << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Lumi Section (" << temp_readout_status.current_section << ":" << tcds_external_temp.current_section << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Lumi Nibble (" << temp_readout_status.current_nibble << ":" << tcds_external_temp.current_nibble << ")");
                            // resynchrnonise
                            m_readout_status[ch_algo] = temp_readout_status;
                            return false;
                        } else {
                            bool is_new_run_external = !(temp_readout_status.current_fill == tcds_external_temp.current_fill && temp_readout_status.current_run == tcds_external_temp.current_run) && tcds_external_temp.current_section == 1 && tcds_external_temp.current_nibble == m_config_frequency_nb;
                            if ( is_new_run_external ) {
                                LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " EXTERNAL Detected new run " << tcds_external_temp.current_run << " - ch" << ch_algo.first << "a" << ch_algo.second);
                            } else {
                                uint32_t nibble_step_external = (tcds_external_temp.current_section*64 + tcds_external_temp.current_nibble) - (temp_readout_status.current_section*64 + temp_readout_status.current_nibble);
                                bool is_correct_increment_external = (nibble_step_external == m_config_frequency_nb);
                                if ( !is_correct_increment_external ) {
                                    if (nibble_step_external == 0) {
                                        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " EXTERNAL Detected duplicate histogram" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                        temp_readout_status.nduplicate++;
                                    } else if ( nibble_step_external > m_config_frequency_nb ) {
                                        uint32_t nskipped = (nibble_step_external-m_config_frequency_nb)/m_config_frequency_nb;
                                        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " EXTERNAL Detected " << nskipped << " skipped histogram(s)" << " - ch" << ch_algo.first << "a" << ch_algo.second << " (missing NB publications on brildata - e.g. due to uTCA reconfiguration)");
                                        temp_readout_status.nskipped += nskipped;
                                    } else {
                                        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " EXTERNAL Detected unidentified internal TCDS info mismatch" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                        temp_readout_status.nunknown++;

                                    }
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tParameters in format (prev:current): ");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tINTERNAL Lumi Section (" << temp_readout_status.current_section_internal << ":" << new_section << ")");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tINTERNAL Lumi Nibble (" << temp_readout_status.current_nibble_internal << ":" << new_nibble << ")");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Fill (" << temp_readout_status.current_fill << ":" << tcds_external_temp.current_fill << ")");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Run (" << temp_readout_status.current_run << ":" << tcds_external_temp.current_run << ")");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Lumi Section (" << temp_readout_status.current_section << ":" << tcds_external_temp.current_section << ")");
                                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tEXTERNAL Lumi Nibble (" << temp_readout_status.current_nibble << ":" << tcds_external_temp.current_nibble << ")");
                                    // resynchronise
                                    m_readout_status[ch_algo] = temp_readout_status;
                                    return false;
                                }
                            }
                        }
                    }
                    temp_readout_status.current_fill = tcds_external_temp.current_fill;
                    temp_readout_status.current_run = tcds_external_temp.current_run;
                    temp_readout_status.current_section = tcds_external_temp.current_section;
                    temp_readout_status.current_nibble = tcds_external_temp.current_nibble;
                    temp_readout_status.current_section_internal = new_section;
                    temp_readout_status.current_nibble_internal = new_nibble;
                    // important overwrite the tcds info
                    histogram_obj->UpdateTCDSInfo(temp_readout_status.current_fill, temp_readout_status.current_run, temp_readout_status.current_section, temp_readout_status.current_nibble);
                    histogram = histogram_obj->GetDataRaw();

                // using internal tcds from the bril histogram header
                } else {
                    if (temp_readout_status.nstablehistograms > 0 && temp_readout_status.current_fill == new_fill && temp_readout_status.current_run == new_run) {
                        if (temp_readout_status.current_section*64 + temp_readout_status.current_nibble + m_config_frequency_nb != new_section*64 + new_nibble) {
                            if ( (new_section*64 + new_nibble) == (temp_readout_status.current_section*64 + temp_readout_status.current_nibble) ) {
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " Detected duplicate histogram" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nduplicate++;
                            } else if ( (new_section*64 + new_nibble) - (temp_readout_status.current_section*64 + temp_readout_status.current_nibble) > m_config_frequency_nb ) {
                                uint32_t diff = (new_section*64 + new_nibble) - (temp_readout_status.current_section*64 + temp_readout_status.current_nibble) - m_config_frequency_nb;
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " Detected " << uint32_t(diff/m_config_frequency_nb) << " skipped histogram(s)" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nskipped += uint32_t(diff/m_config_frequency_nb);
                            } else {
                                LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " Detected unidentified internal TCDS info mismatch" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                                temp_readout_status.nunknown++;
                            }
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tParameters in format (prev:current): ");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tFill (" << temp_readout_status.current_fill << ":" << new_fill << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tRun (" << temp_readout_status.current_run << ":" << new_run << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tLumi Section (" << temp_readout_status.current_section << ":" << new_section << ")");
                            LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tLumi Nibble (" << temp_readout_status.current_nibble << ":" << new_nibble << ")");
                        }
                    }
                    temp_readout_status.current_fill = new_fill;
                    temp_readout_status.current_run = new_run;
                    temp_readout_status.current_section = new_section;
                    temp_readout_status.current_nibble = new_nibble;
                    temp_readout_status.current_section_internal = new_section;
                    temp_readout_status.current_nibble_internal = new_nibble;
                }

                // check residual in any case
                if (temp_readout_status.current_nibble_internal % m_config_frequency_nb != 0 ) {
                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " Residual from division by " << m_config_frequency_nb << " is not good" << " - ch" << ch_algo.first << "a" << ch_algo.second);
                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tLumi Section (" << temp_readout_status.current_section_internal << ")");
                    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << " \tLumi Nibble (" << temp_readout_status.current_nibble_internal << ")");
                }

                // rest
                temp_readout_status.nhistograms++;
                temp_readout_status.nstablehistograms++;
                m_readout_status[ch_algo] = temp_readout_status;

                // now update hist header
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                histogram_header->setTime(temp_readout_status.current_fill, temp_readout_status.current_run, temp_readout_status.current_section, temp_readout_status.current_nibble, t.sec(), t.millisec());
                histogram_header->setResource(99, ch_algo.second, ch_algo.first, interface::bril::shared::StorageType::COMPOUND); // TODO set valid resource source
                histogram_header->setFrequency(m_config_frequency_nb);

                // set data
                interface::bril::shared::CompoundDataStreamer streamer (this->getHistogramPayloadDict());
                //LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " Current dictionary : " << this->getHistogramPayloadDict());
                const uint32_t nbins = histogram_obj->GetNbins();
                const uint32_t counterwidth = histogram_obj->GetCounterWidth();
                const uint32_t incrementwidth = histogram_obj->GetIncrementWidth();
                const uint32_t nunits = histogram_obj->GetNunits();
                const uint32_t nmaskwords = histogram_obj->GetNwordsPerError();
                const uint32_t orbitcounter = histogram_obj->GetOrbitCounter();
                const bool counterovf = histogram_obj->GetCounterOverflow();
                const bool incrementovf = histogram_obj->GetIncrementOverflow();
                uint32_t nerrorwords = histogram_obj->GetNmaskErrorWords()-nmaskwords;
                // get counters
                auto bxraw_vec = histogram_obj->GetCounters();
                uint32_t bxraw[bxraw_vec.size()];
                std::copy(bxraw_vec.begin(), bxraw_vec.end(), bxraw);
                // noew fill data
                streamer.insert_field (histogram_header->payloadanchor, "nbins", &nbins);
                streamer.insert_field (histogram_header->payloadanchor, "counterwidth", &counterwidth);
                streamer.insert_field (histogram_header->payloadanchor, "incrementwidth", &incrementwidth);
                streamer.insert_field (histogram_header->payloadanchor, "nunits", &nunits);
                streamer.insert_field (histogram_header->payloadanchor, "nerrorwords", &nerrorwords);
                streamer.insert_field (histogram_header->payloadanchor, "orbitcounter", &orbitcounter);
                streamer.insert_field (histogram_header->payloadanchor, "counterovf", &counterovf);
                streamer.insert_field (histogram_header->payloadanchor, "incrementovf", &incrementovf);
                streamer.insert_field (histogram_header->payloadanchor, "bxraw", bxraw);
                // only if non zero
                uint16_t mask_vec[nunits];
                for(uint16_t b = 0; b < nunits; b++) mask_vec[b] = 0;
                if (nmaskwords > 0) {
                    for(auto masked_unit : histogram_obj->GetMaskedUnits()) mask_vec[masked_unit] = 1;
                }
                streamer.insert_field (histogram_header->payloadanchor, "mask", mask_vec);
                // here we drop some data - store only error counter per unit
                uint16_t err_cnt_vec[nunits];
                for(uint16_t b = 0; b < nunits; b++) err_cnt_vec[b] = 0;
                if (nerrorwords > 0) {
                    for(auto error : histogram_obj->GetErrors())
                        for(auto unit : error.GetUnitList()) err_cnt_vec[unit] += 1;
                }
                streamer.insert_field (histogram_header->payloadanchor, "errorcnt", err_cnt_vec);
                // copy
                //memcpy(histogram_header->payloadanchor, histogram.data(), histogram.size()*sizeof(uint32_t));
                success = true;
            } catch (...) {
                LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << " Exception caught while checking histogram");
                success = false;
            }
            return success;
        }

        bool Application::publishing(toolbox::task::WorkLoop* wl){
            //LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << " Publishing event" );
            QueueStoreIt it;
            for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
                if(!it->second->empty()) {
                    std::string topicname = it->first;
                    toolbox::mem::Reference* data = it->second->pop();
                    xdata::Properties plist;
                    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                    plist.setProperty("PAYLOAD_DICT", this->getHistogramPayloadDict());
                    // publish to all buses
                    for (auto busname : m_topic_bus_map[topicname]) {
                        LOG4CPLUS_DEBUG(getApplicationLogger(), __func__ << "Publishing " << busname << ":" << topicname);
                        toolbox::mem::Reference* hist_ref = data->duplicate();
                        try {
                            this->getEventingBus(busname).publish(topicname, hist_ref, plist);
                        } catch ( xcept::Exception& e ) {
                            LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << " Failed publishing topic " << topicname);
                            notifyQualified("error", e);
                        }
                    }
                    // release
                    data->release();
                    data = 0;
                }
            }
            usleep(m_config_push_interval);
            return (!m_shutdown_request || m_topicoutqueues.empty());
        }

        // transpose status
        std::map<std::string, std::vector<std::string>> Application::getReadoutStatus() {
            std::map<std::string, std::vector<std::string>> result;
            for (auto ch_algo : m_backend_channel_algo_vec) result["00 Number of accumulated histograms"].push_back(std::to_string(m_readout_status[ch_algo].nhistograms));
            for (auto ch_algo : m_backend_channel_algo_vec) result["01 Number of accumulated histograms since last reset"].push_back(std::to_string(m_readout_status[ch_algo].nstablehistograms));
            for (auto ch_algo : m_backend_channel_algo_vec) result["02 Number of skipped histograms"].push_back(std::to_string(m_readout_status[ch_algo].nskipped));
            for (auto ch_algo : m_backend_channel_algo_vec) result["03 Number of duplicate histograms"].push_back(std::to_string(m_readout_status[ch_algo].nduplicate));
            for (auto ch_algo : m_backend_channel_algo_vec) result["04 Number of histograms with unidentified TCDS mismatch"].push_back(std::to_string(m_readout_status[ch_algo].nunknown));
            for (auto ch_algo : m_backend_channel_algo_vec) result["05 Current fill"].push_back(std::to_string(m_readout_status[ch_algo].current_fill));
            for (auto ch_algo : m_backend_channel_algo_vec) result["06 Current run"].push_back(std::to_string(m_readout_status[ch_algo].current_run));
            for (auto ch_algo : m_backend_channel_algo_vec) result["07 Current section"].push_back(std::to_string(m_readout_status[ch_algo].current_section));
            for (auto ch_algo : m_backend_channel_algo_vec) result["08 Current nibble"].push_back(std::to_string(m_readout_status[ch_algo].current_nibble));
            for (auto ch_algo : m_backend_channel_algo_vec) result["09 Number of pulls per histogram"].push_back(std::to_string(m_readout_status[ch_algo].npulls_static));
            if (m_config_use_external_tcds_info) for (auto ch_algo : m_backend_channel_algo_vec) result["10 TCDS queue size"].push_back(std::to_string(m_tcds_external_queue[ch_algo]->elements()));
            return result;
        }

        // external tcds info
        void Application::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist ) {

            std::string action = plist.getProperty("urn:b2in-eventing:action");
            if ( action == "notify" && ref != 0 ) {
                toolbox::mem::AutoReference t_refGuard(ref);
                std::string topic = plist.getProperty("urn:b2in-eventing:topic");
                LOG4CPLUS_DEBUG(getApplicationLogger(), "Received topic " + topic);
                if ( (m_config_frequency_nb == uint32_t(4) && topic == "NB4") || (m_config_frequency_nb == uint32_t(1) && topic == "NB1") ) {
                    LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received required TCDS data ");
                    interface::bril::shared::DatumHead * inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                    tcds_external_t tcds_external_temp = { .current_fill = inheader->fillnum, .current_run = inheader->runnum, .current_section = inheader->lsnum, .current_nibble = inheader->nbnum };
                    if (tcds_external_temp.current_fill > 0 && tcds_external_temp.current_run > 0) {
                        if(!m_readhist_wl->isActive()) resetAndStartWorkLoops();
                        for (auto pair : m_tcds_external_queue) pair.second->push(tcds_external_temp);
                    }
                }
            }
        }

    }
}