#ifndef _histogramsourcegeneric_SimpleTcpInterface_h
#define _histogramsourcegeneric_SimpleTcpInterface_h

#include "bril/histogramsource/generic/ReadoutInterface.h"
#include <string>
#include <vector>
#include <zmq.hpp>

namespace bril{
    namespace histogramsourcegeneric {
        class SimpleTcpInterface : public ReadoutInterface {
            public:
                // base
                SimpleTcpInterface(std::string host, std::string port, uint32_t histogram_readout_interval_usec);
                ~SimpleTcpInterface();
                // read write methods
                virtual uint32_t read(std::string reg_name);
                virtual std::vector<uint32_t> readBlock(std::string reg_name, uint32_t size);
                virtual void write(std::string reg_name, uint32_t data);
                virtual void writeBlock(std::string reg_name, std::vector<uint32_t> data);
                // parameter list
                virtual  std::vector<std::pair<std::string, std::string>> getConfiguration();
                // local methods
                void global_reset();
                bool checkConnection();

            private:
                // connection
                std::string m_host;
                std::string m_port;
                std::string m_server_address;
                // constants
                zmq::socket_t *m_socket;
                // command definition
                const uint32_t IPBUS_COMMAND_GLOBAL_RESET = 0x81000000;
                const uint32_t IPBUS_COMMAND_READ = 0x82000000;
                const uint32_t IPBUS_COMMAND_READ_BLOCK = 0x83000000;
                const uint32_t IPBUS_COMMAND_WRITE = 0x84000000;
                const uint32_t IPBUS_COMMAND_WRITE_BLOCK = 0x85000000;
                const uint32_t IPBUS_COMMAND_DUMMY = 0x8F000000;
                const uint32_t IPBUS_STATUS_EXCEPTION = 0xF1000000;
                // help functions
                std::vector<char> uint32_to_bytearray(uint32_t value);
                // send tcp command used by the read write methods
                std::vector<uint32_t> send_command(uint32_t command, std::string reg_name, std::vector<uint32_t> data);
        };
    }
}

#endif