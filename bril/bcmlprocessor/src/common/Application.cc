#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "bril/bcmlprocessor/Application.h"
#include "bril/bcmlprocessor/DipErrorHandlers.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "b2in/nub/Method.h"
#include "interface/bril/BCMTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::bcmlprocessor::Application)

bril::bcmlprocessor::Application::Application(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&Application::Default, "Default");
  b2in::nub::bind(this, &bril::bcmlprocessor::Application::onMessage);
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  std::string memPoolName = "bcmlprocessor::Application_memPool";
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
  toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
  m_memPool = m_poolFactory->createPool(urn,allocator);

  m_bkgd1LminusRS1=m_bkgd1LplusRS1=m_bkgd2LminusRS1=m_bkgd2LplusRS1=0;
  m_bkgd1LminusRS12=m_bkgd1LplusRS12=m_bkgd2LminusRS12=m_bkgd2LplusRS12=0;
  m_RS9L1Minus=m_RS9L1Plus=m_RS9L2Minus=m_RS9L2Plus=0;
  m_dipDataReceived=0;
  
  for(int i=0; i<48 ;i++){
    m_PA1_NB4[i]=m_PA1_NB4_Filtered[i]=m_PA1_NB64[i]=m_PA1_NB64_Filtered[i]=-1.0;
    m_PA12_NB4[i]=m_PA12_NB64[i]=-1.0;
    m_RS9_NB4[i]=m_RS9_NB64[i]=-1;
  }
  m_lostFrames=false;
  m_thresholdsOK=true;
  m_timeLastDump=0;
  m_fillnum = 0;
  m_runnum = 0;
  m_lsnum = 0;
  m_sec = 0;
  m_msec = 0;
  m_lF = true;
  m_tOK = false;
  m_tLD = 0;
  m_acqTime.value_ = toolbox::TimeVal();
  for(size_t i=0;i<48;++i){
    m_PA1.push_back(-1);
    m_PA1_Filt.push_back(-1);
    m_PA12.push_back(-1);
    m_RS9.push_back(-1);
  }
  m_busName = "brildata";
  m_dipRoot = "dip/CMS/";
  m_signalTopic = "NB4";//no need to configure, we always listen to NB4
  m_dipServiceName = "bcmlprocessor";
  try{
    getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
    getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
    getApplicationInfoSpace()->fireItemAvailable("busName",&m_busName);
    getApplicationInfoSpace()->fireItemAvailable("dipServiceName",&m_dipServiceName);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    std::string nid("bcmlMon");       
    std::string monurn = createQualifiedInfoSpace(nid).toString();   
    // register all monitorables to mon infospace
    m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));   
    m_monInfoSpace->fireItemAvailable("timestamp",&m_acqTime);
    m_monItemList.push_back("timestamp");
    m_monInfoSpace->fireItemAvailable("PercentAbort1",&m_PA1);
    m_monItemList.push_back("PercentAbort1");
    m_monInfoSpace->fireItemAvailable("PercentAbort12",&m_PA12);
    m_monItemList.push_back("PercentAbort12");
    m_monInfoSpace->fireItemAvailable("RunningSum9",&m_RS9);
    m_monItemList.push_back("RunningSum9");
    m_monInfoSpace->fireItemAvailable("TimeLastDump",&m_tLD);
    m_monItemList.push_back("TimeLastDump");
  } catch (xdata::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to set up infospace";
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
  }  
}

void bril::bcmlprocessor::Application::Default(xgi::Input * in, xgi::Output * out) {}

void bril::bcmlprocessor::Application::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    this->getEventingBus(m_busName.value_).addActionListener(this);        
    if( !m_dipRoot.value_.empty() ){
      char lastChar = *(m_dipRoot.value_.rbegin());
      if( lastChar!='/' ){
	m_dipRoot.value_ += '/';
      }
    }
    m_dip = Dip::create(m_dipServiceName.value_.c_str());
    m_servererrorhandler=new bril::bcmlprocessor::ServerErrorHandler;
    Dip::addDipDimErrorHandler(m_servererrorhandler);
    m_puberrorhandler = new PubErrorHandler;
  }
}

void bril::bcmlprocessor::Application::actionPerformed(toolbox::Event& e){  
  if( e.type() == "eventing::api::BusReadyToPublish" ){
    try{
      std::cout<<"subscribe to "<<m_signalTopic.value_<<" on "<<m_busName.value_<<std::endl;
      this->getEventingBus(m_busName.value_).subscribe(m_signalTopic.value_);
    }catch(eventing::api::exception::Exception& e){
      std::string msg("Failed to subscribe to eventing bus brildata");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bcmlprocessor::exception::Exception,msg,e);     
    }

    m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition",(DipSubscription*)0 ));
    m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/BLM/BLMBCM2/PostMortemData",(DipSubscription*)0 ));

    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/Acquisition",(DipPublication*)0 ));
    //m_dippubs.insert(std::make_pair(m_dipRoot+"BRIL/BCMAnalysis/RawData",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/Summary",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card2",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card3",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card4",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/Background3",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/HighChartsNB4",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/BCMAnalysis/MonitorRS9",(DipPublication*)0 ));

    createDipPublication();
    subscribeToDip();
  }  
}


void bril::bcmlprocessor::Application::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getApplicationLogger(),"Application::handleMessage from "+subname);
  if(message.size()==0) return;
  //if(subname=="dip/acc/LHC/Beam/BLM/BLMBCM2/PostMortemData"){
    //int sizesbp;
    //const DipShort * sbp = message.extractShortArray(sizesbp,"pmCableConnectionTable");
    //std::cout << "pmCableConnectionTable returns: " << sizesbp << " values" <<std::endl; 
  //}
  if(subname=="dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition"){

    // LOAD DATA FROM DIP............
    m_BeamPermit = message.extractBool("blecsUDL");

    int sizeacqTimeStamps;
    const DipLong * acqTimeStamps = message.extractLongArray(sizeacqTimeStamps,"acqTimeStamps");
    const DipLong  acqStamp = message.extractLong("acqStamp");

    int sizeacq = 0;
    const DipLong * acq = message.extractLongArray(sizeacq,"acq");
    //std::cout << "acq returns: " << sizeacq << " values" <<std::endl;

    int sizeThresholds;
    const DipLong * thresholds = message.extractLongArray(sizeThresholds,"thresholds");
    //std::cout << "thresholds returns: " << sizeThresholds << " values" <<std::endl;

    int sizeBIS;
    const DipBool * channelBISConnected = message.extractBoolArray(sizeBIS,"channelBISConnected");
    //std::cout << "channelBISConnected returns: " << sizeBIS << " values" <<std::endl;

    int sizeMasked;
    const DipBool * channelMasked = message.extractBoolArray(sizeMasked,"channelMasked");
    //std::cout << "channelMasked returns: " << sizeMasked << " values" <<std::endl;

    int sizeCable;
    const DipBool * channelCableConnected = message.extractBoolArray(sizeCable,"channelCableConnected");
    //std::cout << "channelCableConnected returns: " << sizeCable << " values" <<std::endl;

    const DipBool blecsDump = message.extractBool("blecsDUMP");

    int sizelf;
    const int * lfa18 = message.extractIntArray(sizelf,"statusLostFramesADetectors1to8");
    const int * lfa916 = message.extractIntArray(sizelf,"statusLostFramesADetectors9to16");
    const int * lfb18 = message.extractIntArray(sizelf,"statusLostFramesBDetectors1to8");
    const int * lfb916 = message.extractIntArray(sizelf,"statusLostFramesBDetectors9to16");
    const int * sNoDumpDet18 = message.extractIntArray(sizelf,"statusNoDumpsDetectors1to8");
    const int * sNoDumpDet916 = message.extractIntArray(sizelf,"statusNoDumpsDetectors9to16");



    // Load data to global variables

    //m_acqTime = acqStamp/1e9;  //This is the time when the data was actually taken
    m_acqTime = toolbox::TimeVal(acqStamp/1e9,0);

    for (int i=0;i<48;i++){
      m_inAbort[i]=(channelBISConnected[i+16]&&channelCableConnected[i+16])&!channelMasked[i+16];
    }
    
    for(int i=0;i<576;i++){
      m_acq[i] = acq[i+192];
    }
    for (int i=0;i<3;i++){
      m_acqtsarray[i]=(unsigned int)(acqTimeStamps[i+1]/1e9);
      //std::cout<<"acq timestamps "<<acqTimeStamps[i+1]<<"  "<<m_acqtsarray[i]<<std::endl;
    }

    for(int i=1;i<4;i++){
      m_LostFrames[i-1]=lfa18[i];
      m_LostFrames[i+3-1]=lfa916[i];
      m_LostFrames[i+6-1]=lfb18[i];
      m_LostFrames[i+9-1]=lfb916[i];
    }

    for(int i=1;i<4;i++){
      m_nDump[i-1]=sNoDumpDet18[i];
      m_nDump[i+3-1]=sNoDumpDet916[i];
    } 




    m_BlecsDump=blecsDump;
    if(blecsDump){
      m_timeLastDump=acqStamp/1e9;
      m_tLD=m_timeLastDump;
    }




   //OLD STUFF......

    //Format DATA.......................
    int64_t acqCard1[192], acqCard2[192],acqCard3[192];
    bool useDiamond[40];
    for(int i=0;i<40;i++){
      useDiamond[i]=false;
    }

    //std::cout << "active acq card data: "<<std::endl;
    // acq array is 16 cards time 192 values
    // however we only use cards numbers 1,2,3 (starting at 0)
    // there are 12 running sums per channel, there are 16 channels per card   
    int card,channel,rsum;
    int index=0;
    int acqIndex=0;
    if(m_dipDataReceived==0){
      for (int i=0;i< MAX_CHANNELS;i++){
        for(int j=0;j<NUMBER_RSUMS;j++){
          m_card2[i][j]=0;
          m_card3[i][j]=0;
          m_card4[i][j]=0;
        }
      }
    }

    m_dipDataReceived++;

    for (card=0; card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        acqIndex=0;
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          //std::cout<<"card "<<card<<","<<" channel "<<channel<<", rsums :";
          for(rsum=0;rsum<NUMBER_RSUMS; rsum++)
          {
            m_acqArray[card][channel][rsum] = static_cast<int64_t>(acq[index]);
            if(card==1){
              acqCard1[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card2[channel][rsum] = std::max(m_card2[channel][rsum], static_cast<int64_t>(acq[index]) );
            } else if (card==2){
              acqCard2[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card3[channel][rsum] = std::max(m_card3[channel][rsum], static_cast<int64_t>(acq[index]) );
            } else if (card==3){
              acqCard3[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card4[channel][rsum]=std::max(m_card4[channel][rsum], static_cast<int64_t>(acq[index]) );
            }
            //std::cout<<m_acqArray[card][channel][rsum]<<";  ";
            index++;
          }
          //std::cout<<std::endl;
        }
      } else {
        index+=MAX_CHANNELS*NUMBER_RSUMS;
      }
    }


    // Now data in: m_acq, m_acqArray (maxalgo), acqCard1/2/3 (local), m_card1/2/3 (maxalgo)



    index=0;
    for (card=0; card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          for(rsum=0;rsum<NUMBER_RSUMS; rsum++)
          {
            m_thresholdArray[card][channel][rsum]=static_cast<int64_t>(thresholds[index]);
            index++;
          }
        }
      } else {
        index+=MAX_CHANNELS*NUMBER_RSUMS;
      }
    }





    //std::cout<<"last dump time is "<<m_timeLastDump<<std::endl;
    if(sizeBIS+sizeMasked+sizeCable == 48*MAX_CHANNELS)
    {
      index=0;
      int i_diamond=0;
      int ntoUse=0;
      for (card=0;card<MAX_CARDS;card++)
      {
        if(acqTimeStamps[card]>0)
        {
          for(channel=0;channel<MAX_CHANNELS;channel++)
          {
            m_abortChannelsArray[card][channel]=(channelBISConnected[index]&&channelCableConnected[index])&!channelMasked[index];
            useDiamond[i_diamond]=m_abortChannelsArray[card][channel];
            ntoUse+=int(m_abortChannelsArray[card][channel]);
            index++;
            i_diamond++;
          }
        } else {
          index=index+MAX_CHANNELS;
        }
      }
      //std::cout<<" there are "<<ntoUse<< "channels in the abort "<<std::endl;
    }else {
      LOG4CPLUS_ERROR(getApplicationLogger(),"inconsistent Connected Channel Data");
    }
    m_liveBCM1LChannels=m_liveBCM2LChannels=0;
    int i_diamond=0;
    for (card=0;card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          float thisFracAbortRS1=
          (100.0*m_acqArray[card][channel][0])/m_thresholdArray[card][channel][0];
          float thisFracAbortRS12=
          (100.0*m_acqArray[card][channel][11])/m_thresholdArray[card][channel][11];
          thisFracAbortRS1=thisFracAbortRS1*m_abortChannelsArray[card][channel];
          thisFracAbortRS12=thisFracAbortRS12*m_abortChannelsArray[card][channel];
          if(useDiamond[i_diamond]){
            m_PA1_NB4[i_diamond]=std::max(m_PA1_NB4[i_diamond],thisFracAbortRS1);
            m_PA1_NB64[i_diamond]=std::max(m_PA1_NB64[i_diamond],thisFracAbortRS1);
            // next line needs to be replaced by filtering code
            m_PA1_NB4_Filtered[i_diamond]=std::max(m_PA1_NB4_Filtered[i_diamond],thisFracAbortRS1);
            m_PA12_NB4[i_diamond]=std::max(m_PA12_NB4[i_diamond],thisFracAbortRS12);
            m_PA12_NB64[i_diamond]=std::max(m_PA12_NB64[i_diamond],thisFracAbortRS12);


            m_PA1[i_diamond]=m_PA1_NB4[i_diamond];
            m_PA12[i_diamond]=m_PA12_NB4[i_diamond];
            m_PA1_Filt[i_diamond]=m_PA1_NB4_Filtered[i_diamond];
          }
          m_RS9_NB4[i_diamond]=std::max(m_RS9_NB4[i_diamond],m_acqArray[card][channel][9]);
      	  m_RS9_NB64[i_diamond]=std::max(m_RS9_NB64[i_diamond],m_acqArray[card][channel][9]);
      	  m_RS9[i_diamond] = float( m_RS9_NB4[i_diamond] );
          //m_RS9[i_diamond]=m_PA12_NB4[i_diamond];

          if(card==1){
            if(channel<4){
              m_bkgd1LminusRS1=std::max(m_bkgd1LminusRS1,thisFracAbortRS1);
              m_bkgd1LminusRS12=std::max(m_bkgd1LminusRS12,thisFracAbortRS12);
              if(useDiamond[i_diamond]){
                m_RS9L1Minus=std::max(m_RS9L1Minus,m_acqArray[card][channel][9]);
              }
            } else if( channel<8){
              m_bkgd1LplusRS1=std::max(m_bkgd1LplusRS1,thisFracAbortRS1);
              m_bkgd1LplusRS12=std::max(m_bkgd1LplusRS12,thisFracAbortRS12);
              if(useDiamond[i_diamond]){
                m_RS9L1Plus=std::max(m_RS9L1Plus,m_acqArray[card][channel][9]);
              }              
            }
          }
          if(card==1&&m_abortChannelsArray[card][channel]&&m_acqArray[card][channel][11]>0){
            m_liveBCM1LChannels++;
          }
          if(card>1&&m_abortChannelsArray[card][channel]&&m_acqArray[card][channel][11]>0){
            m_liveBCM2LChannels++;
          }
          if(card==2){
            m_bkgd2LminusRS1=std::max(m_bkgd2LminusRS1,thisFracAbortRS1);
            m_bkgd2LminusRS12=std::max(m_bkgd2LminusRS12,thisFracAbortRS12);
            if(useDiamond[i_diamond]){
              m_RS9L2Minus=std::max(m_RS9L2Minus,m_acqArray[card][channel][9]);
            }
          }
          if(card==3){
            m_bkgd2LplusRS1=std::max(m_bkgd2LplusRS1,thisFracAbortRS1);
            m_bkgd2LplusRS12=std::max(m_bkgd2LplusRS12,thisFracAbortRS12); 
            if(useDiamond[i_diamond]){
              m_RS9L2Plus=std::max(m_RS9L2Plus,m_acqArray[card][channel][9]);
            }           
          }
          i_diamond++;
        }
      }
      
    }
 
  // Check if RS1 and RS12 abort thresholds are correct
  m_liveRS1Thresholds=m_liveRS12Thresholds=0;
  for(card=0;card<MAX_CARDS;card++) {
      for(channel=0;channel<MAX_CHANNELS;channel++) {
          // Threshold array for RS1
          if ( m_thresholdArray[card][channel][0] >= 100 && m_thresholdArray[card][channel][0] <= 4100 ) {
              m_liveRS1Thresholds++;
          }
          //                                             // Threshold array for RS12
          if ( m_thresholdArray[card][channel][11] > 0 && m_thresholdArray[card][channel][11] < 4294967295 ) {
              m_liveRS12Thresholds++;
          }
      }
  }
          //                                                                                     }
          //                                                                                       }


  float tmp=-1;
  for (int i=0;i<48;i++){
    if(m_PA1[i].value_>tmp) tmp=m_PA1[i].value_;
  }
  m_PA1_max=tmp;





  // HERE GOES THE PUBLISHING....


  // Send data to Flashlist
  try {
    m_monInfoSpace->fireItemGroupChanged(m_monItemList,this);
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to push monitoring items to bcmlMon");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg,e);
    notifyQualified("error",myerrorobj);
  }

  publish_to_eventing();

  publish_to_dip();

  // SOME DIP STUFF, MOVE TO DIP PUBLISHER, REORGANIZE VARIABLES
  DipTimestamp t;
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot.value_+"BRIL/BCMAnalysis/Acquisition");
  //std::cout<<"diptopicname "<<diptopicname<<std::endl;
  dipdata -> insert((int64_t*)acqCard1,192,"Card2");
  dipdata -> insert(acqCard2,192,"Card3");
  dipdata -> insert(acqCard3,192,"Card4");
  m_dippubs[diptopicname]->send(*dipdata,t);
  delete dipdata;
  /*
  DipData* dipdata2 = m_dip->createDipData();
  std::string diptopicname2(m_dipRoot+"BRIL/BCMAnalysis/RawData");
  dipdata2 -> insert(m_acq,576,"acq");
  dipdata2 -> insert(m_acqtsarray,3,"acqTimeStamps");
  dipdata2 -> insert(m_inAbort,48,"inAbort");
  dipdata2 -> insert(m_LostFrames,12,"LostFrames");
  dipdata2 -> insert(m_nDump,6,"NumberDumps"); 
  dipdata2 -> insert(m_BlecsDump,"blecsDump"); 
  m_dippubs[diptopicname2]->send(*dipdata,t);
  delete dipdata2; 
  */

  
  }
}
void bril::bcmlprocessor::Application::publish_to_eventing(){
  std::stringstream msg;  
  //fill message buffer
  std::string topicname=interface::bril::bcmT::topicname();
  toolbox::mem::Reference* bufRef = 0;
  try{

    msg<<"publishing "<<topicname<< " at UTC timestamp "<<m_acqtsarray[0]<<" to "<<m_busName.value_;
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    msg.str("");
    // fill property
    std::string pdict = interface::bril::bcmT::payloaddict();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);
    size_t totalsize = interface::bril::bcmT::maxsize();
    // get message buffer
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    // fill message header
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_sec.value_,m_msec.value_);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(totalsize);
    // fill message payload
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field( header->payloadanchor, "acq", m_acq);
    tc.insert_field( header->payloadanchor, "acqts", m_acqtsarray);
    tc.insert_field( header->payloadanchor, "lostframes", m_LostFrames);
    tc.insert_field( header->payloadanchor, "ndumps", m_nDump);
    tc.insert_field( header->payloadanchor, "blecsdump", &m_BlecsDump);
    tc.insert_field( header->payloadanchor, "inabort", m_inAbort);
    tc.insert_field( header->payloadanchor, "pa1max", &(m_PA1_max.value_) );
    // publish to eventing
    /*msg.str("");
    msg<<m_fillnum.value_<<","<<m_runnum.value_<<","<<m_lsnum.value_<<","<<m_nbnum.value_<<","<<m_sec.value_<<"."<<m_msec.value_<<",";
    msg<<"[";
    for(int i=0;i<576;++i){
      msg<<m_acq[i];
      if(i!=575) msg<<" ";
    }
    msg<<"],[";
    for(int i=0;i<3;++i){
      msg<<m_acqtsarray[i];
      if(i!=2) msg<<" ";
    }
    msg<<"],";
    msg<<m_LostFrames<<",[";
    for(int i=0;i<6;++i){
      msg<<m_nDump[i];
      if(i!=5) msg<<" ";
    }
    msg<<"],";
    msg<<m_BlecsDump<<",";
    msg<<m_inAbort;
    //std::cout<<msg.str()<<std::endl;
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());*/
    this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);

  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<m_busName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){ bufRef->release(); bufRef = 0; }   
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg.str(),e);
    notifyQualified("fatal",myerrorobj);
  }
  m_dipDataReceived=0;
}


void bril::bcmlprocessor::Application::publish_to_dip(){
  int iRun,iSeg,iNbl;
  iRun=iSeg=iNbl=-1;
  iRun = (int)m_runnum.value_;
  iSeg = (int)m_lsnum.value_;
  iNbl = (int)m_nbnum.value_;
  
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot.value_+"BRIL/BCMAnalysis/Summary");
  int64_t card1rs1Values[8];
  //int64_t card1rs9Values[8];
  
  for (int j=0;j<8;j++){
    card1rs1Values[j]=m_acqArray[1][j][0];
    //card1rs9Values[j]=m_acqArray[1][j][8];
  }
  int64_t card2rs1Values[16];
  //int64_t card2rs9Values[16];
  for (int j=0;j<16;j++){
    card2rs1Values[j]=m_acqArray[2][j][0];
  //  card2rs9Values[j]=m_acqArray[2][j][8];
  }
  int64_t card3rs1Values[16];
  //int64_t card3rs9Values[16];
  for (int j=0;j<16;j++){
    card3rs1Values[j]=m_acqArray[3][j][0];
    //card3rs9Values[j]=m_acqArray[3][j][8];
  }
  DipTimestamp t;
  int64_t mus =t.getAsNanos()/1000;
  dipdata -> insert(mus/1000,"timestamp");
  dipdata -> insert(iRun,"TCDS_RunNumber");
  dipdata -> insert(iSeg,"TCDS_LSegmentNumber");
  dipdata -> insert(iNbl,"TCDS_LNibbleNumber");
  dipdata -> insert(m_BeamPermit,"BeamPermit");
  dipdata -> insert(m_liveBCM1LChannels,"LiveAbortBCM1LChannels");
  dipdata -> insert(m_liveBCM2LChannels,"LiveAbortBCM2LChannels");
  dipdata -> insert(m_liveRS1Thresholds, "CorrectBCMLThresholdsRS1");
  dipdata -> insert(m_liveRS12Thresholds, "CorrectBCMLThresholdsRS12");

  dipdata -> insert(std::max(m_bkgd1LminusRS1,m_bkgd1LplusRS1),"maxAbortPercentBCM1L");
  dipdata -> insert(std::max(m_bkgd2LminusRS1,m_bkgd2LplusRS1),"maxAbortPercentBCM2L");
  dipdata -> insert(card1rs1Values,8,"BCM1LRS1Values");
  dipdata -> insert(card2rs1Values,16,"BCM2LMinusRS1Values");
  dipdata -> insert(card3rs1Values,16,"BCM2LPlusRS1Values");
  dipdata -> insert(false,"BCMHVOn");
  dipdata -> insert("HV MONITORING NOT OPERATIONAL!","00Message");

  m_dippubs[diptopicname]->send(*dipdata,t);
  delete dipdata;
  DipData* dipdata1 = m_dip->createDipData();
  std::string diptopic1name(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card2");
  for (int i=0; i<8;i++){
    std::stringstream ss;
     if(i<9){
        ss<<"Channel0"<<i+1;
      } else {
        ss<<"Channel"<<i;        
      }
      std::string s=ss.str();
      //std::cout<<s.c_str()<<"  "<<m_acqArray[1][i][8]<<std::endl;
      dipdata1 -> insert(m_acqArray[1][i][8],s.c_str());
  }
  m_dippubs[diptopic1name]->send(*dipdata1,t);
  delete dipdata1;
  DipData* dipdata2 = m_dip->createDipData();
  std::string diptopic2name(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card3");
  for (int i=0; i<16;i++){
    std::stringstream ss;
      if(i<9){
        ss<<"Channel0"<<i+1;
      } else {
        ss<<"Channel"<<i+1;        
      }
      std::string s=ss.str();
      dipdata2 -> insert(m_acqArray[2][i][8],s.c_str());
  }
  m_dippubs[diptopic2name]->send(*dipdata2,t);
  delete dipdata2;
  DipData* dipdata3 = m_dip->createDipData();
  std::string diptopic3name(m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card4");
  for (int i=0; i<16;i++){
    std::stringstream ss;
     if(i<9){
        ss<<"Channel0"<<i+1;
      } else {
        ss<<"Channel"<<i+1;        
      }
      std::string s=ss.str();
      dipdata3 -> insert(m_acqArray[3][i][8],s.c_str());
  }
  m_dippubs[diptopic3name]->send(*dipdata3,t);
  delete dipdata3;
  DipData* dipdata4 = m_dip->createDipData();
  std::string diptopic4name(m_dipRoot.value_+"BRIL/BCMAnalysis/Background3");
  dipdata4 -> insert(m_bkgd1LminusRS1,"BCM1LMinusRS1");
  dipdata4 -> insert(m_bkgd1LplusRS1,"BCM1LPlusRS1");
  dipdata4 -> insert(m_bkgd2LminusRS1,"BCM2LMinusRS1");
  dipdata4 -> insert(m_bkgd2LplusRS1,"BCM2LPlusRS1");
  dipdata4 -> insert(std::max(m_bkgd1LminusRS1,m_bkgd2LminusRS1),"MinusRS1");
  dipdata4 -> insert(std::max(m_bkgd1LplusRS1,m_bkgd2LplusRS1),"PlusRS1");
  float m1=std::max(m_bkgd1LminusRS1,m_bkgd2LminusRS1);
  float m2=std::max(m_bkgd1LplusRS1,m_bkgd2LplusRS1);
  float bkgd3rs1=std::max(m1,m2);
  dipdata4 -> insert(bkgd3rs1,"BKGD3RS1");

  dipdata4 -> insert(m_bkgd1LminusRS12,"BCM1LMinusRS12");
  dipdata4 -> insert(m_bkgd1LplusRS12,"BCM1LPlusRS12");
  dipdata4 -> insert(m_bkgd2LminusRS12,"BCM2LMinusRS12");
  dipdata4 -> insert(m_bkgd2LplusRS12,"BCM2LPlusRS12");
  dipdata4 -> insert(std::max(m_bkgd1LminusRS12,m_bkgd2LminusRS12),"MinusRS12");
  dipdata4 -> insert(std::max(m_bkgd1LplusRS12,m_bkgd2LplusRS12),"PlusRS12");
  m1=std::max(m_bkgd1LminusRS12,m_bkgd2LminusRS12);
  m2=std::max(m_bkgd1LplusRS12,m_bkgd2LplusRS12);
  float bkgd3rs12=std::max(m1,m2);
  dipdata4 -> insert(bkgd3rs12,"BKGD3RS12");
  dipdata4 -> insert(std::max(bkgd3rs1,bkgd3rs12),"BKGD3");  

  m_dippubs[diptopic4name]->send(*dipdata4,t);
  delete dipdata4;

  DipData* dipdata5 = m_dip->createDipData();
  //std::cout << "in dipPublish m_PA1_NB4 " << m_PA1_NB4 << std::endl;

  std::string diptopic5name(m_dipRoot.value_+"BRIL/BCMAnalysis/HighChartsNB4");
  dipdata5 -> insert(m_PA1_NB4,48,"maxPercentAbortRS1");
  dipdata5 -> insert(m_PA1_NB4_Filtered,48,"maxPercentAbortRS1Filtered");
  dipdata5 -> insert(m_PA12_NB4,48,"maxPercentAbortRS12");
  dipdata5 -> insert(m_RS9_NB4,48,"maxRS9");
  dipdata5 -> insert(m_lostFrames,"lostFrames");
  dipdata5 -> insert(m_thresholdsOK,"thresholdsOK");
  dipdata5 -> insert(m_timeLastDump,"timeOflastDump");
  m_dippubs[diptopic5name]->send(*dipdata5,t);
  delete dipdata5;

  DipData* dipdata6 = m_dip->createDipData();
  std::string diptopic6name(m_dipRoot.value_+"BRIL/BCMAnalysis/MonitorRS9");
  float RS9L1Minus= (float)m_RS9L1Minus*2.7E-9;
  float RS9L1Plus= (float)m_RS9L1Plus*2.7E-9;
  float RS9L2Minus= (float)m_RS9L2Minus*2.7E-9;
  float RS9L2Plus= (float)m_RS9L2Plus*2.7E-9;
  dipdata6 -> insert(RS9L1Minus,"RS9L1MinusGy");
  dipdata6 -> insert(RS9L1Plus,"RS9L1PlusGy");
  dipdata6 -> insert(RS9L2Minus,"RS9L2MinusGy");
  dipdata6 -> insert(RS9L2Plus,"RS9L2PlusGy");  
  m_dippubs[diptopic6name]->send(*dipdata6,t);
  delete dipdata6;


  for(int i=0; i<48 ;i++){
    m_PA1_NB4[i]=0.0;
    m_PA1_NB4_Filtered[i]=0.0;
    m_PA12_NB4[i]=0.0;
    m_RS9_NB4[i]=0;
  }

  //zero these max values now they are published
  m_bkgd1LminusRS1=m_bkgd1LplusRS1=m_bkgd2LminusRS1=m_bkgd2LplusRS1=0;
  m_bkgd1LminusRS12=m_bkgd1LplusRS12=m_bkgd2LminusRS12=m_bkgd2LplusRS12=0;
  m_RS9L1Minus=m_RS9L1Plus=m_RS9L2Minus=m_RS9L2Plus=0;
}

void bril::bcmlprocessor::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
    std::string data_version  = plist.getProperty("DATA_VERSION");    
    std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
    if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
      std::string msg("Received brildaq message has no or wrong version, do not process.");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      ref->release(); ref=0;
      return;
    }
    if(topic == m_signalTopic.toString()){
      if(ref!=0)
      {
        //parse timing signal message header to get timing info
        interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());      
        unsigned int fillnum = thead->fillnum;
      	unsigned int runnum = thead->runnum;
      	unsigned int lsnum = thead->lsnum;
      	unsigned int nbnum = thead->nbnum;	
      	unsigned int sec =  thead->timestampsec;
      	unsigned int msec =  thead->timestampmsec;
      	if(nbnum != m_nbnum.value_ || runnum != m_runnum.value_ ){ // even if nb number is not changed, it could be from different runs. so run number change should also trigger nbnum change signal
      	  m_nbnum.value_ = nbnum;
      	  m_lsnum.value_ = lsnum;
      	  m_runnum.value_ = runnum;
      	  m_fillnum.value_ = fillnum;
      	  m_sec.value_ = sec;
      	  m_msec.value_ = msec;
      	  LOG4CPLUS_INFO(getApplicationLogger(), "Updated TCDS info");


          //maybe push data to flashlist variables here



          // Reset Flashlist data: m_sec, m_PA1, m_PA12, m_RS9, m_tLD
          // 



          // MOVE PUBLICATION TO DIP EVENT
          
          //publish_to_eventing();	  
      	}
      }
    }
  }
  if(ref!=0){
    ref->release();
    ref=0;
  }
}

void bril::bcmlprocessor::Application::connected(DipSubscription* dipsub){
  std::cerr<<"connected to dip publication source "<<dipsub->getTopicName()<<std::endl;
}

void bril::bcmlprocessor::Application::disconnected(DipSubscription* dipsub,char *reason){
  std::cerr<<"Publication source "<<dipsub->getTopicName()<<" unavailable"<<std::endl;
}

void bril::bcmlprocessor::Application::handleException(DipSubscription* dipsub, DipException& ex){
  std::cerr<<"Publication source "<<dipsub->getTopicName()<<" exception "<<ex.what()<<std::endl;
}

void bril::bcmlprocessor::Application::subscribeToDip(){
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"subscribeToDip ");
  m_dip->setTimeout(3);
  for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
    DipSubscription* s = 0;
    s=m_dip->createDipSubscription(it->first.c_str(),this);
    if(!s){
      m_dipsubs.erase(it); // remove from registry
    }else{
      it->second=s;
    }
  }
}

void bril::bcmlprocessor::Application::createDipPublication(){
  for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
    DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
    if(!p){
      LOG4CPLUS_FATAL(getOwnerApplication()->getApplicationLogger(),"Failed to create dip topic "+it->first);
      m_dippubs.erase(it); // remove from registry
    }else{
      it->second=p;
    }
  }
}

bril::bcmlprocessor::Application::~Application(){
  for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
    m_dip->destroyDipSubscription(it->second);
    it->second = 0;
  }
  m_dipsubs.clear();
  for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
    m_dip->destroyDipPublication(it->second);
    it->second = 0;
  }
  m_dippubs.clear();
  delete m_dip;
  delete m_puberrorhandler;
  delete m_servererrorhandler;
}

