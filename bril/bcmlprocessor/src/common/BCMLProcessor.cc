#include "xdata/TableIterator.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "b2in/nub/Method.h"
#include "toolbox/net/UUID.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "interface/bril/BCMTopics.hh"
#include "toolbox/mem/AutoReference.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "bril/bcmlprocessor/BCMLProcessor.h"
#include "bril/bcmlprocessor/exception/Exception.h"

XDAQ_INSTANTIATOR_IMPL (bril::bcmlprocessor::BCMLProcessor)

bril::bcmlprocessor::BCMLProcessor::BCMLProcessor(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&BCMLProcessor::Default, "Default");  
  b2in::nub::bind(this, &bril::bcmlprocessor::BCMLProcessor::onMessage);
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();

  std::string memPoolName = "bcmlprocessor::BCMLProcessor_memPool";  
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
  toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
  m_memPool = m_poolFactory->createPool(urn,allocator);
  
  do_resetRSs();
  do_resetbkgdVariables();
  do_resetRS9Variables();
  do_resetBLMSummaryVariables();
  do_resetBLMSummaryPositions();
  for (int i=0;i<NBIN;i++){
    m_histo[i]=1;
  }
  
  m_dipDataReceived=0;
  // m_blmValueParts=0;
  m_busready_to_publish=false;
  m_dipbusready_to_publish=false;
  

  do_resetNB4Variables();
  for(size_t i=0; i<48 ;++i){
    m_PA1_NB4[i]=-1.0;
    m_PA1_NB4_Filtered[i]=-1.0;
    //m_PA1_NB64[i]=-1.0;
    //m_PA1_NB64_Filtered[i]=-1.0;
    m_PA12_NB4[i]=-1.0;
    //m_PA12_NB64[i]=-1.0;
    m_RS9_NB4[i]=-1.0;
    //m_RS9_NB64[i]=-1.0;
    m_PA1.push_back(-1);
    m_PA1_Filt.push_back(-1);
    m_PA12.push_back(-1);
    m_RS9.push_back(-1);
  }
  for(size_t i=0;i<192;++i){
    m_acqCard1[i]=0;
    m_acqCard2[i]=0;
    m_acqCard3[i]=0;
  }
  
  // they are not actually used in the code
  // TODO: fix it!
  m_lostFrames=false;
  m_thresholdsOK=true;

  m_timeLastDump=0;
  m_fillnum = 0;
  m_runnum = 0;
  m_lsnum = 0;
  m_nbnum = 0;
  m_sec = 0;
  m_msec = 0;
  m_lF = true;
  m_tOK = false;
  m_tLD = 0;
  m_acqTime.value_ = toolbox::TimeVal();
 
  m_busName.fromString("brildata");
  m_dipbusName.fromString("slimbus");
  m_diptopic.fromString("urn:dip:data");
  m_dipRoot = "dip/CMS/";
  m_signalTopic = "NB4";//no need to configure, we always listen to NB4

  getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
  getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
  getApplicationInfoSpace()->fireItemAvailable("busName",&m_busName);
  getApplicationInfoSpace()->fireItemAvailable("dipbusName",&m_dipbusName);
  //getApplicationInfoSpace()->fireItemAvailable("blmValueParts",&m_blmValueParts);

  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  //getApplicationInfoSpace()->addItemChangedListener("blmValueParts",this);
  
  std::string nid("bcmlMon");       
  std::string monurn = createQualifiedInfoSpace(nid).toString();   
  // register all monitorables to mon infospace
  m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));   
  m_monInfoSpace->fireItemAvailable("timestamp",&m_acqTime);
  m_monItemList.push_back("timestamp");
  m_monInfoSpace->fireItemAvailable("PercentAbort1",&m_PA1);
  m_monItemList.push_back("PercentAbort1");
  m_monInfoSpace->fireItemAvailable("PercentAbort12",&m_PA12);
  m_monItemList.push_back("PercentAbort12");
  m_monInfoSpace->fireItemAvailable("RunningSum9",&m_RS9);
  m_monItemList.push_back("RunningSum9");
  m_monInfoSpace->fireItemAvailable("TimeLastDump",&m_tLD);
  m_monItemList.push_back("TimeLastDump");
}

void bril::bcmlprocessor::BCMLProcessor::Default(xgi::Input * in, xgi::Output * out){}

void bril::bcmlprocessor::BCMLProcessor::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
    toolbox::TimeInterval vsec(3,0);//3 sec later to subscribe to all topics
    toolbox::TimeVal startT = now + vsec;
    toolbox::net::UUID uuid;
    std::string startsubtimer_name = "BCMLProcessor_timer_"+uuid.toString();  
    toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( startsubtimer_name );
    t->schedule( this, startT, (void*)0, "start_dipsubscription" );

    this->getEventingBus(m_busName.value_).addActionListener(this); 
    this->getEventingBus(m_dipbusName.value_).addActionListener(this); 
    if( !m_dipRoot.value_.empty() ){
      char lastChar = *(m_dipRoot.value_.rbegin());
      if( lastChar!='/' ){
	m_dipRoot.value_ += '/';
      }
    }    
    m_dipsubs.push_back( "dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition" );
    m_dipsubs.push_back( "dip/acc/LHC/Beam/BLM/LSS5R" );
    m_dipsubs.push_back( "dip/acc/LHC/Beam/BLM/LSS5L" );
    
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/Acquisition" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/Summary" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card2" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card3" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/SourceTest/Card4" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/Background3" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/HighChartsNB4" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BCMAnalysis/MonitorRS9" ); 

    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BLMSummary" );
    m_dippubs.push_back( m_dipRoot.value_+"BRIL/BLMSummary/Histo");   
    
  }
  //else if( e.type() == "ItemChangedEvent" ){
  //std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
  //if( item=="blmValueParts" ){
  //publish_to_dip_blmSummary();
  //publish_to_dip_blmHisto();
  //}
  //}
}

void bril::bcmlprocessor::BCMLProcessor::actionPerformed(toolbox::Event& e){  
  std::stringstream ss;
  if( e.type() == "eventing::api::BusReadyToPublish" ){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    if(busname==m_busName.value_){
      m_busready_to_publish = true;      
    }else{
      m_dipbusready_to_publish = true;      
    }
  }//end e.type() == "eventing::api::BusReadyToPublish"
}



void bril::bcmlprocessor::BCMLProcessor::handleDipMessage(const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"BCMLProcessor::handleMessage from "+topic);
  xdata::Table table;
  char* buf = (char*) ref->getDataLocation();
  size_t bufsize = ref->getDataSize();
  xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, bufsize);
  xdata::exdr::Serializer serializer;
  std::stringstream ss;
  try{
    serializer.import( &table, &inBuffer);
  }catch (xdata::exception::Exception& e){
    ss << "Failed to stream messagge "<<topic<<" into xdata::Table : "<< e.what();
    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
    XCEPT_DECLARE_NESTED( bril::bcmlprocessor::exception::Exception,err,ss.str(), e);
    return;
  }
  
  if(table.begin() == table.end() ){
    LOG4CPLUS_WARN( getApplicationLogger(), toolbox::toString("DIP notification table is empty for this topic %s", topic.c_str()) );
    return;  
  }
  
  xdata::Serializable* p = 0;
  if( topic=="dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition" ){
    // LOAD DATA FROM DIP via xdata::Table............
    p = table.getValueAt(0,"blecsUDL" );
    m_BeamPermit = dynamic_cast<xdata::Boolean&>(*p);

    p = table.getValueAt(0,"acqStamp");    
    long long acqStamp = dynamic_cast<xdata::Integer64*>(p)->value_;
    m_acqTime = toolbox::TimeVal(acqStamp/1e9,0);

    p = table.getValueAt(0,"acqTimeStamps");
    xdata::Vector<xdata::Integer64>* p_acqTimeStamps = dynamic_cast<xdata::Vector<xdata::Integer64>*>( p );   
    long long acqTimeStamps[ p_acqTimeStamps->elements() ];
    for(size_t i=0; i<p_acqTimeStamps->elements(); ++i){
      xdata::Integer64* val = dynamic_cast<xdata::Integer64*>(p_acqTimeStamps->elementAt(i));
      acqTimeStamps[i] = val->value_;
    }

    p = table.getValueAt(0,"acq");
    xdata::Vector<xdata::Integer64>* p_acq = dynamic_cast<xdata::Vector<xdata::Integer64>*>( p );
    long long acq[ p_acq->elements()];
    for(size_t i=0; i<p_acq->elements(); ++i){
      xdata::Integer64* val = dynamic_cast<xdata::Integer64*>(p_acq->elementAt(i));
      acq[i] = val->value_;
    }

    p = table.getValueAt(0,"thresholds");
    xdata::Vector<xdata::Integer64>* p_thresholds =  dynamic_cast<xdata::Vector<xdata::Integer64>*>( p );
    size_t sizeThresholds = p_thresholds->elements();
    long long thresholds[ p_thresholds->elements() ];
    for(size_t i=0; i<sizeThresholds; ++i){
      xdata::Integer64* val = dynamic_cast<xdata::Integer64*>(p_thresholds->elementAt(i));
      thresholds[i] = val->value_;
    }
    
    p = table.getValueAt(0,"channelBISConnected");
    xdata::Vector<xdata::Boolean>* p_channelBISConnected =  dynamic_cast<xdata::Vector<xdata::Boolean>*>( p );
    size_t sizeBIS = p_channelBISConnected->elements();
    bool channelBISConnected[sizeBIS];
    for(size_t i=0; i<sizeBIS; ++i){
      xdata::Boolean* val = dynamic_cast<xdata::Boolean*>(p_channelBISConnected->elementAt(i));
      channelBISConnected[i] = val->value_;
    }

    p = table.getValueAt(0,"channelMasked");
    xdata::Vector<xdata::Boolean>* p_channelMasked = dynamic_cast<xdata::Vector<xdata::Boolean>*>( p );
    size_t sizeMasked = p_channelMasked->elements(); 
    bool channelMasked[sizeMasked];
    for(size_t i=0; i<sizeMasked; ++i){
      xdata::Boolean* val = dynamic_cast<xdata::Boolean*>(p_channelMasked->elementAt(i));
      channelMasked[i] = val->value_;
    }

    p = table.getValueAt(0,"channelCableConnected");
    xdata::Vector<xdata::Boolean>* p_channelCableConnected = dynamic_cast<xdata::Vector<xdata::Boolean>*>( p );
    size_t sizeCable = p_channelCableConnected->elements();
    bool channelCableConnected[sizeCable];
    for(size_t i=0; i<sizeCable; ++i){
      xdata::Boolean* val = dynamic_cast<xdata::Boolean*>(p_channelCableConnected->elementAt(i));
      channelCableConnected[i] = val->value_;
    }
    
    p = table.getValueAt(0,"blecsDUMP");
    bool blecsDump =  dynamic_cast<xdata::Boolean*>(p)->value_;

    p = table.getValueAt(0,"statusLostFramesADetectors1to8");
    xdata::Vector<xdata::Integer32>* p_lfa18 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    size_t sizelf = p_lfa18->elements();
    int lfa18[sizelf];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_lfa18->elementAt(i));
      lfa18[i] = val->value_;
    }

    p = table.getValueAt(0,"statusLostFramesADetectors9to16");
    xdata::Vector<xdata::Integer32>* p_lfa916 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    sizelf = p_lfa916->elements();
    int lfa916[sizelf];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_lfa916->elementAt(i));
      lfa916[i] = val->value_;
    }
    
    p = table.getValueAt(0,"statusLostFramesBDetectors1to8");
    xdata::Vector<xdata::Integer32>* p_lfb18 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    sizelf = p_lfb18->elements();
    int lfb18[ sizelf ];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_lfb18->elementAt(i));
      lfb18[i] = val->value_;
    }

    p = table.getValueAt(0,"statusLostFramesBDetectors9to16");
    xdata::Vector<xdata::Integer32>* p_lfb916 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    sizelf = p_lfb916->elements();
    int lfb916[sizelf];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_lfb916->elementAt(i));
      lfb916[i] = val->value_;
    }

    p = table.getValueAt(0,"statusNoDumpsDetectors1to8");
    xdata::Vector<xdata::Integer32>* p_sNoDumpDet18 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    sizelf = p_sNoDumpDet18->elements();
    int sNoDumpDet18[sizelf];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_sNoDumpDet18->elementAt(i));
      sNoDumpDet18[i] = val->value_;
    }

    p = table.getValueAt(0,"statusNoDumpsDetectors9to16");
    xdata::Vector<xdata::Integer32>* p_sNoDumpDet916 = dynamic_cast<xdata::Vector<xdata::Integer32>*>( p );
    sizelf = p_sNoDumpDet916->elements();
    int sNoDumpDet916[sizelf];
    for( size_t i=0; i<sizelf; ++i){
      xdata::Integer32* val = dynamic_cast<xdata::Integer32*>(p_sNoDumpDet916->elementAt(i));
      sNoDumpDet916[i] = val->value_;
    }
    
    // Load data to global variables

    for (int i=0;i<48;i++){
      m_inAbort[i]=(channelBISConnected[i+16]&&channelCableConnected[i+16])&!channelMasked[i+16];
    }
    
    for(int i=0;i<576;i++){
      m_acq[i] = acq[i+192];
    }
    for (int i=0;i<3;i++){
      m_acqtsarray[i]=(unsigned int)(acqTimeStamps[i+1]/1e9);
      //std::cout<<"acq timestamps "<<acqTimeStamps[i+1]<<"  "<<m_acqtsarray[i]<<std::endl;
    }

    for(int i=1;i<4;i++){
      m_LostFrames[i-1]=lfa18[i];
      m_LostFrames[i+3-1]=lfa916[i];
      m_LostFrames[i+6-1]=lfb18[i];
      m_LostFrames[i+9-1]=lfb916[i];
    }

    for(int i=1;i<4;i++){
      m_nDump[i-1]=sNoDumpDet18[i];
      m_nDump[i+3-1]=sNoDumpDet916[i];
    } 

    m_BlecsDump=blecsDump;
    if(blecsDump){
      m_timeLastDump=acqStamp/1e9;
      m_tLD=m_timeLastDump;
    }


    //OLD STUFF......

    bool useDiamond[40];
    for(int i=0;i<40;i++){
      useDiamond[i]=false;
    }

    //std::cout << "active acq card data: "<<std::endl;
    // acq array is 16 cards time 192 values
    // however we only use cards numbers 1,2,3 (starting at 0)
    // there are 12 running sums per channel, there are 16 channels per card   
    int card,channel,rsum;
    int index=0;
    int acqIndex=0;
    if(m_dipDataReceived==0){
      for (int i=0;i< MAX_CHANNELS;i++){
        for(int j=0;j<NUMBER_RSUMS;j++){
          m_card2[i][j]=0;
          m_card3[i][j]=0;
          m_card4[i][j]=0;
        }
      }
    }

    m_dipDataReceived++;

    for (card=0; card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        acqIndex=0;
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          //std::cout<<"card "<<card<<","<<" channel "<<channel<<", rsums :";
          for(rsum=0;rsum<NUMBER_RSUMS; rsum++)
          {
            m_acqArray[card][channel][rsum] = static_cast<int64_t>(acq[index]);
            if(card==1){
              m_acqCard1[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card2[channel][rsum] = std::max(m_card2[channel][rsum], static_cast<int64_t>(acq[index]) );
            } else if (card==2){
              m_acqCard2[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card3[channel][rsum] = std::max(m_card3[channel][rsum], static_cast<int64_t>(acq[index]) );
            } else if (card==3){
              m_acqCard3[acqIndex++] = static_cast<int64_t>(acq[index]);
              m_card4[channel][rsum]=std::max(m_card4[channel][rsum], static_cast<int64_t>(acq[index]) );
            }
            //std::cout<<m_acqArray[card][channel][rsum]<<";  ";
            index++;
          }
          //std::cout<<std::endl;
        }
      } else {
        index+=MAX_CHANNELS*NUMBER_RSUMS;
      }
    }


    // Now data in: m_acq, m_acqArray (maxalgo), acqCard1/2/3 (local), m_card1/2/3 (maxalgo)


    index=0;
    for (card=0; card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          for(rsum=0;rsum<NUMBER_RSUMS; rsum++)
          {
            m_thresholdArray[card][channel][rsum]=static_cast<int64_t>(thresholds[index]);
            index++;
          }
        }
      } else {
        index+=MAX_CHANNELS*NUMBER_RSUMS;
      }
    }


    //std::cout<<"last dump time is "<<m_timeLastDump<<std::endl;
    if(sizeBIS+sizeMasked+sizeCable == 48*MAX_CHANNELS)
    {
      index=0;
      int i_diamond=0;
      int ntoUse=0;
      for (card=0;card<MAX_CARDS;card++)
      {
        if(acqTimeStamps[card]>0)
        {
          for(channel=0;channel<MAX_CHANNELS;channel++)
          {
            m_abortChannelsArray[card][channel]=(channelBISConnected[index]&&channelCableConnected[index])&!channelMasked[index];
            useDiamond[i_diamond]=m_abortChannelsArray[card][channel];
            ntoUse+=int(m_abortChannelsArray[card][channel]);
            index++;
            i_diamond++;
          }
        } else {
          index=index+MAX_CHANNELS;
        }
      }
      //std::cout<<" there are "<<ntoUse<< "channels in the abort "<<std::endl;
    }else {
      LOG4CPLUS_ERROR(getApplicationLogger(),"inconsistent Connected Channel Data");
    }
    m_liveBCM1LChannels=m_liveBCM2LChannels=0;
    int i_diamond=0;
    for (card=0;card<MAX_CARDS;card++)
    {
      if(acqTimeStamps[card]>0)
      {
        for(channel=0;channel<MAX_CHANNELS;channel++)
        {
          float thisFracAbortRS1=
          (100.0*m_acqArray[card][channel][0])/m_thresholdArray[card][channel][0];
          float thisFracAbortRS12=
          (100.0*m_acqArray[card][channel][11])/m_thresholdArray[card][channel][11];
          thisFracAbortRS1=thisFracAbortRS1*m_abortChannelsArray[card][channel];
          thisFracAbortRS12=thisFracAbortRS12*m_abortChannelsArray[card][channel];
          if(useDiamond[i_diamond]){
            m_PA1_NB4[i_diamond]=std::max(m_PA1_NB4[i_diamond],thisFracAbortRS1);
            //m_PA1_NB64[i_diamond]=std::max(m_PA1_NB64[i_diamond],thisFracAbortRS1);
            // next line needs to be replaced by filtering code
            m_PA1_NB4_Filtered[i_diamond]=std::max(m_PA1_NB4_Filtered[i_diamond],thisFracAbortRS1);
            m_PA12_NB4[i_diamond]=std::max(m_PA12_NB4[i_diamond],thisFracAbortRS12);
            //m_PA12_NB64[i_diamond]=std::max(m_PA12_NB64[i_diamond],thisFracAbortRS12);


            m_PA1[i_diamond]=m_PA1_NB4[i_diamond];
            m_PA12[i_diamond]=m_PA12_NB4[i_diamond];
            m_PA1_Filt[i_diamond]=m_PA1_NB4_Filtered[i_diamond];
          }
          m_RS9_NB4[i_diamond]=std::max(m_RS9_NB4[i_diamond],m_acqArray[card][channel][9]);
      	  //m_RS9_NB64[i_diamond]=std::max(m_RS9_NB64[i_diamond],m_acqArray[card][channel][9]);
      	  m_RS9[i_diamond] = float( m_RS9_NB4[i_diamond] );
          //m_RS9[i_diamond]=m_PA12_NB4[i_diamond];

          if(card==1){
            if(channel<4){
              m_bkgd1LminusRS1=std::max(m_bkgd1LminusRS1,thisFracAbortRS1);
              m_bkgd1LminusRS12=std::max(m_bkgd1LminusRS12,thisFracAbortRS12);
              if(useDiamond[i_diamond]){
                m_RS9L1Minus=std::max(m_RS9L1Minus,m_acqArray[card][channel][9]);
              }
            } else if( channel<8){
              m_bkgd1LplusRS1=std::max(m_bkgd1LplusRS1,thisFracAbortRS1);
              m_bkgd1LplusRS12=std::max(m_bkgd1LplusRS12,thisFracAbortRS12);
              if(useDiamond[i_diamond]){
                m_RS9L1Plus=std::max(m_RS9L1Plus,m_acqArray[card][channel][9]);
              }              
            }
          }
          if(card==1&&m_abortChannelsArray[card][channel]&&m_acqArray[card][channel][11]>0){
            m_liveBCM1LChannels++;
          }
          if(card>1&&m_abortChannelsArray[card][channel]&&m_acqArray[card][channel][11]>0){
            m_liveBCM2LChannels++;
          }
          if(card==2){
            m_bkgd2LminusRS1=std::max(m_bkgd2LminusRS1,thisFracAbortRS1);
            m_bkgd2LminusRS12=std::max(m_bkgd2LminusRS12,thisFracAbortRS12);
            if(useDiamond[i_diamond]){
              m_RS9L2Minus=std::max(m_RS9L2Minus,m_acqArray[card][channel][9]);
            }
          }
          if(card==3){
            m_bkgd2LplusRS1=std::max(m_bkgd2LplusRS1,thisFracAbortRS1);
            m_bkgd2LplusRS12=std::max(m_bkgd2LplusRS12,thisFracAbortRS12); 
            if(useDiamond[i_diamond]){
              m_RS9L2Plus=std::max(m_RS9L2Plus,m_acqArray[card][channel][9]);
            }           
          }
          i_diamond++;
        }
      }
      
    }

  // Check if RS1 and RS12 abort thresholds are correct
  m_liveRS1Thresholds=m_liveRS12Thresholds=0;
  for(card=0;card<MAX_CARDS;card++) {
      for(channel=0;channel<MAX_CHANNELS;channel++) {
          // Threshold array for RS1
          if ( m_thresholdArray[card][channel][0] >= 100 && m_thresholdArray[card][channel][0] <= 4100 ) {
              m_liveRS1Thresholds++;
          }
          // Threshold array for RS12
          if ( m_thresholdArray[card][channel][11] > 0 && m_thresholdArray[card][channel][11] < 4294967295 ) {
              m_liveRS12Thresholds++;
          }
      }
  }

  float tmp=-1;
  for (int i=0;i<48;i++){
    if(m_PA1[i].value_>tmp) tmp=m_PA1[i].value_;
  }
  m_PA1_max=tmp;

  // HERE GOES THE PUBLISHING....


  // Send data to Flashlist
  try {
    m_monInfoSpace->fireItemGroupChanged(m_monItemList,this);
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to push monitoring items to bcmlMon");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg,e);
    notifyQualified("error",myerrorobj);
  }

  publish_to_eventing();
  publish_to_dip_Summary();
  publish_to_dip_SourceTest(2);
  publish_to_dip_SourceTest(3);
  publish_to_dip_SourceTest(4);
  publish_to_dip_Background3();
  publish_to_dip_HighChartsNB4();
  publish_to_dip_MonitorRS9();
  publish_to_dip_Acquisition();
  do_resetNB4Variables();
  do_resetbkgdVariables();
  do_resetRS9Variables();
  
  }else if( topic=="dip/acc/LHC/Beam/BLM/LSS5R" || topic=="dip/acc/LHC/Beam/BLM/LSS5L" ){
    //blmsummary
    xdata::Serializable* p_BLMNames = table.getValueAt( 0,"names");
    xdata::Vector<xdata::String>* BLMNames = dynamic_cast<xdata::Vector<xdata::String>*>( p_BLMNames );
    
    xdata::Serializable* p_BLMRS9 = table.getValueAt(0,"RS9_1310ms"); 
    xdata::Serializable* p_BLMRS8 = table.getValueAt(0,"RS8_655ms"); 
    xdata::Serializable* p_BLMRS1 = table.getValueAt(0,"RS1_40us"); 

    xdata::Vector<xdata::Double>* BLMRS9 = dynamic_cast< xdata::Vector<xdata::Double>* >( p_BLMRS9 );
    xdata::Vector<xdata::Double>* BLMRS8 = dynamic_cast< xdata::Vector<xdata::Double>* >( p_BLMRS8 );
    xdata::Vector<xdata::Double>* BLMRS1 = dynamic_cast< xdata::Vector<xdata::Double>* >( p_BLMRS1 );
    size_t sizeNames = BLMNames->elements();
    size_t sizeValues = BLMRS9->elements();
    size_t loop=sizeNames;
    if(sizeValues<sizeNames) loop=sizeValues;
    for( size_t i=0; i<loop; ++i ){
      auto it = m_blmValues.find( dynamic_cast<xdata::String*>(BLMNames->elementAt(i))->value_ );
      if( it!=m_blmValues.end() ){
        it->second = dynamic_cast<xdata::Double*>(BLMRS9->elementAt(i))->value_;
      }
    }
    if(topic.find("LSS5L")!=std::string::npos){
      for( size_t i=0; i<62; ++i ){
	m_rs9_l[i] = dynamic_cast<xdata::Double*>(BLMRS9->elementAt(i))->value_;
	m_rs8_l[i] = dynamic_cast<xdata::Double*>(BLMRS8->elementAt(i))->value_;
	m_rs1_l[i] = dynamic_cast<xdata::Double*>(BLMRS1->elementAt(i))->value_;
      }
    }else{
      for( size_t i=0; i<62; ++i ){
	m_rs9_r[i] = dynamic_cast<xdata::Double*>(BLMRS9->elementAt(i))->value_;
	m_rs8_r[i] = dynamic_cast<xdata::Double*>(BLMRS8->elementAt(i))->value_;
	m_rs1_r[i] = dynamic_cast<xdata::Double*>(BLMRS1->elementAt(i))->value_;
      }
    }
    //m_blmValueParts = m_blmValueParts+1;
    //if( m_blmValueParts.value_==3 ){
    //  getApplicationInfoSpace()->fireItemValueChanged("bmlValueParts");
    //}
  }
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_eventing(){
  if(!m_busready_to_publish) return;
  std::stringstream msg;  
  //fill message buffer
  std::string topicname=interface::bril::bcmT::topicname();
  toolbox::mem::Reference* bufRef = 0;
  try{
    // fill property
    std::string pdict = interface::bril::bcmT::payloaddict();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);
    size_t totalsize = interface::bril::bcmT::maxsize();
    // get message buffer
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    // fill message header
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_sec.value_,m_msec.value_);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(totalsize);
    // fill message payload
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field( header->payloadanchor, "acq", m_acq);
    tc.insert_field( header->payloadanchor, "acqts", m_acqtsarray);
    tc.insert_field( header->payloadanchor, "lostframes", m_LostFrames);
    tc.insert_field( header->payloadanchor, "ndumps", m_nDump);
    tc.insert_field( header->payloadanchor, "blecsdump", &m_BlecsDump);
    tc.insert_field( header->payloadanchor, "inabort", m_inAbort);
    tc.insert_field( header->payloadanchor, "pa1max", &(m_PA1_max.value_) );
    // publish to eventing

    msg<<m_fillnum.value_<<","<<m_runnum.value_<<","<<m_lsnum.value_<<","<<m_nbnum.value_<<","<<m_sec.value_<<"."<<m_msec.value_<<",";
    msg<<"[";
    for(int i=0;i<576;++i){
      msg<<m_acq[i];
      if(i!=575) msg<<" ";
    }
    msg<<"],[";
    for(int i=0;i<3;++i){
      msg<<m_acqtsarray[i];
      if(i!=2) msg<<" ";
    }
    msg<<"],";
    msg<<m_LostFrames<<",[";
    for(int i=0;i<6;++i){
      msg<<m_nDump[i];
      if(i!=5) msg<<" ";
    }
    msg<<"],";
    msg<<m_BlecsDump<<",";
    msg<<m_inAbort;

    //LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
    this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<m_busName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release() ;
    }
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg.str(),e);
    notifyQualified("fatal",myerrorobj);
  }
  m_dipDataReceived=0;
}


void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_Summary(){
  if(!m_dipbusready_to_publish) return;
  std::string dipname(m_dipRoot.value_+"BRIL/BCMAnalysis/Summary");
  xdata::Vector< xdata::Integer64 > card1rs1Values;
  xdata::Vector< xdata::Integer64 > card1rs9Values;
  for (int j=0;j<8;j++){
    card1rs1Values.push_back(m_acqArray[1][j][0]);
    card1rs9Values.push_back(m_acqArray[1][j][8]);
  }
  xdata::Vector< xdata::Integer64 > card2rs1Values;
  xdata::Vector< xdata::Integer64 > card2rs9Values;
  for (int j=0;j<16;j++){
    card2rs1Values.push_back(m_acqArray[2][j][0]);
    card2rs9Values.push_back(m_acqArray[2][j][8]);
  }
  xdata::Vector< xdata::Integer64 > card3rs1Values;
  xdata::Vector< xdata::Integer64 > card3rs9Values;
  for (int j=0;j<16;j++){
    card3rs1Values.push_back(m_acqArray[3][j][0]);
    card3rs9Values.push_back(m_acqArray[3][j][8]);
  }

  xdata::Integer64 mus = toolbox::TimeVal::gettimeofday().sec()*1000+toolbox::TimeVal::gettimeofday().usec()/1000;//milli sec at the end...
  //int64_t mus =t.getAsNanos()/1000;
  
  xdata::Table::Reference dipmessage( new xdata::Table );
  dipmessage->addColumn("timestamp","int 64");
  dipmessage->addColumn("TCDS_RunNumber","int 32");
  dipmessage->addColumn("TCDS_LSegmentNumber","int 32");
  dipmessage->addColumn("TCDS_LNibbleNumber","int 32");
  dipmessage->addColumn("BeamPermit","bool");
  dipmessage->addColumn("LiveAbortBCM1LChannels","int 32");
  dipmessage->addColumn("LiveAbortBCM2LChannels","int 32");
  dipmessage->addColumn("CorrectBCMLThresholdsRS1","int 32");
  dipmessage->addColumn("CorrectBCMLThresholdsRS12","int 32");
  
  dipmessage->addColumn("maxAbortPercentBCM1L","float");
  dipmessage->addColumn("maxAbortPercentBCM2L","float");
  dipmessage->addColumn("BCM1LRS1Values","vector int 64");
  dipmessage->addColumn("BCM2LMinusRS1Values","vector int 64");
  dipmessage->addColumn("BCM2LPlusRS1Values","vector int 64");
  dipmessage->addColumn("BCMHVOn","bool");
  dipmessage->addColumn("00Message","string");

  dipmessage->setValueAt(0,"timestamp",mus);
  xdata::Integer32 runnum = int(m_runnum.value_);
  dipmessage->setValueAt(0,"TCDS_RunNumber",runnum);
  xdata::Integer32 lsnum = int(m_lsnum.value_);
  dipmessage->setValueAt(0,"TCDS_LSegmentNumber",lsnum);
  xdata::Integer32 nbnum = int(m_nbnum.value_);
  dipmessage->setValueAt(0,"TCDS_LNibbleNumber",nbnum);

  dipmessage->setValueAt(0,"BeamPermit",m_BeamPermit);
  xdata::Integer32 liveBCM1LChannels(m_liveBCM1LChannels);
  dipmessage->setValueAt(0,"LiveAbortBCM1LChannels",liveBCM1LChannels);
  xdata::Integer32 liveBCM2LChannels(m_liveBCM2LChannels);
  dipmessage->setValueAt(0,"LiveAbortBCM2LChannels",liveBCM2LChannels);

  xdata::Integer32 liveRS1Thresholds(m_liveRS1Thresholds);
  dipmessage->setValueAt(0,"CorrectBCMLThresholdsRS1",liveRS1Thresholds);
  xdata::Integer32 liveRS12Thresholds(m_liveRS12Thresholds);
  dipmessage->setValueAt(0,"CorrectBCMLThresholdsRS12",liveRS12Thresholds);

  xdata::Float maxAbortPercentBCM1L = std::max(m_bkgd1LminusRS1,m_bkgd1LplusRS1);
  dipmessage->setValueAt(0,"maxAbortPercentBCM1L",maxAbortPercentBCM1L);
  xdata::Float maxAbortPercentBCM2L = std::max(m_bkgd2LminusRS1,m_bkgd2LplusRS1);
  dipmessage->setValueAt(0,"maxAbortPercentBCM2L",maxAbortPercentBCM2L);
  dipmessage->setValueAt(0,"BCM1LRS1Values",card1rs1Values);
  dipmessage->setValueAt(0,"BCM2LMinusRS1Values",card2rs1Values);
  dipmessage->setValueAt(0,"BCM2LPlusRS1Values",card3rs1Values);
  xdata::Boolean BCMHVOn(true);
  dipmessage->setValueAt(0,"BCMHVOn",BCMHVOn);
  xdata::String V00Message("HV MONITORING NOT OPERATIONAL!");
  dipmessage->setValueAt(0,"00Message",V00Message);
  
  do_publish_to_dipbus(dipname,dipmessage);  
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_SourceTest(int cardnumber){
  if(!m_dipbusready_to_publish) return;
  std::stringstream ss;
  xdata::Table::Reference dipmessage( new xdata::Table );
  ss<<m_dipRoot.value_<<"BRIL/BCMAnalysis/SourceTest/Card"<<cardnumber;
  std::string dipname(ss.str());
  ss.str("");ss.clear();

  int cardidx = cardnumber-1;
  int maxchannel = 8;
  if(cardnumber>2){
    maxchannel = 16;
  }
  std::string s;
  for (int i=0; i<maxchannel; i++){
    if( i<9 ){
      ss<<"Channel0";
    }else{
      ss<<"Channel";
    }
    ss<<i+1;
    dipmessage->addColumn(ss.str(),"int 64");
    xdata::Integer64 v(m_acqArray[cardidx][i][8]);
    dipmessage->setValueAt(0,ss.str(),v);
    ss.str("");ss.clear();
  }  
  do_publish_to_dipbus(dipname,dipmessage);  
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_Background3(){
  if(!m_dipbusready_to_publish) return;
  std::stringstream ss;
  std::string dipname(m_dipRoot.value_+"BRIL/BCMAnalysis/Background3");
  xdata::Table::Reference dipmessage( new xdata::Table );
  dipmessage->addColumn("BCM1LMinusRS1","float");
  xdata::Float f;
  f = m_bkgd1LminusRS1;
  dipmessage->setValueAt(0,"BCM1LMinusRS1",f);
  f = m_bkgd1LplusRS1;
  dipmessage->addColumn("BCM1LPlusRS1","float");
  dipmessage->setValueAt(0,"BCM1LPlusRS1",f);
  f = m_bkgd2LminusRS1;
  dipmessage->addColumn("BCM2LMinusRS1","float");
  dipmessage->setValueAt(0,"BCM2LMinusRS1",f);
  f = m_bkgd2LplusRS1;
  dipmessage->addColumn("BCM2LPlusRS1","float");
  dipmessage->setValueAt(0,"BCM2LPlusRS1",f);

  float m1=std::max(m_bkgd1LminusRS1,m_bkgd2LminusRS1);
  float m2=std::max(m_bkgd1LplusRS1,m_bkgd2LplusRS1);
  float bkgd3rs1=std::max(m1,m2);
  dipmessage->addColumn("MinusRS1","float");
  f = m1;
  dipmessage->setValueAt(0,"MinusRS1",f);

  dipmessage->addColumn("PlusRS1","float");
  f = m2;
  dipmessage->setValueAt(0,"PlusRS1",f);

  f = bkgd3rs1;
  dipmessage->addColumn("BKGD3RS1","float");
  dipmessage->setValueAt(0,"BKGD3RS1",f);
  
  dipmessage->addColumn("BCM1LMinusRS12","float");
  f = m_bkgd1LminusRS12;
  dipmessage->setValueAt(0,"BCM1LMinusRS12",f);

  dipmessage->addColumn("BCM1LPlusRS12","float");
  f = m_bkgd1LplusRS12;
  dipmessage->setValueAt(0,"BCM1LPlusRS12",f);

  dipmessage->addColumn("BCM2LMinusRS12","float");
  f = m_bkgd2LminusRS12;
  dipmessage->setValueAt(0,"BCM2LMinusRS12",f);

  dipmessage->addColumn("BCM2LPlusRS12","float");  
  f = m_bkgd2LplusRS12;
  dipmessage->setValueAt(0,"BCM2LPlusRS12",f);

  dipmessage->addColumn("MinusRS12","float");
  f = std::max(m_bkgd1LminusRS12,m_bkgd2LminusRS12);
  dipmessage->setValueAt(0,"MinusRS12",f);

  dipmessage->addColumn("PlusRS12","float");
  f = std::max(m_bkgd1LplusRS12,m_bkgd2LplusRS12);
  dipmessage->setValueAt(0,"PlusRS12",f);

  m1=std::max(m_bkgd1LminusRS12,m_bkgd2LminusRS12);
  m2=std::max(m_bkgd1LplusRS12,m_bkgd2LplusRS12);
  float bkgd3rs12=std::max(m1,m2);
  f = bkgd3rs12;
  dipmessage->addColumn("BKGD3RS12","float");
  dipmessage->setValueAt(0,"BKGD3RS12",f);

  dipmessage->addColumn("BKGD3","float");
  float mm = std::max(bkgd3rs1,bkgd3rs12);
  f = mm;
  dipmessage->setValueAt(0,"BKGD3",f);  
  
  do_publish_to_dipbus(dipname,dipmessage);
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_HighChartsNB4(){
  if(!m_dipbusready_to_publish) return;
  xdata::Table::Reference dipmessage( new xdata::Table );
  std::string dipname(m_dipRoot.value_+"BRIL/BCMAnalysis/HighChartsNB4");
  
  dipmessage->addColumn("maxPercentAbortRS1","vector float");
  xdata::Vector< xdata::Float > PA1_NB4;
  for(size_t i=0; i<48; ++i){
    PA1_NB4.push_back(m_PA1_NB4[i]);
  }
  dipmessage->setValueAt(0,"maxPercentAbortRS1",PA1_NB4);

  dipmessage->addColumn("maxPercentAbortRS1Filtered","vector float");
  xdata::Vector< xdata::Float > PA1_NB4_Filtered;
  for(size_t i=0; i<48; ++i){
    PA1_NB4_Filtered.push_back(m_PA1_NB4_Filtered[i]);
  }
  dipmessage->setValueAt(0,"maxPercentAbortRS1Filtered",PA1_NB4_Filtered);
  
  dipmessage->addColumn("maxPercentAbortRS12","vector float");
  xdata::Vector< xdata::Float > PA12_NB4;
  for(size_t i=0; i<48; ++i){
    PA12_NB4.push_back(m_PA12_NB4[i]);
  }
  dipmessage->setValueAt(0,"maxPercentAbortRS12",PA12_NB4);

  dipmessage->addColumn("maxRS9","vector int 64");
  xdata::Vector< xdata::Integer64 > RS9_NB4;
  for(size_t i=0; i<48; ++i){
    RS9_NB4.push_back(m_RS9_NB4[i]);
  }
  dipmessage->setValueAt(0,"maxRS9",RS9_NB4);

  dipmessage->addColumn("lostFrames","bool");
  xdata::Boolean lostFrames = m_lostFrames;
  dipmessage->setValueAt(0,"lostFrames",lostFrames);

  dipmessage->addColumn("thresholdsOK","bool");
  xdata::Boolean thresholdsOK = m_thresholdsOK;
  dipmessage->setValueAt(0,"thresholdsOK",thresholdsOK);

  dipmessage->addColumn("timeOflastDump","int 64");
  xdata::Integer64 timeLastDump = m_timeLastDump;
  dipmessage->setValueAt(0,"timeOflastDump",timeLastDump);
  
  do_publish_to_dipbus(dipname,dipmessage);
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_MonitorRS9(){  
  if(!m_dipbusready_to_publish) return;
  std::string dipname(m_dipRoot.value_+"BRIL/BCMAnalysis/MonitorRS9");
  xdata::Table::Reference dipmessage( new xdata::Table );
  xdata::Float RS9L1Minus( (float)m_RS9L1Minus*2.7E-9 );
  xdata::Float RS9L1Plus( (float)m_RS9L1Plus*2.7E-9 );
  xdata::Float RS9L2Minus( (float)m_RS9L2Minus*2.7E-9 );
  xdata::Float RS9L2Plus( (float)m_RS9L2Plus*2.7E-9 );
  //blmsummary
  m_blmValues["CMS.BCML1.1L5"] = double( RS9L1Minus.value_ );
  m_blmValues["CMS.BCML1.1R5"] = double( RS9L1Plus.value_ );
  m_blmValues["CMS.BCML2.1L5"] = double( RS9L2Minus.value_ );
  m_blmValues["CMS.BCML2.1R5"] = double( RS9L2Plus.value_ );
  //m_blmValueParts = m_blmValueParts+1;  
  //if( m_blmValueParts.value_==3 ){
  //  getApplicationInfoSpace()->fireItemValueChanged("blmValueParts");
  //}
  dipmessage->addColumn("RS9L1MinusGy","float");
  dipmessage->setValueAt(0,"RS9L1MinusGy",RS9L1Minus);
  dipmessage->addColumn("RS9L1PlusGy","float");
  dipmessage->setValueAt(0,"RS9L1PlusGy",RS9L1Plus);
  dipmessage->addColumn("RS9L2MinusGy","float");
  dipmessage->setValueAt(0,"RS9L2MinusGy",RS9L2Minus);
  dipmessage->addColumn("RS9L2PlusGy","float");  
  dipmessage->setValueAt(0,"RS9L2PlusGy",RS9L2Plus);
  
  do_publish_to_dipbus(dipname,dipmessage);
}

void bril::bcmlprocessor::BCMLProcessor::publish_to_dip_Acquisition(){
  if(!m_dipbusready_to_publish) return;
  std::string dipname(m_dipRoot.value_+"BRIL/BCMAnalysis/Acquisition");
  xdata::Table::Reference dipmessage( new xdata::Table );
  xdata::Vector< xdata::Integer64 > acqCard1;
  xdata::Vector< xdata::Integer64 > acqCard2;
  xdata::Vector< xdata::Integer64 > acqCard3;
  for(size_t i=0; i<192;++i){
    acqCard1.push_back(m_acqCard1[i]);
    acqCard2.push_back(m_acqCard2[i]);
    acqCard3.push_back(m_acqCard3[i]);
  }
  dipmessage->addColumn("Card2","vector int 64");
  dipmessage->setValueAt(0,"Card2",acqCard1);
  dipmessage->addColumn("Card3","vector int 64");
  dipmessage->setValueAt(0,"Card3",acqCard2);
  dipmessage->addColumn("Card4","vector int 64");
  dipmessage->setValueAt(0,"Card4",acqCard3);
 
  do_publish_to_dipbus(dipname,dipmessage);
  for(size_t i=0;i<192;++i){
    m_acqCard1[i]=0;
    m_acqCard2[i]=0;
    m_acqCard3[i]=0;
  }
}

void bril::bcmlprocessor::BCMLProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
  toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify" && ref!=0 ){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
    if( topic == m_signalTopic.toString() ){
      //parse timing signal message header to get timing info
      interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());      
      unsigned int fillnum = thead->fillnum;
      unsigned int runnum = thead->runnum;
      unsigned int lsnum = thead->lsnum;
      unsigned int nbnum = thead->nbnum;
      unsigned int sec =  thead->timestampsec;
      unsigned int msec =  thead->timestampmsec;

      if(nbnum != m_nbnum.value_ || runnum != m_runnum.value_ ){ // even if nb number is not changed, it could be from different runs. so run number change should also trigger nbnum change signal
	m_nbnum.value_ = nbnum;
	m_runnum.value_ = runnum;
	m_fillnum.value_ = fillnum;
	m_lsnum.value_ = lsnum;
	m_sec.value_ = sec;
	m_msec.value_ = msec;
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Updated TCDS info");
	publish_to_dip_blmSummary();
	publish_to_dip_blmHisto();
	publish_rs_to_eventing();
      }
    }else if( topic.find("dip/")!=std::string::npos ){
      handleDipMessage(topic,ref,plist);
    }  
  }
}

void bril::bcmlprocessor::BCMLProcessor::publish_rs_to_eventing(){
  if(!m_busready_to_publish) return;
  std::stringstream msg;
  std::string pdict = interface::bril::blmrsT::payloaddict();  
  xdata::Properties plist;
  plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
  plist.setProperty("PAYLOAD_DICT",pdict);
  size_t totalsize = interface::bril::blmrsT::maxsize();
  //fill message buffer
  toolbox::mem::Reference* bufRef = 0;
  std::string topicname;
  try{
    topicname = interface::bril::blmrsT::topicname()+"l";
    // get message buffer
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    // fill message header
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_sec.value_,m_msec.value_);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(totalsize);
    // fill message payload
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field( header->payloadanchor, "rs91310ms", &m_rs9_l[0]);
    tc.insert_field( header->payloadanchor, "rs140us", &m_rs8_l[0]);
    tc.insert_field( header->payloadanchor, "rs8655ms", &m_rs1_l[0]);
    // publish to eventing
    this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<m_busName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release() ;
    }
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg.str(),e);
    notifyQualified("fatal",myerrorobj);
  }
  //publish right side
  try{
    topicname = interface::bril::blmrsT::topicname()+"r";
    // get message buffer
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    // fill message header
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_sec.value_,m_msec.value_);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(totalsize);
    // fill message payload
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field( header->payloadanchor, "rs91310ms", &m_rs9_r[0]);
    tc.insert_field( header->payloadanchor, "rs140us", &m_rs8_r[0]);
    tc.insert_field( header->payloadanchor, "rs8655ms", &m_rs1_r[0]);
    // publish to eventing
    this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<m_busName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release() ;
    }
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,msg.str(),e);
    notifyQualified("fatal",myerrorobj);
  }
}

void bril::bcmlprocessor::BCMLProcessor::do_publish_to_dipbus(const std::string& dipname, xdata::Table::Reference dipmessage){
  std::stringstream ss;
  xdata::exdr::AutoSizeOutputStreamBuffer outBuffer; 
  xdata::exdr::Serializer serializer; 
  toolbox::mem::Reference* bufRef=0;
  xdata::Properties plist;
  plist.setProperty( "urn:dipbridge:dipname" , dipname );
  try{
    serializer.exportAll(&(*dipmessage),&outBuffer);
    char* buf = outBuffer.getBuffer();
    size_t bufsize = outBuffer.tellp();    
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,bufsize);
    bufRef->setDataSize(bufsize);	
    memcpy( bufRef->getDataLocation(), buf, bufsize );
    this->getEventingBus(m_dipbusName.value_).publish(m_diptopic.value_,bufRef,plist);
  }catch(xcept::Exception& e){
    ss<<"Failed to publish dip "<<dipname<<" to "<<m_diptopic.value_<<" on bus "<<m_dipbusName.toString();
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef) bufRef->release(); 
    XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::Exception,myerrorobj,ss.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }
}

void bril::bcmlprocessor::BCMLProcessor::timeExpired( toolbox::task::TimerEvent& e ){
  std::stringstream ss;
  try{
    this->getEventingBus(m_busName.value_).subscribe(m_signalTopic.value_);
  }catch(eventing::api::exception::Exception& e){
    ss<<"Failed to subscribe to eventing "<<m_busName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str()+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::bcmlprocessor::exception::Exception,ss.str(),e);     
  }
  try{
    for(std::vector<std::string>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
      this->getEventingBus(m_dipbusName.value_).subscribe(*it);
    }
  }catch(eventing::api::exception::Exception& e){
    ss<<"Failed to subscribe to eventing "<<m_dipbusName.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str()+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::bcmlprocessor::exception::Exception,ss.str(),e);     
  }
}
bril::bcmlprocessor::BCMLProcessor::~BCMLProcessor(){ 
}

void
bril::bcmlprocessor::BCMLProcessor::do_resetRSs(){
  m_rs9_l.fill(0.);
  m_rs8_l.fill(0.);
  m_rs1_l.fill(0.);
  m_rs9_r.fill(0.);
  m_rs8_r.fill(0.);
  m_rs1_r.fill(0.);
}

void
bril::bcmlprocessor::BCMLProcessor::do_resetbkgdVariables(){
  m_bkgd1LminusRS1=0;
  m_bkgd1LplusRS1=0;
  m_bkgd2LminusRS1=0;
  m_bkgd2LplusRS1=0;
  m_bkgd1LminusRS12=0;
  m_bkgd1LplusRS12=0;
  m_bkgd2LminusRS12=0;
  m_bkgd2LplusRS12=0;
}

void
bril::bcmlprocessor::BCMLProcessor::do_resetRS9Variables(){
  m_RS9L1Minus=0;
  m_RS9L1Plus=0;
  m_RS9L2Minus=0;
  m_RS9L2Plus=0;
}

void
bril::bcmlprocessor::BCMLProcessor::do_resetNB4Variables(){
  for(int i=0; i<48 ;i++){
    m_PA1_NB4[i]=0.0;
    m_PA1_NB4_Filtered[i]=0.0;
    m_PA12_NB4[i]=0.0;
    m_RS9_NB4[i]=0;
  }
}

void
bril::bcmlprocessor::BCMLProcessor::do_resetBLMSummaryVariables(){
  // the list of BLM Monitors and their positions to monitor
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B2I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B2I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B1E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B1E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_TANC.4R5",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B2I10_TCTVA.4R5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B2I10_TCTH.4R5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_TCLP.4R5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E20_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I30_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E30_MQY",0.0));

  m_blmValues.insert(std::make_pair("BLMQI.01L5.B1I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B1I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B2E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B2E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_TANC.4L5",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B1I10_TCTVA.4L5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B1I10_TCTH.4L5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_TCLP.4L5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I30_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E10_MQY_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E30_MQY",0.0));
  
  m_blmValues.insert(std::make_pair("CMS.BCML1.1L5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML2.1L5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML1.1R5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML2.1R5",0.0)); 

}

void
bril::bcmlprocessor::BCMLProcessor::do_resetBLMSummaryPositions(){
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B2I30_MQXA",21.4013));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E10_MQXA",23.5113));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B2I20_MQXA",25.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E20_MQXA",26.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I30_MQXB",29.5305));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E30_MQXA",32.3705));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I23_MQXB",34.8173));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E21_MQXB",35.3673));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I22_MQXB",37.4663));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E22_MQXB",38.6413));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I21_MQXB",40.8793));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E23_MQXB",41.8793));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I30_MQXA",43.8205));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E30_MQXB",46.7633));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I20_MQXA",49.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B1E20_MQXA",50.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I10_MQXA",53.8837));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B1E30_MQXA",59.0833));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_TANC.4R5",140.2613));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B2I10_TCTVA.4R5.B2",144.6653));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B2I10_TCTH.4R5.B2",146.7513));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_TCLP.4R5.B1",151.0813));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_XRP",153.4297));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E20_XRP",162.1713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I30_MQY",162.1713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E10_MQY",164.6713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I20_MQY",166.9943));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E20_MQY",168.1943));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I10_MQY",170.1897));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E30_MQY",174.6643));

  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B1I30_MQXA",-21.4013));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E10_MQXA",-23.3587));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B1I20_MQXA",-25.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E20_MQXA",-26.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I30_MQXB",-29.4775));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E30_MQXA",-32.2875));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I23_MQXB",-34.8137));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E21_MQXB",-35.2047));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I22_MQXB",-37.0927));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E22_MQXB",-38.6287));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I21_MQXB",-40.3017));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E23_MQXB",-41.7707));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I30_MQXA",-43.4727));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E30_MQXB",-46.4667));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I20_MQXA",-49.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B2E20_MQXA",-50.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I10_MQXA",-53.3897));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B2E30_MQXA",-58.1397));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_TANC.4L5",-138.6703));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B1I10_TCTVA.4L5.B1",-144.5227));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B1I10_TCTH.4L5.B1",-146.6087));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_TCLP.4L5.B2",-150.9387));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_XRP",-153.4303));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I30_MQY",-162.0287));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E10_MQY_XRP",-164.5287));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I20_MQY",-166.8467));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E20_MQY",-168.0467));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I10_MQY",-170.5437));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E30_MQY",-174.5217));

  m_blmPositions.insert(std::make_pair("CMS.BCML1.1L5",-1.8));
  m_blmPositions.insert(std::make_pair("CMS.BCML2.1L5",-14.4));
  m_blmPositions.insert(std::make_pair("CMS.BCML1.1R5",1.8));
  m_blmPositions.insert(std::make_pair("CMS.BCML2.1R5",14.4));  

}

void
bril::bcmlprocessor::BCMLProcessor::publish_to_dip_blmSummary(){
  xdata::Table::Reference table(new xdata::Table);
  try{
    for( auto iter=std::begin(m_blmValues); iter!=std::end(m_blmValues); ++iter ){
      table->addColumn( iter->first, "double" );
      xdata::Double v=iter->second; 
      table->setValueAt( 0, iter->first,v );
    }
  }catch( xdata::exception::Exception& e){
    std::stringstream ss ;
    ss<< "Failed create blmSummary message table "<< e.what();
    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
    XCEPT_DECLARE_NESTED( bril::bcmlprocessor::exception::Exception,err,ss.str(), e);
  }
  std::string topicnameH( m_dipRoot.value_+"BRIL/BLMSummary");
  do_publish_to_dipbus( topicnameH, table);
  //m_blmValueParts = 0;
}

void
bril::bcmlprocessor::BCMLProcessor::publish_to_dip_blmHisto(){
 
  for ( auto iter = std::begin(m_blmValues); iter != std::end(m_blmValues); ++iter){
    
    auto iter1 = m_blmPositions.find(iter->first);
    if( iter1!=m_blmPositions.end() ){      
      int z;
      if( iter1->second<0 ){
	z = iter1->second-0.5;
      }else {
	z = iter1->second+0.5;
      }
      int bin = (NBIN/2) + z;
      if( bin>NBIN-1 ) bin = NBIN-1;
      if( bin<0 ) bin=0;
      m_histo[bin] = iter->second*1e9;
    }
  }
  xdata::Vector< xdata::Float > histValues;
  std::string diptopicnameH(m_dipRoot.value_+"BRIL/BLMSummary/Histo");
  for( size_t idx=0; idx<NBIN; ++idx ){
    xdata::Float v(m_histo[idx]);
    histValues.push_back( v );
  } 
  xdata::Table::Reference histTable( new xdata::Table );
  histTable->addColumn( "BLMHisto", "vector float" );
  histTable->setValueAt(0, "BLMHisto", histValues);
  do_publish_to_dipbus(diptopicnameH, histTable);
}
