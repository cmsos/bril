#ifndef _bril_bcmlprocessor_BCMLProcessor_h_
#define _bril_bcmlprocessor_BCMLProcessor_h_
#include <string>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/TimeVal.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "bril/bcmlprocessor/exception/Exception.h"

namespace bril{

  namespace bcmlprocessor{
    const int MAX_CARDS = 4;
    //BCM specifics
    const int MAX_CHANNELS = 16;
    const int NUMBER_RSUMS = 12;
    const int NUMBERTOEVENTING = 576;
    
    class BCMLProcessor : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      BCMLProcessor(xdaq::ApplicationStub* s);
      // destructor
      ~BCMLProcessor();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //timer callback
      void timeExpired( toolbox::task::TimerEvent& e );

      void handleDipMessage(const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist);      
    protected:
      xdata::UnsignedInteger32 m_fillnum;
      xdata::UnsignedInteger32 m_runnum;
      xdata::UnsignedInteger32 m_lsnum;
      xdata::UnsignedInteger32 m_nbnum;      
      xdata::UnsignedInteger32 m_sec;
      xdata::UnsignedInteger32 m_msec;
      xdata::String m_busName;
      xdata::String m_dipbusName;
      xdata::String m_diptopic;
      xdata::String m_dipRoot;
      xdata::String m_signalTopic;
      xdata::String m_dipServiceName;
      //registries
      std::vector< std::string > m_dipsubs;
      std::vector< std::string > m_dippubs; 

    private:
      // members
      // configuration parameters
      toolbox::BSem m_applock;
      
      bool m_busready_to_publish;
      bool m_dipbusready_to_publish;

      //Variables associated with the BCM task
    
      int64_t m_acqArray[MAX_CARDS][MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_acqEventing[NUMBERTOEVENTING];
      int64_t m_thresholdArray[MAX_CARDS][MAX_CHANNELS][NUMBER_RSUMS];
      bool m_abortChannelsArray[MAX_CARDS][MAX_CHANNELS];
      int64_t m_card2[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_card3[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_card4[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_acqCard1[192];
      int64_t m_acqCard2[192];
      int64_t m_acqCard3[192];

      int64_t m_dipDataReceived;
      int64_t m_bcmDipTime;

      int m_liveBCM1LChannels;
      int m_liveBCM2LChannels;
      int m_liveRS1Thresholds;
      int m_liveRS12Thresholds;

      xdata::Boolean m_BeamPermit;
      float m_RS1AbortFrac[MAX_CARDS][MAX_CHANNELS];
      float m_RS12AbortFrac[MAX_CARDS][MAX_CHANNELS];
      float m_bkgd1LminusRS1;
      float m_bkgd1LplusRS1;
      float m_bkgd2LminusRS1;
      float m_bkgd2LplusRS1;
      float m_bkgd1LminusRS12;
      float m_bkgd1LplusRS12;
      float m_bkgd2LminusRS12;
      float m_bkgd2LplusRS12;
      int64_t m_RS9L1Minus;
      int64_t m_RS9L1Plus;
      int64_t m_RS9L2Minus;
      int64_t m_RS9L2Plus;
      // PA are PercentageAbort variable, RS is the Unnormalized Running Sums
      float m_PA1_NB4_Filtered[48];
      //float m_PA1_NB64_Filtered[48];
      float m_PA1_NB4[48];
      //float m_PA1_NB64[48];
      int64_t m_RS9_NB4[48];
      //int64_t m_RS9_NB64[48];
      float m_PA12_NB4[48];
      //float m_PA12_NB64[48];
      bool m_lostFrames;
      bool m_thresholdsOK;
      int64_t m_timeLastDump;

      // variables to send to eventing for offline analysis
      unsigned int m_acq[576];
      unsigned int m_acqtsarray[3];
      int m_LostFrames[12];
      int m_nDump[6];
      bool m_BlecsDump;
      bool m_inAbort[48];
      
      xdata::InfoSpace *m_monInfoSpace;           
      xdata::Vector< xdata::Float > m_PA1;
      xdata::Float m_PA1_max;
      xdata::Vector< xdata::Float > m_PA1_Filt;
      xdata::Vector< xdata::Float > m_PA12;
      xdata::Vector< xdata::Float > m_RS9;
      xdata::Boolean m_lF;
      xdata::Boolean m_tOK;
      xdata::UnsignedInteger m_tLD;
      xdata::TimeVal m_acqTime;

      std::list<std::string> m_monItemList; 

      toolbox::mem::MemoryPoolFactory* m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      //BLM summary variables
      //xdata::UnsignedInteger m_blmValueParts;
      
      std::map< std::string, double > m_blmValues;
      std::map< std::string, float > m_blmPositions;
      std::map< std::string, int > m_blmEntries;
      std::array<double, 62> m_rs9_l;
      std::array<double, 62> m_rs8_l;
      std::array<double, 62> m_rs1_l;
      std::array<double, 62> m_rs9_r;
      std::array<double, 62> m_rs8_r;
      std::array<double, 62> m_rs1_r;
      const static int NBIN=350;
      float m_histo[NBIN];
      
      // private functions
      void publish_to_eventing();
      void publish_to_dip_Summary();
      void publish_to_dip_SourceTest(int cardid);
      void publish_to_dip_Background3();
      void publish_to_dip_HighChartsNB4();
      void publish_to_dip_MonitorRS9();
      void publish_to_dip_Acquisition();
      void publish_to_dip_blmSummary();
      void publish_to_dip_blmHisto();
      void publish_rs_to_eventing();
      void do_publish_to_dipbus(const std::string& dipname,xdata::Table::Reference dipmessage);
      void do_resetRSs();
      void do_resetbkgdVariables();
      void do_resetRS9Variables();
      void do_resetNB4Variables();

      void do_resetBLMSummaryVariables();
      void do_resetBLMSummaryPositions();
      //
      // nocopy protection
      BCMLProcessor(const BCMLProcessor&);
      BCMLProcessor& operator=(const BCMLProcessor&);
    };
  }
}
#endif
