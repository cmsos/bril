// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_webutils_version_h_
#define _bril_webutils_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILWEBUTILS_VERSION_MAJOR 3
#define BRIL_BRILWEBUTILS_VERSION_MINOR 0
#define BRIL_BRILWEBUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILWEBUTILS_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILWEBUTILS_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILWEBUTILS_VERSION_MAJOR,BRIL_BRILWEBUTILS_VERSION_MINOR,BRIL_BRILWEBUTILS_VERSION_PATCH)
#ifndef BRIL_BRILWEBUTILS_PREVIOUS_VERSIONS
#define BRIL_BRILWEBUTILS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILWEBUTILS_VERSION_MAJOR,BRIL_BRILWEBUTILS_VERSION_MINOR,BRIL_BRILWEBUTILS_VERSION_PATCH)
#else
#define BRIL_BRILWEBUTILS_FULL_VERSION_LIST BRIL_BRILWEBUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILWEBUTILS_VERSION_MAJOR,BRIL_BRILWEBUTILS_VERSION_MINOR,BRIL_BRILWEBUTILS_VERSION_PATCH)
#endif

namespace brilwebutils
{
        const std::string project = "bril";
	const std::string package = "brilwebutils";
	const std::string versions = BRIL_BRILWEBUTILS_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bhmsource";
	const std::string description = "bril webpage utilities";
	const std::string authors = "N. Tosi";
	const std::string link = "";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () /**throw (config::PackageInfo::VersionException) noexcept(false)*/;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
