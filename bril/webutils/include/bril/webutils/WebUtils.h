/**
 * @brief Webpage utility functions and highcharts based monitoring.
 *
 * @author N. Tosi \<nicolo.tosi@cern.ch\>
 *
 * This package includes various utility functions to create tables and plots in xdaq webpages.
 */

#include "bril/webutils/WebHelpers.h"
#include "bril/webutils/WebTables.h"
#include "bril/webutils/WebCharts.h"
