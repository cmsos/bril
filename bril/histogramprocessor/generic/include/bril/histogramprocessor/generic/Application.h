#ifndef _histogramprocessorgeneric_Application_h_
#define _histogramprocessorgeneric_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xgi/exception/Exception.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Vector.h"

#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"

#include "bril/webutils/WebUtils.h"
#include "bril/webutils/WebCharts.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/HistogramGenericTopics.hh"

namespace bril{
    namespace histogramprocessorgeneric {
        class Application:  public xdaq::Application,
                            public xgi::framework::UIManager,
                            public xdata::ActionListener,
                            public toolbox::ActionListener,
                            public eventing::api::Member {
            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);
                ~Application();
                // callback
                void onMessage( toolbox::mem::Reference*, xdata::Properties& );
                void actionPerformed( xdata::Event& ); // infospace event callback
                void actionPerformed( toolbox::Event& ); // toolbox event callback

                // web
                void Default(xgi::Input * in, xgi::Output * out);
                void requestRawData(xgi::Input * in, xgi::Output * out);
                void setupCharts();

            private:
                bool m_shutdown_request;
                toolbox::BSem m_appLock;
                toolbox::mem::Pool * m_memory_pool;

            private:
                // eventing
                xdata::Vector<xdata::Properties> m_config_channels;
                xdata::Vector<xdata::Properties> m_config_eventing_input;
                xdata::UnsignedInteger m_config_poll_interval;
                std::set<std::string> m_topiclist;
                toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>> m_inputQueue;
                toolbox::task::WorkLoop* m_inputprocess_wl;
                bool inputProcessJob (toolbox::task::WorkLoop* wl);
                void rawhistProcessJob (toolbox::mem::Reference* rawhist);

            private:
                // webcharts
                std::map<std::string, std::vector<uint32_t>> m_rawoccupancyData;
                webutils::WebChart* m_rawoccupancyChart;
                std::map<std::string, uint32_t> m_avgoccupancyCount;
                std::map<std::string, std::vector<float>> m_avgoccupancyData;
                webutils::WebChart* m_avgoccupancyChart;
                std::map<std::string, std::vector<float>> m_rateData;
                webutils::WebChart* m_rateChart;
                // fill
                template<typename DataT>
                void fillChartData (std::string type, std::vector<DataT> bufVec, uint32_t channelID, uint32_t algoID);

            protected:
                // these methods are detector specific
                virtual std::string getApplicationSummary() { return std::string("Histogram Processor Generic"); }
                virtual std::string getHistogramTopicname() { return interface::bril::histogramgeneric_rawhistT::topicname();}
                virtual std::string getHistogramPayloadDict() { return interface::bril::histogramgeneric_rawhistT::payloaddict();}
                const virtual interface::bril::histogramgeneric::HistogramGenericParametersT getHistogramParameters() { return interface::bril::HISTOGRAMGENERIC_PARAMETERS; }

            protected:
                Application(const Application&);
                Application& operator=(const Application&);

        };

    }
}

#endif