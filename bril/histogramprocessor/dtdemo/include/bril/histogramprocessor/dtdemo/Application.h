#ifndef _histogramprocessordtdemo_Application_h_
#define _histogramprocessordtdemo_Application_h_

#include "bril/histogramprocessor/generic/Application.h"
#include "interface/bril/HistogramDtDemoTopics.hh"

namespace bril{
    namespace histogramprocessordtdemo {

        class Application: public bril::histogramprocessorgeneric::Application {

            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);

            protected:
                // these methods are detector specific
                std::string getApplicationSummary() { return std::string("Histogram Processor DT Demonstrator"); }
                std::string getHistogramTopicname() { return interface::bril::dtdemo_rawhistT::topicname(); }
                std::string getHistogramPayloadDict() { return interface::bril::dtdemo_rawhistT::payloaddict(); }
                const virtual interface::bril::histogramgeneric::HistogramGenericParametersT getHistogramParameters() { return interface::bril::HISTOGRAMDTDEMO_PARAMETERS; }
        };

    }
}

#endif