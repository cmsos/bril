#ifndef _histogramprocessorot_Application_h_
#define _histogramprocessorot_Application_h_

#include "bril/histogramprocessor/generic/Application.h"
#include "interface/bril/HistogramOtTopics.hh"

namespace bril{
    namespace histogramprocessorot {

        class Application: public bril::histogramprocessorgeneric::Application {

            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);

            protected:
                // these methods are detector specific
                std::string getApplicationSummary() { return std::string("Histogram Processor OT"); }
                std::string getHistogramTopicname() { return interface::bril::ot_rawhistT::topicname(); }
                const virtual interface::bril::histogramgeneric::HistogramGenericParametersT getHistogramParameters() { return interface::bril::HISTOGRAMOT_PARAMETERS; }
                typedef interface::bril::ot_rawhistT rawhist_type;
        };

    }
}

#endif