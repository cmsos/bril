#ifndef _bril_histogramprocessorot_version_h_
#define _bril_histogramprocessorot_version_h_

#include "config/PackageInfo.h"

#define BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MAJOR 1
#define BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MINOR 0
#define BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_PATCH 0
#undef BRIL_BRILHISTOGRAMPROCESSOROT_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MAJOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MINOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_PATCH)
#ifndef BRIL_BRILHISTOGRAMPROCESSOROT_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMPROCESSOROT_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MAJOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MINOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_PATCH)
#else
#define BRIL_BRILHISTOGRAMPROCESSOROT_FULL_VERSION_LIST BRIL_BRILHISTOGRAMPROCESSOROT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MAJOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_MINOR, BRIL_BRILHISTOGRAMPROCESSOROT_VERSION_PATCH)
#endif

namespace histogramprocessorot
{
    const std::string project = "bril";
    const std::string package = "histogramprocessorot";
    const std::string versions = BRIL_BRILHISTOGRAMPROCESSOROT_FULL_VERSION_LIST;
    const std::string summary = "OT Processor";
    const std::string description = "A generic application to read based on histogramprocessorgeneric to read bril_histogram module";
    const std::string authors = "Mykyta Haranko";
    const std::string link = "";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif