import sys,re,os

'''
Get $project/$package name from Makefile Package=
Work in file $bril/$package/include/$project/$package/version.h:
replace 
   #define *_VERSION_MAJOR with #define $project_$project$package_VERSION_MAJOR 
   #define *_VERSION_MINOR with #define $project_$project$package_VERSION_MINOR
   #define *_VERSION_PATCH with #define $project_$project$package_VERSION_PATCH
   #define *_PREVIOUS_VERSIONS with #define $project_$project$package_PREVIOUS_VERSIONS
   #define *_VERSION_CODE PACKAGE_VERSION_CODE(*) with PACKAGE_VERSION_CODE($project_$project$package_VERSION_MAJOR,$project_$project$package_VERSION_MINOR,$project_$project$package_VERSION_PATCH)
   rewrite structure:
      #ifndef * \n
      #define * \n
      #else \n
      #define \n
      #endif \n
   to 
   #ifndef *_PREVIOUS_VERSIONS\n
   #define *_FULL_VERSION_LIST PACKAGE_VERSION_STRING($project_$project$package_VERSION_MAJOR,($project_$project$package_VERSION_MAJOR,($project_$project$package_VERSION_MINOR,$project_$project$package_VERSION_PATCH)  \n
   #else\n
   #define $project_$project$package_FULL_VERSION_LIST $project_$project$package_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING($project_$project$package_VERSION_MAJOR,$project_$project$package_VERSION_MINOR,$project_$project$package_VERSION_PATCH)\n
   #endif \n   

    const std::string versions = *_FULL_VERSION_LIST;
'''

def rewrite_versionh():
    currentdir = os.getcwd() 
    pieces = currentdir.split('/')
    project = pieces[-2]
    package = pieces[-1] 
    incpath = os.path.join('include',project,package)
    fname = os.path.join(incpath,'version.h')
    if not os.path.exists(fname) :
        print 'version.h does not exist in %s'%incpath
        return
    fobj = open(fname,'r')
    mytext = fobj.read()
    fobj.close()

    p00 = re.compile(r'#define\s+(.+)_VERSION_MAJOR\s+')
    #result = re.search(p00,mytext)
    #if result is not None:
    #    print result.groups()
    newphrase00 = '#define {project}_{project}{package}_VERSION_MAJOR '.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p00,newphrase00,mytext)
    
    p01 = re.compile(r'#define\s+(.+)_VERSION_MINOR\s+')
    result = re.search(p01,mytext)
    #if result is not None:
    #    print result.groups()
    newphrase01 = '#define {project}_{project}{package}_VERSION_MINOR '.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p01,newphrase01,mytext)

    p02 = re.compile(r'#define\s+(.+)_VERSION_PATCH\s+')
    result = re.search(p02,mytext)
    #if result is not None:
    #    print result.groups()
    newphrase02 = '#define {project}_{project}{package}_VERSION_PATCH '.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p02,newphrase02,mytext)

    p1 = re.compile(r'#define (.+_VERSION_CODE) (PACKAGE_VERSION_CODE\(.+\))\n')
    #result = re.search(p1,mytext)
    #if result is not None:
    #    print result.groups()
    newphrase1 = '#define {project}_{project}{package}_VERSION_CODE PACKAGE_VERSION_CODE({project}_{project}{package}_VERSION_MAJOR,{project}_{project}{package}_VERSION_MINOR,{project}_{project}{package}_VERSION_PATCH)\n'.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p1,newphrase1,mytext)

    #p2 = re.compile(r'#ifndef (.+_PREVIOUS_VERSIONS)\n#define (.+_FULL_VERSION_LIST PACKAGE_VERSION_STRING\(.+\))\n#else\n#define (\.+_FULL_VERSION_LIST \.+_PREVIOUS_VERSIONS \.+)\n#endif\n')
    p2 = re.compile(r'#ifndef (.+)_PREVIOUS_VERSIONS\n#define\s+(.+)_FULL_VERSION_LIST\s+PACKAGE_VERSION_STRING\(.+\)\n#else\n#define\s+(.+)_FULL_VERSION_LIST\s+(.+)_PREVIOUS_VERSIONS\s+","\s+PACKAGE_VERSION_STRING\((.+)\)\n')
    #result = re.search(p2,mytext)
    #if result is not None:
    #    print result.groups()
    newphrase2 = '#ifndef {project}_{project}{package}_PREVIOUS_VERSIONS\n#define {project}_{project}{package}_FULL_VERSION_LIST PACKAGE_VERSION_STRING({project}_{project}{package}_VERSION_MAJOR,{project}_{project}{package}_VERSION_MINOR,{project}_{project}{package}_VERSION_PATCH)\n#else\n#define {project}_{project}{package}_FULL_VERSION_LIST {project}_{project}{package}_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING({project}_{project}{package}_VERSION_MAJOR,{project}_{project}{package}_VERSION_MINOR,{project}_{project}{package}_VERSION_PATCH)\n'.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p2,newphrase2,mytext)

    p3 = re.compile(r'const std::string versions\s+=(.+)_FULL_VERSION_LIST;\n')
    #result = re.search(p3,mytext)
    #if result is not None:
    #    print result.groups()
    
    newphrase3 = 'const std::string versions = {project}_{project}{package}_FULL_VERSION_LIST;\n'.format(project=project.upper(),package=package.upper())
    mytext = re.sub(p3,newphrase3,mytext)
    new_fobj = open(fname+'.new','w')
    new_fobj.write(mytext)
    new_fobj.close()
    os.rename(fname,fname+'.old')
    os.rename(fname+'.new',fname)

def dirlist(topdir,exclusion):
    result = []
    dirlist = os.listdir(topdir)
    for d in dirlist:
        if d.find('.')!=-1:#exclude hidden directory
            continue
        if d in exclusion:#exclude exclusion
            continue
        result.append(d)
    return result

def main():
    #scan bril directory,exclude extern,scripts
    topdir = 'bril'
    exclusion = ['extern','scripts']
    dlist = dirlist(topdir,exclusion)
    currentdir = os.getcwd()
    for d in dlist:
        try:
            os.chdir(os.path.join(topdir,d))
        except OSError:
            continue
        rewrite_versionh()    
        os.chdir(currentdir) 
if __name__=='__main__':
    main()
