# variables
LIB_NAME=bril_histogram
GIT_URL=ssh://git@gitlab.cern.ch:7999/bril-phase2/bril_histogram.git
# select version tag
TAG="v1.0"

# script
if [ -d ${LIB_NAME} ]; then echo "First store all changes in ${LIB_NAME} folder and remove it" && return 1; fi
mkdir ${LIB_NAME}
cd ${LIB_NAME}/
git init
git remote add origin ${GIT_URL}
git config core.sparsecheckout true

# note folders to add
echo "components/bril_histogram/addr_table" >> .git/info/sparse-checkout
echo "components/bril_histogram/software/include" >> .git/info/sparse-checkout
echo "components/bril_histogram/software/src" >> .git/info/sparse-checkout

# continue script
git pull origin tag ${TAG} --depth=1
git checkout ${TAG}
cd ..
tar -czf ${LIB_NAME}.tar.gz ${LIB_NAME}
rm -rf ${LIB_NAME}
