#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "xdata/InfoSpaceFactory.h"
#include "b2in/nub/Method.h"
#include "bril/bcm1fsource/Application.h" 
#include "bril/bcm1fsource/exception/Exception.h"
#include <unistd.h>

//#include "interface/bril/BCM1FTopics.h"
#include "interface/bril/BCM1FTopics.hh" // NEW DATA FORMATS

XDAQ_INSTANTIATOR_IMPL (bril::bcm1fsource::Application)

bril::bcm1fsource::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&bril::bcm1fsource::Application::Default,"Default"); 
  b2in::nub::bind(this, &bril::bcm1fsource::Application::onMessage); 
  m_appInfoSpace = getApplicationInfoSpace();
  m_appDescriptor= getApplicationDescriptor();
  m_classname    = m_appDescriptor->getClassName();
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  m_instanceid = 0;
  if( m_appDescriptor->hasInstanceNumber() ){
    m_instanceid = m_appDescriptor->getInstance();        
  }
  toolbox::net::URN memurn("toolbox-mem-pool",m_classname.toString()+"_"+m_instanceid.toString()+"_mem"); 
  try{
    m_appInfoSpace->fireItemAvailable("simSource",&m_simSource);
    m_appInfoSpace->fireItemAvailable("signalTopic",&m_signalTopic);
    m_appInfoSpace->fireItemAvailable("bus",&m_bus);
    m_appInfoSpace->fireItemAvailable("rhuIpAddress",&m_rhuIpAddress);
    m_appInfoSpace->fireItemAvailable("rhuPort",&m_rhuPort);
    m_appInfoSpace->fireItemAvailable("topics",&m_topics);
    m_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool = m_poolFactory->createPool(memurn,allocator);
    xdata::UnsignedInteger instanceid = m_appDescriptor->getInstance();
    m_appURN.fromString(m_appDescriptor->getURN());
    m_readingwl = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_reading","waiting");
    m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_publishing","waiting");
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to set up InfoSpace ");
    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e)); 
    XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg,e);	
  }catch(xcept::Exception& e){
    std::string msg("Failed to setup memory pool ");
    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg,e);	
  }

  //m_rhu = 0; // OLD RHU INTERFACE

  // 
  // monitoring
  // 
  try{
    //URN(Universal Resource Name): urn:urnnamespace:specificname. Uniqueid to find myself in the universe
    std::string nidpull("bcm1fsourceMonPull");
    std::string nid("bcm1fsourceMon");

    // define monInfoSpacePull
    // from the web page, we want to see some information about this instance. If running as sim, we want to know
    // the timing signal; if running as real, we want to check the current ru queue size.
    std::string monurnPull = createQualifiedInfoSpace(nidpull).toString();
    m_monInfoSpacePull = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurnPull));
    m_histTimeOut = false;

    m_rhuTemperature = 0;
    m_nHistsReceived = 0;
    m_nHistsDiscarded = 0;

    m_RunNumber = 0;
    m_LumiSectionNumber = 0;
    m_LumiNibbleNumber = 0;
    m_FillNumber = 0;
    m_NumberOfOrbits.resize(8,0);

    //m_FullOrbitHistCounts = 0;

    m_monInfoSpacePull->fireItemAvailable("className",&m_classname);
    m_monItemListPull.push_back("className");
    m_monInfoSpacePull->fireItemAvailable("appURN",&m_appURN);
    m_monItemListPull.push_back("appURN");
    m_monInfoSpacePull->fireItemAvailable("rhuIpAddress",&m_rhuIpAddress);
    m_monItemListPull.push_back("rhuIpAddress");
    m_monInfoSpacePull->fireItemAvailable("rhuPort",&m_rhuPort);
    m_monItemListPull.push_back("rhuPort");
    m_monInfoSpacePull->fireItemAvailable("simSource",&m_simSource);
    m_monItemListPull.push_back("simSource");
    m_monInfoSpacePull->fireItemAvailable("signalTopic",&m_signalTopic);
    m_monItemListPull.push_back("signalTopic");
    m_monInfoSpacePull->fireItemAvailable("rhuTemperature",&m_rhuTemperature);
    m_monItemListPull.push_back("rhuTemperature");
    m_monInfoSpacePull->fireItemAvailable("histTimeOut",&m_histTimeOut);
    m_monItemListPull.push_back("histTimeOut");
    m_monInfoSpacePull->fireItemAvailable("nHistsReceived",&m_nHistsReceived);
    m_monItemListPull.push_back("nHistsReceived");
    m_monInfoSpacePull->fireItemAvailable("nHistsDiscarded",&m_nHistsDiscarded);
    m_monItemListPull.push_back("nHistsDiscarded");
    m_monInfoSpacePull->fireItemAvailable("RunNumber",&m_RunNumber);
    m_monItemListPull.push_back("RunNumber");
    m_monInfoSpacePull->fireItemAvailable("LumiSectionNumber",&m_LumiSectionNumber);
    m_monItemListPull.push_back("LumiSectionNumber");
    m_monInfoSpacePull->fireItemAvailable("LumiNibbleNumber",&m_LumiNibbleNumber);
    m_monItemListPull.push_back("LumiNibbleNumber");
    m_monInfoSpacePull->fireItemAvailable("FillNumber",&m_FillNumber);
    m_monItemListPull.push_back("FillNumber");
    m_monInfoSpacePull->fireItemAvailable("NumberOfOrbits",&m_NumberOfOrbits);
    m_monItemListPull.push_back("NumberOfOrbits");
    m_monInfoSpacePull->fireItemAvailable("FullOrbitHistCounts",&m_FullOrbitHistCounts);
    m_monItemListPull.push_back("FullOrbitHistCounts");
    // attach ourself as listener to batch retrieve event of this infospace
    m_monInfoSpacePull->addGroupRetrieveListener(this);

    //define monInfoSpacePush
    // we want to push the average of all bunches to the monitorable collector
    std::string monurnPush = createQualifiedInfoSpace(nid).toString();
    m_monInfoSpacePush = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurnPush));
    m_avgValue = 0.;
    m_monInfoSpacePush->fireItemAvailable("avgValue",&m_avgValue);
    m_monItemListPush.push_back("avgValue");
    m_monInfoSpacePush->fireItemAvailable("NumberOfOrbits",&m_NumberOfOrbits);
    m_monItemListPush.push_back("NumberOfOrbits");
    //In this case, we just push the mon items, we don't want to listen. so, no addxxxListener needed

  }catch(xdata::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to fire monitoring items";
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg.str(),e);
  }catch(xdaq::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to create infospace ";
    LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
    XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,stdformat_exception_history(e),e);
  }catch(std::exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),"std error "+std::string(e.what()));
    XCEPT_RAISE(bril::bcm1fsource::exception::Exception,e.what());
  }

}

bril::bcm1fsource::Application::~Application (){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear(); 

}

void bril::bcm1fsource::Application::Default (xgi::Input * in, xgi::Output * out){
  std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
  *out << "URL: "<< appurl;
  *out << cgicc::br();
  *out <<busesToHTML();
  if(m_simSource){
    *out << "We are publishing simulated data on frequency : "<< m_signalTopic.toString();    
  }else{
    *out << "We are publishing \"real\" data from RHU at " << m_rhuIpAddress.toString() 
	 << " (RHU port " << m_rhuPort << ")";
  }
  *out << cgicc::br();
  //
  // monitoring
  //
  // on every web page load/refresh, we fire ItemGroupRetrieve event asking for
  // updates of the monitoring items. It's called the "pull" mode since the
  // items are delivered only when asked for (pulled).
  //
  // update monitorables
  m_monInfoSpacePull->lock();
  m_monInfoSpacePull->fireItemGroupRetrieve(m_monItemListPull,this);
  m_monInfoSpacePull->unlock();
  // dump to web table
  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("monitoring items in monInfoSpacePull");
  *out << cgicc::tbody();
  
  *out << cgicc::tr();
  *out << cgicc::th("className");
  *out << cgicc::td( m_classname.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("appURN");
  *out << cgicc::td( m_appURN.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("simSource");
  *out << cgicc::td( m_simSource.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("signalTopic");
  *out << cgicc::td( m_signalTopic.toString());
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("rhuTemperature") ;
  *out << cgicc::td( m_rhuTemperature.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("histTimeOut") ;
  *out << cgicc::td( m_histTimeOut.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("nHistsReceived") ;
  *out << cgicc::td( m_nHistsReceived.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("nHistsDiscarded") ;
  *out << cgicc::td( m_nHistsDiscarded.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("RunNumber") ;
  *out << cgicc::td( m_RunNumber.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("LumiSectionNumber") ;
  *out << cgicc::td( m_LumiSectionNumber.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("LumiNibbleNumber") ;
  *out << cgicc::td( m_LumiNibbleNumber.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("FillNumber") ;
  *out << cgicc::td( m_FillNumber.toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("NumberOfOrbits in channel 0") ;
  *out << cgicc::td( ((xdata::UnsignedInteger*)m_NumberOfOrbits.elementAt(0))->toString() );
  *out << cgicc::tr();
  
  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();

  // Table of accumulated full-orbit counts for this nibble

  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("Histogram contents for this nibble");
  *out << cgicc::tbody();

  for (size_t channel = 0; channel < m_FullOrbitHistCounts.elements(); channel++)
  //for (xdata::UnsignedInteger channel = 0; channel < m_FullOrbitHistCounts.elements(); channel++)
    {
  
      xdata::UnsignedInteger chan(channel);

      *out << cgicc::tr();
      *out << cgicc::th( chan.toString() );
      xdata::UnsignedInteger * count = dynamic_cast<xdata::UnsignedInteger*>(m_FullOrbitHistCounts.elementAt(channel));
      *out << cgicc::td( count->toString() );
      *out << cgicc::tr();
  
    }

  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();

}

//
// action performed on XDATA::Event (configuration)
//
void bril::bcm1fsource::Application::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    //
    //monitoring
    //
    // set up the timer for pushing into monInfoSpacePush
    std::string timername(m_appURN.toString()+"_monpusher");
    try{
      toolbox::TimeInterval monpushInterval(10,0);// push every 10 seconds
      toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername);
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
      timer->scheduleAtFixedRate(start, this, monpushInterval, 0, timername);
    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer "<<timername;
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg.str(),e);
    }
    // end monitoring

    //
    // Starting up the data framework
    //
    this->getEventingBus(m_bus.value_).addActionListener(this);    
    if(m_simSource){
      LOG4CPLUS_INFO(getApplicationLogger(), std::string("I'm a simulator, subscribing to ")+m_signalTopic.toString()); 
      try{
	this->getEventingBus(m_bus.value_).subscribe(m_signalTopic.value_); // e.g. NB4
      }catch(eventing::api::exception::Exception& e){
	std::string msg("Failed to subscribe to eventing bus");
	msg+=m_bus.value_;
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg,e);	
	m_receiver.reset(); // to dispose the receiver
      }
    }else{
      LOG4CPLUS_INFO(getApplicationLogger(), std::string("I'm for real, connect to RHU"));
      //m_receiver = rhuclient::getRhuReceiver("10.176.132.79", 5001); // rhuboard03--cms; std::string address, int port
      // LOG4CPLUS_INFO(this->getApplicationLogger(), "Connecting to RHU...");
      // std::cout << "IP address is " << m_rhuIpAddress.toString() << " and rhuPort is 5001" << std::endl;
      
      m_receiver = rhuclient::getRhuReceiver(m_rhuIpAddress.toString(), m_rhuPort); // std::string address, int port

/*      // how to accept beam abort
      rhuclient::PControl control=rhuclient::getRhuControl(m_rhuIpAddress.toString());
      
      if (control->isConnected()) {
    	  control->acceptBeamAbort();
      }
  */
      // // Configure RHU
      // // Also make these configurable in xml file?
      // m_control = rhuclient::getRhuControl(m_rhuIpAddress.toString());
      // // * Set daq clock to (the opposite of) EXTERNAL (DOESN'T BEHAVE RELIABLY)
      // //    * =rhucfg --daqclock 1=
      // m_control->setClockSource(rhuclient::ClockSource::EXTERNAL);
      // // * Set lumi nibble boundary marker to 16 (works, or something)
      // //    * =rhucfg --nibbleboundary 16=
      // //m_control // UMMMMMMM, DOESN'T EXIST IN CONTROL INTERFACE?????  okay, won't set it now!
      // // * Set lumi nibble source to ttc (ALSO DOESN'T BEHAVE RELIABLY)
      // //    * =rhucfg --nibblesource 0=
      // // BUT RIGHT NOW OF COURSE SET IT TO INTERNAL THRESHOLD
      // m_control->setLumiNibbleTrigger(rhuclient::LumiNibbleTrigger::THRESHOLD);
      // // * Set running in simulator mode
      // //    * =rhucfg --simulation 1 --simmode 0 -m run=
      // // * OR set running in real mode
      // //    * =rhucfg --simulation 0 -m run=
      // m_control->setOperationMode(rhuclient::Mode::ACQUISITION);
      // m_control->setSimulatorEnable(false);
      // // Set edge-triggered mode
      // m_control->setSampleMode(rhuclient::SamplingMode::EDGE);

    }
    
    if(!m_topics.value_.empty()){
      m_topiclist = toolbox::parseTokenSet(m_topics.value_,",");
    }
    for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
      m_topicoutqueues.insert(std::make_pair(*it,new toolbox::squeue<toolbox::mem::Reference*>));
    }
    // end starting up data framework
  }
}

//
// action performed on TOOLBOX::Event (start workloop?) does this need to differentiate by sim/real?
// okay, actually it does, it only does anything if m_simSource is false
//
void bril::bcm1fsource::Application::actionPerformed(toolbox::Event& e){
  LOG4CPLUS_INFO(getApplicationLogger(), "Received toolbox event " << e.type());
  if ( e.type() == "eventing::api::BusReadyToPublish" ){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "Eventing bus '" << busname << "' is ready to publish";
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    // EVERYTHING IS INSIDE THIS IF STATEMENT
    if(!m_simSource){
      try{
	LOG4CPLUS_INFO(getApplicationLogger(), std::string("REAL mode, start reading and publishing workloops"));
	// bind method "reading" to reading workloop
	toolbox::task::ActionSignature* as_reading = toolbox::task::bind(this,&bril::bcm1fsource::Application::reading,"reading");
	// activate and submit workloop "reading"
	m_readingwl->activate();
	m_readingwl->submit(as_reading);	
      }catch(toolbox::task::exception::Exception& e){
	// Note: when caught an exception, if you don't handle it
	// you should rethrow either as the same type, or as a different type. 
	// Exceptions swallowed by you are hidden from upstream callers who
	// might be able to handle it.
	// Here we log the error then rethrow it as our own type 
	std::string msg("Failed to start workloop ");
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg,e);	
      }
    }
    // start publishing workloop in either mode
    try{
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::bcm1fsource::Application::publishing,"publishing");
      m_publishing->activate();
      m_publishing->submit(as_publishing);
    }catch(toolbox::task::exception::Exception& e){      
      std::string msg("Failed to start publishing workloop ");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bcm1fsource::exception::Exception,msg,e);	
    } 
  }
}

// generate simulated data on reception of the timing message
//void interface::bril::bcm1fsource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
void bril::bcm1fsource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic"); // e.g. NB1
    if(topic == m_signalTopic.toString()){
      // On receiving timing signal we generate simulated data 
      // THIS IS HOW YOU GET THE TIMING NUMBERS
      std::stringstream msg;
      if(ref!=0){	
	//parse timing signal message header to get timing info
	interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 
	unsigned int fillnum = thead->fillnum;
	unsigned int runnum = thead->runnum;
	unsigned int lsnum = thead->lsnum;
	unsigned int nbnum = thead->nbnum;
	msg<<"on timer "<<" fill "<<fillnum<<" run "<<runnum<<" ls "<<lsnum<<" nb "<<nbnum;
	LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
	for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
	  for(unsigned int ichannel=1;ichannel<=interface::bril::BCM1F_NRU;++ichannel){
	    toolbox::mem::Reference* outmessage = do_generatedata(*it,fillnum,runnum,lsnum,nbnum,ichannel);
	    msg.str("");
	    msg<<"push data to "<<*it<<" ichannel "<<ichannel;
	    LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
	    m_topicoutqueues[*it]->push(outmessage);		
	  }
	}
      }
    }
  }
  if(ref!=0){
    ref->release();
    ref=0;
  }
}

/** publish an array to bril b2in 
    if you publish to DIP, change here
*/
// This one is for simulated data, I think
toolbox::mem::Reference* bril::bcm1fsource::Application::do_generatedata(const std::string& topic,unsigned int fillnum, unsigned int runnum,unsigned int lsnum,unsigned int nbnum, unsigned int ichannel){
  // generate data per channel
  std::stringstream msg;
  msg<<"Generate data topic "<<topic<<" fill "<<fillnum<<"  run " << runnum << " ls "<<lsnum<<" nb "<<nbnum<<" channel "<<ichannel;
  LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
  toolbox::mem::Reference* hist_ref = 0;
  try{
    if(topic==interface::bril::bcm1fhistT::topicname()){ 
      hist_ref = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fhistT::maxsize()); 
      hist_ref->setDataSize(interface::bril::bcm1fhistT::maxsize()); 
      //prepare bcm1fhist head
      interface::bril::shared::DatumHead* hist_header = (interface::bril::shared::DatumHead*)(hist_ref->getDataLocation()); 
      // OLD DATA FORMAT
      // hist_header->setTime(runnum,lsnum,lsnum,nbnum,t.sec(),t.millisec());
      // hist_header->setResource(interface::bril::DataSource::BCM1F,0,0,ichannel,0,interface::bril::StorageType::UINT16);
      // hist_header->setFrequency(1,4);//I'm sending on NB1, hint to my collector to collect every 4 nb
      hist_header->setTime(fillnum,runnum,lsnum,nbnum,t.sec(),t.millisec());
      unsigned int channelid = ichannel + (m_instanceid << 3); 
      hist_header->setResource(interface::bril::shared::DataSource::BCM1F,0,channelid,interface::bril::shared::StorageType::UINT16); 
      hist_header->setFrequency(1);
      hist_header->setTotalsize(interface::bril::bcm1fhistT::maxsize()); 
      unsigned int nitems = interface::bril::bcm1fhistT::n(); 
      unsigned short channelhist[nitems];
      for(unsigned int i=0;i<nitems;++i){
	channelhist[i]=15;
      }    
      memcpy(hist_header->payloadanchor,channelhist,hist_header->payloadsize());
    }
  }catch(xcept::Exception& e){
    std::string msg("Failed to generate data ");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    if(hist_ref){
      hist_ref->release();
      hist_ref = 0;
    }
    XCEPT_DECLARE_NESTED(bril::bcm1fsource::exception::Exception,myerrorobj,msg,e);
    this->notifyQualified("fatal",myerrorobj);
  }
  return hist_ref;
}

// This one is for real data read from RHU
bool bril::bcm1fsource::Application::reading(toolbox::task::WorkLoop* wl){
  std::stringstream ss;
  // ss<<"Publish \"REAL\" data instanceid = "<<m_instanceid.value_;
  // LOG4CPLUS_INFO(getApplicationLogger(), ss.str());  

  bool histogram_timeout = false;

  // HERE IS WHERE THE "REAL" DATA COLLECTION GETS DONE
  try {
      m_receiver->receive(m_nibble, RHUTIMEOUT);
      m_nHistsReceived++;
  } catch (std::runtime_error const&) {
      ss << "Histogram receiving timed out on " << m_rhuIpAddress.toString();
      LOG4CPLUS_ERROR(this->getApplicationLogger(), ss.str());
      histogram_timeout = true;
      m_histTimeOut = true;
  }

  //  if(dataAddress){
  // if(dataAddress[0]>5&&dataAddress[0]<10){
  //   // we notify the external error collector(sentinel) that a hardware error occured. 
  //   // Note: here we don't throw. 
  //   //Because it's inside a workloop, no one can handle the error.
  //   // So we just notify the system and leave it at that.
  //   std::string msg("bunch 1 overflow");
  //   XCEPT_DECLARE(bril::bcm1fsource::exception::HardwareError,myerrorobj,msg);
  //   this->notifyQualified("fatal",myerrorobj);
  // }

  // put data into a "frame" to publish it?
  //    size_t datasize = m_fakeru->datasize();
  //    size_t datasize = sizeof(uint16_t)*4*3564;

  if (!histogram_timeout)
    {

      m_histTimeOut = false;
      toolbox::mem::Reference* hist_ref = 0;
      try{

	if (m_nibble.hasTcdsInfo())
	  {
	    rhuclient::TcdsInfo info=m_nibble.tcdsInfo();
	    m_RunNumber = info.cmsRunNumber;
	    m_LumiSectionNumber = info.sectionNumber;
	    m_LumiNibbleNumber = info.nibbleNumber;
	    m_FillNumber = info.lhcFillNumber;
	  }
	else
	  {
	    m_RunNumber = 9999;
	    m_LumiSectionNumber = 999;
	    m_LumiNibbleNumber = 999;
	    m_FillNumber = 9999;
	  }
      
	rhuclient::Status status = m_nibble.status();
	m_rhuTemperature = status.temperature;
      
	unsigned int fillnum = m_FillNumber;
	unsigned int runnum = m_RunNumber;
	unsigned int lsnum = m_LumiSectionNumber;
	unsigned int nbnum = m_LumiNibbleNumber;

	m_FullOrbitHistCounts.clear();

	for(unsigned int ichannel=1;ichannel<=interface::bril::BCM1F_NRU;++ichannel){//take 8 channels per instance, NRU = nchannels here, or 8 (per board)
	  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
	  hist_ref = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fhistT::maxsize());
	  hist_ref->setDataSize(interface::bril::bcm1fhistT::maxsize());
	  //prepare bcm1fhist head
	  interface::bril::shared::DatumHead* hist_header = (interface::bril::shared::DatumHead*)(hist_ref->getDataLocation());
	  hist_header->setTime(fillnum,runnum,lsnum,nbnum,t.sec(),t.millisec());
	  unsigned int channelid = ichannel + (m_instanceid << 3);
	  unsigned int algorithm = 999;
	  //BCM1FHistAlgos_ENUM algorithm;
	  //interface::bril::BCM1FHistAlgos_ENUM algorithm;
	  //interface::bril::BCM1FHistAlgos::lookuptable algorithm;
	  //interface::bril::BCM1FHistAlgos::LookupItem algorithm;
	  //interface::bril::BCM1FHistAlgos algorithm;
	  //interface::bril::BCM1FHistAlgos::XX algorithm;
	  if (m_instanceid.value_ < 6)
	    algorithm = interface::bril::BCM1FHistAlgos::OC;
	  else if (m_instanceid.value_ == 6)
	    algorithm = interface::bril::BCM1FHistAlgos::COMB;
	  else if (m_instanceid.value_ == 7)
	    algorithm = interface::bril::BCM1FHistAlgos::TT;
	  // else
	  //   {
	  //     LOG4CPLUS_WARN(this->getApplicationLogger(), "RHU index not understood!");
	  //     algorithm = NULL;
	  //   }
	  //hist_header->setResource(interface::bril::DataSource::BCM1F,0,channelid,interface::bril::StorageType::UINT16); 
	  hist_header->setResource(interface::bril::shared::DataSource::BCM1F,algorithm,channelid,interface::bril::shared::StorageType::UINT16); 
	  hist_header->setFrequency(1);
	  hist_header->setTotalsize(interface::bril::bcm1fhistT::maxsize());
	  //prepare hist payload
	  // publish one full-orbit histogram for each channel
	  unsigned int nitems = interface::bril::bcm1fhistT::n(); // nbinsperbx*nbx, I think, so total # of bins 
	  //unsigned int nchannels = 8;
	  //      unsigned int nbinsperbx = 4; // take value from data format file!
	  //      unsigned int nbins = nitems/nbinsperbx;
	  unsigned short channelhist[nitems]; //need to be unsigned short! because you declared so.
	  rhuclient::Histogram histogram = m_nibble.histogram(ichannel-1);
	  if (nitems != rhuclient::HIST_MAX_BIN_COUNT)
	    {
	      LOG4CPLUS_WARN(this->getApplicationLogger(), "Indices aren't right!");
	      ss.clear(); ss.str("");
	      ss<< "rhuclient::HIST_MAX_BIN_COUNT is " << rhuclient::HIST_MAX_BIN_COUNT
		<< ", nitems is " << nitems
		<< ", histogram.bincount is " << histogram.bincount;
	      LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
	    }

	  // I THINK THIS MESSES THINGS UP WHEN THE FIRST NIBBLE OF THE RUN IS SHORTER THAN 4096 ORBITS
	  // if (histogram.orbits < 4095) // safety margin of 1 orbit
	  //   {
	  //     LOG4CPLUS_INFO(this->getApplicationLogger(), "Too few orbits this nibble!  Discarding histogram, continuing...");
	  //     m_nHistsDiscarded++;
	  //     return true;
	  //   }

	float scaleFactor = 1.;
	unsigned int channelTotal = 0;
	m_NumberOfOrbits[ichannel-1] = histogram.orbits;
	if (histogram.orbits < 4096)
	  {
	    ss.clear(); ss.str("");
	    ss<< "Fewer than 4096 orbits this nibble! Scaling to nOrbits: " << histogram.orbits;
	    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
	    scaleFactor = 4096./((float) histogram.orbits);
	  }
	  for(unsigned int bin=0;bin<nitems;++bin)
	    {
	      // use m_nibble
	      float binsFloat = (float) histogram.bins[bin];
	      float valueTemp = scaleFactor*binsFloat;
	      channelhist[bin] = (unsigned short) valueTemp;
	      channelTotal = channelTotal + (unsigned int) channelhist[bin];
	    }

	  xdata::UnsignedInteger total(channelTotal);
	  m_FullOrbitHistCounts.push_back(total);
	
	  memcpy(hist_header->payloadanchor,channelhist,hist_header->payloadsize());
	  //push to output queue
	  m_topicoutqueues[interface::bril::bcm1fhistT::topicname()]->push(hist_ref);
	} // end loop channels
	// TRY MONITORING AND PUBLISH, OTHERWISE ERRORS
      }catch(toolbox::mem::exception::Exception& e){
	std::string msg("Failed to obtain buffer from memory pool ");
	XCEPT_DECLARE_NESTED(bril::bcm1fsource::exception::Exception,myerrorobj,msg,e);
	this->notifyQualified("fatal",myerrorobj);
      }
    } // end if (!histogram_timeout)

  try{
    m_monInfoSpacePush->fireItemGroupChanged(m_monItemListPush,this);
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to push monitoring items ");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
  }
  ss.clear(); ss.str();
  ss << "nOrbits in ch 0 is " << m_NumberOfOrbits[0].value_
    <<". Publish \"REAL\" data instanceid = "<<m_instanceid.value_;

  LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());  



  // return true asks the workloop coming back for more work until canceled.
  return true;
}

//
// monitoring
//
void bril::bcm1fsource::Application::timeExpired(toolbox::task::TimerEvent& e){
  try{
    m_monInfoSpacePush->fireItemGroupChanged(m_monItemListPush,this);
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to push monitoring items ");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
  }
}



void bril::bcm1fsource::Application::do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef){
  std::stringstream msg;
  try{
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    msg<<"Publishing "<<busname<<":"<<topicname;
    LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
    this->getEventingBus(busname).publish(topicname,bufRef,plist);    
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<busname;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
    XCEPT_DECLARE_NESTED(bril::bcm1fsource::exception::Exception,myerrorobj,msg.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }
  // if(bufRef){
  //   bufRef->release();
  //   bufRef = 0;
  // }
}

bool bril::bcm1fsource::Application::publishing(toolbox::task::WorkLoop* wl){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    if(!it->second->empty()) {
      std::string topicname = it->first;
      toolbox::mem::Reference* data = it->second->pop();
      do_publish(m_bus.value_,topicname,data);
    }    
  }
  return true;
}

