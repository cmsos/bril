/*
 * types.h
 *
 *  Created on: 23.12.2010
 *      Author: marekp
 */

/* #ifndef TOOLS_TYPES_H_ */
/* #define TOOLS_TYPES_H_ */

#ifndef TYPES_H_
#define TYPES_H_

#ifndef SWIG
// Include size_t
#ifdef __cplusplus
#include <cstddef>
#else
#include <sys/mman.h>
#endif
#else
// define the size_t
typedef long int size_t;
#endif

#undef u8_t
#undef u16_t
#undef uint8_t
#undef uint16_t
#undef uint32_t
#undef int8_t
#undef int16_t
#undef int32_t
#undef u64_t

#ifndef u8_t
typedef unsigned char u8_t;
#endif
#ifndef u16_t
typedef unsigned short u16_t;
#endif
#ifndef u32_t
typedef unsigned int u32_t;
#endif
#ifndef u64_t
typedef unsigned long long u64_t;
#endif

typedef unsigned char uint8;
typedef unsigned shortuint16;
typedef unsigned intuint32;
typedef char int8;
typedef short int16;
typedef int int32;

//typedef unsigned intaddr32_t;
//typedef unsigned intoffset32_t;

typedef size_t address_t;
#ifndef addr_t
typedef size_t addr_t;
#endif
typedef int offset_t;
#ifndef offs_t
typedef int offs_t;
#endif

typedef unsigned char data8_t;
typedef unsigned short data16_t;
typedef unsigned int data32_t;
#ifdef __cplusplus
typedef bool bit_t;
#else
typedef unsigned int bit_t;
#endif

static inline void* PTR_OFFS(void* ptr, size_t offs)
{
  return (void*)((size_t)ptr + offs);
}


#endif /* TOOLS_TYPES_H_ */
