/***************************************************************************** 
 * File  bits.h
 * created on 30.05.2011
 *****************************************************************************
 * Author:Dipl.Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/


#ifndef __TOOLS_BITS_HPP__
#define __TOOLS_BITS_HPP__

#include "types.h"

namespace tools {


  template <class TYPE>
  static inline TYPE setBit(TYPE value, u8_t index)
  {
    return (value | (1<<index));
  }

  template <class TYPE, class TYPE2>
  static inline TYPE setBits(TYPE value, TYPE2 mask)
  {
    return (value | mask);
  }


  template <class TYPE>
  static inline TYPE clrBit(TYPE value, u8_t index)
  {
    return (value & ~(1<<index));
  }

  template <class TYPE, class TYPE2>
  static inline TYPE clrBits(TYPE value, TYPE2 mask)
  {
    return (value & ~(mask));
  }

  template <class TYPE>
  static inline TYPE setBit(TYPE value, u8_t index, bit_t bit)
  {
    if (bit) return setBit<TYPE>(value, index); else return clrBit<TYPE>(value, index);
  }

  template <class TYPE>
  static inline bool getBit(TYPE value, u8_t index)
  {
    return (value & (1<<index));
  }


  // flips a 8 bit Word
  static inline u8_t flip8(u8_t data)
  {
    data = ((data & 0xF0) >> 4) | ((data & 0x0F) << 4);
    data = ((data & 0xCC) >> 2) | ((data & 0x33) << 2);
    data = ((data & 0xAA) >> 1) | ((data & 0x55) << 1);
    return data;
  }

  // flips a 16 bit Word
  static inline u16_t flip16(u16_t data)
  {
    data = ((data & 0xFF00) >> 8) | ((data & 0x00FF) << 8);
    data = ((data & 0xF0F0) >> 4) | ((data & 0x0F0F) << 4);
    data = ((data & 0xCCCC) >> 2) | ((data & 0x3333) << 2);
    data = ((data & 0xAAAA) >> 1) | ((data & 0x5555) << 1);
    return data;
  }

  // flips a 32 bit Word
  static inline u32_t flip32(u32_t data)
  {
    data = ((data & 0xFFFF0000) >> 16) | ((data & 0x0000FFFF) << 16);
    data = ((data & 0xFF00FF00) >>  8) | ((data & 0x00FF00FF) <<  8);
    data = ((data & 0xF0F0F0F0) >>  4) | ((data & 0x0F0F0F0F) <<  4);
    data = ((data & 0xCCCCCCCC) >>  2) | ((data & 0x33333333) <<  2);
    data = ((data & 0xAAAAAAAA) >>  1) | ((data & 0x55555555) <<  1);
    return data;
  }

} // tools

#endif /* BITS_HPP_ */
