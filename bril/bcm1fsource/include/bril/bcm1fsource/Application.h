#ifndef _bril_bcm1fsource_Application_h_
#define _bril_bcm1fsource_Application_h_
#include <string>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Float.h"
#include "xdata/Vector.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/squeue.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"

#include "rhuclient/receiver.hpp"
#include "rhuclient/data.hpp"
#include "rhuclient/control.hpp"
/**
   A xdaq application that when configured as simulation mode simulate data on bril timing signal; in real mode, it interacts with an external library in a workloop, sends data to bril whenever the readout queue has data. It uses an external library for readout(histograming unit or dip client)

   1. Configurable
   2. Has a web page
   3. In sim mode, subscribe to bril timing signal and generate simulated data
   4. In real mode, fetch data from fakeru readout queue in a workloop
   5. Publish data to bril eventing
   6. Notify external observer when a hardware error happens
   7. Compiletime linked to external libfakeru.so
*/

//forward declaration
namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}
/* namespace fake{ */
/*   class FakeRU; */
/* } */

/*namespace rhu{*/
/*     namespace shm{*/
/*     class Client;*/
/*    } */
/* } */

namespace bril{
  namespace bcm1fsource{
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener, public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      // timer callback 
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
    private:
      const int RHUTIMEOUT = 5000;  // ms

      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      xdata::String m_classname;
      xdata::String m_appURN;
      xdata::UnsignedInteger m_instanceid;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      // configuration parameters
      xdata::Boolean m_simSource;
      xdata::String m_signalTopic;
      xdata::String m_bus;
      xdata::String m_rhuIpAddress;
      xdata::UnsignedInteger m_rhuPort;
      xdata::String m_topics; 
      std::set<std::string> m_topiclist;
      // real mode      
      // outgoing queue
      typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > QueueStore;
      typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > m_topicoutqueues;

      // reading workloop
      toolbox::task::WorkLoop* m_readingwl;
      // publishing workloop
      toolbox::task::WorkLoop* m_publishing;

      // fakeru handle
      //      fake::FakeRU* m_fakeru;
      // rhu::shm::Client* m_rhu; // OLD RHU INTERFACE
      rhuclient::PReceiver m_receiver;
      rhuclient::PControl m_control;
      rhuclient::LumiNibble m_nibble;

      // monitoring
      // Note: monitorables must be of xdata types, headers can be found in daq/xdata
      xdata::InfoSpace *m_monInfoSpacePull;
      //      xdata::UnsignedInteger m_ruqueueSize;
      std::list<std::string> m_monItemListPull; //monInfoSpacePull metric

      xdata::Boolean m_histTimeOut;

      xdata::UnsignedInteger m_rhuTemperature;
      xdata::UnsignedInteger m_nHistsReceived;
      xdata::UnsignedInteger m_nHistsDiscarded;

      xdata::UnsignedInteger m_RunNumber;
      xdata::UnsignedInteger m_LumiSectionNumber;
      xdata::UnsignedInteger m_LumiNibbleNumber;
      xdata::UnsignedInteger m_FillNumber;

      xdata::Vector<xdata::UnsignedInteger> m_FullOrbitHistCounts;
      xdata::Vector<xdata::UnsignedInteger> m_NumberOfOrbits;

      xdata::InfoSpace *m_monInfoSpacePush;
      xdata::Float m_avgValue;
      std::list<std::string> m_monItemListPush; //monInfoSpacePush metric

      // application lock, it's useful to always include one
      toolbox::BSem m_applock;

    private:
      // methods
      toolbox::mem::Reference* do_generatedata(const std::string& topic, unsigned int fillnum, unsigned int runnum,unsigned int lsnum,unsigned int nbnum, unsigned int ichannel);
      void do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef);
      // workloop worker methods
      bool reading(toolbox::task::WorkLoop* wl);
      bool publishing(toolbox::task::WorkLoop* wl);
      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
