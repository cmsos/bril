#include "bril/lumistore/OutputFile.h"
#include "bril/lumistore/OutputFileConfig.h"
#include "bril/lumistore/OutputFileStatus.h"
#include "bril/lumistore/StorageUnit.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "toolbox/TimeVal.h"
#include <sstream>
#include <iomanip>
#include <iostream>
namespace bril{ namespace lumistore{
    std::string OutputFile::makeFilename(const std::string& filepath,const std::string& basename,const std::string& suffix,int filecount){
      std::string result;
      toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
      if(!filepath.empty()) {
	result += filepath;
	if( *filepath.rbegin() !='/'){
	  result.push_back('/');
	}
      }
      std::string datestr = now.toString("%y%m%d%H%M",toolbox::TimeVal::gmt);
      result += basename+"_"+datestr;
      std::stringstream ss;
      ss<<filecount;    //total filecounts as long as process alive
      result += "_"+ss.str();
      result += suffix;
      return result;
    }

    OutputFile::OutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* fstatus):m_fconfig(fconfig),m_status(fstatus){
      m_status->filename = OutputFile::makeFilename(fconfig->filepath,basename,fconfig->suffix,fstatus->filecount);
      m_status->isopen= true;
    }

    OutputFile::~OutputFile(){
      if( !m_status->isopen ) return;
      close(OutputFileStatus::force);	
    }

    void OutputFile::close(const OutputFileStatus::ClosingReason r){
      if( !m_status->isopen ) return;
      m_status->isopen = false;
      m_status->closereason = r;
      m_status->filecount++;
      //m_status->filesize = do_getfilesize();
      m_status->datasize = 0;
      do_close();
    }

    void OutputFile::write(const StorageUnit* u){
      do_writeStorageUnit(u);
      if(m_status->nentries==0){
	m_status->firstrun = u->dataheader()->runnum;
	m_status->firstentrysec = toolbox::TimeVal::gettimeofday().sec();
      }
      m_status->lastentrysec = toolbox::TimeVal::gettimeofday().sec();
      ++m_status->nentries;
      m_status->datasize += (unsigned long long)u->totalDataSize();
    }

    bool OutputFile::tooOld(unsigned int nowsec) const{
      return (nowsec-m_status->lastentrysec) > m_fconfig->maxagesec;
    }

    bool OutputFile::tooLarge(unsigned int datasize) const {
      if((m_status->datasize+(unsigned long long)datasize) > m_fconfig->maxfilesize) return true;
      return false;
    }
    
    size_t OutputFile::filesize() const{
      if( !m_status->isopen ) return 0;
      return do_getfilesize();
    }

}}
