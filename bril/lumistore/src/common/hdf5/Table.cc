#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/Table.h"
#include "bril/lumistore/hdf5/Row.h"
#include "bril/lumistore/hdf5/H5Utils.h"
#include "hdf5_hl.h"
#include <stdlib.h>
#include <string.h>
#include <iostream>
namespace bril{ namespace lumistore { namespace hdf5{
      Table::Table(const std::string& parentpath, hid_t parentid, const std::string& name, File* file,size_t datacolumnlen, hid_t datacolumnhtype,size_t nrows, bool shrink):Node(parentpath,parentid,name,file),m_datacolumnlen(datacolumnlen),m_datacolumnhtype(datacolumnhtype),m_nrows(nrows),m_shrink(shrink){
	size_t itemsize = H5Tget_size(datacolumnhtype);
	if(itemsize<=0){
	  std::cout<<"wrong data type"<<std::endl;
	}
	m_datacolumnsize = m_datacolumnlen*itemsize;
	m_rowsize = sizeof(Row)-sizeof(char)*4+m_datacolumnsize;
	m_rowtype = 0;
	m_wbuf = malloc(m_rowsize*m_nrows);
	//memset(m_wbuf, 0xff, sizeof(m_wbuf));
        memset(m_wbuf, 0xff, m_rowsize*m_nrows);
	m_current = 0;
	m_rowsinbuffer = 0;
	m_committedrows = 0;
	m_committedtimes = 0;
	m_allocatedrows = 0;	
	m_id = create_table();
	m_isopen = true;
      }
      Table::~Table(){
	close();
      }
      void Table::append(const interface::bril::shared::DatumHead* datahead, unsigned char* payloadaddress){
	char* buffc = (char*)m_wbuf;
	m_current = (Row*)(buffc+m_rowsize*m_rowsinbuffer);
	m_current->fillnum = datahead->fillnum;
	m_current->runnum = datahead->runnum;
	m_current->lsnum = datahead->lsnum;
	m_current->nbnum = datahead->nbnum;
	m_current->timestampsec = datahead->timestampsec;
	m_current->timestampmsec = datahead->timestampmsec;
	m_current->publishnnb = datahead->publishnnb;
	m_current->datasourceid = datahead->datasourceid;
	m_current->algoid = datahead->algoid;
	m_current->channelid = datahead->channelid;	
	memcpy(m_current->data,(void*)payloadaddress,m_datacolumnsize);
	m_rowsinbuffer++;
	if(m_rowsinbuffer>=m_nrows){ 
	  flush();
	}
      }
      void Table::close(){
	if(!m_isopen) return;
	if(m_rowsinbuffer>0) flush();
	if(m_shrink){
	  hsize_t dims[1];
	  dims[0] = m_committedrows ;
	  H5Dset_extent(m_id,dims);
	}
	H5Dclose(m_id);
	free(m_wbuf);
	m_isopen = false;
      }
      hid_t Table::create_table(){
	m_rowtype = H5Tcreate(H5T_COMPOUND,m_rowsize);
	H5Tinsert(m_rowtype,"fillnum", HOFFSET(Row,fillnum), H5T_NATIVE_UINT); 
	H5Tinsert(m_rowtype,"runnum",HOFFSET(Row,runnum), H5T_NATIVE_UINT);
	H5Tinsert(m_rowtype,"lsnum",HOFFSET(Row,lsnum), H5T_NATIVE_UINT);
	H5Tinsert(m_rowtype,"nbnum", HOFFSET(Row,nbnum), H5T_NATIVE_UINT);
	H5Tinsert(m_rowtype,"timestampsec", HOFFSET(Row,timestampsec), H5T_NATIVE_UINT);
	H5Tinsert(m_rowtype,"timestampmsec",HOFFSET(Row,timestampmsec), H5T_NATIVE_UINT);
        H5Tinsert(m_rowtype,"publishnnb", HOFFSET(Row,publishnnb), H5T_NATIVE_UCHAR);
	H5Tinsert(m_rowtype,"datasourceid", HOFFSET(Row,datasourceid), H5T_NATIVE_UCHAR);
	H5Tinsert(m_rowtype,"algoid",HOFFSET(Row,algoid), H5T_NATIVE_UCHAR);
	H5Tinsert(m_rowtype,"channelid", HOFFSET(Row,channelid), H5T_NATIVE_UCHAR);
	
	hsize_t arraydims[1]; arraydims[0]=m_datacolumnlen;
	hid_t dataid = H5Tarray_create(m_datacolumnhtype,1,arraydims);
	H5Tinsert(m_rowtype, "data", HOFFSET(Row, data), dataid);
	H5Tclose(dataid); 
	// H5TBOmake_table( hid_t loc_id,const char* dset_name,hid_t type_id,hsize_t nrecords,hsize_t chunk_size,void *fill_data,const void *data )

	hid_t tableid = H5TBOmake_table(m_parentid,m_name.c_str(),m_rowtype,0,m_nrows,0,0);
	if(tableid<0) {
	  std::cout<<"failed to create datatable"<<std::endl;
	  return 0;
	}

	H5LTset_attribute_string(tableid,m_pathname.c_str(),"CLASS","TABLE");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"TITLE",m_name.c_str());
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"VERSION","2.7");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_0_NAME","runnum");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_1_NAME","lsnum");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_2_NAME","cmslsnum");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_3_NAME","nbnum");
        H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_4_NAME","publishnnb");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_5_NAME","timestampsec");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_6_NAME","timestampmsec");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_7_NAME","algoid");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_8_NAME","ruid");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_9_NAME","groupid");
	H5LTset_attribute_string(tableid,m_pathname.c_str(),"FIELD_10_NAME","data");	
	return tableid; 
      }

      void Table::flush(){
	// signature: H5TBOappend_records( hid_t dataset_id, hid_t mem_type_id,hsize_t nrecords,hsize_t nrecords_orig,const void *data )
	size_t previouscommitted = m_committedtimes*m_nrows;
	//std::cout<<"Table::flush "<<"nrows "<<m_nrows<<" m_rowsinbuffer "<<m_rowsinbuffer<<" m_committedtimes  "<<m_committedtimes<<" previouscommitted "<<previouscommitted<<std::endl;
	herr_t status = H5TBOappend_records(m_id,m_rowtype,m_rowsinbuffer,previouscommitted,m_wbuf);
	if(status<0){
	  std::cout<<"failed to write to disk"<<std::endl;
	  return;
	}
	m_allocatedrows += m_nrows;
	m_committedrows += m_rowsinbuffer;
	m_committedtimes ++;
	m_rowsinbuffer = 0;
      }
}}}
