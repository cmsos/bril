#include "bril/lumistore/hdf5/Node.h"
#include "bril/lumistore/hdf5/File.h"
namespace bril{ namespace lumistore{ namespace hdf5{
      Node::Node(const std::string& parentpath, hid_t parentid, const std::string& name,File* file):m_parentpath(parentpath),m_parentid(parentid), m_name(name),m_file(file),m_id(0),m_isopen(false){
	if(*parentpath.rbegin() != '/'){
	  m_parentpath.push_back('/');
	}
	m_pathname = m_parentpath+m_name;
      }
      Node::~Node(){}
      hid_t Node::id()const{
	return m_id;
      }
      bool  Node::isopen() const{
	return m_isopen;
      }
      std::string Node::pathname() const{
	return m_pathname;
      }
      std::string Node::parentpath() const{
	return m_parentpath;
      }
      File* Node::file(){
	return m_file;
      }
}}}
