#include "bril/lumistore/hdf5/H5Utils.h"
#include "hdf5_hl.h"

namespace bril{ namespace lumistore{ namespace hdf5{
     
herr_t H5TBOmake_table( hid_t loc_id,
                        const char* dset_name,
                        hid_t type_id,
                        hsize_t nrecords,
                        hsize_t chunk_size,
                        void  *fill_data,
                        const void *data )
{

 hid_t   dataset_id;
 hid_t   space_id;
 hid_t   plist_id;
 hsize_t dims[1];
 hsize_t dims_chunk[1];
 hsize_t maxdims[1] = { H5S_UNLIMITED };
 

 dims[0]       = nrecords;
 dims_chunk[0] = chunk_size;

 /* Create a simple data space with unlimited size */
 if ( (space_id = H5Screate_simple( 1, dims, maxdims )) < 0 ) return -1;

 /* Modify dataset creation properties, i.e. enable chunking  */
 plist_id = H5Pcreate (H5P_DATASET_CREATE);
 if ( H5Pset_chunk ( plist_id, 1, dims_chunk ) < 0 ) return -1;

 // if ( H5Pset_fill_time(plist_id, H5D_FILL_TIME_ALLOC) < 0 ) return -1;
 /* Set the fill value using a struct as the data type. */
 //if ( fill_data) {
 //  if ( H5Pset_fill_value( plist_id, type_id, fill_data ) < 0 ) return -1;
 //} else {
 //  if ( H5Pset_fill_time(plist_id, H5D_FILL_TIME_ALLOC) < 0 ) return -1;
 //}
 
 /* Create the dataset. */
 if ( (dataset_id = H5Dcreate( loc_id, dset_name, type_id, space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT )) < 0 ) goto out;

 /* Only write if there is something to write */
 if ( data ) {
   /* Write data to the dataset. */
   if ( H5Dwrite( dataset_id, type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data ) < 0 ) goto out;
   
 }
 
 /* Terminate access to the data space. */
 if ( H5Sclose( space_id ) < 0 ) goto out;

 /* End access to the property list */
 if ( H5Pclose( plist_id ) < 0 ) goto out;

 /* Return the object unique ID for future references */
 return dataset_id;
 
 /* error zone, gracefully close */
 out:
 H5E_BEGIN_TRY {
   H5Dclose(dataset_id);
   H5Sclose(space_id);
   H5Pclose(plist_id);
 } H5E_END_TRY;
 return -1;
 
}

 /*-------------------------------------------------------------------------
 * Function: H5TBOappend_records
 *
 * Purpose: Appends records to a table
 * Comments: Uses memory offsets
 *-------------------------------------------------------------------------
 */

herr_t H5TBOappend_records( hid_t dataset_id,
                            hid_t mem_type_id,
                            hsize_t nrecords,
                            hsize_t nrecords_orig,
                            const void *data )
{
 hid_t    space_id = -1;       
 hsize_t  count[1];
 hsize_t  offset[1];
 hid_t    mem_space_id = -1;   
 hsize_t  dims[1];

 /* Extend the dataset */
 dims[0] = nrecords_orig;
 dims[0] += nrecords;
 if ( H5Dset_extent(dataset_id, dims) < 0 ) return -1;

 /* Create a simple memory data space */
 count[0]=nrecords;
 if ( (mem_space_id = H5Screate_simple( 1, count, NULL )) < 0 ) return -1;

 /* Get the file data space */
 if ( (space_id = H5Dget_space(dataset_id)) < 0 ) return -1;

 /* Define a hyperslab in the dataset */
 offset[0] = nrecords_orig;
 if ( H5Sselect_hyperslab( space_id, H5S_SELECT_SET, offset, NULL, count, NULL) < 0 ) return -1;

 if ( H5Dwrite( dataset_id, mem_type_id, mem_space_id, space_id, H5P_DEFAULT, data ) < 0 ) return -1;

 /* Terminate access to the dataspace */
 if ( H5Sclose( mem_space_id ) < 0 ) return -1;
 if ( H5Sclose( space_id ) < 0 ) return -1;
return 0;

}
      
}}}
