#include "bril/lumistore/hdf5/H5RowDef.h"
#include "hdf5_hl.h"
#include <cstdlib>
#include <stdio.h>
//#include <iostream>
//#include <sstream>
namespace bril{ namespace lumistore { namespace hdf5{
      H5RowDef::H5RowDef(const std::string& rowdict):m_comp(rowdict){
	size_t ncolumns = m_comp.ncolumns();
	m_h5types.reserve(ncolumns);
	for(int i=0; i<(int)ncolumns; ++i){
	  m_h5types.push_back(0);
	  H5_opentype(i);
	}    
      }

      size_t H5RowDef::ncolumns()const{ 
	return m_comp.ncolumns(); 
      }
      
      size_t H5RowDef::rowsize()const{
	return m_comp.datasize();
      }
      
      void H5RowDef::H5_opentype(int colindex){ 
	std::string coltype = m_comp.column(colindex).type;
	int nitems = m_comp.column(colindex).nitems;
	std::string sub = coltype.substr(0,3);
	if(sub == "str"){     
	  std::string strmaxlen = coltype.substr(3,coltype.size()-3);
	  int maxlen = atoi( strmaxlen.c_str() );
	  hid_t strT = H5Tcopy(H5T_C_S1);
	  H5Tset_size(strT,maxlen);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems; 
	    m_h5types[colindex] = H5Tarray_create(strT,1,arraydims);
	    H5Tclose(strT); 	
	  }else{
	    m_h5types[colindex] = strT;
	  }
	}else if(coltype == "uint32"){
	  hid_t uintT = H5Tcopy(H5T_NATIVE_UINT);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;
	    m_h5types[colindex] = H5Tarray_create(uintT,1,arraydims);
	    H5Tclose(uintT);
	  }else{
	    m_h5types[colindex] = uintT;
	  }
	}else if(coltype == "int32"){
	  hid_t intT = H5Tcopy(H5T_NATIVE_INT);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(intT,1,arraydims);
	    H5Tclose(intT); 
	  }else{
	    m_h5types[colindex] = intT;
	  }
	}else if(coltype == "float"){
	  hid_t floatT = H5Tcopy(H5T_NATIVE_FLOAT);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(floatT,1,arraydims);
	    H5Tclose(floatT); 
	  }else{
	    m_h5types[colindex] = floatT;
	  }
	}else if(coltype == "double"){
	  hid_t doubleT = H5Tcopy(H5T_NATIVE_DOUBLE);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(doubleT,1,arraydims);
	    H5Tclose(doubleT); 
	  }else{
	    m_h5types[colindex] = doubleT;
	  }
	}else if(coltype == "uint16"){
	  hid_t ushortT = H5Tcopy(H5T_NATIVE_USHORT);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(ushortT,1,arraydims);
	    H5Tclose(ushortT); 
	  }else{
	    m_h5types[colindex] = ushortT;
	  }
	}else if(coltype == "int16"){
	  hid_t shortT = H5Tcopy(H5T_NATIVE_SHORT);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(shortT,1,arraydims);
	    H5Tclose(shortT); 
	  }else{
	    m_h5types[colindex] = shortT;
	  }
	}else if(coltype == "uint8" || coltype == "bool"){
	  hid_t ucharT = H5Tcopy(H5T_NATIVE_UCHAR);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(ucharT,1,arraydims);
	    H5Tclose(ucharT); 
	  }else{
	    m_h5types[colindex] = ucharT;
	  }
	}else if(coltype == "int8"){
	  hid_t scharT = H5Tcopy(H5T_NATIVE_SCHAR);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(scharT,1,arraydims);
	    H5Tclose(scharT); 
	  }else{
	    m_h5types[colindex] = scharT;
	  }
	}else if(coltype == "uint64"){
	  hid_t ullT = H5Tcopy(H5T_NATIVE_ULLONG);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(ullT,1,arraydims);
	    H5Tclose(ullT); 
	  }else{
	    m_h5types[colindex] = ullT;
	  }
	}else if(coltype == "int64"){
	  hid_t llT = H5Tcopy(H5T_NATIVE_LLONG);
	  if(nitems>1){
	    hsize_t arraydims[1]; arraydims[0]=nitems;      
	    m_h5types[colindex] = H5Tarray_create(llT,1,arraydims);
	    H5Tclose(llT); 
	  }else{
	    m_h5types[colindex] = llT;
	  }
	}else{
	  std::cout<<"unsupported storage type "<<coltype<<std::endl;
	}
      }
      
      hid_t H5RowDef::H5_gettype(int colindex) const { 
	return m_h5types[colindex]; 
      } 

      size_t H5RowDef::column_offset(int colindex) const{ 
	return m_comp.get_offset(colindex); 
      }
      
      std::string H5RowDef::column_name(int colindex) const{
	return m_comp.column(colindex).name; 
      }

      void H5RowDef::H5_closetype(int colindex){ 
	H5Tclose(m_h5types[colindex]); m_h5types[colindex]=0; 
      }

      void H5RowDef::H5_definerow(hid_t rowtypeid){
	for(int colindex = 0; colindex<(int)(ncolumns()); ++colindex){
	  H5Tinsert( rowtypeid, column_name(colindex).c_str(),column_offset(colindex),H5_gettype(colindex) );
	  H5_closetype(colindex);
	}
      }

      void H5RowDef::H5_setfieldnames(hid_t table_id,const char* obj_name,const char* table_name){
	H5LTset_attribute_string(table_id,obj_name,"CLASS","TABLE");
	H5LTset_attribute_string(table_id,obj_name,"TITLE",table_name);
	H5LTset_attribute_string(table_id,obj_name,"VERSION","2.7");
	for(int fieldindex = 0; fieldindex<(int)(ncolumns()); ++fieldindex){
	  //std::stringstream ss;
	  //ss<<"FIELD_"<<fieldindex<<"_NAME";
	  //H5LTset_attribute_string(table_id,obj_name,ss.str().c_str(),column_name( fieldindex ).c_str());
	  char fieldidxStr[16];
	  sprintf(fieldidxStr,"FIELD_%d_NAME",fieldindex);
	  H5LTset_attribute_string(table_id,obj_name,fieldidxStr,column_name( fieldindex ).c_str());
	}
      }
    }}}
