#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "toolbox/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/net/UUID.h"

#include "eventing/api/exception/Exception.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "b2in/nub/Method.h"
#include "xdata/InfoSpaceFactory.h"

#include "bril/lumistore/Application.h"
#include "bril/lumistore/OutputFile.h"
#include "bril/lumistore/OutputFileConfig.h"
#include "bril/lumistore/OutputFileStatus.h"
#include "bril/lumistore/H5OutputFile.h"
#include "bril/lumistore/RAWOutputFile.h"
#include "bril/lumistore/StorageUnit.h"
#include "bril/lumistore/exception/Exception.h"

//#include "interface/bril/CommonDataFormat.h"

XDAQ_INSTANTIATOR_IMPL (bril::lumistore::Application)

bril::lumistore::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL)
{
  xgi::framework::deferredbind(this,this,&bril::lumistore::Application::Default, "Default");
  b2in::nub::bind(this, &bril::lumistore::Application::onB2INMessage);
  m_appInfoSpace = getApplicationInfoSpace();
  m_classname    = getApplicationDescriptor()->getClassName();
  m_poolFactory = toolbox::mem::getMemoryPoolFactory();
  m_maxstalesec = 60;
  m_checkagesec = 20;
  m_maxsizeMB = 1024;
  m_nrowsperwbuf = 102;
  m_filepath.fromString("");
  m_fileformat.fromString("hd5");
  m_totalsizeByte = 0;
  m_totaltimeSec = 0;
  m_throughput = 0.;
  m_dataqueuesize = 0;
  m_lastupdatesec = 0;
  m_firstupdatesec = 0;
  m_lastFileTimeoutCheckTime = 0;  
  m_filechecktimername = "urn:bril-lumistore-timer:filecheckTimer";
  m_filechecktimer = 0;
  m_workinterval = 50000;
  try{
    toolbox::net::UUID uuid;
    m_processuuid = uuid.toString();
    toolbox::mem::HeapAllocator *allocator = new toolbox::mem::HeapAllocator();
    xdata::UnsignedInteger lid = getApplicationDescriptor()->getLocalId();
    xdata::UnsignedInteger instanceid = getApplicationDescriptor()->getInstance();
    toolbox::net::URN urn("toolbox-mem-pool",m_classname+lid.toString()+instanceid.toString());
    m_memPool = m_poolFactory->createPool(urn,allocator);
 
    m_appInfoSpace->fireItemAvailable("datasources",&m_datasources);
    m_appInfoSpace->fireItemAvailable("maxstalesec",&m_maxstalesec);
    m_appInfoSpace->fireItemAvailable("checkagesec",&m_checkagesec);
    m_appInfoSpace->fireItemAvailable("maxsizeMB",&m_maxsizeMB);
    m_appInfoSpace->fireItemAvailable("nrowperwbuf",&m_nrowsperwbuf);    
    m_appInfoSpace->fireItemAvailable("filepath",&m_filepath);
    m_appInfoSpace->fireItemAvailable("fileformat",&m_fileformat);
    m_appInfoSpace->fireItemAvailable("workinterval",&m_workinterval);
    m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");

    std::string nid(m_classname+lid.toString()+instanceid.toString()+"_mon");
    m_monurn = createQualifiedInfoSpace(nid).toString();
    m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(m_monurn));
    m_monInfoSpace->fireItemAvailable("totalsizeByte",&m_totalsizeByte);
    m_monItemList.push_back("totalsizeByte");
    m_monInfoSpace->fireItemAvailable("totaltimeSec",&m_totaltimeSec);
    m_monItemList.push_back("totaltimeSec");
    m_monInfoSpace->fireItemAvailable("throughput",&m_throughput);
    m_monItemList.push_back("throughput");
    m_monInfoSpace->fireItemAvailable("dataqueuesize",&m_dataqueuesize);
    m_monItemList.push_back("dataqueuesize");
    m_monInfoSpace->fireItemAvailable("lastupdatesec",&m_lastupdatesec);
    m_monItemList.push_back("lastupdatesec");
    m_monInfoSpace->addGroupRetrieveListener(this);
    m_filechecktimer = toolbox::task::getTimerFactory()->createTimer(m_filechecktimername);
  }catch(toolbox::net::exception::Exception& e){
    std::string msg("Failed to generate process uuid");
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    XCEPT_RETHROW(bril::lumistore::exception::Application, msg, e);
  }catch(xdata::exception::Exception& e){
    std::string msg = "Failed to setup infospace ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::lumistore::exception::Application, msg, e);
  }catch(toolbox::task::exception::Exception& e){
    std::string msg = "Failed to create filechecktimer ";
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::lumistore::exception::Application, msg, e);
  }catch(xcept::Exception & e ){
    std::string msg("Failed to create memorypool");
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    XCEPT_RETHROW(bril::lumistore::exception::Application, msg, e);
  }
  
  toolbox::Runtime* r = toolbox::getRuntime();
  r->addShutdownListener(this);  
  m_file = 0;
  m_fconfig = new OutputFileConfig;
  m_fstatus = new OutputFileStatus;
  m_shuttingdown = false;
  m_filetimedout = false;  
  m_writingwl = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_wlwriting","waiting");
  m_as_writingwl = toolbox::task::bind(this,&bril::lumistore::Application::writing,"writing");
    
}

bril::lumistore::Application::~Application (){  
  delete m_fconfig;
  m_fconfig = 0;
  delete m_fstatus;
  m_fstatus = 0;
}

void bril::lumistore::Application::Default(xgi::Input * in, xgi::Output * out){
  *out << busesToHTML();
 
  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("Output file configuration parameters");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  *out << cgicc::th("filepath");
  *out << cgicc::td(m_filepath.toString());
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("fileformat");
  *out << cgicc::td(m_fileformat.toString());
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("maxage(sec)");
  *out << cgicc::td(m_maxstalesec.toString());
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("filemaxsize(MB)");
  *out << cgicc::td(m_maxsizeMB.toString());
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("nrowsperwbuf");
  *out << cgicc::td(m_nrowsperwbuf.toString());
  *out << cgicc::tr();

  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();

  m_monInfoSpace->lock();
  m_monInfoSpace->fireItemGroupRetrieve(m_monItemList,this);
  m_monInfoSpace->unlock();

  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("Monitoring Items: "+m_monurn);
  *out << cgicc::tbody();

  *out << cgicc::tr();
  *out << cgicc::th("elasped time(sec)");
  *out << cgicc::td( m_totaltimeSec.toString() );
  *out << cgicc::tr();
    
  *out << cgicc::tr();
  *out << cgicc::th("totalsize(Byte)");
  *out << cgicc::td(m_totalsizeByte.toString());
  *out << cgicc::tr();  

  *out << cgicc::tr();
  *out << cgicc::th("throughput(MB/sec)"); 
  *out << cgicc::td( m_throughput.toString());
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("dataqueuesize");
  *out << cgicc::td( m_dataqueuesize.toString() );
  *out << cgicc::tr();

  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();

}

void bril::lumistore::Application::onB2INMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) 
{
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string msg;
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    msg = "received b2in data from "+topic;
    LOG4CPLUS_DEBUG(getApplicationLogger(),msg);
    msg = "";
    std::string v = plist.getProperty("DATA_VERSION");
    if(v.empty()){ 
      msg = "Received data message without version header, do nothing";
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      if(ref!=0){
	ref->release();
	ref=0;
      }
      return;
    }
    if(interface::bril::shared::DATA_VERSION!=v){
      msg = "received DATA_VERSION "+interface::bril::shared::DATA_VERSION+" does not match, do nothing";
      LOG4CPLUS_ERROR(this->getApplicationLogger(),msg);
      XCEPT_DECLARE(bril::lumistore::exception::DataVersionError,myerrorobj,msg);
      this->notifyQualified("error",myerrorobj);
      if(ref!=0){
	ref->release();
	ref=0;
      }
      return;
    }

    std::string nostore = plist.getProperty("NOSTORE");
    if(!nostore.empty()){
      LOG4CPLUS_INFO(this->getApplicationLogger(),"NOSTORE "+topic);
      if(ref!=0){
      	ref->release();
      	ref=0;
      }
      return;
    }

    toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize()); 
    memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
    StorageUnit* s = new StorageUnit(topic,myref);    
    std::string p = plist.getProperty("PAYLOAD_DICT");
    if(!p.empty()){
      msg = "Compound payload found "+p;
      LOG4CPLUS_DEBUG(getApplicationLogger(),msg);
      s->payloadDict(p);
      msg = "";
    }
    m_dataqueue.push(s);
    m_applock.take();
    m_dataqueuesize.value_++;
    m_applock.give();
  }
  if(ref!=0){
    ref->release();
    ref = 0;
  }
}

void bril::lumistore::Application::actionPerformed(toolbox::Event& e){ 
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Received toolbox event "+e.type());
  if( e.type() == "Shutdown" ){
    m_shuttingdown = true; 
  }
}

void bril::lumistore::Application::actionPerformed(xdata::Event& e){ 
  std::stringstream msg;
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received xdata event " + e.type());
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    m_fconfig->maxagesec = m_maxstalesec.value_;
    m_fconfig->maxfilesize = (unsigned long long)m_maxsizeMB.value_*1024*1024;
    m_fconfig->fileclosingtestsec = m_checkagesec.value_;
    m_fconfig->nrowsperwbuf = m_nrowsperwbuf.value_;
    m_fconfig->h5shrink = m_h5shrink.value_;
    m_fconfig->compressed = m_compressed.value_;
    m_fconfig->filepath = m_filepath.value_;
    if(m_fileformat.value_=="bin"){
      m_fconfig->suffix = ".bin";
    }else{
      m_fconfig->suffix = ".hd5";
    }
    size_t nsources = m_datasources.elements();
    try{      
      for(size_t i=0;i<nsources;++i){
	xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
	xdata::String databus;
	xdata::String topicsStr;
	if(p){
	  databus = p->getProperty("bus");  	 
	  topicsStr = p->getProperty("topics");
	 	  
	  std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,",");
	  m_bustopics.insert(std::make_pair(databus.value_,topics));	  
	}
      }
      subscribeall();      
    }catch( xdata::exception::Exception& e ){
      msg<<"Failed to parse application property";
      LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
    }catch( eventing::api::exception::Exception& e ){
      msg<<"Failed to subscribe to eventing";
      LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
      m_bustopics.clear();
    }catch( bril::lumistore::exception::DataSourceError& e){
      LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
      XCEPT_DECLARE_NESTED(bril::lumistore::exception::Exception,myerrorobj,"DataSourceError",e);
      this->notifyQualified("error",myerrorobj);
    }

    try{    
      m_writingwl->activate();
      m_writingwl->submit(m_as_writingwl);
    }catch(toolbox::task::exception::Exception& e){
      std::string msg("Failed to start writing workloop ");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    }

    try{
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
      toolbox::TimeInterval duration((double)m_fconfig->fileclosingtestsec);
      m_filechecktimer->scheduleAtFixedRate(start,this,duration,0,m_filechecktimername);
    }catch(toolbox::task::exception::Exception& e){
      std::string msg("Failed to start timer "+m_filechecktimername);
      LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    }
  }
}

bool bril::lumistore::Application::writing(toolbox::task::WorkLoop* wl){
    if(m_shuttingdown){            
      LOG4CPLUS_INFO(getApplicationLogger(),"Shutdown, close open file");
      if(m_file){
	while(!m_dataqueue.empty()){
	  writedata();
	}
	m_file->close(OutputFileStatus::force);
	delete m_file;
	m_file = 0;
	m_filetimedout = false;
      }
      m_shuttingdown = false;
      return false;
    }
    if(m_filetimedout){
      if(m_file){
	LOG4CPLUS_INFO(getApplicationLogger(),"Timeout, close file");
	m_file->close(OutputFileStatus::timeout);
	delete m_file;
	m_file = 0;
      }
      m_filetimedout = false;
    }    
    while(!m_dataqueue.empty()){
      writedata();
    }
    usleep(m_workinterval.value_);
    return true;
}

void bril::lumistore::Application::writedata(){
  m_applock.take();
  StorageUnit* s = m_dataqueue.pop();
  m_dataqueuesize.value_--;
  m_applock.give();
  if(!m_file){
    createfile();	
  }else{
    if(!s){
      LOG4CPLUS_INFO(getApplicationLogger(),"Corrupted data?!?!");
      return;
    }
    if(m_file->tooLarge(s->totalDataSize())){
      LOG4CPLUS_INFO(getApplicationLogger(),"Max file size reached");
      m_file->close(OutputFileStatus::oversize);
      delete m_file;
      m_file = 0;
      createfile();
    }
  }  
  m_file->write(s);
  s->release();
  delete s;
  s = 0;      
  m_applock.take();
  m_firstupdatesec.value_ = m_fstatus->firstentrysec;
  m_lastupdatesec.value_ = m_fstatus->lastentrysec;
  m_totaltimeSec.value_ =  m_lastupdatesec.value_ - m_firstupdatesec.value_;
  m_totalsizeByte.value_ = m_fstatus->datasize;
  m_throughput.value_ = 0.;
  if(m_totaltimeSec.value_!=0){
    m_throughput.value_ = float( m_totalsizeByte.value_ )/float(m_totaltimeSec.value_)/1024./1024.;
  }
  m_applock.give();
}

void bril::lumistore::Application::checkForFileTimeOuts(){
  if(!m_file){
    return;
  }
  if(m_lastupdatesec.value_ == 0){
    return;
  }
  unsigned int nowsec = toolbox::TimeVal::gettimeofday().sec();  
  if ( (nowsec - m_lastFileTimeoutCheckTime) > m_fconfig->fileclosingtestsec ){
    if(m_file->tooOld(nowsec) ){
      m_applock.take();
      m_filetimedout = true;
      m_applock.give();
    }
    m_applock.take();
    m_lastFileTimeoutCheckTime.value_ = nowsec;
    m_applock.give();
  }
}

void bril::lumistore::Application::timeExpired(toolbox::task::TimerEvent& e){
  checkForFileTimeOuts();
}

/**
void bril::lumistore::Application::unsubscribeall(){
  try{
    for(std::map<std::string, std::set<std::string> >::iterator bit=m_bustopics.begin(); bit!=m_bustopics.end(); ++bit ){
      std::string busname = bit->first;      
      for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){
	LOG4CPLUS_INFO(getApplicationLogger(),"unsubscribing "+busname+":"+*topicit); 
	this->getEventingBus(busname).unsubscribe(*topicit);
      }
    }
  }catch(eventing::api::exception::Exception& e){
    //failed to unsubscribe, pass
    LOG4CPLUS_ERROR(getApplicationLogger(),"failed to unsubscribe "+stdformat_exception_history(e));
  }
}
*/

void bril::lumistore::Application::subscribeall(){
  for(std::map<std::string, std::set<std::string> >::iterator bit=m_bustopics.begin(); bit!=m_bustopics.end(); ++bit ){
    std::string busname = bit->first; 
    std::string truename ;
    std::vector< std::string > tosubscribe;
    for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){    
      truename = *topicit;
      size_t firstmark_pos = topicit->find_first_of('%');
      if( firstmark_pos!=std::string::npos ){
	size_t lastmark_pos = topicit->find_last_of('%');
	std::string begStr = truename.substr( firstmark_pos+1,lastmark_pos-firstmark_pos-1 );	 
	std::string endStr = truename.substr( lastmark_pos+1 );
	int begint ;
	std::stringstream convertbeg( begStr );
	if( !(convertbeg >> begint) ){
	  LOG4CPLUS_ERROR( getApplicationLogger(), "Failed to convert "+begStr+" to number in "+truename );
	  XCEPT_RAISE( bril::lumistore::exception::Exception, "Wrong range topic format" );
	}
	
	int endint ;
	std::stringstream convertend( endStr );
	if( !(convertend >> endint) ){
	  LOG4CPLUS_ERROR( getApplicationLogger(), "Failed to convert "+endStr+" to number in "+truename );
	  XCEPT_RAISE( bril::lumistore::exception::Exception, "Wrong range topic format" );
	}
	for(int id=begint; id<=endint; ++id){
	  std::string t = truename.substr(0,firstmark_pos);
	  std::stringstream topicnamess;
	  topicnamess << t << id;
	  tosubscribe.push_back( topicnamess.str() );
	}
      }else{
	tosubscribe.push_back( truename );
      }
    }
    for( std::vector< std::string >::const_iterator sit=tosubscribe.begin(); sit!=tosubscribe.end(); ++sit){
      LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*sit); 

      try{
	this->getEventingBus(busname).subscribe(*sit);      
      }catch(eventing::api::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
	m_bustopics[busname].erase(truename);
      }
    }//end loop for subscribe      
  }
}


void bril::lumistore::Application::createfile(){
  if(m_fileformat!="bin"){
    LOG4CPLUS_INFO(getApplicationLogger(),"create new hd5 file");
    m_file = new H5OutputFile(m_processuuid,m_fconfig,m_fstatus);
  }else{
    LOG4CPLUS_INFO(getApplicationLogger(),"create new bin file");
    m_file = new RAWOutputFile(m_processuuid,m_fconfig,m_fstatus);
  }
}
