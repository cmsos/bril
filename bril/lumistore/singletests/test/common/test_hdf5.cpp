#include "hdf5.h"
#include "hdf5_hl.h"
#define FILE "file.h5"

int main(){
  
  hid_t file_id, group_id, dataset_id, dataspace_id; /* identifiers */
  herr_t status;
  hsize_t dims[2];
  
  int i, j, dset1_data[3][3], dset2_data[2][10];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      dset1_data[i][j] = j + 1;
  
  for(i = 0; i<2; i++)
    for(j = 0; j<10; j++)
      dset2_data[i][j] = j + 1;
  
  /* Create a new file using default properties. */
  file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  //  file_id = H5Fopen(FILE, H5F_ACC_RDWR, H5P_DEFAULT);

  /* Create a group named "/MyGroup" in the file. */
  group_id = H5Gcreate2(file_id,"MyGroup", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  /* Create the data space for the dataset */
  dims[0] = 3;
  dims[1] = 3;
  dataspace_id = H5Screate_simple(2,dims,NULL);
  dataset_id = H5Dcreate2(file_id, "/MyGroup/dset1", H5T_STD_I32BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset1_data);
  status = H5Sclose(dataspace_id);
  /* End access to the dataset and release resources used by it*/
  status = H5Dclose(dataset_id);

  /* Open an existing group of the specified file*/
  group_id = H5Gopen(file_id,"/MyGroup", H5P_DEFAULT);
  
  dims[0] = 2;
  dims[1] = 10;
  dataspace_id = H5Screate_simple(2, dims, NULL);
  
  dataset_id = H5Dcreate2(group_id, "dset2", H5T_STD_I32BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dset2_data);
  status = H5Sclose(dataspace_id);

  status = H5Dclose(dataset_id);

  status = H5Gclose(group_id);

  status = H5Fclose(file_id);


  
}
