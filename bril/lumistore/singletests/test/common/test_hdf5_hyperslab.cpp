#include "hdf5.h"
#include <stdio.h>
#include <stdlib.h>

#define FILE            "h5ex_d_hyper.h5"
#define DATASET         "DS1"
#define DIM0            6
#define DIM1            8

int
main (void)
{
    hid_t       file, space, dset;          /* Handles */
    herr_t      status;
    hsize_t     dims[2] = {DIM0, DIM1},
                start[2],
                stride[2],
                count[2],
                block[2];
    int         wdata[DIM0][DIM1],          /* Write buffer */
                rdata[DIM0][DIM1],          /* Read buffer */
                i, j;

    /*
     * Initialize data to "1", to make it easier to see the selections.
     */
    for (i=0; i<DIM0; i++)
        for (j=0; j<DIM1; j++)
            wdata[i][j] = 1;

    /*
     * Print the data to the screen.
     */
    printf ("Original Data:\n");
    for (i=0; i<DIM0; i++) {
        printf (" [");
        for (j=0; j<DIM1; j++)
            printf (" %3d", wdata[i][j]);
        printf ("]\n");
    }

    /*
     * Create a new file using the default properties.
     */
    file = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /*
     * Create dataspace.  Setting maximum size to NULL sets the maximum
     * size to be the current size.
     */
    space = H5Screate_simple (2, dims, NULL);

    /*
     * Create the dataset.  We will use all default properties for this
     * example.
     */
    dset = H5Dcreate (file, DATASET, H5T_STD_I32LE, space, H5P_DEFAULT,
                H5P_DEFAULT, H5P_DEFAULT);

    /*
     * Define and select the first part of the hyperslab selection.
     */
    start[0] = 0;
    start[1] = 0;
    stride[0] = 3;
    stride[1] = 3;
    count[0] = 2;
    count[1] = 3;
    block[0] = 2;
    block[1] = 2;
    /*
      herr_t H5Sselect_hyperslab(hid_t space_id, H5S_seloper_t op, const hsize_t *start, const hsize_t *stride, const hsize_t *count, const hsize_t *block ) 
      Selects a hyperslab region to add to the current selected region for the dataspace specified by space_id. 
      The start, stride, count, and block arrays must be the same size as the rank of the dataspace. For example, if the dataspace is 4-dimensional, each of these parameters must be a 1-dimensional array of size 4. 
      The selection operator op determines how the new selection is to be combined with the already existing selection for the dataspace. The following operators are supported:
      H5S_SELECT_SET 	Replaces the existing selection with the parameters from this call. Overlapping blocks are not supported with this operator.
      H5S_SELECT_OR 	Adds the new selection to the existing selection.    (Binary OR)
      H5S_SELECT_AND 	Retains only the overlapping portions of the new selection and the existing selection.    (Binary AND)
      H5S_SELECT_XOR 	Retains only the elements that are members of the new selection or the existing selection, excluding elements that are members of both selections.    (Binary exclusive-OR, XOR)
      H5S_SELECT_NOTB   	Retains only elements of the existing selection that are not in the new selection.
      H5S_SELECT_NOTA 	Retains only elements of the new selection that are not in the existing selection. 
       The start array specifies the offset of the starting element of the specified hyperslab.

       The stride array chooses array locations from the dataspace with each value in the stride array determining how many elements to move in each dimension. Setting a value in the stride array to 1 moves to each element in that dimension of the dataspace; setting a value of 2 in allocation in the stride array moves to every other element in that dimension of the dataspace. In other words, the stride determines the number of elements to move from the start location in each dimension. Stride values of 0 are not allowed. If the stride parameter is NULL, a contiguous hyperslab is selected (as if each value in the stride array were set to 1).

       The count array determines how many blocks to select from the dataspace, in each dimension.

       The block array determines the size of the element block selected from the dataspace. If the block parameter is set to NULL, the block size defaults to a single element in each dimension (as if each value in the block array were set to 1).

       For example, consider a 2-dimensional dataspace with hyperslab selection settings as follows: the start offset is specified as [1,1], stride is [4,4], count is [3,7], and block is [2,2]. In C, these settings will specify a hyperslab consisting of 21 2x2 blocks of array elements starting with location (1,1) with the selected blocks at locations (1,1), (5,1), (9,1), (1,5), (5,5), etc.; in Fortran, they will specify a hyperslab consisting of 21 2x2 blocks of array elements starting with location (2,2) with the selected blocks at locations (2,2), (6,2), (10,2), (2,6), (6,6), etc.

       Regions selected with this function call default to C order iteration when I/O is performed. 

       hid_t space_id 	IN: Identifier of dataspace selection to modify
       H5S_seloper_t op 	IN: Operation to perform on current selection.
       const hsize_t *start 	IN: Offset of start of hyperslab
       const hsize_t *count 	IN: Number of blocks included in hyperslab.
       const hsize_t *stride    IN: Hyperslab stride.
       const hsize_t *block 	IN: Size of block in hyperslab.
     */
    status = H5Sselect_hyperslab (space, H5S_SELECT_SET, start, stride, count,block);

    /*
     * Define and select the second part of the hyperslab selection,
     * which is subtracted from the first selection by the use of
     * H5S_SELECT_NOTB
     */
    block[0] = 1;
    block[1] = 1;
    status = H5Sselect_hyperslab (space, H5S_SELECT_NOTB, start, stride, count, block);

    /*
     * Write the data to the dataset.
     */
    status = H5Dwrite (dset, H5T_NATIVE_INT, H5S_ALL, space, H5P_DEFAULT,
                wdata[0]);

    /*
     * Close and release resources.
     */
    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Fclose (file);


    /*
     * Now we begin the read section of this example.
     */

    /*
     * Open file and dataset using the default properties.
     */
    file = H5Fopen (FILE, H5F_ACC_RDONLY, H5P_DEFAULT);
    dset = H5Dopen (file, DATASET, H5P_DEFAULT);

    /*
     * Read the data using the default properties.
     */
    status = H5Dread (dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                rdata[0]);

    /*
     * Output the data to the screen.
     */
    printf ("\nData as written to disk by hyberslabs:\n");
    for (i=0; i<DIM0; i++) {
        printf (" [");
        for (j=0; j<DIM1; j++)
            printf (" %3d", rdata[i][j]);
        printf ("]\n");
    }

    /*
     * Initialize the read array.
     */
    for (i=0; i<DIM0; i++)
        for (j=0; j<DIM1; j++)
            rdata[i][j] = 0;

    /*
     * Define and select the hyperslab to use for reading.
     */
    space = H5Dget_space (dset);
    start[0] = 0;
    start[1] = 1;
    stride[0] = 4;
    stride[1] = 4;
    count[0] = 2;
    count[1] = 2;
    block[0] = 2;
    block[1] = 3;
    status = H5Sselect_hyperslab (space, H5S_SELECT_SET, start, stride, count, block);

    /*
     * Read the data using the previously defined hyperslab.
     */
    status = H5Dread (dset, H5T_NATIVE_INT, H5S_ALL, space, H5P_DEFAULT,
                rdata[0]);

    /*
     * Output the data to the screen.
     */
    printf ("\nData as read from disk by hyperslab:\n");
    for (i=0; i<DIM0; i++) {
        printf (" [");
        for (j=0; j<DIM1; j++)
            printf (" %3d", rdata[i][j]);
        printf ("]\n");
    }

    /*
     * Close and release resources.
     */
    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Fclose (file);

    return 0;
}
