// $Id$
#ifndef _bril_lumistore_version_h_
#define _bril_lumstore_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILLUMISTORE_VERSION_MAJOR 4
#define BRIL_BRILLUMISTORE_VERSION_MINOR 0
#define BRIL_BRILLUMISTORE_VERSION_PATCH 1
#define BRILLUMISTORE_PREVIOUS_VERSIONS 

// Template macros
//
#define BRIL_BRILLUMISTORE_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILLUMISTORE_VERSION_MAJOR,BRIL_BRILLUMISTORE_VERSION_MINOR,BRIL_BRILLUMISTORE_VERSION_PATCH)
#ifndef BRIL_BRILLUMISTORE_PREVIOUS_VERSIONS
#define BRIL_BRILLUMISTORE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILLUMISTORE_VERSION_MAJOR,BRIL_BRILLUMISTORE_VERSION_MINOR,BRIL_BRILLUMISTORE_VERSION_PATCH)
#else
#define BRIL_BRILLUMISTORE_FULL_VERSION_LIST BRIL_BRILLUMISTORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILLUMISTORE_VERSION_MAJOR,BRIL_BRILLUMISTORE_VERSION_MINOR,BRIL_BRILLUMISTORE_VERSION_PATCH)
#endif
namespace brillumistore{
  const std::string project = "bril";
  const std::string package = "brillumistore";
  const std::string versions = BRIL_BRILLUMISTORE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ lumistore";
  const std::string description = "bril daq storage";
  const std::string authors = "Z. Xie";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
