#ifndef _bril_lumistore_OutputFileConfig_h
#define _bril_lumistore_OutputFileConfig_h

namespace bril{
  namespace lumistore{
    struct OutputFileConfig{
    OutputFileConfig():maxagesec(60),maxfilesize(1073741824),fileclosingtestsec(20),nrowsperwbuf(1024),h5shrink(false),compressed(false),filepath(""),suffix(".hd5"){}
      unsigned int maxagesec;
      unsigned long long maxfilesize;
      unsigned int fileclosingtestsec;
      unsigned int nrowsperwbuf; //number of rows per write buffer
      bool h5shrink;
      bool compressed;
      std::string filepath;
      std::string suffix;
    };  
  }}
#endif
