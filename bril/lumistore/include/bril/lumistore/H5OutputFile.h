#ifndef _bril_lumistore_H5OutputFile_h
#define _bril_lumistore_H5OutputFile_h
#include <string>
#include <map>
#include "hdf5.h"
#include "bril/lumistore/OutputFile.h"
namespace bril{
  namespace lumistore{
    namespace hdf5{
      class File;
    }
    class StorageUnit;        
    class H5OutputFile: public OutputFile{
    public:
      H5OutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* fstatus);
      virtual ~H5OutputFile();
    protected:
      virtual void do_writeStorageUnit(const StorageUnit* u);
      virtual void do_close();
      virtual size_t do_getfilesize() const;
    private:      
      hdf5::File* m_file;
    };
  }}//ns
#endif
