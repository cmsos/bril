#ifndef _bril_lumistore_hdf5_H5RowDef_h
#define _bril_lumistore_hdf5_H5RowDef_h
#include "interface/bril/shared/CompoundDataStreamer.h"
#include "hdf5.h"
#include <vector>
namespace bril{ namespace lumistore { namespace hdf5{
class H5RowDef{
 public:
  explicit H5RowDef(const std::string& rowdict);
  size_t ncolumns()const;
  size_t rowsize()const;
  void H5_opentype(int colindex);
  hid_t H5_gettype(int colindex) const;
  size_t column_offset(int colindex) const;
  std::string column_name(int colindex) const;
  void H5_closetype(int colindex);
  void H5_definerow(hid_t rowtypeid);
  void H5_setfieldnames(hid_t table_id,const char* obj_name,const char* table_name);
 private:
  interface::bril::shared::CompoundDataStreamer m_comp;
  std::vector<hid_t> m_h5types;
};
    }}}
#endif
