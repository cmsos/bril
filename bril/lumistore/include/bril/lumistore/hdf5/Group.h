#ifndef _bril_lumistore_hdf5_Group_h
#define _bril_lumistore_hdf5_Group_h
#include <string>
#include "bril/lumistore/hdf5/Node.h"
#include "hdf5.h"

namespace bril{ namespace lumistore{ namespace hdf5{
      
      class Group : public Node{
      public:
	Group(const std::string& parentpath, hid_t parentid, const std::string& name,File* file);
	virtual ~Group();
	virtual void close();
      private:
	hid_t create_group();
      };

      class RootGroup: public Group{
      public:
	explicit RootGroup(File* file);
	~RootGroup();
      };
    }}}

#endif
