#ifndef _bril_lumistore_OutputFileStatus_h
#define _bril_lumistore_OutputFileStatus_h
#include <string>
namespace bril{
  namespace lumistore{
    struct OutputFileStatus{
    OutputFileStatus():filecount(0),datasize(0),filesize(0),firstrun(0),firstentrysec(0),lastentrysec(0),nentries(0),isopen(false),closereason(notClosed){}
      std::string filename;
      int filecount;
      unsigned long long datasize;
      unsigned long long filesize;
      unsigned int firstrun;
      unsigned int firstentrysec;
      unsigned int lastentrysec;
      unsigned int nentries;
      bool isopen;
      enum ClosingReason{notClosed = 0, timeout, oversize, force};
      ClosingReason closereason;
    };
    
  }}
#endif
