#ifndef _bril_lumistore_RAWOutputFile_h
#define _bril_lumistore_RAWOutputFile_h
#include <string>
#include "bril/lumistore/OutputFile.h"
#include <cstdio>
namespace bril{
  namespace lumistore{
    class StorageUnit;    
    class RAWOutputFile: public OutputFile{
    public:
      RAWOutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* fstatus);
      virtual ~RAWOutputFile();
    protected:
      virtual void do_writeStorageUnit(const StorageUnit* u);
      virtual void do_close();
      virtual size_t do_getfilesize() const;
    private:
      FILE* p_file;
    };
  }}//ns
#endif
