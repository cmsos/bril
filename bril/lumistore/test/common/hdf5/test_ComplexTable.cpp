#include "interface/bril/CommonDataFormat.h"
#include "bril/lumistore/hdf5/Table.h"
#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/Group.h"
#include "bril/lumistore/hdf5/ComplexTable.h"
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <string>

using namespace bril;
using namespace bril::lumistore::hdf5;
const size_t NUMBER_OF_BLOCKS = 1000;
const size_t ROWS_PER_BLOCK = 10;

int main(){
  std::string dictionary("col1:uint32:1 col2:str5:1 col3:str128:1 col4:uint32:10 col5:str5:10");

  std::string filename="test_ComplexTable.hd5";
  File* f = new File(filename);
  std::cout<<"parent path "<<f->get_rootnode()->pathname()<<std::endl;  
  Node* t = f->create_complextable(f->get_rootnode()->pathname(),"datatable",dictionary,ROWS_PER_BLOCK,false);   
  std::cout<<"table pathname "<<t->pathname()<<std::endl;

  interface::bril::CompoundDataStreamer cdata(dictionary);
  std::cout<<"cdata size "<<cdata.datasize()<<std::endl;
  for(size_t i=0; i<cdata.ncolumns(); ++i){
    std::cout<<"cdata offset "<<cdata.get_offset(i)<<std::endl;
    std::cout<<"cdata fieldsize "<<cdata.get_fieldsize(i)<<std::endl;
  }
  unsigned char* ref = (unsigned char*)malloc(cdata.datasize());

  unsigned int col1_val=16;
  //char col2_val[5]="1234";
  const char* col2_val="1234";
  //char col3_val[128]="abcdefg";
  const char* col3_val="abcdefghi";
  unsigned int col4_val[10];
  for(int i=0;i<10;++i){col4_val[i]=i;}
  char col5_val[10][5];
  for(int i=0;i<10;++i){ strcpy(col5_val[i],"ahijk"); }

  cdata.insert_field( ref, 0, (void*)(&col1_val) );
  cdata.insert_field( ref, 1, (void*)(col2_val) );
  cdata.insert_field( ref, 2, (void*)(col3_val) );
  cdata.insert_field( ref, 3, (void*)(col4_val) );
  cdata.insert_field( ref, 4, (void*)(col5_val) );

  //test extract column by index
  int result_ncolumns = cdata.ncolumns();
  for(int i=0; i<result_ncolumns; ++i){
    std::string colname = cdata.column(i).name;
    std::string coltype = cdata.column(i).type;
    if(colname == "col1"){
      unsigned int out_col1val = 0;
      cdata.extract_field(&out_col1val, i, ref);
      std::cout<<"out_col1val "<<out_col1val<<std::endl;
    }
    if(colname == "col2"){
      char out_col2val[5] = "";
      cdata.extract_field(out_col2val, i, ref);
      std::cout<<"out_col2val "<<out_col2val<<std::endl;
    }
  }
  //test extract column by name
  char out_col3val[128] = "";
  cdata.extract_field(&out_col3val, "col3", ref);
  std::cout<<"col3 "<<out_col3val<<std::endl;

  (dynamic_cast<ComplexTable*>(t))->append(ref);
  free(ref);
  t->close();
  f->close();
  delete f;
  
}
