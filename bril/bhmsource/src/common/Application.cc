#include <algorithm>
#include <array>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <numeric>
#include <cmath>

#include "b2in/nub/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"

#include "xoap/Method.h"
#include "xoap/MessageFactory.h"

#include "xercesc/util/Base64.hpp"

#include "bril/bhmsource/Application.h"
#include "bril/bhmsource/WebUtils.h"
#include "bril/bhmsource/exception/Exception.h"

/**
 * @note This file must be built in C++0x mode. Make sure to port hacks to real C++14 when compiler support is available (gcc 5.x)
 */

namespace
 {

  struct SafeLocker // Exception safe RAII style mutex
   {
    SafeLocker( toolbox::BSem& lock ) : m_lock( lock ) { m_lock.take(); }
    ~SafeLocker() { m_lock.give(); }
    void unlockedwait( long us ) { m_lock.give(); toolbox::u_sleep( us ); m_lock.take(); }
    toolbox::BSem& m_lock;
   };

  struct RefDestroyer  ////RAII exception safe guard object
   {
    RefDestroyer( toolbox::mem::Reference* ref ) : m_ref( ref ){}
    ~RefDestroyer(){ if ( m_ref ) m_ref->release(); }
    toolbox::mem::Reference* m_ref;
   };

 }

XDAQ_INSTANTIATOR_IMPL( bril::bhmsource::Application )

namespace bril
 {

  namespace bhmsource
   {

    class Application::ThresholdScanner
     {

      public:
      typedef std::array<unsigned long, s_nchannels> DataPoint;

      public:
      ThresholdScanner( FrontEnd* fe ) :
       m_front_end( fe ), m_nibblesdone( 0 )
       {}

      ~ThresholdScanner()
       {
        m_front_end->loadConfig();
       }

      void start( int npp, uint8_t first = 0x00, uint8_t last = 0xff, uint8_t step= 1 )
       {
        if ( active() )
          return;
        m_nibblesperpoint = npp;
        m_threshold = first;
        m_max = last;
        m_step = step;
        m_datapoints.clear();
       }

      bool active()
       {
        return m_threshold < m_max;
       }

      void newDataPoint( const DataPoint& d )
       {
        if ( !m_nibblesdone++ ) //the first point after setting a new threshold is not valid
          return;
        auto it = m_datapoints.find( m_threshold );
        if ( it == m_datapoints.end() )
          m_datapoints[m_threshold] = d;
        else
          std::transform( it->second.begin(), it->second.end(), d.begin(), it->second.begin(), std::plus<unsigned long>() );
        if ( m_nibblesdone == m_nibblesperpoint )
         {
          m_nibblesdone = 0;
          m_threshold += m_step;
          if ( m_threshold >= m_max )
            return;
          std::string thrs = WebUtils::to_string( m_threshold );
          m_front_end->setQieThreshold( thrs );
         }
       }

      void cgiOutput( xgi::Output* out )
       {
        using namespace cgicc;
        *out << h3() << "Threshold scan: " << m_threshold << '/' << m_max << h3() << br() << std::endl;
        if ( !active() )
        *out << "Scan results:" << br() << std::endl;
        for ( auto it = m_datapoints.begin(); it != m_datapoints.end(); ++it )
         {
          *out << it->first << ", ";
          std::copy( it->second.begin(), it->second.end(), std::ostream_iterator<unsigned long>( *out, ", " ) );
          *out << br() << std::endl;
         }
       }

      private:
      FrontEnd* m_front_end;
      int m_threshold;
      int m_max;
      int m_step;
      int m_nibblesperpoint;
      int m_nibblesdone;
      std::map<int, DataPoint> m_datapoints;

     };

    const int Application::s_nchannels;

/**
 * @brief Constructor
 *
 * The constructor registers all the callbacks, initializes data structures and monitoring parameters.
 */
    Application::Application( xdaq::ApplicationStub* s ):
     xdaq::Application( s ),
     xgi::framework::UIManager( this ),
     eventing::api::Member( this ),
     m_applock( toolbox::BSem::FULL ),
     m_shutdown( false ),
     m_uhtr( 0 ),
     m_front_end( 0 ),
     m_last_occupancy( 0. ),
     m_how_many_times_was_orbit_length_bad( 0 ),
     m_next_data_bad( true ),
     m_calibration_active( false ),
     m_qie_threshold_scan( 0 ),
     m_mon_collected_histos( 0 )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      m_mon_quality.resize( s_nchannels, 0 );
      m_mon_rawrate.resize( s_nchannels, 0.f );
      m_mon_calpeak.resize( s_nchannels, 0 );
      try
       {
        std::string classname = getApplicationDescriptor()->getClassName();
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Initializing application " << classname );
        toolbox::net::URN memurn( "toolbox-mem-pool", "mem-pool_" + getApplicationDescriptor()->getURN() );
        m_pool_factory = toolbox::mem::getMemoryPoolFactory();
        m_mem_pool = m_pool_factory->createPool( memurn, new toolbox::mem::HeapAllocator() );
        m_timer = toolbox::task::getTimerFactory()->createTimer( "PeriodicDiagnostic_" + getApplicationDescriptor()->getURN() );
        setupConfiguration();
        setupMonitoring();
        xgi::framework::deferredbind( this, this, &Application::Default, "Default" );
        xgi::framework::deferredbind( this, this, &Application::expertPage, "expert" );
        b2in::nub::bind( this, &Application::onMessage );
        toolbox::getRuntime()->addShutdownListener( this );
       }
      catch( xcept::Exception& e )
       {
        notifyQualified( "fatal", e );
        throw; //Errors in the constructor should not be recoverable
       }
     }

    Application::~Application()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      delete m_readamp_wl;
      delete m_readocc_wl;
      delete m_publish_wl;
     }

    void Application::setupConfiguration()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      auto infospace = getApplicationInfoSpace();
      infospace->fireItemAvailable( "eventinginput", &m_cfg.data_sources );
      infospace->fireItemAvailable( "eventingoutput", &m_cfg.output_topics );
      infospace->fireItemAvailable( "beam", &m_cfg.beam );
      infospace->fireItemAvailable( "amplitude", &m_cfg.collect_amplitude );
      infospace->fireItemAvailable( "montimerperiod", &m_cfg.mon_timer_period_sec );
      infospace->fireItemAvailable( "connectionuri", &m_cfg.uhtr_uri );
      infospace->fireItemAvailable( "ngccmsrvuri", &m_cfg.ngccmsrv_uri );
      infospace->fireItemAvailable( "channels", &m_cfg.channels );
      infospace->fireItemAvailable( "slot", &m_cfg.rm_slot );
      infospace->fireItemAvailable( "integratennb", &m_cfg.integrate_nibbles );
      infospace->fireItemAvailable( "orbitphase", &m_cfg.uhtr_orbit_phase );
      infospace->fireItemAvailable( "tdcmap", &m_cfg.uhtr_tdcmap );
      infospace->fireItemAvailable( "calenable", &m_cfg.calibration_enabled );
      infospace->fireItemAvailable( "caldelay", &m_cfg.pulser_delay );
      infospace->fireItemAvailable( "calpulsernear", &m_cfg.pulser_bias_top );
      infospace->fireItemAvailable( "calpulserfar", &m_cfg.pulser_bias_bot );
      infospace->fireItemAvailable( "calsipmnear", &m_cfg.sipm_bias_top );
      infospace->fireItemAvailable( "calsipmfar", &m_cfg.sipm_bias_bot );
      infospace->addListener( this, "urn:xdaq-event:setDefaultValues" );
      //Changing the parameters below will invoke an action, other parameters should NOT be changed at runtime
      //The possibility of changing these online should only be used for testing hardware configurations, not in production
      infospace->addItemChangedListener( "channels", this ); //unfortunately this does not work with the hyperdaq "editParameters" page, because that can't make the correct SOAP call.
      infospace->addItemChangedListener( "orbitphase", this );
      infospace->addItemChangedListener( "tdcmap", this );
      infospace->addItemChangedListener( "amplitude", this );
      infospace->addItemChangedListener( "calenable", this );
      infospace->addItemChangedListener( "caldelay", this );
      infospace->addItemChangedListener( "calpulsernear", this );
      infospace->addItemChangedListener( "calpulserfar", this );
      infospace->addItemChangedListener( "calsipmnear", this );
      infospace->addItemChangedListener( "calsipmfar", this );
     }

    void Application::setupEventing()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      for ( size_t i = 0; i != m_cfg.data_sources.size(); ++i )
       {
        std::string databus = m_cfg.data_sources[i].getProperty( "bus" );
        std::string topics = m_cfg.data_sources[i].getProperty( "topics" );
        auto topicset = toolbox::parseTokenSet( topics, "," );
        for ( auto it = topicset.begin(); it != topicset.end(); ++it )
         {
          getEventingBus( databus ).subscribe( *it );
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Subscribing to " << databus << "/" << *it );
         }
       }
      m_output_topics_map.clear();
      m_unready_buses.clear();
      for ( size_t i = 0; i != m_cfg.output_topics.size(); ++i )
       {
        std::string topic = m_cfg.output_topics[i].getProperty( "topic" );
        std::string buses = m_cfg.output_topics[i].getProperty( "buses" );
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Publishing topic " << topic << " on buses " << buses );
        BusNameSet set = toolbox::parseTokenSet( buses, "," );
        m_output_topics_map[topic] = set;
        m_unready_buses.insert( set.begin(), set.end() );
       }
      for ( auto it = m_unready_buses.begin(); it != m_unready_buses.end(); ++it )
       {
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Waiting for bus \"" << *it << "\" to be ready." );
        getEventingBus( *it ).addActionListener( this );
       }
     }

    void Application::setupHardwareConfiguration()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      //bookkeeping
      auto names = m_cfg.channel_names();
      for ( auto n = names.begin(); n != names.end(); ++n )
        m_channel_ids.push_back( *n );
      //Back End
      m_uhtr = new Uhtr( m_cfg, this );
      int tries = 5;
      do
       {
        m_uhtr->reset();
        toolbox::u_sleep( 500000 );
       }
      while ( !m_uhtr->sanityCheck() && tries-- );
//      if ( tries < 0 )
//        XCEPT_RAISE( exception::HardwareError, "Cannot configure hardware at startup." );
//        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Cannot configure Back End." );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Configuring Back End." );
      m_uhtr->loadConfig();
      //Front End
      m_front_end = new FrontEnd( m_cfg, this );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Configuring Front End." );
      m_front_end->stopCalibration(); //in case the application died with calibration ON
      m_front_end->loadConfig();
     }

    void Application::setupMonitoring()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      std::string monurn = createQualifiedInfoSpace( "bhmsourceMon" ).toString();
      m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
      const int nvars = 9;
      const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", "ncollected", "channelQuality", "channelRawRate", "channelCalPeak" };
      xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_mon_collected_histos, &m_mon_quality, &m_mon_rawrate, &m_mon_calpeak };
      for ( int i = 0; i != nvars; ++i )
       {
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
       }
     }

    void Application::setupWorkloop()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      m_readocc_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( getApplicationDescriptor()->getURN() + "_readocc", "waiting" );
      toolbox::task::ActionSignature* as_readocc = toolbox::task::bind( this, &Application::readOccupancyMain, "readocc" );
      m_readocc_wl->activate();
      m_readocc_wl->submit( as_readocc );
      m_readamp_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( getApplicationDescriptor()->getURN() + "_readamp", "waiting" );
      toolbox::task::ActionSignature* as_readamp = toolbox::task::bind( this, &Application::readAmplitudeMain, "readamp" );
      m_readamp_wl->activate();
      m_readamp_wl->submit( as_readamp );
      m_publish_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( getApplicationDescriptor()->getURN() + "_publish", "waiting" );
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind( this, &Application::publishMain, "publish" );
      m_publish_wl->activate();
      m_publish_wl->submit( as_publishing );
     }

    void Application::Default( xgi::Input* in, xgi::Output* out )
     {
      using namespace cgicc;
      using std::endl;
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      *out << h1() << "BHM Source" << h1() << endl;
      *out << h3() << "Eventing state" << h3() << endl;
      *out << busesToHTML() << endl;
      *out << br() << hr() << br() << endl;
      *out << h3() << "Hardware status" << h3() << endl;
      bool fe = m_front_end && m_front_end->alive();
      bool cal = fe && m_front_end->hasCalibration();
      bool be = m_uhtr && m_uhtr->sanityCheck();
      *out << p().set( "style", "color: " + std::string( fe ? "green" : "red" ) ) << b() << "Front End " + std::string( fe ? "UP" : "DOWN" ) << b() << p();
      *out << p().set( "style", "font-size: 80%; color: " + std::string( cal ? "green" : "red" ) ) << b() << "  Calibration " + std::string( cal ? "UP" : "DOWN" ) << b() << p();
      *out << p().set( "style", "color: " + std::string( be ? "green" : "red" ) ) << b() << "Back End " + std::string( be ? "UP" : "DOWN" ) << b() << p();
      *out << hr() << br() << endl;
      *out << h3() << "Current Monitoring info" << h3() << endl;
      auto titles = { "" };
      auto shortlist = { "Fill", "Run", "LumiSection", "LumiNibble", "Timestamp", "Collected Histograms" };
      WebUtils::cgiVerticalTable( out, titles, shortlist, m_mon_vars );
      *out << br() << endl;
      auto header = { "Channel", "NNB per read", "Rate", "Peak" };
      WebUtils::cgiHorizontalTable( out, header, m_cfg.channel_names(), m_mon_quality, m_mon_rawrate, m_mon_calpeak );
      *out << br() << hr() << br() << endl;
      std::string experturl = "/urn:xdaq-application:lid=" + WebUtils::to_string( getApplicationDescriptor()->getLocalId() ) + "/expert";
      *out << h3() << a( "Expert page" ).set( "href", experturl ) << h3() << br() << endl;
     }

    void Application::expertPage( xgi::Input* in, xgi::Output* out )
     {
      using namespace cgicc;
      using std::endl;
      Cgicc cgi( in );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      std::string configurl = "/urn:xdaq-application:service=hyperdaq/editProperties?lid=" + WebUtils::to_string( getApplicationDescriptor()->getLocalId() );
      *out << h3() << "Current Hardware configuration - " << a( "CHANGE" ).set( "href", configurl ) << h3() << br() << endl;
      //auto titles = { "" };
//      WebUtils::cgiVerticalTable( out, titles, m_cfg.info_varnames(), m_cfg.info_variables() );
      *out << br() << hr() << endl;
      *out << h3() << "Link histograms" << h3() << br() << endl;
      auto channels = { "", "Ch 0", "Ch 1", "Ch 2", "Ch 3", "Ch 4", "Ch 5", "Ch 6", "Ch 7" };
      auto x = std::vector<std::string>( 256 );
      for ( int i = 0; i != 256; ++i )
        x[i] = std::to_string( i );
      auto hs = m_uhtr->getLinkHistograms( 4096 );
      WebUtils::cgiVerticalTable( out, channels, x, hs[0], hs[1], hs[2], hs[3], hs[4], hs[5], hs[6], hs[7] );
      *out << br() << hr() << endl;

/*      std::string experturl = "/urn:xdaq-application:lid=" + WebUtils::to_string( getApplicationDescriptor()->getLocalId() ) + "/expert";
      *out << h3() << "QIE Threshold Scan" << h3() << br() << endl;
      *out << form().set( "action", experturl ).set( "method", "get" ) << endl;
      *out << "Enable " << input().set( "type", "checkbox" ).set( "name", "enable" ) << br() << endl;
      *out << "Minimum Threshold " << input().set( "type", "number" ).set( "name", "min" ).set( "min", "0" ).set( "max", "255" ).set( "value", cgi( "min" ) ) << br() << endl;
      *out << "Maximum Threshold " << input().set( "type", "number" ).set( "name", "max" ).set( "min", "0" ).set( "max", "255" ).set( "value", cgi( "max" ) ) << br() << endl;
      *out << "Threshold Step " << input().set( "type", "number" ).set( "name", "step" ).set( "min", "0" ).set( "max", "255" ).set( "value", cgi( "step" ) ) << br() << endl;
      *out << "Points per Step " << input().set( "type", "number" ).set( "name", "pps" ).set( "min", "1" ).set( "value", cgi( "pps" ) ) << br() << endl;
      *out << button().set( "type", "submit" ) << "Start threshold scan" << button() << endl;
      *out << form() << endl;
      *out << br() << hr() << endl;
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Starting Threshold Scan?? enable is " << cgi( "enable" ) << ", ptr is " << m_qie_threshold_scan );
      if ( cgi( "enable" ) == "on" && !m_qie_threshold_scan ) //start the scan
       {
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Starting Threshold Scan." );
        m_qie_threshold_scan = new ThresholdScanner( m_front_end );
        uint8_t thrmin = std::stoi( cgi( "min" ) );
        uint8_t thrmax = std::stoi( cgi( "max" ) );
        uint8_t thrstep = std::stoi( cgi( "step" ) );
        uint8_t pps = std::stoi( cgi( "pps" ) );
        m_qie_threshold_scan->start( pps, thrmin, thrmax, thrstep );
       }
      if ( m_qie_threshold_scan )
       {
        m_qie_threshold_scan->cgiOutput( out );
        if ( !m_qie_threshold_scan->active() )
         {
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Completed Threshold Scan." );
          delete m_qie_threshold_scan;
          m_qie_threshold_scan = 0;
         }
       }*/
     }

    void Application::actionPerformed( xdata::Event& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received xdata event " << e.type() );
      try
       {
        if ( e.type() == "urn:xdaq-event:setDefaultValues" ) //this is used as an initialization signal
         {
          setupEventing();
          setupHardwareConfiguration();
          //start monitoring timer
          toolbox::TimeInterval interval( m_cfg.mon_timer_period_sec, 0 );
          toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
          m_timer->scheduleAtFixedRate( start, this, interval,  0, "mon_timer" );

	  ///Calibration 
	  if ( m_calibration_active && !m_cfg.calibration_enabled ){ 
	    m_front_end->stopCalibration();
	  }
	  else if ( m_cfg.calibration_enabled ){ //calibration was enabled or parameters were changed
	    m_front_end->startCalibration();
	  }
	  m_calibration_active = m_cfg.calibration_enabled;
	  //////
	  
         }
        else if ( e.type() == "ItemChangedEvent" )
         {
          std::string item = static_cast<xdata::ItemEvent&>( e ).itemName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Parameter " << item << " changed." );
          if ( !m_uhtr || !m_front_end ) //this event happens once before initialization has completed
            return;
          SafeLocker locker( m_applock );
          m_uhtr->loadConfig();
          m_front_end->loadConfig();
          if ( item.substr( 0, 3 ) == "cal" )
           {
            if ( m_calibration_active && !m_cfg.calibration_enabled ) //calibration was disabled
              m_front_end->stopCalibration();
            else if ( m_cfg.calibration_enabled ) //calibration was enabled or parameters were changed
              m_front_end->startCalibration();
            m_calibration_active = m_cfg.calibration_enabled;
           }
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

    void Application::actionPerformed( toolbox::Event& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received toolbox event " << e.type() );
      try
       {
        if ( e.type() == "eventing::api::BusReadyToPublish" )
         {
          std::string busname = static_cast<eventing::api::Bus*>( e.originator() )->getBusName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Bus \"" << busname << "\" is ready." );
          m_unready_buses.erase( busname );
          if ( !m_unready_buses.empty() )
            return; //wait until all buses are ready
          setupWorkloop();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " All buses ready for publishing" );
         }
        if ( e.type() == "Shutdown" )
         {
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Shutting down" );
          m_shutdown = true;
          toolbox::u_sleep( 500000 );
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

/**
 * @brief XDAQ Eventing receiver callback
 *
 * Receives the three types of data that are used by this application: the BHM amplitude and occupancy histograms, and the beam information.
 */
    void Application::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist )
     {
      try
       {
        RefDestroyer rd( ref );
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
        std::string action = plist.getProperty( "urn:b2in-eventing:action" );
        if ( action != "notify" )
          return;
        std::string version = plist.getProperty( "DATA_VERSION" );
        if( version.empty() || version != interface::bril::shared::DATA_VERSION )
          XCEPT_RAISE( exception::EventingError, "Received data with missing or wrong version header (" + version + ")." );
        TopicName topic = plist.getProperty( "urn:b2in-eventing:topic" );
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Received data from " << topic );
        if ( topic == "beam" )
         {
          std::string payloaddict = plist.getProperty( "PAYLOAD_DICT" );
          if ( payloaddict.empty() )
            XCEPT_RAISE( exception::EventingError, "Received data with missing dictionary." );
          interface::bril::beamT* incoming = static_cast<interface::bril::beamT*>( ref->getDataLocation() );
          interface::bril::shared::CompoundDataStreamer cds( payloaddict );
          char status[30];
          status[29] = '\0';
          if ( cds.extract_field( status, "status", incoming->payloadanchor ) )
           {
            LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Beam mode " << status );
            SafeLocker locker( m_applock );
            m_mon_beammode = status;
           }
          else
            XCEPT_RAISE( exception::EventingError, " Could not unpack information from Beam Data." );
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

/**
 * Collects monitoring information and pushes it to the infospace for collection.
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    void Application::timeExpired( toolbox::task::TimerEvent& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      if ( e.originator() != m_timer )
        return;
      try
       {
        SafeLocker locker( m_applock );
        if ( !m_uhtr->sanityCheck() || m_how_many_times_was_orbit_length_bad > 1 )
         {
          m_uhtr->reset();
          m_next_data_bad = true;
          m_how_many_times_was_orbit_length_bad = 0;
         }
        m_uhtr->readConfig(); //read back parameters to see if they changed (why would they?)
        m_front_end->readConfig(); //these could change if power loss
        m_mon_timestamp = toolbox::TimeVal::gettimeofday();
        //for ( int ch = 0; ch != s_nchannels; ++ch )
        //  m_mon_quality[ch] = m_channel_ids[ch].valid();
        m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
        auto fetemps = m_front_end->readTemperatures();
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Front end temperatures are " << WebUtils::to_string( fetemps ) << " C" );
        if ( fetemps[0] > 40.0 )
          XCEPT_RAISE( exception::HardwareError, "Front End temperature too high: " + WebUtils::to_string( fetemps ) + " C" );
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

    void Application::occupancyStats( const Uhtr::OccupancyHistogramList& histos )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      ThresholdScanner::DataPoint calrates;
      SafeLocker locker( m_applock );
      for ( int ch = 0; ch != s_nchannels; ++ch )
       {
        calrates[ch] = 0;
        m_mon_quality[ch] = -1;
        m_mon_rawrate[ch] = -1.f;
        m_mon_calpeak[ch] = -1;
       }
      //xdaq monitorables
      bool hasvalidhisto = false; //at least one good histo?
      for ( auto it = histos.begin(); it != histos.end(); ++it )
       {
        int ch = it->index;
        if ( ch < 0 || ch >= s_nchannels )
          continue;
        bhm::ChannelId id = m_channel_ids[ch];
        if ( !id.valid() )
          continue;
        if ( !it->lhc_fill )//some entries have all zeros, which is bad, we don't want to see that
          continue;
        hasvalidhisto = true;
        unsigned long entries = std::accumulate( it->h.begin(), it->h.end(), 0UL );
        auto offset = it->h.begin() + m_cfg.pulser_delay * 4 + 260; //~70 bx is the calibration delay
        calrates[ch] = std::accumulate( offset, offset + 40, 0UL ); //conservative interval that includes the pulse but not the afterpulse
        int peak = m_cfg.calibration_enabled ? std::distance( it->h.begin(), std::max_element( it->h.begin(), it->h.end() ) ) : -1;
        //for monitoring, we just count all the entries in the occupancy histogram
        m_mon_rawrate[ch] = entries / 1.4569f;
        //the calibration peak position is the weighted average of counts in the various bins. This calculation assumes only the calibration pulses are over threshold.
        m_mon_calpeak[ch] = peak;
        //temporary: for quality we count the number of reported orbits:
        m_mon_quality[ch] = ( long( it->orb_last ) - long( it->orb_init ) ) / 4096;
        m_mon_fill = it->lhc_fill;
        m_mon_run = it->cms_run;
        m_mon_ls = it->lumi_section;
        m_mon_nb = it->lumi_nibble;
       }
      m_mon_collected_histos = m_mon_collected_histos + int( hasvalidhisto );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Collected a total of " << m_mon_collected_histos.toString() << " histograms" );
      //internal, threshold scan
      if ( !m_qie_threshold_scan || !m_qie_threshold_scan->active() || histos.empty() )
        return;
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Adding data point to threshold scan, max = " << *std::max_element( calrates.begin(), calrates.end() ) );
      m_qie_threshold_scan->newDataPoint( calrates );
     }

/**
 * Occupancy workloop main function.
 *
 * This function retrieves occupancy histograms when available, and pushes them into the publication queue.
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 *
 * @returns True to continue processing, false otherwise
 */
    bool Application::readOccupancyMain( toolbox::task::WorkLoop* wl )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      try
       {
        Uhtr::OccupancyHistogramList histos = m_uhtr->getOccupancy();
        toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
        double dt = t;
        if ( histos.empty() )
          return !m_shutdown;
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Read out " << histos.size() << " histograms, " << dt - m_last_occupancy << "s after last readout." );
        m_last_occupancy = dt;
        occupancyStats( histos );
        bool orbitlengthissue = false;
        for ( auto it = histos.begin(); it != histos.end(); ++it )
         {
          int ch = it->index;
          LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Info for ch " << ch << " (" << long( it->orb_init ) << ',' << long( it->orb_last ) << ") => " << long( it->orb_last ) - long( it->orb_init ) << "; LN = " << long( it->lumi_nibble ) );
          unsigned int algoid = 1; //used as quality
          algoid += m_next_data_bad << 1;
          algoid += bool( it->overflow ) << 2;
          if ( ch < 0 || ch >= s_nchannels )
            continue;
          bhm::ChannelId id = m_channel_ids[ch];
          if ( !id.valid() || ch > 17 ) //channels 21, 22 and 23 do not exist in hardware, but are readout by sw. Ignore them.
            continue;
          if ( long( it->orb_last ) - long( it->orb_init ) != m_cfg.integrate_nibbles * 4096 )
           {
            orbitlengthissue = true;
            continue;
           }
          auto occhist = newPub<interface::bril::bhmocchistT>( algoid, id.linearized(), interface::bril::shared::StorageType::UINT16 );
          for ( unsigned bin = 0; bin != it->h.size(); ++bin )
            occhist.second->payload()[bin] = it->h[bin];
//          for ( unsigned bx = 0; bx != it->h.size() / 3; ++bx )
//           {
//            occhist.second->payload()[4 * bx] = it->h[4 * bx];
//            occhist.second->payload()[4 * bx + 1] = it->h[4 * bx + 1];
//            occhist.second->payload()[4 * bx + 2] = it->h[4 * bx + 2];
//            occhist.second->payload()[4 * bx + 3] = -1;
//           }
          occhist.second->setTime( it->lhc_fill, it->cms_run, it->lumi_section, it->lumi_nibble, t.sec(), t.millisec() );
          occhist.second->setFrequency( 4 );
          m_occupancy.push( occhist.first );
          m_next_data_bad = false;
         }
        if ( orbitlengthissue )
         {
          SafeLocker locker( m_applock );
          LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Orbit length issue detected (" << m_how_many_times_was_orbit_length_bad << " times since last reset)." );
          ++m_how_many_times_was_orbit_length_bad;
          m_next_data_bad = true;
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
      toolbox::u_sleep( 200000 ); //this sleep time should be sufficient to read even at 1 NB
      return !m_shutdown;
     }

/**
 * Amplitude workloop main function.
 *
 * This function retrieves amplitude histograms, and pushes them into the publication queue.
 * The interface to acquire amplitude histograms requires timing control by sw, which makes it imprecise.
 * As a result, histograms will not have a perfect correspondance to nibbles, nor an exact entry count.
 *
 * @note The uhtr lock is only held during actual access, and released while sleeping or doing other things...
 * @note This function executes in a different thread, all write access to member variables should be locked.
 *
 * @returns True to continue processing, false otherwise
 */
    bool Application::readAmplitudeMain( toolbox::task::WorkLoop* wl )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      if ( !m_cfg.collect_amplitude )
       {
        toolbox::u_sleep( 250000 );
        return !m_shutdown;
       }
      try
       {
        Uhtr::AmplitudeHistogramList histos = m_uhtr->getAmplitude();
        toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
        for ( int ch = 0; ch != s_nchannels; ++ch )
         {
          bhm::ChannelId id = m_channel_ids[ch];
          if ( !id.valid() ) //it's some extra channel
            continue;
          for ( int tdcbin = 1; tdcbin != 5; ++tdcbin )
           {
            auto amphist = newPub<interface::bril::bhmamphistT>( tdcbin, id.linearized(), interface::bril::shared::StorageType::UINT32 );
            for ( int bin = 0; bin != 256; ++bin )
              amphist.second->payload()[bin] = histos.histo[ch][tdcbin - 1][bin];
            amphist.second->setTime( histos.fill, histos.run, histos.ls, histos.nb, t.sec(), t.millisec() );
            amphist.second->setFrequency( 4 );
            m_amplitude.push( amphist.first );
           }
         }
       }
      catch ( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Failed to read amplitude" );
        notifyQualified( "error", e );
       }
      return !m_shutdown;
     }

/**
 * Publishing workloop main function. This function pops available histograms from the occupancy and amplitude queue and publishes them.
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 *
 * @returns True to continue processing, false otherwise
 */
    bool Application::publishMain( toolbox::task::WorkLoop* wl )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      try
       {
        while ( !m_occupancy.empty() )
          doPublish( interface::bril::bhmocchistT::topicname(), m_occupancy.pop() );
        while ( !m_amplitude.empty() )
          doPublish( interface::bril::bhmamphistT::topicname(), m_amplitude.pop() );
       }
      catch( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
      toolbox::u_sleep( 100000 );
      return !m_shutdown;
     }

/**
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    template <typename T>
    std::pair<toolbox::mem::Reference*, T*> Application::newPub( unsigned int algoid, unsigned int channelid, unsigned int storage_type )
     {
      toolbox::mem::Reference* memref = 0;
      memref = m_pool_factory->getFrame( m_mem_pool, T::maxsize() );
      memref->setDataSize( T::maxsize() );
      T* datum = static_cast<T*>( memref->getDataLocation() );
      datum->setTotalsize( T::maxsize() );
      datum->setResource( interface::bril::shared::DataSource::BHM, algoid, channelid, storage_type );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << " Created message of size " << memref->getDataSize() );
      return std::make_pair( memref, datum );
     }

/**
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    void Application::doPublish( const TopicName& topic, toolbox::mem::Reference* memref, const std::string& payloaddict )
     {
      RefDestroyer rd( memref );
      xdata::Properties plist;
      plist.setProperty( "DATA_VERSION", interface::bril::shared::DATA_VERSION );
      if ( !payloaddict.empty() )
        plist.setProperty( "PAYLOAD_DICT", payloaddict );
      if ( m_mon_beammode == "SETUP" || m_mon_beammode == "ABORT" || m_mon_beammode == "BEAM DUMP" || m_mon_beammode == "RAMP DOWN" ||
           m_mon_beammode == "CYCLING" || m_mon_beammode == "RECOVERY" || m_mon_beammode == "NO BEAM" )
        plist.setProperty( "NOSTORE", "1" );
      auto iset = m_output_topics_map.find( topic );
      if ( iset == m_output_topics_map.end() )
        XCEPT_RAISE( exception::EventingError, "Topic \"" + topic + "\" is not in the list of known topics" );
      for ( auto it = iset->second.begin(); it != iset->second.end(); ++it )
       {
        BusName bus = *it;
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": publishing to " << bus << '/' << topic << " a message of size " << memref->getDataSize() );
        toolbox::mem::Reference* ref = memref->duplicate(); //Having to do this manually is *awful*, xdaq memory allocation *sucks* hardcore. The dupe reference ends up leaked ofc.
        getEventingBus( bus ).publish( topic, ref, plist );
       } //the last duplicate is cleaned up by the rd.
     }

   }

 }
