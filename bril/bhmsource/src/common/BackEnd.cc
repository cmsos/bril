#include <algorithm>
#include <iomanip>

#include "xdaq/Application.h"

#include "toolbox/utils.h"

#include "uhal/log/exception.hpp"
#include "uhal/ConnectionManager.hpp"
#include "uhal/HwInterface.hpp"

#include "hcal/uhtr/uHTRsensors.hh"
#include "hcal/uhtr/LinkFormatTools.hh"

#include "bril/bhmsource/exception/Exception.h"

#include "bril/bhmsource/BackEnd.h"

namespace
 {

  struct SafeLocker  // Exception safe RAII style mutex
   {
    SafeLocker( toolbox::BSem& lock ) : m_lock( lock ) { m_lock.take(); }
    ~SafeLocker() { m_lock.give(); }
    void unlockedwait( long us ) { m_lock.give(); toolbox::u_sleep( us ); m_lock.take(); }
    toolbox::BSem& m_lock;
   };

 }

namespace bril
 {

  namespace bhmsource
   {

    Uhtr::Uhtr( Configuration& cfg, xdaq::Application* app ) :
     m_lock( toolbox::BSem::FULL ),
     m_uhtr( 0 ),
     r_cfg( cfg ),
     m_app( app )
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      std::string addrtable = "file:///opt/xdaq/ipbus/hcal/uHTR.xml";
      std::string uhtrname( "bhm_uhtr_beam " );
      uhtrname += r_cfg.beam.toString();
      LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Connecting to uHTR at " << r_cfg.uhtr_uri.toString() );
      try
       {
        SafeLocker locker( m_lock );
        uhal::HwInterface hw = uhal::ConnectionManager::getDevice( uhtrname, r_cfg.uhtr_uri, addrtable );
        m_uhtr = new hcal::uhtr::uHTR( uhtrname, hw );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

    Uhtr::~Uhtr()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      delete m_uhtr;
     }

/**
 * This function Initializes the hardware. It also resets the links.
 */
    void Uhtr::reset()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      try
       {
        SafeLocker locker( m_lock );
        uint8_t flavor, major, minor, patch;
        m_uhtr->firmwareRevision( true, flavor, major, minor, patch );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Front FPGA fw " << ( flavor == hcal::uhtr::uHTR::FL_BHM ? "BHM v" : "non BHM v" ) << int( major ) << '.' << int( minor ) << '.' << int( patch ) << ( major >= 1 ? " (new " : " (old " ) << " link version)." );
        m_uhtr->firmwareRevision( false, flavor, major, minor, patch );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Back FPGA fw " << ( flavor == hcal::uhtr::uHTR::FL_BHM ? "BHM v" : "non BHM v" ) << int( major ) << '.' << int( minor ) << '.' << int( patch  ) );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": PCB revision " << m_uhtr->pcbRevision() << " - Serial #" << m_uhtr->serialNumber() );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Resetting Lumi links" );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Initializing clocks with 5.0 Gbps async scheme" );
        m_uhtr->setupClock( 1, 320 ); //copied from uHTRtool.cc
        m_uhtr->setupClock( 2, 250 );
        toolbox::u_sleep( 500000 ); //settle clocks
        //These parameters need not be runtime configurable.
        //std::vector<uint32_t> orbitdelay(2*12, 128); //not to be confused with orbit phase; this is a parameter for the 5 Gbps links.
	std::vector<uint32_t> orbitdelay(2*12, 178); //not to be confused with orbit phase; this is a parameter for the 5 Gbps links. ///SJ - changed as suggested by Jeremy in Jan, 2022 to make it larger than the write delay.

        bool enable_cdr_autoreset = false;
        std::vector<bool> use_qiereset_asmark(2*12, false);
        bool do_resets = true;
        bool text_info = true;
	
	//26th April, 2022 - SJ
	int pipelineZeroPhaseDelay = 0;  ///L1A pipeline for PPOD0 - the one which we use. We dont have any L1A pipeline
	int pipelineOnePhaseDelay = 0; /// L1A pipeline for PPOD0 - the one which we dont use
	
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Initializing links ( orbitdelay=" << orbitdelay.at(0) << ", cdrrst=" << enable_cdr_autoreset << ", qiemark=" << use_qiereset_asmark.at(0) << ", doresets=" << do_resets << " )" );
        m_uhtr->link_init_setup( orbitdelay, enable_cdr_autoreset, pipelineZeroPhaseDelay, pipelineOnePhaseDelay, use_qiereset_asmark ) ;
        m_uhtr->link_init( do_resets, text_info ) ;
        toolbox::u_sleep( 500000 ); //settle links
        m_uhtr->lumi_link_reset();
        toolbox::u_sleep( 10000000 ); //settle links
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }
/**
 * This function runs all possible diagnostics. Making no attempt to be clever, if it finds one problem it returns false, in which case re-init should be called
 */
    bool Uhtr::sanityCheck()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      try
       {
        SafeLocker locker( m_lock );
        if ( !m_uhtr->isBHM() )
         {
          LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": uHTR has incorrect firmware loaded !!!" );
          return false;
         }
        std::vector<bool> RXlos;
        std::vector<double> power;
        m_uhtr->sensors()->readPowerPPODs( power );
        m_uhtr->sensors()->readLOSPPODs( RXlos );
        uhal::ValWord<uint32_t> rx_reset[24];
        for (int i = 0; i < 24; i++)
         {
          char buf[32];
          sprintf( buf, "LINKS.RX%d_RESET", i );
          rx_reset[i] = m_uhtr->getFrontNode().getNode( buf ).read();
         }
        m_uhtr->getHW()->dispatch();
        //This double query was copied from uHTRtool.cc #8686, reason is unknown to me
        //actually needs more reads and more waits...
        m_uhtr->link_query();
        toolbox::u_sleep( 1000000 );
        m_uhtr->link_query();
        toolbox::u_sleep( 1000000 );
        m_uhtr->link_query();
        toolbox::u_sleep( 1000000 );
        m_uhtr->link_query();
        for ( int link = 3; link != 9; ++link )
          if ( rx_reset[link].value() == 1 || RXlos[link] || m_uhtr->info_link_BadDataCounterOn( link ) != 1 )
           {
            LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": Link " << link << " is not active: RX rst = " << rx_reset[link].value() << "; RXlos = " << RXlos[link] << "; Baddatactr = " << m_uhtr->info_link_BadDataCounterOn( link ) );
            return false;
           }
        const float min_orb_rate = 11.2f;
        const float max_orb_rate = 11.3f;
        for ( int link = 3; link != 9; ++link )
          if ( ( m_uhtr->info_link_orbitRate( link ) < min_orb_rate ) || ( m_uhtr->info_link_orbitRate( link ) > max_orb_rate ) )
           {
            LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": Link " << link << " orbit rate incorrect " << m_uhtr->info_link_orbitRate( link ) );
            return false;
           }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return true;
     }

/**
 * Load Configuration into the uHTR
 */
    void Uhtr::loadConfig()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      int n_orbits = 0x1000 * r_cfg.integrate_nibbles;
      //thresholds
      std::vector<uint8_t> thresholds( 24 );
      for ( size_t ch = 0; ch != 24; ++ch )
        thresholds[ch] = toolbox::toUnsignedLong( r_cfg.channels[ch].getProperty( "uhtr_threshold" ) );
      //tdcmap
      auto lst = toolbox::parseTokenList( r_cfg.uhtr_tdcmap, "," );
      if ( lst.size() != unsigned( hcal::uhtr::uHTR::BHM_N_TDCMAP ) )
        XCEPT_RAISE( exception::ConfigurationError, "There should be 64 values in the TDC map" );
      std::vector<uint8_t> tdcmap( hcal::uhtr::uHTR::BHM_N_TDCMAP );
      std::transform( lst.begin(), lst.end(), tdcmap.begin(), toolbox::toUnsignedLong );
      if ( *std::max_element( tdcmap.begin(), tdcmap.end() ) > 4 )
        XCEPT_RAISE( exception::ConfigurationError, "TDC map values should be in the range 0-4" );
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_setup( n_orbits, r_cfg.uhtr_orbit_phase, thresholds, tdcmap );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

/**
 * Retrieve configuration from the uHTR
 */
    void Uhtr::readConfig()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      int n_orbits, orbitphase;
      std::vector<uint8_t> thresholds, tdcmap;
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_read_setup( n_orbits, orbitphase, thresholds, tdcmap );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      r_cfg.integrate_nibbles = n_orbits / 0x1000;
      r_cfg.uhtr_orbit_phase = orbitphase;
      //tdcmap
      std::ostringstream ostr;
      std::copy( tdcmap.begin(), tdcmap.end() - 1, std::ostream_iterator<int>( ostr, "," ) );
      ostr << int( tdcmap.back() );
      r_cfg.uhtr_tdcmap = ostr.str();
      //thresholds
      for ( size_t ch = 0; ch != 24; ++ch )
        r_cfg.channels[ch].setProperty( "uhtr_threshold", std::to_string( (unsigned long long) thresholds[ch] ) );
     }

/**
 * Occupancy workloop main function.
 *
 * This function retrieves occupancy histograms when available, and pushes them into the publication queue.
 *
 * @returns An std::vector of Histograms, empty if they were not retrieved
 */
    Uhtr::OccupancyHistogramList Uhtr::getOccupancy()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      try
       {
        SafeLocker locker( m_lock );
        const uhal::Node& bbhm = m_uhtr->getBackNode().getNode( "BHM" );
        auto available = bbhm.getNode( "DATA_AVAILABLE" ).readBlock( 3 );
        auto overflow = bbhm.getNode( "DATA_LOST" ).readBlock( 3 );
        m_uhtr->getHW()->dispatch();
        bool ava = ( available[0] == 0xffffffff ) && ( available[1] == 0xffffffff ) && ( available[2] == 0xff );
        bool ovf = ( overflow[0] || overflow[1] || overflow[2] );
        LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ << ": Histograms available " << ava << "; in overflow " << ovf );
        OccupancyHistogramList histos;
        if ( !ava ) //not ready yet
         {
          return histos;
         }
        if ( ovf ) //the histograms are in overflow, probably network congestion or something... Advance pointer without actually reading (data is lost)
         {
          bbhm.getNode( "ADVANCE_POINTER" ).write( 0xffffffffu );
          m_uhtr->getHW()->dispatch();
          return histos;
         }
        histos.resize( 24 );
        for ( int i = 0; i != 24; ++i )
         {
          histos[i].index = i;
          histos[i].h = std::vector<uint32_t>( 3564 * 4, 0 );
         }
        uint32_t base = bbhm.getNode( "HISTS" ).getAddress();
        for ( int ic = 0; ic != 24; ++ic )
         {
          auto payload = m_uhtr->getBackNode().getClient().readBlock( base + 0x1000 * ic, 3564 );
          auto trailer = m_uhtr->getBackNode().getClient().readBlock( base + 0x1000 * ic + 0xFE0, 32 );
          m_uhtr->getHW()->dispatch();
          //undo the packing
          for ( int ibin = 0; ibin != 3564; ++ibin )
           {
            histos[ic / 3 * 3 + 0].h[ibin * 4 + ic % 3] = ( payload[ibin] >>  0 ) & 0x1FF;
            histos[ic / 3 * 3 + 1].h[ibin * 4 + ic % 3] = ( payload[ibin] >> 10 ) & 0x1FF;
            histos[ic / 3 * 3 + 2].h[ibin * 4 + ic % 3] = ( payload[ibin] >> 20 ) & 0x1FF;
           }
          //metadata
          auto unpktrail = [trailer]( int ofst ) { return ( trailer[ofst] & 0xff ) | ( trailer[ofst + 1] & 0xff ) * 0x100 | ( trailer[ofst + 2] & 0xff ) * 0x10000 | ( trailer[ofst + 3] & 0xff ) * 0x1000000; };
          //here actually the metadata for one channel every three is taken for simplicity for all three (following uHTR_lumi code)
          for ( int j = 0; j != 3; ++j )
           {
            histos[ic / 3 * 3 + j].lumi_nibble = unpktrail( 0 );
            histos[ic / 3 * 3 + j].lumi_section = unpktrail( 4 );
            histos[ic / 3 * 3 + j].cms_run = unpktrail( 8 );
            histos[ic / 3 * 3 + j].lhc_fill = unpktrail( 12 );
            histos[ic / 3 * 3 + j].overflow = trailer[17];
            histos[ic / 3 * 3 + j].orb_init = unpktrail( 19 );
            histos[ic / 3 * 3 + j].orb_last = unpktrail( 23 );
           }
          LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ << ": LN = " << unpktrail( 0 ) << "; LS = " << unpktrail( 4 ) << "; RUN = " << unpktrail( 8 ) << "; FILL = " << unpktrail( 12 ) );
         }
        bbhm.getNode( "ADVANCE_POINTER" ).write( 0xffffffffu );
        m_uhtr->getHW()->dispatch();
        return histos;
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

/**
 * Amplitude workloop main function.
 *
 * This function retrieves amplitude histograms, and pushes them into the publication queue.
 * The interface to acquire amplitude histograms requires timing control by sw, which makes it imprecise.
 * As a result, histograms will not have a perfect correspondance to nibbles, nor an exact entry count.
 *
 * @note The uhtr lock is only held during actual access, and released while sleeping or doing other things...
 *
 * @returns A structure holding the list of histogram and some run info
 */
    Uhtr::AmplitudeHistogramList Uhtr::getAmplitude()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      AmplitudeHistogramList list;
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_ampl_histo_control( false, true ); //set the clear flag
        locker.unlockedwait( 100000 ); //allow time to clear (conservative)
        m_uhtr->bhm_ampl_histo_control( true, false ); //set run flag and reset clear flag
        locker.unlockedwait( 1300000 ); //integration time (approximately four nibbles), replace with loop checking m_uhtr run info and counting 4 nibbles...
        m_uhtr->bhm_ampl_histo_control( false, false ); //stop everything
        m_uhtr->get_run_info( true, list.nb, list.ls, list.run, list.fill );
        for ( int link = 3; link != 9; ++link ) //fiber index
          for ( int lc = 0; lc != 4; ++lc ) //channel within fiber
           {
            int ch = ( link - 3 ) * 4 + lc;
            for ( int tdcbin = 1; tdcbin != 5; ++tdcbin )
              list.histo[ch][tdcbin - 1] = m_uhtr->bhm_ampl_histo_read( link, lc, tdcbin );
           }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return list;
     }

/**
 * Diagnostic function to read a snapshot of data link
 *
 * @returns a vector of parsed, human readable link data for display
 */
    std::vector<std::string> Uhtr::getLinkFragments( size_t len )
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      std::vector<std::string> strings;
      try
       {
        SafeLocker locker( m_lock );
        //Link spy (raw unaligned)
        m_uhtr->link_spySetup( true, false, false, true, false, 1, 0 );
        m_uhtr->link_spyExecute();
        std::vector<uint32_t> buffers[6];
        size_t startidx[6] = { 0, 0, 0, 0, 0, 0 };
        for ( int link = 3; link != 9; ++link )
         {
          buffers[link - 3] = m_uhtr->link_spyRead( link );
          while ( startidx[link - 3] < 12 )
           {
            uint8_t b0 = buffers[link - 3][startidx[link - 3]] & 0xFF;
            uint8_t b6 = buffers[link - 3][startidx[link - 3] + 6] & 0xFF;
            if ( ( b0 == 0xBC && b6 == 0xBC ) || ( b0 == 0xBC && b6 == 0x7C ) || ( b0 == 0x7C && b6 == 0xBC ) )
              break;
            ++startidx[link - 3];
           }
         }
        size_t offset = 0;
        while ( len-- > 0 )
         {
          uint16_t samples[6];
          std::stringstream ss;
          ss.fill( '0' );
          ss << std::setw( 4 ) << offset << ":\n";
          strings.push_back( ss.str() );
          ss.str( "    " );
          bool bc0;
          int capId[24];
          uint8_t adcs[24], tdc_le[24], tdc_te[24], tdce[24];
          for ( int link = 3; link != 9; ++link )
           {
            auto p = buffers[link - 3].begin() + startidx[link - 3] + offset;
            std::copy( p, p + 6, samples );
            auto ch = 4 * ( link - 3 );
            bool valid = hcal::uhtr::unpack_link_4chan( samples, bc0, capId + ch, adcs + ch, tdc_le + ch, tdc_te + ch, tdce + ch );
            if ( !valid )
             {
              ss << "Invalid data! Unpacking failed.\n";
              strings.push_back( ss.str() );
              break;
             }
            ss << " comma: " << ( bc0 ? "BC0" : " - " ) << ' ';
           }
          ss << '\n';
          strings.push_back( ss.str() );
          ss.str( "    " );
          for ( auto c : capId )
            ss << ' ' << std::setw( 2 ) << int( c );
          ss << '\n';
          strings.push_back( ss.str() );
          ss.str( "    " );
          for ( auto c : adcs )
            ss << ' ' << std::setw( 2 ) << int( c );
          ss << '\n';
          strings.push_back( ss.str() );
          ss.str( "    " );
          for ( auto c : tdc_le )
            ss << ' ' << std::setw( 2 ) << int( c );
          ss << '\n';
          strings.push_back( ss.str() );
          ss.str( "    " );
          for ( auto c : tdce )
            ss << ' ' << std::setw( 2 ) << int( c );
          ss << '\n';
          strings.push_back( ss.str() );
          offset += 6;
         }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return strings;
     }

/**
 * Diagnostic function to histogram link data.
 *
 * @returns A list of histograms
 */
    std::vector<Uhtr::AmplitudeHistogram> Uhtr::getLinkHistograms( size_t integration )
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      std::vector<AmplitudeHistogram> histos( 24, AmplitudeHistogram( 256, 0 ) );
      size_t nreads = integration * 6 / 2048;
      try
       {
        SafeLocker locker( m_lock );
        //Link spy (raw unaligned)
        m_uhtr->link_spySetup( true, false, false, true, false, 1, 0 );
        for ( size_t n = 0; n != nreads; ++n )
         {
          m_uhtr->link_spyExecute();
          std::vector<uint32_t> buffers[6];
          size_t startidx[6] = { 0, 0, 0, 0, 0, 0 };
          for ( int link = 3; link != 9; ++link )
           {
            buffers[link - 3] = m_uhtr->link_spyRead( link );
            while ( startidx[link - 3] < 12 )
             {
              uint8_t b0 = buffers[link - 3][startidx[link - 3]] & 0xFF;
              uint8_t b6 = buffers[link - 3][startidx[link - 3] + 6] & 0xFF;
              if ( ( b0 == 0xBC && b6 == 0xBC ) || ( b0 == 0xBC && b6 == 0x7C ) || ( b0 == 0x7C && b6 == 0xBC ) )
                break;
              ++startidx[link - 3];
             }
           }
          size_t offset = 0;
          while ( offset < 2043 )
           {
            uint16_t samples[6];
            bool bc0;
            int capId[4];
            uint8_t adcs[4], tdc_le[4], tdc_te[4], tdce[4];
            for ( int link = 3; link != 9; ++link )
             {
              auto p = buffers[link - 3].begin() + startidx[link - 3] + offset;
              std::copy( p, p + 6, samples );
              bool valid = hcal::uhtr::unpack_link_4chan( samples, bc0, capId, adcs, tdc_le, tdc_te, tdce );
              if ( valid )
                for ( int ch = 0; ch != 4; ++ch )
                  ++histos[( link - 3 ) * 4 + ch][adcs[ch]];
             }
            offset += 6;
           }
         }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return histos;
     }

   }

 }

