#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>
#include <tuple>

#include <jansson.h>

#include "bril/bhmsource/easywsclient.hpp"
//#include "bril/bhmsource/json.h"
#include "bril/bhmsource/exception/Exception.h"
#include "bril/bhmsource/FrontEnd.h"


namespace bril
 {

  namespace bhmsource
   {

    using easywsclient::WebSocket;

    class ngCCMClient
     {

      struct dispatcher
       {
        dispatcher( ngCCMClient& parent ): r_parent( parent ){}
        ngCCMClient& r_parent;
        void operator() ( const std::string& message ) { r_parent.m_tmp_message = message; r_parent.m_pending = false; }
       };

      friend class dispatcher;

      public:
      ngCCMClient( std::string url ) :
       m_socket( WebSocket::from_url( url ) ),
       m_pending( false )
       {
        if ( !m_socket || m_socket->getReadyState() == WebSocket::CLOSED )
          XCEPT_RAISE( exception::CommunicationError, "Cannot connect to websocket: " + url );
       }

      ~ngCCMClient()
       {
        if ( m_socket )
          m_socket->close();
       };

      std::string get( const std::string& );

      void put( const std::string&, const std::string& );

      private:
      json_t* execute( const json_t* );

      private:
      WebSocket::pointer m_socket;
      bool m_pending;
      std::string m_tmp_message;

     };

    std::string ngCCMClient::get( const std::string& varname )
     {
//      Json::Value command( Json::objectValue );
//      command["jsonrpc"] = "2.0";
//      command["method"] = "getv";
//      Json::Value params( Json::objectValue );
//      params["name"] = varname;
//      command["params"] = params;
      json_t* params = json_pack( "{s:s}", "name", varname.c_str() );
      json_t* command = json_pack( "{s:s, s:s, s:o}", "jsonrpc", "2.0", "method", "getv", "params", params );
      json_t* root = execute( command );
      json_t* retparams = json_object_get( root, "params" );
      std::string line = json_string_value( json_object_get( retparams, "line" ) );
      json_t* valid = json_object_get( retparams, "valid" );
      if ( json_is_false( valid ) )
        XCEPT_RAISE( exception::HardwareError, "Error reading " + varname + ": " + line.substr( line.find( "ERROR!!" ) + 8 ) );
      return line.substr( line.find( '#' ) + 2 );
     }

    void ngCCMClient::put( const std::string& varname, const std::string& value )
     {
//      Json::Value command( Json::objectValue );
//      command["jsonrpc"] = "2.0";
//      command["method"] = "putv";
//      Json::Value params( Json::objectValue );
//      params["name"] = varname + " " + value;
//      command["params"] = params;
      json_t* params = json_pack( "{s:s}", "name", ( varname + " " + value ).c_str() );
      json_t* command = json_pack( "{s:s, s:s, s:o}", "jsonrpc", "2.0", "method", "putv", "params", params );
      json_t* root = execute( command );
      json_t* retparams = json_object_get( root, "params" );
      std::string line = json_string_value( json_object_get( retparams, "line" ) );
//      std::string line = root["params"]["line"].asString();
//      if ( !root["params"]["valid"].asBool() )
      json_t* valid = json_object_get( retparams, "valid" );
      if ( json_is_false( valid ) )
        XCEPT_RAISE( exception::HardwareError, "Error writing " + varname + ": " + line.substr( line.find( "ERROR!!" ) + 8 ) );
     }

    json_t* ngCCMClient::execute( const json_t* request )
     {
//      Json::FastWriter writer;
      char* cstr = json_dumps( request, JSON_COMPACT );
//      std::string out_string = writer.write( request );
      m_socket->send( std::string( cstr ) );
      ::free( cstr );
      m_pending = true;
      //query result
      int maxtime = 30; //30 seconds timeout
      while ( m_socket->getReadyState() != WebSocket::CLOSED && m_pending && --maxtime )
       {
        m_socket->poll( 1000 );
        m_socket->dispatch( dispatcher( *this ) );
       }
      if ( m_pending )
        XCEPT_RAISE( exception::CommunicationError, "Websocket closed or timed out." );
      //decode result
//      Json::Reader jreader; 
      json_error_t jerr;
      json_t* root = json_loads( m_tmp_message.c_str(), 0, &jerr );
      if ( !root )
        XCEPT_RAISE( exception::CommunicationError, "Error parsing jsonrpc: " + m_tmp_message + ": " + jerr.text );
      return root;
     }


    FrontEnd::FrontEnd( Configuration& cfg, xdaq::Application* ) :
     r_cfg( cfg ),
     m_ngccm( 0 ),
     m_calpresent( true )
     {
      m_ngccm = new ngCCMClient( r_cfg.ngccmsrv_uri );
      std::stringstream prefix;
      prefix << "BHM-" << r_cfg.rm_slot.toString() << '-';
      m_varprefix = prefix.str();
      //One time initialization of the QIE board...
      m_ngccm->get( m_varprefix + "B_FIRMVERSION_MAJOR" ); //this just so that failure will throw
      //One time initialization of the Calibration Mezzanine
      try //since the calibration mezzanine might be missing, this failure is not fatal, but it permanently disables calibration
       {
        m_ngccm->put( m_varprefix + "CalMez-Temperature-Ctrl", "0xE0" ); //max resolution
        m_ngccm->put( m_varprefix + "CalMez-SiPM", "0x3FFF" ); //reduce bias as much as possible
        m_ngccm->put( m_varprefix + "CalMez-SiPM", "0x0FFF" );
        m_ngccm->put( m_varprefix + "CalMez-SiPM", "0xF024" ); //enable channels
        m_ngccm->put( m_varprefix + "CalMez-Plsr-Top-Cmdr", "0x07" ); // RSEL2-0 -> 1, others -> 0
        m_ngccm->put( m_varprefix + "CalMez-Plsr-Bot-Cmdr", "0x07" );
       }
      catch ( ... )
       {
        m_calpresent = false;
       }
     }

    FrontEnd::~FrontEnd()
     {
      delete m_ngccm;
     }

    bool FrontEnd::alive()
     {
      if ( !m_ngccm )
        return false;
      try
       {
        m_ngccm->put( m_varprefix + "B_SCRATCH", "0x12345678" );
        return m_ngccm->get( m_varprefix + "B_SCRATCH" ) == "0x12345678";
       }
      catch ( ... )
       {
        return false;
       }
     }

    void FrontEnd::loadConfig()
     {
      //RM configuration
      std::stringstream phases;
      std::stringstream qiethresh;
      for ( auto ch = r_cfg.channels.begin(); ch != r_cfg.channels.end(); ++ch )
       {
        phases << ' ' << ch->getProperty( "clk_phase" );
        qiethresh << ' ' << ch->getProperty( "qie_threshold" );
       }
      m_ngccm->put( m_varprefix + "Qie[1-24]_ck_ph", phases.str() );
      m_ngccm->put( m_varprefix + "QIE[1-24]_TimingThresholdDAC", qiethresh.str() );
     }

    void FrontEnd::setQieThreshold( std::string thrs )
     {
      std::stringstream qiethresh;
      for ( int i = 0; i != 24; ++i )
        qiethresh << ' ' << thrs;
      m_ngccm->put( m_varprefix + "QIE[1-24]_TimingThresholdDAC", qiethresh.str() );
     }

    void FrontEnd::readConfig()
     {
      std::stringstream phases( m_ngccm->get( m_varprefix + "Qie[1-24]_ck_ph" ) );
      std::stringstream qiethresh( m_ngccm->get( m_varprefix + "QIE[1-24]_TimingThresholdDAC" ) );
      for ( auto ch = r_cfg.channels.begin(); ch != r_cfg.channels.end(); ++ch )
       {
        std::string rd;
        phases >> rd;
        ch->setProperty( "clk_phase", rd );
        qiethresh >> rd;
        ch->setProperty( "qie_threshold", rd );
       }
     }

    void FrontEnd::startCalibration()
     {
      std::cerr << __func__ << std::endl;
      if ( !m_calpresent )
        return;
      //CalMez configuration
      auto reply = m_ngccm->get( m_varprefix + "CalMez-Temperature-Data" );
      uint16_t rdtemp = std::stoul( reply );
      std::ostringstream sipmtop, sipmbot, plsrtop, plsrbot, cfgstart, cfgstop;
      sipmtop << ( 0x3000 | max5841_cvt( r_cfg.sipm_bias_top, rdtemp ) );
      m_ngccm->put( m_varprefix + "CalMez-SiPM", sipmtop.str() );
      sipmbot << ( 0x0000 | max5841_cvt( r_cfg.sipm_bias_bot, rdtemp ) );
      m_ngccm->put( m_varprefix + "CalMez-SiPM", sipmbot.str() );
      plsrtop << lt3582_cvt( r_cfg.pulser_bias_top );
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Top-Ctrl", "0x22" ); //- -> 0, Lock -> 0, VPLUS -> 1, IRMP -> 00 (slowest ramp), PDDIS -> 0, PUSEQ -> 10
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Top-Data", plsrtop.str() );
      plsrbot << lt3582_cvt( r_cfg.pulser_bias_bot );
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Bot-Ctrl", "0x22" );
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Bot-Data", plsrbot.str() );
      //FIXME
      cfgstop << 0x000100FE + 0x00010000 * r_cfg.pulser_delay;
      m_ngccm->put( m_varprefix + "B_PulserB_Config", cfgstop.str() );
      cfgstart << 0x0000000E + 0x00010000 * r_cfg.pulser_delay;
      m_ngccm->put( m_varprefix + "B_PulserA_Config", cfgstart.str() );
     }

    void FrontEnd::stopCalibration()
     {
      std::cerr << __func__ << std::endl;
      if ( !m_calpresent )
        return;
      m_ngccm->put( m_varprefix + "CalMez-SiPM", "0x3FFF" ); //reduce bias as much as possible (about 20V)
      m_ngccm->put( m_varprefix + "CalMez-SiPM", "0x0FFF" );
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Top-Ctrl", "0" ); //cut power consumption (and reverse bias the LED by VCAL - 3.2V, not an issue)
      m_ngccm->put( m_varprefix + "CalMez-Plsr-Bot-Ctrl", "0" );
      m_ngccm->put( m_varprefix + "B_PulserA_Config", "0" );
      m_ngccm->put( m_varprefix + "B_PulserB_Config", "0" );
     }

    std::vector<float> FrontEnd::readTemperatures()
     {
      std::vector<float> temps;
      temps.push_back( std::stof( m_ngccm->get( m_varprefix + "B_SHT_temp_f" ) ) );
      if ( m_calpresent )
       {
        uint16_t rawtemp = std::stoul( m_ngccm->get( m_varprefix + "CalMez-Temperature-Data" ) );
        temps.push_back( ( rawtemp >> 4 ) / 0.0625f );
       }
      return temps;
     }

    uint16_t FrontEnd::max5841_cvt( float bias, uint16_t rawtemp )
     {
      if ( rawtemp != 0xFFFF ) //correct for temperature dependent breakdown voltage
       {
        const float reference_temp = 25.f; // (initial value measured by the on board probe TMP100)
        const float bvtc = 0.026f; //26 mV/C from NUV SiPM datasheet
        float realtmp = ( rawtemp >> 4 ) / 0.0625f;
        float deltat = realtmp - reference_temp;
        bias += deltat * bvtc;
       }
      const float offset = 30.0;
      const float slope = -0.008192;
      uint16_t vset = ( bias - offset ) / slope + 0x0B00;
      return vset & 0x0FFC;
     }

    uint16_t FrontEnd::lt3582_cvt( float bias )
     {
      std::cerr << __func__ << '(' << bias << ')' << std::endl;
      //VPout = 3.2V + 50mV * VPset + Vplus * 25mV, with VPset 0..191, Vplus set to 1
      const float offset = 3.45;
      const float slope = 0.05;
      uint16_t vpset = ( bias - offset ) / slope;
      if ( vpset >= 192 )
        XCEPT_RAISE( exception::ConfigurationError, "LED bias " + std::to_string( (long double) bias ) + " out of range." );
      return vpset;
     }

   }

 }
