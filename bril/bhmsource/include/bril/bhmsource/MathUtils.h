#ifndef _bril_onlinedqm_MathUtils_h_
#define _bril_onlinedqm_MathUtils_h_
#include <string>
#include <math.h>
#include <vector>
#include <stdexcept>
namespace bril{ namespace bhmsource { namespace MathUtils{
      
      // maximum of T array[len]
      template < typename T > 
	T getMax(const T* data, size_t len){
	if(!data){
	  throw std::invalid_argument("MathUtils::getMax invalid input address");
	}
	T maximum = data[0];
	for (size_t i = 1; i < len; ++i) {
	  if (maximum < data[i]) maximum = data[i];
        }
	return maximum;
      }

      // minimum of T array[len]
      template < typename T > 
	T getMin(const T* data, size_t len){
	if(!data){
	  throw std::invalid_argument("MathUtils::getMin invalid input address");
	}
	T minimum = data[0];
	for (size_t i = 1; i < len; ++i) {
	  if (minimum > data[i]) minimum = data[i];
        }
	return minimum;
      }

      // mean value of T array[len]
      template < typename T > 
	double getMean(const T* data, size_t len){
	if(!data){
	  throw std::invalid_argument("MathUtils::getMean invalid input address");
	}	
	double sum = 0;
	for(size_t i = 0; i<len; ++i){
	  sum += data[i];
	}
	return len==0 ? 0 : (sum/len);
      }

      // variance = (val-mean)^2 + (val-mean)^2
      template < typename T > 
	double getVariance(const T* data, size_t len){
	double mean = getMean(data,len);
	double temp = 0;
	for(size_t i = 0; i<len; ++i ){
	  temp += (data[i] - mean) * (data[i] - mean);	  
	}
	return len==0 ? 0 : (temp/len);
      }
      
      // StdDeviation of T array[len] , sd = sqrt(variance) 
      template < typename T > 
	double getStdDeviation(const T* data, size_t len){
	  return sqrt(getVariance(data,len));
      }

      // mean value of a vector<T>
      template < typename T > 
	double getMean(const std::vector<T>& data){
	size_t len = data.size();
	const T* begAddress = data.empty() ? 0 : &data[0];
	return getMean(begAddress,len);
      }
      
      // variance of vector<T>: (val-mean)^2 + (val-mean)^2
      template < typename T > 
	double getVariance(const std::vector<T>& data){
	size_t len = data.size();
	const T* begAddress = data.empty() ? 0 : &data[0];
	return getVariance(begAddress,len);
      }
      
      // StdDeviation of vector<T>: sd = sqrt(variance) 
      template < typename T > 
	double getStdDeviation(const std::vector<T>& data){	
	return sqrt(getVariance(data));
      }
      
      // minimum of vector<T> 
      template < typename T > 
	T getMin(const std::vector<T>& data){
	size_t len = data.size();
	const T* begAddress = data.empty() ? 0 : &data[0];
	return getMin(begAddress,len);
      }
      
      // maximum of vector<T> 
      template < typename T > 
	T getMax(const std::vector<T>& data){
	size_t len = data.size();
	const T* begAddress = data.empty() ? 0 : &data[0];
	return getMax(begAddress,len);
      }      
      
}}}
#endif
