// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_bhmsource_version_h_
#define _bril_bhmsource_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILBHMSOURCE_VERSION_MAJOR 4
#define BRIL_BRILBHMSOURCE_VERSION_MINOR 1
#define BRIL_BRILBHMSOURCE_VERSION_PATCH 3 ///before 19th April, 2022, this was 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILBHMSOURCE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILBHMSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBHMSOURCE_VERSION_MAJOR,BRIL_BRILBHMSOURCE_VERSION_MINOR,BRIL_BRILBHMSOURCE_VERSION_PATCH)
#ifndef BRIL_BRILBHMSOURCE_PREVIOUS_VERSIONS
#define BRIL_BRILBHMSOURCE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBHMSOURCE_VERSION_MAJOR,BRIL_BRILBHMSOURCE_VERSION_MINOR,BRIL_BRILBHMSOURCE_VERSION_PATCH)
#else
#define BRIL_BRILBHMSOURCE_FULL_VERSION_LIST BRIL_BRILBHMSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBHMSOURCE_VERSION_MAJOR,BRIL_BRILBHMSOURCE_VERSION_MINOR,BRIL_BRILBHMSOURCE_VERSION_PATCH)
#endif

namespace brilbhmsource
{
        const std::string project = "bril"; 
	const std::string package = "brilbhmsource";
	const std::string versions = BRIL_BRILBHMSOURCE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bhmsource";
	const std::string description = "bril bhm readout";
	const std::string authors = "N.Tosi S.Orfanelli";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
