/**
 * @brief BHM Source xdaq Application.
 *
 * @author S. Orfanelli, N. Tosi \<nicko@cern.ch\>
 *
 * This Application queries the hardware for histograms and publishes them to the bhm bus.
 *
 */

#ifndef _bril_bhmsource_Application_h_
#define _bril_bhmsource_Application_h_

#include <list>
#include <map>
#include <set>
#include <string>

#include "b2in/nub/exception/Exception.h"

#include "bril/bhmsource/ChannelId.h"

#include "eventing/api/Member.h"

#include "interface/bril/BHMTopics.hh"
#include "interface/bril/BEAMTopics.hh"

#include "log4cplus/logger.h"

#include "toolbox/ActionListener.h"
#include "toolbox/BSem.h"
#include "toolbox/Condition.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "toolbox/task/TimerListener.h"

#include "xdaq/Application.h"

#include "xdata/ActionListener.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"
#include "xdata/Vector.h"

#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xgi/Method.h"
#include "xgi/Output.h"

#include "xoap/MessageReference.h"

#include "bril/bhmsource/BackEnd.h"
#include "bril/bhmsource/FrontEnd.h"

namespace toolbox
 {
  namespace task
   {
    class WorkLoop;
   }
 }

namespace bril
 {

  namespace bhmsource
   {

    class Application : public xdaq::Application,
                        public xgi::framework::UIManager,
                        public eventing::api::Member,
                        public xdata::ActionListener,
                        public toolbox::ActionListener,
                        public toolbox::task::TimerListener
     {

      public:
      typedef std::string BusName;
      typedef std::string TopicName;
      typedef std::set<BusName> BusNameSet;
      static const int s_nchannels = 24;

      class ThresholdScanner;

      public:
      XDAQ_INSTANTIATOR();

      Application( xdaq::ApplicationStub* );

      ~Application();

      virtual void Default( xgi::Input* in, xgi::Output* out ); // xgi(web) callback

      virtual void expertPage( xgi::Input* in, xgi::Output* out ); // xgi(web) callback for the expert page

      virtual void actionPerformed( xdata::Event& ); // infospace event callback

      virtual void actionPerformed( toolbox::Event& ); // toolbox event callback

      virtual void onMessage( toolbox::mem::Reference*, xdata::Properties& ); // b2in message callback

      virtual void timeExpired( toolbox::task::TimerEvent& ); // timer callback

      private:
      Application( const Application& );

      Application& operator = ( const Application& );

      private:
      void setupConfiguration();

      void setupEventing();

      void setupHardwareConfiguration();

      void setupMonitoring();

      void setupWorkloop();

      bool readOccupancyMain( toolbox::task::WorkLoop* );

      bool readAmplitudeMain( toolbox::task::WorkLoop* );

      bool publishMain( toolbox::task::WorkLoop* );

      void doPublish( const TopicName&, toolbox::mem::Reference*, const std::string& = std::string() );

      void occupancyStats( const Uhtr::OccupancyHistogramList& );

      template <typename T>
      std::pair<toolbox::mem::Reference*, T*> newPub( unsigned int, unsigned int, unsigned int );

      private: //xdaq stuff
      toolbox::BSem m_applock;
      std::map<TopicName, BusNameSet> m_output_topics_map;
      BusNameSet m_unready_buses;
      toolbox::mem::MemoryPoolFactory* m_pool_factory;
      toolbox::mem::Pool* m_mem_pool;
      toolbox::task::Timer* m_timer;
      toolbox::task::WorkLoop* m_readocc_wl;
      toolbox::task::WorkLoop* m_readamp_wl;
      toolbox::task::WorkLoop* m_publish_wl;
      bool m_shutdown;

      private: //bhm specific
      std::vector<bhm::ChannelId> m_channel_ids;
      Configuration m_cfg;
      Uhtr* m_uhtr;
      FrontEnd* m_front_end;
      toolbox::squeue<toolbox::mem::Reference*> m_occupancy; //has its own mutex
      toolbox::squeue<toolbox::mem::Reference*> m_amplitude; //has its own mutex
      double m_last_occupancy;
      int m_how_many_times_was_orbit_length_bad;
      bool m_next_data_bad;
      bool m_calibration_active;
      ThresholdScanner* m_qie_threshold_scan;

      private: //flashlist stuff
      xdata::InfoSpace* m_mon_infospace;
      std::list<std::string> m_mon_varlist;
      std::vector<xdata::Serializable*> m_mon_vars;
      xdata::String m_mon_beammode;
      xdata::UnsignedInteger m_mon_fill;
      xdata::UnsignedInteger m_mon_run;
      xdata::UnsignedInteger m_mon_ls;
      xdata::UnsignedInteger m_mon_nb;
      xdata::TimeVal m_mon_timestamp;
      xdata::UnsignedInteger m_mon_collected_histos;
      xdata::Vector<xdata::Integer> m_mon_quality;
      xdata::Vector<xdata::Float> m_mon_rawrate;
      xdata::Vector<xdata::Integer> m_mon_calpeak;

    };

   }

 }

#endif

