#ifndef _bril_hfsource_Application_h_
#define _bril_hfsource_Application_h_
#include <string>
#include <list>
#include <map>

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"

#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Table.h"
#include "xdata/Serializable.h"

#include "toolbox/squeue.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"

#include "hcal/uhtr/version.h"
#include "hcal/uhtr/uHTR.hh"
#include "interface/bril/BEAMTopics.hh"

#include "toolbox/BSem.h"

/**
   A xdaq application that when configured as simulation mode simulates data on bril timing signal; as real mode, it pushes data into readout queue and publish from queue to bril eventing.
   In real mode, it is forseen to use instance=0 for channel 1-20; instanceid=1 for channel 21-40
*/
const unsigned int nChannels = 4;
const unsigned int nFibers = 24;
const unsigned int nElements  = 256;
const unsigned int Nbytes_allocated = 12*1024*1024;
const bool compressLUT=true;

//forward declaration
namespace hcal{
  namespace uhtr{
    class uHTR;
  }
}

namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}

namespace bril{
  namespace hfsource{
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener {
  
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
    private:
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      xdata::String m_classname;
      xdata::String m_appURN;
      xdata::UnsignedInteger m_instanceid;

      // Configuration parameter
      xdata::String m_signalTopic;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      toolbox::BSem m_applock;
   
      // Buses and topics to subscribe
      xdata::String m_bus;
      xdata::String m_topics;
      std::set<std::string> m_topiclist;
      
      xdata::Integer m_orbits_per_nibble;         // lumi nibble size (integer number of orbits)
      xdata::Integer m_integration_period_nb;     // integration period in lumi nibbles
      xdata::Integer m_nibbles_per_section;       // lumi section size (integer number of lumi nibbles)
      xdata::Integer m_cms1_threshold;            // threshold for filling CMS1 occupancy histograms
      xdata::Integer m_cms2_threshold;            // threshold for filling CMS2 occupancy histograms
      xdata::Integer m_BXOffset;                  // bx shift for CMS_ET
      xdata::Integer m_OCBXOffset;                // bx shift for CMS_OC
      xdata::Integer m_orbit_phase;               // bx phase shift in histograms
      xdata::String  m_uhtrAddress;               // addresses of uHTR boards
      xdata::String  m_addressTable;              // path to file with name-to-address map of uHTR endpoints
      xdata::String  m_luts_path;                 // path to file with LUTs (empty for 1-to-1 LUTs)
      xdata::String m_histsToRead;                  // Name of Histograms to read
      xdata::String m_channels_to_mask;           // Channels to mask (will be converted in binary, one for 4 channels)
      xdata::UnsignedInteger m_crateId;           // crate number -> unsigned char datasourceid;
      xdata::UnsignedInteger m_slotId;            // board number -> unsigned char channelid;
      
      // WorkLoop related
      toolbox::task::WorkLoop* m_subscribing;
      toolbox::task::WorkLoop* m_publishing;
      
      // Topic queue
      typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > m_topicoutqueues;
     
    private:
      // uHTR related data, status, etc
    
      // HCAL uHTR class
      hcal::uhtr::uHTR* uHTR;
      std::string uhtrShortName;
      
      // Name of Histograms to read
      std::set<std::string> m_histNames;
    
        // Channel masking
      std::vector<bool> m_channel_disable_mask_custom;
      
      std::map<std::string,bool> m_lumi_hAvailable;
      std::map<std::string,bool> m_lumi_hOverflow;

      
    private:

      // current run status
      unsigned int m_currentfill;
      unsigned int m_currentrun;
      unsigned int m_currentls;
      unsigned int m_currentnibble;
      unsigned int m_timestamp;
      
      unsigned int m_previousfill;
      unsigned int m_previousrun;
      unsigned int m_previousls;
      unsigned int m_previousnibble;
      
      unsigned int lastPublish;
      unsigned int lastConfigure;
          
      toolbox::TimeVal timeNow;
    
      bool configuring;
      bool ready_to_publish;
      
      // Global/local
      bool m_local_flag, m_global_flag;
      
      std::stringstream debugStatement;
    
      std::map<std::string, unsigned int> m_stat_collected;
      typedef std::multimap<std::string, std::vector<uint32_t>>::iterator HISTOIterator;
      std::multimap<std::string, std::vector<uint32_t>> m_collected_histos;

    private:
        // Local variables we will receive from uHTR
        // Read Firmware
        uint32_t loc_firmwareRevision;
        bool loc_front;
        uint8_t loc_flavor, loc_major, loc_minor, loc_patch;
        
        // Lumi link status
        int loc_ilink; // 0 for HF
        uint32_t loc_status, loc_errors;
        float loc_lumi_bc0_rate, loc_trig_bc0_rate;
        
        // Lumi read status
        int loc_orbit_phase, loc_n_lumi_nibbles, loc_lhc_threshold, loc_cms1_threshold, loc_cms2_threshold;
        bool loc_disable_ln_bcast;
        std::vector<bool> loc_channel_disable_mask;


    private:
      
      // workloop methods
      bool publishing(toolbox::task::WorkLoop* wl);
      bool subscribing(toolbox::task::WorkLoop* wl);
      
      std::vector<bool> generate_mask( xdata::String & channels_to_mask );
    
      bool connect();
      bool disconnect();
      bool reconfigure();
      bool reset_link();
    
      bool read_status( hcal::uhtr::uHTR* uHTR );
      bool configure( hcal::uhtr::uHTR* uHTR );
      
      bool sanity_check( unsigned int &fill
                       , unsigned int &run
                       , unsigned int &ls
                       , unsigned int &nb );
      
      void do_publish ( const std::string& busname
                      , const std::string& topicname
                      , toolbox::mem::Reference* bufRef );
                      
      int  lumi_programlut();
      bool readlut(std::vector<uint16_t> lut[nFibers][nChannels]);
      
      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif

