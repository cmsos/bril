#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/squeue.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/hfsource/Application.h"
#include "bril/hfsource/WebUtils.h"
#include "bril/hfsource/MathUtils.h"
#include "bril/hfsource/exception/Exception.h"

#include <unistd.h>
#include "uhal/log/exception.hpp"
#include "uhal/ConnectionManager.hpp"
#include "uhal/HwInterface.hpp"

#include "interface/bril/HFTopics.hh"

#include <algorithm>

#include <iomanip>

XDAQ_INSTANTIATOR_IMPL (bril::hfsource::Application)

bril::hfsource::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application(s)
                                                    , xgi::framework::UIManager(this)
                                                    , eventing::api::Member(this)
                                                    , m_applock(toolbox::BSem::FULL)
                                                    {
    
    xgi::framework::deferredbind(this, this, &bril::hfsource::Application::Default, "Default");
    
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor= getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    
    m_BXOffset=1; //old default before HF firmware upgrade in Oct 2018
    m_OCBXOffset=-1; //old default before HF firmware upgrade in Oct 2018

    if( m_appDescriptor->hasInstanceNumber() ){
        m_instanceid = m_appDescriptor->getInstance();
    }

    toolbox::net::URN memurn("toolbox-mem-pool",m_classname.toString()+"_"+m_instanceid.toString()+"_mem");

    try {
        m_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");
        
        m_appInfoSpace->fireItemAvailable("signalTopic", &m_signalTopic);
        m_appInfoSpace->fireItemAvailable("uhtrAddress", &m_uhtrAddress);
        m_appInfoSpace->fireItemAvailable("bus", &m_bus);
        m_appInfoSpace->fireItemAvailable("topics", &m_topics);
        m_appInfoSpace->fireItemAvailable("histsToRead", &m_histsToRead);
        
        m_appInfoSpace->fireItemAvailable("orbits_per_nibble", &m_orbits_per_nibble);
        m_appInfoSpace->fireItemAvailable("integration_period_nb", &m_integration_period_nb);
        m_appInfoSpace->fireItemAvailable("nibbles_per_section", &m_nibbles_per_section);
        m_appInfoSpace->fireItemAvailable("cms1_threshold", &m_cms1_threshold);
        m_appInfoSpace->fireItemAvailable("cms2_threshold", &m_cms2_threshold);
        
        m_appInfoSpace->fireItemAvailable("BXOffset", &m_BXOffset);
        m_appInfoSpace->fireItemAvailable("OCBXOffset", &m_OCBXOffset);
        
        m_appInfoSpace->fireItemAvailable("orbit_phase", &m_orbit_phase);
        
        m_appInfoSpace->fireItemAvailable("crate", &m_crateId);
        m_appInfoSpace->fireItemAvailable("slot", &m_slotId);
        
        m_appInfoSpace->fireItemAvailable("channels_to_mask", &m_channels_to_mask);
        
        m_appInfoSpace->fireItemAvailable("addressTable", &m_addressTable);
        m_appInfoSpace->fireItemAvailable("luts_path", &m_luts_path);

        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool(memurn,allocator);
        m_appURN.fromString(m_appDescriptor->getURN());

        m_subscribing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_subscribing","waiting");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_publishing","waiting");

    } catch (xdata::exception::Exception& e) {
        std::string msg("Failed to setup appInfoSpace ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::hfsource::exception::Exception,msg,e);
    } catch(xcept::Exception& e){
        std::string msg("Failed to setup memory pool ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::hfsource::exception::Exception,msg,e);
    }
    
    // Set uHTR verbose level
    uhal::setLogLevelTo( uhal::Warning() );
            
    debugStatement << "UHTR VERSION: "
                   << "HCALUHTR_VERSION_MAJOR " << HCALUHTR_VERSION_MAJOR << ", "
                   << "HCALUHTR_VERSION_MINOR " << HCALUHTR_VERSION_MINOR << ", "
                   << "HCALUHTR_VERSION_PATCH " << HCALUHTR_VERSION_PATCH;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // Clean all TCDS/timing data
    timeNow = toolbox::TimeVal::gettimeofday();
    lastPublish = timeNow.sec();
    lastConfigure = timeNow.sec();
    configuring = false;
    
    // Protects from reading first block
    m_currentfill = 0;
    m_currentrun = 0;
    m_currentls = 0;
    m_currentnibble = 0;
        
    m_previousfill = 0;
    m_previousrun = 0;
    m_previousls = 0;
    m_previousnibble = 0;
    
    // data harvesting flag
    ready_to_publish = false;
    
    // Clean up histograms and their status
    for(std::set<std::string>::iterator histName = m_histNames.begin(); histName != m_histNames.end(); histName++){
        m_lumi_hAvailable[*histName]=false;
        m_lumi_hOverflow[*histName]=false;
    }

    m_stat_collected.clear();
    m_collected_histos.clear();
    
}

bril::hfsource::Application::~Application () {

    // Clear topic queue
    QueueStoreIt it;
    for (it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it ) {
        delete it->second;
    }
    m_topicoutqueues.clear();

    // Clean connections to uHTRs
    if ( uHTR ) {
        delete uHTR;
        uHTR = 0;
    }
    
}

void bril::hfsource::Application::Default (xgi::Input * in, xgi::Output * out) {

    // TODO: develop proper application page for improved monitoring and data quility checks
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
    *out << "<h2 align=\"center\"> Monitoring "<< appurl << "</h2>";
    *out << cgicc::br();
    *out << "<p> Eventing Status</p>";
    *out << busesToHTML();
    *out << cgicc::br();
    *out << "<p> HF Status</p>";
    *out << "<p> In local:" << m_local_flag << "</p>";
    *out << "<p> In global:" << m_global_flag << "</p>";
}

// Apply default settings
void bril::hfsource::Application::actionPerformed(xdata::Event& e) {
    
    // Setup default configurations (define topics, buses, apply parameters from config files)
    LOG4CPLUS_INFO(getApplicationLogger(), "Received xdata event " << e.type());
    if ( e.type() == "urn:xdaq-event:setDefaultValues" ) {
        
        // Subscribe to buses
        this->getEventingBus(m_bus.value_).addActionListener(this);
        
        if ( this->getEventingBus(m_bus.value_).canPublish() ) {
            LOG4CPLUS_INFO(getApplicationLogger(), "Eventing bus is ready at setDefaultValues");
        }
        
        // Parse Histogram topics to subscribe
        if( !m_histsToRead.value_.empty() ) {
            m_histNames = toolbox::parseTokenSet(m_histsToRead.value_,",");
        }
  
        // Define topic queues for every hist and instantiate it
        if(!m_topics.value_.empty()) {
            m_topiclist = toolbox::parseTokenSet(m_topics.value_,",");
        }
        
        for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
            m_topicoutqueues.insert( std::make_pair( *it
                                                   , new toolbox::squeue<toolbox::mem::Reference*>) );
        }
        
        // Construct uHTR name. TODO: could find more eligant solution
        uhtrShortName.clear();
        uhtrShortName = m_uhtrAddress.toString();
        uhtrShortName = ( uhtrShortName.substr(uhtrShortName.find("hcal-uhtr")
                        , uhtrShortName.find(":50001")));

        debugStatement << "uHTR name: " << uhtrShortName;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        
        m_crateId = std::atoi((uhtrShortName.substr(10,2)).c_str());
        m_slotId = std::atoi((uhtrShortName.substr(13,2)).c_str());
        
        // Generate custom masks
        m_channel_disable_mask_custom = generate_mask(m_channels_to_mask);

    }
}

// Read all status info possible for uHTR Lumi
bool bril::hfsource::Application::read_status( hcal::uhtr::uHTR* uHTR ) {
                
    // ==============================================================================
    // Check slot/crate/name ========================================================
    // ==============================================================================
    debugStatement << "uHTR name: " << uHTR->name() << ", "
                   << "uHTR crate: " << uHTR->crate() << ", "
                   << "uHTR slot: " << uHTR->slot();
    
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // ==============================================================================
    // Check basic link status ======================================================
    // ==============================================================================
    debugStatement << "is 4800: " << uHTR->is4800() << ", "
                   << "is 1600: " << uHTR->is1600() << ", "
                   << "is BHM: " << uHTR->isBHM() << ", "
                   << "is 5GbpsAsync: " << uHTR->is5GbpsAsync();

    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");


    // ==============================================================================
    // Retrieve firmware version ====================================================
    // ==============================================================================
    loc_front = false;
    loc_firmwareRevision = uHTR->firmwareRevision( loc_front
                                                 , loc_flavor
                                                 , loc_major
                                                 , loc_minor
                                                 , loc_patch );

    debugStatement << "firmwareRevision: " << loc_firmwareRevision << ", "
                   << "pcbRevision: " << uHTR->pcbRevision() << ", "
                   << "serialNumber: " << uHTR->serialNumber() << ", "
                   << "front: " << loc_front << ", "
                   << "flavor: " << int(loc_flavor) << ", "
                   << "major: " << int(loc_major) << ", "
                   << "minor: " << int(loc_minor) << ", "
                   << "patch: " << int(loc_patch);
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // ==============================================================================
    // Lumi link status =============================================================
    // ==============================================================================
    loc_ilink = 0; // 0 for HF / 1 for BHM
    uHTR->lumi_link_status( loc_ilink
                          , loc_status
                          , loc_lumi_bc0_rate
                          , loc_trig_bc0_rate
                          , loc_errors );

    debugStatement << "ilink: " << loc_ilink << ", "
                   << "status: " << loc_status << ", "
                   << "lumi_bc0_rate: " << loc_lumi_bc0_rate << ", "
                   << "trig_bc0_rate: " << loc_trig_bc0_rate << ", "
                   << "errors: " << loc_errors;
              
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // ==============================================================================
    // Read lumi setup ==============================================================
    // ==============================================================================
    uHTR->lumi_read_setup( loc_orbit_phase
                         , loc_n_lumi_nibbles
                         , loc_lhc_threshold
                         , loc_cms1_threshold
                         , loc_cms2_threshold
                         , loc_channel_disable_mask
                         , loc_disable_ln_bcast);
        
    debugStatement << "orbit_phase: " << loc_orbit_phase << ", "
                   << "n_lumi_nibbles: " << loc_n_lumi_nibbles << ", "
                   << "lhc_threshold: " << loc_lhc_threshold << ", "
                   << "cms1_threshold: " << loc_cms1_threshold << ", "
                   << "cms2_threshold: " << loc_cms2_threshold << ", "
                   << "channel_disable_mask_size: " << loc_channel_disable_mask.size() << ", "
                   << "disable_ln_bcast: " << loc_disable_ln_bcast << std::endl;
              
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
              
    // ==============================================================================

    return true;

}

bool bril::hfsource::Application::configure( hcal::uhtr::uHTR* uHTR ) {

    // ==============================================================================
    // Setup new lumi settings ======================================================
    // ==============================================================================
    int num_prog = lumi_programlut();
    if (num_prog < 0)
        throw std::runtime_error("LUT programming failed");
    
    uHTR->lumi_setup( m_orbit_phase.value_
                    , m_integration_period_nb.value_
                    , m_cms1_threshold.value_
                    , m_cms1_threshold.value_
                    , m_cms2_threshold.value_
                    , m_channel_disable_mask_custom
                    , false);
    
    // ==============================================================================
    return true;

}

// Generate custom mask for the boards with XDAQ string provided (could also read from files)
std::vector<bool> bril::hfsource::Application::generate_mask( xdata::String & channels_to_mask ) {
                                                    
    std::vector<bool> channel_disable_mask_custom;
    std::vector<int> mask_list;

    // Parse and apply custom mask
    bool sanity(true);
    
    //channel_disable_mask_custom.clear();
    std::list<std::string> channels_mask_str = toolbox::parseTokenList(m_channels_to_mask, ",");
    
    if ( channels_mask_str.size() != nFibers) {
        sanity = false;
        LOG4CPLUS_DEBUG( getApplicationLogger()
                       , "Something went wrong during the mask parsing. Missing values for fibers");
    } else {
        for ( std::string chn : channels_mask_str ) {
            int chn_int = std::stoi(chn);
            if (chn_int < 0 || chn_int > 15) {
                sanity = false;
                LOG4CPLUS_DEBUG( getApplicationLogger()
                               , "Channel mask is out of acceptable range of values (0:15). Check numbers in configuration file");
            } else {
                mask_list.push_back(chn_int);
            }
        }
    }
    
    // If sanity failed construct default mask
    if ( !sanity ) {
        mask_list.clear();
        int maskPerFiber[]={3, 15, 12, 3, 15, 12, 15, 15, 15, 15, 15, 15, 3, 15, 12, 3, 15, 12, 15, 15, 15, 15, 15, 15};
        
        for ( auto &mask : maskPerFiber ) {
            mask_list.push_back(mask);
        }
        LOG4CPLUS_ERROR(getApplicationLogger(), "Default fiber mask had to be applied");
    }
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Generating mask for fibers and channels ");
    for ( unsigned int iFiber = 0; iFiber < nFibers; iFiber++ ) {
        for ( unsigned int ch = 0; ch < nChannels; ch++ ) {
            channel_disable_mask_custom.push_back(mask_list[iFiber] & 1<<ch);
        }
    }
    
    return channel_disable_mask_custom;
}

bool bril::hfsource::Application::sanity_check( unsigned int &fill, unsigned int &run, unsigned int &ls, unsigned int &nb ) {

    // By default consider TCDS information true
    bool sane = true;
    
    // Increased fill number up to 20000 since we are rapidly approaching
    if ( fill < 0 || fill > 20000) {
        sane = false;
    } else if ( run < 0 || run > 999999) {
        sane = false;
    } else if ( ls < 0 || ls > 999999) {
        sane = false;
    } else if ( nb < 0 || nb > 64) {
        sane = false;
    }
    return sane;
}

bool bril::hfsource::Application::reset_link() {

    bool reset(false);

    // reset link
    try {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Trying to reset the link");
        uHTR->lumi_link_reset();
        // Give time for clocks to settle
        usleep(50000); // 50ms
        reset = true;
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Link reset was succesfull during connect");
    } catch(uhal::exception::exception& e) {
        debugStatement << "Failed to lumi_link_reset in uHTR " << m_crateId << ", " << m_slotId;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }
    
    return reset;
}

bool bril::hfsource::Application::reconfigure() {
    
    // Simply reconfigure procedure called by expired timer
    configuring = true;
    
    usleep(50000);
    disconnect();
    usleep(50000);
    connect();
    
    return true;
}

bool bril::hfsource::Application::disconnect() {
    
    try{
        delete uHTR;
        uHTR=0;
        LOG4CPLUS_INFO(getApplicationLogger(), "Deleted uHTR and set pointer to 0.");
    }catch(uhal::exception::exception& e){
        LOG4CPLUS_INFO(getApplicationLogger(), "Couldn't delete uHTR... setting pointer to 0.");
        uHTR=0;
    }
    return true;
}

bool bril::hfsource::Application::connect() {

    bool connected(false);
    configuring = true;
    
    //unsigned int counter(1);
    
    // Constructor for uHTR part
    debugStatement << "Connecting to uHTR: " << uhtrShortName;
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
            
    try {
        uhal::HwInterface hw = uhal::ConnectionManager::getDevice( uhtrShortName
                                                                 , m_uhtrAddress.value_
                                                                 , m_addressTable.value_);
        uHTR = new hcal::uhtr::uHTR(uhtrShortName,hw);
        uHTR->setCrateSlot(m_crateId, m_slotId);
        connected = true;
            
        debugStatement << "Succesfully connected uHTR: " << uhtrShortName;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

    } catch (uhal::exception::exception& e) {
        debugStatement << "Failed to connect uHTR: " << uhtrShortName;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        connected = false;
    }
    
    try {
        // Check setup status here and config
        // Reset the link
        connected = reset_link();
        connected = read_status( uHTR );
        connected = configure( uHTR );
        
    } catch (xdata::exception::Exception& e) {
        std::string msg("Failed to setup uHTRs ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::hfsource::exception::Exception,msg,e);
    }
    
    configuring = false;
    
    timeNow = toolbox::TimeVal::gettimeofday();
    lastConfigure = timeNow.sec();
    
    return connected;
}

void bril::hfsource::Application::actionPerformed(toolbox::Event& e){
    
    LOG4CPLUS_INFO(getApplicationLogger(), "Received toolbox event " << e.type());
    
    if ( e.type() == "eventing::api::BusReadyToPublish" ) {
        std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
        
        std::stringstream msg;
        msg<< "Eventing bus '" << busname << "' is ready to publish";
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        
        std::string timername(m_appURN.toString()+"_monpusher");
        try {
            // check timer every 10 seconds to check uHTR is publishing and restart if neccessary
            toolbox::TimeInterval monpushInterval(10,0);
            toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate(start, this, monpushInterval, 0, timername);
        } catch ( toolbox::exception::Exception& e ) {
            std::stringstream msg;
            msg << "failed to start timer "<<timername;
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::hfsource::exception::Exception,msg.str(),e);
        }
        
        // First connect to uHTR
        connect();
                            
        // Activate workloops
        toolbox::task::ActionSignature* as_taking =
            toolbox::task::bind(this, &bril::hfsource::Application::subscribing, "subscribing");
        m_subscribing->activate();
        m_subscribing->submit(as_taking);
        
        toolbox::task::ActionSignature* as_publishing =
            toolbox::task::bind(this, &bril::hfsource::Application::publishing,"publishing");
        m_publishing->activate();
        m_publishing->submit(as_publishing);
        
    }
   
}

void bril::hfsource::Application::timeExpired(toolbox::task::TimerEvent& e) {

    // If last publish more than 5 seconds ago and not in local do reconfigure
    timeNow = toolbox::TimeVal::gettimeofday();
    if ( !uHTR ) {
        LOG4CPLUS_INFO(getApplicationLogger(), "uHTR is not responding, trying to reconnect");
        connect();
        LOG4CPLUS_INFO(getApplicationLogger(), "In timer, reconnected succesfully");
    } else if (timeNow.sec() - lastPublish > 5) {
        LOG4CPLUS_INFO(getApplicationLogger(), "No publication for more than 5 s, trying to reconnect");
        reconfigure();
        LOG4CPLUS_INFO(getApplicationLogger(), "Reconnected succesfully (in timer)");
    } else {
        debugStatement << "At time: " << timeNow.sec() << ", last publish: " << lastPublish << ", uHTR is ok";
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str() );
        debugStatement.str("");
    }
}

bool bril::hfsource::Application::subscribing(toolbox::task::WorkLoop *wl) {

    // Starting data harvesting workloop
    if ( configuring ) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Configuration in process. Skipping histogram");
        usleep(100000);
        return true;
    }
    
    // Wait at least one second after reconfiguration before collecting data
    timeNow = toolbox::TimeVal::gettimeofday();
    if ( (timeNow.sec() - lastConfigure) < 1 ) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Configuration was less than 1s ago. Skipping histogram");
        usleep(100000);
        return true;
    }
    
    if ( !uHTR ) {
        // If connection is lost - wait until clocks will force reconfigure!
        usleep(100000);
        return true;
    }
    
    // ==============================================================================
    // Check global/local run =======================================================
    // ==============================================================================
    uHTR->lumi_run_bit_read(m_local_flag, m_global_flag);
    debugStatement << "HCAL local_flag: " << m_local_flag << ", "
                   << "HCAL global_flag: " << m_global_flag;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    unsigned int nAvailable=0;
    unsigned int nOverflow=0;
            
    m_applock.take();
    m_lumi_hAvailable.clear();
    m_lumi_hOverflow.clear();
    m_applock.give();
    
    // Do the TOPIC constuction and publishing
    // Reserve memory block for histograms
    unsigned int occ1Data[interface::bril::shared::MAX_NBX];
    unsigned int sumETData[interface::bril::shared::MAX_NBX];
    unsigned int validData[interface::bril::shared::MAX_NBX];
        
    memset(occ1Data , 0 ,sizeof(occ1Data));
    memset(sumETData, 0 ,sizeof(sumETData));
    memset(validData, 0 ,sizeof(validData));
        
    // Topic data
    toolbox::mem::Reference* hist_ref = 0;
    unsigned int * thisData;
    int topicSize=0;
    std::string topicName;
    
    // Read lumi histo status
    uHTR->lumi_histo_status(m_lumi_hAvailable, m_lumi_hOverflow);
        
    // Iterate over all histograms from collecting list
    for (auto &histName : m_histNames) {
    
        if ( !m_lumi_hAvailable[histName] ) {
            debugStatement << "Histo: " << histName << " is not available, skipping";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            continue;
        }
        
        hcal::uhtr::uHTR::LumiHistogram uhtrLumiHist;
        
        // Read histogram
        uHTR->lumi_read_histogram(histName, uhtrLumiHist);
        
        debugStatement << "Histo: " << histName << ", "
                       << "Fill: " << uhtrLumiHist.lhc_fill << ", "
                       << "Run: " << uhtrLumiHist.cms_run << ", "
                       << "LS: " << uhtrLumiHist.lumi_section << ", "
                       << "NB: " << uhtrLumiHist.lumi_nibble << ", "
                       << "Orbit start: " << uhtrLumiHist.orb_init << ", "
                       << "Orbit end: " << uhtrLumiHist.orb_last << ", "
                       << "Orbit diff: " << uhtrLumiHist.orb_last - uhtrLumiHist.orb_init;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        
        nAvailable++;
        
        if ( m_lumi_hOverflow[histName] ) {
            debugStatement << "Histo: " << histName << " in overflow, skipping";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            
            nOverflow++;
            continue;
        }
        
        if ( !sanity_check(m_currentfill, m_currentrun, m_currentls, m_currentnibble) ) {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "TCDS data for the histogram is corrupted. Skippping");
            continue;
        }
        
        // Take a look at lumi received
        std::vector<uint32_t> histo = uhtrLumiHist.h;
        
        // Shouldn't I drop the histogram if it is not complete?
        if( histo.size() != (unsigned int)interface::bril::shared::MAX_NBX || histo.size() == 0 ) {
            debugStatement << "Size of histo deviates from the expected. Incomplete data size: " << histo.size();
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            
            continue;
        }
        
        m_currentfill   = uhtrLumiHist.lhc_fill;
        m_currentrun    = uhtrLumiHist.cms_run;
        m_currentls     = uhtrLumiHist.lumi_section;
        m_currentnibble = uhtrLumiHist.lumi_nibble;
                
        // Depending on global/local flag decide to publish or not.
        // Also advance last published timestamp to prevent forced reconfiguration
        if ( m_local_flag ) {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "HCAL in local, will skip data");
            timeNow = toolbox::TimeVal::gettimeofday();
            lastPublish = timeNow.sec();
            continue;
        }
        
        // If a new run publish whatever you collected to avoid run boundary drops.
        if ( m_currentnibble % 4 == 2 || m_currentrun != m_previousrun) {
            // We are publishing this nibble. Collect data and proceed
            m_collected_histos.emplace(histName, histo);
            ++m_stat_collected[histName];
            
            debugStatement << "Histo: " << histName
                           << " Fill: " << m_currentfill
                           << " Run: " << m_currentrun
                           << " LS: " << m_currentls
                           << " nb: " << m_currentnibble
                           << " first element: " << histo[0];
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");

            ready_to_publish = true;
            
            toolbox::TimeVal topic_time = toolbox::TimeVal::gettimeofday();
                
            std::pair<HISTOIterator, HISTOIterator> all_hists_per_type = m_collected_histos.equal_range(histName);
                
            unsigned int brildaqHist[interface::bril::shared::MAX_NBX];
            int length = std::min( interface::bril::shared::MAX_NBX, (int)histo.size());
            int hfetOffset = m_BXOffset;
            int hfocOffset = m_OCBXOffset;

            unsigned int histo_to_publish[interface::bril::shared::MAX_NBX];
            memset(histo_to_publish, 0, sizeof(histo_to_publish));
            
            // Sum and normalize vectors
            for (HISTOIterator it = all_hists_per_type.first; it != all_hists_per_type.second; it++) {
                // Base vector
                std::vector<uint32_t> local_vect = it->second;
                    
                for ( int ibx=0; ibx<length; ibx++ ) {
                    histo_to_publish[ibx] += local_vect[ibx];
                }
            }
                        
            // Keep place to ingnore some blocks after reconfiguration
            if( histName == "CMS1" ) {
                thisData=occ1Data;
                topicSize=interface::bril::hfCMS1T::maxsize();
                topicName=interface::bril::hfCMS1T::topicname();
            } else if ( histName == "CMS_ET" ) {
                thisData=sumETData;
                topicSize=interface::bril::hfCMS_ETT::maxsize();
                topicName=interface::bril::hfCMS_ETT::topicname();
            } else if ( histName == "CMS_VALID" ) {
                thisData=validData;
                topicSize=interface::bril::hfCMS_VALIDT::maxsize();
                topicName=interface::bril::hfCMS_VALIDT::topicname();
            } else {
                debugStatement << "Unknown topic: " << histName<<"  Skipping...";
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                continue;
            }
                
            hist_ref = m_poolFactory->getFrame( m_memPool
                                              , topicSize );
            hist_ref->setDataSize( topicSize );
                
            interface::bril::shared::DatumHead* datumHead = (interface::bril::shared::DatumHead*)(hist_ref->getDataLocation());
                
            // advance nb to the next nibble 4 boundary
            datumHead->setTime( m_currentfill
                              , m_currentrun
                              , m_currentls
                              , m_currentnibble + 2
                              , topic_time.sec()
                              , topic_time.millisec() );
                
            datumHead->setTotalsize( topicSize );
                
            datumHead->setResource(m_crateId, 0, m_slotId, interface::bril::shared::StorageType::UINT32);
                
            // add offset to the histos
            for ( int ibx=0; ibx<length; ibx++ ) {
                if( histName == "CMS_ET" ) {
                    brildaqHist[(ibx+hfetOffset)%3564]=histo_to_publish[ibx];
                    *(thisData+((ibx+hfetOffset)%3564))=histo_to_publish[ibx];
                } else {
                    brildaqHist[ibx]=histo_to_publish[ibx];
                    *(thisData+ibx)=histo_to_publish[ibx];
                }
            }
                
            if( length < interface::bril::shared::MAX_NBX ) {
                for(int ibx=length; ibx<interface::bril::shared::MAX_NBX; ibx++){
                    if( histName == "CMS_ET"){
                        brildaqHist[(ibx+hfetOffset)%3564]=0;
                        *(thisData+((ibx+hfetOffset)%3564))=0;
                    } else {
                        brildaqHist[ibx]=0;
                        *(thisData+ibx)=0;
                    }
                }
            }
                
            for( int ibx = 0; ibx < length; ibx++ ) {
                if ( histName == "CMS1" ) {
                    brildaqHist[(ibx+hfocOffset)%3564]=histo_to_publish[ibx];
                    *(thisData+((ibx+hfocOffset)%3564))=histo_to_publish[ibx];
                } else {
                    brildaqHist[ibx] = histo_to_publish[ibx];
                    *(thisData+ibx) = histo_to_publish[ibx];
                }
            }
                
            if( length<interface::bril::shared::MAX_NBX ) {
                for ( int ibx=length; ibx<interface::bril::shared::MAX_NBX; ibx++ ) {
                    if( histName == "CMS1" ) {
                        brildaqHist[(ibx+hfocOffset)%3564] = 0;
                        *(thisData+((ibx+hfocOffset)%3564)) = 0;
                    } else {
                        brildaqHist[ibx] = 0;
                        *(thisData+ibx) = 0;
                    }
                }
            }
                
            unsigned int publ_fillnum = datumHead->fillnum;
            unsigned int publ_runnum = datumHead->runnum;
            unsigned int publ_lsnum = datumHead->lsnum;
            unsigned int publ_nbnum = datumHead->nbnum;
                
            debugStatement << "Histo will be published: " << histName
                           << " Fill: " << publ_fillnum
                           << " Run: " << publ_runnum
                           << " LS: " << publ_lsnum
                           << " nb: " << publ_nbnum
                           << " first element: " << brildaqHist[0];
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
                
            memcpy(datumHead->payloadanchor,brildaqHist,datumHead->payloadsize());
            m_topicoutqueues[topicName]->push(hist_ref);

        } else {
            // Add histos to collected and increase statistics
            m_collected_histos.emplace(histName, histo);
            ++m_stat_collected[histName];
        }
        
    }
    // =======================================================================================
    // If all topics are ready to be published (either new nb4 expected
    // or new run is comming than publish whatever amount of histograms collected
    if ( ready_to_publish == true ) {
        // End of processing
        m_previousfill = m_currentfill;
        m_previousrun = m_currentrun;
        m_previousls = m_currentls;
        m_previousnibble = m_currentnibble;
        
        // List of collected histograms
        for (auto it : m_collected_histos) {
            debugStatement << "Collected histograms: " << it.first;
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        }
        
        timeNow = toolbox::TimeVal::gettimeofday();
        lastPublish = timeNow.sec();
        
        m_stat_collected.clear();
        m_collected_histos.clear();
        ready_to_publish = false;
    }
        
    usleep(50000);
        
    return true;
    
}

bool bril::hfsource::Application::publishing(toolbox::task::WorkLoop* wl) {
    
    if ( configuring ) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Will not publish because configuring in progress.");
        usleep(100000);
        return true;
    }
    
    QueueStoreIt it;
    for ( it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it){
        
        if(it->second->empty()) continue;
        
        std::string topicname = it->first;
        
        toolbox::mem::Reference* data = it->second->pop();
        
        debugStatement << "Trying to publish in topic: " << topicname;
        LOG4CPLUS_DEBUG( getApplicationLogger(), debugStatement.str() );
        debugStatement.str("");
        
        do_publish(m_bus.value_,topicname,data);
        
        LOG4CPLUS_DEBUG( getApplicationLogger(), "Succesfully published to eventing" );
    }
    usleep(50000);
    return true;
}

// give the topic and publish its data.
void bril::hfsource::Application::do_publish( const std::string& busname, const std::string& topicname, toolbox::mem::Reference* bufRef) {
    std::stringstream msg;
    try{
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        
        msg<<"Publishing "<<busname<<":"<<topicname;
        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
        
        this->getEventingBus(busname).publish(topicname,bufRef,plist);
        
    }catch(xcept::Exception& e){
        msg << "Failed to publish " << topicname << " to " << busname;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){
            bufRef->release();
            bufRef = 0;
        }
        XCEPT_DECLARE_NESTED(bril::hfsource::exception::Exception,myerrorobj,msg.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}

bool bril::hfsource::Application::readlut(std::vector<uint16_t> lut[nFibers][nChannels])
{
    // Returns false on success.

    // NOTE: allocate memory on heap since buffer is large (avoids segfaults)
    char* buf = new char[Nbytes_allocated];  // 8 Mb

    // read whole file in one shot
    FILE* f = fopen(m_luts_path.value_.c_str(), "r");
    if (!f) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file open failed");
        delete buf;
        return true;
    }

    size_t nbytes = fread(buf, 1, Nbytes_allocated, f);

    if (ferror(f) != 0) {
        fclose(f);
        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file read failed");
        delete buf;
        return true;
    }

    if (feof(f) == 0 || nbytes == Nbytes_allocated) {
        fclose(f);
        LOG4CPLUS_ERROR(getApplicationLogger(), "too large LUT file");
        delete buf;
        return true;
    }

    fclose(f);

    // append end of string
    buf[nbytes] = '\0';

    // NOTE: allocate memory on heap since buffer is large (avoids segfaults)
    char* line = new char[Nbytes_allocated];

    char* saveptr_line;
    char* lineptr = strtok_r(buf, "\n", &saveptr_line);

    // loop over lines
    while (lineptr) {
        // make a copy since strtok_r() modifies the original buffer
        strcpy(line, lineptr);

        char* saveptr_lineParser;
        char* lineParser = strtok_r(line, " \t", &saveptr_lineParser);

        /////  stands for the columns in the LUTs file
        int  LineIndex = 0;
        int iFiber=-1;
        int  iChan = -1;

        // loop over lineParser
        while (lineParser) {
            // crateId, slot, fiberIndex, fiberChanId, ieta, iphi, depth, LUT[0], LUT[1], ...

            if ( LineIndex != 4 && LineIndex != 5 && LineIndex != 6) {
                // convert to integer (decimal, octal and hex numbers are accepted)
                char* ptr; // reference to first invalid character

                long int res = strtol(lineParser, &ptr, 0);

                if ( strlen(lineParser) == 1 )
                {
                    if ( lineParser[0] == 0x0A || lineParser[0] == 0x0D ) break;
                }
                if ( strlen(lineParser) == 2 )
                {
                    if ( lineParser[0] == 0x0D && lineParser[1] == 0x0A ) break;
                }
                // validation

                if (res == LONG_MIN || res == LONG_MAX || *ptr != '\0' || res < 0 || res > /*UINT16_MAX*/65535) {
                    LOG4CPLUS_ERROR(getApplicationLogger(), "not a number or wrong value, reading of LUTs failed");
                    delete line;
                    delete buf;
                    return true;
                }

                if ( LineIndex == 0) {
                    // to to next line if this is wrong crate
                    if (m_crateId.value_ != res)
                        break;
                }
                else if (  LineIndex == 1) {
                    // to to next line if this is wrong slot
                    if (m_slotId.value_ != res)
                        break;
                }
                else if ( LineIndex == 2) {
                    iFiber = res;
                    if (iFiber < 0 || iFiber > int(nFibers-1)) {
                        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file: wrong iFiber");
                        delete line;
                        delete buf;
                        return true;
                    }
                }
                else if ( LineIndex == 3) {
                    iChan = res;
                    if (iChan < 0 || iChan > int(nChannels-1)) {
                        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file: wrong iChan ============");
                        delete line;
                        delete buf;
                        return true;
                    }
                }
                else
                lut[iFiber][iChan].push_back((uint16_t)res);
            }

            LineIndex++;
            lineParser = strtok_r(NULL, " \t", &saveptr_lineParser);
        }

        lineptr = strtok_r(NULL, "\n", &saveptr_line);
    }

    delete line;
    delete buf;

    LOG4CPLUS_DEBUG(getApplicationLogger(), "made it so far... will push the 1-to-1 LUT");
    // write 1-to-1 LUTs for those fibers/channels which are missing in the file
    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
        for (unsigned int iChan = 0; iChan < nChannels; iChan++)
            if (lut[iFiber][iChan].size() == 0) {
                LOG4CPLUS_DEBUG(getApplicationLogger(), "No LUT data for crate # " << m_crateId.value_ << ", slot # " << m_slotId.value_ << ", fiber # " << iFiber << ", chanel # " << iChan << " . Using 1-to-1 LUT");

                for (uint16_t i = 0; i < nElements; i++) lut[iFiber][iChan].push_back(i);
            }

bool ret_ = false;
    // verify that sizes of LUTs are correct
    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
        for (unsigned int iChan = 0; iChan < nChannels; iChan++)
            if (lut[iFiber][iChan].size() != nElements) {
                LOG4CPLUS_ERROR(getApplicationLogger(), "invalid LUT size for iFiber=" << iFiber << ", iChan=" << iChan);
                // this used to be an exit, we may change it back in the future
                ret_=true;
            }


    if(compressLUT){
        for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
            for (unsigned int iChan = 0; iChan < nChannels; iChan++)
                for (uint16_t i = 0; i < nElements; i++) {
                    lut[iFiber][iChan][i]=int(float(lut[iFiber][iChan][i])/2.+0.5);
                    if (lut[iFiber][iChan][i]>1023) lut[iFiber][iChan][i] = 1023;
                }
    }

    return ret_;
}

int bril::hfsource::Application::lumi_programlut(){
    // Sets up lookup tables.
    // Returns number of reprogrammed LUTs.
    std::vector<uint16_t> lut[nFibers][nChannels];

    // LUTs from file vs 1-to-1 LUTs (1 ADC count = 1 energy unit)
    if (m_luts_path.value_.size() > 0) {
        LOG4CPLUS_DEBUG(getApplicationLogger(),"trying to read in LUTs....");
        if (readlut(lut)) {
            LOG4CPLUS_ERROR(getApplicationLogger()," failed to get LUTs from file...");
            return -1; // failed to get LUTs from file
        }
    } else {
        for (unsigned int iFiber = 0; iFiber < nFibers; ++iFiber)
            for (unsigned int iChan = 0; iChan < nChannels; ++iChan)
                for (uint16_t i = 0; i < nElements; ++i)
                    lut[iFiber][iChan].push_back(i);
    }

    LOG4CPLUS_DEBUG(getApplicationLogger(),"read  LUTs alright....");
    
    // number of reprogrammed LUTs

    int num_prog = 0;

    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++) { // fibers
        for (unsigned int iChan = 0; iChan < nChannels; iChan++) { // channels
            // get currently used LUT
            bool lut_invalid = false;
            std::vector<uint16_t> curr_lut;
            try{
                uHTR->lumi_read_lut(iFiber, iChan, curr_lut);
            } catch(uhal::exception::exception& e) {
                std::stringstream msg;
                msg<<"Failed to lumi_read_lut "<<m_crateId<<","<<m_slotId<<" uHTR\n";
                msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
                continue;
            }

            // verify current LUT
            if (curr_lut.size() == lut[iFiber][iChan].size()) {
                for (size_t ilut = 0; ilut < lut[iFiber][iChan].size(); ilut++) {
                    if (curr_lut[ilut] != lut[iFiber][iChan][ilut]) {
                        lut_invalid = true;
                        break;
                    }
                }
            } else {
                lut_invalid = true;
            }

            LOG4CPLUS_DEBUG(getApplicationLogger(),"next we perform LUT reconfiguration....");

            // perform LUT reconfiguration then necessary
            if (lut_invalid) {
                num_prog++;
                try{
                    uHTR->lumi_program_lut(iFiber, iChan, lut[iFiber][iChan]);
                } catch(uhal::exception::exception& e) {
                    std::stringstream msg;
                    msg<<"Failed to lumi_program_lut "<<m_crateId<<","<<m_slotId<<" uHTR\n";
                    msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                    LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
                    continue;
                }


                // re-verify
                curr_lut.clear();
                try{
                    LOG4CPLUS_DEBUG(getApplicationLogger(),"read luts to verify....");
                    uHTR->lumi_read_lut(iFiber, iChan, curr_lut);
                    LOG4CPLUS_DEBUG(getApplicationLogger(),"exiting this lumi_read_lut....");

                } catch(uhal::exception::exception& e) {
                    std::stringstream msg;
                    msg<<"Failed to lumi_program_lut "<<m_crateId<<","<<m_slotId<<" uHTR\n";
                    msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                    LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
                    continue;
                }
                if (curr_lut.size() != lut[iFiber][iChan].size()) {
                    LOG4CPLUS_ERROR(getApplicationLogger()," problem with the size of curr_lut...");
                    return -1;
                }

                std::stringstream msg;
                
                msg << "PRINTING LUT Crate # " << m_crateId <<" board # " << m_slotId << " fiber # " << iFiber << " channel # " << iChan << " : " << std::setw(4);

                for (size_t ilut = 0; ilut < lut[iFiber][iChan].size(); ilut++) {
                    if (curr_lut[ilut] != lut[iFiber][iChan][ilut]) {
                        LOG4CPLUS_ERROR(getApplicationLogger(),"Item in LUT fails verification:  "<<ilut<<" in uHTR "<<curr_lut[ilut]<<"  in hfsource "<<lut[iFiber][iChan][ilut]);
                        return -1;
                    }
                }

                LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
            }
        }
    }
    //m_applock.take();

    std::string mmm; mmm = "LUTs are loaded from the file: " + m_luts_path.value_ + " verified.";

    LOG4CPLUS_DEBUG(getApplicationLogger(),mmm);

    //m_lut.value_ = ss.str();
    //m_applock.give();
    return num_prog;
}
