#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/string.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "b2in/nub/Method.h"
//#include "bril/supervisor/WebUtils.h"
#include "bril/supervisor/CalibDataService.h"
#include "interface/bril/TCDSTopics.hh"
#include "bril/supervisor/exception/Exception.h"
#include "bril/supervisor/INIReader.h"
#include "bril/supervisor/Decoder.h"
#include "bril/supervisor/SinceData.h"
#include <set>
#include <cstring>
#include <jansson.h>
#include <string>
XDAQ_INSTANTIATOR_IMPL(bril::supervisor::CalibDataService)

namespace bril{ 
  namespace supervisor {
    
    CalibDataService::CalibDataService(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this){
      xgi::framework::deferredbind(this,this,&bril::supervisor::CalibDataService::Default, "Default");
      b2in::nub::bind(this, &bril::supervisor::CalibDataService::onMessage);
      m_bus.fromString("brildata");//default
      m_lastrun = -1;
      m_currentrun = -1;   
      m_isfirstrun = true;   
      m_resourceclean = true;
      m_connectionPoolSize = 5;
      m_connectionLifetime = 60*5;
      m_connectionPool = 0;
      m_dbalias.fromString("online");
      m_dbschema.fromString("cms_lumi_prod");
      m_authpath.fromString("db.ini");
      try{
	getApplicationInfoSpace()->fireItemAvailable("dbalias",&m_dbalias);
	getApplicationInfoSpace()->fireItemAvailable("dbschema",&m_dbschema);
	getApplicationInfoSpace()->fireItemAvailable("connectionPoolSize",&m_connectionPoolSize);
	getApplicationInfoSpace()->fireItemAvailable("connectionLifetime",&m_connectionLifetime);
	getApplicationInfoSpace()->fireItemAvailable("authpath",&m_authpath);
	getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
	getApplicationInfoSpace()->fireItemAvailable("normtags",&m_normtagsStr);
	getApplicationInfoSpace()->fireItemAvailable("runnum",&m_currentrun);
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationInfoSpace()->addItemChangedListener("runnum",this);
	toolbox::net::URN memurn("toolbox-mem-pool","brilCalibDataService_mem"); 
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
      }catch(xcept::Exception & e){
	XCEPT_RETHROW(xdaq::exception::Exception, "Failed to set up infospace or memory pool", e);
      }     
    }
    
    CalibDataService::~CalibDataService(){  
      cleanresource();
    }
    
    void CalibDataService::Default(xgi::Input * in, xgi::Output * out){  
      std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+getApplicationDescriptor()->getURN();
      *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
      *out << "<p> Eventing Status</p>";
      *out <<busesToHTML();        
    }
    
    void CalibDataService::actionPerformed(xdata::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
      if( e.type() == "urn:xdaq-event:setDefaultValues" ){       
	try{
	  if(!m_normtagsStr.value_.empty()){
	    m_normtags = toolbox::parseTokenSet(m_normtagsStr.value_,",");
	  }
	  std::string dbservice;
	  std::string dbuser;
	  std::string dbpass;
	  if( !parseDBparam(m_authpath.value_,m_dbalias.value_,dbservice,dbuser,dbpass) ){
	    XCEPT_RAISE( bril::supervisor::exception::CalibDataService, "authpath parsing error");
	  }
	  char connectstr[80];
	  sprintf(connectstr, "service=%s user=%s password=%s",dbservice.c_str(),dbuser.c_str(),dbpass.c_str());
	  m_connectstr=std::string(connectstr);
	  m_connectionPool = new soci::connection_pool(m_connectionPoolSize.value_);
	  initcache();	   
	  this->getEventingBus(m_bus.value_).addActionListener(this);	  
	  this->getEventingBus(m_bus.value_).subscribe("NB4"); 
	}catch( eventing::api::exception::Exception& e ){
	  std::string msg("Failed to listen to eventing bus "+m_bus.value_);
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	  XCEPT_RETHROW(exception::Exception,msg,e);     
	}	
      }else if ( e.type() == "ItemChangedEvent"){      
	std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
	if( item=="runnum" ){
	  for(validitymap_t::iterator it = m_validity.begin(); it!=m_validity.end(); ++it ){
	    if( !is_currentrunvalid(it->first,m_currentrun.value_) ){	
	      try{
		getvaliddata(it->first,m_currentrun.value_);
	      }catch( bril::supervisor::exception::DBQueryError& e){
		LOG4CPLUS_ERROR(getApplicationLogger(),e.what());
		XCEPT_DECLARE(exception::Exception,myerrorobj,e.what());
		this->notifyQualified("error",myerrorobj);
	      }
	    }
	  }
	}
      }
    }
    
    void CalibDataService::actionPerformed(toolbox::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
      if ( e.type() == "eventing::api::BusReadyToPublish" ){

      }
    }

    void CalibDataService::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if (action == "notify"){      
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
	std::string data_version  = plist.getProperty("DATA_VERSION");    
	std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
	if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
	  std::string msg("Received brildaq message has no or wrong version, do not process.");
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg);
	  if(ref!=0){
	    ref->release(); 
	    ref=0;
	  }
	  return;
	}
	if(topic == "NB4"){
	  if(ref!=0){
	    interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
	    m_currentrun.value_ = thead->runnum;
	    if( m_isfirstrun ){
	      LOG4CPLUS_INFO(getApplicationLogger(), "is first run");
	      m_lastrun = m_currentrun;
	      m_isfirstrun = false;	      
	      getApplicationInfoSpace()->fireItemValueChanged("runnum",this);
	    }else if( m_currentrun.value_ != m_lastrun.value_ ){
	      LOG4CPLUS_INFO(getApplicationLogger(), "runnum changed");
	      m_lastrun = m_currentrun;
	      getApplicationInfoSpace()->fireItemValueChanged("runnum",this);
	    }
	    do_publish();	    
	  }
	}
      }
      if(ref!=0){
	ref->release();
	ref=0;
      }
    }
    
    void CalibDataService::do_publish(){
      unsigned long long topicid;
      std::string topicname;
      std::string funcname;      
      size_t objsize = 0;
      toolbox::mem::Reference* bufRef = 0;
      for(calibratormap_t::iterator it=m_calibfunctor.begin(); it!=m_calibfunctor.end(); ++it){
	xdata::Properties plist;
	topicid = it->first;
	topicname = m_tagidtoname[topicid];
	funcname = (it->second).first;
	plist.setProperty("funcname",funcname);
	objsize = sizeof(interface::bril::CalibData);
	try{
	  bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,4096);	  
	  memcpy( bufRef->getDataLocation(), (void*)(it->second).second, objsize );
	  bufRef->setDataSize( objsize );	  
	  getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
	}catch(xcept::Exception& e){
	  if(bufRef){
	    bufRef->release();
	    bufRef = 0;
	  }
	}	
      }
    }

    bool CalibDataService::is_currentrunvalid(unsigned long long tagnameid, int runnum){
      std::stringstream ss;
      std::pair<int,int> thistag_validity = m_validity[tagnameid];
      if( runnum>=thistag_validity.first && runnum<thistag_validity.second ){
	return true;
      }else{
	ss <<runnum<<" is NOT in range for tag "<<tagnameid;
	LOG4CPLUS_INFO( getApplicationLogger(), ss.str() );
      }
      return false;
    }
       
    void CalibDataService::initcache(){
      std::stringstream ss;
      size_t sessionid;
      if( !m_connectionPool->try_lease(sessionid,m_leasetimeout) ){
	XCEPT_RAISE( bril::supervisor::exception::DBConnectionError, "failed to lease connection from connection pool");
      }
      soci::session* p_session = &( m_connectionPool->at(sessionid) );
      p_session->open("oracle",m_connectstr);     
      m_openconnections.insert( std::make_pair(sessionid,p_session) );
      std::string tablename = m_dbschema.value_+"."+"iovtags";
      std::string tagname ;
      unsigned long long tagnameid;      
      soci::statement st = ( p_session->prepare << "select tagid from "<<tablename<<" where tagname=:tagname",soci::into(tagnameid),soci::use(tagname,"tagname") );	
      for(std::set<std::string>::iterator it=m_normtags.begin(); it!=m_normtags.end(); ++it){
	tagname = *it;
	tagnameid = 0;
	st.execute();
	int i = 0;	
	while( st.fetch() ){
	  ss<<"tagname "<<tagname<<" tagnameid "<<tagnameid;
	  LOG4CPLUS_INFO( getApplicationLogger(), ss.str() );
	  ss.str(""); ss.clear();
	  m_tagidtoname.insert( std::make_pair(tagnameid,tagname) );//fill tagidtoname map
	  m_validity.insert( std::make_pair(tagnameid, std::make_pair(-1,-1)) ); //initialize validity map;
	  m_calibfunctor.insert( std::make_pair(tagnameid, std::make_pair("",(interface::bril::CalibData*)0)) );//initialize calibobj map
	  ++i;
	}
	if( !i ) XCEPT_RAISE( bril::supervisor::exception::TagNotFoundError, "iov tag "+tagname+" not found in db" );
      }	
            
      p_session->close();
      m_connectionPool->give_back(sessionid);
      m_openconnections.erase(sessionid);
    }

    void CalibDataService::parsePayloadString(const std::string& func_payload, std::map<std::string,std::string>& result){
      const char* strpayload=func_payload.c_str();
      json_error_t jerror;
      json_t* jpayload = json_loads(strpayload, 0, &jerror);
      if(!jpayload){
	std::cout<<"error: on line "<<jerror.line<<" message "<<jerror.text<<std::endl;
      }      
      const char *key;
      json_t *value;   
      void* iter = json_object_iter(jpayload);
      while(iter){
	key = json_object_iter_key(iter);
	value = json_object_iter_value(iter);
	std::string valuestr( json_string_value(value) );
	result.insert( std::make_pair(key,valuestr) );
	iter = json_object_iter_next(jpayload, iter);
      }
      json_decref( jpayload );
    }

    void CalibDataService::parseCoefsStr(const std::string& coefsStr, float* coefs, size_t& ncoefs){
      std::list<std::string> tokens = toolbox::parseTokenList(coefsStr,",");
      ncoefs = tokens.size();
      size_t i = 0;
      for(std::list<std::string>::iterator it=tokens.begin(); it!=tokens.end(); ++it){
	coefs[i] = ::atof( (*it).c_str() );
	++i;
      }
    }

    void CalibDataService::parseAfterglowStr(const std::string& afterglowStr, interface::bril::AfterglowThreshold* afterglow, size_t& nafterglow){
      std::string a ;
      if(afterglowStr.size()>2){
	a = afterglowStr.substr(1, afterglowStr.size()-2 );//remove [] head/tail
      }
      if( a[a.size()-1] == ','){
	a = a.substr(0, a.size()-1); //remove last , if any
      }
      
      std::list<std::string> tokens = toolbox::parseTokenList(a,"),(");
      nafterglow = tokens.size();
      size_t i= 0;
      for(std::list<std::string>::iterator it=tokens.begin(); it!=tokens.end(); ++it, ++i){
	std::string item = *it;
	if( item[item.size()-1] == ')' ){
	  item = item.substr(0,item.size()-1); //remove last )
	}
	if( item[0] == '(' ){
	  item = item.substr(1,item.size()-1); //remove first )
	}
	size_t pos = item.find(',');
	if( pos != std::string::npos ){
	  std::string nbxStr = item.substr(0, pos);
	  std::string tvalStr = item.substr(pos+1, item.size()-pos-1);
	  afterglow[i].first = ::atoi( nbxStr.c_str() );
	  afterglow[i].second = ::atof( tvalStr.c_str() );
	}
      }
    }

    void CalibDataService::getvaliddata(unsigned long long iovtagnameid, int runnum){
      std::cout<<"getvaliddata for "<<iovtagnameid<<" runnum "<<runnum<<std::endl;
      size_t sessionid;
      if( !m_connectionPool->try_lease(sessionid,m_leasetimeout) ){
	std::cout<<"faied to lease session from connection pool"<<std::endl;
      }
      soci::session* p_session = &( m_connectionPool->at(sessionid) );
      p_session->open("oracle",m_connectstr);     
      m_openconnections.insert( std::make_pair(sessionid,p_session) );

      std::string tablename = m_dbschema.value_+"."+"iovtagdata";
      SinceData r;
      Sincemap allsincedata;
      try{
	soci::statement st = (p_session->prepare << "select since,payload,func from "<<tablename<<" where tagid=:iovtagnameid" ,soci::into(r), soci::use(iovtagnameid,"iovtagnameid") );
	st.execute();
	while( st.fetch() ){
	  allsincedata.insert( std::make_pair(r.since,r) );
	}
      }catch( const soci::soci_error& er){
	XCEPT_RAISE( bril::supervisor::exception::DBQueryError, er.what() );
      }catch( const std::exception& er ){
        XCEPT_RAISE( bril::supervisor::exception::DBQueryError, er.what() );
      }

      Sincemap::iterator it;
      unsigned int since = 1;
      unsigned int till = 999999;
      it = allsincedata.upper_bound(runnum);
      if( it!=allsincedata.end() ){
	till = (it->first)-1;
	if( it==allsincedata.begin() ){
	  since = 0;//exception!!!!
	}else{
	  since = (--it)->first;
	}
      }else{
	std::cout<<" NOT found upper_bound for run "<<runnum<<std::endl;
	since = allsincedata.rbegin()->first;
      }
      std::cout<<"since "<<since<<" till "<<till<<std::endl;
      if( since!=0 ){
	it = allsincedata.find(since);
	std::string fname = it->second.func;//json string must be double quoted
	std::replace( fname.begin(), fname.end(), '\'', '\"' );
	std::string fpayload = it->second.payload;
	std::cout<<fname<<" "<<fpayload<<std::endl;
	std::replace( fpayload.begin(),fpayload.end(), '\'', '\"' );
	m_validity[iovtagnameid] = std::make_pair(since, till);
	if( m_calibfunctor[iovtagnameid].second != (interface::bril::CalibData*)0 ){
	  delete m_calibfunctor[iovtagnameid].second;
	  m_calibfunctor[iovtagnameid].second = (interface::bril::CalibData*)0;
	}
	interface::bril::CalibData* v = new interface::bril::CalibData;
	v->setValidity(since,till);
	std::map<std::string,std::string> payloadmap;
	parsePayloadString(fpayload,payloadmap);
	std::map<std::string,std::string>::iterator cit = payloadmap.find("coefs");
	if( cit!=payloadmap.end() ){
	  float coefs[interface::bril::MAX_CALIBCOEFS];
	  size_t ncoefs = 0;
	  parseCoefsStr(cit->second,coefs,ncoefs);
	  v->setCoefs(coefs,ncoefs);
	}
	std::map<std::string,std::string>::iterator ait = payloadmap.find("afterglowthresholds");
	if( ait!=payloadmap.end() ){
	  interface::bril::AfterglowThreshold af[interface::bril::MAX_AFTERGLOWTH];
	  size_t nafterglow = 0;
	  parseAfterglowStr(ait->second,af,nafterglow);
	  v->setAfterglowThresholds(af,nafterglow);
	}
	m_calibfunctor[iovtagnameid] = std::make_pair(fname,v);
	m_resourceclean = false;
      }
      
      p_session->close();
      m_connectionPool->give_back(sessionid);
      m_openconnections.erase(sessionid);
    }
    
    void CalibDataService::cleanresource(){
      if(!m_resourceclean){
	for(calibratormap_t::iterator it=m_calibfunctor.begin(); it!=m_calibfunctor.end(); ++it){
	  if( (it->second).second != (interface::bril::CalibData*)0 ){
	    delete (it->second).second;
	  }
	}
	delete m_connectionPool;
      }      
      m_resourceclean = true;
    }
       
    bool CalibDataService::parseDBparam(const std::string& authf, const std::string& dbalias, std::string& o_netservice, std::string& o_dbuser, std::string& o_dbpass){
      INIReader reader(authf);
      if (reader.ParseError() < 0) {
	std::cout << "Can't load ini file\n";
      }
      o_netservice = reader.Get(dbalias,"service","");
      if(o_netservice.empty()){
	std::cout<<"empty netservice"<<std::endl;
	return false;
      }
      o_dbuser = reader.Get(dbalias,"user","");
      if(o_dbuser.empty()){
	std::cout<<"empty user"<<std::endl;
    return false;
      }
      std::string pass = reader.Get(dbalias,"pwd","");
      if(pass.empty()){
	std::cout<<"empty pass"<<std::endl;
	return false;
      }
      o_dbpass = base64_decode(pass); 
      return true;
    }

    void CalibDataService::closeOpenConnections(){
      std::map<size_t,soci::session*>::iterator it;
      for( it=m_openconnections.begin();it!=m_openconnections.end();++it ){
	it->second->close();
	m_connectionPool->give_back(it->first);
	m_openconnections.erase(it);
      }
    }
    
  }//ns supervisor
}//ns bril
