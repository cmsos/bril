#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Guard.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Properties.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/TimeVal.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "toolbox/mem/AutoReference.h"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/shared/LUMITopics.hh"
#include "bril/supervisor/DTLumiProcessor.h"
#include "bril/supervisor/exception/Exception.h"
#include "bril/supervisor/Events.h"

XDAQ_INSTANTIATOR_IMPL (bril::supervisor::DTLumiProcessor)

bril::supervisor::DTLumiProcessor::DTLumiProcessor(xdaq::ApplicationStub* s) : xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this), m_applock(toolbox::BSem::FULL){

    xgi::framework::deferredbind(this,this,&bril::supervisor::DTLumiProcessor::Default, "Default");
    b2in::nub::bind(this, &bril::supervisor::DTLumiProcessor::onMessage);    
    m_outbus.fromString("brildata");
    m_outtopic.fromString("dtlumi");
    m_background = 0;
    m_scalefactor = 1;    

    memset(m_bxbeamintensity1,0,sizeof(float)*3564);
    memset(m_bxbeamintensity2,0,sizeof(float)*3564);

    m_fillnum = 0;
    m_runnum = 0;
    m_lsnum = 0;
    m_nbnum = 0;
    m_timestampsec = 0;
    m_timestampmsec = 0;
    m_canpublish = false;
    m_RunNumber = 0;
    m_algo_lumiSection = 0;
    m_bmtfSortedRate = 0;

    toolbox::net::UUID u=getApplicationDescriptor()->getUUID();
    m_uniqueid = u.toString();
    m_publishing_wl = 0;
    m_as_publishing_wl = 0;

    getApplicationInfoSpace()->fireItemAvailable("eventinginput",&m_eventinginput);
    getApplicationInfoSpace()->fireItemAvailable("outbus",&m_outbus);
    getApplicationInfoSpace()->fireItemAvailable("outtopic",&m_outtopic);
    getApplicationInfoSpace()->fireItemAvailable("calibtag",&m_calibtag);
    getApplicationInfoSpace()->fireItemAvailable("sigmaVis",&m_sigmaVis);
    getApplicationInfoSpace()->fireItemAvailable("background",&m_background);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    m_publishing_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_uniqueid +"_publishing","waiting");  
    m_as_publishing_wl = toolbox::task::bind(this,&bril::supervisor::DTLumiProcessor::publishing,"publishing"); 

    toolbox::net::URN memurn("toolbox-mem-pool","DTLumiProcessor_mem");
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    try{
      toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
      m_memPool = m_poolFactory->createPool(memurn,allocator);
  }catch(xcept::Exception& e){
      std::stringstream msg;
      msg<<"Failed to setup memory pool "<<stdformat_exception_history(e);
      LOG4CPLUS_FATAL(getApplicationLogger(),msg.str());
    }
   
}

bril::supervisor::DTLumiProcessor::~DTLumiProcessor(){
}

void bril::supervisor::DTLumiProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
  toolbox::mem::AutoReference refguard(ref);
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
    
    LOG4CPLUS_DEBUG(getApplicationLogger(),"received topic "+topic);
    
    if(topic == "urn:xdaq-flashlist:ugmt_cell"){
      //parse table message
      xdata::Table table;
      xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
      xdata::exdr::Serializer serializer;
      
      try{
	serializer.import(&table, &inBuffer);	
	xdata::Serializable* p = 0;
	p = table.getValueAt(0, "RunNumber" );
	if( p ) {
	  LOG4CPLUS_INFO( getApplicationLogger(),"received RunNumber "+dynamic_cast<xdata::UnsignedLong*>(p)->toString() );
	  m_RunNumber = (unsigned int)(dynamic_cast<xdata::UnsignedLong*>(p)->value_);	  
	  p = 0;
	}
	if( !m_RunNumber ){
	  return;
	}
	
	p = table.getValueAt(0, "processor_UGMTProcessor");
	xdata::Table* p_mytable = 0;
	if( p ){
	  p_mytable = dynamic_cast<xdata::Table*>(p);
	  if (!p_mytable){
	    LOG4CPLUS_ERROR(getApplicationLogger(), "p_mytable is zero");
	  }else{
	    xdata::Serializable* p_bmtfSortedRate = p_mytable->getValueAt(0, "algo_ratesSorter_bmSorterRate");
	    if (!p_bmtfSortedRate){
	      LOG4CPLUS_ERROR(getApplicationLogger(), "p_bmtfSortedRate is zero");
	    }else{
	      LOG4CPLUS_INFO(getApplicationLogger(),"received algo_ratesSorter_bmSorterRate "+dynamic_cast<xdata::Float*>(p_bmtfSortedRate)->toString() );
	      m_bmtfSortedRate = dynamic_cast<xdata::Float*>(p_bmtfSortedRate)->value_;	      
	    }
	    //std::vector<std::string> cols=p_mytable->getColumns();
	    //for(std::vector<std::string>::const_iterator it=cols.begin();it!=cols.end();++it){
	    //  std::cout<<*it<<std::endl;	      
	    //}
	    xdata::Serializable* p_algo_lumiSection = p_mytable->getValueAt(0, "algo_lumiSection");
	    if (!p_algo_lumiSection){
	      LOG4CPLUS_ERROR(getApplicationLogger(), "p_algo_lumiSection is zero");
	    }else{
	      LOG4CPLUS_INFO(getApplicationLogger(),"received algo_lumiSection "+dynamic_cast<xdata::UnsignedInteger32*>(p_algo_lumiSection)->toString()+"+1" );
	      m_algo_lumiSection = dynamic_cast<xdata::UnsignedInteger32*>(p_algo_lumiSection)->value_+1;
	    }
	  }
	}
      }catch (xdata::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"Failed to deserialize incoming table "+stdformat_exception_history(e));
	if(ref!=0){
	  ref->release();
	}
      }
      
      uint64_t timeindex = (uint64_t) m_RunNumber<<32 | m_algo_lumiSection;
      {
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
	m_runlstorate.insert( std::make_pair(timeindex,m_bmtfSortedRate) );
      }
    }else if( topic == "beam"){
      interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 
      unsigned int previous_runnum = m_runnum;
      unsigned int previous_lsnum = m_lsnum;
      m_fillnum = thead->fillnum;
      m_runnum = thead->runnum;
      m_lsnum = thead->lsnum;
      m_nbnum = thead->nbnum;
      m_timestampsec = thead->timestampsec;
      m_timestampmsec = thead->timestampmsec;
      
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      tc.extract_field( m_bxbeamintensity1, "bxintensity1", thead->payloadanchor );
      tc.extract_field( m_bxbeamintensity2, "bxintensity2", thead->payloadanchor );
      tc.extract_field( m_beamstatus, "status", thead->payloadanchor );

      if(previous_runnum != m_runnum || previous_lsnum != m_lsnum ){
	uint64_t timeindex = (uint64_t) m_runnum<<32 | m_lsnum;
	if( m_runlstotimestamp.size()>=3 ){
	  m_runlstotimestamp.erase( m_runlstotimestamp.begin() );
	}
	m_runlstotimestamp.insert( std::make_pair(timeindex,m_timestampsec) );
	bril::supervisor::LumiSectionChangedEvent myevent;
	this->fireEvent( myevent ) ;
      }
    }
  }
  //if(ref!=0){
  //  ref->release();
  //  ref=0;
  //}
}

void bril::supervisor::DTLumiProcessor::Default(xgi::Input * in, xgi::Output * out){
}

void bril::supervisor::DTLumiProcessor::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " +e.type() );
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    this->getEventingBus(m_outbus.value_).addActionListener(this);
    addActionListener(this);
    size_t ninputs = m_eventinginput.elements();

    for(size_t i=0; i<ninputs; ++i){
      xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_eventinginput.elementAt(i));
      xdata::String databus;
      xdata::String topicsStr;
      if(p){
	databus = p->getProperty("bus");   
	topicsStr = p->getProperty("topics");
	//std::cout<<"databus "<<databus.toString()<<" topicsStr "<<topicsStr.toString()<<std::endl;
	std::set<std::string> itopics = toolbox::parseTokenSet(topicsStr.value_,","); 
	m_in_busTotopics.insert(std::make_pair(databus.value_, itopics));
      }
    }

    for( std::map< std::string , std::set<std::string> >::iterator it = m_in_busTotopics.begin(); it!=m_in_busTotopics.end(); ++it){
      std::string ibus = it->first;

      std::set<std::string> itopics = it->second;
      for( std::set<std::string>::iterator setit = itopics.begin(); setit!=itopics.end(); ++setit ){
	std::string itopic = *setit;
	try{
	  this->getEventingBus(ibus).subscribe( itopic );
	  LOG4CPLUS_INFO(getApplicationLogger(),"subscribed to "+itopic+" on "+ibus);	    
	}catch(eventing::api::exception::Exception& e){
	  std::string msg("Failed to subscribe to eventing bus "+ibus);
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	  XCEPT_RETHROW( bril::supervisor::exception::Exception,msg,e);
	}
      }//end loop over itopics
    }//end loop over m_in_busTotopics
  }
}

void bril::supervisor::DTLumiProcessor::actionPerformed( toolbox::Event& e ){
  LOG4CPLUS_DEBUG( this->getApplicationLogger(), "Received toolbox event " + e.type() );   
  if( e.type() == "urn:bril-supervisor-event:LumiSectionChanged" ){
    if( !m_RunNumber){
      LOG4CPLUS_INFO(getApplicationLogger(), "CMS not running, do nothing");
      return;
    }
    //if( m_runlstotimestamp.size()<3 ){
    //  LOG4CPLUS_INFO(getApplicationLogger(), "Not enough historical data, do nothing");
    //  return;
    //}
    m_publishing_wl->submit( m_as_publishing_wl );
  }else if( e.type() == "eventing::api::BusReadyToPublish" ){
    m_canpublish = true;
    m_publishing_wl->activate();    
  }
}

bool bril::supervisor::DTLumiProcessor::publishing( toolbox::task::WorkLoop* wl ){      
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Entering publishing");

  if(!m_canpublish) return false;
  
  std::map< uint64_t, float, std::less<uint64_t> > runlstorate;
  {  
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    runlstorate = m_runlstorate;
    m_runlstorate.clear();
  }

  for(std::map< uint64_t, float>::iterator it=runlstorate.begin(); it!=runlstorate.end(); ++it){
    float bmtfSortedRate = it->second;
    uint64_t timeindex =  it->first;
    unsigned int algo_lumiSection = timeindex & 0x00000000ffffffff;
    unsigned int RunNumber = timeindex >> 32 ;
    //if rate too high (roughly equivalent to 100 hz/nB) don't believe it
    if( bmtfSortedRate>1.6e6 ) continue;
    unsigned int myts = 0;
    std::map< uint64_t, uint32_t >::iterator rIt=m_runlstotimestamp.find(timeindex);
    if( rIt!=m_runlstotimestamp.end() ){
      myts = rIt->second;
    }else{
      std::stringstream ss;
      ss<<"Cannot find timestampsec for RunNumber "<<RunNumber<<" m_algo_lumiSection "<<algo_lumiSection<<". Do nothing.";
      LOG4CPLUS_WARN( getApplicationLogger(),ss.str() );
      continue;
    }

    float avgraw = bmtfSortedRate;
    float corraw = std::max(bmtfSortedRate - m_background.value_,(float)0.1);
    float avg = corraw * 11245.6 / m_sigmaVis.value_;
  
    //publish dtlumi topic 
    std::string dictstr = interface::bril::shared::dtlumiT::payloaddict();//lumi topic dict is the same for all types
    size_t payloadsize = interface::bril::shared::dtlumiT::maxsize();
    
    xdata::Properties plist;    
    plist.setProperty( "DATA_VERSION", interface::bril::shared::DATA_VERSION );
    plist.setProperty( "PAYLOAD_DICT", dictstr ); 
 
    toolbox::mem::Reference* myMemoryREF = m_poolFactory->getFrame(m_memPool,payloadsize);
    myMemoryREF->setDataSize(payloadsize);

    interface::bril::shared::dtlumiT* lumi_payload = (interface::bril::shared::dtlumiT*)(myMemoryREF->getDataLocation() );
    //lumi_payload->setTime( m_fillnum, m_runnum, m_lsnum, m_nbnum, m_timestampsec, m_timestampmsec );
    lumi_payload->setTime( m_fillnum, RunNumber, algo_lumiSection, 4, myts , 0 );
    lumi_payload->setResource( interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND );
    lumi_payload->setTotalsize( payloadsize );
  
    interface::bril::shared::CompoundDataStreamer cds( dictstr ); 
  
    cds.insert_field(lumi_payload->payloadanchor,"calibtag", m_calibtag.c_str() );
    cds.insert_field(lumi_payload->payloadanchor,"avgraw", &avgraw );
    cds.insert_field(lumi_payload->payloadanchor,"avg", &avg );
    
    //we don't care because we don't publish when cms is not running. If we care, we had to cache also beamstatus
    //if( std::string(m_beamstatus)!="STABLE BEAMS" && std::string(m_beamstatus)!="SQUEEZE" && std::string(m_beamstatus)!="ADJUST" && std::string(m_beamstatus)!="FLAT TOP") {
    //plist.setProperty("NOSTORE","1");
    //LOG4CPLUS_DEBUG(getApplicationLogger(),"NOSTORE dtlumi");
    //}
  
    std::stringstream ss;
    ss<<"Publishing "<<m_fillnum<<" "<<m_runnum<<" "<<m_lsnum<<" "<<m_timestampsec<<" "<<m_outtopic.value_<<" to "<<m_outbus.value_<<" RunNumber "<<RunNumber<<" algo_lumiSection "<<algo_lumiSection<<" acq_timestampsec "<<myts<<" bmtfSortedRate "<<bmtfSortedRate<<" avgraw "<<avgraw<<" avg "<<avg;
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    try{
      getEventingBus(m_outbus.value_).publish( m_outtopic.value_,  myMemoryREF ,plist);
    }catch(xcept::Exception& e){
      myMemoryREF->release();
      LOG4CPLUS_ERROR( getApplicationLogger(),"Failed to publish "+ m_outtopic.value_ );
      LOG4CPLUS_ERROR( getApplicationLogger(),stdformat_exception_history(e) );    
    }
    usleep(1000000);//sleep for flashlist resolution
  }//end loop over cached rates

  return false;
}



