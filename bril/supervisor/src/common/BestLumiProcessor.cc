#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/TimeVal.h"
#include "b2in/nub/Method.h"
#include "interface/bril/shared/LUMITopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/HFTopics.hh"
#include "bril/supervisor/WebUtils.h"
#include "bril/supervisor/BestLumiProcessor.h"
#include "bril/supervisor/exception/Exception.h"

#include <cstdio>
#include <cmath>

XDAQ_INSTANTIATOR_IMPL(bril::supervisor::BestLumiProcessor)

namespace bril{
namespace supervisor {
static int maxnbx=3564;

BestLumiProcessor::BestLumiProcessor(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL),m_plock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::supervisor::BestLumiProcessor::Default, "Default");
    b2in::nub::bind(this, &bril::supervisor::BestLumiProcessor::onMessage);
    m_minbias = 78400;
    m_bus.fromString("brildata");//default
    m_tcdstopic = interface::bril::tcdsT::topicname();

    m_fillnum = 0;
    m_runnum = 0;
    m_lsnum = 0;
    m_nbnum = 0;
    m_tssec = 0;
    m_tsmsec = 0;
    //m_lastnbnum = 0;
    m_lasttcdscount = m_tcdscount = 0;
    m_deadtimefrac = 1.0;
    m_cmson = false;
    m_norb = 0;
    m_nbperls = 0;
    m_ncollidingbx = 0;
    m_delivered = -99;
    m_recorded = -99;
    m_avgpu = -99;
    m_storable = false;
    m_instanceid = 0;
    m_lumistorethreshold = 1.0e-4;
    if( getApplicationDescriptor()->hasInstanceNumber() ){
        m_instanceid = getApplicationDescriptor()->getInstance();
    }

    defineLumiMonTable();

    try{
        getApplicationInfoSpace()->fireItemAvailable("nbnum",&m_nbnum);
        getApplicationInfoSpace()->fireItemAvailable("minbias",&m_minbias);
        getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
        getApplicationInfoSpace()->fireItemAvailable("inTopics",&m_inTopicStr);
        getApplicationInfoSpace()->fireItemAvailable("lumiPriority",&m_lumiPriorityStr);
        getApplicationInfoSpace()->fireItemAvailable("lumistorethreshold",&m_lumistorethreshold);
        getApplicationInfoSpace()->fireItemAvailable("topicToflash",&m_topicToflash);
        getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
        getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
        toolbox::net::URN memurn("toolbox-mem-pool","brilBestLumiProcessor_mem");
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_instanceid.toString()+"_publishing","waiting");
        m_as_publishing = toolbox::task::bind(this,&bril::supervisor::BestLumiProcessor::publishing,"publishing");

    }catch(xcept::Exception & e){
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to set up infospace or memory pool", e);
    }
    try{
        std::string nid("lumiMon");
        std::string monurn = createQualifiedInfoSpace(nid).toString();
        m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));
        m_monInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
        m_monItemList.push_back("nbnum");
        m_monInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
        m_monItemList.push_back("lsnum");
        m_monInfoSpace->fireItemAvailable("runnum",&m_runnum);
        m_monItemList.push_back("runnum");
        m_monInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
        m_monItemList.push_back("fillnum");
        m_monInfoSpace->fireItemAvailable("inTopics",&m_inTopicStr);
        m_monItemList.push_back("inTopics");
        m_monInfoSpace->fireItemAvailable("lumiPriority",&m_lumiPriorityStr);
        m_monItemList.push_back("lumiPriority");
        m_monInfoSpace->fireItemAvailable("minbias",&m_minbias);
        m_monItemList.push_back("minbias");
        m_monInfoSpace->fireItemAvailable("deadtimefrac",&m_deadtimefrac);
        m_monItemList.push_back("deadtimefrac");
        m_monInfoSpace->fireItemAvailable("cmson",&m_cmson);
        m_monItemList.push_back("cmson");
        m_monInfoSpace->fireItemAvailable("norb",&m_norb);
        m_monItemList.push_back("norb");
        m_monInfoSpace->fireItemAvailable("nbperls",&m_nbperls);
        m_monItemList.push_back("nbperls");
        m_monInfoSpace->fireItemAvailable("ncollidingbx",&m_ncollidingbx);
        m_monItemList.push_back("ncollidingbx");
        m_monInfoSpace->fireItemAvailable("delivered",&m_delivered);
        m_monItemList.push_back("delivered");
        m_monInfoSpace->fireItemAvailable("recorded",&m_recorded);
        m_monItemList.push_back("recorded");
        m_monInfoSpace->fireItemAvailable("lumiprovider",&m_lumiprovider);
        m_monItemList.push_back("lumiprovider");
        m_monInfoSpace->fireItemAvailable("avgpu",&m_avgpu);
        m_monItemList.push_back("avgpu");
        m_monInfoSpace->fireItemAvailable("lumimonTable",&m_mon_lumitable);
        m_monItemList.push_back("lumimonTable");

        m_mon_configtable.insert(std::make_pair("inTopics",&m_inTopicStr));
        m_mon_configtable.insert(std::make_pair("lumiPriority",&m_lumiPriorityStr));
        m_mon_configtable.insert(std::make_pair("minBias",&m_minbias));

        m_mon_runtable.insert(std::make_pair("fill",&m_fillnum));
        m_mon_runtable.insert(std::make_pair("run",&m_runnum));
        m_mon_runtable.insert(std::make_pair("lsnum",&m_lsnum));
        m_mon_runtable.insert(std::make_pair("nbnum",&m_nbnum));

        m_mon_tcdstable.insert(std::make_pair("deadtimefrac",&m_deadtimefrac));
        m_mon_tcdstable.insert(std::make_pair("cmson",&m_cmson));
        m_mon_tcdstable.insert(std::make_pair("norb",&m_norb));
        m_mon_tcdstable.insert(std::make_pair("nbperls",&m_nbperls));
        m_mon_tcdstable.insert(std::make_pair("ncollidingbx",&m_ncollidingbx));

        m_mon_besttable.insert(std::make_pair("lumiprovider",&m_lumiprovider));
        m_mon_besttable.insert(std::make_pair("delivered",&m_delivered));
        m_mon_besttable.insert(std::make_pair("recorded",&m_recorded));
        m_mon_besttable.insert(std::make_pair("avgpu",&m_avgpu));

        m_monInfoSpace->addGroupRetrieveListener(this);
    }catch(xdata::exception::Exception& e){
        std::stringstream msg;
        msg<<"Failed to fire monitoring items";
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        XCEPT_RETHROW(bril::supervisor::exception::Exception,msg.str(),e);
    }catch(xdaq::exception::Exception& e){
        std::stringstream msg;
        msg<<"Failed to create infospace ";
        LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
        XCEPT_RETHROW(bril::supervisor::exception::Exception,stdformat_exception_history(e),e);
    }catch(std::exception& e){
        LOG4CPLUS_ERROR(getApplicationLogger(),"std error "+std::string(e.what()));
        XCEPT_RAISE(bril::supervisor::exception::Exception,e.what());
    }
}

BestLumiProcessor::~BestLumiProcessor(){
    do_clearpriority();
}

void BestLumiProcessor::Default(xgi::Input * in, xgi::Output * out){
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+getApplicationDescriptor()->getURN();
    *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
    *out << "<p> Eventing Status</p>";
    *out <<busesToHTML();
    m_monInfoSpace->lock();
    m_monInfoSpace->fireItemGroupRetrieve(m_monItemList,this);
    m_monInfoSpace->unlock();
    *out << bril::supervisor::WebUtils::mapToHTML("Configuration",m_mon_configtable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("Run Status",m_mon_runtable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("TCDS Status",m_mon_tcdstable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("Best Lumi",m_mon_besttable,"",false);
    *out << "<br>";
    *out << "<p> Incoming Lumi Data</p>";
    *out << bril::supervisor::WebUtils::xdatatableToHTML(&m_mon_lumitable);
}

void BestLumiProcessor::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        try{
            if(!m_inTopicStr.value_.empty()){
                m_intopics = toolbox::parseTokenSet(m_inTopicStr.value_,",");
            }
#ifdef x86_64_slc6
            std::map< std::string, std::string > p = m_topicToflash.getProperties();
#else
            std::map< std::string, std::string >& p = m_topicToflash;
#endif
            std::map< std::string, std::string >::iterator pit;
            for(pit = p.begin();pit!=p.end(); ++pit){
                std::string tname = pit->first;
                std::string flashname = pit->second;
                std::string urn = createQualifiedInfoSpace(std::string("lumimon_")+flashname).toString();
                xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(urn));
                m_flashinfostore.insert(std::make_pair(tname,is));//topic name to flash infospace
                init_lumiFlash(is,tname);
            }
            //fireItemAvailable for all
            declareFlashlists();
            if(!m_lumiPriorityStr.value_.empty()){
                std::list<std::string> li = toolbox::parseTokenList(m_lumiPriorityStr.value_,",");
                std::copy(li.begin(), li.end(), std::back_inserter(m_lumipriority) );
            }
            initializeLumiMonTable();
            this->getEventingBus(m_bus.value_).addActionListener(this);

            //activate workloop
            m_publishing->activate();
        }catch(eventing::api::exception::Exception& e){
            std::string msg("Failed to listen to eventing bus "+m_bus.value_);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(exception::Exception,msg,e);
        }catch(xcept::Exception& e){
            std::string msg("Failed to setup infospaces for flashlists");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(exception::Exception,msg,e);
        }
    }else if ( e.type() == "ItemChangedEvent"){
        std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
        if(item=="nbnum"){
            //submit workloop
            //LOG4CPLUS_INFO(getApplicationLogger(),"start publishing workloop");
            m_publishing->submit(m_as_publishing);
        }
    }
}

void BestLumiProcessor::actionPerformed(toolbox::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
    if ( e.type() == "eventing::api::BusReadyToPublish" ){
        std::set<std::string>::iterator it;
        for(it=m_intopics.begin(); it!=m_intopics.end(); ++it){
            this->getEventingBus(m_bus.value_).subscribe(*it);
        }
    }
}

void BestLumiProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        if(ref!=0){
            std::stringstream msg;
            std::string data_version = plist.getProperty("DATA_VERSION");
            std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
            if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
                std::string msg("Received brildaq message "+topic+" with no or wrong version, do not process.");
                LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                ref->release(); ref=0;
                return;
            }
            if(payloaddict.empty()){
                msg<<"Received "<<topic<<" with no dictionary, do not process";
                LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
                ref->release(); ref = 0;
                return;
            }
            interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
            msg<<"received "<<topic<<": fill "<<thead->fillnum<<", run "<<thead->runnum<<", ls "<<thead->lsnum<<", " <<" nb "<<thead->nbnum;
            LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
            msg.str("");msg.clear();
            std::vector<std::string>::iterator lit = std::find(m_lumipriority.begin(),m_lumipriority.end(),topic);
            if( topic == m_tcdstopic ){
                m_applock.take();
                m_fillnum.value_ = thead->fillnum;
                m_nbnum.value_ = thead->nbnum;
                m_lsnum.value_ = thead->lsnum;
                m_runnum.value_ = thead->runnum;
                m_tssec.value_ = thead->timestampsec;
                m_tsmsec.value_ = thead->timestampmsec;
                m_deadtimefrac.value_ = -1.;
                m_tcdscount++;
                m_applock.give();
                interface::bril::shared::CompoundDataStreamer tc(payloaddict);
                unsigned int norb = 0;
                unsigned int nbperls = 0;
                bool cmson = false;
                unsigned int ncollidingbx = 0;
                float dtfrac = 1.;
                tc.extract_field( &norb, "norb", thead->payloadanchor );
                tc.extract_field( &nbperls, "nbperls", thead->payloadanchor );
                tc.extract_field( &cmson, "cmson", thead->payloadanchor );
                tc.extract_field( &dtfrac, "deadfrac", thead->payloadanchor );
                tc.extract_field( &ncollidingbx, "ncollidingbx", thead->payloadanchor );
                if( isnan(dtfrac)||isinf(dtfrac) ){
                    LOG4CPLUS_ERROR(getApplicationLogger(), "deadfrac is Nan or Inf, reset to 1.");
                    dtfrac = 1.;
                }
                msg.str("");
                msg<<"norb "<<norb<<" nbperls "<<nbperls<<" cmson "<<cmson<<" deadtime "<<dtfrac<<" ncollidingbx "<<ncollidingbx;
                m_applock.take();
                m_deadtimefrac.value_ = dtfrac;
                m_cmson.value_ = cmson;
                m_norb.value_ = norb;
                m_nbperls.value_ = nbperls;
                m_ncollidingbx.value_ = ncollidingbx;
                m_applock.give();
                LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
            }else if( topic == interface::bril::beamT::topicname() ){
                char beamstatus[50];
                interface::bril::shared::CompoundDataStreamer tc(payloaddict);
                tc.extract_field( beamstatus, "status", thead->payloadanchor );
                m_beamstatus.value_ = std::string(beamstatus);
                m_storable = isDataStorable( std::string(beamstatus) );
            }else if( lit!=m_lumipriority.end() ){
                // intercept lumi message for monitoring
                interface::bril::shared::CompoundDataStreamer tc(payloaddict);
                interface::bril::shared::DatumHead * mhead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                float avg = -1; float avgraw = -1;
                unsigned int masklow = 0;
                unsigned int maskhigh = 0;
                unsigned int fillnum = thead->fillnum;
                unsigned int runnum = thead->runnum;
                unsigned int lsnum = thead->lsnum;
                if(fillnum<=0 || fillnum>9999 || runnum<=0 || runnum>999999 || lsnum<=0 || lsnum>999999 ){
                    //filter out out of range time headers
                    ref->release(); ref=0;
                    LOG4CPLUS_INFO(getApplicationLogger(), "discard time header out of range from "+topic);
                    return;
                }
                //float bxraw[bril::supervisor::maxnbx];
                float bxlumi[bril::supervisor::maxnbx];
                //for(int i=0;i<bril::supervisor::maxnbx;++i) bxraw[i]=0;
                for(int i=0;i<bril::supervisor::maxnbx;++i) bxlumi[i]=0;
                tc.extract_field( &avg, "avg", mhead->payloadanchor );
                tc.extract_field( &avgraw, "avgraw", mhead->payloadanchor );
                //tc.extract_field( bxraw, "bxraw", mhead->payloadanchor );
                tc.extract_field( bxlumi, "bx", mhead->payloadanchor );
                tc.extract_field( &masklow, "masklow", mhead->payloadanchor );
                tc.extract_field( &maskhigh, "maskhigh", mhead->payloadanchor );
                char calibtag[50];
                tc.extract_field( calibtag, "calibtag", mhead->payloadanchor );

                if( isnan(avg)||isinf(avg) ){
                    ref->release(); ref=0;
                    LOG4CPLUS_ERROR(getApplicationLogger(), topic+": avg is Nan or Inf, discarding");
                    return;
                }
                if( isnan(avgraw)||isinf(avgraw) ){
                    LOG4CPLUS_ERROR(getApplicationLogger(), topic+": avgraw is Nan or Inf, reset to 0.");
                    avgraw = 0.;
                }
                for( int i=0; i<bril::supervisor::maxnbx; ++i){
                    if( isnan(bxlumi[i])||isinf(bxlumi[i]) ){ //if found one corrupt bunch, reset the whole array
                        LOG4CPLUS_ERROR(getApplicationLogger(), topic+": bxlumi contains Nan or Inf, reset all to 0.");
                        for( int j=0; j<bril::supervisor::maxnbx; ++j){
                            bxlumi[j] = 0;
                        }
                        break;
                    }
                }
                // hyperdaq mon
                xdata::Float xavg(avg);
                xdata::Float xavgraw(avgraw);
                xdata::String xcalibtag;
                xcalibtag.fromString(std::string(calibtag));
                size_t rowid = std::distance(m_lumipriority.begin(),lit);
                xdata::String xts;
                msg.str("");
                msg<<mhead->fillnum<<" "<<mhead->runnum<<" "<<mhead->lsnum<<" "<<mhead->nbnum<<" "<<mhead->timestampsec<<"."<<mhead->timestampmsec;
                xts.fromString(msg.str());
                xdata::String xtopic; xtopic.fromString(topic);
                m_mon_lumitable.setValueAt(rowid,"Topic",xtopic);
                m_mon_lumitable.setValueAt(rowid,"Avg",xavg);
                m_mon_lumitable.setValueAt(rowid,"AvgRaw",xavgraw);
                m_mon_lumitable.setValueAt(rowid,"Calibtag",xcalibtag);
                m_mon_lumitable.setValueAt(rowid,"TimeInfo",xts);

                std::vector<int> nonzerobx;
                float maxlumi = -99;
                unsigned int maxbx = 0;
                for(int i=0; i<bril::supervisor::maxnbx;++i ){
                    if(bxlumi[i]>0) {
                        nonzerobx.push_back(i);
                    }
                    if(bxlumi[i]>maxlumi){
                        maxbx = i;
                        maxlumi = bxlumi[i];
                    }
                }
                xdata::UnsignedInteger nlumibx(nonzerobx.size());
                xdata::UnsignedInteger xmaxbx(maxbx);
                m_mon_lumitable.setValueAt(rowid,"NFilledBX",nlumibx);
                m_mon_lumitable.setValueAt(rowid,"MaxBXId",xmaxbx);

                // detlumi flashlist mon
                std::map< std::string,xdata::InfoSpace* >::iterator fIt = m_flashinfostore.find(*lit);
                if( fIt!=m_flashinfostore.end() ){
                    xdata::InfoSpace* detlumiIS = fIt->second;
                    try{
                        xdata::UnsignedInteger32* p_runnum = dynamic_cast<xdata::UnsignedInteger32* >( detlumiIS->find("runnum") );
                        xdata::UnsignedInteger32* p_lsnum = dynamic_cast<xdata::UnsignedInteger32* >( detlumiIS->find("lsnum") );
                        if( (p_lsnum->value_!=mhead->lsnum) || (p_runnum->value_!=mhead->runnum) ){
                            p_lsnum->value_ = mhead->lsnum;
                            p_runnum->value_ = mhead->runnum;
                            xdata::UnsignedInteger32* p_fillnum = dynamic_cast<xdata::UnsignedInteger32* >( detlumiIS->find("fillnum") );
                            p_fillnum->value_ = mhead->fillnum;

                            xdata::TimeVal* p_timestamp = dynamic_cast<xdata::TimeVal* >( detlumiIS->find("timestamp") );
                            p_timestamp->value_.sec(mhead->timestampsec);
                            p_timestamp->value_.usec(mhead->timestampmsec*1000);
                            xdata::UnsignedInteger32* p_masklow = dynamic_cast<xdata::UnsignedInteger32* >( detlumiIS->find("masklow") );
                            p_masklow->value_ = masklow;
                            xdata::UnsignedInteger32* p_maskhigh = dynamic_cast<xdata::UnsignedInteger32* >( detlumiIS->find("maskhigh") );
                            p_maskhigh->value_ = maskhigh;
                            xdata::String* p_calibtag = dynamic_cast<xdata::String* >( detlumiIS->find("calibtag") );
                            p_calibtag->value_ = xcalibtag.value_;
                            xdata::Float* p_avglumi = dynamic_cast<xdata::Float* >( detlumiIS->find("avglumi") );
                            p_avglumi->value_ = xavg.value_;
                            xdata::Vector<xdata::Integer>* p_bxidx =  dynamic_cast< xdata::Vector<xdata::Integer>* >( detlumiIS->find("bxidx") );
                            p_bxidx->clear();
                            xdata::Vector<xdata::Float>* p_bxlumi = dynamic_cast< xdata::Vector<xdata::Float>* >( detlumiIS->find("bxlumi") );
                            p_bxlumi->clear();
                            for( std::vector<int>::iterator nonzeroIt = nonzerobx.begin(); nonzeroIt!=nonzerobx.end(); ++nonzeroIt) {
                                p_bxidx->push_back( xdata::Integer( *nonzeroIt ) );
                                p_bxlumi->push_back( xdata::Float(bxlumi[ *nonzeroIt ]) );
                            }
                            //fire flashlist
                            fireFlashlist(*lit);
                        }
                    }catch(xdata::exception::Exception& e){
                        std::string msg("Failed to find item in infospace for flashlist of ");
                        msg+=fIt->first;
                        LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                        XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
                        this->notifyQualified("error",myerrorobj);
                    }
                }

                // push into priority queue
                m_plock.take();
                m_pq.push(TopicItem(topic,payloaddict,ref->duplicate(),m_lumipriority));
                m_plock.give();
            }
            //if( m_nbnum.value_ != m_lastnbnum ){ //use nbnum change as signal does not work if someone screw up the nb number
            if( m_lasttcdscount!=m_tcdscount ){//here we require only tcds signal. we don't look into nbnum values
                getApplicationInfoSpace()->fireItemValueChanged("nbnum",this);//first (4) nbnum change
                m_lasttcdscount = m_tcdscount;
            }
            m_plock.take();
            if( m_pq.size()>(m_lumipriority.size()*2) ){
                LOG4CPLUS_ERROR(getApplicationLogger(),"Priority queue is STALE, data are LOST!");
                do_clearpriority();
            }
            m_plock.give();
        }
    }

    if(ref!=0){
        ref->release();
        ref=0;
    }
}

void BestLumiProcessor::do_clearpriority(){
    if( m_pq.empty() ) return;
    while( !m_pq.empty() ){
        bril::supervisor::TopicItem m = m_pq.top();
        m_pq.pop();
        if( m.ref() ){
            std::stringstream ss;
            ss << "do_clearpriority releasing "<<m.ref()->getBuffer();
            LOG4CPLUS_INFO(getApplicationLogger(), ss.str() );
            if( m.ref()->getBuffer()==0 ){
                LOG4CPLUS_INFO(getApplicationLogger(), "WOULD HAVE! ");
            }else{
                m.releaseRef();
            }
        }
    }
}

bool BestLumiProcessor::publishing(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_INFO(getApplicationLogger(), "Entering publishing");
    usleep(400000);
    m_plock.take();
    while( !m_pq.empty() ){
        bril::supervisor::TopicItem m = m_pq.top();
        m_pq.pop();
        LOG4CPLUS_INFO(getApplicationLogger(), "publishing top priority "+m.name() );
        bool published = do_publish_bestlumi(m.name(),m.payloaddict(),m.ref());
        if(published){
            do_clearpriority();
        }
    }
    m_plock.give();
    LOG4CPLUS_INFO(getApplicationLogger(), "Leaving publishing");
    return false;
}

bool BestLumiProcessor::do_publish_bestlumi( const std::string& lumitopic, const std::string& payloaddict, toolbox::mem::Reference* bufRef){
    std::stringstream msg;
    // default values
    std::string lumiprovider("UNKNOWN");
    float delivered = 0.;
    float recorded = 0.;
    float avgpu = 0.;
    float bxdelivered[bril::supervisor::maxnbx];
    for(int i=0; i<bril::supervisor::maxnbx; ++i) bxdelivered[i]=0.;
    // parse in message
    float avg = 0;
    float bx[bril::supervisor::maxnbx];
    for(int i=0; i<bril::supervisor::maxnbx; ++i) bx[i]=0.;
    char calibtag[50];

    interface::bril::shared::CompoundDataStreamer bufstreamer(payloaddict);
    interface::bril::shared::DatumHead * mhead = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    //unsigned int datasourceid = mhead->getDataSourceID();
    //if(datasourceid>0){
    //	lumiprovider = interface::bril::NameById(interface::bril::DataSource::lookuptable,datasourceid);
    //}
    //hardcoded hack on lumiprovider appearance
    if (lumitopic=="pltlumizero"){
        lumiprovider = "PLTZERO";
    }else if (lumitopic=="pltlumi"){
        lumiprovider = "PLT";
    }else if (lumitopic=="bcm1flumi"){
        lumiprovider = "BCM1F";
    }else if(lumitopic=="hflumi"){
        lumiprovider = "HFOC";
    }else if(lumitopic=="hfetlumi"){
        lumiprovider = "HFET";
    }

    bool estatus = false;
    estatus = bufstreamer.extract_field( calibtag, "calibtag", mhead->payloadanchor );
    if(!estatus){
        calibtag[0] = '\0';
        LOG4CPLUS_ERROR(getApplicationLogger(),"failed to extract calibtag, use default");
    }
    ////bufstreamer.extract_field( &avgraw, "avgraw", mhead->payloadanchor );
    ////bufstreamer.extract_field( bxraw, "bxraw", mhead->payloadanchor );
    estatus = bufstreamer.extract_field( &avg, "avg", mhead->payloadanchor );
    if(!estatus){
        avg = 0;
        LOG4CPLUS_ERROR(getApplicationLogger(),"failed to extract avg, use default");
    }
    if( isnan(avg)||isinf(avg) ){
        avg = 0;
    }
    estatus = bufstreamer.extract_field( bx, "bx", mhead->payloadanchor );
    if(!estatus){
        for(int i=0; i<bril::supervisor::maxnbx; ++i) bx[i]=0.;
        LOG4CPLUS_ERROR(getApplicationLogger(),"failed to extract bx, use default");
    }

    if(bufRef){
        msg.str("");
        msg << "do_publish_bestlumi releasing " << bufRef->getBuffer();
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        if( bufRef->getBuffer()==0 ){
            LOG4CPLUS_INFO(getApplicationLogger(), "WOULD HAVE! ");
        }else{
            bufRef->release(); bufRef = 0;
        }
    }
    if(m_ncollidingbx.value_>0){
        float rotationrate = 11245.613;
        avgpu = (avg/m_ncollidingbx.value_)*float(m_minbias.value_)/rotationrate;
    }
    delivered = avg;
    recorded = avg*(1.-m_deadtimefrac.value_);
    for(int i=0; i<bril::supervisor::maxnbx; ++i){
        if( isnan(bx[i])||isinf(bx[i]) ){
            bxdelivered[i] = 0;
        }else{
            bxdelivered[i] = bx[i];
        }
    }
    m_applock.take();
    m_delivered.value_ = delivered;
    m_recorded.value_ = recorded;
    m_avgpu.value_ = avgpu;
    m_lumiprovider.value_ = lumiprovider;
    m_applock.give();

    // build bestlumi
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",interface::bril::shared::bestlumiT::payloaddict());
    if( !m_storable || m_delivered.value_<m_lumistorethreshold.value_ ){
        plist.setProperty("NOSTORE","1");
        LOG4CPLUS_INFO(getApplicationLogger(),"NOSTORE bestlumi");
    }
    toolbox::mem::Reference* bestlumiRef = 0;
    size_t totalsize = interface::bril::shared::bestlumiT::maxsize();
    std::string pdict = interface::bril::shared::bestlumiT::payloaddict();
    //std::cout<<"bestlumidict "<<pdict<<" totalsize "<<totalsize<<std::endl;
    bestlumiRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bestlumiRef->setDataSize(totalsize);
    interface::bril::shared::DatumHead* bestlumiheader = (interface::bril::shared::DatumHead*)(bestlumiRef->getDataLocation());
    bestlumiheader->setTime(mhead->fillnum,mhead->runnum,mhead->lsnum,mhead->nbnum,mhead->timestampsec,mhead->timestampmsec);
    bestlumiheader->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
    bestlumiheader->setFrequency(4);
    bestlumiheader->setTotalsize(totalsize);
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    //std::cout<<"lumiprovider "<<lumiprovider<<std::endl;
    tc.insert_field( bestlumiheader->payloadanchor, "provider", lumiprovider.c_str() );
    //std::cout<<"calibtag "<<calibtag<<std::endl;
    tc.insert_field( bestlumiheader->payloadanchor, "calibtag", calibtag );
    //std::cout<<"delivered "<<delivered<<std::endl;
    tc.insert_field( bestlumiheader->payloadanchor, "delivered", &delivered );
    //std::cout<<"recorded "<<recorded<<std::endl;
    tc.insert_field( bestlumiheader->payloadanchor, "recorded", &recorded );
    //std::cout<<"avgpu "<<avgpu<<std::endl;
    tc.insert_field( bestlumiheader->payloadanchor, "avgpu", &avgpu );
    tc.insert_field( bestlumiheader->payloadanchor, "bxdelivered", bxdelivered );
    msg.str("");
    msg<<"provider "<<lumiprovider<<" calibtag "<<calibtag<<" delivered(Hz/ub) "<<delivered<<" recorded(Hz/ub) "<<recorded<<" avgpu "<<avgpu <<" fill "<<mhead->fillnum<<" run "<<mhead->runnum<<" ls "<<mhead->lsnum<<" nbnum "<<mhead->nbnum<<" tssec "<<mhead->timestampsec;
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());

    //build and fire bestlumi flashlist
    //std::cout<<"so far m_flashinfostore size "<<m_flashinfostore.size()<<std::endl;
    std::map< std::string,xdata::InfoSpace* >::iterator fIt = m_flashinfostore.find("bestlumi");
    if( fIt!=m_flashinfostore.end() ){
        xdata::InfoSpace* bestlumiIS = fIt->second;
        try{
            xdata::UnsignedInteger32* p_runnum = dynamic_cast<xdata::UnsignedInteger32* >( bestlumiIS->find("runnum") );
            xdata::UnsignedInteger32* p_lsnum = dynamic_cast<xdata::UnsignedInteger32* >( bestlumiIS->find("lsnum") );
            if( (p_lsnum->value_!=mhead->lsnum) || (p_runnum->value_!=mhead->runnum) ){
                p_runnum->value_ = mhead->runnum;
                p_lsnum->value_ = mhead->lsnum;
                xdata::UnsignedInteger32* p_fillnum = dynamic_cast<xdata::UnsignedInteger32* >( bestlumiIS->find("fillnum") );
                p_fillnum->value_ = mhead->fillnum;
                xdata::TimeVal* p_timestamp = dynamic_cast<xdata::TimeVal* >( bestlumiIS->find("timestamp") );
                p_timestamp->value_.sec(mhead->timestampsec);
                p_timestamp->value_.usec(mhead->timestampmsec*1000);

                xdata::String* p_provider = dynamic_cast<xdata::String* >( bestlumiIS->find("provider") );
                p_provider->value_ = lumiprovider;
                xdata::Integer* p_ncollidingbx = dynamic_cast<xdata::Integer* >( bestlumiIS->find("ncollidingbx") );
                p_ncollidingbx->value_ = (int)m_ncollidingbx.value_;
                xdata::Float* p_avgpu = dynamic_cast<xdata::Float* >( bestlumiIS->find("avgpu") );
                p_avgpu->value_ = m_avgpu.value_;
                xdata::Float* p_deadtimefrac = dynamic_cast<xdata::Float* >( bestlumiIS->find("deadtimefrac") );
                p_deadtimefrac->value_ = m_deadtimefrac.value_;
                xdata::String* p_beamstatus = dynamic_cast<xdata::String* >( bestlumiIS->find("beamstatus") );
                p_beamstatus->value_ = m_beamstatus.value_;
                //std::cout<<"FIRE bestlumi flash list"<<std::endl;
                fireFlashlist("bestlumi");
            }
        }catch(xdata::exception::Exception& e){
            std::string msg("Failed to find item in infospace for flashlist of bestlumi");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
            this->notifyQualified("error",myerrorobj);
        }
    }

    try{
      getEventingBus(m_bus.value_).publish(interface::bril::shared::bestlumiT::topicname(),bestlumiRef ,plist);
    }catch(xcept::Exception& e){
        msg<<"Failed to publish bestlumi to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bestlumiRef){ bestlumiRef->release(); bestlumiRef = 0; }
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(exception::Exception,myerrorobj,msg.str(),e);
        this->notifyQualified("error",myerrorobj);
        return false;
    }
    return true;
}

void BestLumiProcessor::defineLumiMonTable(){
    m_mon_lumitable.addColumn("Topic","string");
    m_mon_lumitable.addColumn("Calibtag","string");
    m_mon_lumitable.addColumn("Avg","float");
    m_mon_lumitable.addColumn("AvgRaw","float");
    m_mon_lumitable.addColumn("NFilledBX","unsigned int");
    m_mon_lumitable.addColumn("MaxBXId","unsigned int");
    m_mon_lumitable.addColumn("TimeInfo","string");
}

void BestLumiProcessor::initializeLumiMonTable(){
    for(std::vector<std::string>::iterator it=m_lumipriority.begin(); it!=m_lumipriority.end(); ++it){
        size_t row = std::distance(m_lumipriority.begin(),it);
        xdata::String lumitopic;lumitopic.fromString(*it);
        m_mon_lumitable.setValueAt(row,"Topic",lumitopic);
        xdata::String calib; calib.fromString("UNKNOWN");
        m_mon_lumitable.setValueAt(row,"Calibtag",calib);
        xdata::Float avglumi(-1);
        m_mon_lumitable.setValueAt(row,"Avg",avglumi);
        xdata::Float avgrawlumi(-1);
        m_mon_lumitable.setValueAt(row,"AvgRaw",avgrawlumi);
        xdata::UnsignedInteger nbx(0);
        m_mon_lumitable.setValueAt(row,"NFilledBX",nbx);
        xdata::UnsignedInteger maxbx(0);
        m_mon_lumitable.setValueAt(row,"MaxBXId",maxbx);
    }
}

bool BestLumiProcessor::isDataStorable(const std::string& beamstatus) const{
    if(beamstatus.empty()) return true;
    if(beamstatus!="STABLE BEAMS" && beamstatus!="SQUEEZE" && beamstatus!="ADJUST" && beamstatus!="FLAT TOP"){
        return false;
    }
    return true;
}

void BestLumiProcessor::init_lumiFlash(xdata::InfoSpace* is,const std::string& tname){
    if(tname=="bestlumi"){
        init_bestlumiFlash(is);
    }else{
        init_detlumiFlash(is);
    }
}

void BestLumiProcessor::init_detlumiFlash(xdata::InfoSpace* is){
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;
    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal(toolbox::TimeVal()) );
    fieldnames.push_back( fieldname );

    fieldname = "masklow";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "maskhigh";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "calibtag";
    fieldvalues.push_back( new xdata::String("") );
    fieldnames.push_back( fieldname );

    fieldname = "avglumi";
    fieldvalues.push_back( new xdata::Float(0.) );
    fieldnames.push_back( fieldname );

    fieldname = "bxlumi";
    fieldvalues.push_back( new xdata::Vector<xdata::Float>() );
    fieldnames.push_back( fieldname );

    fieldname = "bxidx";
    fieldvalues.push_back( new xdata::Vector<xdata::Integer>() );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
}

void BestLumiProcessor::init_bestlumiFlash(xdata::InfoSpace* is){
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
    fieldnames.push_back( fieldname );

    fieldname = "provider";
    fieldvalues.push_back( new xdata::String("") );
    fieldnames.push_back( fieldname );

    fieldname = "ncollidingbx";
    fieldvalues.push_back( new xdata::Integer(0) );
    fieldnames.push_back( fieldname );

    fieldname = "avgpu";
    fieldvalues.push_back( new xdata::Float(0.) );
    fieldnames.push_back( fieldname );

    fieldname = "deadtimefrac";
    fieldvalues.push_back( new xdata::Float(0.) );
    fieldnames.push_back( fieldname );

    fieldname = "beamstatus";
    fieldvalues.push_back( new xdata::String("") );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
}

void BestLumiProcessor::fireFlashlist(const std::string& topicname){
    std::map< std::string,xdata::InfoSpace* >::iterator it=m_flashinfostore.find(topicname);
    try{
        if( it!=m_flashinfostore.end() ){
            xdata::InfoSpace* is = it->second;
            if(is){
                std::map< xdata::InfoSpace*, std::list<std::string> >::iterator fit = m_flashfields.find(is);
                if( fit!=m_flashfields.end() ) is->fireItemGroupChanged(fit->second,this);
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"");
                XCEPT_DECLARE(bril::supervisor::exception::Exception,myerrorobj,topicname+" flashlist is not associated to an infospace");
                notifyQualified("error",myerrorobj);
            }
        }
    }catch(xdata::exception::Exception& e){
        std::string msg("Failed to fire flashlist for topic "+topicname);
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(bril::supervisor::exception::Exception,myerrorobj,msg,e);
        notifyQualified("error",myerrorobj);
    }
}

void BestLumiProcessor::declareFlashlists(){
    std::map< std::string,xdata::InfoSpace* >::iterator it = m_flashinfostore.begin();
    try{
        for(; it!=m_flashinfostore.end(); ++it ){ //loop over flashlists
            xdata::InfoSpace* is = it->second;
            std::map< xdata::InfoSpace*, std::list<std::string> >::iterator nameit=m_flashfields.find(is);
            std::map< xdata::InfoSpace*, std::vector<xdata::Serializable*> >::iterator valueit=m_flashcontent.find(is);
            if(nameit!=m_flashfields.end()&&valueit!=m_flashcontent.end() ){
                std::list<std::string>::const_iterator nIt = nameit->second.begin();
                std::vector<xdata::Serializable*>::const_iterator vIt = valueit->second.begin();
                for(; nIt!=nameit->second.end() && vIt!=valueit->second.end(); ++nIt, ++vIt){
                    is->fireItemAvailable(*nIt,*vIt);
                }
            }
        }
    }catch(xdata::exception::Exception& e){
        std::string msg( "Failed to declare flashlist for topic "+it->first );
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(bril::supervisor::exception::Exception,myerrorobj,msg,e);
        notifyQualified("error",myerrorobj);
    }
}

void BestLumiProcessor::cleanFlashlists(){
    std::map< std::string,xdata::InfoSpace* >::iterator it = m_flashinfostore.begin();
    for(; it!=m_flashinfostore.end(); ++it ){ //loop over flashlists
        xdata::InfoSpace* is = it->second;
        std::map< xdata::InfoSpace*, std::vector<xdata::Serializable*> >::iterator valueit=m_flashcontent.find(is);
        if(valueit!=m_flashcontent.end() ){
            std::vector<xdata::Serializable*>::const_iterator vIt = valueit->second.begin();
            for(; vIt!=valueit->second.end(); ++vIt){
                delete *vIt;
            }
        }
    }
}
}
}//ns bril::supervisor
