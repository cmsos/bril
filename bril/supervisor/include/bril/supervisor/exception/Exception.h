#ifndef _bril_supervisor_exception_Exception_h_
#define _bril_supervisor_exception_Exception_h_
#include "xcept/Exception.h"
namespace bril{ 
  namespace supervisor{
    namespace exception { 
      class Exception: public xcept::Exception{
      public: 
      Exception( std::string name, std::string message, std::string module, int line, std::string function ):xcept::Exception(name, message, module, line, function) {} 
      Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):xcept::Exception(name, message, module, line, function, e) {} 
      };       
    }//ns exception
  }//ns supervisor

   XCEPT_DEFINE_EXCEPTION(supervisor,BestLumiProcessor)

   XCEPT_DEFINE_EXCEPTION(supervisor,CalibDataService)

   XCEPT_DEFINE_EXCEPTION(supervisor,DBConnectionError)

   XCEPT_DEFINE_EXCEPTION(supervisor,DBQueryError)  

   XCEPT_DEFINE_EXCEPTION(supervisor,TagNotFoundError)

   XCEPT_DEFINE_EXCEPTION(supervisor,MedianOnEmptyContainer)
     
}//ns bril
#endif
