#ifndef _bril_supervisor_Events_h_
#define _bril_supervisor_Events_h_

#include "toolbox/Event.h"

namespace bril{
  namespace supervisor{

    class LumiSectionChangedEvent : public toolbox::Event{
    public:
      LumiSectionChangedEvent();
      ~LumiSectionChangedEvent();
    };    
   
   class TopicLumiSectionChangedEvent : public LumiSectionChangedEvent{
   public:
      TopicLumiSectionChangedEvent(const std::string& topicname);
      ~TopicLumiSectionChangedEvent();
      std::string topicname_;
   };

  }
}

#endif
