#ifndef _bril_supervisor_CalibDataService_h_
#define _bril_supervisor_CalibDataService_h_
#include <string>
#include <map>
#include <vector>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Integer.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/mem/Reference.h"
#include "interface/bril/CalibData.h"
#include "soci/soci.h"

namespace bril{ namespace supervisor {
    class CalibDataService : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      CalibDataService(xdaq::ApplicationStub* s);
      // destructor
      ~CalibDataService();     
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

    private: 
      void do_publish();
      void initcache();
      bool is_currentrunvalid(unsigned long long tagnameid, int runnum);
      unsigned long long gettagnameid(const std::string& tagname);
      /**
	 fill validity and functor cache       
	 return false if no valid data found
      */
      void getvaliddata(unsigned long long tagnameid, int runnum);
      void cleanresource();
      bool parseDBparam(const std::string& authf, const std::string& dbalias, std::string& o_netservice, std::string& o_dbuser, std::string& o_dbpass);
      void parsePayloadString(const std::string& func_payload, std::map<std::string,std::string>& result);
      void parseCoefsStr(const std::string& coefsStr, float* coefs, size_t& ncoefs);
      void parseAfterglowStr(const std::string& afterglowStr, interface::bril::AfterglowThreshold* afterglow, size_t& nafterglow);
      //force close / release long time opening connections 
      void closeOpenConnections();
      //config params      
      xdata::String m_normtagsStr;//normtags string
      xdata::String m_bus; //dest bus
      toolbox::mem::Pool* m_memPool;      
      xdata::String m_dbalias; //db alias
      xdata::String m_dbschema; //db schema
      xdata::Integer m_connectionPoolSize; //db connection pool size
      xdata::UnsignedInteger m_connectionLifetime; //db connection lifetime
      xdata::String m_authpath; //db authentication file
      // database access
      int m_leasetimeout; //connection pool lease timeout (millisec)
      std::string m_connectstr; //soci db connect string
      soci::connection_pool* m_connectionPool; //db connection pool
      std::map<size_t,soci::session*> m_openconnections; //registry of currently open connections

      //data cache
      xdata::Integer m_lastrun;
      xdata::Integer m_currentrun;
      bool m_isfirstrun;
      bool m_resourceclean;
      std::set<std::string> m_normtags; 
      //std::map< std::string, unsigned long long > m_tagnametoid;//configured tagname:tagnameid   
      std::map< unsigned long long, std::string > m_tagidtoname;//configured tagnameid:tagname
      typedef std::map< unsigned long long, std::pair< int, int> > validitymap_t;
      validitymap_t m_validity; //tagnameid: current validity

      typedef std::map< unsigned long long, std::pair< std::string, interface::bril::CalibData* > > calibratormap_t;
      calibratormap_t  m_calibfunctor; //tagnameid: (calibfunctorname,calibfunctorobj)      
      // nocopy protection
      CalibDataService (const CalibDataService&);
      CalibDataService & operator=(const CalibDataService&);
    };
  }}
#endif
