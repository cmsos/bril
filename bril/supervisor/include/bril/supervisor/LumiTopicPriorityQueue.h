#ifndef _bril_LumiTopicPriorityQueue_h_
#define _bril_LumiTopicPriorityQueue_h_
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include "bril/supervisor/LumiTopicItem.h"
#include "bril/supervisor/exception/Exception.h"

namespace bril{ namespace supervisor{

    struct ComparePriority {
      explicit ComparePriority(size_t p):m_p(p){}
      inline bool operator()(const LumiTopicItem& mc) const {
	return mc.priority() == m_p;
      }
      size_t m_p;
    };


    typedef std::priority_queue< LumiTopicItem, std::vector<LumiTopicItem>, std::greater<LumiTopicItem> > QueueBase;

    class LumiTopicPriorityQueue : public std::priority_queue< LumiTopicItem, std::vector<LumiTopicItem>, std::greater<LumiTopicItem> > {

    public: 
      LumiTopicPriorityQueue(){}
      ~LumiTopicPriorityQueue(){}
      float median_avglumi() const {
	if( empty() ){
	  XCEPT_RAISE(bril::supervisor::exception::MedianOnEmptyContainer,"Median on empty container");
	}	
	std::vector<float> avgs;
	for(std::vector<LumiTopicItem>::const_iterator it=c.begin(); it!=c.end(); ++it ){
	  avgs.push_back( it->avg );
	}
	std::sort(avgs.begin(), avgs.end() );
	//This is not true median. for lumi, if even number of elements,we pick the higher bound of the middle.
	return avgs[avgs.size()/2]; 
      }
      bool allinrange(float lower, float upper)const{
	for(std::vector<LumiTopicItem>::const_iterator it=c.begin(); it!=c.end(); ++it ){
	  if(it->avg>upper || it->avg<lower ){
	    return false;
	  }
	}
	return true;
      }
      void enqueue( LumiTopicItem& value ){
	size_t ppos = value.priority();
	std::vector<LumiTopicItem>::iterator it = std::find_if( c.begin(),c.end(),ComparePriority(ppos) );
	if( it!=c.end() ){
	  //swap gut
	  std::swap<LumiTopicItem>( *it,value );
	}else{
	  push(value);
	}
      }

      friend std::ostream& operator<<(std::ostream& os, const LumiTopicPriorityQueue& ti){
	for(std::vector<LumiTopicItem>::const_iterator it=ti.c.begin(); it!=ti.c.end(); ++it ){
	  if( it!=ti.c.begin() )  os<<" , ";	  
	  os<<*it;
	}	
	return os;
      }
    };
  }
}
#endif
