#ifndef _bril_BestLumiProcessor2_h_
#define _bril_BestLumiProcessor2_h_
#include <string>
#include <set>
#include <queue>
#include <vector>
#include <list>
#include <algorithm>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/task/TimerListener.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/Boolean.h"
#include "xdata/Table.h"
#include "xdata/Vector.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"
#include "bril/supervisor/LumiTopicPriorityQueue.h"

namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{ namespace supervisor {    
        
    class BestLumiProcessor2 : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      BestLumiProcessor2(xdaq::ApplicationStub* s);
      // destructor
      ~BestLumiProcessor2();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //workloop 
      bool publishing(toolbox::task::WorkLoop* wl);
    protected:      
      // configuration parameters
      xdata::UnsignedInteger32 m_minbias;
      xdata::String m_bus;
      std::string m_tcdstopic;
      xdata::String m_lumiPriorityStr;
      std::vector<std::string> m_lumipriority;
      xdata::String m_inTopicStr;
      std::set<std::string> m_intopics;
      xdata::Float m_medianband_upper;//percentage of median value
      xdata::Float m_medianband_lower;//percentage of median value
      xdata::Float m_excludeband_upper;//absolute value (hz/ub) close to 0
      xdata::Float m_excludeband_lower;//absolute value (hz/ub) close to 0

      // caches
      xdata::UnsignedInteger32 m_fillnum;
      xdata::UnsignedInteger32 m_runnum;
      xdata::UnsignedInteger32 m_lsnum;
      xdata::UnsignedInteger32 m_nbnum;
      xdata::UnsignedInteger32 m_tssec;
      xdata::UnsignedInteger32 m_tsmsec;      
      xdata::UnsignedInteger m_instanceid;
      xdata::Float m_lumistorethreshold;
      
      LumiTopicPriorityQueue m_pq;
      toolbox::mem::Pool* m_memPool;
      toolbox::BSem m_applock;
      toolbox::BSem m_plock;
      toolbox::task::ActionSignature* m_as_publishing;
      toolbox::task::WorkLoop* m_publishing;
      unsigned int m_lasttcdscount;
      unsigned int m_tcdscount;
      // hyperdaq monitoring
      xdata::InfoSpace *m_monInfoSpace;
      std::list<std::string> m_monItemList;
      std::map<std::string, xdata::Serializable*> m_mon_configtable;
      std::map<std::string, xdata::Serializable*> m_mon_runtable;
      std::map<std::string, xdata::Serializable*> m_mon_besttable;
      std::map<std::string, xdata::Serializable*> m_mon_tcdstable;
      xdata::Table m_mon_lumitable;

      xdata::Float m_deadtimefrac;
      xdata::Boolean m_cmson;
      xdata::UnsignedInteger32 m_norb;
      xdata::UnsignedInteger32 m_nbperls;
      xdata::UnsignedInteger32 m_ncollidingbx;
      xdata::Float m_delivered;
      xdata::Float m_recorded;
      xdata::String m_lumiprovider;
      xdata::Float m_avgpu;
      xdata::String m_beamstatus;
      bool m_storable;
      // bestlumi flash
      xdata::InfoSpace* m_bestlumiIS;
      std::list< std::string > m_bestlumiFlashFields;
    private: 

      bool do_publish_bestlumi( const LumiTopicItem& lumi );
      void do_toppriority();
      void defineLumiMonTable();
      void initializeLumiMonTable();
      bool isDataStorable(const std::string& beamstatus) const;
      void init_bestlumiFlash();

      // nocopy protection
      BestLumiProcessor2 (const BestLumiProcessor2&);
      BestLumiProcessor2 & operator=(const BestLumiProcessor2&);
  };
  }}
#endif
