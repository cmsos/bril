// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_supervisor_version_h_
#define _bril_supervisor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILSUPERVISOR_VERSION_MAJOR 4
#define BRIL_BRILSUPERVISOR_VERSION_MINOR 1
#define BRIL_BRILSUPERVISOR_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILSUPERVISOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILSUPERVISOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILSUPERVISOR_VERSION_MAJOR,BRIL_BRILSUPERVISOR_VERSION_MINOR,BRIL_BRILSUPERVISOR_VERSION_PATCH)
#ifndef BRIL_BRILSUPERVISOR_PREVIOUS_VERSIONS
#define BRIL_BRILSUPERVISOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILSUPERVISOR_VERSION_MAJOR,BRIL_BRILSUPERVISOR_VERSION_MINOR,BRIL_BRILSUPERVISOR_VERSION_PATCH)
#else
#define BRIL_BRILSUPERVISOR_FULL_VERSION_LIST BRIL_BRILSUPERVISOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILSUPERVISOR_VERSION_MAJOR,BRIL_BRILSUPERVISOR_VERSION_MINOR,BRIL_BRILSUPERVISOR_VERSION_PATCH)
#endif

namespace brilsupervisor
{
        const std::string project = "bril";
	const std::string package = "brilsupervisor";
	const std::string versions = BRIL_BRILSUPERVISOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ supervisor";
	const std::string description = "bril supervisor";
	const std::string authors = "Zhen Xie";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
