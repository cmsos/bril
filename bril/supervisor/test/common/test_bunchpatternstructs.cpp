#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <string.h>
#include <cassert>
#include <cstdio>
#include <inttypes.h>

struct Provider{
  std::string name;
  short id;
  short priority;//larger numbers have higher priority  
  friend std::ostream&  operator<<(std::ostream& o, Provider p){
    o<<" Provider:"<<p.name<<","<<p.id<<" Priority:"<<p.priority;
    return o;
  }
  bool operator==(const Provider& y) const{
    return (name==y.name && id==y.id && priority==y.priority);
  }
  bool operator>(const Provider& y)const{
    return  priority>y.priority;
  }
};    

struct Priority_IsLesser{
      bool operator()(const Provider& x, const Provider& y) const{
	return x.priority<y.priority;
      }
    };
      
    struct Priority_IsGreater{
      bool operator()(const Provider& x, const Provider& y) const{
	return x.priority>y.priority;
      }
    };

    struct BunchPattern{
    BunchPattern():fillnum(0),runnum(0),lsnum(0),nbnum(0),tssec(0),tsmsec(0){
	std::fill_n( bxpattern, 7128, false );	
      }
      std::vector<Provider> providerpriorities;
      bool bxpattern[7128];
      int fillnum;
      int runnum;
      int lsnum;
      int nbnum;
      int tssec;
      int tsmsec;
     
      unsigned short ncollidingbx(){
	unsigned short result=0;
	for(int i=0; i<7128; ++i){
	  int j = i+3564;
	  if( bxpattern[i] && bxpattern[j]){
	    ++result;
	  }
	}
	return result;
      }

      bool operator==(const BunchPattern& a) const{	
	return !memcmp(bxpattern, a.bxpattern, sizeof(bxpattern) );
      }

      friend std::ostream& operator<<(std::ostream& o, BunchPattern a){
	o<<a.fillnum<<","<<a.runnum<<","<<a.lsnum<<","<<a.nbnum<<","<<a.tssec; 
	o<<"\n";
	for(std::vector<Provider>::iterator it=a.providerpriorities.begin(); it!=a.providerpriorities.end();++it){
	  o<<*it;
	}
	o<<std::endl;
	return o;      
      }
      
    };   
    
    struct BunchPattern_HasNoBestProvider{
      bool operator()(const BunchPattern& x, const BunchPattern& y) const{	
	return (x.providerpriorities == y.providerpriorities);	    
      }
    };
    
    struct BunchPattern_IsGreater{
      bool operator()(const BunchPattern& x, const BunchPattern& y) const{
	if( x.providerpriorities.size()>y.providerpriorities.size() ){
	  return true; //compare number of providers
	}
	std::vector<Provider>::const_iterator xIt = std::max_element( x.providerpriorities.begin(), x.providerpriorities.end(), Priority_IsLesser() );
	std::vector<Provider>::const_iterator yIt = std::max_element( y.providerpriorities.begin(), y.providerpriorities.end(), Priority_IsLesser() );	
	if( yIt==y.providerpriorities.end() ) return true;
	if( xIt!=x.providerpriorities.end() ){
	  return *xIt>*yIt;	  
	}
	return false;
      }
    };
   
int main(){
  Provider r;
  r.name="bptx_rhu";
  r.id = 0;
  r.priority = 4;

  Provider s;
  s.name = "bptx_scope";
  s.id = 1;
  s.priority = 3;

  Provider f;
  f.name = "fbct";
  f.id = 2;
  f.priority = 2;

  Provider u;
  u.name = "bptx_utca";
  u.id = 3;
  u.priority = 1;

  Provider l;
  l.name = "lhcconfig";
  l.id = 4;
  l.priority = 0;

  BunchPattern pt0;
  for(int i=0; i<3564*2; ++i){
    pt0.bxpattern[i] = true;    
  }
  pt0.providerpriorities.push_back(r);
  pt0.providerpriorities.push_back(s);

  BunchPattern pt1;
  for(int i=0; i<3564*2; ++i){
    pt1.bxpattern[i] = true;    
  }
  pt1.providerpriorities.push_back(r);
  pt1.providerpriorities.push_back(s);
  

  BunchPattern pt2;
  for(int i=0; i<3564*2; ++i){
    pt2.bxpattern[i] = false;
  }
  pt2.providerpriorities.push_back(f);
  pt2.providerpriorities.push_back(u);
  pt2.providerpriorities.push_back(l);  

  assert( (pt0==pt1) );
  std::cout<<"pt0 pt1 bx pattern is equal "<<std::endl;  
  std::cout<<"pt1 ncolliding bunches "<<pt1.ncollidingbx()<<std::endl;
  assert( !(pt1==pt2) );
  std::cout<<"pt1 pt2 bx pattern is not equal "<<std::endl;
  std::cout<<"pt2 ncolliding bunches "<<pt2.ncollidingbx()<<std::endl;

  // priority tests

  BunchPattern pta;
  pta.providerpriorities.push_back(r);
  pta.providerpriorities.push_back(s);

  BunchPattern ptb;
  ptb.providerpriorities.push_back(r);
  ptb.providerpriorities.push_back(s);
  ptb.providerpriorities.push_back(f);

  BunchPattern ptc;
  ptb.providerpriorities.push_back(u);//
  ptb.providerpriorities.push_back(s);

  assert( BunchPattern_IsGreater()(ptb,ptc) );
  std::cout<<"ptb priority is greater than pta due to more number of providers"<<std::endl;
  assert( BunchPattern_IsGreater()(pta,ptc) );
  std::cout<<"pta priority is greater than ptc due to on equal number of providers higher priority provider found"<<std::endl;

  uint32_t leastS = 0;
  uint32_t mostS = 1;
  uint64_t i=(uint64_t) mostS<<32 | leastS;
  //printf("%llu\n", i);
  std::cout<<i<<std::endl;
  
}
