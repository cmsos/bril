#ifndef _bril_dip_DipAnalyzer_h_
#define _bril_dip_DipAnalyzer_h_
#include <string>
#include <map>
#include "eventing/api/Member.h"
//#include "xdaq/Object.h"
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "toolbox/ActionListener.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"

#include "bril/dipprocessor/exception/Exception.h"

namespace bril{
  namespace dip{
    class PubErrorHandler;
    class ServerErrorHandler;

    class DipAnalyzer : public eventing::api::Member, public xdata::ActionListener,public toolbox::ActionListener, public DipSubscriptionListener{
    public:
      DipAnalyzer(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner);
      // destructor
      virtual ~DipAnalyzer();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e)=0;
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e)=0;
      // dip message handler
      virtual void handleMessage(DipSubscription* dipsub, DipData& message)=0;

      // DipSubscriptionListener methods
      void connected(DipSubscription* dipsub);
      virtual void disconnected(DipSubscription* subscription,char *reason);
      void handleException(DipSubscription* subscription, DipException& ex);
      void subscribeToDip();
      void createDipPublication();
      
    protected:      
      std::string m_name;
      unsigned int m_dipfrequency;
      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;
      //registries
      std::map< std::string,DipSubscription* > m_dipsubs;
      std::map< std::string,DipPublication* > m_dippubs; 
    private:    
      // nodefault constructor
      DipAnalyzer();
      // nocopy protection
      DipAnalyzer(const DipAnalyzer&);
      DipAnalyzer& operator=(const DipAnalyzer&);
    };
  }
}
#endif
