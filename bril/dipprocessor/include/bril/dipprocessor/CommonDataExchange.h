#ifndef _bril_dip_CommonDataExchange_h_
#define _bril_dip_CommonDataExchange_h_
#include <string>
#include <map>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger32.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include <fstream>

namespace toolbox{
namespace task{
class WorkLoop;
class ActionSignature;
}
}

namespace bril{
namespace dip{

class PubErrorHandler;
class ServerErrorHandler;

class CommonDataExchange : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener, public DipSubscriptionListener, public toolbox::task::TimerListener {
public:
    XDAQ_INSTANTIATOR();
    // constructor
    CommonDataExchange(xdaq::ApplicationStub* s);
    // destructor
    ~CommonDataExchange ();
    // xgi(web) callback
    void Default(xgi::Input * in, xgi::Output * out);
    // infospace event callback
    virtual void actionPerformed(xdata::Event& e);
    // toolbox event callback
    virtual void actionPerformed(toolbox::Event& e);
    // b2in message callback
    void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
    // timer callback
    void timeExpired(toolbox::task::TimerEvent& e);

    // dip message handler
    void handleMessage(DipSubscription* dipsub, DipData& message);

    // DipSubscriptionListener methods
    void connected(DipSubscription* dipsub);
    void disconnected(DipSubscription* subscription,char *reason);
    void handleException(DipSubscription* subscription, DipException& ex);
    void subscribeToDip();
    void createDipPublication();

protected:
    xdata::UnsignedInteger32 m_fillnum;
    xdata::UnsignedInteger32 m_runnum;
    xdata::UnsignedInteger32 m_lsnum;
    xdata::UnsignedInteger32 m_nbnum;
    xdata::UnsignedInteger32 m_tssec;
    xdata::UnsignedInteger32 m_tsmsec;

    DipFactory* m_dip;
    PubErrorHandler* m_puberrorhandler;
    ServerErrorHandler* m_servererrorhandler;

    //registries
    std::map< std::string,DipSubscription* > m_dipsubs;

    // configuration parameters
    xdata::String m_signalTopic;
    xdata::Boolean m_withcsv;
    xdata::String m_bus;
    xdata::String m_inTopicStr;
    xdata::String m_outTopicStr;
    xdata::String m_dipSubStr;
    xdata::String m_dipPubStr;
    xdata::String m_dipPubRoot;
    xdata::String m_flashlistStr;
    std::set<std::string> m_flashlists;
    std::vector<std::string> m_fastflashLists;
    std::vector<std::string> m_slowflashLists;
    // evt registry
    toolbox::mem::Pool* m_memPool;
    xdata::InfoSpace *m_monInfoSpace;

    std::list<std::string> m_time_ItemList;
    std::list<std::string> m_vdmscan_ItemList;
    std::list<std::string> m_beam_ItemList;
    std::map< std::string,std::string > m_topicRegistry;

    //flashlist params
    std::map< std::string,xdata::InfoSpace* > m_flashinfostore; // flashlistname to infospace
    std::map< xdata::InfoSpace*, std::list<std::string> > m_flashfields;//p_infospace, field names
    std::map< xdata::InfoSpace*, std::vector< xdata::Serializable*> > m_flashcontent; //p_infospace, field values

    toolbox::BSem m_applock;
    bool m_busready;
    bool m_clockready;
private:
    void writeVDMToCSV(std::ostream& ss);
    void writeVDMHeader(std::ostream& ss);
    void writeCommonDataToCSV(std::ostream& ss);
    void writeCommonDataHeader(std::ostream& ss);
    void initMonParams();
    void publishToEventing( const std::string& topicname );
    void publishToDip( const std::string& diptopic );
    void do_dippublish_automaticscan( const std::string& diptopic );
    void do_dippublish_scopedata( const std::string& diptopic );
    void do_dippublish_vdmoutput( toolbox::mem::Reference* ref , xdata::Properties & plist, const std::string& diptopic);
    void do_publish_beam();
    void do_publish_atlasbeam();
    void do_publish_bunchlength();
    void do_publish_vdmflag();
    void do_publish_vdmscan();
    void do_publish_bxconfig();
    void do_publish_bsrlfracBunched();
    void do_publish_bsrlqBunched();
    void do_publish_luminousregion();
    void do_publish_remus();
    void process_scopedata( toolbox::mem::Reference* ref, xdata::Properties & plist);
    void init_flashlist(xdata::InfoSpace* is, const std::string& flashname);
    void init_brilsummaryFlash(xdata::InfoSpace* is);
    void init_beamFlash(xdata::InfoSpace* is);
    void init_vachistoFlash(xdata::InfoSpace* is);
    void init_blmhistoFlash(xdata::InfoSpace* is);
    void init_pltcurrentsFlash(xdata::InfoSpace* is);

    void fireFlashlist(const std::string& flashname);
    void declareFlashLists();
    void cleanFlashLists();
    void updateAllFlashTime();
    void build_pltcurrentsFlashList();
    void build_beamFlashList();
    void build_blmhistoFlashList();    
    bool firefastflashlists(toolbox::task::WorkLoop* wl);
    bool fireslowflashlists(toolbox::task::WorkLoop* wl);
private:
    // nocopy protection
    CommonDataExchange (const CommonDataExchange&);
    CommonDataExchange & operator=(const CommonDataExchange&);
private:

    //dip data cache (monitorable)
    std::map<std::string,xdata::Serializable*> m_mon_runstatusTable;
    std::map<std::string,xdata::Serializable*> m_mon_dipsubs;
    std::map<std::string,xdata::Serializable*> m_mon_vdmStatus;
    std::map<std::string,xdata::Serializable*> m_mon_vdmParams;
    std::map<std::string,xdata::Serializable*> m_mon_beaminfoTable;

    bool m_scan_flag; //used to control csv writing
    //vdm data
    xdata::Boolean m_vdm_acqflag;
    xdata::Integer m_vdm_beam;
    xdata::Integer m_vdm_ip;
    xdata::String m_vdm_scanstatus;
    xdata::String m_vdm_plane;
    xdata::String m_vdm_nominal_separation_plane;
    xdata::Integer m_vdm_step;
    xdata::Integer m_vdm_progress;
    xdata::Float m_vdm_nominal_separation;
    xdata::Float m_vdm_read_nominal_B1sepPlane;
    xdata::Float m_vdm_read_nominal_B1xingPlane;
    xdata::Float m_vdm_read_nominal_B2sepPlane;
    xdata::Float m_vdm_read_nominal_B2xingPlane;
    xdata::Float m_vdm_set_nominal_B1sepPlane;
    xdata::Float m_vdm_set_nominal_B1xingPlane;
    xdata::Float m_vdm_set_nominal_B2sepPlane;
    xdata::Float m_vdm_set_nominal_B2xingPlane;

    xdata::String m_vdm_bpm_5LDOROS_B1Names;
    xdata::Float m_vdm_bpm_5LDOROS_B1hPos;
    xdata::Float m_vdm_bpm_5LDOROS_B1vPos;
    xdata::Float m_vdm_bpm_5LDOROS_B1hErr;
    xdata::Float m_vdm_bpm_5LDOROS_B1vErr;

    xdata::String m_vdm_bpm_5RDOROS_B1Names;
    xdata::Float m_vdm_bpm_5RDOROS_B1hPos;
    xdata::Float m_vdm_bpm_5RDOROS_B1vPos;
    xdata::Float m_vdm_bpm_5RDOROS_B1hErr;
    xdata::Float m_vdm_bpm_5RDOROS_B1vErr;

    xdata::String m_vdm_bpm_5LDOROS_B2Names;
    xdata::Float m_vdm_bpm_5LDOROS_B2hPos;
    xdata::Float m_vdm_bpm_5LDOROS_B2vPos;
    xdata::Float m_vdm_bpm_5LDOROS_B2hErr;
    xdata::Float m_vdm_bpm_5LDOROS_B2vErr;

    xdata::String m_vdm_bpm_5RDOROS_B2Names;
    xdata::Float m_vdm_bpm_5RDOROS_B2hPos;
    xdata::Float m_vdm_bpm_5RDOROS_B2vPos;
    xdata::Float m_vdm_bpm_5RDOROS_B2hErr;
    xdata::Float m_vdm_bpm_5RDOROS_B2vErr;

    xdata::Float m_vdm_atlas_totInst;

    xdata::Integer m_bstar5;
    xdata::Integer m_ip5xingHmurad;

    //common dip data

    //beam status
    xdata::String m_beamstatus;
    xdata::String m_machinemode;
    xdata::String m_fillscheme;
    xdata::UnsignedShort m_targetegev;
    xdata::String m_amodetag;
    xdata::Float m_egev;
    xdata::Vector<xdata::Integer> m_bxconfig1;
    xdata::Vector<xdata::Integer> m_bxconfig2;

    //beam intensities
    xdata::Integer m_nfilledBunches_1;
    xdata::Integer m_nfilledBunches_2;
    xdata::Float m_wholeBeamIntensity_1;
    xdata::Float m_wholeBeamIntensity_2;
    xdata::Vector<xdata::Float> m_avgBunchIntensities_1;
    xdata::Vector<xdata::Float> m_avgBunchIntensities_2;

    //atlas beam intensities
    xdata::Float m_atlasBeamIntensity_1;
    xdata::Float m_atlasBeamIntensity_2;
    xdata::Vector<xdata::Float> m_atlasBunchIntensities_1;
    xdata::Vector<xdata::Float> m_atlasBunchIntensities_2;

    // Luminous region
    xdata::Vector<xdata::Float> m_luminous_region_tilt;
    xdata::Vector<xdata::Float> m_luminous_region_centroid;
    xdata::Vector<xdata::Float> m_luminous_region_size;

    //bunch lengths
    xdata::Float m_bunchlengthmean_1;
    xdata::Float m_bunchlengthmean_2;
    xdata::Vector<xdata::Float> m_bunchlength_1;
    xdata::Vector<xdata::Float> m_bunchlength_2;

    //bptx
    xdata::Integer m_bptx1_number_of_bunches;
    xdata::Integer m_bptx2_number_of_bunches;
    xdata::Vector<xdata::Integer> m_bptx1_pattern;
    xdata::Vector<xdata::Integer> m_bptx2_pattern;

    //scopedata
    float m_tot_int_b1;
    float m_tot_int_b2;
    unsigned short m_int_b1[3564];
    unsigned short m_int_b2[3564];
    unsigned short m_avg_len_b1;
    unsigned short m_avg_len_b2;
    short m_avg_phase_b1;
    short m_avg_phase_b2;
    unsigned short m_len_b1[3564];
    unsigned short m_len_b2[3564];
    short m_phase_b1[3564];
    short m_phase_b2[3564];
    unsigned int m_scopeacqsec;
    bool m_scopepublishtodip;
    //brilsummary

    xdata::Vector<xdata::Float> m_blmhisto;

    //bsrl
    //bool m_previous_vdmflag;
    bool m_previous_scanflag;
    bool m_bsrlfracBunched_canpublish;
    bool m_bsrlqBunched_canpublish;
    unsigned int m_bsrl_acqStamp;
    float m_bsrl_post_fraction_ghost_bunched[2];
    float m_bsrl_post_fraction_sat_bunched[2];
    float m_bsrl_post_q_bunched[2][3][11880];

    //remus
    float m_remus;

    //pltcurrents
    xdata::Float m_plt_HmF;
    xdata::Float m_plt_HmN;
    xdata::Float m_plt_HpF;
    xdata::Float m_plt_HpN;
    //workloops
    toolbox::task::WorkLoop* m_wl_firefastflash;
    toolbox::task::WorkLoop* m_wl_fireslowflash;
    toolbox::task::ActionSignature* m_as_firefastflash;
    toolbox::task::ActionSignature* m_as_fireslowflash;

    //outdir
    xdata::String m_outdir;
    //eventing input topics
    std::set<std::string> m_intopics;
    //eventing output topics
    std::set<std::string> m_outtopics;
    //dip output topics
    std::map< std::string,DipPublication* > m_dippubs;
    //ostreams
    std::ofstream m_vdmfile;
    //std::ofstream m_commondatafile;
    std::string m_processuuid;
    std::string m_bsrlfracBunched_timerurn;
    std::string m_bsrlqBunched_timerurn;
};
}
}
#endif
