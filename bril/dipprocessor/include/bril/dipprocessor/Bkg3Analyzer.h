#ifndef _bril_dipprocessor_Bkg3Analyzer_h_
#define _bril_dipprocessor_Bkg3Analyzer_h_

#include <map>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "bril/dipprocessor/DipAnalyzer.h"


namespace bril{
  namespace dip{

    class Bkg3Analyzer: public DipAnalyzer{
    public:
      Bkg3Analyzer(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~Bkg3Analyzer();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);      
    private:
      unsigned int m_fill;
      unsigned int m_run;
      unsigned int m_ls;
      unsigned int m_nb;
      
      xdata::Float m_bkgd1;
      xdata::Float m_bkgd2;
      xdata::Float m_bkgd3;
      
      xdata::Boolean m_non_colliding;
      
      std::string m_detector;
      std::string m_algo;
      
      std::string m_dipRoot;
      
      xdata::Float m_vme_old1;
      xdata::Float m_vme_old2;
      xdata::Float m_vme_nc1;
      xdata::Float m_vme_nc2;
      xdata::Float m_vme_lead1;
      xdata::Float m_vme_lead2;
      
      xdata::Float m_utca_nc1;
      xdata::Float m_utca_nc2;
      xdata::Float m_utca_lead1;
      xdata::Float m_utca_lead2;
      
      xdata::Float m_plt_nc1;
      xdata::Float m_plt_nc2;
      xdata::Float m_plt_lead1;
      xdata::Float m_plt_lead2;
      
      xdata::Float m_bcml;
            
    private:    
      void publish_to_dip();
      // nodefaut constructor
      Bkg3Analyzer();
      // nocopy protection
      Bkg3Analyzer(const Bkg3Analyzer&);
      Bkg3Analyzer& operator=(const Bkg3Analyzer&);
    };
  }
}
#endif
