#include "bril/dipprocessor/DipAnalyzer.h"
#include "bril/dipprocessor/Bkg3Analyzer.h"
#include "bril/dipprocessor/BkgdPublisher.h"
#include "bril/dipprocessor/VacSummary.h"
#include "bril/dipprocessor/BLMSummary.h"
#include "bril/dipprocessor/Magnet.h"
#include "bril/dipprocessor/BRILAlarms.h"
#include "bril/dipprocessor/FillLumi.h"
namespace bril{
  namespace dip{
    // a simplest possible factory
    class DipAnalyzerFactory{
    public:
      static DipAnalyzer* create(const std::string& classname, 
				 unsigned int dipfrequency, const std::string& dipServiceName, 
				 xdaq::Application* owner);
    private:
      DipAnalyzerFactory();
      DipAnalyzerFactory(const DipAnalyzerFactory&);
      DipAnalyzerFactory& operator=(const DipAnalyzerFactory&);
    };

    DipAnalyzer* DipAnalyzerFactory::create(const std::string& classname,
					    unsigned int dipfrequency, const std::string& dipServiceName,
					    xdaq::Application* owner){
     if(classname == "Magnet"){
  return new Magnet(classname,dipfrequency,dipServiceName,owner);
      }else if(classname == "Bkg3Analyzer"){
  return new Bkg3Analyzer(classname,dipfrequency,dipServiceName,owner);
      } else if(classname == "VacSummary"){
  return new VacSummary(classname,dipfrequency,dipServiceName,owner);
      } else if(classname == "BLMSummary"){
  return new BLMSummary(classname,dipfrequency,dipServiceName,owner);
  //}else if(classname == "BRILAlarms"){return new BRILAlarms(classname,dipfrequency,dipServiceName,owner);
      }else if(classname == "FillLumi"){
  return new FillLumi(classname,dipfrequency,dipServiceName,owner);
      }else if(classname == "BkgdPublisher"){
  return new BkgdPublisher(classname,dipfrequency,dipServiceName,owner);
      }else{
	       return NULL;
      }
      return NULL;
    }
  }
}
