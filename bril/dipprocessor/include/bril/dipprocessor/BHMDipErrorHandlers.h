#ifndef _bril_dip_DipErrorHandlers_h_
#define _bril_dip_DipErrorHandlers_h_
#include "dip/Dip.h"
#include "dip/DipDimErrorHandler.h"
#include "dip/DipPublicationErrorHandler.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "xcept/Exception.h"
class DipPublication;
namespace bril{
  namespace bhmdipprocessor{
    class PubErrorHandler:public DipPublicationErrorHandler{
    public:
      void handleException(DipPublication* publication, DipException& ex){
	std::cout<<"dip pub error "<<ex.what()<<std::endl;
      }
    };

    class ServerErrorHandler:public DipDimErrorHandler{
    public:
      virtual void handleException(int severity, int code, char *msg){
	std::cout<<"dns server error severity "<<severity<<" code "<<code<<" msg "<<msg<<std::endl;	
      }
    };
  }
}


#endif
