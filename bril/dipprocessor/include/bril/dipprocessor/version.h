// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_dipprocessor_version_h_
#define _bril_dipprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILDIPPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILDIPPROCESSOR_VERSION_MINOR 1
#define BRIL_BRILDIPPROCESSOR_VERSION_PATCH 2

// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILDIPPROCESSOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILDIPPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILDIPPROCESSOR_VERSION_MAJOR,BRIL_BRILDIPPROCESSOR_VERSION_MINOR,BRIL_BRILDIPPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILDIPPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILDIPPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILDIPPROCESSOR_VERSION_MAJOR,BRIL_BRILDIPPROCESSOR_VERSION_MINOR,BRIL_BRILDIPPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILDIPPROCESSOR_FULL_VERSION_LIST BRIL_BRILDIPPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILDIPPROCESSOR_VERSION_MAJOR,BRIL_BRILDIPPROCESSOR_VERSION_MINOR,BRIL_BRILDIPPROCESSOR_VERSION_PATCH)
#endif

namespace brildipprocessor
{
        const std::string project = "bril";
	const std::string package = "brildipprocessor";
	const std::string versions = BRIL_BRILDIPPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ dipprocessor";
	const std::string description = "dip analyzer processor";
	const std::string authors = "Zhen Xie, David Stickland";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
