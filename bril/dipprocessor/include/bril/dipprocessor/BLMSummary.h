#ifndef _bril_dipprocessor_BLMSummary_h_
#define _bril_dipprocessor_BLMSummary_h_

#include <map>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "bril/dipprocessor/DipAnalyzer.h"
#define NBIN 350

namespace bril{
  namespace dip{

    class BLMSummary: public DipAnalyzer{
    public:
      BLMSummary(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~BLMSummary();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);      
    private:
      //Variables associated with the BLMSummary task
      std::string m_dipRoot;
      std::map< std::string,double > m_blmValues;
      std::map< std::string,float > m_blmPositions;
      std::map< std::string,int > m_blmEntries;
      float m_histo[NBIN];


      
    
  
    private:    
      void publish_to_dip();
      // nodefaut constructor
      BLMSummary();
      // nocopy protection
      BLMSummary(const BLMSummary&);
      BLMSummary& operator=(const BLMSummary&);
    };
  }
}
#endif
