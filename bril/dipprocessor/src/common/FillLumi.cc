#include "bril/dipprocessor/FillLumi.h"
#include <fstream>
#include <cstdlib>
bril::dip::FillLumi::FillLumi(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
    std::stringstream msg;
    std::string contextURL = getOwnerApplication()->getApplicationContext()->getContextDescriptor()->getURL();
    msg << "Context URL: " << contextURL;
    LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), msg.str());
    msg.str("");
    msg.clear();
    m_fillLumiRecoveryFilePath = std::string("/tmp/fill_lumi_") + contextURL.substr(7);
    msg<<"fill Lumi recovery file: "<<m_fillLumiRecoveryFilePath;
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), msg.str());
    msg.str("");
    msg.clear();

    try{
        //if need to read configuraiton, listen also to defaultparameters
        getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
        getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    }catch( xcept::Exception & e){
        LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
    }

    std::cout<<"FILLumiConstructr"<<std::endl;
    m_dipRoot="dip/CMS/";
    m_fillNumber="NOT_INITIALIZED";
    m_cmsRunNumber=0;
    m_deliveredLumi=0;
    m_recordedLumi=0;
    m_deadfrac=1;
    m_beamMode="NOT_INITIALIZED";
    m_machineMode="NOT_INITIALIZED";
}

bril::dip::FillLumi::~FillLumi(){
}

void bril::dip::FillLumi::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        xdata::String* dipRoot =
            dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
        if(dipRoot)m_dipRoot= dipRoot->value_;
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/RunControl/BeamMode",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/RunControl/RunConfiguration",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/RunControl/MachineMode",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/CMS/LHC/Luminosity",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/CMS/BRIL/Luminosity",(DipSubscription*)0 ));
        m_dippubs.insert(std::make_pair(m_dipRoot+"LHC/FillLumi",(DipPublication*)0 ));
    }
    if( e.type()== "ItemChangedEvent" ){
        std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
        if(item =="nbnum" ){
            std::stringstream ss;
            ss<<"FillLumi publish to dip"<<std::endl;
            xdata::Serializable* currentfillnum = getOwnerApplication()->getApplicationInfoSpace()->find("fillnum");
            std::string fn = (dynamic_cast<xdata::UnsignedInteger*>(currentfillnum))->toString();
            if (m_fillNumber == "NOT_INITIALIZED") {
                recover_fill_lumi(fn);
            }

            if(fn != m_fillNumber){
                m_fillNumber=fn;
                m_deliveredLumi=0;
                m_recordedLumi=0;
                LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"Setting Fill Number "+m_fillNumber);
            }

            xdata::Serializable* currentrunnum = getOwnerApplication()->getApplicationInfoSpace()->find("runnum");
            xdata::Serializable* currentlsnum = getOwnerApplication()->getApplicationInfoSpace()->find("lsnum");
            xdata::Serializable* currentnbnum = getOwnerApplication()->getApplicationInfoSpace()->find("nbnum");
            if(currentrunnum && currentlsnum && currentnbnum){
                ss<<" fill "<<dynamic_cast<xdata::UnsignedInteger*>(currentfillnum)->value_ ;
                ss<<" run "<<dynamic_cast<xdata::UnsignedInteger*>(currentrunnum)->value_ ;
                ss<<" ls "<<dynamic_cast<xdata::UnsignedInteger*>(currentlsnum)->value_ ;
                ss<<" nb "<<dynamic_cast<xdata::UnsignedInteger*>(currentnbnum)->value_ ;
            }
            LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
            std::cout<<"fake publish_to_dip"<<std::endl;
            publish_to_dip();
        }
    }

}

void bril::dip::FillLumi::actionPerformed(toolbox::Event& e){
}

void bril::dip::FillLumi::connected(DipSubscription* dipsub){
    std::string subname(dipsub->getTopicName());
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"FillLumi::Connected to "+subname);
}
void bril::dip::FillLumi::disconnected(DipSubscription* dipsub, char *reason){
    std::string subname(dipsub->getTopicName());
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"FillLumi::disConnected from "+subname);
}

void bril::dip::FillLumi::handleMessage(DipSubscription* dipsub, DipData& message){
    std::string subname(dipsub->getTopicName());
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"FillLumi::handleMessage from "+subname);
    if(message.size()==0) return;

    //if (subname=="dip/acc/LHC/RunControl/RunConfiguration"){
    //std::string fn=message.extractString("FILL_NO");
    //if (m_fillNumber == "NOT_INITIALIZED") {
    //  recover_fill_lumi(fn);
    //}
    //if(fn != m_fillNumber){
    //  m_fillNumber=fn;
    //  m_deliveredLumi=0;
    //  m_recordedLumi=0;
    //  std::cout<<"Setting Fill Number  "<<m_fillNumber<<std::endl;
    //}
    if (subname=="dip/acc/LHC/RunControl/BeamMode"){
        m_beamMode=message.extractString("value");
    } else if (subname=="dip/acc/LHC/RunControl/MachineMode"){
        m_machineMode=message.extractString("value");
    } else if (subname=="dip/CMS/LHC/Luminosity" && m_beamMode=="STABLE BEAMS"){
        float l=message.extractFloat("Lumi_TotInst");
        float itime=message.extractFloat("CollRateIntTime");
        m_deliveredLumi+=l*itime/1000.;
        m_recordedLumi+=(l*itime/1000.*(1.0-m_deadfrac));
    } else if (subname=="dip/CMS/BRIL/Luminosity"){
        m_deadfrac=message.extractFloat("TCDS_deadFrac");
    }
}

void bril::dip::FillLumi::publish_to_dip(){
    // you can use common data such as beamegev in calculation from infospace.
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"FillLumi::publish_to_dip");
    DipTimestamp t;
    DipData* dipdata = m_dip->createDipData();
    std::string diptopicname(m_dipRoot+"LHC/FillLumi");
    dipdata -> insert(m_deliveredLumi,"IntLumi_Delivered_StableBeams");
    dipdata -> insert(m_recordedLumi,"IntLumi_Recorded");
    dipdata -> insert(m_fillNumber,"FillNumber");
    m_dippubs[diptopicname]->send(*dipdata,t);
    delete dipdata;
    update_recovery_file();
}

bool bril::dip::FillLumi::recover_fill_lumi(std::string const & current_fill_number) {
    std::stringstream msg;
    std::ifstream recovery_file;
    std::string fill_from_file;
    std::string delivered_lumi_from_file_str;
    std::string recorded_lumi_from_file_str;
    std::string temp;
    msg<<"Reading fill lumi recovery file: "<<m_fillLumiRecoveryFilePath;
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), msg.str());
    msg.str("");
    msg.clear();
    try {
        recovery_file.open(m_fillLumiRecoveryFilePath.c_str(), std::ifstream::in | std::ifstream::binary);
        if (recovery_file.fail()) {
            msg << "Failed opening fill lumi recovery file: " << m_fillLumiRecoveryFilePath;
            LOG4CPLUS_WARN(getOwnerApplication()->getApplicationLogger(), msg.str());
            return false;
        }
        getline(recovery_file,temp,' ');
        if ( (temp.size() !=4) && (temp.size() !=5 ) ){
            XCEPT_RAISE(xcept::Exception, "Fill lumi recovery file fillnumber must be of size 4 or 5.");
        }
        fill_from_file = temp;
 	getline(recovery_file,delivered_lumi_from_file_str,' ');
        getline(recovery_file,recorded_lumi_from_file_str,' ');
        if (recovery_file.fail()) {
            XCEPT_RAISE(xcept::Exception, "Failed reading delivered and/or recorded lumi values.");
        }
        recovery_file.close();
    } catch(xcept::Exception & e) {
        msg << "Failed reading fill lumi recovery file: " << std::string(e.what());
        LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), msg.str());
        if (recovery_file.is_open()) {
            recovery_file.close();
        }
        return false;
    }
    if (current_fill_number != fill_from_file) {
        msg << "No need to recover, fill number different - current:" <<
            current_fill_number << ", in file:" << fill_from_file;
        LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), msg.str());
        return false;
    }
    m_deliveredLumi = std::stof(delivered_lumi_from_file_str);
    m_recordedLumi = std::stof(recorded_lumi_from_file_str);
    m_fillNumber = fill_from_file;
    msg << "Same fill. Recovering - fill:" << fill_from_file << ", delivered:" <<
        delivered_lumi_from_file_str << ", recorded:" << recorded_lumi_from_file_str;
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), msg.str());
    return true;
}

bool bril::dip::FillLumi::update_recovery_file() {
    std::stringstream msg;
    std::ofstream recovery_file;
    msg << "Updating fill lumi recovery file - fill:" << m_fillNumber << ", delivered:" <<
        m_deliveredLumi << ", recorded:" << m_recordedLumi;
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), msg.str());
    msg.str("");
    msg.clear();
    if (m_fillNumber == "NOT_INITIALIZED") {
        LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),
                       "Fill number not initialized - do not update fill lumi recovery file");
        return false;
    }
    if ( (m_fillNumber.size() != 4) && (m_fillNumber.size() !=5 ) ) {
        LOG4CPLUS_WARN(getOwnerApplication()->getApplicationLogger(),
                       "Fill number must be string of length 4 or 5 - do not update fill lumi recovery file");
        return false;
    }
    try {
        recovery_file.open(m_fillLumiRecoveryFilePath.c_str(), std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);
        if (!recovery_file.is_open()) {
            XCEPT_RAISE(xcept::Exception, std::string("Failed opening fill lumi recovery file: ") + m_fillLumiRecoveryFilePath);
        }
        recovery_file<<m_fillNumber<<" "<<m_deliveredLumi<<" "<<m_recordedLumi;
        if (recovery_file.fail()) {
            XCEPT_RAISE(xcept::Exception, "Failed writing fill lumi recovery file.");
        }
        recovery_file.close();
    } catch(xcept::Exception & e) {
        msg << "Failed updating fill lumi recovery file: " << std::string(e.what());
        LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), msg.str());
        if (recovery_file.is_open()) {
            recovery_file.close();
        }
        return false;
    }
    return true;
}
