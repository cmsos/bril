#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/net/UUID.h"
#include "b2in/nub/Method.h"
#include "dip/DipData.h"
#include "dip/Dip.h"

#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "bril/dipprocessor/BCMFAnalyzer.h"
#include "interface/bril/BCM1FTopics.hh"
#include <cmath>

XDAQ_INSTANTIATOR_IMPL (bril::dip::BCMFAnalyzer)
namespace bril{ 
  namespace dip{
    static int maxnbx=3564;

    BCMFAnalyzer::BCMFAnalyzer(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this){
      xgi::framework::deferredbind(this,this,&bril::dip::BCMFAnalyzer::Default, "Default");
      b2in::nub::bind(this, &bril::dip::BCMFAnalyzer::onMessage);
      m_bus.fromString("brildata");//default
      m_dipRoot.fromString("dip/CMS/");//default
      memset (m_integrals,0,sizeof(m_integrals));  
      memset (m_histosSection,0,sizeof(m_histosSection));
      memset (m_histosNibble,0,sizeof(m_histosNibble));   
      m_nbcount = 0;
      getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
      getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
      getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");      
    }

    BCMFAnalyzer::~BCMFAnalyzer(){}

    void BCMFAnalyzer::Default(xgi::Input * in, xgi::Output * out){}

    void BCMFAnalyzer::actionPerformed(xdata::Event& e){
      if( e.type()== "urn:xdaq-event:setDefaultValues" ){            
        try{
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::bcm1fagghistT::topicname());
        }catch(eventing::api::exception::Exception& e){
          std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
          LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
          XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);     
        } 
	toolbox::net::UUID uuid;
	std::string processuuid = uuid.toString();
	m_dip = Dip::create(( std::string("brilBCMFAnalyzer_")+processuuid).c_str() );
        m_servererrorhandler=new ServerErrorHandler;
        Dip::addDipDimErrorHandler(m_servererrorhandler);
        m_puberrorhandler = new PubErrorHandler;   
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/RHUAnalysis",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/RHUHistos",(DipPublication*)0 ));
        createDipPublication();    
      }
    }

    void BCMFAnalyzer::actionPerformed(toolbox::Event& e){        
    }
    
    void BCMFAnalyzer::createDipPublication(){
      for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
        if(!p){
          LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
          m_dippubs.erase(it); // remove from registry
        }else{
          it->second=p;
        }
      }
    }
        
    void BCMFAnalyzer::publishToDip(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"publishToDip");
      DipTimestamp t;
      DipData* dipdata = m_dip->createDipData();
      std::string diptopicname(m_dipRoot.value_+"BRIL/RHUAnalysis");
      dipdata -> insert((int64_t*)&m_integrals[0],8,"ChannelIntegralsRHU0");
      dipdata -> insert((int64_t*)&m_integrals[8],8,"ChannelIntegralsRHU1");
      dipdata -> insert((int64_t*)&m_integrals[16],8,"ChannelIntegralsRHU2");
      dipdata -> insert((int64_t*)&m_integrals[24],8,"ChannelIntegralsRHU3");
      dipdata -> insert((int64_t*)&m_integrals[32],8,"ChannelIntegralsRHU4");
      dipdata -> insert((int64_t*)&m_integrals[40],8,"ChannelIntegralsRHU5");
      //for (int i=48;i<56;i++){
      //  m_integrals[i]/=16384;
      //}
      // For the bunch counting just require more than 10% of the orbits to have 
      // a hit in this bx
      for (int i=48;i<56;i++){
        m_integrals[i]=0;
        for (int j=0;j<maxnbx;j++){
          if (m_histosNibble[i][j]> 1638){
            m_integrals[i]++;
          }
        }
      }

      dipdata -> insert((int64_t*)&m_integrals[48],8,"ChannelIntegralsRHU6");
      dipdata -> insert((int64_t*)&m_integrals[56],8,"ChannelIntegralsRHU7");
 
      m_dippubs[diptopicname]->send(*dipdata,t);
      delete dipdata;
    }

    void BCMFAnalyzer::publishHistosToDip(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"publishHistosToDip");
      int sum[5];
      DipInt  bptxHist[8][maxnbx];
      memset (bptxHist,0,sizeof(bptxHist));
      memset (sum,0,sizeof(sum));
      for (int i=48;i<56;i++){
        for (int j=0;j<maxnbx;j++){
          bptxHist[i-48][j]+=m_histosSection[i][j];
          if (i<51){
            sum[i-48]+=m_histosSection[i][j];
          }
        }
      }
      DipInt  lutHist[8][maxnbx];
      memset (lutHist,0,sizeof(lutHist));
      for (int i=56;i<64;i++){
        for (int j=0;j<maxnbx;j++){
          lutHist[i-56][j]+=m_histosSection[i][j];
        }
      }
      DipInt  zSumHist[2][maxnbx];
      memset (zSumHist,0,sizeof(zSumHist));
      int index;
      for (int i =0; i<48;i++){
        index=0;
        if(i>23){
          index=1;
        }
        for (int j=0;j<maxnbx;j++){
          zSumHist[index][j]+=m_histosSection[i][j];
          sum[index+3]+=m_histosSection[i][j];
        }
      }

      DipTimestamp t;
      DipData* dipdata = m_dip->createDipData();
      std::string diptopicname(m_dipRoot.value_+"BRIL/RHUHistos");
      dipdata -> insert((DipInt*)&bptxHist[0],maxnbx,"BPTXB1");
      dipdata -> insert((DipInt*)&bptxHist[1],maxnbx,"BPTXB2");
      dipdata -> insert((DipInt*)&bptxHist[3],maxnbx,"BPTXAND");
      dipdata -> insert((DipInt*)&zSumHist[0],maxnbx,"BCM1FPlus");
      dipdata -> insert((DipInt*)&zSumHist[1],maxnbx,"BCM1FMinus");
      m_dippubs[diptopicname]->send(*dipdata,t);
      delete dipdata;      
    }



    void BCMFAnalyzer::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
      std::stringstream ss;
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string data_version  = plist.getProperty("DATA_VERSION");    
        if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
          std::string msg("Received brildaq message has no or wrong version, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
        if(topic == interface::bril::bcm1fagghistT::topicname()){
          if(ref!=0){
            //parse timing signal message header to get timing info
            interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());                 
	    k_algoid = thead->getAlgoID();
            // I need "1"  which is enum rsolved to "SUM"
            // interface::bril::BCM1FAggAlgos::SUM
            //std::cout<<"FILL,RUN,LS,NB "<<fillnum<<", "<<runnum<<", "<<lsnum<<", "<<nbnum<<std::endl;

            if (k_algoid!=interface::bril::BCM1FAggAlgos::SUM){
              ref->release(); 
              ref=0;      
              return;
            }
            unsigned int channelID = thead->getChannelID();
            if( channelID<1 || channelID>64){
	      ss<<" ChannelID out of range "<<channelID;
	      LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	      ref->release(); 
	      ref=0; 
	      return;
	    }
	    ss.str(""); ss.clear();
	    ss<<"Received BCM1FAggAlgos::SUM "<<thead->fillnum<<","<<thead->runnum<<","<<thead->lsnum<<","<<thead->nbnum<<","<<thead->timestampsec<<","<<channelID;
	    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
	    ss.str(""); ss.clear();

            uint16_t histogram[maxnbx];
            memset (histogram,0,sizeof(histogram));
            memcpy (histogram,thead->payloadanchor,sizeof(histogram));

            // copy histograms to local arrays and calculate intergrals
            int sum=0;
            for (int i=0;i<maxnbx;i++){
              m_histosNibble[channelID-1][i]=histogram[i];
              m_histosSection[channelID-1][i]+=histogram[i];
              sum+=histogram[i];
            }
            m_integrals[channelID-1]=sum;             
           
            if (channelID==56){
	      LOG4CPLUS_INFO(getApplicationLogger(),"channel 56, about to publishToDip");
              publishToDip();
              m_nbcount+=1; 
	      ss<<"Done publishToDip, m_nbcount="<<m_nbcount;
	      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
              memset (m_histosNibble,0,sizeof(m_histosNibble));
              memset (m_integrals,0,sizeof(m_integrals));   
	      ss.str("");
            }
 
            if(m_nbcount==16){
	      LOG4CPLUS_INFO(getApplicationLogger(),"m_nbcount 16, about to publishHistosToDip");
              publishHistosToDip();
              memset (m_histosSection,0,sizeof(m_histosSection));
              m_nbcount=0;
	      ss<<"Done publishHistosToDip, reset m_nbcount";
	      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	      ss.str("");
            }             
          }    
        }
      }
      if(ref!=0){ 
        ref->release(); 
        ref=0; 
      }       
    }
  }
}
