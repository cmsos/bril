#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/AutoReference.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/TimeVal.h"
#include "toolbox/BSem.h"
#include "b2in/nub/Method.h"
#include "bril/dipprocessor/CommonDataExchange.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "bril/dipprocessor/WebUtils.h"
#include "dip/DipData.h"
#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/BPTXTopics.hh"
#include "interface/bril/REMUSTopics.hh"
#include "toolbox/net/UUID.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include <cstdlib>
#include <cstdio>

#ifdef __DEBUG__
//#include <unistd.h>
//#include <sys/syscall.h>
#include "bril/dipprocessor/VerboseGuard.h"
#else
#include "toolbox/task/Guard.h"
#endif

XDAQ_INSTANTIATOR_IMPL (bril::dip::CommonDataExchange)
namespace bril{
namespace dip{

static int maxnbx=3564;

CommonDataExchange::CommonDataExchange (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::dip::CommonDataExchange::Default, "Default");
    b2in::nub::bind(this, &bril::dip::CommonDataExchange::onMessage);
    toolbox::net::URN memurn("toolbox-mem-pool","brildipCommonDataExchange_mem");
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
    toolbox::net::UUID uuid;
    m_processuuid = uuid.toString();
    m_bsrlfracBunched_timerurn = "urn:CommonDataExchange_bsrlfracBunched_timer"+m_processuuid;
    m_bsrlqBunched_timerurn = "urn:CommonDataExchange_bsrlqBunched_timer"+m_processuuid;
    m_fillnum = 0;
    m_runnum = 0;
    m_lsnum = 0;
    m_nbnum = 0;
    m_tssec = 0;
    m_tsmsec = 0;
    m_signalTopic.fromString("NB4");//default
    m_targetegev = 6500;
    m_amodetag.fromString("PROTPHYS");//default
    m_bus.fromString("brildata");//default
    m_dipPubRoot.fromString("dip/CMS/");//default
    //m_outTopicsStr.fromString("beam");
    m_busready = false;
    m_clockready = false;
    m_outdir = "/tmp";
    initMonParams();

    this->getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
    getApplicationInfoSpace()->fireItemAvailable("withcsv",&m_withcsv);
    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("inTopics",&m_inTopicStr);
    getApplicationInfoSpace()->fireItemAvailable("outDir",&m_outdir);
    getApplicationInfoSpace()->fireItemAvailable("amodetag",&m_amodetag);
    getApplicationInfoSpace()->fireItemAvailable("targetegev",&m_targetegev);
    getApplicationInfoSpace()->fireItemAvailable("outTopics",&m_outTopicStr);
    getApplicationInfoSpace()->fireItemAvailable("dipSubs",&m_dipSubStr);
    getApplicationInfoSpace()->fireItemAvailable("dipPubRoot",&m_dipPubRoot);
    getApplicationInfoSpace()->fireItemAvailable("dipPubs",&m_dipPubStr);
    getApplicationInfoSpace()->fireItemAvailable("flashlists",&m_flashlistStr);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    try{
        // make all monitorables available to mon infospace
        std::string nid("brildipprocessor_mon");
        std::string monurn = createQualifiedInfoSpace(nid).toString();
        m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));
        m_monInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
        m_time_ItemList.push_back("fillnum");
        m_monInfoSpace->fireItemAvailable("runnum",&m_runnum);
        m_time_ItemList.push_back("runnum");
        m_monInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
        m_time_ItemList.push_back("lsnum");
        m_monInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
        m_time_ItemList.push_back("nbnum");
        m_monInfoSpace->fireItemAvailable("tssec",&m_tssec);
        m_time_ItemList.push_back("tssec");
        m_monInfoSpace->fireItemAvailable("tsmsec",&m_tsmsec);
        m_time_ItemList.push_back("tsmsec");
        m_monInfoSpace->fireItemAvailable("amodetag",&m_amodetag);
        m_time_ItemList.push_back("amodetag");
        m_monInfoSpace->fireItemAvailable("targetegev",&m_targetegev);
        m_time_ItemList.push_back("targetegev");

        m_monInfoSpace->fireItemAvailable("vdm_acqflag",&m_vdm_acqflag);
        //m_vdmflag_ItemList.push_back("vdm_acqflag");

        m_monInfoSpace->fireItemAvailable("vdm_beam",&m_vdm_beam);
        m_vdmscan_ItemList.push_back("vdm_beam");
        m_monInfoSpace->fireItemAvailable("vdm_ip",&m_vdm_ip);
        m_vdmscan_ItemList.push_back("vdm_ip");
        m_monInfoSpace->fireItemAvailable("vdm_scanstatus",&m_vdm_scanstatus);
        m_vdmscan_ItemList.push_back("vdm_scanstatus");
        m_monInfoSpace->fireItemAvailable("plane",&m_vdm_plane);
        m_vdmscan_ItemList.push_back("plane");
        m_monInfoSpace->fireItemAvailable("nominal_separation_plane",&m_vdm_nominal_separation_plane);
        m_vdmscan_ItemList.push_back("nominal_separation_plane");

        m_monInfoSpace->fireItemAvailable("vdm_step",&m_vdm_step);
        m_vdmscan_ItemList.push_back("vdm_step");
        m_monInfoSpace->fireItemAvailable("vdm_progress",&m_vdm_progress);
        m_vdmscan_ItemList.push_back("vdm_progress");
        m_monInfoSpace->fireItemAvailable("vdm_nominal_separation",&m_vdm_nominal_separation);
        m_vdmscan_ItemList.push_back("vdm_nominal_separation");
        m_monInfoSpace->fireItemAvailable("vdm_read_nominal_B1sepPlane",&m_vdm_read_nominal_B1sepPlane);
        m_vdmscan_ItemList.push_back("vdm_read_nominal_B1sepPlane");
        m_monInfoSpace->fireItemAvailable("vdm_read_nominal_B1xingPlane",&m_vdm_read_nominal_B1xingPlane);
        m_vdmscan_ItemList.push_back("vdm_read_nominal_B1xingPlane");
        m_monInfoSpace->fireItemAvailable("vdm_read_nominal_B2sepPlane",&m_vdm_read_nominal_B2sepPlane);
        m_vdmscan_ItemList.push_back("vdm_read_nominal_B2sepPlane");
        m_monInfoSpace->fireItemAvailable("vdm_read_nominal_B2xingPlane",&m_vdm_read_nominal_B2xingPlane);
        m_vdmscan_ItemList.push_back("vdm_read_nominal_B2xingPlane");
        m_monInfoSpace->fireItemAvailable("vdm_set_nominal_B1sepPlane",&m_vdm_set_nominal_B1sepPlane);
        m_vdmscan_ItemList.push_back("vdm_set_nominal_B1sepPlane");
        m_monInfoSpace->fireItemAvailable("vdm_set_nominal_B1xingPlane",&m_vdm_set_nominal_B1xingPlane);
        m_vdmscan_ItemList.push_back("vdm_set_nominal_B1xingPlane");
        m_monInfoSpace->fireItemAvailable("vdm_set_nominal_B2sepPlane",&m_vdm_set_nominal_B2sepPlane);
        m_vdmscan_ItemList.push_back("vdm_set_nominal_B2sepPlane");
        m_monInfoSpace->fireItemAvailable("vdm_set_nominal_B2xingPlane",&m_vdm_set_nominal_B2xingPlane);
        m_vdmscan_ItemList.push_back("vdm_set_nominal_B2xingPlane");

        m_monInfoSpace->fireItemAvailable("wholeBeamIntensity_1",&m_wholeBeamIntensity_1);
        m_beam_ItemList.push_back("wholeBeamIntensity_1");
        m_monInfoSpace->fireItemAvailable("wholeBeamIntensity_2",&m_wholeBeamIntensity_2);
        m_beam_ItemList.push_back("wholeBeamIntensity_2");

        m_monInfoSpace->fireItemAvailable("avgBunchIntensities_1",&m_avgBunchIntensities_1);
        m_beam_ItemList.push_back("avgBunchIntensities_1");
        m_monInfoSpace->fireItemAvailable("avgBunchIntensities_2",&m_avgBunchIntensities_2);
        m_beam_ItemList.push_back("avgBunchIntensities_2");

        m_monInfoSpace->fireItemAvailable("bunchlength_1",&m_bunchlengthmean_1);
        m_beam_ItemList.push_back("bunchlength_1");
        m_monInfoSpace->fireItemAvailable("bunchlength_2",&m_bunchlengthmean_2);
        m_beam_ItemList.push_back("bunchlength_2");

        m_mon_runstatusTable.insert(std::make_pair("fillnum",&m_fillnum));
        m_mon_runstatusTable.insert(std::make_pair("runnum",&m_runnum));
        m_mon_runstatusTable.insert(std::make_pair("lsnum",&m_lsnum));
        m_mon_runstatusTable.insert(std::make_pair("nbnum",&m_nbnum));
        m_mon_runstatusTable.insert(std::make_pair("tssec",&m_tssec));
        m_mon_runstatusTable.insert(std::make_pair("tsmsec",&m_tsmsec));
        m_mon_runstatusTable.insert(std::make_pair("amodetag",&m_amodetag));
        m_mon_runstatusTable.insert(std::make_pair("targetegev",&m_targetegev));

        m_mon_vdmStatus.insert(std::make_pair("acqflag",&m_vdm_acqflag));
        m_mon_vdmStatus.insert(std::make_pair("beam",&m_vdm_beam));
        m_mon_vdmStatus.insert(std::make_pair("ip",&m_vdm_ip));
        m_mon_vdmStatus.insert(std::make_pair("status",&m_vdm_scanstatus));
        m_mon_vdmStatus.insert(std::make_pair("plane",&m_vdm_plane));
        m_mon_vdmStatus.insert(std::make_pair("nominal_separation_plane",&m_vdm_nominal_separation_plane));
        m_mon_vdmStatus.insert(std::make_pair("step",&m_vdm_step));
        m_mon_vdmStatus.insert(std::make_pair("progress",&m_vdm_progress));
        m_mon_vdmStatus.insert(std::make_pair("separation",&m_vdm_nominal_separation));

        m_mon_vdmParams.insert(std::make_pair("r_B1sep",&m_vdm_read_nominal_B1sepPlane));
        m_mon_vdmParams.insert(std::make_pair("r_B1xing",&m_vdm_read_nominal_B1xingPlane));
        m_mon_vdmParams.insert(std::make_pair("r_B2sep",&m_vdm_read_nominal_B2sepPlane));
        m_mon_vdmParams.insert(std::make_pair("r_B2xing",&m_vdm_read_nominal_B2xingPlane));
        m_mon_vdmParams.insert(std::make_pair("s_B1sep",&m_vdm_set_nominal_B1sepPlane));
        m_mon_vdmParams.insert(std::make_pair("s_B1xing",&m_vdm_set_nominal_B1xingPlane));
        m_mon_vdmParams.insert(std::make_pair("s_B2sep",&m_vdm_set_nominal_B2sepPlane));
        m_mon_vdmParams.insert(std::make_pair("s_B2xing",&m_vdm_set_nominal_B2xingPlane));

        m_mon_beaminfoTable.insert(std::make_pair("beam1 Intensity Total",&m_wholeBeamIntensity_1));
        m_mon_beaminfoTable.insert(std::make_pair("beam2 Intensity Total",&m_wholeBeamIntensity_2));
        m_mon_beaminfoTable.insert(std::make_pair("beam1 BunchLength Mean",&m_bunchlengthmean_1));
        m_mon_beaminfoTable.insert(std::make_pair("beam2 BunchLength Mean",&m_bunchlengthmean_2));

        m_monInfoSpace->addGroupRetrieveListener(this);
    }catch(xdata::exception::Exception& e){
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    }
    m_wl_firefastflash = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_firefastflash","waiting");
    m_wl_fireslowflash = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_fireslowflash","waiting");
    m_as_firefastflash = toolbox::task::bind(this,&bril::dip::CommonDataExchange::firefastflashlists,"firefastflash");
    m_as_fireslowflash = toolbox::task::bind(this,&bril::dip::CommonDataExchange::fireslowflashlists,"fireslowflash");
    m_tot_int_b1 = 0;
    m_tot_int_b2 = 0;
    m_avg_len_b1 = 0;
    m_avg_len_b2 = 0;
    m_avg_phase_b1 = 0;
    m_avg_phase_b2 = 0;
    memset(m_int_b1,0,sizeof(unsigned short)*3564);
    memset(m_int_b2,0,sizeof(unsigned short)*3564);
    memset(m_len_b1,0,sizeof(unsigned short)*3564);
    memset(m_len_b2,0,sizeof(unsigned short)*3564);
    memset(m_phase_b1,0,sizeof(short)*3564);
    memset(m_phase_b2,0,sizeof(short)*3564);
    
    m_scopeacqsec = 0;
    m_scopepublishtodip = true;
    m_previous_scanflag = false;

    m_bsrl_acqStamp = 0;
    memset(m_bsrl_post_fraction_ghost_bunched,0, sizeof(float)*2 );
    memset(m_bsrl_post_fraction_sat_bunched,0, sizeof(float)*2 );
    memset(m_bsrl_post_q_bunched,0, sizeof(float)*2*3*11880 );
    m_bsrlqBunched_canpublish = true;
    m_bsrlfracBunched_canpublish = true;
    //printf("THE ID of MAIN IS: %ld\n",(long int)syscall(186) );
    m_remus = 0;
    m_plt_HmF = 0;
    m_plt_HmN = 0;
    m_plt_HpF = 0;
    m_plt_HpN = 0;
}

CommonDataExchange::~CommonDataExchange (){
    if(m_withcsv){
        m_vdmfile.close();
    }
    for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
        m_dip->destroyDipSubscription(it->second);
        it->second = 0;
    }
    m_dipsubs.clear();
    for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        m_dip->destroyDipPublication(it->second);
        it->second = 0;
    }
    m_dippubs.clear();
    delete m_dip;
    if(m_puberrorhandler){
        delete m_puberrorhandler; m_puberrorhandler = 0;
    }
    if(m_servererrorhandler){
        delete m_servererrorhandler; m_servererrorhandler = 0;
    }
    cleanFlashLists();
}

void CommonDataExchange::Default(xgi::Input * in, xgi::Output * out) {
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+getApplicationDescriptor()->getURN();
    *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
    *out << "<p> Eventing Status</p>";
    *out <<busesToHTML();
    // update monitorables
    m_monInfoSpace->lock();
    m_monInfoSpace->fireItemGroupRetrieve(m_time_ItemList,this);
    m_monInfoSpace->fireItemGroupRetrieve(m_beam_ItemList,this);
    m_monInfoSpace->fireItemValueRetrieve("vdm_acqflag",this);
    m_monInfoSpace->unlock();
    *out << bril::dip::WebUtils::mapToHTML("Run Status",m_mon_runstatusTable,"",false);
    *out << bril::dip::WebUtils::mapToHTML("VDM Status",m_mon_vdmStatus,"",false);
    if(m_vdm_acqflag.value_){
        *out << WebUtils::mapToHTML("VDM Plane Parameters",m_mon_vdmParams,"",false);
    }
    *out << WebUtils::mapToHTML("Beam Info",m_mon_beaminfoTable,"",false);
}

void CommonDataExchange::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        //printf("THE ID of XDATA EVENT IS: %ld\n",(long int)syscall(186) );

        std::stringstream ss;
        ss << "Intopics: "<<m_inTopicStr.value_<<" ; Outtopics:"<<m_outTopicStr.value_;
        m_outtopics = toolbox::parseTokenSet(m_outTopicStr.value_,",");
        LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
        m_intopics = toolbox::parseTokenSet(m_inTopicStr.value_,",");
        m_dip = Dip::create( (std::string("brilcommon_")+m_processuuid).c_str() );
        m_servererrorhandler=new bril::dip::ServerErrorHandler;
        Dip::addDipDimErrorHandler(m_servererrorhandler);
        m_puberrorhandler = new PubErrorHandler;
        std::set<std::string> dipsubs = toolbox::parseTokenSet(m_dipSubStr.value_,",");
        for( std::set<std::string>::iterator it=dipsubs.begin(); it!=dipsubs.end(); ++it ){
            m_dipsubs.insert(std::make_pair(*it,(DipSubscription*)0 ));
        }
        if( !m_dipPubRoot.value_.empty() ){
            char lastChar = *m_dipPubRoot.value_.rbegin();
            if( lastChar!='/' ){ //dipPubRoot must end with '/'
                m_dipPubRoot.value_ += '/';
            }
        }
        std::set<std::string> dipouttopics = toolbox::parseTokenSet(m_dipPubStr.value_,",");//dippub string start AFTER dippubroot
        for( std::set<std::string>::iterator it=dipouttopics.begin(); it!=dipouttopics.end(); ++it ){
            std::string d = *it;
            if( !d.empty() ){
                if( *d.begin() == '/' ){//pub dip name must NOT start with '/'
                    d.erase(0,1);
                }
                m_dippubs.insert( std::make_pair(d, (DipPublication*)0) );
            }
        }
	createDipPublication();
        try{
            this->getEventingBus(m_bus.value_).addActionListener(this);
            this->getEventingBus(m_bus.value_).subscribe("NB4");

            for( std::set<std::string>::iterator it=m_intopics.begin(); it!=m_intopics.end(); ++it){
                this->getEventingBus(m_bus.value_).subscribe(*it);
            }
        }catch(eventing::api::exception::Exception& e){
            std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(exception::Exception,msg,e);
        }
        //batch create infospace for flashlist
        if( !m_flashlistStr.value_.empty() ){
            m_flashlists = toolbox::parseTokenSet(m_flashlistStr.value_,",");
            std::set< std::string >::iterator it;
            for( it=m_flashlists.begin(); it!=m_flashlists.end(); ++it ){
                std::string urn = createQualifiedInfoSpace(std::string("commondataexchange_")+*it).toString();
                xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(urn));
                m_flashinfostore.insert(std::make_pair(*it,is));//flash name to flash infospace
                init_flashlist(is,*it);
            }
            declareFlashLists();
            m_wl_firefastflash->activate();
            m_wl_fireslowflash->activate();
        }
        //dip pubs here if any
    }
}

void CommonDataExchange::actionPerformed(toolbox::Event& e){
    LOG4CPLUS_INFO(getApplicationLogger(), "Received toolbox event " << e.type());
    if ( e.type() == "eventing::api::BusReadyToPublish" ){
        //printf("THE ID of TOOLBOX EVENT IS: %ld\n",(long int)syscall(186) );
        std::stringstream msg;
        msg<< "Eventing bus '" << m_bus.value_ << "' is ready to publish";
        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
        if(m_withcsv){
            m_vdmfile.open((m_outdir.value_+std::string("/vdm_")+std::string(m_processuuid)+std::string(".csv")).c_str(),std::fstream::out|std::fstream::app);
            writeVDMHeader(m_vdmfile);
        }
        m_busready = true;
    }
}

void CommonDataExchange::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
  toolbox::mem::AutoReference refguard(ref);
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"&&ref!=0){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    if(topic == m_signalTopic.toString()){
      //parse timing signal message header to get timing info
      interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
      
      m_fillnum = thead->fillnum;
      m_nbnum = thead->nbnum;
      m_lsnum = thead->lsnum;
      m_runnum = thead->runnum;
      m_tssec = thead->timestampsec;
      m_tsmsec = thead->timestampmsec;
      if( m_clockready==false ){
	if( m_busready ){
	  subscribeToDip();
	  m_clockready = true;
	  m_wl_firefastflash->submit(m_as_firefastflash);
	  m_wl_fireslowflash->submit(m_as_fireslowflash);
	}
      }                
      
      std::stringstream msg;
      msg<<"Fill "<<m_fillnum.toString()<<" Run "<<m_runnum.toString()<<" ls "<<m_lsnum.toString()<<" nb "<<m_nbnum.toString();
      LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
      
      {// start guard
#ifdef __DEBUG__
	VerboseGuard<toolbox::BSem> g("onMessage "+topic,m_applock);
#else
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
#endif
	updateAllFlashTime();
	//write to csv
	if(m_withcsv){
	  if(m_scan_flag){
	    writeVDMToCSV(m_vdmfile);
	    m_vdmfile.flush();
	  }
	}
	for(std::set<std::string>::iterator it=m_outtopics.begin();it!=m_outtopics.end();++it){
	  publishToEventing(*it);
	}
      }//end guard
      
      for( std::map< std::string,DipPublication* >::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
	LOG4CPLUS_DEBUG( getApplicationLogger(), "about to publish to dip "<<it->first );
	publishToDip(it->first);
      }
      
    }else if( topic==interface::bril::ScopeDataT::topicname() ){
      if(ref){
	process_scopedata(ref,plist);
      }
    }else if( topic==interface::bril::vdmoutputT::topicname() ){
      if(ref){
	do_dippublish_vdmoutput( ref , plist, "BRIL/VdMMonitor");
      }
    }
  }
}

void CommonDataExchange::process_scopedata( toolbox::mem::Reference* ref , xdata::Properties & plist){
    std::stringstream ss;
    ss<<"process_scopedata";
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");ss.clear();
    std::string pdict = plist.getProperty("PAYLOAD_DICT");
    if( pdict.empty() ){
        pdict = interface::bril::ScopeDataT::payloaddict();
    }
    interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
    unsigned int scopeacqsec = thead->timestampsec;
    if( scopeacqsec!=m_scopeacqsec ){ //extract only new data
        m_scopeacqsec = scopeacqsec;
        m_scopepublishtodip = true;
        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.extract_field( &m_tot_int_b1, "TOT_INT_B1", thead->payloadanchor );
        tc.extract_field( &m_tot_int_b2, "TOT_INT_B2", thead->payloadanchor );
        tc.extract_field( &m_avg_len_b1, "AVG_LEN_B1", thead->payloadanchor );
        tc.extract_field( &m_avg_len_b2, "AVG_LEN_B2", thead->payloadanchor );
        tc.extract_field( m_len_b1, "LEN_B1", thead->payloadanchor );
        tc.extract_field( m_len_b2, "LEN_B2", thead->payloadanchor );
        tc.extract_field( m_int_b1, "INT_B1", thead->payloadanchor );
        tc.extract_field( m_int_b2, "INT_B2", thead->payloadanchor );
	tc.extract_field( &m_avg_phase_b1, "AVG_PHASE_B1", thead->payloadanchor );
	tc.extract_field( &m_avg_phase_b2, "AVG_PHASE_B2", thead->payloadanchor );
	tc.extract_field( m_phase_b1, "PHASE_B1", thead->payloadanchor );
	tc.extract_field( m_phase_b2, "PHASE_B2", thead->payloadanchor );
    }else{
        m_scopepublishtodip = false;
    }
}
  void CommonDataExchange::do_dippublish_vdmoutput( toolbox::mem::Reference* ref , xdata::Properties & plist, const std::string& diptopic){
    std::stringstream ss;
    ss<<"Entering do_dippublish_vdmoutput";
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");ss.clear();
    std::string pdict = plist.getProperty("PAYLOAD_DICT");
    if( pdict.empty() ){
        pdict = interface::bril::ScopeDataT::payloaddict();
    }
    interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
    int maxnbx = 3564;
    char provider[32];
    float mean_sep[maxnbx];
    float meanerr_sep[maxnbx];
    float sigma_sep[maxnbx];
    float sigmaerr_sep[maxnbx];
    float avgsigma_sep = 0;
    float avgsigmaerr_sep = 0;
    float mean_cross[maxnbx];
    float meanerr_cross[maxnbx];
    float sigma_cross[maxnbx];
    float sigmaerr_cross[maxnbx];
    float avgsigma_cross = 0;
    float avgsigmaerr_cross = 0;
    float pileup[maxnbx];
    float avgpileup = 0;
    float bxemittanceX[maxnbx];
    float bxemittanceY[maxnbx];
    float avgemittanceX = 0;
    float avgemittanceY = 0;
    float bxemittanceX_err[maxnbx];
    float bxemittanceY_err[maxnbx];
    float avgemittanceX_err = 0;
    float avgemittanceY_err = 0;

    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.extract_field( provider, "provider", thead->payloadanchor );
    tc.extract_field( mean_sep, "mean_sep", thead->payloadanchor );
    tc.extract_field( meanerr_sep, "meanerr_sep", thead->payloadanchor );
    tc.extract_field( sigma_sep, "sigma_sep", thead->payloadanchor );
    tc.extract_field( sigmaerr_sep, "sigmaerr_sep", thead->payloadanchor );
    tc.extract_field( &avgsigma_sep, "avgsigma_sep", thead->payloadanchor );
    tc.extract_field( &avgsigmaerr_sep, "avgsigmaerr_sep", thead->payloadanchor );
    tc.extract_field( mean_cross, "mean_cross", thead->payloadanchor );
    tc.extract_field( meanerr_cross, "meanerr_cross", thead->payloadanchor );
    tc.extract_field( sigma_cross, "sigma_cross", thead->payloadanchor );
    tc.extract_field( sigmaerr_cross, "sigmaerr_cross", thead->payloadanchor );
    tc.extract_field( &avgsigma_cross, "avgsigma_cross", thead->payloadanchor );
    tc.extract_field( &avgsigmaerr_cross, "avgsigmaerr_cross", thead->payloadanchor );
    tc.extract_field( pileup, "pileup", thead->payloadanchor );
    tc.extract_field( &avgpileup, "avgpileup", thead->payloadanchor );
    tc.extract_field( bxemittanceX, "bxemittanceX", thead->payloadanchor );
    tc.extract_field( bxemittanceY, "bxemittanceY", thead->payloadanchor );
    tc.extract_field( &avgemittanceX, "avgemittanceX", thead->payloadanchor );
    tc.extract_field( &avgemittanceY, "avgemittanceY", thead->payloadanchor );
    tc.extract_field( bxemittanceX_err, "bxemittanceX_err", thead->payloadanchor );
    tc.extract_field( bxemittanceY_err, "bxemittanceY_err", thead->payloadanchor );
    tc.extract_field( &avgemittanceX_err, "avgemittanceX_err", thead->payloadanchor );
    tc.extract_field( &avgemittanceY_err, "avgemittanceY_err", thead->payloadanchor );
    std::string truediptopic = diptopic+std::string("/")+std::string(provider);

    LOG4CPLUS_INFO(getApplicationLogger(),"publish vdmoutput to DIP");
    if( m_dippubs.find(diptopic)!=m_dippubs.end() ){
      DipTimestamp t;
      DipData* dipdata = m_dip->createDipData();

      dipdata->insert( (DipFloat*)&mean_sep,maxnbx,"mean_separation" );
      dipdata->insert( (DipFloat*)&meanerr_sep,maxnbx,"meanerr_separation" );
      dipdata->insert( (DipFloat*)&sigma_sep,maxnbx,"capsigma_separation" );
      dipdata->insert( (DipFloat*)&sigmaerr_sep,maxnbx,"capsigmaerr_separation" );
      dipdata->insert( DipFloat(avgsigma_sep),"average_capsigma_separation" );
      dipdata->insert( DipFloat(avgsigmaerr_sep),"averageerr_capsigma_separation" );
      dipdata->insert( (DipFloat*)&mean_cross,maxnbx,"mean_crossing" );
      dipdata->insert( (DipFloat*)&meanerr_cross,maxnbx,"meanerr_crossing" );
      dipdata->insert( (DipFloat*)&sigma_cross,maxnbx,"capsigma_crossing" );
      dipdata->insert( (DipFloat*)&sigmaerr_cross,maxnbx,"capsigmaerr_crossing" );
      dipdata->insert( DipFloat(avgsigma_cross),"average_capsigma_crossing" );
      dipdata->insert( DipFloat(avgsigmaerr_cross),"averageerr_capsigma_crossing" );
      dipdata->insert( (DipFloat*)&pileup,maxnbx,"pielup" );
      dipdata->insert( DipFloat(avgpileup),"avgpileup" );
      dipdata->insert( (DipFloat*)&bxemittanceX,maxnbx,"bxemittanceX" );
      dipdata->insert( (DipFloat*)&bxemittanceY,maxnbx,"bxemittanceY" );
      dipdata->insert( DipFloat(avgemittanceX),"avgemittanceX" );
      dipdata->insert( DipFloat(avgemittanceY),"avgemittanceY" );
      dipdata->insert( (DipFloat*)&bxemittanceX_err,maxnbx,"bxemittanceX_err" );
      dipdata->insert( (DipFloat*)&bxemittanceY_err,maxnbx,"bxemittanceY_err" );
      dipdata->insert( DipFloat(avgemittanceX_err),"avgemittanceX_err" );
      dipdata->insert( DipFloat(avgemittanceY_err),"avgemittanceY_err" );

      if( m_dippubs.find(truediptopic)==m_dippubs.end() ){
	std::string fulldipname=m_dipPubRoot.value_+truediptopic;
	DipPublication* p=m_dip->createDipPublication(fulldipname.c_str(),m_puberrorhandler);
	if(!p){
	  LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+fulldipname);	  
        }else{
	  m_dippubs.insert( std::make_pair(truediptopic,p) );
        }

      } 
      ss<<"DIP Published to "<<truediptopic;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      ss.str("");ss.clear();
      try{
	m_dippubs[truediptopic]->send(*dipdata,t);
      }catch( DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));
      }
      delete dipdata;
    }
}
void CommonDataExchange::publishToDip(const std::string& diptopic){
    if( diptopic.find("BPTX-BunchData")!=std::string::npos ){
        if( m_intopics.find("ScopeData")==m_intopics.end() ){
            //only matters if subscribed to ScopedData.
            LOG4CPLUS_INFO(getApplicationLogger(),"NOT subscribed to eventing topic ScopeData, do nothing");
            return;
        }
        if( !m_scopepublishtodip ) {
            LOG4CPLUS_INFO(getApplicationLogger(),"NO new scope data, do nothing");
            return;
        }
        do_dippublish_scopedata(diptopic);
    }else if( diptopic.find("AutomaticScan")!=std::string::npos ){
      do_dippublish_automaticscan(diptopic);
    }
}

void CommonDataExchange::do_dippublish_automaticscan( const std::string& diptopic ){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"do_dippublish_automaticscan "+diptopic);
  std::stringstream ss;
  if( m_dippubs[diptopic] ){
    DipData* dipdata = m_dip->createDipData();
    DipTimestamp t;
    int val = 0;
    std::string strval("ENABLE");
    dipdata->insert( DipInt(val),"Delay1" );
    dipdata->insert( strval,"Sequence" );
    ss<<"DIP Published to "<<std::string(m_dippubs[diptopic]->getTopicName());
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    ss.str("");ss.clear();
    try{
      m_dippubs[diptopic]->send(*dipdata,t);
    }catch( DipException& e){
      LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));
    }
    delete dipdata;
  }else{
    LOG4CPLUS_INFO(getApplicationLogger(),diptopic+" not found in the dippubs, do nothing");
  }
}

void CommonDataExchange::do_dippublish_scopedata( const std::string& diptopic){
    LOG4CPLUS_INFO(getApplicationLogger(),"do_dippublish_scopedata "+diptopic);
    std::stringstream ss;
    if( m_dippubs[diptopic] ){
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        DipTimestamp t;
        DipData* dipdata = m_dip->createDipData();
        float myint_b1[maxnbx];
        float myint_b2[maxnbx];
        float mylen_b1[maxnbx];
        float mylen_b2[maxnbx];
	float myphase_b1[maxnbx];
	float myphase_b2[maxnbx];
        if( now.sec()-m_scopeacqsec>300 ){//if scope data is 5 minutes old, reset values to zero
            for( int i=0; i<maxnbx; ++i ){
                m_tot_int_b1 = 0;
                m_tot_int_b2 = 0;
                m_avg_len_b1 = 0;
                m_avg_len_b2 = 0;
		m_avg_phase_b1 = 0;
		m_avg_phase_b2 = 0;
                memset(m_int_b1,0,sizeof(m_int_b1));
                memset(m_int_b2,0,sizeof(m_int_b2));
                memset(m_len_b1,0,sizeof(m_len_b1));
                memset(m_len_b2,0,sizeof(m_len_b2));
		memset(m_phase_b1,0,sizeof(m_phase_b1));
		memset(m_phase_b2,0,sizeof(m_phase_b2));
            }
        }

        for( int i=0; i<maxnbx; ++i ){
            myint_b1[i] = float(m_int_b1[i])*1e7;
            myint_b2[i] = float(m_int_b2[i])*1e7;
            mylen_b1[i] = float(m_len_b1[i])/1e3;
            mylen_b2[i] = float(m_len_b2[i])/1e3;
	    myphase_b1[i] = float(m_phase_b1[i])/1e3;
	    myphase_b2[i] = float(m_phase_b2[i])/1e3;
        }

        dipdata->insert( DipLong(m_scopeacqsec),"ACQ_TIMESTAMPSEC" );
        dipdata->insert( DipFloat(m_tot_int_b1),"TOT_INT_B1" );
        dipdata->insert( DipFloat(m_tot_int_b2),"TOT_INT_B2" );
        //ticket JIRA CMSBRIL-50
        //divide the values of m_avg_len_b1 and m_avg_len_b2 by 1000 before publication
        dipdata->insert( DipFloat(m_avg_len_b1/1000.),"AVG_LEN_B1" );
        dipdata->insert( DipFloat(m_avg_len_b2/1000.),"AVG_LEN_B2" );
        dipdata->insert( (DipFloat*)&mylen_b1,maxnbx,"LEN_B1" );
        dipdata->insert( (DipFloat*)&mylen_b2,maxnbx,"LEN_B2" );
        dipdata->insert( (DipFloat*)&myint_b1,maxnbx,"INT_B1" );
        dipdata->insert( (DipFloat*)&myint_b2,maxnbx,"INT_B2" );
	dipdata->insert( DipFloat(float(m_avg_phase_b1)/1000.),"AVG_PHASE_B1" );
	dipdata->insert( DipFloat(float(m_avg_phase_b2)/1000.),"AVG_PHASE_B2" );
	dipdata->insert( (DipFloat*)&myphase_b1,maxnbx,"PHASE_B1" );
	dipdata->insert( (DipFloat*)&myphase_b2,maxnbx,"PHASE_B2" );
        ss<<"DIP Published to "<<std::string(m_dippubs[diptopic]->getTopicName());
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        ss.str("");ss.clear();
        try{
            m_dippubs[diptopic]->send(*dipdata,t);
        }catch( DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));
        }
        delete dipdata;
    }else{
        LOG4CPLUS_INFO(getApplicationLogger(),diptopic+" not found in the dippubs, do nothing");
    }
}

void CommonDataExchange::publishToEventing(const std::string& topicname){
    if(topicname=="beam"){
      do_publish_beam();
    }else if( topicname=="remuslumi" ){
      do_publish_remus();
    }else if(topicname=="vdmscan"){
      if(m_scan_flag){
	do_publish_vdmscan();
      }
    }else if(topicname=="atlasbeam"){
      if(m_vdm_acqflag.value_){
	do_publish_atlasbeam();
      }
    }else if(topicname=="bunchlength"){
        do_publish_bunchlength();
    }else if(topicname=="vdmflag"){
      if(m_scan_flag){
        do_publish_vdmflag();
      }
    }else if(topicname=="bxconfig"){
      do_publish_bxconfig();
    }else if(topicname=="bsrlfracBunched"){
      if( m_scan_flag && !m_previous_scanflag ){
            toolbox::task::Timer* t = 0;
            if( !toolbox::task::TimerFactory::getInstance()->hasTimer( m_bsrlfracBunched_timerurn ) ){
                t = toolbox::task::TimerFactory::getInstance()->createTimer( m_bsrlfracBunched_timerurn );
            }else{
                t = toolbox::task::TimerFactory::getInstance()->getTimer( m_bsrlfracBunched_timerurn );
            }
            if( m_bsrlfracBunched_canpublish ){
                const std::string taskname("unblock_bsrlfracBunched");
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                toolbox::TimeInterval vsec(300,0);//5 minutes
                toolbox::TimeVal startT = now + vsec;
                //start timer task: at the end of time set m_bsrlfracBunched_canpublish to true
                if( t->isActive() ){
                    t->stop();
                }
                LOG4CPLUS_DEBUG(getApplicationLogger(), "start timer for "+taskname);
                t->start();
                t->schedule( this, startT, (void*)0, taskname );
            }
            do_publish_bsrlfracBunched();// at the end of succesful publishing set m_bsrlfracBunched_canpublish to false
        }
    }else if(topicname=="bsrlqBunched"){
        if( m_scan_flag && !m_previous_scanflag ){
            toolbox::task::Timer* t = 0;

            if( !toolbox::task::TimerFactory::getInstance()->hasTimer( m_bsrlqBunched_timerurn ) ){
                t = toolbox::task::TimerFactory::getInstance()->createTimer( m_bsrlqBunched_timerurn );
            }else{
                t = toolbox::task::TimerFactory::getInstance()->getTimer( m_bsrlqBunched_timerurn );
            }
            if( m_bsrlqBunched_canpublish ){
                const std::string taskname("unblock_bsrlqBunched");
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                toolbox::TimeInterval vsec(300,0);//5 minutes
                toolbox::TimeVal startT = now + vsec;
                //start timer task: at the end of time set m_bsrlqBunched_canpublish to true
                if( t->isActive() ){
                    t->stop();
                }
                LOG4CPLUS_DEBUG(getApplicationLogger(), "start timer for "+taskname);
                t->start();
                t->schedule( this, startT, (void*)0, taskname );
            }
            do_publish_bsrlqBunched();// at the end of succesful publishing set m_bsrlqBunched_canpublish to false
        }
    }else if( topicname=="luminousregion" ){
      if( m_vdm_acqflag.value_ ){
	do_publish_luminousregion();
      }
    }
}
void CommonDataExchange::timeExpired( toolbox::task::TimerEvent& e ){
    if( e.getTimerTask()->name=="unblock_bsrlfracBunched" ){
        m_bsrlfracBunched_canpublish = true;
    }else if( e.getTimerTask()->name=="unblock_bsrlqBunched" ){
        m_bsrlqBunched_canpublish = true;
    }
}

void CommonDataExchange::subscribeToDip(){
    LOG4CPLUS_INFO(getApplicationLogger(),"subscribeToDip ");
    m_dip->setTimeout(3);
    for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
        DipSubscription* s = 0;
        s=m_dip->createDipSubscription(it->first.c_str(),this);
        if(!s){
            m_dipsubs.erase(it); // remove from registry
        }else{
            it->second=s;
        }
    }
}

void CommonDataExchange::connected(DipSubscription* dipsub){
    LOG4CPLUS_INFO(getApplicationLogger(),"connected to dip publication source "+std::string(dipsub->getTopicName()) );
}

void CommonDataExchange::disconnected(DipSubscription* dipsub,char *reason){
    LOG4CPLUS_INFO(getApplicationLogger(),"disconnected from dip publication source "+std::string(dipsub->getTopicName()) );
}

void CommonDataExchange::handleException(DipSubscription* dipsub, DipException& ex){
    LOG4CPLUS_INFO(getApplicationLogger(),"Publication source "+std::string(dipsub->getTopicName())+" exception "+std::string(ex.what()));
}

void CommonDataExchange::handleMessage(DipSubscription* dipsub, DipData& message){
    std::string subname(dipsub->getTopicName());
    LOG4CPLUS_DEBUG(getApplicationLogger(),"CommonDataExchange::handleMessage from "+subname);
    if( message.isEmpty() ) {
        LOG4CPLUS_ERROR(getApplicationLogger(),"DIP topic is empty "+subname);
        return;
    }
#ifdef __DEBUG__
    VerboseGuard<toolbox::BSem> g("handleMessage "+subname,m_applock);
#else
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
#endif
    if(subname=="dip/acc/LHC/Beam/AutomaticScanIP5"){
        m_scan_flag = false;
        bool vdm_acqflag = false;//any scan is on
        int vdm_ip = 0;	
	try{
	  m_scan_flag = (bool)message.extractBool("Active");
	  if(m_scan_flag) {
	    vdm_ip=32;
	  }
        }catch(const DipException& e){
	  LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        try{//if false, beam is moving; if true, scan data valid
	  vdm_acqflag = (bool)message.extractBool("Acquisition_Flag");
	  if( !m_scan_flag ){
	    vdm_acqflag = false;
	  }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        int vdm_beam = 0;
        try{
            vdm_beam = (int)message.extractInt("Beam");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        //try{
	  //   vdm_ip = (int)message.extractInt("IP");
        //}catch(const DipException& e){
        //    LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        //}
        std::string vdm_scanstatus;
        try{
            vdm_scanstatus = message.extractString("LumiScan_Status");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        std::string vdm_plane;
        try{
            vdm_plane = message.extractString("Plane");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        std::string vdm_nominal_separation_plane;
        try{
            vdm_nominal_separation_plane = message.extractString("Nominal_Separation_Plane");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        int vdm_step = 0;
        try{
            vdm_step = (int)message.extractInt("Step");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        int vdm_progress = 0;
        try{
            vdm_progress = (int)message.extractInt("Step_Progress");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_nominal_separation = 0;
        try{
            vdm_nominal_separation = (float)message.extractDouble("Nominal_Separation");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_read_nominal_B1sepPlane = 0;
        try{
            vdm_read_nominal_B1sepPlane = (float)message.extractDouble( "Read_Nominal_Displacement_B1_sepPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_read_nominal_B1xingPlane = 0;
        try{
            vdm_read_nominal_B1xingPlane = (float)message.extractDouble( "Read_Nominal_Displacement_B1_xingPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_read_nominal_B2sepPlane = 0;
        try{
            vdm_read_nominal_B2sepPlane = (float)message.extractDouble( "Read_Nominal_Displacement_B2_sepPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_read_nominal_B2xingPlane = 0;
        try{
            vdm_read_nominal_B2xingPlane = (float)message.extractDouble( "Read_Nominal_Displacement_B2_xingPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_set_nominal_B1sepPlane = 0;
        try{
            vdm_set_nominal_B1sepPlane = (float)message.extractDouble( "Set_Nominal_Displacement_B1_sepPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_set_nominal_B1xingPlane = 0;
        try{
            vdm_set_nominal_B1xingPlane = (float)message.extractDouble( "Set_Nominal_Displacement_B1_xingPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_set_nominal_B2sepPlane = 0;
        try{
            vdm_set_nominal_B2sepPlane = (float)message.extractDouble( "Set_Nominal_Displacement_B2_sepPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        float vdm_set_nominal_B2xingPlane = 0;
        try{
            vdm_set_nominal_B2xingPlane = (float)message.extractDouble( "Set_Nominal_Displacement_B2_xingPlane" );
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
	m_previous_scanflag = m_scan_flag;
        // update mon infospace
        m_vdm_acqflag = vdm_acqflag;
        m_vdm_beam = vdm_beam;
        m_vdm_ip = vdm_ip ;
        m_vdm_scanstatus = vdm_scanstatus;
        m_vdm_plane = vdm_plane;
        m_vdm_step = vdm_step;
        m_vdm_progress = vdm_progress;
        m_vdm_nominal_separation = vdm_nominal_separation;
        m_vdm_read_nominal_B1sepPlane = vdm_read_nominal_B1sepPlane;
        m_vdm_read_nominal_B1xingPlane = vdm_read_nominal_B1xingPlane;
        m_vdm_read_nominal_B2sepPlane = vdm_read_nominal_B2sepPlane;
        m_vdm_read_nominal_B2xingPlane = vdm_read_nominal_B2xingPlane;
        m_vdm_set_nominal_B1sepPlane = vdm_set_nominal_B1sepPlane;
        m_vdm_set_nominal_B1xingPlane = vdm_set_nominal_B1xingPlane;
        m_vdm_set_nominal_B2sepPlane = vdm_set_nominal_B2sepPlane;
        m_vdm_set_nominal_B2xingPlane = vdm_set_nominal_B2xingPlane;
        m_vdm_nominal_separation_plane = vdm_nominal_separation_plane;

    }else if(subname=="dip/acc/LHC/Beam/Intensity24Bits/Beam1"){
        std::stringstream ss;
        float wholeBeamIntensity = 0;
        try{
            wholeBeamIntensity = (float)message.extractFloat("totalIntensityB1");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"wholeBeamIntensity_1: "<<wholeBeamIntensity;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());

        m_wholeBeamIntensity_1 = wholeBeamIntensity;

        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "beam1_intensity";
            if( isIt->second->hasItem(isvar) ){
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = m_wholeBeamIntensity_1.value_;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }

    }else if(subname=="dip/acc/LHC/Beam/Intensity24Bits/Beam2"){
        std::stringstream ss;
        float wholeBeamIntensity = 0;
        try{
            wholeBeamIntensity = (float)message.extractFloat("totalIntensityB2");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"wholeBeamIntensity_2: "<<wholeBeamIntensity;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());

        m_wholeBeamIntensity_2 = wholeBeamIntensity;

        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "beam2_intensity";
            if( isIt->second->hasItem(isvar) ){
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = m_wholeBeamIntensity_2.value_;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }

    }else if(subname=="dip/acc/LHC/Beam/IntensityPerBunch/Beam1"){
        std::stringstream ss;

        float* avgBunchIntensities = 0;
        int beamintensity_arraysize = 0;
        int bxconfig_arraysize = 0;
        bool* beamconfig = 0;

        try{
            avgBunchIntensities = (float*)message.extractFloatArray(beamintensity_arraysize,"averageBunchIntensities"); //FBCT
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        try{
            beamconfig = (bool*)message.extractBoolArray(bxconfig_arraysize,"bunchFillingPattern");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        //m_wholeBeamIntensity_1 = wholeBeamIntensity;
        m_nfilledBunches_1 = 0;
        for( int bx=0;bx<maxnbx;++bx ){
            m_avgBunchIntensities_1[bx] = 0;
            m_bxconfig1[bx] = 0;
        }
        for( int bx=0;bx<beamintensity_arraysize;++bx ){
            m_avgBunchIntensities_1[bx] = avgBunchIntensities[bx];
        }
        for( int bx=0;bx<bxconfig_arraysize;++bx ){
            m_bxconfig1[bx] = (int)beamconfig[bx];
            if( beamconfig[bx] ){
                m_nfilledBunches_1 = m_nfilledBunches_1+1 ;
            }
        }
        ss<<"fbct_1 nbx : "<<m_nfilledBunches_1.toString();
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
        LOG4CPLUS_DEBUG( getApplicationLogger(),"fbct_1 : pattern "+m_bxconfig1.toString() );
        //end of dip extraction
    }else if(subname=="dip/acc/LHC/Beam/IntensityPerBunch/Beam2"){
        std::stringstream ss;

        float* avgBunchIntensities = 0;
        int beamintensity_arraysize = 0;
        int bxconfig_arraysize = 0;
        bool* beamconfig = 0;

        try{
            avgBunchIntensities = (float*)message.extractFloatArray(beamintensity_arraysize,"averageBunchIntensities"); //FBCT
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        try{
            beamconfig = (bool*)message.extractBoolArray(bxconfig_arraysize,"bunchFillingPattern");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        m_nfilledBunches_2 = 0;
        for( int bx=0;bx<maxnbx;++bx ){
            m_avgBunchIntensities_2[bx] = 0;
            m_bxconfig2[bx] = 0;
        }
        for( int bx=0;bx<beamintensity_arraysize;++bx ){
            m_avgBunchIntensities_2[bx] = avgBunchIntensities[bx];
        }
        for( int bx=0;bx<bxconfig_arraysize;++bx ){
            m_bxconfig2[bx] = (int)beamconfig[bx];
            if( beamconfig[bx] ){
                m_nfilledBunches_2 = m_nfilledBunches_2+1 ;
            }
        }
        ss<<"fbct_2 nbx : "<<m_nfilledBunches_2.toString();
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
        LOG4CPLUS_DEBUG( getApplicationLogger(),"fbct_2 : pattern "+m_bxconfig2.toString() );

    } else if (subname=="dip/CMS/LHC/LuminousRegion") {
        std::stringstream ss;
        float* tilt = 0;
        int tilt_array_size = 0;
        float* centroid = 0;
        int centroid_array_size = 0;
        float* size = 0;
        int size_array_size = 0;
        try{
            tilt = (float*)message.extractFloatArray(tilt_array_size, "Tilt");
            centroid = (float*)message.extractFloatArray(centroid_array_size, "Centroid");
            size = (float*)message.extractFloatArray(size_array_size, "Size");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        std::fill(m_luminous_region_tilt.begin(), m_luminous_region_tilt.end(), -1.0);
        for (int i = 0; i < tilt_array_size && i < int(m_luminous_region_tilt.size()); ++i) {
            m_luminous_region_tilt[i] = tilt[i];
        }
        std::fill(m_luminous_region_centroid.begin(), m_luminous_region_centroid.end(), -1.0);
        for (int i = 0; i < centroid_array_size && i < int(m_luminous_region_centroid.size()); ++i) {
            m_luminous_region_centroid[i] = centroid[i];
        }
        std::fill(m_luminous_region_size.begin(), m_luminous_region_size.end(), -1.0);
        for (int i = 0; i < size_array_size && i < int(m_luminous_region_size.size()); ++i) {
            m_luminous_region_size[i] = size[i];
        }

        ss << "LuminousRegion Tilt: " << m_luminous_region_tilt.toString() <<" Centroid: "<< m_luminous_region_centroid.toString()<< " Size: "<<m_luminous_region_size.toString();        
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());

    }else if( subname=="dip/acc/LHC/Beam/BetaStar/Bstar5" ){
        std::stringstream ss;
        int bstar5 = 0;
        try{
            bstar5 = message.extractInt("payload");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        if( bstar5!=m_bstar5.value_ ){
            m_bstar5 = bstar5;
            ss<<"bstar5 : "<<m_bstar5;
            LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
        }

    }else if( subname=="dip/acc/LHC/Beam/BPM/LSS5L/B1_DOROS" ){

        try{
            int arraysize = 0;
            const std::string* vName = message.extractStringArray(arraysize,"bpmNames");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B1Names.value_ = vName[0];
                arraysize = 0;
            }
            const double* hPos = message.extractDoubleArray(arraysize,"horizontalPos");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B1hPos.value_ = float(hPos[0]);
                arraysize = 0;
            }
            const double* vPos = message.extractDoubleArray(arraysize,"verticalPos");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B1vPos.value_ = float(vPos[0]);
                arraysize = 0;
            }
            const double* hErr = message.extractDoubleArray(arraysize,"errorH");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B1hErr.value_ = float(hErr[0]);
                arraysize = 0;
            }
            const double* vErr = message.extractDoubleArray(arraysize,"errorV");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B1vErr.value_ = float(vErr[0]);
                arraysize = 0;
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
    }else if(subname=="dip/acc/LHC/Beam/BPM/LSS5L/B2_DOROS"){
        ;
        try{
            int arraysize = 0;
            const std::string* vName = message.extractStringArray(arraysize,"bpmNames");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B2Names.value_ = vName[0];
                arraysize = 0;
            }

            const double* hPos = message.extractDoubleArray(arraysize,"horizontalPos");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B2hPos.value_ = float(hPos[0]);
                arraysize = 0;
            }
            const double* vPos = message.extractDoubleArray(arraysize,"verticalPos");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B2vPos.value_ = float(vPos[0]);
                arraysize = 0;
            }
            const double* hErr = message.extractDoubleArray(arraysize,"errorH");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B2hErr.value_ = float(hErr[0]);
                arraysize = 0;
            }
            const double* vErr = message.extractDoubleArray(arraysize,"errorV");
            if(arraysize>0){
                m_vdm_bpm_5LDOROS_B2vErr.value_ = float(vErr[0]);
                arraysize = 0;
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
    }else if(subname=="dip/acc/LHC/Beam/BPM/LSS5R/B1_DOROS"){

        try{
            int arraysize = 0;
            const std::string* vName = message.extractStringArray(arraysize,"bpmNames");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B1Names.value_ = vName[0];
                arraysize = 0;
            }
            const double* hPos = message.extractDoubleArray(arraysize,"horizontalPos");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B1hPos.value_ = float(hPos[0]);
                arraysize = 0;
            }
            const double* vPos = message.extractDoubleArray(arraysize,"verticalPos");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B1vPos.value_ = float(vPos[0]);
                arraysize = 0;
            }
            const double* hErr = message.extractDoubleArray(arraysize,"errorH");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B1hErr.value_ = float(hErr[0]);
                arraysize = 0;
            }
            const double* vErr = message.extractDoubleArray(arraysize,"errorV");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B1vErr.value_ = float(vErr[0]);
                arraysize = 0;
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
    }else if(subname=="dip/acc/LHC/Beam/BPM/LSS5R/B2_DOROS"){
        ;
        try{
            int arraysize = 0;
            const std::string* vName = message.extractStringArray(arraysize,"bpmNames");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B2Names.value_ = vName[0];
                arraysize = 0;
            }
            const double* hPos = message.extractDoubleArray(arraysize,"horizontalPos");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B2hPos.value_ = float(hPos[0]);
                arraysize = 0;
            }
            const double* vPos = message.extractDoubleArray(arraysize,"verticalPos");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B2vPos.value_ = float(vPos[0]);
                arraysize = 0;
            }
            const double* hErr = message.extractDoubleArray(arraysize,"errorH");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B2hErr.value_ = float(hErr[0]);
                arraysize = 0;
            }
            const double* vErr = message.extractDoubleArray(arraysize,"errorV");
            if(arraysize>0){
                m_vdm_bpm_5RDOROS_B2vErr.value_ = float(vErr[0]);
                arraysize = 0;
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
    }else if(subname=="dip/TNproxy/ATLAS/LHC/Luminosity"){
        if (m_vdm_acqflag.value_){
            std::stringstream ss;
            float atlaslumi = 0;
            try{
                atlaslumi = message.extractFloat("Lumi_TotInst");
            }catch(const DipException& e){
                LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
            }
            ;
            m_vdm_atlas_totInst.value_ = atlaslumi;
            ss<<"ATLAS inst lumi: "<<atlaslumi;
            LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
        }
    }else if(subname=="dip/TNproxy/ATLAS/LHC/BPTX1/IntensitiesPerBunch"){
        std::stringstream ss;
        int arraysize = 0;
        float* bxintensity = 0;
        float atlasBeamIntensity = 0;
        int nbx = 0;
        try{
            atlasBeamIntensity = (float)message.extractFloat("Sum");
            bxintensity = (float*)message.extractFloatArray(arraysize,"Values");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"ATLAS beamintensity_1: "<<atlasBeamIntensity <<" , ";

        m_atlasBeamIntensity_1 = atlasBeamIntensity;
        for(int bx=0; bx<maxnbx;++bx){
            m_atlasBunchIntensities_1[bx] = 0;
        }
        for( int i=0;i<arraysize;++i ){
            m_atlasBunchIntensities_1[i] = bxintensity[i];
            if( bxintensity[i]>0 ){
                nbx++;
            }
        }
        ss<<"nbx: "<<nbx;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());

    }else if(subname=="dip/TNproxy/ATLAS/LHC/BPTX2/IntensitiesPerBunch"){
        std::stringstream ss;
        int arraysize = 0;
        float* bxintensity = 0;
        float atlasBeamIntensity = 0;
        int nbx = 0;
        try{
            atlasBeamIntensity = (float)message.extractFloat("Sum");
            bxintensity = (float*)message.extractFloatArray(arraysize,"Values");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"ATLAS beamintensity_2: "<<atlasBeamIntensity<<" , ";

        m_atlasBeamIntensity_2 = atlasBeamIntensity;
        for(int bx=0; bx<maxnbx;++bx){
            m_atlasBunchIntensities_2[bx] = 0;
        }
        for( int i=0;i<arraysize;++i ){
            m_atlasBunchIntensities_2[i] = bxintensity[i];
            if( bxintensity[i]>0 ){
                nbx++;
            }
        }
        ss<<"nbx: "<<nbx;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
    }else if(subname=="dip/acc/LHC/Beam/BunchLengths/Beam1"){
        std::stringstream ss;
        float bunchlengthmean = 0;
        float* bunchlength = 0;
        int arraysize = 0;
        try{
            bunchlengthmean = (float)message.extractFloat("bunchLengthMean");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"bunchLengthMean_1: "<<bunchlengthmean;
        try{
            bunchlength = (float*)message.extractFloatArray(arraysize,"bunchLengths");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str() );

        m_bunchlengthmean_1 = bunchlengthmean;
        for( int i=0;i<maxnbx;++i){
            m_bunchlength_1[i] = 0;
        }
        for( int i=0;i<arraysize;++i ){
            m_bunchlength_1[i] = bunchlength[i];
        }
    }else if(subname=="dip/acc/LHC/Beam/BunchLengths/Beam2"){
        std::stringstream ss;
        float bunchlengthmean = 0;
        float* bunchlength = 0;
        int arraysize = 0;
        try{
            bunchlengthmean = (float)message.extractFloat("bunchLengthMean");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"bunchLengthMean_2: "<<bunchlengthmean;
        try{
            bunchlength = (float*)message.extractFloatArray(arraysize,"bunchLengths");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str() );

        m_bunchlengthmean_2 = bunchlengthmean;
        for( int i=0;i<maxnbx;++i){
            m_bunchlength_2[i] = 0;
        }
        for( int i=0;i<arraysize;++i ){
            m_bunchlength_2[i] = bunchlength[i];
        }
    }else if(subname=="dip/acc/LHC/Beam/Energy"){

        m_egev = 0;
        try{
            m_egev = ( (float)message.extractInt("payload") *0.120 );//conversion from *non-standard* units to GeV
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        if( m_egev.value_>1000 && abs(m_egev.value_-m_targetegev.value_)>300 ){
            //automatically adjust targetegev from manually configured to int of beamenergy, but only if egev is sensible
            //m_targetegev = (unsigned short)(floor( (m_egev.value_+0.5) )) ;
            m_targetegev = (unsigned short)(ceil(m_egev.value_));
        }
        LOG4CPLUS_INFO(getApplicationLogger(),"BeamEgev: "+m_egev.toString()+" , TargetEgev: "+m_targetegev.toString() );

    }else if( subname=="dip/acc/LHC/RunControl/BeamMode" ){
        try{
            std::string BeamMode = message.extractString("value");
            ;
            if( !BeamMode.empty() ){
                m_beamstatus.fromString(BeamMode);
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_INFO(getApplicationLogger(),"BeamStatus: "+m_beamstatus.toString() );

    }else if(subname=="dip/acc/LHC/RunControl/RunConfiguration"){
        std::stringstream ss;
        try{
            m_fillscheme = message.extractString("ACTIVE_INJECTION_SCHEME");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        std::string p1type;
        std::string p2type;
        std::string ip5xingHmurad_str;
        int ip5xingHmurad = 0;
        try{
            p1type = message.extractString("PARTICLE_TYPE_B1");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        try{
            p2type = message.extractString("PARTICLE_TYPE_B2");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        try{
            ip5xingHmurad_str = message.extractString("IP5-XING-H-MURAD");
            std::stringstream cs(ip5xingHmurad_str);
            int s = 0;
            ip5xingHmurad = cs >> s ? s : 0;
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        if( m_ip5xingHmurad.value_!= ip5xingHmurad ){
            m_ip5xingHmurad = ip5xingHmurad;
        }
        if( (p1type==p2type)&&(p1type=="PROTON") ){
            m_amodetag = "PROTPHYS";
        }else if( (p1type=="PROTON")&&(p2type=="ION") ){
            m_amodetag = "PAPHYS";
        }else if( (p1type=="ION")&&(p2type=="PROTON") ){
            m_amodetag = "APPHYS";
        }else if( (p1type=="ION")&&(p2type=="ION") ){
            m_amodetag = "IONPHYS";
        }
        ss<<"amodetag: "<<m_amodetag.toString()<<" , fillscheme: "<<m_fillscheme.toString()<<" ip5xingHmurad: "<<m_ip5xingHmurad.toString();
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str());
    }else if(subname=="dip/acc/LHC/RunControl/MachineMode"){
        try{
            m_machinemode = message.extractString("value");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_INFO( getApplicationLogger(),"machine mode: "+m_machinemode.toString() );
    }else if( subname=="dip/CMS/BRIL/BPTX/Scope-Bunches" ){
        std::stringstream ss;

        float colliding_delta_t = -0.99;
        int* bptxpattern_1 = 0;
        int* bptxpattern_2 = 0;
        int nbptx_1 = 0;
        int nbptx_2 = 0;
        ;
        m_bptx1_number_of_bunches = 0;
        m_bptx2_number_of_bunches = 0;
        m_bptx1_pattern.clear();
        m_bptx2_pattern.clear();
        for( int i=0;i<maxnbx;++i ){
            m_bptx1_pattern.push_back(0);
            m_bptx2_pattern.push_back(0);
        }

        try{
            colliding_delta_t = message.extractFloat("colliding_delta_t");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"colliding_delta_t: "<<colliding_delta_t<<" , ";
        ;
        try{
            m_bptx1_number_of_bunches = (int)message.extractInt("bptx1_number_of_bunches");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }

        ss<<"bptx1 nbx: "<<m_bptx1_number_of_bunches.toString()<<" , ";
        if (m_bptx1_number_of_bunches.value_ < 0) {
            ss<<"droping message"<<std::endl;
            LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
            return;
        }
        ;
        try{
            m_bptx2_number_of_bunches = (int)message.extractInt("bptx2_number_of_bunches");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"bptx2 nbx: "<<m_bptx2_number_of_bunches.toString()<<std::endl;
        if (m_bptx2_number_of_bunches.value_ < 0) {
            ss<<"droping message"<<std::endl;
            LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
            return;
        }
        ;
        try{
            bptxpattern_1 = (int*)message.extractIntArray(nbptx_1,"bptx1_bunch_pattern");
            for(int i=0; i<nbptx_1; ++i ){
                //precheck sanity
                if( bptxpattern_1[i]<0 || bptxpattern_1[i]>3564 ){
                    LOG4CPLUS_ERROR( getApplicationLogger(),ss.str() );
                    ss<<"dip/CMS/BRIL/BPTX/Scope-Bunches bptxpattern_1["<<i<<"] OUT OF RANGE";
                    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
                    return;
                }
            }
            for(int i=0; i<nbptx_1; ++i ){
                int bxidx = bptxpattern_1[i]-1;
                if(bptxpattern_1[i]>0){
                    m_bptx1_pattern[bxidx] = 1;
                }
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_DEBUG( getApplicationLogger(),"bptx1: pattern: "+m_bptx1_pattern.toString() );

        try{
            bptxpattern_2 = (int*)message.extractIntArray(nbptx_2,"bptx2_bunch_pattern");
            for(int i=0; i<nbptx_2; ++i ){
                //precheck sanity
                if( bptxpattern_2[i]<0 || bptxpattern_2[i]>3564 ){
                    LOG4CPLUS_ERROR( getApplicationLogger(),ss.str() );
                    ss<<"dip/CMS/BRIL/BPTX/Scope-Bunches bptxpattern_2["<<i<<"] OUT OF RANGE";
                    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
                    return;
                }
            }
            for(int i=0; i<nbptx_2; ++i ){
                int bxidx = bptxpattern_2[i]-1;
                if(bptxpattern_2[i]>0){
                    m_bptx2_pattern[bxidx] = 1;
                }
            }
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        LOG4CPLUS_DEBUG( getApplicationLogger(),"bptx2: pattern: "+m_bptx2_pattern.toString() );
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        //end of dip extraction
        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "bptx_colliding_deltaT";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = colliding_delta_t;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }

    }else if( subname=="dip/CMS/BRIL/BPTX/scaler_rates" ){
        std::stringstream ss;
        int B1_bunches = -1;
        int B2_bunches = -1;
        int B1andB2 = -1;
        int B1notB2 = -1;
        int B1orB2 = -1;
        int B2notB1 = -1;
        std::string fieldname;
        fieldname = "B1";
        try{
            B1_bunches = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B1_bunches<<",";
        fieldname = "B2";
        try{
            B2_bunches = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B2_bunches<<",";
        fieldname = "B1ANDB2";
        try{
            B1andB2 = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B1andB2<<",";
        fieldname = "B1NOTB2";
        try{
            B1notB2 = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B2notB1<<",";

        fieldname = "B1ORB2";
        try{
            B1orB2 = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B1orB2<<",";

        fieldname = "B2NOTB1";
        try{
            B2notB1 = (int)message.extractLong(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<B2notB1;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        //end of dip extraction
        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "B1_bunches";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B1_bunches;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "B2_bunches";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B2_bunches;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "B1andB2";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B1andB2;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "B1notB2";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B1notB2;
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "B1orB2";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B1orB2;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "B2notB1";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Integer* >( isIt->second->find(isvar) )->value_ = B2notB1;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

        }
    }else if( subname=="dip/CMS/LHC/Timing/BPTX" ){
        std::stringstream ss;
        //deltaT
        float deltaT = -0.99;
        try{
            deltaT = message.extractFloat("deltaT");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"deltaT"<<":"<<deltaT;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );

        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "bptx_deltaT";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = deltaT;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }
    }else if( subname=="dip/CMS/LHC/Timing/BPTX1" ){
        std::stringstream ss;
        //Phase
        float phase = -0.99;
        try{
            phase = message.extractFloat("Phase");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"Phase"<<":"<<phase;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "bptx_phase";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = phase;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }

    }else if( subname=="dip/CMS/LHC/BKGD" ){
        std::stringstream ss;
        //bkgd1,2,3
        float bkgd1 = -1;
        float bkgd2 = -1;
        float bkgd3 = -1;
        std::string fieldname;
        fieldname = "BKGD1";
        try{
            bkgd1 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bkgd1<<",";

        fieldname = "BKGD2";
        try{
            bkgd2 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bkgd2<<",";

        fieldname = "BKGD3";
        try{
            bkgd3 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bkgd3;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        //end of dip extraction
        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "bkgd1";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bkgd1;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "bkgd2";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bkgd2;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "bkgd3";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bkgd3;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }
    }else if( subname=="dip/CMS/BRIL/BCMAnalysis/Background3" ){
        std::stringstream ss;
        //bcml1,2plus/minusrs1
        float bcml1_rs1_minus = -1;
        float bcml1_rs1_plus = -1;
        float bcml2_rs1_minus = -1;
        float bcml2_rs1_plus = -1;
        std::string fieldname;
        fieldname = "BCM1LMinusRS1";
        try{
            bcml1_rs1_minus = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bcml1_rs1_minus<<",";

        fieldname = "BCM1LPlusRS1";
        try{
            bcml1_rs1_plus = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bcml1_rs1_plus<<",";

        fieldname = "BCM2LMinusRS1";
        try{
            bcml2_rs1_minus = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bcml2_rs1_minus<<",";

        fieldname = "BCM2LPlusRS1";
        try{
            bcml2_rs1_plus = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<bcml2_rs1_plus;
        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        //end of dip extraction
        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "bcml1_rs1_minus";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bcml1_rs1_minus;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "bcml1_rs1_plus";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bcml1_rs1_plus;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "bcml2_rs1_minus";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bcml2_rs1_minus;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "bcml2_rs1_plus";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = bcml2_rs1_plus;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }
    }else if( subname=="dip/CMS/BRIL/BLMSummary/Histo" ){
        int arraysize = 0;
        float* blmhisto = 0;
        try{
            blmhisto = (float*)message.extractFloatArray(arraysize,"BLMHisto");
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        if( blmhisto ){

            m_blmhisto.clear();
            for( int i=0;i<arraysize;++i ){
                m_blmhisto.push_back( blmhisto[i] );
            }

        }
    }else if( subname=="dip/CMS/BRIL/VacSummary" ){
        std::stringstream ss;
        float vgpb_7_4l5 = 0;
        float vgi_220_1l5 = 0;
        float vgpb_220_1l5 = 0;
        float vgi_183_1l5 = 0;
        float vgpb_183_1l5 = 0;
        float vgpb_148_1l5 = 0;
        float vgpb_147_1l5 = 0;
        float vgpb_147_1r5 = 0;
        float vgpb_148_1r5 = 0;
        float vgi_183_1r5 = 0;
        float vgi_220_1r5 = 0;
        float vgpb_7_4r5 = 0;
        int arraysize = 0;
        float* vacarray = 0;
        std::string fieldname;
        fieldname = "VGPB_7_4L5";
        try{
            vgpb_7_4l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_7_4l5<<",";

        fieldname = "VGI_220_1L5";
        try{
            vgi_220_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgi_220_1l5<<",";

        fieldname = "VGPB_220_1L5";
        try{
            vgpb_220_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_220_1l5<<",";

        fieldname = "VGI_183_1L5";
        try{
            vgi_183_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgi_183_1l5<<",";

        fieldname = "VGPB_183_1L5";
        try{
            vgpb_183_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_183_1l5<<",";

        fieldname = "VGPB_148_1L5";
        try{
            vgpb_148_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_148_1l5<<",";

        fieldname = "VGPB_147_1L5";
        try{
            vgpb_147_1l5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_147_1l5<<",";

        fieldname = "VGPB_147_1R5";
        try{
            vgpb_147_1r5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_147_1r5<<",";

        fieldname = "VGPB_148_1R5";
        try{
            vgpb_148_1r5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_148_1r5<<",";

        fieldname = "VGI_183_1R5";
        try{
            vgi_183_1r5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgi_183_1r5<<",";

        fieldname = "VGI_220_1R5";
        try{
            vgi_220_1r5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgi_220_1r5<<",";

        fieldname = "VGPB_7_4R5";
        try{
            vgpb_7_4r5 = message.extractFloat(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<":"<<vgpb_7_4r5<<",";

        fieldname = "VacArray";
        try{
            vacarray = (float*)message.extractFloatArray(arraysize,fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<fieldname<<" size: "<<arraysize;

        LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
        //end of dip extraction

        std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("brilsummary");
        std::map< std::string,xdata::InfoSpace* >::iterator isItH = m_flashinfostore.find("vachisto");

        std::string isvar;
        if(isIt!=m_flashinfostore.end()){
            isvar = "vgpb_7_4l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_7_4l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
            isvar = "vgi_220_1l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgi_220_1l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_220_1l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_220_1l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgi_183_1l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgi_183_1l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_148_1l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_148_1l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_147_1l5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_147_1l5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_147_1r5";
            if( isIt->second->hasItem(isvar) ){
                ;
                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_147_1r5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_148_1r5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_148_1r5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgi_183_1r5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgi_183_1r5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgi_220_1r5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgi_220_1r5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }

            isvar = "vgpb_7_4r5";
            if( isIt->second->hasItem(isvar) ){

                dynamic_cast<xdata::Float* >( isIt->second->find(isvar) )->value_ = vgpb_7_4r5;

            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
            }
        }

        isvar = "vachisto";
        if( isItH!=m_flashinfostore.end() ){
            if( isItH->second->hasItem(isvar) ){
                xdata::Vector<xdata::Float>*  p_vachisto = dynamic_cast<xdata::Vector<xdata::Float>* >( isItH->second->find(isvar) );
                if( vacarray ){

                    p_vachisto->clear();
                    for( int i=0;i<arraysize;++i ){
                        p_vachisto->push_back( vacarray[i] );
                    }

                }
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isItH->first);
            }
        }


    }//end dip/CMS/VacSummary
    else if( subname=="dip/acc/LHC/Beam/BSRL/Beam1" || subname=="dip/acc/LHC/Beam/BSRL/Beam2" ){
        std::stringstream ss;
        std::string fieldname;
        unsigned int acqStamp = 0;
        float post_fraction_ghost_bunched = 0;
        float post_fraction_sat_bunched = 0;
        float* post_q_bunched = 0;

        int beamidx = 0;

        if( subname=="dip/acc/LHC/Beam/BSRL/Beam2" ){
            beamidx = 1;
        }
        ss<<subname<<" beam"<<beamidx<<" ";

        fieldname = "acqStamp";
        try{
            unsigned long long a = (unsigned long long)message.extractLong(fieldname.c_str());
            acqStamp = a/1000000000;
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"acqStamp "<<acqStamp<<" ";
        fieldname = "post_fraction_ghost_bunched";
        try{
            post_fraction_ghost_bunched = (float)message.extractDouble(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"post_fraction_ghost_bunched "<<post_fraction_ghost_bunched<<" ";
        fieldname = "post_fraction_sat_bunched";
        try{
            post_fraction_sat_bunched = (float)message.extractDouble(fieldname.c_str());
        }catch(const DipException& e){
            LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
        }
        ss<<"post_fraction_sat_bunched "<<post_fraction_sat_bunched<<" ";
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        fieldname = "post_q_bunched";
        int s = (int)message.getValueDimension(fieldname.c_str());
        if( s!=35640 ){
            std::stringstream sss;
            sss<<s;
            LOG4CPLUS_ERROR(getApplicationLogger(),"post_q_bunched has WRONG size "+sss.str());
        }else{
            try{
                post_q_bunched = (float*)message.extractFloatArray(s,fieldname.c_str());
            }catch(const DipException& e){
                LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
            }
        }

        m_bsrl_acqStamp = acqStamp;
        m_bsrl_post_fraction_ghost_bunched[beamidx] = post_fraction_ghost_bunched;
        m_bsrl_post_fraction_sat_bunched[beamidx] = post_fraction_sat_bunched;
        for( int pieceid=0; pieceid<3; ++pieceid) {
            for( int i=0; i<11880; ++i){
                unsigned int myid = pieceid*11880+i;
                m_bsrl_post_q_bunched[beamidx][pieceid][i] = post_q_bunched[myid];
            }
        }
    }// end dip/acc/LHC/Beam/BSRL/Beam1 or 2
    else if(subname=="dip/REMUS/CMS/PMIL5511"){
      std::stringstream ss;
      float remus = 0;
      try{
	remus = (float)message.extractFloat("DR");
      }catch(const DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
      m_remus = remus;
      ss<<"REMUS DR: "<<remus;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    }//end dip/REMUS/CMS/PMIL5511
    else if(subname=="dip/CMS/BRIL/PLT/CAEN/Ivalues"){
      //this dip publication is bad: it fires 6-N times identical values on one go. add protection only assign values when changed
      std::stringstream ss;
      float HmF = 0;
      float HmN = 0;
      float HpF = 0;
      float HpN = 0;
      try{
	HmF = (float)message.extractFloat("PLT_Ana_HmF");	
      }catch(const DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
      if(m_plt_HmF.value_!=HmF){
	m_plt_HmF = HmF;
	ss<<"plt_HmF "<<m_plt_HmF.toString();
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");ss.clear();
      }
      try{
	HmN = (float)message.extractFloat("PLT_Ana_HmN");	
      }catch(const DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }  
      if(m_plt_HmN.value_!=HmN){
	m_plt_HmN =HmN;      
	ss<<"plt_HmN "<<m_plt_HmN.toString();
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");ss.clear();
      }
      try{
	HpF = (float)message.extractFloat("PLT_Ana_HpF");	
      }catch(const DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
      if(m_plt_HpF.value_!=HpF){
	m_plt_HpF =HpF;
	ss<<"plt_HmF "<<m_plt_HmF.toString();
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");ss.clear();
      }
      try{
	HpN = (float)message.extractFloat("PLT_Ana_HpN");
      }catch(const DipException& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
      if(m_plt_HpN.value_!=HpN){
	m_plt_HpN =HpN;
	ss<<"plt_HpN "<<m_plt_HpN.toString();
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");ss.clear();
      }
    }
}//end handleMessage

void CommonDataExchange::updateAllFlashTime(){
    //loop over all requested flashlist and update time related fields if exist
    std::map< std::string,xdata::InfoSpace* >::iterator isIt;
    for( isIt=m_flashinfostore.begin(); isIt!=m_flashinfostore.end(); ++isIt){
        if( isIt->second==0 ) continue;
        if( isIt->second->hasItem("fillnum") ){
            xdata::UnsignedInteger32* p_fillnum = dynamic_cast<xdata::UnsignedInteger32* >( isIt->second->find("fillnum") );
            p_fillnum->value_ = m_fillnum.value_;
        }

        if( isIt->second->hasItem("runnum") ){
            xdata::UnsignedInteger32* p_runnum = dynamic_cast<xdata::UnsignedInteger32* >( isIt->second->find("runnum") );
            p_runnum->value_ = m_runnum.value_;
        }
        if( isIt->second->hasItem("lsnum") ){
            xdata::UnsignedInteger32* p_lsnum = dynamic_cast<xdata::UnsignedInteger32* >( isIt->second->find("lsnum") );
            p_lsnum->value_ = m_lsnum.value_;
        }
        if( isIt->second->hasItem("timestamp") ){
            xdata::TimeVal* p_timestamp = dynamic_cast<xdata::TimeVal* >( isIt->second->find("timestamp") );
            p_timestamp->value_.sec(m_tssec.value_);
            p_timestamp->value_.usec(m_tsmsec.value_*1000);
        }
    }
}
void CommonDataExchange::writeVDMHeader(std::ostream& ss){
    ss<<"fill,run,ls,nb,sec,msec"<<",";
    ss<<"acqflag"<<",";
    ss<<"step"<<",";
    ss<<"beam"<<",";
    ss<<"ip"<<",";
    ss<<"scanstatus"<<",";
    ss<<"plane"<<",";
    ss<<"progress"<<",";
    ss<<"nominal_separation"<<",";
    ss<<"read_nominal_B1sepPlane"<<",";
    ss<<"read_nominal_B1xingPlane"<<",";
    ss<<"read_nominal_B2sepPlane"<<",";
    ss<<"read_nominal_B2xingPlane"<<",";
    ss<<"set_nominal_B1sepPlane"<<",";
    ss<<"set_nominal_B1xingPlane"<<",";
    ss<<"set_nominal_B2sepPlane"<<",";
    ss<<"set_nominal_B2xingPlane"<<",";
    ss<<"bpm_5LDOROS_B1Names"<<",";
    ss<<"bpm_5LDOROS_B1hPos"<<",";
    ss<<"bpm_5LDOROS_B1vPos"<<",";
    ss<<"bpm_5LDOROS_B1hErr"<<",";
    ss<<"bpm_5LDOROS_B1vErr"<<",";
    ss<<"bpm_5RDOROS_B1Names"<<",";
    ss<<"bpm_5RDOROS_B1hPos"<<",";
    ss<<"bpm_5RDOROS_B1vPos"<<",";
    ss<<"bpm_5RDOROS_B1hErr"<<",";
    ss<<"bpm_5RDOROS_B1vErr"<<",";
    ss<<"bpm_5LDOROS_B2Names"<<",";
    ss<<"bpm_5LDOROS_B2hPos"<<",";
    ss<<"bpm_5LDOROS_B2vPos"<<",";
    ss<<"bpm_5LDOROS_B2hErr"<<",";
    ss<<"bpm_5LDOROS_B2vErr"<<",";
    ss<<"bpm_5RDOROS_B2Names"<<",";
    ss<<"bpm_5RDOROS_B2hPos"<<",";
    ss<<"bpm_5RDOROS_B2vPos"<<",";
    ss<<"bpm_5RDOROS_B2hErr"<<",";
    ss<<"bpm_5RDOROS_B2vErr"<<",";
    ss<<"atlas_totInst"<<",";
    ss<<"nominal_separation_plane"<<",";
    ss<<"bstar5"<<",";
    ss<<"ip5xingHmurad";
    ss<<std::endl;
}
void CommonDataExchange::writeCommonDataHeader(std::ostream& ss){
    ss<<"fillnum,runnum,lsnum,nbnum,timestampsec,timestampmsec"<<",";
    ss<<"status"<<",";
    ss<<"egev"<<",";
    ss<<"targetegev"<<",";
    ss<<"bxconfig1"<<",";
    ss<<"bxconfig2"<<",";
    ss<<"intensity1"<<",";
    ss<<"intensity2"<<",";
    ss<<"bxintensity1"<<",";
    ss<<"bxintensity2"<<",";
    ss<<"machinemode"<<",";
    ss<<"fillscheme";
    ss<<std::endl;
}
void CommonDataExchange::writeVDMToCSV(std::ostream& ss){
    ss<<m_fillnum<<","<<m_runnum<<","<<m_lsnum<<","<<m_nbnum<<","<<m_tssec<<","<<m_tsmsec<<",";
    ss<<m_vdm_acqflag.value_<<",";
    ss<<m_vdm_step.value_<<",";
    ss<<m_vdm_beam.value_<<",";
    ss<<m_vdm_ip.value_<<",";
    ss<<m_vdm_scanstatus.value_<<",";
    ss<<m_vdm_plane.value_<<",";
    ss<<m_vdm_progress.value_<<",";
    ss<<m_vdm_nominal_separation.value_<<",";
    ss<<m_vdm_read_nominal_B1sepPlane.value_<<",";
    ss<<m_vdm_read_nominal_B1xingPlane.value_<<",";
    ss<<m_vdm_read_nominal_B2sepPlane.value_<<",";
    ss<<m_vdm_read_nominal_B2xingPlane.value_<<",";
    ss<<m_vdm_set_nominal_B1sepPlane.value_<<",";
    ss<<m_vdm_set_nominal_B1xingPlane.value_<<",";
    ss<<m_vdm_set_nominal_B2sepPlane.value_<<",";
    ss<<m_vdm_set_nominal_B2xingPlane.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B1Names.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B1hPos.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B1vPos.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B1hErr.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B1vErr.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B1Names.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B1hPos.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B1vPos.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B1hErr.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B1vErr.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B2Names.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B2hPos.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B2vPos.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B2hErr.value_<<",";
    ss<<m_vdm_bpm_5LDOROS_B2vErr.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B2Names.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B2hPos.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B2vPos.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B2hErr.value_<<",";
    ss<<m_vdm_bpm_5RDOROS_B2vErr.value_<<",";
    ss<<m_vdm_atlas_totInst.value_<<",";
    ss<<m_vdm_nominal_separation_plane.value_<<",";
    ss<<m_bstar5.value_<<",";
    ss<<m_ip5xingHmurad.value_;
    ss<<std::endl;
}

void CommonDataExchange::writeCommonDataToCSV(std::ostream& ss){
    ss<<m_fillnum<<","<<m_runnum<<","<<m_lsnum<<","<<m_nbnum<<","<<m_tssec<<","<<m_tsmsec<<",";
    ss<<m_beamstatus.value_<<",";
    ss<<m_egev.value_<<",";
    ss<<m_targetegev.value_<<",";
    ss<<m_bxconfig1.toString()<<",";
    ss<<m_bxconfig2.toString()<<",";
    ss<<m_wholeBeamIntensity_1.value_<<",";
    ss<<m_wholeBeamIntensity_2.value_<<",";
    ss<<m_avgBunchIntensities_1.toString()<<",";
    ss<<m_avgBunchIntensities_2.toString()<<",";
    ss<<m_machinemode.value_<<",";
    ss<<m_fillscheme.value_;
    ss<<std::endl;
}
void CommonDataExchange::initMonParams(){
    m_vdm_acqflag = false;
    m_vdm_beam = 0;
    m_vdm_ip = 0;
    m_vdm_step = 0;
    m_vdm_progress = 0;
    m_vdm_nominal_separation = 0;
    m_vdm_read_nominal_B1sepPlane = 0;
    m_vdm_read_nominal_B1xingPlane = 0;
    m_vdm_read_nominal_B2sepPlane = 0;
    m_vdm_read_nominal_B2xingPlane = 0;
    m_vdm_set_nominal_B1sepPlane = 0;
    m_vdm_set_nominal_B1xingPlane = 0;
    m_vdm_set_nominal_B2sepPlane = 0;
    m_vdm_set_nominal_B2xingPlane = 0;

    m_vdm_bpm_5LDOROS_B1hPos = 0;
    m_vdm_bpm_5LDOROS_B1vPos = 0;
    m_vdm_bpm_5LDOROS_B1hErr = 0;
    m_vdm_bpm_5LDOROS_B1vErr = 0;

    m_vdm_bpm_5RDOROS_B1hPos = 0;
    m_vdm_bpm_5RDOROS_B1vPos = 0;
    m_vdm_bpm_5RDOROS_B1hErr = 0;
    m_vdm_bpm_5RDOROS_B1vErr = 0;

    m_vdm_bpm_5LDOROS_B2hPos = 0;
    m_vdm_bpm_5LDOROS_B2vPos = 0;
    m_vdm_bpm_5LDOROS_B2hErr = 0;
    m_vdm_bpm_5LDOROS_B2vErr = 0;

    m_vdm_bpm_5RDOROS_B2hPos = 0;
    m_vdm_bpm_5RDOROS_B2vPos = 0;
    m_vdm_bpm_5RDOROS_B2hErr = 0;
    m_vdm_bpm_5RDOROS_B2vErr = 0;
    m_vdm_atlas_totInst = 0;
    m_bstar5 = 0;
    m_ip5xingHmurad = 0;

    //beam status
    m_beamstatus.fromString("N/A");
    m_targetegev = 0;
    m_egev = 0;
    m_nfilledBunches_1 = 0;
    m_nfilledBunches_2 = 0;

    for(int i=0; i<maxnbx; ++i){
        m_bxconfig1.push_back(0);
        m_bxconfig2.push_back(0);
        m_bptx1_pattern.push_back(0);
        m_bptx2_pattern.push_back(0);
        m_avgBunchIntensities_1.push_back(0);
        m_avgBunchIntensities_2.push_back(0);
        m_atlasBunchIntensities_1.push_back(0);
        m_atlasBunchIntensities_2.push_back(0);
        m_bunchlength_1.push_back(0);
        m_bunchlength_2.push_back(0);
    }

    //beam intensities
    m_wholeBeamIntensity_1 = 0;
    m_wholeBeamIntensity_2 = 0;
    m_atlasBeamIntensity_1 = 0;
    m_atlasBeamIntensity_2 = 0;

    //atlas beam intensities
    m_atlasBeamIntensity_1 = 0;
    m_atlasBeamIntensity_2 = 0;

    // Luminous Region
    m_luminous_region_tilt.resize(2, -1.0);
    m_luminous_region_centroid.resize(3, -1.0);
    m_luminous_region_size.resize(3, -1.0);

    //bunch lengths
    m_bunchlengthmean_1 = 0;
    m_bunchlengthmean_2 = 0;

    //bptx
    m_bptx1_number_of_bunches = -1;
    m_bptx2_number_of_bunches = -1;

}

void CommonDataExchange::do_publish_bunchlength(){
    std::stringstream ss;
    ss<<"Publishing bunchlength to "<<m_bus.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");
    if( m_dipsubs.find( "dip/acc/LHC/Beam/BunchLengths/Beam1")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/BunchLengths/Beam2")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/acc/LHC/Beam/BunchLengths/Beam*. No data to publish for topic bunchlength");
        return;
    }else if( m_bunchlength_1.elements()==0 && m_bunchlength_2.elements()==0 ){
        LOG4CPLUS_ERROR(getApplicationLogger(),"Empty data for topic bunchlength");
        return;
    }

    std::string topicname = interface::bril::bunchlengthT::topicname();
    toolbox::mem::Reference* bufRef=0;
    try{
        std::string pdict = interface::bril::bunchlengthT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        size_t totalsize = interface::bril::bunchlengthT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);
        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.insert_field( header->payloadanchor, "b1mean", &m_bunchlengthmean_1.value_ );
        tc.insert_field( header->payloadanchor, "b2mean", &m_bunchlengthmean_2.value_ );
        ss<<"Published bunchlengthmean_1 "<<m_bunchlengthmean_1.toString()<<", bunchlengthmean_2 "<<m_bunchlengthmean_1.toString();
        float b1length[maxnbx];
        float b2length[maxnbx];
        for( int i=0;i<maxnbx;++i ){
            b1length[i] = m_bunchlength_1[i];
            b2length[i] = m_bunchlength_2[i];
        }
        tc.insert_field( header->payloadanchor, "b1length", b1length);
        tc.insert_field( header->payloadanchor, "b2length", b2length );

        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
    }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(bril::dip::exception::CommonDataExchange,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}

void CommonDataExchange::do_publish_beam(){
    std::stringstream ss;
    ss<<"Publishing beam to "<<m_bus.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");
    if( m_dipsubs.find( "dip/acc/LHC/RunControl/BeamMode")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/IntensityPerBunch/Beam1")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/IntensityPerBunch/Beam2")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/Energy")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/RunControl/RunConfiguration")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/RunControl/MachineMode")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/Intensity24Bits/Beam1")==m_dipsubs.end() ||  m_dipsubs.find( "dip/acc/LHC/Beam/Intensity24Bits/Beam2")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/acc/LHC/RunControl/BeamMode, dip/acc/LHC/Beam/IntensityPerBunch/Beam*, dip/acc/LHC/Beam/Intensity24Bits/Beam*, dip/acc/LHC/Beam/Energy, dip/acc/LHC/RunControl/RunConfiguration, dip/acc/LHC/RunControl/MachineMode. No data to publish for topic beam ");
        return;
    }else if( m_avgBunchIntensities_1.elements()==0 || m_avgBunchIntensities_2.elements()==0 ){
        LOG4CPLUS_ERROR(getApplicationLogger(),"Empty data for topic beam");
        return;
    }

    std::string topicname = interface::bril::beamT::topicname();
    toolbox::mem::Reference* bufRef=0;
    try{
        std::string pdict = interface::bril::beamT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if( m_beamstatus.value_!="STABLE BEAMS"&&m_beamstatus.value_!="ADJUST"&&m_beamstatus.value_!="SQUEEZE"&&m_beamstatus.value_!="FLAT TOP" ){
            if( !m_vdm_acqflag.value_ ){
                plist.setProperty("NOSTORE","1");
                ss<<"NOSTORE beam status "<<m_beamstatus.value_ ;
                LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
                ss.str("");
            }
        }
        size_t totalsize = interface::bril::beamT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);
        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.insert_field( header->payloadanchor, "status", m_beamstatus.value_.c_str() );
        tc.insert_field( header->payloadanchor, "egev", &m_egev.value_);
        tc.insert_field( header->payloadanchor, "targetegev", &m_targetegev.value_);
        bool bxconfig1[maxnbx];
        bool bxconfig2[maxnbx];
        bool collidable[maxnbx];
        float avgbxintensity1[maxnbx];
        float avgbxintensity2[maxnbx];
        int ncollidable = 0;
        for( int i=0; i<maxnbx; ++i ){
            bxconfig1[i] = (bool)m_bxconfig1[i];
            bxconfig2[i] = (bool)m_bxconfig2[i];
            collidable[i] = bxconfig1[i] && bxconfig2[i];
            if(collidable[i]) ++ncollidable;
            avgbxintensity1[i] = m_avgBunchIntensities_1[i];
            avgbxintensity2[i] = m_avgBunchIntensities_2[i];
        }

        tc.insert_field( header->payloadanchor, "bxconfig1", bxconfig1 );
        tc.insert_field( header->payloadanchor, "bxconfig2", bxconfig2 );
        tc.insert_field( header->payloadanchor, "intensity1", &m_wholeBeamIntensity_1.value_ );
        tc.insert_field( header->payloadanchor, "intensity2", &m_wholeBeamIntensity_2.value_ );
        tc.insert_field( header->payloadanchor, "bxintensity1", avgbxintensity1 );
        tc.insert_field( header->payloadanchor, "bxintensity2", avgbxintensity2 );
        tc.insert_field( header->payloadanchor, "machinemode", m_machinemode.value_.c_str() );
        tc.insert_field( header->payloadanchor, "fillscheme", m_fillscheme.value_.c_str());
        tc.insert_field( header->payloadanchor, "ncollidable", &ncollidable );
        tc.insert_field( header->payloadanchor, "collidable", collidable );

        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);

        ss<<"Published lhc_bxconfig ncollidable "<<ncollidable;
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());

    }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }

}

void CommonDataExchange::do_publish_atlasbeam(){
    std::stringstream ss;
    ss<<"Publishing atlasbeam to "<<m_bus.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");
    if( m_dipsubs.find( "dip/TNproxy/ATLAS/LHC/BPTX1/IntensitiesPerBunch")==m_dipsubs.end() || m_dipsubs.find( "dip/TNproxy/ATLAS/LHC/BPTX2/IntensitiesPerBunch")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to ip/TNproxy/ATLAS/LHC/BPTX*/IntensitiesPerBunch. No data to publish for topic atlasbeam ");
        return;
    }else if( m_atlasBunchIntensities_1.elements()==0 && m_atlasBunchIntensities_2.elements()==0 ){
        LOG4CPLUS_ERROR(getApplicationLogger(),"Empty data for topic atlasbeam");
        return;
    }

    std::string topicname = interface::bril::atlasbeamT::topicname();
    toolbox::mem::Reference* bufRef=0;
    try{
        std::string pdict = interface::bril::atlasbeamT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        size_t totalsize = interface::bril::atlasbeamT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);

        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.insert_field( header->payloadanchor, "intensity1", &m_atlasBeamIntensity_1.value_ );
        tc.insert_field( header->payloadanchor, "intensity2", &m_atlasBeamIntensity_2.value_ );

        float bxintensity1[maxnbx];
        float bxintensity2[maxnbx];
        for( int i=0; i<maxnbx; ++i ){
            bxintensity1[i]=m_atlasBunchIntensities_1[i];
            bxintensity2[i]=m_atlasBunchIntensities_2[i];
        }
        tc.insert_field( header->payloadanchor, "bxintensity1", bxintensity1 );
        tc.insert_field( header->payloadanchor, "bxintensity2", bxintensity2 );
        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}

void CommonDataExchange::do_publish_vdmflag(){
    std::stringstream msg;
    std::string topicname(interface::bril::vdmflagT::topicname());
    toolbox::mem::Reference* bufRef=0;
    try{
        msg<<"Publishing "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
        msg.str("");
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        size_t totalsize = interface::bril::vdmflagT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);
        ((interface::bril::vdmflagT*)buf)->payload()[0]=m_vdm_acqflag.value_;
        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
    }catch(xcept::Exception& e){
        msg<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,msg.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}

void CommonDataExchange::do_publish_vdmscan(){
    std::stringstream ss;
    ss<<"Publishing vdmscan to "<<m_bus.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");
    if( m_dipsubs.find( "dip/acc/LHC/Beam/AutomaticScanIP5")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/acc/LHC/Beam/AutomaticScanIP5. No data to publish for topic vdmscan.");
        return;
    }
    std::string topicname(interface::bril::vdmscanT::topicname());
    toolbox::mem::Reference* bufRef=0;
    try{
        std::string pdict = interface::bril::vdmscanT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if (!m_vdm_acqflag.value_){
            plist.setProperty("NOSTORE","1");
        }
        size_t totalsize = interface::bril::vdmscanT::maxsize();
        bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
        bufRef->setDataSize(totalsize);
        unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
        interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
        header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
        header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
        header->setFrequency(4);
        header->setTotalsize(totalsize);

        interface::bril::shared::CompoundDataStreamer tc(pdict);
        tc.insert_field(header->payloadanchor, "step", &m_vdm_step.value_);
        tc.insert_field(header->payloadanchor, "progress", &m_vdm_progress.value_);
        tc.insert_field(header->payloadanchor, "beam", &m_vdm_beam.value_);
        tc.insert_field(header->payloadanchor, "ip", &m_vdm_ip.value_);
        tc.insert_field(header->payloadanchor, "plane", m_vdm_plane.value_.c_str());
        tc.insert_field(header->payloadanchor, "nominal_sep_plane", m_vdm_nominal_separation_plane.value_.c_str());
        tc.insert_field(header->payloadanchor, "stat", m_vdm_scanstatus.value_.c_str());
        tc.insert_field(header->payloadanchor, "sep", &m_vdm_nominal_separation.value_);
        tc.insert_field(header->payloadanchor, "r_sepP1", &m_vdm_read_nominal_B1sepPlane.value_);
        tc.insert_field(header->payloadanchor, "r_xingP1", &m_vdm_read_nominal_B1xingPlane.value_);
        tc.insert_field(header->payloadanchor, "r_sepP2", &m_vdm_read_nominal_B2sepPlane.value_);
        tc.insert_field(header->payloadanchor, "r_xingP2", &m_vdm_read_nominal_B2xingPlane.value_);
        tc.insert_field(header->payloadanchor, "s_sepP1", &m_vdm_set_nominal_B1sepPlane.value_);
        tc.insert_field(header->payloadanchor, "s_xingP1", &m_vdm_set_nominal_B1xingPlane.value_);
        tc.insert_field(header->payloadanchor, "s_sepP2", &m_vdm_set_nominal_B2sepPlane.value_);
        tc.insert_field(header->payloadanchor, "s_xingP2", &m_vdm_set_nominal_B2xingPlane.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b1names", m_vdm_bpm_5LDOROS_B1Names.value_.c_str());
        tc.insert_field(header->payloadanchor, "5ldoros_b1hpos", &m_vdm_bpm_5LDOROS_B1hPos.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b1vpos",&m_vdm_bpm_5LDOROS_B1vPos.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b1herr",&m_vdm_bpm_5LDOROS_B1hErr.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b1verr",&m_vdm_bpm_5LDOROS_B1vErr.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b1names",m_vdm_bpm_5RDOROS_B1Names.value_.c_str());
        tc.insert_field(header->payloadanchor, "5rdoros_b1hpos", &m_vdm_bpm_5RDOROS_B1hPos.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b1vpos", &m_vdm_bpm_5RDOROS_B1vPos.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b1herr", &m_vdm_bpm_5RDOROS_B1hErr.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b1verr", &m_vdm_bpm_5RDOROS_B1vErr.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b2names",m_vdm_bpm_5LDOROS_B2Names.value_.c_str() );
        tc.insert_field(header->payloadanchor, "5ldoros_b2hpos", &m_vdm_bpm_5LDOROS_B2hPos.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b2vpos", &m_vdm_bpm_5LDOROS_B2vPos.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b2herr", &m_vdm_bpm_5LDOROS_B2hErr.value_);
        tc.insert_field(header->payloadanchor, "5ldoros_b2verr", &m_vdm_bpm_5LDOROS_B2vErr.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b2names", m_vdm_bpm_5RDOROS_B2Names.value_.c_str());
        tc.insert_field(header->payloadanchor, "5rdoros_b2hpos", &m_vdm_bpm_5RDOROS_B2hPos.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b2vpos", &m_vdm_bpm_5RDOROS_B2vPos.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b2herr", &m_vdm_bpm_5RDOROS_B2hErr.value_);
        tc.insert_field(header->payloadanchor, "5rdoros_b2verr", &m_vdm_bpm_5RDOROS_B2vErr.value_);
        if( pdict.find("bstar")!=std::string::npos ){
            tc.insert_field(header->payloadanchor, "bstar5", &m_bstar5.value_ );
        }
        if( pdict.find("Hmurad")!=std::string::npos ){
            tc.insert_field(header->payloadanchor, "xingHmurad", &m_ip5xingHmurad.value_ );
        }
        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
    }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){ bufRef->release(); bufRef = 0; }
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}
void CommonDataExchange::do_publish_bsrlfracBunched(){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_bsrlfracBunched");
    if(!m_bsrlfracBunched_canpublish) return;
    if( m_dipsubs.find( "dip/acc/LHC/Beam/BSRL/Beam1")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/BSRL/Beam2" )==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/AutomaticScanIP5")==m_dipsubs.end() ){
        LOG4CPLUS_ERROR(getApplicationLogger(),"Not subscribed to dip/acc/LHC/Beam/BSRL or dip/acc/LHC/Beam/AutomaticScanIP5. No data to publish for bsrlfracBunched.");
        return;
    }
    std::stringstream ss;
    ss<<"do_publish_bsrlfracBunched publishing bsrlfracBunched to "<<m_bus.value_<<" "<<m_bsrl_post_fraction_ghost_bunched[0]<<" "<<m_bsrl_post_fraction_ghost_bunched[1]<<" "<<m_bsrl_post_fraction_sat_bunched[0]<<" "<<m_bsrl_post_fraction_sat_bunched[1];
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    ss.clear();ss.str("");

    std::string topicname(interface::bril::bsrlfracBunchedT::topicname());
    toolbox::mem::Reference* bufRef=0;

    std::string pdict = interface::bril::bsrlfracBunchedT::payloaddict();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);

    size_t totalsize = interface::bril::bsrlfracBunchedT::maxsize();
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
    header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_bsrl_acqStamp,0);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(300);
    header->setTotalsize(totalsize);

    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field(header->payloadanchor, "postfracghostBunched", m_bsrl_post_fraction_ghost_bunched );
    tc.insert_field(header->payloadanchor, "postfracsatBunched", m_bsrl_post_fraction_sat_bunched );
    try{
        this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
    }catch(xcept::Exception& e){
        ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef)  bufRef->release();
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
    m_bsrlfracBunched_canpublish = false;
}

void CommonDataExchange::do_publish_bsrlqBunched(){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_bsrlqBunched");
    if(!m_bsrlqBunched_canpublish) return;
    if( m_dipsubs.find( "dip/acc/LHC/Beam/BSRL/Beam1")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/BSRL/Beam2")==m_dipsubs.end() || m_dipsubs.find( "dip/acc/LHC/Beam/AutomaticScanIP5")==m_dipsubs.end()){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/acc/LHC/Beam/BSRL/Beam1,2 or dip/acc/LHC/Beam/AutomaticScanIP5. No data to publish for bsrlfracBunched.");
        return;
    }
    std::stringstream ss;
    ss<<"do_publish_bsrlqBunched publishing bsrlqBunched to "<<m_bus.value_;
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    ss.clear();ss.str("");

    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    std::string topicname = interface::bril::bsrlqBunchedT::topicname();
    toolbox::mem::Reference* qbufRef=0;

    for(int beamidx=0; beamidx<2; ++beamidx){
        for(int pieceid=0; pieceid<3; ++pieceid){
            size_t qsize = interface::bril::bsrlqBunchedT::maxsize();
            qbufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,qsize);
            qbufRef->setDataSize(qsize);
            //unsigned char* qbuf = (unsigned char*)(qbufRef->getDataLocation());
            interface::bril::shared::DatumHead* qheader = (interface::bril::shared::DatumHead*)(qbufRef->getDataLocation());
            qheader->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_bsrl_acqStamp,0);
            int ichannel = (beamidx+1)*10+(pieceid+1);
            qheader->setResource(interface::bril::shared::DataSource::DIP,0,ichannel,interface::bril::shared::StorageType::FLOAT);
            qheader->setFrequency(300);
            qheader->setTotalsize(qsize);
            interface::bril::bsrlqBunchedT* fullobj = (interface::bril::bsrlqBunchedT*)(qbufRef->getDataLocation());
            for(int i=0; i<11880; ++i){
                fullobj->payload()[i] = m_bsrl_post_q_bunched[beamidx][pieceid][i];
            }
            try{
                this->getEventingBus(m_bus.value_).publish(topicname,qbufRef,plist);
            }catch(xcept::Exception& e){
                ss<<"Failed to publish "<<topicname<<" beam "<<beamidx+1<<" piece "<<pieceid+1<<" to "<<m_bus.value_;
                LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
                if(qbufRef) qbufRef->release();
                XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
                this->notifyQualified("fatal",myerrorobj);
            }
        }
    }
    m_bsrlqBunched_canpublish = false;
}

void CommonDataExchange::do_publish_luminousregion(){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering do_publish_luminousregion");
  if( m_dipsubs.find( "dip/CMS/LHC/LuminousRegion")==m_dipsubs.end() ){
    LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/CMS/LHC/LuminousRegion, no data for luminousregion ");
    return;
  } 
  std::stringstream ss;
  toolbox::mem::Reference* bufRef=0;
  std::string topicname = interface::bril::luminousregionT::topicname();
  std::string pdict = interface::bril::luminousregionT::payloaddict();
  size_t totalsize = interface::bril::luminousregionT::maxsize();
  
  ss<<"Publishing "<<topicname<<" to "<<m_bus.value_;
  LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
  ss.clear();ss.str("");
  
  xdata::Properties plist;
  plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
  plist.setProperty("PAYLOAD_DICT",pdict);
  bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
  bufRef->setDataSize(totalsize);
  unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
  interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
  header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
  header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
  header->setFrequency(10);
  header->setTotalsize(totalsize);
  interface::bril::shared::CompoundDataStreamer tc(pdict);
  float l_centroid[3];
  float l_size[3];
  float l_tilt[2];
  for( size_t i=0; i<3; ++i){
    l_centroid[i] = m_luminous_region_centroid[i].value_;
    l_size[i] = m_luminous_region_size[i].value_;
  }
  for( size_t i=0; i<2; ++i ){
    l_tilt[i] = m_luminous_region_tilt[i].value_;
  }

  tc.insert_field( header->payloadanchor, "centroid", l_centroid );
  tc.insert_field( header->payloadanchor, "size", l_size );
  tc.insert_field( header->payloadanchor, "tilt", l_tilt );
  try{
    this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef)  bufRef->release();
    XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }
}
void CommonDataExchange::do_publish_bxconfig(){
    std::stringstream ss;
    toolbox::mem::Reference* bufRef=0;
    std::string topicname = interface::bril::bxconfigT::topicname();
    std::string pdict = interface::bril::bxconfigT::payloaddict();
    size_t totalsize = interface::bril::bxconfigT::maxsize();
    interface::bril::shared::CompoundDataStreamer tc(pdict);

    if( m_dipsubs.find( "dip/acc/LHC/Beam/IntensityPerBunch/Beam1")==m_dipsubs.end() || m_dipsubs.find("dip/acc/LHC/Beam/IntensityPerBunch/Beam2")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/acc/LHC/Beam/IntensityPerBunch/Beam*, no data for fbct bxconfig ");
    }else if( m_bxconfig1.elements()==0 && m_bxconfig2.elements()==0 ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Empty data for fbct bxconfig ");
    }else{
        try{
            ss<<"Publishing fbct "<<topicname<<" to "<<m_bus.value_;
            LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
            ss.str("");
            xdata::Properties plist;
            plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
            plist.setProperty("PAYLOAD_DICT",pdict);
            bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
            bufRef->setDataSize(totalsize);
            unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
            interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
            header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
            header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
            header->setFrequency(4);
            header->setTotalsize(totalsize);
            bool fbct_bxconfig1[maxnbx];
            bool fbct_bxconfig2[maxnbx];
            int fbct_ncollidable = 0;
            for( int i=0; i<maxnbx; ++i ){
                fbct_bxconfig1[i] = false;
                fbct_bxconfig2[i] = false;
                if( m_bxconfig1.elementAt(i) ){
                    fbct_bxconfig1[i] = (bool)m_bxconfig1[i];
                }
                if( m_bxconfig2.elementAt(i) ){
                    fbct_bxconfig2[i] = (bool)m_bxconfig2[i];
                }
                if( fbct_bxconfig1[i] && fbct_bxconfig2[i] ) ++fbct_ncollidable;
            }
            tc.insert_field( header->payloadanchor, "beam1", fbct_bxconfig1 );
            tc.insert_field( header->payloadanchor, "beam2", fbct_bxconfig2 );

            tc.insert_field( header->payloadanchor, "datasource", &interface::bril::bxconfig_fbct );
            this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
            ss<<"Published fbct bxconfig ncollidable "<<fbct_ncollidable;
            LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
            ss.str("");
        }catch(xcept::Exception& e){
            ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
            LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
            if(bufRef){ bufRef->release(); bufRef = 0; }
            XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
            this->notifyQualified("fatal",myerrorobj);
        }
    }

    if( m_dipsubs.find("dip/CMS/BRIL/BPTX/Scope-Bunches")==m_dipsubs.end() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/CMS/BRIL/BPTX/Scope-Bunches, no data for bptx_scope bxconfig ");
    }else if( m_bptx1_pattern.elements()==0 && m_bptx2_pattern.elements() ){
        LOG4CPLUS_INFO(getApplicationLogger(),"Empty data for bptx_scope bxconfig ");
    } else{
        try{
            ss<<"Publishing bptx_scope "<<topicname<<" to "<<m_bus.value_;
            LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
            ss.str("");
            xdata::Properties plist;
            plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
            plist.setProperty("PAYLOAD_DICT",pdict);
            bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
            bufRef->setDataSize(totalsize);
            unsigned char* buf = (unsigned char*)(bufRef->getDataLocation());
            interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)buf;
            header->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
            header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
            header->setFrequency(4);
            header->setTotalsize(totalsize);

            bool scope_bxconfig1[maxnbx];
            bool scope_bxconfig2[maxnbx];
            int scope_ncollidable = 0;
            for( int i=0; i<maxnbx; ++i ){
                scope_bxconfig1[i] = false;
                scope_bxconfig2[i] = false;
                if( m_bptx1_pattern.elementAt(i) ){
                    scope_bxconfig1[i] = (bool)m_bptx1_pattern[i];
                }
                if( m_bptx2_pattern.elementAt(i) ){
                    scope_bxconfig2[i] = (bool)m_bptx2_pattern[i];
                }
                if( scope_bxconfig1[i] && scope_bxconfig2[i] ) ++scope_ncollidable;
            }
            tc.insert_field( header->payloadanchor, "beam1", scope_bxconfig1 );
            tc.insert_field( header->payloadanchor, "beam2", scope_bxconfig2 );
            tc.insert_field( header->payloadanchor, "datasource", &interface::bril::bxconfig_bptx_scope );
            this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
            ss<<"Published bptx_scope bxconfig"<<" ncollidable "<<scope_ncollidable;
            LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
            ss.str("");
        }catch(xcept::Exception& e){
            ss<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
            LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
            if(bufRef){ bufRef->release(); bufRef = 0; }
            XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,ss.str(),e);
            this->notifyQualified("fatal",myerrorobj);
        }
    }

}

void CommonDataExchange::do_publish_remus(){
  if( m_dipsubs.find( "dip/REMUS/CMS/PMIL5511")== m_dipsubs.end() ){
    LOG4CPLUS_INFO(getApplicationLogger(),"Not subscribed to dip/REMUS/CMS/PMIL5511, no data for remuslumi ");
    return;
  }
  std::stringstream msg;
  std::string topicname(interface::bril::remuslumiT::topicname());
  toolbox::mem::Reference* bufRef=0;
  try{
    msg<<"Publishing "<<topicname<<" to "<<m_bus.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
    msg.str("");
    std::string dictstr = interface::bril::remuslumiT::payloaddict();//lumi topic dict is the same for all type
    size_t payloadsize = interface::bril::remuslumiT::maxsize();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty( "PAYLOAD_DICT", dictstr );
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,payloadsize);
    bufRef->setDataSize(payloadsize);
    interface::bril::remuslumiT* lumi_payload = (interface::bril::remuslumiT*)(bufRef->getDataLocation() );
    lumi_payload->setTime(m_fillnum.value_,m_runnum.value_,m_lsnum.value_,m_nbnum.value_,m_tssec.value_,m_tsmsec.value_);
    lumi_payload->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
    lumi_payload->setFrequency(4);
    lumi_payload->setTotalsize(payloadsize);
    interface::bril::shared::CompoundDataStreamer cds( dictstr );
    std::string calibtag("1");
    cds.insert_field(lumi_payload->payloadanchor,"calibtag", calibtag.c_str() );
    cds.insert_field(lumi_payload->payloadanchor,"avgraw", &m_remus );
    cds.insert_field(lumi_payload->payloadanchor,"avg", &m_remus );
    this->getEventingBus(m_bus.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<m_bus.value_;
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){ bufRef->release(); bufRef = 0; }
    XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,msg.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }
}

void CommonDataExchange::init_flashlist(xdata::InfoSpace* is,const std::string& tname){
    if( tname=="brilsummary" ){
      init_brilsummaryFlash(is);
    }else if( tname=="beam" ){
      init_beamFlash(is);
    }else if( tname=="vachisto" ){
      init_vachistoFlash(is);
    }else if( tname=="blmhisto" ){
      init_blmhistoFlash(is);
    }else if( tname=="pltcurrents" ){
      init_pltcurrentsFlash(is);
    }
}
void CommonDataExchange::init_brilsummaryFlash(xdata::InfoSpace* is){
    //fire per nb4
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "bkgd1";
    fieldvalues.push_back( new xdata::Float(-1) );
    fieldnames.push_back( fieldname );

    fieldname = "bkgd2";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bkgd3";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bcml1_rs1_minus";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bcml1_rs1_plus";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bcml2_rs1_minus";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bcml2_rs1_plus";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B1_bunches";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B2_bunches";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B1andB2";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B1notB2";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B1orB2";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "B2notB1";
    fieldvalues.push_back( new xdata::Integer(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "bptx_deltaT";
    fieldvalues.push_back( new xdata::Float(-0.99)  );
    fieldnames.push_back( fieldname );

    fieldname = "bptx_phase";
    fieldvalues.push_back( new xdata::Float(-0.99)  );
    fieldnames.push_back( fieldname );

    fieldname = "bptx_colliding_deltaT";
    fieldvalues.push_back( new xdata::Float(-0.99)  );
    fieldnames.push_back( fieldname );

    fieldname = "beam1_intensity";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "beam2_intensity";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_7_4l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgi_220_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_220_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgi_183_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_183_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_148_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_147_1l5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_147_1r5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_148_1r5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgi_183_1r5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgi_220_1r5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "vgpb_7_4r5";
    fieldvalues.push_back( new xdata::Float(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
    m_fastflashLists.push_back("brilsummary");
    
}


void CommonDataExchange::init_pltcurrentsFlash(xdata::InfoSpace* is){
    //fire per nb4
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "PLT_HmF";
    fieldvalues.push_back( new xdata::Float(-1) );
    fieldnames.push_back( fieldname );

    fieldname = "PLT_HmN";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "PLT_HpF";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "PLT_HpN";
    fieldvalues.push_back( new xdata::Float(-1)  );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
    m_fastflashLists.push_back("pltcurrents");    
}

void CommonDataExchange::init_beamFlash(xdata::InfoSpace* is){
    //fire per ls
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "beamstatus";
    fieldvalues.push_back( new xdata::String("")  );
    fieldnames.push_back( fieldname );

    fieldname = "fillscheme";
    fieldvalues.push_back( new xdata::String("")  );
    fieldnames.push_back( fieldname );

    fieldname = "beamegev";
    fieldvalues.push_back( new xdata::Float(0) );
    fieldnames.push_back( fieldname );

    fieldname = "targetegev";
    fieldvalues.push_back( new xdata::Integer(0)  );
    fieldnames.push_back( fieldname );

    fieldname = "amodetag";
    fieldvalues.push_back( new xdata::String("") );
    fieldnames.push_back( fieldname );

    fieldname = "beam1_bx_intensity";
    fieldvalues.push_back( new xdata::Vector<xdata::Float>()  );
    fieldnames.push_back( fieldname );

    fieldname = "beam2_bx_intensity";
    fieldvalues.push_back( new xdata::Vector<xdata::Float>()  );
    fieldnames.push_back( fieldname );

    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
    m_slowflashLists.push_back("beam");
}

void CommonDataExchange::init_vachistoFlash(xdata::InfoSpace* is){
    //fire per ls
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "vachisto";
    fieldvalues.push_back( new xdata::Vector<xdata::Float>()  );
    fieldnames.push_back( fieldname );

    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
    m_slowflashLists.push_back("vachisto");
}

void CommonDataExchange::init_blmhistoFlash(xdata::InfoSpace* is){
    //fire per ls
    std::vector< xdata::Serializable*> fieldvalues;
    std::list< std::string > fieldnames;
    std::string fieldname;

    fieldname = "blmhisto";
    fieldvalues.push_back( new xdata::Vector<xdata::Float>()  );
    fieldnames.push_back( fieldname );

    fieldname = "fillnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "runnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "lsnum";
    fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
    fieldnames.push_back( fieldname );

    fieldname = "timestamp";
    fieldvalues.push_back( new xdata::TimeVal(toolbox::TimeVal()) );
    fieldnames.push_back( fieldname );

    m_flashfields.insert( std::make_pair(is,fieldnames) );
    m_flashcontent.insert( std::make_pair(is,fieldvalues) );
    m_slowflashLists.push_back("blmhisto");
}

bool CommonDataExchange::fireslowflashlists(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering fireslowflashlists");
    usleep(23310000);
    for(std::vector<std::string>::iterator it=m_slowflashLists.begin();it!=m_slowflashLists.end();++it){
        //some flashlist variables are updated at firing time from cached values
        {// start guard

#ifdef __DEBUG__
            VerboseGuard<toolbox::BSem> g("fireslowflashlist "+*it,m_applock);
#else
            toolbox::task::Guard<toolbox::BSem> g(m_applock);
#endif

            if( (*it)=="beam" ){
                build_beamFlashList();
            }else if( (*it)=="blmhisto" ){
                build_blmhistoFlashList();
            }
        }//end guard
        fireFlashlist(*it);
    }
    return true;
}
void CommonDataExchange::fireFlashlist(const std::string& flashname){
    std::map< std::string,xdata::InfoSpace* >::iterator it=m_flashinfostore.find(flashname);
    try{
        if( it!=m_flashinfostore.end() ){
            xdata::InfoSpace* is = it->second;
            if(is){
                std::map< xdata::InfoSpace*, std::list<std::string> >::iterator fit = m_flashfields.find(is);
                if( fit!=m_flashfields.end() ){
                    is->fireItemGroupChanged(fit->second,this);
                }
            }else{
                LOG4CPLUS_ERROR(getApplicationLogger(),"");
                XCEPT_DECLARE(bril::dip::exception::Exception,myerrorobj,it->first+" flashlist is not associated to an infospace");
                notifyQualified("error",myerrorobj);
            }
        }
    }catch(xdata::exception::Exception& e){
        std::string msg("Failed to fire flashlist for topic "+flashname);
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,msg,e);
        notifyQualified("error",myerrorobj);
    }
}

void CommonDataExchange::declareFlashLists(){
    std::map< std::string,xdata::InfoSpace* >::iterator it=m_flashinfostore.begin();
    try{
        for(; it!=m_flashinfostore.end(); ++it ){ //loop over flashlists
            xdata::InfoSpace* is = it->second;
            std::map< xdata::InfoSpace*, std::list<std::string> >::iterator nameit=m_flashfields.find(is);
            std::map< xdata::InfoSpace*, std::vector<xdata::Serializable*> >::iterator valueit=m_flashcontent.find(is);
            if(nameit!=m_flashfields.end()&&valueit!=m_flashcontent.end() ){
                std::list<std::string>::const_iterator nIt = nameit->second.begin();
                std::vector<xdata::Serializable*>::const_iterator vIt = valueit->second.begin();
                for(; nIt!=nameit->second.end() && vIt!=valueit->second.end(); ++nIt, ++vIt){
                    is->fireItemAvailable(*nIt,*vIt);
                }
            }
        }
    }catch(xdata::exception::Exception& e){
        std::string msg( "Failed to declare flashlist "+it->first );
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
        XCEPT_DECLARE_NESTED(bril::dip::exception::Exception,myerrorobj,msg,e);
        notifyQualified("error",myerrorobj);
    }
}

void CommonDataExchange::cleanFlashLists(){
    std::map< std::string,xdata::InfoSpace* >::iterator it = m_flashinfostore.begin();
    for(; it!=m_flashinfostore.end(); ++it ){ //loop over flashlists
        xdata::InfoSpace* is = it->second;
        std::map< xdata::InfoSpace*, std::vector<xdata::Serializable*> >::iterator valueit=m_flashcontent.find(is);
        if(valueit!=m_flashcontent.end() ){
            std::vector<xdata::Serializable*>::const_iterator vIt = valueit->second.begin();
            for(; vIt!=valueit->second.end(); ++vIt){
                delete *vIt;
            }
        }
    }
}

bool CommonDataExchange::firefastflashlists(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering firefastflashlists");
    usleep(1500000);
    for(std::vector<std::string>::iterator it=m_fastflashLists.begin();it!=m_fastflashLists.end();++it){
      {// start guard
#ifdef __DEBUG__
	VerboseGuard<toolbox::BSem> g("firefastflashlist "+*it,m_applock);
#else
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
#endif
	if( (*it)=="pltcurrents" ){
	  build_pltcurrentsFlashList();
	}
      }//end guard
      fireFlashlist(*it);	
    }
    return true;
}

void CommonDataExchange::build_pltcurrentsFlashList(){
   std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("pltcurrents");
   std::string isvar;
   if(isIt!=m_flashinfostore.end()){
     isvar = "PLT_HmF";
     if( isIt->second->hasItem(isvar) ){
       dynamic_cast<xdata::Float*>( isIt->second->find(isvar) )->value_ = m_plt_HmF.value_;
     }else{
       LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
     }
     isvar = "PLT_HmN";
     if( isIt->second->hasItem(isvar) ){
       dynamic_cast<xdata::Float*>( isIt->second->find(isvar) )->value_ = m_plt_HmN.value_;
     }else{
       LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
     }
     isvar = "PLT_HpF";
     if( isIt->second->hasItem(isvar) ){
       dynamic_cast<xdata::Float*>( isIt->second->find(isvar) )->value_ = m_plt_HpF.value_;
     }else{
       LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
     }
     isvar = "PLT_HpN";
     if( isIt->second->hasItem(isvar) ){
       dynamic_cast<xdata::Float*>( isIt->second->find(isvar) )->value_ = m_plt_HpN.value_;
     }else{
       LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
     }
   }
}

void CommonDataExchange::build_beamFlashList(){
    std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("beam");
    std::string isvar;
    if(isIt!=m_flashinfostore.end()){
        isvar = "beamstatus";
        if( isIt->second->hasItem(isvar) ){
            dynamic_cast<xdata::String*>( isIt->second->find(isvar) )->value_ = m_beamstatus.value_;
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "fillscheme";
        if( isIt->second->hasItem(isvar) ){
            dynamic_cast<xdata::String*>( isIt->second->find(isvar) )->value_ = m_fillscheme.value_;
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "beamegev";
        if( isIt->second->hasItem(isvar) ){
            dynamic_cast<xdata::Float*>( isIt->second->find(isvar) )->value_ = m_egev.value_;
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "targetegev";
        if( isIt->second->hasItem(isvar) ){
            dynamic_cast<xdata::Integer*>( isIt->second->find(isvar) )->value_ = int(m_targetegev.value_);
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "amodetag";
        if( isIt->second->hasItem(isvar) ){
            dynamic_cast<xdata::String*>( isIt->second->find(isvar) )->value_ = m_amodetag.value_;
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "beam1_bx_intensity";
        if( isIt->second->hasItem(isvar) ){
            xdata::Vector<xdata::Float>* p = dynamic_cast< xdata::Vector<xdata::Float>* >( isIt->second->find(isvar) );
            p->clear();
            p->setValue(m_avgBunchIntensities_1);
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
        isvar = "beam2_bx_intensity";
        if( isIt->second->hasItem(isvar) ){
            xdata::Vector<xdata::Float>* p = dynamic_cast< xdata::Vector<xdata::Float>* >( isIt->second->find(isvar) );
            p->clear();
            p->setValue(m_avgBunchIntensities_2);
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
    }
}

void CommonDataExchange::build_blmhistoFlashList(){
    std::map< std::string,xdata::InfoSpace* >::iterator isIt = m_flashinfostore.find("blmhisto");
    std::string isvar;
    if(isIt!=m_flashinfostore.end()){
        isvar = "blmhisto";
        if( isIt->second->hasItem(isvar) ){
            xdata::Vector<xdata::Float>* p = dynamic_cast< xdata::Vector<xdata::Float>* >( isIt->second->find(isvar) );
            p->clear();
            p->setValue(m_blmhisto);
        }else{
            LOG4CPLUS_ERROR(getApplicationLogger(),"No var "+isvar+" found in IS "+isIt->first);
        }
    }
}

void CommonDataExchange::createDipPublication(){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"CommonDataExchange::createDipPublication");
    for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){      
      if( it->first.find("VdMMonitor")==std::string::npos ){
	std::string fulldipname=m_dipPubRoot.value_+(it->first);
	DipPublication* p=m_dip->createDipPublication(fulldipname.c_str(),m_puberrorhandler);
	if(!p){
	  LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
	  m_dippubs.erase(it); // remove from registry
	}else{
	  it->second=p;
	}
      }//end if. VdMMonitor dip publication is created dynamically. So it should be excluded here
    }
}

}}//ns bril::dip
