#include "bril/dipprocessor/BkgdPublisher.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "xcept/tools.h"

bril::dip::BkgdPublisher::BkgdPublisher(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
    m_fill = 0;
    m_run = 0;
    m_ls = 0;
    m_nb = 0;
    
    // BCM1F RHU
    m_vme_old1 = -1.; m_vme_old2 = -1.;
    m_vme_nc1 = -1.; m_vme_nc2 = -1.;
    m_vme_lead1 = -1.; m_vme_lead2 = -1.;
    
    // BCM1F UTCA
    m_utca_nc1 = -1.; m_utca_nc2 = -1.;
    m_utca_lead1 = -1.; m_utca_lead2 = -1.;
    
    // PLT
    m_plt_nc1 = -1.; m_plt_nc2 = -1.;
    m_plt_lead1 = -1.; m_plt_lead2 = -1.;
    
    // BHM
    m_bhm1 = -1.; m_bhm2 = -1.;
    
    // BCML
    m_bcml = -1.;
  
    try{
        //if need to read configuraiton, listen also to defaultparameters
        getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
        getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    }catch( xcept::Exception & e){
        LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
    }
    
    m_dipRoot="dip/CMS/";
}

bril::dip::BkgdPublisher::~BkgdPublisher(){
}

void bril::dip::BkgdPublisher::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        try{
            xdata::String* dipRoot = dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
            if(dipRoot) m_dipRoot= dipRoot->value_;
        }catch(xdata::exception::Exception& e){
            std::string msg("Failed to find dipRoot in ApplicationInfoSpace ");
            LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
        }
        
        m_dippubs.insert(std::make_pair(m_dipRoot+"LHC/BackgroundSources",(DipPublication*)0 ));
    }
    if( e.type()== "ItemChangedEvent" ){
        std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
        if(item =="nbnum" ){
            std::stringstream ss;
            try{
                m_fill = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("fillnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find fillnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_run = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("runnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find runnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_ls = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("lsnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find lsnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_nb = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("nbnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find nbnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BCM1F VME
            try{
                m_vme_old1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_old1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_old1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_old2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_old2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_old2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BCM1F UTCA
            try{
                m_utca_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // PLT
            try{
                m_plt_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BHM
            try{
                m_bhm1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_bhm1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_bhm2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_bhm2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BCML
            try{
                m_bcml = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_bcml"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_bcml in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
      
      ss << "bril::dip::BkgdPublisher "
               << "Fill: " << m_fill << ", "
               << "Run: "  << m_run << ", "
               << "LS: "   << m_ls << ", "
               << "NB4: "  << m_nb;
      LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
      publish_to_dip();      
    }
  }
}

void bril::dip::BkgdPublisher::actionPerformed(toolbox::Event& e){
}


void bril::dip::BkgdPublisher::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(),"BkgdPublisher::handleMessage from "+subname);
  if(message.size()==0) return; 
}

void bril::dip::BkgdPublisher::publish_to_dip(){
    // you can use common data such as beamegev in calculation from infospace.
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"BkgdPublisher::publish_to_dip");
    DipData* dipdata = m_dip->createDipData();
    std::string diptopicname(m_dipRoot+"LHC/BackgroundSources");
    
    DipFloat bkgd_vme_old1 = m_vme_old1.value_;
    DipFloat bkgd_vme_old2 = m_vme_old2.value_;
    DipFloat bkgd_vme_nc1 = m_vme_nc1.value_;
    DipFloat bkgd_vme_nc2 = m_vme_nc2.value_;
    DipFloat bkgd_vme_lead1 = m_vme_lead1.value_;
    DipFloat bkgd_vme_lead2 = m_vme_lead2.value_;
    
    DipFloat bkgd_utca_nc1 = m_utca_nc1.value_;
    DipFloat bkgd_utca_nc2 = m_utca_nc2.value_;
    DipFloat bkgd_utca_lead1 = m_utca_lead1.value_;
    DipFloat bkgd_utca_lead2 = m_utca_lead2.value_;
    
    DipFloat bkgd_bhm1 = m_bhm1.value_;
    DipFloat bkgd_bhm2 = m_bhm2.value_;
    
    DipFloat bkgd_plt_nc1 = m_plt_nc1.value_;
    DipFloat bkgd_plt_nc2 = m_plt_nc2.value_;
    DipFloat bkgd_plt_lead1 = m_plt_lead1.value_;
    DipFloat bkgd_plt_lead2 = m_plt_lead2.value_;
    
    DipFloat bkgd_bcml = m_bcml.value_;
        
    dipdata->insert(bkgd_vme_old1,"BCM1FVME_OLD_1");
    dipdata->insert(bkgd_vme_old2,"BCM1FVME_OLD_2");
    dipdata->insert(bkgd_vme_nc1,"BCM1FVME_NC_1");
    dipdata->insert(bkgd_vme_nc2,"BCM1FVME_NC_2");
    dipdata->insert(bkgd_vme_lead1,"BCM1FVME_LEAD_1");
    dipdata->insert(bkgd_vme_lead2,"BCM1FVME_LEAD_2");
    
    dipdata->insert(bkgd_utca_nc1,"BCM1FUTCA_NC_1");
    dipdata->insert(bkgd_utca_nc2,"BCM1FUTCA_NC_2");
    dipdata->insert(bkgd_utca_lead1,"BCM1FUTCA_LEAD_1");
    dipdata->insert(bkgd_utca_lead2,"BCM1FUTCA_LEAD_2");
    
    dipdata->insert(bkgd_plt_nc1,"PLT_NC_1");
    dipdata->insert(bkgd_plt_nc2,"PLT_NC_2");
    dipdata->insert(bkgd_plt_lead1,"PLT_LEAD_1");
    dipdata->insert(bkgd_plt_lead2,"PLT_LEAD_2");
    
    dipdata->insert(bkgd_bhm1,"BHM_MUON_1");
    dipdata->insert(bkgd_bhm2,"BHM_MUON_2");
    
    dipdata->insert(bkgd_bcml,"BCML_BKGD3");
        
    DipTimestamp t;
    m_dippubs[diptopicname]->send(*dipdata,t);
    delete dipdata;
}
