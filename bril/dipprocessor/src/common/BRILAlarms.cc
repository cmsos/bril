#include <sstream>
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Guard.h"
#include "xcept/tools.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/BRILAlarms.h"

XDAQ_INSTANTIATOR_IMPL (bril::dip::BRILAlarms)

bril::dip::BRILAlarms::BRILAlarms(xdaq::ApplicationStub* s) : xdaq::Application(s),m_applock(toolbox::BSem::FULL){
  
  m_dipInRoot.fromString("dip/CMS/");
  m_dipOutRoot.fromString("dip/CMS/");
  m_checkIntervalSec = 1.5;
  m_machineMode="INVALID";
  
  m_plcCrate=m_CAEN=m_bkgd=m_bcml=m_bcmLHV=m_beamdump=m_PLTCAEN=false;
  m_plcCrateValid=true;
  m_plcCrate_ts=m_CAEN_ts=m_bkgd_ts=m_tcds_ts=m_bcmLHV_ts=m_beamdump_ts=0;
  m_bcm1LHV=m_bcm2LHV=0;
  m_bkgd1_local=m_bkgd2_local=m_bkgd3_local=0.0;
  l_bkgd1=l_bkgd2=l_bkgd3=0.0;
  m_bptxScaler_ts=m_bptxScopeMon_ts=m_bptxLHCTiming_ts=0; 
  
  m_PLTCAEN_ts=0;
  m_RHUAnalysis_ts=m_RHUHistos_ts=0;

  // Luminometers alarm
  // Time of last publication
  m_hflumi_ts=m_hflumiet_ts=m_pltzlumi_ts=m_bcm1flumi_ts=m_bcm1futcalumi_ts=m_bestlumi_ts=0;
  m_remuslumi_ts=m_dtlumi_ts=0;
  
  m_hflumi_nb4=m_hflumi_ls=m_hflumi_run=0;
  m_hflumi_live=m_hflumi_insync=false;

  m_hflumiet_nb4=m_hflumiet_ls=m_hflumiet_run=0;
  m_hflumiet_live=m_hflumiet_insync=false;
  
  m_pltzlumi_nb4=m_pltzlumi_ls=m_pltzlumi_run=0;
  m_pltzlumi_live=m_pltzlumi_insync-false;
  
  m_bcm1flumi_nb4=m_bcm1flumi_ls=m_bcm1flumi_run=0;
  m_bcm1flumi_live=m_bcm1flumi_insync=false;
  
  m_bcm1futcalumi_nb4=m_bcm1futcalumi_ls=m_bcm1futcalumi_run=0;
  m_bcm1futcalumi_live=m_bcm1futcalumi_insync=false;
  
  m_bhm=m_bhm_live=false;
  
  // No nb4 for DT
  m_dtlumi_ls=m_dtlumi_run=0;
  m_dtlumi_live=m_dtlumi_insync=false;
  
  m_remuslumi_nb4=m_remuslumi_ls=m_remuslumi_run=0;
  m_remuslumi_live=m_remuslumi_insync=false;
  
  m_bestlumi_nb4=m_bestlumi_ls=m_bestlumi_run=0;
  m_bestlumi_live=m_bestlumi_insync=false;
  
  m_b1current_ts=m_b2current_ts=0;
  m_b1_in=m_b2_in=true;// I don't understand at all about all the logics around m_bx_in!! Beam present or not
  
  // Luminometers not in sync.
  m_hf_BX1=m_pltz_BX1=m_bcm1f_BX1=m_bcm1futca_BX1=-1;

  //CAEN Setup
  m_1FmHV.insert(std::make_pair("1FZmFaCh00",false)) ;
  m_1FmHV.insert(std::make_pair("1FZmFaCh01",false)); 
  m_1FmHV.insert(std::make_pair("1FZmFaCh02",false));
  m_1FmHV.insert(std::make_pair("1FZmFaCh03",false));
  m_1FmHV.insert(std::make_pair("1FZmFaCh04",false));
  m_1FmHV.insert(std::make_pair("1FZmFaCh05",false));
  m_1FmLV.insert(std::make_pair("1FZmFaLVBC",false));
  m_1FmLV.insert(std::make_pair("1FZmFaLVPP",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh00",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh01",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh02",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh03",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh04",false));
  m_1FmHV.insert(std::make_pair("1FZmNeCh05",false));
  m_1FmLV.insert(std::make_pair("1FZmNeLVBC",false));
  m_1FmLV.insert(std::make_pair("1FZmNeLVPP",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh00",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh01",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh02",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh03",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh04",false));
  m_1FpHV.insert(std::make_pair("1FZpFaCh05",false));
  m_1FpLV.insert(std::make_pair("1FZpFaLVBC",false));
  m_1FpLV.insert(std::make_pair("1FZpFaLVPP",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh00",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh01",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh02",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh03",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh04",false));
  m_1FpHV.insert(std::make_pair("1FZpNeCh05",false));
  m_1FpLV.insert(std::make_pair("1FZpNeLVBC",false));
  m_1FpLV.insert(std::make_pair("1FZpNeLVPP",false));
  m_1LmHV.insert(std::make_pair("1LZmFarBottom",false));
  m_1LmHV.insert(std::make_pair("1LZmFarTop",false));
  m_1LmHV.insert(std::make_pair("1LZmNearBottom",false));
  m_1LmHV.insert(std::make_pair("1LZmNearTop",false));
  m_1LpHV.insert(std::make_pair("1LZpFarBottom",false));
  m_1LpHV.insert(std::make_pair("1LZpFarTop",false));
  m_1LpHV.insert(std::make_pair("1LZpNearBottom",false));
  m_1LpHV.insert(std::make_pair("1LZpNearTop",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerBottom",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerFar",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerFar_B",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerNear",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerNear_B",false));
  m_2LmHV.insert(std::make_pair("2LZmInnerTop",false));
  m_2LmHV.insert(std::make_pair("2LZmOuterBottom",false));
  m_2LmHV.insert(std::make_pair("2LZmOuterBottom-Far",false));
  m_2LmHV.insert(std::make_pair("2LZmOuterFar",false));
  m_2LmHV.insert(std::make_pair("2LZmOuterNear",false));
  m_2LmHV.insert(std::make_pair("2LZmOuterTop",false));
  m_2LmLV.insert(std::make_pair("2LZmOuterTop-Near",false));
  m_2LmLV.insert(std::make_pair("2LZmTC1RST",false));
  m_2LmLV.insert(std::make_pair("2LZmTC1n",false));
  m_2LmLV.insert(std::make_pair("2LZmTC1p",false));
  m_2LmLV.insert(std::make_pair("2LZmTC2RST",false));
  m_2LmLV.insert(std::make_pair("2LZmTC2n",false));
  m_2LmLV.insert(std::make_pair("2LZmTC2p",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerBottom",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerBottom_B",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerFar",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerFar_B",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerNear",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerNear_B",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerTop",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerTop-Near",false));
  m_2LpHV.insert(std::make_pair("2LZpInnerTop_B",false));
  m_2LpHV.insert(std::make_pair("2LZpOuterBottom",false));
  m_2LpHV.insert(std::make_pair("2LZpOuterBottom-Near",false));
  m_2LpHV.insert(std::make_pair("2LZpOuterFar",false));
  m_2LpHV.insert(std::make_pair("2LZpOuterNear",false));
  m_2LpHV.insert(std::make_pair("2LZpOuterTop",false));
  m_2LpLV.insert(std::make_pair("2LZpTC3RST",false));
  m_2LpLV.insert(std::make_pair("2LZpTC3n",false));
  m_2LpLV.insert(std::make_pair("2LZpTC3p",false));
  m_2LpLV.insert(std::make_pair("2LZpTC4RST",false));
  m_2LpLV.insert(std::make_pair("2LZpTC4n",false));
  m_2LpLV.insert(std::make_pair("2LZpTC4p",false));
  m_BHMLV.insert(std::make_pair("BhmLvNcSpare00",false));
  m_BHMLV.insert(std::make_pair("BhmLvNcSpare01",false));
  m_BHMLV.insert(std::make_pair("BhmLvNcSpare02",false));
  m_BHMLV.insert(std::make_pair("BhmLvNcSpare03",false));
  m_BHMLV.insert(std::make_pair("BhmLvVAux",false));
  m_BHMLV.insert(std::make_pair("BhmLvVCal",false));
  m_BHMLV.insert(std::make_pair("BhmLvVPwr",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh01",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh02",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh03",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh04",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh05",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh06",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh07",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh08",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh09",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaCh10",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaRef",false));
  m_BHMmHV.insert(std::make_pair("BhmZmFaSpare",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh01",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh02",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh03",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh04",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh05",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh06",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh07",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh08",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh09",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeCh10",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeRef",false));
  m_BHMmHV.insert(std::make_pair("BhmZmNeSpare",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh01",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh02",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh03",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh04",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh05",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh06",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh07",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh08",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh09",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaCh10",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaRef",false));
  m_BHMpHV.insert(std::make_pair("BhmZpFaSpare",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh01",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh02",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh03",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh04",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh05",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh06",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh07",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh08",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh09",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeCh10",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeRef",false));
  m_BHMpHV.insert(std::make_pair("BhmZpNeSpare",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmFT0",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmFT1",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmFT2",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmFT3",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmNT0",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmNT1",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmNT2",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HmNT3",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpFT0",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpFT1",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpFT2",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpFT3",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpNT0",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpNT1",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpNT2",false));
  m_PLTHV.insert(std::make_pair("PLTHV_HpNT3",false));
  m_PLTLV.insert(std::make_pair("PLT_Ana_HmF",false));
  m_PLTLV.insert(std::make_pair("PLT_Ana_HmN",false));  
  m_PLTLV.insert(std::make_pair("PLT_Ana_HpF",false));  
  m_PLTLV.insert(std::make_pair("PLT_Ana_HpN",false));
  m_PLTLV.insert(std::make_pair("PLT_Dig_HmF",false));
  m_PLTLV.insert(std::make_pair("PLT_Dig_HmN",false));  
  m_PLTLV.insert(std::make_pair("PLT_Dig_HpF",false));  
  m_PLTLV.insert(std::make_pair("PLT_Dig_HpN",false));

  getApplicationInfoSpace()->fireItemAvailable("dipInRoot",&m_dipInRoot);
  getApplicationInfoSpace()->fireItemAvailable("dipOutRoot",&m_dipOutRoot);
  getApplicationInfoSpace()->fireItemAvailable("checkIntervalSec",&m_checkIntervalSec);
  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

bril::dip::BRILAlarms::~BRILAlarms(){
}

void bril::dip::BRILAlarms::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        // We read alarms through dip. Adding BCM1F UTCA, REMUS, DT. BHM, LHC Radmons under discussion
        
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/RunControl/MachineMode",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/Intensity/Beam1",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/Intensity/Beam2",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRM/PLCcrate/Status",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/CAEN/Status",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/CAEN/Summary",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"LHC/BKGD",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/BPTX/scaler_rates",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiHF",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiHFET",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiPLTZ",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiBCM1F",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/Luminosity",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiSection/LumiPerBunch",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/RHUAnalysis",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/RHUHistos",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/PLT/CAEN/HVvalues",(DipSubscription*)0 ));
        
        // New alarms
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiBCM1FUTCA",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiDT",(DipSubscription*)0 ));
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/LumiREMUS",(DipSubscription*)0 ));
        
        // For BHM
        m_dipsubs.insert(std::make_pair(m_dipInRoot.value_+"BRIL/BKGD",(DipSubscription*)0 ));
        
        // Pubs
        m_dippubs.insert(std::make_pair(m_dipOutRoot.value_+"BRIL/BRILAlarms",(DipPublication*)0 ));
        
        toolbox::net::UUID uuid;
        std::string processuuid = uuid.toString();
        m_dip = Dip::create(( std::string("BRILAlarms_")+processuuid).c_str() );
        m_puberrorhandler = new PubErrorHandler;
        createDipPublication();
        subscribeToDip();
        m_timername = "BRIMAlarms_timer";
        std::stringstream ss;
        
        try{
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            toolbox::TimeInterval delta(3,0); //start timer 3 seconds from now
            start += delta;
            toolbox::TimeInterval checkinterval( (double)m_checkIntervalSec.value_ );
            m_timer = toolbox::task::getTimerFactory()->createTimer(m_timername);
            m_timer->scheduleAtFixedRate(start,this,checkinterval,(void*)0, m_timername);
        }catch(toolbox::task::exception::Exception& e){
            ss<<"Failed to start timer "<<m_timername <<" "<< stdformat_exception_history(e);
            LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
        }
    }
}

void bril::dip::BRILAlarms::connected(DipSubscription* dipsub){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getApplicationLogger(),"Connected to "+subname);
}
void bril::dip::BRILAlarms::disconnected(DipSubscription* dipsub, char *reason){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getApplicationLogger(),"disConnected from "+subname);
  DipTimestamp t;
  long now =t.getAsMillis()/1000;
  if (subname == m_dipInRoot.value_+"BRIL/CAEN/Status"){
    m_CAEN=false;
    m_CAEN_ts=now;
  }
  if (subname == m_dipInRoot.value_+"BRIL/PLT/CAEN/HVvalues"){
    m_PLTCAEN=false;
    m_PLTCAEN_ts=now;
  }
}

void bril::dip::BRILAlarms::handleException(DipSubscription* dipsub, DipException& ex){
  LOG4CPLUS_ERROR(getApplicationLogger(),"Publication source "+std::string(dipsub->getTopicName())+" exception "<<ex.what());
}

void bril::dip::BRILAlarms::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_DEBUG(getApplicationLogger(),"handleMessage "+subname);
  if(message.size()==0) return;
  const DipTimestamp TSSeconds=message.extractDipTime();
  long seconds =TSSeconds.getAsMillis()/1000;
  const DipQuality quality=message.extractDataQuality();
  bool dq=false;
  if (quality==DIP_QUALITY_GOOD){
    dq=true;
  }
  
  if (subname=="dip/acc/LHC/Beam/BLM/BLMBCM2/Acquisition"){
      try{
          const bool bpermit = message.extractBool("blecsUDL");
          if(!bpermit && !m_beamdump){
              //a new beamdump event
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_beamdump_ts=seconds;
          }
          if(!bpermit || (m_beamdump && (seconds-m_beamdump_ts)<600)){
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_beamdump=true;
          } else {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_beamdump=false;
          }
      }catch(const DipException& e){
          LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
      
  }else if (subname == "dip/acc/LHC/RunControl/MachineMode"){
      try{
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_machineMode=message.extractString("value");
      }catch(const DipException& e){
          LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+std::string(e.what()));
      }
  
  } else if (subname == m_dipInRoot.value_+"BRM/PLCcrate/Status"){
      try{
          const bool inputBeamPermit = message.extractBool("input(beam_permit)");
          const bool inputInjectPermit = message.extractBool("input(inject_permit)");
          const bool inputPowerSupply1 = message.extractBool("power_supply_1");
          const bool inputPowerSupply2 = message.extractBool("power_supply_2");
          const bool outputBeamPermit = message.extractBool("output(beam_permit)");
          const bool outputInjectionPermit = message.extractBool("output(inject_permit)");
          {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_plcCrate=dq;
              m_plcCrate_ts=seconds;
              m_plcCrateValid = inputBeamPermit||inputInjectPermit||inputPowerSupply1||inputPowerSupply2||outputBeamPermit||outputInjectionPermit;
          }
          if (m_plcCrateValid){
              if(!outputBeamPermit && !m_beamdump){
                  //a new beamdump event
                  toolbox::task::Guard<toolbox::BSem> g(m_applock);
                  m_beamdump_ts=seconds;
              }
              if(!outputBeamPermit || (m_beamdump && (seconds-m_beamdump_ts)<600)){
                  toolbox::task::Guard<toolbox::BSem> g(m_applock);
                  m_beamdump=true;
              } else {
                  toolbox::task::Guard<toolbox::BSem> g(m_applock);
                  m_beamdump=false;
              }
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no stop on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/CAEN/Status"){
      {
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_CAEN = dq;
          m_CAEN_ts = seconds;
      }
      try{
          std::map<std::string,bool>::iterator iter;
          for (iter=m_1FmHV.begin(); iter != m_1FmHV.end(); iter++){
              iter->second = message.extractBool(iter->first.c_str());
          }
          for (iter=m_1FpHV.begin(); iter != m_1FpHV.end(); iter++){
              iter->second = message.extractBool(iter->first.c_str());
          }
          for (iter=m_1FmLV.begin(); iter != m_1FmLV.end(); iter++){
              iter->second = message.extractBool(iter->first.c_str());
          }
          for (iter=m_1FpLV.begin(); iter != m_1FpLV.end(); iter++){
              iter->second=message.extractBool(iter->first.c_str());
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/CAEN/Summary"){
      {
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_bcmLHV=dq;
          m_bcmLHV_ts=seconds;
      }
      try{
          m_bcm1LHV=message.extractInt("BCML1_HV_CHANNELS_ON");
          m_bcm2LHV=message.extractInt("BCML2_HV_CHANNELS_ON");
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/PLT/CAEN/HVvalues"){
      {
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_PLTCAEN = dq;
          m_PLTCAEN_ts = seconds;
      }
      try{
          std::map<std::string,bool>::iterator iter;
          for(iter=m_PLTHV.begin(); iter != m_PLTHV.end(); iter++){
              float v=message.extractFloat(iter->first.c_str());
              iter->second = v>90;
          }
          for (iter=m_PLTLV.begin(); iter != m_PLTLV.end(); iter++){
              float v=message.extractFloat(iter->first.c_str());
              iter->second = v>4;
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if ( subname == m_dipInRoot.value_+"LHC/BKGD"){
      {
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_bcml=m_bkgd=dq;
          m_bkgd_ts=seconds;
      }
      try{
          l_bkgd1=m_bkgd1_local;
          l_bkgd2=m_bkgd2_local;
          l_bkgd3=m_bkgd3_local;
          m_bkgd1_local=message.extractFloat("BKGD1");
          m_bkgd2_local=message.extractFloat("BKGD2");
          m_bkgd3_local=message.extractFloat("BKGD3");
          if(m_bkgd1_local<-0.5 or m_bkgd2_local<-0.5){
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_bkgd=false;
          }
          if(m_bkgd3_local<-0.5){
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_bcml=false;
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no stop on dip extract error, but log the error message
      }
      
  } else if ( subname == m_dipInRoot.value_+"BRIL/BKGD"){
      {
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_bhm_live = dq;
          m_bhm_ts = seconds;
      }
      try{
          l_bhm1=message.extractFloat("BHM_MUON_1");
          l_bhm2=message.extractFloat("BHM_MUON_2");
          
          if (l_bhm1 <-0.5 or l_bhm1 <-0.5){
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_bhm = false;
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no stop on dip extract error, but log the error message
      }

  } else if ( subname == m_dipInRoot.value_+"BRIL/BPTX/scaler_rates"){
      toolbox::task::Guard<toolbox::BSem> g(m_applock);
      m_bptxScaler_ts=seconds;
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiHF"){
      try{
          m_hflumi_nb4=message.extractInt("Nibble");
          m_hflumi_ls=message.extractInt("LumiSection");
          m_hflumi_run=message.extractInt("Run");
          m_hflumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiHFET"){
      try{
          m_hflumiet_nb4=message.extractInt("Nibble");
          m_hflumiet_ls=message.extractInt("LumiSection");
          m_hflumiet_run=message.extractInt("Run");
          m_hflumiet_ts=seconds;
          
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiHFET"){
      try{
          m_hflumiet_nb4=message.extractInt("Nibble");
          m_hflumiet_ls=message.extractInt("LumiSection");
          m_hflumiet_run=message.extractInt("Run");
          m_hflumiet_ts=seconds;
          
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiPLTZ"){
      try{
          m_pltzlumi_nb4=message.extractInt("Nibble");
          m_pltzlumi_ls=message.extractInt("LumiSection");
          m_pltzlumi_run=message.extractInt("Run");
          m_pltzlumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiBCM1F"){
      try{
          m_bcm1flumi_nb4=message.extractInt("Nibble");
          m_bcm1flumi_ls=message.extractInt("LumiSection");
          m_bcm1flumi_run=message.extractInt("Run");
          m_bcm1flumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiBCM1FUTCA"){
      try{
          m_bcm1futcalumi_nb4=message.extractInt("Nibble");
          m_bcm1futcalumi_ls=message.extractInt("LumiSection");
          m_bcm1futcalumi_run=message.extractInt("Run");
          m_bcm1futcalumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiDT"){
      try{

          m_dtlumi_ls=message.extractInt("LumiSection");
          m_dtlumi_run=message.extractInt("Run");
          m_dtlumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiREMUS"){
      try{
          m_remuslumi_nb4=message.extractInt("Nibble");
          m_remuslumi_ls=message.extractInt("LumiSection");
          m_remuslumi_run=message.extractInt("Run");
          m_remuslumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/Luminosity"){
      try{
          m_bestlumi_nb4=message.extractInt("LumiNibble");
          m_bestlumi_ls=message.extractInt("LumiSection");
          m_bestlumi_run=message.extractInt("Run");
          m_bestlumi_ts=seconds;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/RHUAnalysis"){
      toolbox::task::Guard<toolbox::BSem> g(m_applock);
      m_RHUAnalysis_ts=seconds;
  
  } else if (subname == m_dipInRoot.value_+"BRIL/RHUHistos"){
      toolbox::task::Guard<toolbox::BSem> g(m_applock);
      m_RHUHistos_ts=seconds;
  
  } else if (subname == "dip/acc/LHC/Beam/Intensity/Beam1"){
      try{
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_b1current_ts=seconds;
          m_b1_in=false;
          if (message.extractDouble("totalIntensity") > 1E10 ){
              m_b1_in=true;
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname=="dip/acc/LHC/Beam/Intensity/Beam2"){
      try{
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_b2current_ts=seconds;
          m_b2_in=false;
          if (message.extractDouble("totalIntensity") > 1E10 ){
              m_b2_in=true;
          }
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  
  } else if (subname == m_dipInRoot.value_+"BRIL/LumiSection/LumiPerBunch"){
      try{
          int sizeValues;
          {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_hf_BX1 = -1;
              const DipFloat * HFBX = message.extractFloatArray(sizeValues,"HF_BXLumi");
              if (sizeValues>0){
                  for (int i=0; i<sizeValues; i++){
                      if (HFBX[i]>1.0){
                          m_hf_BX1=i;
                          break;
                      }
                  }
              }
          }
          {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_pltz_BX1 = -1;
              const DipFloat * PLTBX = message.extractFloatArray(sizeValues,"PLTZC_BXLumi");
              if (sizeValues>0){
                  for (int i=0; i<sizeValues; i++){
                      if (PLTBX[i]>1.0){
                          m_pltz_BX1=i;
                          break;
                      }
                  }
              }
          }
          {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_bcm1f_BX1 = -1;
              const DipFloat * BCMFBX = message.extractFloatArray(sizeValues,"BCMF_BXLumi");
              if (sizeValues>0){
                  for (int i=0; i<sizeValues; i++){
                      if (BCMFBX[i]>1.0){
                          m_bcm1f_BX1=i;
                          break;
                      }
                  }
              }
          }
          {
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
              m_bcm1futca_BX1 = -1;
              const DipFloat * BCMFUTCABX = message.extractFloatArray(sizeValues,"BCMFUTCA_BXLumi");
              if (sizeValues>0){
                  for (int i=0; i<sizeValues; i++){
                      if (BCMFUTCABX[i]>1.0){
                          m_bcm1futca_BX1=i;
                          break;
                      }
                  }
              }
          }
          //std::cerr<<"FirstCollidingBX: HF "<<m_hf_BX1<<"   PLTZ "<<m_pltz_BX1<<" BCMF "<<m_bcm1f_BX1<<std::endl;
      }catch( DipException& e ){
          LOG4CPLUS_ERROR(getApplicationLogger(),std::string(e.what()));//no throw on dip extract error, but log the error message
      }
  }
}

void bril::dip::BRILAlarms::timeExpired(toolbox::task::TimerEvent& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"timeExpired");
    publish_to_dip();
}

void bril::dip::BRILAlarms::publish_to_dip(){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"publish_to_dip");

  DipTimestamp t;
  long now =t.getAsMillis()/1000;
  
  //some validity logic
  if(now-m_plcCrate_ts>30){
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    m_plcCrate=false;
  }
  if(now-m_bkgd_ts>10){
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    m_bkgd=false;
    m_bcml=false;
  }
  {
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    m_bcmLHV=false;
  }
  
  if(m_bcm1LHV>4 && m_bcm2LHV>4  && m_CAEN){
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    m_bcmLHV=true;
  }
  //std::cout<<"BCML "<<m_bcm1LHV<<" "<< m_bcm2LHV<<" "<<m_CAEN<<std::endl;
  //
  //Count BCM1FHV channels ON
  //
  std::map<std::string,bool>::iterator iter;
  int mfhvon=0;
  int pfhvon=0;
  for (iter=m_1FmHV.begin(); iter != m_1FmHV.end(); iter++){
    if(iter->second){mfhvon++;}
  }
  for (iter=m_1FpHV.begin(); iter != m_1FpHV.end(); iter++){
    if(iter->second){pfhvon++;}
  }

  //
  //Now the logic to declare BCM1FHV as good
  //
  bool _1fHVON=false;
  if(mfhvon==12 and pfhvon==12 and m_CAEN){_1fHVON=true;}
  //
  //Count BCM1FLV channels ON
  //
  int mflvon=0;
  int pflvon=0;
  for (iter=m_1FmLV.begin(); iter != m_1FmLV.end(); iter++){
    if(iter->second){mflvon++;}
  }
  for (iter=m_1FpLV.begin(); iter != m_1FpLV.end(); iter++){
    if(iter->second){pflvon++;}
  }  

  //
  //Now the logic to declare BCM1FLV as good
  //
  bool _1fLVON=false;
  if(mflvon==4 and pflvon==4 and m_CAEN){_1fLVON=true;}

  //
  //Count BHMHV channels ON
  //
  int mhhvon=0;
  int phhvon=0;
  for (iter=m_BHMmHV.begin(); iter != m_BHMmHV.end(); iter++){
    if(iter->second){mhhvon++;}
  }
  for (iter=m_BHMpHV.begin(); iter != m_BHMpHV.end(); iter++){
    if(iter->second){phhvon++;}
  }

  //
  //Now the logic to declare BHMHV as good
  //
  //bool _bhmHVON=false;
  //if(mhhvon>5 && phhvon>5 && m_CAEN){_bhmHVON=true;}

  //
  //Now the logic to declare BHMLV as good
  //
  //bool _bhmLVON=false;
  //for (iter=m_BHMLV.begin(); iter != m_BHMLV.end(); iter++){
  //  if(iter->first=="BhmLvVPwr" && iter->second ){_bhmLVON=true;}
  //}

  //Count PLT channels ON
  //

  int plthvon=0;
  int pltlvon=0;
  for (iter=m_PLTHV.begin(); iter != m_PLTHV.end(); iter++){
    if(iter->second){plthvon++;}
  }
  for (iter=m_PLTLV.begin(); iter != m_PLTLV.end(); iter++){
    if(iter->second){pltlvon++;}
  }

  //
  //Now the logic to declare PLT Voltages as good
  //
  bool _plthvon=false;
  bool _pltlvon=false;
  if (plthvon==16)_plthvon=true;
  if (pltlvon==8) _pltlvon=true;
  //std::cout<<"PLT CAEN: HV "<<plthvon<<" LV "<<pltlvon<<"  DQ "<<m_PLTCAEN<<" T "<<now-m_PLTCAEN_ts<<std::endl;

  //
  //Now the logic to declare detect high bkgd rates
  //
  bool bkgd1=false;
  if(l_bkgd1>50 || m_bkgd1_local>50){bkgd1=true;}
  bool bkgd2=false;
  if(l_bkgd2>50 || m_bkgd2_local>50){bkgd2=true;}
  bool bkgd3=false;
  if(l_bkgd3>50 || m_bkgd3_local>50){bkgd3=true;}
  
  //Ready for injection
  bool readyI=m_bcmLHV && m_bcml && m_bkgd && m_CAEN;
  //
  //BPTX Publications
  //
  bool bptxScaler=true;
  //bool bptxTiming=true;
  if(now-m_bptxScaler_ts>10){
    bptxScaler=false;
  }
  //if(now-m_bptxLHCTiming_ts>50){
  //  bptxTiming=false;
  //}

  //HFLumi Publication
  m_hflumi_live=false;
  if(now - m_hflumi_ts<60){
  	m_hflumi_live=true;
  }

  //HFLumiET Publication
  m_hflumiet_live=false;
  if(now - m_hflumiet_ts<60){
    m_hflumiet_live=true;	
  }

  //PLTZLumi Publication
  m_pltzlumi_live=false;
  if(now - m_pltzlumi_ts<60){
    m_pltzlumi_live=true;
  }
  //BCM1FLumi Publication
  m_bcm1flumi_live=false;
  if(now - m_bcm1flumi_ts<60){
    m_bcm1flumi_live=true;
  }
  
  //BCM1FUTCALumi Publication
  m_bcm1futcalumi_live=false;
  if(now - m_bcm1futcalumi_ts<60){
    m_bcm1futcalumi_live=true;
  }
  
  m_dtlumi_live=false;
  if(now - m_dtlumi_ts<240){ // Slightly higher tolerance for LS granularity
  	m_dtlumi_live=true;
  }
  
  m_remuslumi_live=false;
  if(now - m_remuslumi_ts<60){
  	m_remuslumi_live=true;
  }
  
  //bestLumi Publication
  m_bestlumi_live=false;
  if(now - m_bestlumi_ts<60){    
    m_bestlumi_live=true;
  }
  
  m_bhm_live=false;
  if (now - m_bhm_ts<60){
    m_bhm_live=true;
  }

  // InSync publications
  m_hflumi_insync=m_hflumiet_insync=m_pltzlumi_insync=m_bcm1flumi_insync=m_bcm1futcalumi_insync=false;
  if (m_bestlumi_live){
      if (m_hflumi_live){
          if (m_bestlumi_run ==m_hflumi_run){
              if(abs(m_bestlumi_ls- m_hflumi_ls)<2){
                  m_hflumi_insync=true;
                  if(abs(m_bestlumi_nb4 - m_hflumi_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_hflumi_insync=false;
                  }
              }
          }
      }
      
      if (m_hflumiet_live){
          if (m_bestlumi_run ==m_hflumiet_run){
              if(abs(m_bestlumi_ls- m_hflumiet_ls)<2){
                  m_hflumiet_insync=true;
                  if(abs(m_bestlumi_nb4 - m_hflumiet_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_hflumiet_insync=false;
                  }
              }
          }
      }
      
      if (m_bcm1flumi_live){
          if (m_bestlumi_run ==m_bcm1flumi_run){
              if(abs(m_bestlumi_ls- m_bcm1flumi_ls)<2){
                  m_bcm1flumi_insync=true;
                  if(abs(m_bestlumi_nb4 - m_bcm1flumi_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_bcm1flumi_insync=false;
                  }
              }
          }
      }
      if (m_bcm1futcalumi_live){
          if (m_bestlumi_run ==m_bcm1futcalumi_run){
              if(abs(m_bestlumi_ls- m_bcm1futcalumi_ls)<2){
                  m_bcm1futcalumi_insync=true;
                  if(abs(m_bestlumi_nb4 - m_bcm1futcalumi_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_bcm1futcalumi_insync=false;
                  }
              }
          }
      }
      
      if (m_remuslumi_live){
          if (m_bestlumi_run ==m_remuslumi_run){
              if(abs(m_bestlumi_ls- m_remuslumi_ls)<2){
                  m_remuslumi_insync=true;
                  if(abs(m_bestlumi_nb4 - m_remuslumi_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_hflumi_insync=false;
                  }
              }
          }
      }
      
      // For DT no nb4 granularity so-far. Will use only LS check
      if (m_dtlumi_live){
          if (m_bestlumi_run == m_dtlumi_run){
              if(abs(m_bestlumi_ls- m_dtlumi_ls)<2){
                  m_remuslumi_insync=true;
              }
          }
      }
      
      if (m_pltzlumi_live){
          if (m_bestlumi_run ==m_pltzlumi_run){
              if(abs(m_bestlumi_ls- m_pltzlumi_ls)<2){
                  m_pltzlumi_insync=true;
                  if(abs(m_bestlumi_nb4 - m_pltzlumi_nb4)>4 and m_bestlumi_nb4>4 and m_bestlumi_nb4< 60){
                      m_pltzlumi_insync=false;
                  }
              }
          }
      }
      
      //b1_current Publication
      if(now - m_b1current_ts>10){ //????
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_b1_in=true;
      }
      //b2_current Publication
      if(now - m_b2current_ts>10){ //????
          toolbox::task::Guard<toolbox::BSem> g(m_applock);
          m_b2_in=true;
      }
  }
  
  int lumiSources=int(m_hflumi_live&&m_hflumi_insync)+int(m_bcm1flumi_live&&m_bcm1flumi_insync)+int(m_pltzlumi_live&&m_pltzlumi_insync)+int(m_hflumiet_live&&m_hflumiet_insync)+int(m_bcm1futcalumi_live&&m_bcm1futcalumi_insync)+(m_remuslumi_live&&m_remuslumi_insync)+(m_dtlumi_live&&m_dtlumi_insync);
  
  // Best lumi and at least 1 luminometer. TODO: Decide if DT or Remus are fullfil the criterion
  bool readyP=m_bestlumi_live && lumiSources > 1;
  
  bool postRamp=false;
  bool stable_beams = false;
  postRamp=m_machineMode=="FLAT TOP"||m_machineMode=="ADJUST"||m_machineMode=="SQUEEZE"||m_machineMode=="STABLE BEAMS";
  stable_beams = m_machineMode=="STABLE BEAMS";
  
  bool twoBeams=m_b1_in&&m_b2_in;
  bool beamIn=m_b1_in||m_b2_in;  
  
  bool firstBX=false;
  if((m_hf_BX1 >=0 && m_pltz_BX1>=0) && m_hf_BX1 != m_pltz_BX1 )firstBX=true;  
  if((m_hfet_BX1 >=0 && m_pltz_BX1>=0) && m_hfet_BX1 != m_pltz_BX1 )firstBX=true;  
  if((m_hf_BX1 >=0 && m_bcm1f_BX1>=0) && m_hf_BX1 != m_bcm1f_BX1 )firstBX=true;
  if((m_hfet_BX1 >=0 && m_bcm1f_BX1>=0) && m_hfet_BX1 != m_bcm1f_BX1 )firstBX=true;
  if((m_bcm1f_BX1 >=0 && m_pltz_BX1>=0) && m_bcm1f_BX1 != m_pltz_BX1 )firstBX=true;

  DipData* dipdata2 = m_dip->createDipData();  
  std::string diptopicname2(m_dipOutRoot.value_+"BRIL/BRILAlarms");

  // Multyple conditions
  dipdata2->insert(!m_bkgd&&beamIn,"BKGD12NotAliveBeamIn");
  dipdata2->insert(!m_bcmLHV,"BCMLNotOn");
  dipdata2->insert(!(_1fLVON&&_1fHVON)&&beamIn,"BCMFHVorLVNotOn.BeamIn");
  dipdata2->insert(m_beamdump,"CMSDumpedBeam");
  dipdata2->insert(!m_plcCrate,"PLCMonitoringNotAlive");
  dipdata2->insert(!m_bcml,"BCMLReadoutNotRunning");
  dipdata2->insert(!bptxScaler&&beamIn,"BPTXScalersNotRunningBeamIn");
  dipdata2->insert(!m_bhm&&m_bhm_live&&twoBeams,"BHMNotRunningBeamsIn");
  dipdata2->insert(!m_hflumi_live&&twoBeams,"HFLumiNotRunningBeamsIn");
  dipdata2->insert(!m_hflumiet_live&&twoBeams,"HFLumiETNotRunningBeamsIn");
  dipdata2->insert(!m_pltzlumi_live&&twoBeams,"PLTZLumiNotRunningBeamsIn");
  dipdata2->insert(!m_bcm1flumi_live&&twoBeams,"BCM1FRULumiNotRunningBeamsIn");
  dipdata2->insert(!m_bcm1futcalumi_live&&twoBeams,"BCM1FUTCALumiNotRunningBeamsIn");
  dipdata2->insert(!m_dtlumi_live&&twoBeams&&stable_beams,"DTLumiNotRunningBeamsIn");
  dipdata2->insert(!m_remuslumi_live&&twoBeams&&stable_beams,"REMUSLumiNotRunningBeamsIn");
  dipdata2->insert(!m_bestlumi_live&&twoBeams,"BestLumiNotRunningBeamsIn");
  dipdata2->insert(!(m_hflumi_insync&&m_hflumiet_insync&&m_pltzlumi_insync&&m_bcm1flumi_insync&&m_bcm1futcalumi_insync)&&twoBeams&&postRamp,"LuminometersNotInSyncBeamsIn");
  dipdata2->insert(!readyI&&m_machineMode=="INJECTION PROBE BEAM","NotReadyForInjectionBeamModeInjection");
  dipdata2->insert(!readyP&&postRamp&&twoBeams,"LumiometersNotReadyForPhysicsFlattop");
  dipdata2->insert(!m_hflumi_insync&&twoBeams&&postRamp,"HFLumiNotInSyncBeamsIn");
  dipdata2->insert(!m_hflumiet_insync&&twoBeams&&postRamp,"HFLumiETNotInSyncBeamsIn");
  dipdata2->insert(!m_pltzlumi_insync&&twoBeams&&postRamp,"PLTZLumiNotInSyncBeamsIn");
  dipdata2->insert(!m_bcm1flumi_insync&&twoBeams&&postRamp,"BCM1FRHULumiNotInSyncBeamsIn");
  dipdata2->insert(!m_bcm1futcalumi_insync&&twoBeams&&postRamp,"BCM1FUTCALumiNotInSyncBeamsIn");
  dipdata2->insert(!m_dtlumi_insync&&twoBeams&&postRamp,"DTLumiNotInSyncBeamsIn");
  dipdata2->insert(!m_remuslumi_insync&&twoBeams&&postRamp,"REMUSLumiNotInSyncBeamsIn");
  dipdata2->insert(firstBX &&twoBeams&&postRamp,"LuminometersDisagreeFirstCollidingBunch");  
  dipdata2->insert(bkgd1&&beamIn,"BKGD1High");
  dipdata2->insert(bkgd2&&beamIn,"BKGD2High");
  dipdata2->insert(bkgd3,"BKGD3High");
  dipdata2->insert(!m_plcCrateValid,"PLCCrateNotValid");
  dipdata2->insert((!_plthvon || !_pltlvon ||!m_PLTCAEN ||(now-m_PLTCAEN_ts)>30)&&twoBeams,"PLTHVLVInError");
  m_dippubs[diptopicname2]->send(*dipdata2,t);
  delete dipdata2;
  std::stringstream ss;
  ss<<"BKGD12NotAliveBeamIn "<<(!bptxScaler&&beamIn)<<",BCMLNotOn "<<(!m_bcmLHV)<<",BCMFHVorLVNotOn.BeamIn "<<(!(_1fLVON&&_1fHVON)&&beamIn)<<",CMSDumpedBeam "<<m_beamdump<<",PLCMonitoringNotAlive "<<(!m_plcCrate)<<",BCMLReadoutNotRunning "<<(!m_bcml)<<",BPTXScalersNotRunningBeamIn "<<(!bptxScaler&&beamIn)<<",HFLumiNotRunningBeamsIn "<<(!m_hflumi_live&&twoBeams)<<",PLTZLumiNotRunningBeamsIn "<<(!m_pltzlumi_live&&twoBeams)<<",BCM1FRULumiNotRunningBeamsIn "<<(!m_bcm1flumi_live&&twoBeams)<<",BestLumiNotRunningBeamsIn "<<(!m_bestlumi_live&&twoBeams)<<",LuminometersNotInSyncBeamsIn "<<(!(m_hflumi_insync&&m_pltzlumi_insync&&m_bcm1flumi_insync)&&twoBeams&&postRamp)<<",NotReadyForInjectionBeamModeInjection "<<(!readyI&&m_machineMode=="INJECTION PROBE BEAM")<<",LumiometersNotReadyForPhysicsFlattop "<<(!readyP&&postRamp&&twoBeams)<<",HFLumiNotInSyncBeamsIn "<<(!m_hflumi_insync&&twoBeams&&postRamp)<<",PLTZLumiNotInSyncBeamsIn "<<(!m_pltzlumi_insync&&twoBeams&&postRamp)<<",BCM1FRHULumiNotInSyncBeamsIn "<<(!m_bcm1flumi_insync&&twoBeams&&postRamp)<<",LuminometersDisagreeFirstCollidingBunch "<<(firstBX&&twoBeams&&postRamp)<<",BKGD1High "<<(bkgd1&&beamIn)<<",BKGD2High "<<(bkgd2&&beamIn)<<",BKGD3High "<<bkgd3<<",PLCCrateNotValid "<<(!m_plcCrateValid)<<",PLTHVLVInError "<< ( !_plthvon || !_pltlvon ||!m_PLTCAEN ||(now-m_PLTCAEN_ts)>30 );
  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
}
void bril::dip::BRILAlarms::subscribeToDip(){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"subscribeToDip ");
  m_dip->setTimeout(3);
  for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
    DipSubscription* s = 0;
    s=m_dip->createDipSubscription(it->first.c_str(),this);
    if(!s){
      m_dipsubs.erase(it); // remove from registry
    }else{
      it->second=s;
    }
  }
}
void bril::dip::BRILAlarms::createDipPublication(){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"createDipPublication");
  for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
    DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
    if(!p){
      LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
      m_dippubs.erase(it); // remove from registry
    }else{
      it->second=p;
    }
  }
}
