#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "bril/dipprocessor/WebUtils.h"
#include "toolbox/mem/HeapAllocator.h"
#include "dip/DipData.h"
#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "toolbox/net/UUID.h"

#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "bril/dipprocessor/PLTAnalyzer.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include <cmath>


#include <iomanip>

XDAQ_INSTANTIATOR_IMPL (bril::dip::PLTAnalyzer)
namespace bril{ 
  namespace dip{
    static int maxnbx=3564;

    PLTAnalyzer::PLTAnalyzer(xdaq::ApplicationStub* s): xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
      xgi::framework::deferredbind(this,this,&bril::dip::PLTAnalyzer::Default, "Default");
      b2in::nub::bind(this, &bril::dip::PLTAnalyzer::onMessage);
      toolbox::net::UUID uuid;
      m_processuuid = uuid.toString();
      m_fillnum = 0;
      m_runnum = 0;
      m_lsnum = 0;
      m_nbnum = 0;
      m_tssec = 0;
      m_tsmsec = 0;
      m_bus.fromString("brildata");//default
      m_busready = false;
      m_dipRoot = "bril/CMSTEST/";

      nLS = 0;
      nLS_max = 16;
      badTCDS = false;

      binSize = maxnbx;
      abortGapSize = 120;

      _avgLuminosity = 0.0; _avgLuminosityError = 0.0;
      _avgraw = 0.0; _avgrawError = 0.0;
      memset(_bxraw,0,sizeof(_bxraw)); memset(_bx,0,sizeof(_bx));
      _bxSum = 0.0; _bxrawSum = 0.0;
      memset(_bxtail,0,sizeof(_bxtail));memset(_bxrawtail,0,sizeof(_bxrawtail));


      bkgd = false;
      allChAvg = 0.0;

      memset(_chAvgCoincs, 0, sizeof(_chAvgCoincs));
      memset(_pOFAvg, 0, sizeof(_pOFAvg));


      // resize vectors to hold channels values
      _chLS.resize(chCount);
      m_mon_lumivec.resize(chCount);

      memset(_chTripCounter,0,sizeof(_chTripCounter));

      for (int i =0; i < chCount; i++){
	m_mon_lumivec[i] = 0;
        _chLS[i].resize(nLS_max,0);
      }

      memset(_chHits,0,sizeof(_chHits));
      memset(_chHitsT,0,sizeof(_chHitsT));
      memset(_chHitsZ,0,sizeof(_chHitsZ));
      memset(_chHitsSum,0,sizeof(_chHitsSum));
      memset(_lumi_track_telescope,0,sizeof(_lumi_track_telescope));
      memset(_lumi_zero_telescope,0,sizeof(_lumi_zero_telescope));
    
    

      m_diplocation ="dip/CMSTEST/BRIL/PLT/";

      _beamstatus = "";

      try{
        getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
        getApplicationInfoSpace()->fireItemAvailable("dipSubs",&m_dipSubStr);
        getApplicationInfoSpace()->fireItemAvailable("dipPubs",&m_dipPubStr);
	getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRootArg);
        getApplicationInfoSpace()->fireItemAvailable("dipServiceName",&m_dipServiceName);
        getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
      } catch (xdata::exception::Exception& e){
        std::stringstream msg;
        msg<<"Failed to set up infospace";
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
      }



      try {
	std::string nid("pltMon");
	std::string monurn = createQualifiedInfoSpace(nid).toString();
	m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));

        m_monInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
	m_monItemList.push_back("nbnum");

        m_monInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
	m_monItemList.push_back("fillnum");


        m_monInfoSpace->fireItemAvailable("runnum",&m_runnum);
	m_monItemList.push_back("runnum");


        m_monInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
	m_monItemList.push_back("lsnum");

	m_monInfoSpace->fireItemAvailable("pltChMonVec",&m_mon_lumivec);
	m_monItemList.push_back("pltChMonVec");

	m_monInfoSpace->addGroupRetrieveListener(this);
      } catch(xdaq::exception::Exception& e){
	std::stringstream msg;
	msg<<"Failed to create infospace ";
	LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      }
    }


    PLTAnalyzer::~PLTAnalyzer(){}
    void PLTAnalyzer::Default(xgi::Input * in, xgi::Output * out){}



    void PLTAnalyzer::actionPerformed(xdata::Event& e){
      if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        try{
          xdata::String* busName = dynamic_cast<xdata::String*>(getApplicationInfoSpace()->find("bus")) ;
          if(busName) m_bus = busName->value_;
        }catch(xdata::exception::Exception& e){
          std::string msg("Failed to find busName in ApplicationInfoSpace ");
          LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        }    
        this->getEventingBus(m_bus).addActionListener(this);
        
        try{
          std::cout<<"subscribing to "<<interface::bril::pltlumizeroT::topicname()<<std::endl;
	  std::cout<<"subscribing to "<<interface::bril::pltlumiT::topicname()<<std::endl;
	  std::cout<<"subscribing to "<<interface::bril::pltagghistT::topicname()<<std::endl;
	  std::cout<<"subscribing to "<<interface::bril::pltaggzeroT::topicname()<<std::endl;
	  std::cout<<"subscribing to "<<interface::bril::beamT::topicname()<<std::endl;
          this->getEventingBus(m_bus.value_).addActionListener(this);
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltagghistT::topicname());
	  this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltaggzeroT::topicname());
	  this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltlumiT::topicname());
	  this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltlumizeroT::topicname());
	  this->getEventingBus(m_bus.value_).subscribe(interface::bril::beamT::topicname());

        }catch(eventing::api::exception::Exception& e){
          std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
          LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
          XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);     
        } 
        std::cout<<" DIPSERVCEINAME "<<m_dipServiceName.value_<<std::endl;
        std::string dipsubscriber(m_dipServiceName.value_);
        m_dip = Dip::create(dipsubscriber.c_str());
        m_servererrorhandler=new ServerErrorHandler;
        Dip::addDipDimErrorHandler(m_servererrorhandler);
        m_puberrorhandler = new PubErrorHandler;   
        m_dippubs.insert(std::make_pair(m_diplocation+"pltagghist",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_diplocation+"pltaggzero",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_diplocation+"pltlumi",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_diplocation+"pltlumizero",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_diplocation+"DipAlarms",(DipPublication*)0 ));

        createDipPublication();
    
 
      }
    }

    void PLTAnalyzer::actionPerformed(toolbox::Event& e){  
      if( e.type() == "eventing::api::BusReadyToPublish" ){
        m_busready = true;
      }
    }

    void PLTAnalyzer::subscribeToDip(){
      LOG4CPLUS_INFO(getApplicationLogger(),"subscribeToDip ");
      m_dip->setTimeout(3);
      for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
        DipSubscription* s = 0;
        s=m_dip->createDipSubscription(it->first.c_str(),this);
        if(!s){
          m_dipsubs.erase(it); // remove from registry
        }else{
          it->second=s;
        }
      }
    }
    void PLTAnalyzer::createDipPublication(){
      for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
        if(!p){
          LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
          m_dippubs.erase(it); // remove from registry
        }else{
          it->second=p;
        }
      }
    }
    
    void PLTAnalyzer::connected(DipSubscription* dipsub){
      LOG4CPLUS_INFO(getApplicationLogger(),"connected to dip publication source "+std::string(dipsub->getTopicName()) );
    }
    
    void PLTAnalyzer::disconnected(DipSubscription* dipsub,char *reason){
      LOG4CPLUS_INFO(getApplicationLogger(),"disconnected from dip publication source "+std::string(dipsub->getTopicName()) );
    }
    
    void PLTAnalyzer::handleException(DipSubscription* dipsub, DipException& ex){
      LOG4CPLUS_INFO(getApplicationLogger(),"Publication source "+std::string(dipsub->getTopicName())+" exception "+std::string(ex.what()));
    }
 


    void PLTAnalyzer::handleMessage(DipSubscription* dipsub, DipData& message){
      std::string subname(dipsub->getTopicName());
      LOG4CPLUS_INFO(getApplicationLogger(),"PLTAnalyzer::handleMessage from "+subname);
      if(message.size()==0) return;
  
    }




    void PLTAnalyzer::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        //std::cout<<"in PLTAnalyzer, topic "<<topic<<std::endl;
            
	//	std::cout << "got a message with topic " << topic << std::endl;

	interface::bril::shared::DatumHead* lumiheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());

	//toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
	unsigned int max_ch = 15;
	//double secs = t.sec();
	//double usecs = t.usec();
	//double ts_1 =  secs*1000000. + usecs;
	//double ts_2 = ts_1 - 1458000;
	// hard coding that the IoV of a lumi period is 4 nibbles.                                                                             
	//float ts_center = (ts_1 + ts_2)/2000.0;
	//superlong tsmilliseconds = static_cast<superlong>(ts_center);
	DipTimestamp timestamp;

	//int ls = lumiheader->lsnum;
        int currNb = lumiheader->nbnum;
        int nbDiff = (abs(int(currNb - prevNb))) % 100 ;// divide by any number greater than 4
	                                           //	eg. (25-20) % 10 ==5
	if(currNb < 8) {                           // but if currNB = 5, prevNB = 64,
                                                   // (4-64) % 10 =0, but we want 4
	  nbDiff = (abs(64-prevNb) + currNb) % 100;// what if prevNB != 64?
	}
	double NbRatio = (nbDiff)/4.0;             // normalize this shit per NB4



	///////////////////////////////////////////////////////////
	//////////////// pltagghist and pltzggzero ////////////////
	///////////////////////////////////////////////////////////                                                                            

	if(topic == interface::bril::pltagghistT::topicname()) {
	  interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
	  unsigned int channelId = inheader->getChannelID();

	  //	  std::cout << "Channel Id: "<<channelId << std::endl;


	  interface::bril::pltagghistT* dataptrAgg=(interface::bril::pltagghistT*)ref->getDataLocation();

	  int tot = 0;
	  int fill = dataptrAgg->fillnum;
	  int run = dataptrAgg->runnum;
	  int ls = dataptrAgg->lsnum;
	  int  nb = dataptrAgg->nbnum;

	  //	  std::cout << ls << " " << nb << " " << channelId << std::endl;

	  m_nbnum = nb;
	  m_fillnum = fill;
	  m_runnum = run;
	  m_lsnum = ls;

          runChange = run > LastRun;
          LastRun = run;

	  for(int i = 0; i<binSize; i++){
	    tot+=dataptrAgg->payload()[i];
	  }

	  // "normalize" hits by nibble numbers. nb=4==>1.
	  tot = tot / NbRatio;                    // usually, NbRatio == 1.0

	  _chHitsT[channelId] = tot; 
	  _chHitsSum[channelId] += tot; 

	  allChAvg += tot; // for all channels

          // check for alert situations only when nb>60 i.e. every LS
	  // currNb is a global variable, defined from pltagghistT
          if (m_nbnum > 60U) {

	    //	    std::cout << "Aggregating after 60nb "<<nLS << " " << nLS_max << " " << channelId << std::endl;

	    // nLS_max lumi sections
	    if (nLS >= nLS_max) {
	      nLS = 0;
	      badTCDS = false; // check for badTCDS every nLS_max nibbles
	    }


	    //_chLS is a matrix of size chCount*nLS_max, rows = channels, columns contains coincidences

	    _chLS[channelId][nLS] = _chHitsSum[channelId];//for 16NB4==>1 LS
	    _chHitsSum[channelId] = 0;



	    // sum last N coincindences per LS for this channel
	    double chavgNLS = 0.0;
	    int i = 0;
	    for (std::vector<int>::iterator it =  _chLS[channelId].begin(); it !=  _chLS[channelId].end(); ++it) {
	      //	      std::cout << channelId << " " << i << " " << _chLS[channelId][i] << std::endl;
	      chavgNLS +=_chLS[channelId][i];
	      i++;
	    }


	    chavgNLS = chavgNLS/nLS_max;
	    _chAvgCoincs[channelId] = chavgNLS;

	    int thisLS = _chLS[channelId][nLS];


	    if (chavgNLS > 0) {
	      double pOff = abs((thisLS - chavgNLS)*100/chavgNLS);
	      _pOFAvg[channelId] = pOff;
	    }

	  } //m_nbnum > 60


	  // At the end of the stream, publish

	  if((channelId == max_ch)) {


	    // update m_mon_lumivec values at every LS ==> NB==4
	    if (m_nbnum <= 7U) {
	      
	      for (int i = 0; i < chCount; i++){
		m_mon_lumivec[i] = _chHitsT[i];
	      }
	      
	      m_monInfoSpace->fireItemGroupChanged( m_monItemList, this );

	      //	      std::cout << "Sent to monInfoSpace" << std::endl;
	    }




	    // sends one time alert per beam status.
	    // badTCDS needs to be reset to false at some point to avid multiple email alerts   
	    // for now, it gets reset to false when there's change in beam status, and when nLS >=nLS_max
	    if (!badTCDS  //send alarm only at certain beam status? 
		//		&& (_beamstatus =="STABLE BEAMS" || _beamstatus == "SQUEEZE" || _beamstatus =="RAMP")
		&& ( run == 0 || fill == 0 || ls == 0 || nb == 0 || run > 9999999 || fill > 99999 || ls > 9999 || nb > 64)) {

	      badTCDS = true;
		
		
	      std::stringstream runNum,fillNum, lsNum, nbNum;
	      runNum << run;
	      fillNum << fill;
	      nbNum << nb;
	      lsNum << ls;

	      std::string nbStr = "Current values from TCDS\n Nibble: " + std::string(nbNum.str()) + "\n";
	      std::string lsStr =  " ls: " + std::string(lsNum.str()) +"\n";
	      std::string fillStr = " Fill: " + std::string(fillNum.str())+ "\n";
	      std::string runStr = " run: " + std::string(runNum.str())+ "\n";


	      std::string bs = " Beam status: " + _beamstatus + "\n";		

	      std::string s1 = "echo -e";
	      std::string s2 = "'Subject: Bad TCDS \n\n Bad data from TCDS!!!";

	      std::string body = nbStr + lsStr + fillStr + runStr + bs + " '";

	      std::string s3 = " | sendmail 'kthapa@cern.ch' 'paul.lujan@cern.ch' ";

	      std::string s4 = s1 + " " + s2 + " " + body + s3;
	      std::string msg = s2 + " " + body;
	      std::cout<<msg.c_str()<<std::endl;
	      //	      system(s4.c_str());


	    }



	    if (m_nbnum > 60U) {
	      nLS++;
	    }
	      

	    prevNb = currNb;
	      
	    DipData * dataAgg = m_dip->createDipData();


	    dataAgg->insert(int(channelId),     "ChannelId"   );
	    dataAgg->insert(int(fill),     "fillNum"   );
	    dataAgg->insert(int(run),     "runNum"   );
	    dataAgg->insert(int(ls),     "lsNum"   );
	    dataAgg->insert(int(nb),     "nbNum"   );

	    dataAgg->insert((DipInt*)&_chHitsT, chCount, "tot_hit_telescope");
	    dataAgg->insert(topic,"Source");      
	    dataAgg->insert(static_cast<DipDouble>(1.458),"IntTime"    );


	    std::string diptopic(m_diplocation+"pltagghist");

	    try{
	      m_dippubs[diptopic]->send(*dataAgg,timestamp);
	      delete dataAgg;
	    } catch(DipException & ex){
	      std::cout<<ex.what()<<std::endl;
	    }

	  } // ChID == max_ch
	} //pltagghistT topic
	  

	
	if(topic == interface::bril::pltaggzeroT::topicname()) {
	  interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());


	  unsigned int channelId = inheader->getChannelID();

	  interface::bril::pltaggzeroT* dataptrZero=(interface::bril::pltaggzeroT*)ref->getDataLocation();

	  int fill = dataptrZero->fillnum;
	  int run = dataptrZero->runnum;
	  int ls = dataptrZero->lsnum;
	  int  nb = dataptrZero->nbnum;

	  float n_zeros = 0;
	  float tot = 0;
	  if (topic == interface::bril::pltaggzeroT::topicname()){
	    for(int i = 0; i < binSize; i++){
	      n_zeros = dataptrZero->payload()[i];
	      tot = -std::log(n_zeros/(4096*4));
	    }
	    _chHitsZ[channelId] = tot;
	    //	    _chHitsZ[channelId] = 0.0 / 0.0;
	  }


	  if((channelId == max_ch)) {

	    DipData * dataAgg = m_dip->createDipData();


	    dataAgg->insert(int(channelId),     "ChannelId"   );
	    dataAgg->insert(int(fill),     "fillNum"   );
	    dataAgg->insert(int(run),     "runNum"   );
	    dataAgg->insert(int(ls),     "lsNum"   );
	    dataAgg->insert(int(nb),     "nbNum"   );
	    dataAgg->insert((DipInt*)&_chHitsZ, chCount, "tot_hit_telescope");
	    dataAgg->insert(topic,"Source");      
	    dataAgg->insert(static_cast<DipDouble>(1.458),"IntTime"    );
	    std::string diptopic(m_diplocation+"pltaggzero");

	    try {
	      m_dippubs[diptopic]->send(*dataAgg,timestamp);
	      delete dataAgg;
	    } catch(DipException & ex){
	      std::cout<<ex.what()<<std::endl;
	    }
	  }      
	}
	///////////////////////////////////////////////////////////
	//////////////// pltlumi, pltlumizero, and beam ///////////
	///////////////////////////////////////////////////////////

	else if((topic == interface::bril::pltlumiT::topicname()) || (topic == interface::bril::pltlumizeroT::topicname()) || topic == interface::bril::beamT::topicname()) {


	  //      std::cout << "processing "<< topic << std::endl;                           

	  // these topics come one after another but they have same attributes
	  // use same data pointer for both pltlumi and pltlumizero so that
	  // we can use same algorithm for publication

	  interface::bril::pltlumiT* dataptr=(interface::bril::pltlumiT*)ref->getDataLocation();
	  interface::bril::shared::CompoundDataStreamer tc(interface::bril::pltlumiT::payloaddict());

	  // as written this code is completely wrong since these variables go out of scope as soon as we
	  // leave the if block. to preserve previous behavior I've just commented this out entirely. but
	  // this should be fixed at some point. -- PJL 2017/04/06
	  // if (topic == interface::bril::pltlumizeroT::topicname()){
	  //   //      std::cout << " changing dataptr " << std::endl;                                                                            
	  //   interface::bril::pltlumizeroT* dataptr=(interface::bril::pltlumizeroT*)ref->getDataLocation();
	  //   interface::bril::shared::CompoundDataStreamer tc(interface::bril::pltlumizeroT::payloaddict());
	  // }


	  //---------------------- Extract data ------------------------------                                                                 

	  if (topic == interface::bril::beamT::topicname()){
	    interface::bril::beamT* dataptrBeam = (interface::bril::beamT*)ref->getDataLocation();

	    interface::bril::shared::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());

	    int ls = dataptrBeam->lsnum;
	    int fill = dataptrBeam->nbnum;
	    int nb = dataptrBeam->lsnum;
	    int run = dataptrBeam->lsnum;


	    char machinemode[50];
	    tc.extract_field(machinemode, "status",  dataptrBeam->payloadanchor);

	    if(_beamstatus != std::string(machinemode)) {

	      badTCDS = false;


	      
	      //	      std::string s1 = "echo -e";
	      std::string subj = "'Subject: Beam Status Changed \n\n";
	      //	      std::string send = "' | sendmail 'kthapa@cern.ch'";
	      //	      std::string all = s1 + " " + subj + " From "+ _beamstatus + " to "+ std::string(machinemode) + send;
	      //	      system(all.c_str());



	      // save timestamp and beam_status change info to file
	      time_t tme = time(0);
	      struct tm * now = localtime( & tme);

	      
	      std::cout << "Beamstatus changed from " 
			<< _beamstatus << " to " << std::string(machinemode) 
			<< " at " << (now->tm_year + 1900) << "-"
			<< (now->tm_mon + 1) << "-"
			<< now->tm_mday << "-"
			<< now->tm_hour <<"-"
			<< now->tm_min << "-" << now->tm_sec<< std::endl;	      

	    }



	    _beamstatus = std::string(machinemode);
	    


	    // end of this LS when currNB > 60
	    // update perLS variables
	    
	    if (m_nbnum > 60U) {


	      // allChavg is global
	      allChAvg = allChAvg / (chCount * 16);  // 16 4NBs
	      // If avg. from all channels is << 500, it is most likely just background
	      // Or, mostly from beamstatus other than STABLE
	      // Generally, this mechanism isn't stable for no-STABLE BEAMS==>many alarms
	      // maybe smooth out avg. deviation % by adding it over couple of LS
	      // if you'd like to receive alarms when avg. < 500

	      if (allChAvg < 500) { // dare you to lower me
		bkgd = true;
	      } else {
		bkgd = false;
	      }


	      // what if all channels are down/up by same %?
	      // look at avg. and see the deviation from avg. to define what a "trip" is
	      double ppAvg = 0.0;

	      //changed starting to 1 since ch0 is basically out because of mask
	      // back to 0 now that all 16 channels are online
	      for (int i = 0; i < chCount; i++){
		ppAvg += _pOFAvg[i];
	      }



	      ppAvg = ppAvg / (chCount); // get the avg. percentage deviation             
	      double pOfpOfAvg = 0;
	      tripCounter = 0;

	      for (int i = 0; i < chCount; i++){
		pOfpOfAvg = abs(int(100*(_pOFAvg[i] - ppAvg)/ ppAvg));
		//reset that _pOFAvg array with ppoff
		_pOFAvg[i] = pOfpOfAvg;

		if (pOfpOfAvg > 30) {
		  //i=0 is special because of mask
		  _chTripCounter[i] = 1;
		  //		  if(i > 0){
		    tripCounter +=1;
		    //		  }
		} else {
		  _chTripCounter[i] = 0;
		}
	      }
	      //	      _pOFAvg[0] = 0;

	      //per channel. needs to be fixed for all channels                          
	      if (!bkgd && tripCounter !=0 && tripCounter != 13 && nLS % 3 == 0 
		  //		  && (_beamstatus =="STABLE BEAMS" || _beamstatus == "SQUEEZE" || _beamstatus =="FLAT TOP" || _beamstatus == "RAMP")){
		  && (_beamstatus =="STABLE BEAMS" || _beamstatus == "SQUEEZE" || _beamstatus =="FLAT TOP" || _beamstatus == "ADJUST")){
			       		
		std::stringstream cnbStr,crunStr,cfillStr, clsStr, trC,tAvgStr;

		cnbStr << m_nbnum;
		crunStr << m_runnum;
		clsStr << m_lsnum;
		cfillStr << m_fillnum;
		trC << tripCounter;
		tAvgStr << allChAvg;

		std::string cfill = "fillNum: " + std::string(cfillStr.str()) + "\n";
		std::string crun = "runNum: " + std::string(crunStr.str()) + "\n";
		std::string cls = "lsNum: " + std::string(clsStr.str()) + "\n";
		std::string cnb = "nbNum: " + std::string(cnbStr.str()) + "\n";

		std::string trCstr =  "#Tripped channels: " + std::string(trC.str()) +"\n";
		std::string tavg = "#Avg of all channels[this LS]: " + std::string(tAvgStr.str())+ "\n";
		std::string bs = "Beam status: " + _beamstatus+ "\n";

		std::string chTCount = "Tripped channels: ";
		std::string chHits = "abs(#Avg.[16 LS] - #Coinc. [this LS]): ";
		std::string chAvg = "#Avg Coinc. count[16 LS]: ";
		std::string pOff = "%Off from avg. %deviation: ";
		

		for (int i = 0; i < chCount; i++) {
		  std::stringstream chT, chH, chA, chP;

		  chT <<  std::setw(5) << _chTripCounter[i] << "|";
		  chTCount += std::string(chT.str());

		  chH <<  std::setw(10) << abs (int(_chAvgCoincs[i]) /  int(nLS_max -_chHitsT[i])) << "|";
		  chHits += std::string(chH.str());

		  chA <<  std::setw(10) << int(_chAvgCoincs[i]) /  nLS_max << "|";
		  chAvg += std::string(chA.str());

		  chP <<  std::setw(10) << int(_pOFAvg[i])  << "|";
		  pOff += std::string(chP.str());

		}
	

		std::string s1 = "echo -e";
		std::string s2 = "'Subject: Dip Alarm-Channels tripped \n\n";

		std::string body = trCstr + chTCount + "\n" + pOff + "\n" + chAvg + "\n" + chHits + "\n" + bs + cfill + crun + cls + cnb + tavg + " '";

		std::string s3 = " | sendmail  'kthapa@cern.ch' ";
		//std::string s3 = " | sendmail  'kthapa@cern.ch' 'paul.lujan@cern.ch' ";

		std::string s4 = s1 + " " + s2 + " " + body + s3;
		std::string msg = s2 + " " + body;
		std::cout<<msg.c_str()<<std::endl;

		//		system(s4.c_str());
		
	      } // alarm condition met
	      allChAvg = 0;
	    } // every ~64 nibble	   



	    DipData * dataAlarm= m_dip->createDipData();
	    dataAlarm->insert((DipInt*)&_chTripCounter, chCount, "Channel Trips"); 
	    dataAlarm->insert(int(tripCounter),     "#ChTrip"   );
	    dataAlarm->insert(bool(badTCDS),     "badTCDS"   );

	    dataAlarm->insert(int(ls),     "ls"   );
	    dataAlarm->insert(int(nb),     "nb"   );
	    dataAlarm->insert(int(fill),     "fill"   );
	    dataAlarm->insert(int(run),     "run"   );
	    dataAlarm->insert(int(allChAvg / max_ch),     "chavg"   );
	    dataAlarm->insert((DipInt*)&_chAvgCoincs, chCount, "Avg Coincs"); 

	    std::string diptopic(m_diplocation+"DipAlarms");

	    try {
	      m_dippubs[diptopic]->send(*dataAlarm,timestamp);
	      delete dataAlarm;
	    } catch(DipException & ex){
	      std::cout<<ex.what()<<std::endl;
	    }


	  } //end of beam topics

	  // use same algorithm for pltlumi and pltlumizero
	  // publish to different location depending on which topic it is

	  tc.extract_field(&_avgLuminosity, "avg",  dataptr->payloadanchor);
	  tc.extract_field(&_avgraw, "avgraw",  dataptr->payloadanchor);
	  tc.extract_field(&_bx,"bx",dataptr->payloadanchor);
	  tc.extract_field(&_bxraw,"bxraw",dataptr->payloadanchor);
	  //	std::string lumicalibStr(m_calibtag);
	  //	  tc.insert_field( lumiheader->payloadanchor, "calibtag" , lumicalibStr.c_str() );


	  _avgLuminosityError = sqrt(_avgLuminosity);
	  _avgrawError = sqrt(_avgraw);

	  int bxZero = 0; int bxrawZero = 0;
	  float bxtailSum = 0.0; float bxrawtailSum = 0.0;

	  for(int i = 0;i < binSize;i++) {
	    if(i < (binSize - abortGapSize)) {
	      _bxSum += _bx[i];
	      if(_bx[i] == 0.0){
		bxZero++;
	      }
	    } else {
	      bxtailSum += _bx[i];
	      _bxtail[i - 3444 - 1] = _bx[i];   //@ bin 3564-120=3444, bxtail[0]                                                               
	    }
	  }

	  for(int i = 0;i < binSize;i++) {
	    if(i < (binSize - abortGapSize)) {
	      _bxrawSum += _bxraw[i];
	      if(_bxraw[i] == 0.0) {
		bxrawZero++;
	      }
	    } else {
	      bxrawtailSum += _bxraw[i];
	      _bxrawtail[i - 3444 - 1] = _bxraw[i];   //@ bin 3564-120=3444, bxrawtail[0]                                                      
	    }
	  }

	  //	  std::cout << std::setprecision(8) << " time: " << ts_1 << " " << ts_2 << " " << ts_center << " " << t.sec() << " " << t.usec() << " " << " CollRate: " << _avgLuminosity<<" raw: "<< _avgraw <<" bxTailsum: "<<bxtailSum<<" bxrawTailSum "<<bxrawtailSum<<std::endl;   


	  //---------------------- Insert data for Dip publication------------------------------                                               
	  DipData * data = m_dip->createDipData();


	  data->insert(static_cast<DipDouble>(_avgLuminosity),  "CollRate");
	  data->insert(static_cast<DipDouble>(_avgLuminosityError),  "CollRateError");
	  data->insert(static_cast<DipDouble>(_avgraw),"avgRawBx");
	  data->insert(static_cast<DipDouble>(_bxSum),"bxSum");
	  data->insert((int)bxZero,"bxZeros");
	  data->insert(static_cast<DipDouble>(bxtailSum),     "bxTailSum");
	  data->insert((DipFloat*)&_bx, binSize, "bx");
	  data->insert((DipFloat*)&_bxtail, 120, "bxTail");

	  data->insert(static_cast<DipDouble>(_bxrawSum),"bxrawSum");
	  data->insert((int)bxrawZero,"bxrawZeros");
	  data->insert(static_cast<DipDouble>(bxrawtailSum), "bxrawTailSum"   );
	  //	  data->insert((DipFloat*)&_bxraw, binSize, "bxraw");
	  data->insert((DipFloat*)&_bxrawtail, 120, "bxrawTail");

	  data->insert(static_cast<DipDouble>(1.458),"IntTime"    );
	  data->insert(topic,"Source");      



	  //---------------------- Send data ------------------------------                                   // to avoid the spikes during change of run                                             
	  //	  if (!runChange) {

	  //pltlumi                                                                                                                            
	  if (topic == interface::bril::pltlumiT::topicname()) {
	    std::string diptopic(m_diplocation+"pltlumi");
	    try {
	      m_dippubs[diptopic]->send(*data,timestamp);
	      delete data;
	    } catch(DipException & ex) {
	      std::cout<<ex.what()<<std::endl;
	    }
	  }
	  //pltlumizero                                                                                                                        
	  else if (topic == interface::bril::pltlumizeroT::topicname()) {
	    std::string diptopic(m_diplocation+"pltlumizero");
	    try {
	      m_dippubs[diptopic]->send(*data,timestamp);
	      delete data;
	    } catch(DipException & ex) {
	      std::cout<<ex.what()<<std::endl;
	    }
	  }
	  //	  }
	  //---------------------- Reset variables ------------------------------                                                              
	  //    ref->release();                                                                                                                
	  _avgraw = 0.0; _avgrawError = 0.0;
	  _avgLuminosity = 0.0; _avgLuminosityError = 0.0;
	  memset(_bxraw,0.0,sizeof(_bxraw)); memset(_bx,0,sizeof(_bx));
	  _bxSum = 0.0; _bxrawSum = 0.0;
	  memset(_bxtail,0.0,sizeof(_bxtail));memset(_bxrawtail,0,sizeof(_bxrawtail));
 
	}
	
      }

      
      if(ref!=0){ 
	ref->release(); 
	ref=0; 
      }       
    }
  }
}

