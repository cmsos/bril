#include "xcept/tools.h"
#include "xgi/framework/Method.h"
#include "toolbox/net/UUID.h"
#include "xdata/InfoSpaceFactory.h"
#include "b2in/nub/Method.h"
#include "dip/DipData.h"
#include "dip/Dip.h"
#include "toolbox/task/Guard.h"
#include "bril/dipprocessor/LumiTopicsPublisher.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BCM1FUTCATopics.hh"

#include "interface/bril/HFTopics.hh"

// New
#include "interface/bril/REMUSTopics.hh"

#include "interface/bril/shared/LUMITopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include <cmath>
#include <cstdio>

XDAQ_INSTANTIATOR_IMPL (bril::dip::LumiTopicsPublisher)
namespace bril{ 
  namespace dip{
    static int maxnbx=3564;

    LumiTopicsPublisher::LumiTopicsPublisher(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
      xgi::framework::deferredbind(this,this,&bril::dip::LumiTopicsPublisher::Default, "Default");
      b2in::nub::bind(this, &bril::dip::LumiTopicsPublisher::onMessage);
      m_isfirsttcds = true;
      m_bus.fromString("brildata");//default
      m_dipRoot.fromString("dip/CMS/");//default
      
      m_remuslumi = -1;
      m_remuslumiRaw = -1;
      
      m_dtlumi = -1;
      m_dtlumiRaw = -1;
      
      m_hflumi=-1;
      m_hflumiRaw=-1;
      m_maxhf=0;
      m_binhf=0;
      
      m_hflumiet=-1;
      m_hflumietRaw=-1;
      m_maxhfet=0;
      m_binhfet=0;
      
      m_bcmflumi=-1;
      m_bcmflumiRaw=-1;
      m_maxbcmf=0;
      m_binbcmf=0;
      
      m_bcmfutcalumi=-1;
      m_bcmfutcalumiRaw=-1;
      m_maxbcmfutca=0;
      m_binbcmfutca=0;
      
      m_pltlumi=-1;
      m_pltlumiRaw=-1;
      m_maxplt=0;
      m_binplt=0;
      
      m_pltlumizero=-1;
      m_pltlumizeroRaw=-1;
      m_maxpltzero=0;
      m_binpltzero=0;
      
      m_fill=0;
      m_run=0;
      m_lastrun=0;
      m_ls=0;
      m_nb=0; 
      m_bestfill=0;
      m_bestrun=0;
      m_bestls=0;
      m_bestnb=0;
      m_ts=0;
      m_tms=0;            
      m_nColliding=0;
      m_bestlumits=0;
      m_deadfrac=1; 
      m_avgPileup=0.0;
      m_rmsPileup=0.0;
      m_lumiError=0.0;
      
      m_mode="UNKNOWN";
      m_lumi = -1;
      m_source="UNKNOWN";
      memset (m_bxdelivered,-1,sizeof(m_bxdelivered));
      for(int i=0; i<maxnbx; ++i){
          m_collmask[i] = false;
      }
      memset (m_collidingCurrents,-1,sizeof(m_collidingCurrents));
      memset (m_specificLumi,-1,sizeof(m_specificLumi));

      //lumisection plots
      ls_hflumi = 0;
      ls_hflumiet = 0;
      ls_bcmflumi = 0;
      ls_bcmfutcalumi = 0;
      ls_pltlumi = 0;
      ls_pltzclumi = 0;
      
      memset (ls_bxhflumi,0,sizeof(ls_bxhflumi));
      memset (ls_bxhflumiet,0,sizeof(ls_bxhflumiet));
      memset (ls_bxbcmfutcalumi,0,sizeof(ls_bxbcmfutcalumi));
      memset (ls_bxbcmflumi,0,sizeof(ls_bxbcmflumi));
      memset (ls_bxpltlumi,0,sizeof(ls_bxpltlumi));
      memset (ls_bxpltzclumi,0,sizeof(ls_bxpltzclumi));
      
      ls_hfCount=0;
      ls_hfetCount=0;      
      ls_bcmfCount = 0;
      ls_bcmfutcaCount = 0;
      ls_pltCount = 0;
      ls_pltzcCount = 0;
      ls_lastPublication = 0;
      ls_lastrun = 0;
      ls_lastls = 0;
      
      getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
      getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
      getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues"); 
      
      m_wl_publishing_todip = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_dippublish","waiting"); 
      m_wl_publishing_todipls = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_dippublishLS","waiting"); 
      m_as_publishing_todip = toolbox::task::bind(this,&bril::dip::LumiTopicsPublisher::publishing_todip,"dippublish");
      m_as_publishing_todipls = toolbox::task::bind(this,&bril::dip::LumiTopicsPublisher::publishing_todipls,"dippublishLS");
      m_last_publishing_todip = toolbox::TimeVal::zero();
      m_last_publishing_todipls = toolbox::TimeVal::zero();
    }

    LumiTopicsPublisher::~LumiTopicsPublisher(){}
    void LumiTopicsPublisher::Default(xgi::Input * in, xgi::Output * out) {}

    void LumiTopicsPublisher::actionPerformed(xdata::Event& e){
      if( e.type()== "urn:xdaq-event:setDefaultValues" ){        
        try{
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltlumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::pltlumizeroT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::bcm1flumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::hfoclumiT::topicname());
	      this->getEventingBus(m_bus.value_).subscribe(interface::bril::hfetlumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::beamT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::shared::bestlumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::tcdsT::topicname());
          
          // new systems
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::remuslumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::shared::dtlumiT::topicname());
          this->getEventingBus(m_bus.value_).subscribe(interface::bril::bcm1futcalumiT::topicname());
          
        }catch(eventing::api::exception::Exception& e){
          std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
          LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
          XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);     
        }     
	toolbox::net::UUID uuid;
	std::string processuuid = uuid.toString();
	m_dip = Dip::create(( std::string("brilLumiTopicPublisher_")+processuuid).c_str() );    
        m_puberrorhandler = new PubErrorHandler;   
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiPLT",(DipPublication*)0 )); //PLT
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiPLTZ",(DipPublication*)0 )); //PLTZero
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiBCM1F",(DipPublication*)0 )); //BCMF
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiBCM1FUTCA",(DipPublication*)0 )); //BCMFUTCA
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiHF",(DipPublication*)0 )); //HF
	    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiHFET",(DipPublication*)0 )); //HFET
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiREMUS",(DipPublication*)0 )); //Remus
    m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiDT",(DipPublication*)0 )); //Remus
 
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiPLT_PerBunch",(DipPublication*)0 )); //PLT
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiPLTZ_PerBunch",(DipPublication*)0 )); //PLTZero
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiBCM1F_PerBunch",(DipPublication*)0 )); //BCMF
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiBCM1FUTCA_PerBunch",(DipPublication*)0 )); //BCMFUTCA
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiHF_PerBunch",(DipPublication*)0 )); //HF
	m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiHFET_PerBunch",(DipPublication*)0 )); //HFET
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"LHC/Luminosity",(DipPublication*)0 ));  //Luminosity
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"LHC/LumiPerBunch",(DipPublication*)0 )); //Per Bunch
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"LHC/SpecificLumiPerBunch",(DipPublication*)0 )); //Per Bunch
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/Luminosity",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiSection/Luminosity",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiSection/LumiPerBunch",(DipPublication*)0 ));
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"BRIL/LumiSection/LumiRatios",(DipPublication*)0 ));
        createDipPublication();
	m_wl_publishing_todip->activate();	
	m_wl_publishing_todipls->activate();
      }
    }

    void LumiTopicsPublisher::actionPerformed(toolbox::Event& e){  
      delete m_dip;
      if(m_puberrorhandler){
	delete m_puberrorhandler; m_puberrorhandler = 0;
      }
    }

    void LumiTopicsPublisher::createDipPublication(){
      LOG4CPLUS_DEBUG(getApplicationLogger(),"createDipPublication: ");
      for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
        if(!p){
          LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
          m_dippubs.erase(it); // remove from registry
        }else{
          it->second=p;
        }
      }
    }

    void LumiTopicsPublisher::accumulateLumiSection(const std::string &lumiDet, const float lumi,const float * bxlumi){
      std::stringstream ss;
      ss<<"accumulateLumiSection: accumuate data from "<<lumiDet<<"instLumi "<<lumi;
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
      ss.str(""); ss.clear();
      if (lumiDet=="HF"){
        ls_hfCount++;
        ls_hflumi+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxhflumi[i]+=bxlumi[i];
        }
      }
      if (lumiDet=="HFET"){
        ls_hfetCount++;
        ls_hflumiet+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxhflumiet[i]+=bxlumi[i];
        }
      }
      if (lumiDet=="BCMF"){
        ls_bcmfCount++;
        ls_bcmflumi+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxbcmflumi[i]+=bxlumi[i];
        }
      }
      if (lumiDet=="BCMFUTCA"){
        ls_bcmfutcaCount++;
        ls_bcmfutcalumi+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxbcmfutcalumi[i]+=bxlumi[i];
        }
      }
      if (lumiDet=="PLT"){
        ls_pltCount++;
        ls_pltlumi+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxpltlumi[i]+=bxlumi[i];
        }
      }
      if (lumiDet=="PLTzero"){
        ls_pltzcCount++;
        ls_pltzclumi+=lumi;
        for (int i=0;i<maxnbx;i++){
          ls_bxpltzclumi[i]+=bxlumi[i];
        }
      }
    }

    void LumiTopicsPublisher::publishLumiSection(){      
        LOG4CPLUS_DEBUG(getApplicationLogger(), "publishLumiSection: Entering");
        m_applock.take();
        ls_lastrun = m_run;
        ls_lastls = m_ls;
        ls_lastPublication = m_ts;
        //do the publications
        //first normalize to the number of entries for each accumulator
        if(ls_hfCount>0){
            ls_hflumi/=ls_hfCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxhflumi[i]/=ls_hfCount;
            }
        }

        if(ls_hfetCount>0){
            ls_hflumiet/=ls_hfetCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxhflumiet[i]/=ls_hfetCount;
            }
        }
      
        if(ls_bcmfCount>0){
            ls_bcmflumi/=ls_bcmfCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxbcmflumi[i]/=ls_bcmfCount;
            }
        }
        
        if(ls_bcmfutcaCount>0){
            ls_bcmfutcalumi/=ls_bcmfutcaCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxbcmfutcalumi[i]/=ls_bcmfutcaCount;
            }
        }
        
        if(ls_pltCount>0){
            ls_pltlumi/=ls_pltCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxpltlumi[i]/=ls_pltCount;
            }
        }
        
        if(ls_pltzcCount>0){
            ls_pltzclumi/=ls_pltzcCount;
            for (int i=0;i<maxnbx;i++){
                ls_bxpltzclumi[i]/=ls_pltzcCount;
            }
        }
      m_applock.give();

      std::stringstream ss;
      ss<<"publishLumiSection: ls_lastrun "<<ls_lastrun<<",s_lastls "<<ls_lastls<<",ls_lastPublication "<<ls_lastPublication<<",ls_hfCount "<<ls_hfCount<<",ls_hflumiet "<<",ls_hfetCount "<<ls_hfetCount<<",ls_hflumiet "<<",ls_bcmfCount "<<ls_bcmfCount<<",ls_bcmfutcaCount "<<ls_bcmfutcaCount<<",ls_pltCount "<<ls_pltCount<<",ls_pltzcCount "<<ls_pltzcCount;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      
      DipTimestamp t;
      DipData* dipdata = m_dip->createDipData();
      std::string diptopicname( m_dipRoot.value_+std::string("BRIL/LumiSection/Luminosity") );
      dipdata -> insert(m_fill,"Fill");
      dipdata -> insert(m_run,"Run");
      dipdata -> insert(m_ls,"LumiSection");
      dipdata -> insert(ls_hflumi,"HF_InstLumi"); 
      dipdata -> insert(ls_hflumiet,"HFET_InstLumi"); 
      dipdata -> insert(ls_bcmflumi,"BCMF_InstLumi");
      dipdata -> insert(ls_bcmflumi,"BCMFUTCA_InstLumi");
      dipdata -> insert(ls_pltlumi,"PLTTK_InstLumi");
      dipdata -> insert(ls_pltzclumi,"PLTZC_InstLumi");
      m_dippubs[diptopicname]->send(*dipdata,t);
      delete dipdata; 

      DipData* dipdata2 = m_dip->createDipData();
      diptopicname = m_dipRoot.value_+"BRIL/LumiSection/LumiPerBunch";
      dipdata2 -> insert(m_fill,"Fill");
      dipdata2 -> insert(m_run,"Run");
      dipdata2 -> insert(m_ls,"LumiSection");
      dipdata2 -> insert((DipFloat*)&ls_bxhflumi,3564,"HF_BXLumi");
      dipdata2 -> insert((DipFloat*)&ls_bxhflumiet,3564,"HFET_BXLumi");
      dipdata2 -> insert((DipFloat*)&ls_bxbcmflumi,3564,"BCMF_BXLumi");
      dipdata2 -> insert((DipFloat*)&ls_bxbcmfutcalumi,3564,"BCMFUTCA_BXLumi");
      dipdata2 -> insert((DipFloat*)&ls_bxpltzclumi,3564,"PLTZC_BXLumi");
      m_dippubs[diptopicname]->send(*dipdata2,t);
      delete dipdata2;  

      DipData* dipdata3 = m_dip->createDipData();
      diptopicname = m_dipRoot.value_+"BRIL/LumiSection/LumiRatios"; 
      dipdata3 -> insert(m_fill,"Fill");
      dipdata3 -> insert(m_run,"Run");
      dipdata3 -> insert(m_ls,"LumiSection");
      float ratio1=1.25;
      float ratio2=1.25;
      if(ls_pltzclumi > 0.1){
        ratio1=ls_hflumi/ls_pltzclumi;
        ratio2=ls_bcmflumi/ls_pltzclumi;
      } 
      if (ratio1<0.75) ratio1=0.75;
      if (ratio1>1.25) ratio1=1.25;
      if (ratio2<0.75) ratio2=0.75;
      if (ratio2>1.25) ratio2=1.25; 
      dipdata3 -> insert(ratio1,"HF/PLTZC");
      dipdata3 -> insert(ratio2,"BCMF/PLTZC");
      ratio1=1.25;
      if(ls_hflumi > 0.1){
        ratio1=ls_bcmflumi/ls_hflumi;
      } 
      if (ratio1<0.75) ratio1=0.75;
      if (ratio1>1.25) ratio1=1.25; 
      dipdata3 -> insert(ratio1,"BCMF/HF");
      m_dippubs[diptopicname]->send(*dipdata3,t);
      delete dipdata3;              

      //zero the accumulators;
      m_applock.take();
      ls_hflumi=0;
      ls_hflumiet=0;
      ls_bcmflumi=0;
      ls_bcmfutcalumi=0;
      ls_pltlumi=0;
      ls_pltzclumi=0;
      memset (ls_bxhflumi,0,sizeof(ls_bxhflumi));
      memset (ls_bxhflumiet,0,sizeof(ls_bxhflumiet));
      memset (ls_bxbcmflumi,0,sizeof(ls_bxbcmflumi));
      memset (ls_bxbcmfutcalumi,0,sizeof(ls_bxbcmfutcalumi));
      memset (ls_bxpltlumi,0,sizeof(ls_bxpltlumi));
      memset (ls_bxpltzclumi,0,sizeof(ls_bxpltzclumi));
      ls_hfCount=0;
      ls_hfetCount=0;
      ls_bcmfCount=0;
      ls_bcmfutcaCount=0;
      ls_pltCount=0;
      ls_pltzcCount=0;      
      m_applock.give();
    }

    void LumiTopicsPublisher::publishToDip(){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishToDip: Entering");
      DipData* dipdata = m_dip->createDipData();
      std::string diptopicname(m_dipRoot.value_+"BRIL/Luminosity");
      dipdata -> insert(m_fill,"Fill");
      dipdata -> insert(m_run,"Run");
      dipdata -> insert(m_ls,"LumiSection");
      dipdata -> insert(m_nb,"LumiNibble");
      dipdata -> insert(m_hflumi,"HF_InstLumi");
      dipdata -> insert(m_maxhf,"HF_MaxBXLumi");
      dipdata -> insert(m_binhf,"HF_BunchMax");
      dipdata -> insert(m_hflumiet,"HFET_InstLumi");
      dipdata -> insert(m_maxhfet,"HFET_MaxBXLumi");
      dipdata -> insert(m_binhfet,"HFET_BunchMax");
      dipdata -> insert(m_bcmflumi,"BCMF_InstLumi");
      dipdata -> insert(m_maxbcmf,"BCMF_MaxBXLumi");
      dipdata -> insert(m_binbcmf,"BCMF_BunchMax");
      
      dipdata -> insert(m_bcmfutcalumi,"BCMFUTCA_InstLumi");
      dipdata -> insert(m_maxbcmfutca,"BCMFUTCA_MaxBXLumi");
      dipdata -> insert(m_binbcmfutca,"BCMFUTCA_BunchMax");
      
      dipdata -> insert(m_pltlumi,"PLT_InstLumi");
      dipdata -> insert(m_maxplt,"PLT_MaxBXLumi");
      dipdata -> insert(m_binplt,"PLT_BunchMax"); 
      dipdata -> insert(m_pltlumizero,"PLTZero_InstLumi");
      dipdata -> insert(m_maxpltzero,"PLTZero_MaxBXLumi");
      dipdata -> insert(m_binpltzero,"PLTZero_BunchMax"); 
      dipdata -> insert(m_nColliding,"Collidable_Bunches"); 
      dipdata -> insert(m_deadfrac,"TCDS_deadFrac");   
      dipdata -> insert(m_source,"LumiSource");
      dipdata -> insert(m_lumi,"InstLumi");
      dipdata -> insert(m_avgPileup,"AvgPileUp");
      dipdata -> insert(m_rmsPileup,"RMSPileUp");
      dipdata -> insert(m_lumiError,"LumiError");

      DipTimestamp t;
      m_dippubs[diptopicname]->send(*dipdata,t);
      delete dipdata;
     
      std::stringstream ss;
      ss<<"publishToDip:  "<<m_fill<<","<<m_run<<","<<m_ls<<","<<m_nb<<",HF_InstLumi "<<m_hflumi<<",HF_MaxBXLumi "<<m_maxhf<<",HF_BunchMax "<<m_binhf<<",HFET_InstLumi "<<m_hflumiet<<",HFET_MaxBXLumi "<<m_maxhfet<<",HFET_BunchMax "<<m_binhfet<<",BCMF_InstLumi "<<m_bcmflumi<<",BCMF_MaxBXLumi "<<m_maxbcmf<<",BCMF_BunchMax "<<m_binbcmf<<",BCMFUTCA_InstLumi "<<m_bcmfutcalumi<<",BCMFUTCA_MaxBXLumi "<<m_maxbcmfutca<<",BCMFUTCA_BunchMax "<<m_binbcmfutca<<",PLTZero_InstLumi "<<m_pltlumizero<<",PLTZero_MaxBXLumi "<<m_maxpltzero<<",PLTZero_BunchMax "<<m_binpltzero<<",Collidable_Bunches "<<m_nColliding<<",TCDS_deadFrac "<<m_deadfrac<<",LumiSource "<<m_source<<",InstLumi "<<m_lumi<<",AvgPileUp "<<m_avgPileup;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      ss.str("");ss.clear();
     
      publishLumiToDip();

      //reset variables already sent
      m_applock.take();
      m_hflumi = -1;
      m_maxhf = 0;
      m_binhf = 0;
      m_hflumiet = -1;
      m_maxhfet = 0;
      m_binhfet = 0;
      m_bcmflumi = -1;
      m_maxbcmf = 0;
      m_binbcmf = 0;
      
      m_bcmfutcalumi = -1;
      m_maxbcmfutca = 0;
      m_binbcmfutca = 0;
      
      m_pltlumi = -1;
      m_maxplt = 0;
      m_binplt = 0;
      m_pltlumizero = -1;
      m_maxpltzero = 0;
      m_binpltzero = 0;
      m_nColliding = 0;
      m_deadfrac = 1;
      m_source ="UNKNOWN";
      m_lumi = -1;
      m_avgPileup = 0;
      m_rmsPileup = 0;
      m_lumiError = 0;
      m_applock.give();
    }

    void LumiTopicsPublisher::publishLumiToDip(){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishLumiToDip: Entering");      
      
      std::stringstream ss;
      DipData* dipdata2 = m_dip->createDipData();
      std::string diptopicname(m_dipRoot.value_+"LHC/Luminosity");
      if( m_source == "" ){
	m_source="HF";
      }
      dipdata2 -> insert(m_source,"Lumi_Source");
      dipdata2 -> insert(m_source,"CollRateSource");
      dipdata2 -> insert(m_lumi,"CollRate");
      float err = 0;
      if(m_lumi>0){
	err = sqrt(m_lumi);
      }
      if( isnan(err) || isinf(err) ){
	ss<<"publishLumiToDip: CollRateErr is NaN or Inf, reset";
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	ss.str("");ss.clear();
	err = 0;
      }

      dipdata2 -> insert(err,"CollRateErr");      
      dipdata2 -> insert(m_lumi,"Lumi_TotInst");
      dipdata2 -> insert((float)1.456,"CollRateIntTime");

      DipTimestamp t;
      m_dippubs[diptopicname]->send(*dipdata2,t);
      delete dipdata2; 

      DipData* dipdata3 = m_dip->createDipData();
      diptopicname=m_dipRoot.value_+"LHC/LumiPerBunch";
      dipdata3 -> insert((DipFloat*)&m_bxdelivered,3564,"Lumi_BunchInst");
      dipdata3 -> insert(m_source,"Source");
      m_dippubs[diptopicname]->send(*dipdata3,t);
      delete dipdata3; 

      for (int i=0; i<maxnbx; i++){ //This processing piece can be done only here when you are sure both bxdelivered and collidingcurrent info both are arrived. Otherwise they are random results
	float s = m_bxdelivered[i]/m_collidingCurrents[i];	  
	if( isnan(s) || isinf(s) ){
	  std::stringstream ss;
	  ss<<"publishLumiToDip: bxdelivered/m_collidingCurrents Nan or Inf at "<<i;	    
	  LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	  ss.str("");ss.clear();
	  s = 0;
	}
	m_specificLumi[i] = s;
      }	    
      
      DipData* dipdata4 = m_dip->createDipData();
      diptopicname=m_dipRoot.value_+"LHC/SpecificLumiPerBunch";
      dipdata4 -> insert((DipFloat*)&m_specificLumi,maxnbx,"SpecificLumi_BunchInst");      
      dipdata4 -> insert(m_source,"Source");
      m_dippubs[diptopicname]->send(*dipdata4,t);
      delete dipdata4;
      
      ss<<"publishLumiToDip: "<<m_fill<<","<<m_run<<","<<m_nb<<",Lumi_Source,CollRateSource "<<m_source<<",CollRate,Lumi_TotInst "<<m_lumi;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str()); 

      m_applock.take();   
      m_lumi = -1;
      memset (m_bxdelivered,-1,sizeof(m_bxdelivered));
      memset (m_specificLumi,-1,sizeof(m_specificLumi));
      memset (m_collidingCurrents,-1,sizeof(m_specificLumi));
      m_applock.give();      
    }  


    void LumiTopicsPublisher::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
        std::string action = plist.getProperty("urn:b2in-eventing:action");
        if (action == "notify") {
            std::string topic = plist.getProperty("urn:b2in-eventing:topic");
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
            std::string data_version  = plist.getProperty("DATA_VERSION");
            if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION) {
                std::string msg("Received brildaq message has no or wrong version, do not process.");
                LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                ref->release(); ref=0;
                return;
            }
        
            // Protection
            std::string lumiDet="UNKNOWN";
            std::string lumiScan="UNKNOWN";
            
            m_preferredScanSource="BCMF";  //should not need this!
            if(ref!=0) {
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                std::string detpayload;
                
                if (topic == interface::bril::tcdsT::topicname()) {
                    //Note: the time header of detector messages can NOT be trusted therefore should NOT be written into the common time information because for example HF nbnum, lsnum are guaranteed to be different from others. We use tcds's time as the common.
                    m_applock.take();
                    m_fill = thead->fillnum;
                    m_run = thead->runnum;
                    m_ls = thead->lsnum;
                    m_nb = thead->nbnum;
                    m_ts = thead->timestampsec;
                    m_tms =thead->timestampmsec;
                    m_applock.give();
                    
                } else if(topic == interface::bril::pltlumiT::topicname()){
                    lumiDet = "PLT";
                    lumiScan = "LumiPLT";
                    detpayload = interface::bril::pltlumiT::payloaddict();
                } else if (topic == interface::bril::pltlumizeroT::topicname()){
                    lumiDet="PLTzero";
                    lumiScan="LumiPLTZ";
                    detpayload = interface::bril::pltlumizeroT::payloaddict();
                } else if (topic == interface::bril::bcm1flumiT::topicname()){
                    lumiDet="BCMF";
                    lumiScan="LumiBCM1F";
                    detpayload = interface::bril::bcm1flumiT::payloaddict();
                } else if (topic == interface::bril::bcm1futcalumiT::topicname()) {
                    lumiDet="BCMFUTCA";
                    lumiScan="LumiBCM1FUTCA";
                    detpayload = interface::bril::bcm1futcalumiT::payloaddict();
                } else if (topic == interface::bril::hfoclumiT::topicname()){
                    lumiDet="HF";
                    lumiScan="LumiHF";
                    detpayload = interface::bril::hfoclumiT::payloaddict();
                } else if (topic == interface::bril::hfetlumiT::topicname()){
                    lumiDet="HFET";
                    lumiScan="LumiHFET";
                    detpayload = interface::bril::hfetlumiT::payloaddict();
                }
                // TODO: add UTCA
                
                if(lumiDet !="UNKNOWN") {
                    float _avgLuminosity = 0;
                    float _avgRawLuminosity = 0;
                    float _avgBx[maxnbx];
                    float _avgBxRaw[maxnbx];
                    
                    interface::bril::shared::CompoundDataStreamer tc(detpayload);
                    tc.extract_field( &_avgLuminosity, "avg", thead->payloadanchor );
                    tc.extract_field( &_avgRawLuminosity, "avgraw", thead->payloadanchor );
                    tc.extract_field( _avgBx, "bx", thead->payloadanchor );
                    tc.extract_field( _avgBxRaw, "bxraw", thead->payloadanchor );
                    
                    //check non-numbers
                    if( isnan(_avgLuminosity)||isinf(_avgLuminosity) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),std::string("onMessage: received avg NaN or Inf from ")+lumiDet);
                        _avgLuminosity = 0;
                    }
                    if( isnan(_avgRawLuminosity)||isinf(_avgRawLuminosity) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),std::string("onMessage: received avgraw NaN or Inf from ")+lumiDet);
                        _avgRawLuminosity = 0;
                    }
                    
                    int maxbx = -1;
                    float maxlumi = 0;
                    for ( int i=0;i<maxnbx;i++ ){
                        if( _avgBx[i]<0){ _avgBx[i]=0; }
                        if( _avgBxRaw[i]<0){ _avgBxRaw[i]=0; }
                        if( _avgBx[i] > maxlumi ){
                            maxlumi = _avgBx[i];
                            maxbx = i;
                        }
                    }
                    
                    m_applock.take();
                    if(lumiDet=="HF"){
                        m_hflumi = _avgLuminosity;
                        m_hflumiRaw = _avgRawLuminosity;
                        m_maxhf = maxlumi;
                        m_binhf = maxbx;
                    }else if(lumiDet=="HFET"){
                        m_hflumiet = _avgLuminosity;
                        m_hflumietRaw = _avgRawLuminosity;
                        m_maxhfet = maxlumi;
                        m_binhfet = maxbx;
                    } else if (lumiDet=="BCMF"){
                        m_bcmflumi = _avgLuminosity;
                        m_bcmflumiRaw = _avgRawLuminosity;
                        m_maxbcmf = maxlumi;
                        m_binbcmf = maxbx;
                    } else if (lumiDet=="BCMFUTCA"){
                        m_bcmfutcalumi = _avgLuminosity;
                        m_bcmfutcalumiRaw = _avgRawLuminosity;
                        m_maxbcmfutca = maxlumi;
                        m_binbcmfutca = maxbx;
                    } else if (lumiDet=="PLT"){
                        m_pltlumi = _avgLuminosity;
                        m_pltlumiRaw = _avgRawLuminosity;
                        m_maxplt = maxlumi;
                        m_binplt = maxbx;
                    } else if (lumiDet=="PLTzero"){
                        m_pltlumizero = _avgLuminosity;
                        m_pltlumizeroRaw = _avgRawLuminosity;
                        m_maxpltzero = maxlumi;
                        m_binpltzero = maxbx;
                    }
                    
                    // TODO: UTCA
                    m_applock.give();
                    
                    //accumulate data in lumisection plots
                    accumulateLumiSection(lumiDet,_avgLuminosity,_avgBx);
                    
                    int detfill = thead->fillnum;
                    int detrun = thead->runnum;
                    int detls = thead->lsnum;
                    int detnb = thead->nbnum;
                    
                    DipData* dipdata = m_dip->createDipData();
                    std::string diptopicname((m_dipRoot.value_+"BRIL/"+lumiScan));
                    dipdata -> insert((float)_avgLuminosity,"Lumi_TotInst");
                    dipdata -> insert((float)1.458,"IntTime");
                    dipdata -> insert(detfill,"Fill");
                    dipdata -> insert(detrun,"Run");
                    dipdata -> insert(detls,"LumiSection");
                    dipdata -> insert(detnb,"Nibble");
                    DipTimestamp t;
                    m_dippubs[diptopicname]->send(*dipdata,t);
                    delete dipdata;
                    
                    DipData* dipdata2 = m_dip->createDipData();
                    diptopicname=m_dipRoot.value_+"BRIL/"+lumiScan+"_PerBunch";
                    dipdata2 -> insert((DipFloat*)&_avgBx,3564,"Lumi_BunchInst");
                    int nonZero=0;
                    for (int i=0; i<3564;i++){
                        if(_avgBx[i]>0){nonZero++;}
                    }
                    dipdata2 -> insert(nonZero,"nonZeroBunches");
                    m_dippubs[diptopicname]->send(*dipdata2,t);
                    delete dipdata2;
                    
                    // End of orbit integrated detectors. Start with DT/REMUS/...
                } else if (topic == interface::bril::remuslumiT::topicname() || topic == interface::bril::shared::dtlumiT::topicname()){
                    
                    // TODO: Add DT, and ...
                    float _avgLuminosity = 0;
                    float _avgRawLuminosity = 0;
                    
                    if (topic == interface::bril::remuslumiT::topicname()) {
                        lumiDet = "REMUS";
                        lumiScan = "LumiREMUS";
                        detpayload = interface::bril::remuslumiT::payloaddict();
                    } else if (topic == interface::bril::shared::dtlumiT::topicname()) {
                        lumiDet = "DT";
                        lumiScan = "LumiDT";
                        detpayload = interface::bril::remuslumiT::payloaddict();
                    }
                    
                    interface::bril::shared::CompoundDataStreamer tc(detpayload);
                    tc.extract_field( &_avgLuminosity, "avg", thead->payloadanchor );
                    tc.extract_field( &_avgRawLuminosity, "avgraw", thead->payloadanchor );
                    
                    //check non-numbers
                    if( isnan(_avgLuminosity)||isinf(_avgLuminosity) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),std::string("onMessage: received avg NaN or Inf from ")+lumiDet);
                        _avgLuminosity = 0;
                    }
                    if( isnan(_avgRawLuminosity)||isinf(_avgRawLuminosity) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),std::string("onMessage: received avgraw NaN or Inf from ")+lumiDet);
                        _avgRawLuminosity = 0;
                    }
                    
                    m_applock.take();
                    if (lumiDet=="REMUS") {
                        m_remuslumi = _avgLuminosity;
                        m_remuslumiRaw = _avgRawLuminosity;
                    } else if (lumiDet=="DT") {
                        m_dtlumi = _avgLuminosity;
                        m_dtlumiRaw = _avgRawLuminosity;
                    }
                    m_applock.give();
                    
                    // Adding dip information
                    int detfill = thead->fillnum;
                    int detrun = thead->runnum;
                    int detls = thead->lsnum;
                    
                    DipData* dipdata = m_dip->createDipData();
                    std::string diptopicname((m_dipRoot.value_+"BRIL/"+lumiScan));
                    dipdata -> insert((float)_avgLuminosity,"Lumi_TotInst");
                    dipdata -> insert((float)1.458,"IntTime");
                    dipdata -> insert(detfill,"Fill");
                    dipdata -> insert(detrun,"Run");
                    dipdata -> insert(detls,"LumiSection");
                    
                    if (lumiDet=="REMUS") {
                        int detnb = thead->nbnum;
                        dipdata -> insert(detnb,"Nibble");
                    }
                    
                    DipTimestamp t;
                    m_dippubs[diptopicname]->send(*dipdata,t);
                    delete dipdata;
                
                } else if(topic == interface::bril::shared::bestlumiT::topicname()) {
                    float _delivered = 0;
                    char _provider[50];
                    float _avgpu=0;
                    interface::bril::shared::CompoundDataStreamer tc(interface::bril::shared::bestlumiT::payloaddict());
                    tc.extract_field(&_delivered, "delivered",  thead->payloadanchor);
                    
                    if( isnan(_delivered) || isinf(_delivered) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),"onMessage: received delivered NaN or Inf from bestlumi");
                        _delivered = 0;
                    }
                    
                    tc.extract_field(&_avgpu, "avgpu",  thead->payloadanchor);
                    if( isnan(_avgpu) || isinf(_avgpu) ){
                        LOG4CPLUS_ERROR(getApplicationLogger(),"onMessage: received avgpu NaN or Inf from bestlumi");
                        _avgpu = 0;
                    }
                    
                    if( _delivered<1E-8 ){
                        _delivered = 0;
                    }
                    
                    m_applock.take();
                    m_bestfill = thead->fillnum;
                    m_bestrun = thead->runnum;
                    m_bestls = thead->lsnum;
                    m_bestnb = thead->nbnum;
                    m_bestlumits = thead->timestampsec;
                    m_lumi = _delivered;
                    m_avgPileup = _avgpu;
                    tc.extract_field(m_bxdelivered, "bxdelivered",  thead->payloadanchor);
                    tc.extract_field(_provider, "provider",  thead->payloadanchor);
                    m_source=std::string(_provider);
                    m_applock.give();
                    
                    if( !m_isfirsttcds ){
                        //On start of process, incomplete set of messages are useless, do not process
                        m_wl_publishing_todip->submit(m_as_publishing_todip);
                    }
                } else if(topic == interface::bril::beamT::topicname()) {
                    float _bx1[maxnbx];
                    float _bx2[maxnbx];
                    bool _con1[maxnbx];
                    bool _con2[maxnbx];
                    char machinemode[50];
                    interface::bril::shared::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());
                    tc.extract_field(_bx1, "bxintensity1",  thead->payloadanchor);
                    tc.extract_field(_bx2, "bxintensity2",  thead->payloadanchor);
                    tc.extract_field(_con1, "bxconfig1",  thead->payloadanchor);
                    tc.extract_field(_con2, "bxconfig2",  thead->payloadanchor);
                    tc.extract_field(machinemode, "status",  thead->payloadanchor);
                    //bool collidable=false;
                    float f;
                    int ncol=0;
                    
                    m_applock.take();
                    m_mode=std::string(machinemode);
                    //if(m_mode=="FLAT TOP" ||m_mode=="SQUEEZE" || m_mode=="ADJUST"||m_mode=="STABLE BEAMS"||m_mode=="UNSTABLE BEAMS"){
                    //  collidable=true;
                    //}
                    for (int i=0; i<maxnbx; i++){
                        m_collidingCurrents[i]=-1;
                        f=_bx1[i]*_bx2[i];
                        m_collmask[i]=false;
                        if (_con1[i]&&_con2[i]){
                            m_collmask[i]=true;
                            ncol++;
                        }
                        if(f>0.0001){
                            m_collidingCurrents[i]=f;
                        }
                    }
                    m_nColliding=ncol;
                    m_applock.give();
                    
                } else if(topic == interface::bril::tcdsT::topicname()) {
                    interface::bril::shared::CompoundDataStreamer tc(interface::bril::tcdsT::payloaddict());
                    float _deadfrac = 1.;
                    tc.extract_field(&_deadfrac, "deadfrac",  thead->payloadanchor);
                    //check against non-numbers
                    if( isnan(_deadfrac)||isinf(_deadfrac) ){
                        _deadfrac = 1;
                    }
                    
                    m_applock.take();
                    m_deadfrac = _deadfrac;
                    if( m_run!=m_lastrun ){
                        m_isfirsttcds = true;
                        m_lastrun = m_run;
                    }
                    m_applock.give();
                    
                    //submit workloops
                    std::stringstream ss;
                    if ( m_isfirsttcds ){
                        //to wait in case it is the first lumi section ever or of a new run
                        m_applock.take();
                        ls_lastls = m_ls;
                        ls_lastPublication = m_ts;
                        m_applock.give();
                        ss<<"onMessage: on tcds topic, do nothing at the beginning "<<m_run<<","<<m_ls;
                        LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
                        m_isfirsttcds = false;
                    } else if( m_ls>1 && ( ls_lastls!=m_ls ) ){
                        ss<<"onMessage: on tcds topic, publish lsdata on ls boundary change from "<<m_run<<":"<<ls_lastls<<" to "<<m_run<<":"<<m_ls;
                        LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
                        m_wl_publishing_todipls->submit(m_as_publishing_todipls);
                    } else if( (m_ts-ls_lastPublication)>25 ){
                        ss<<"onMessage: on tcds topic, desperate publish lsdata 25sec idle since "<<m_run<<":"<<ls_lastls<<" lastPublication time "<<ls_lastPublication;
                        LOG4CPLUS_ERROR(getApplicationLogger(), ss.str());
                        m_wl_publishing_todipls->submit(m_as_publishing_todipls);
                    }
                }
            }
        }
    
        if(ref!=0){
            ref->release();
            ref=0;
        }
    }

    bool LumiTopicsPublisher::publishing_todip(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing_todip");
      toolbox::TimeVal now=toolbox::TimeVal::gettimeofday();//thread execution time
      if( m_last_publishing_todip!=toolbox::TimeVal::zero() && (now-m_last_publishing_todip) < toolbox::TimeVal(0,100000) ){//test thread execution time against submission time to decide if cancel itself.
	LOG4CPLUS_ERROR(getApplicationLogger(), "DIP holdup! workloop task queued (submission time  - last task end time) <100ms ");
	return false;
      }      
      usleep(400000);      
      std::stringstream ss;
      ss<<"publishing_todip: "<<m_bestfill<<  //The Fill Number
	","<<m_bestrun<<      //The Run Number
	","<<m_bestls<<       //The Lumi Segment
	","<<m_bestnb<<       //The NB4 number
	","<<m_mode<<     //Machine Mode
	","<<m_ts<<       //timstemap in seconds
	","<<m_tms<<      //timestamp msec
	","<<m_deadfrac<< //deadfrac from TCDS
	","<<m_source<<   //Source of our primary luminosty
	","<<m_lumi<<     //Inst_lumi of that primary source
	","<<m_hflumi<<   //Inst Lumi from HFLumi
	","<<m_hflumiRaw<<   //Raw Lumi from HFLumi
	","<<m_hflumiet<<   //Inst Lumi from HFLumiET
	","<<m_hflumietRaw<<   //Raw Lumi from HFLumiET
	" ,"<<m_pltlumi<< //Inst Lumi from PLT
	" ,"<<m_pltlumiRaw<< //Raw Lumi from PLT
	" ,"<<m_pltlumizero<< //Inst Lumi from PLT
	" ,"<<m_pltlumizeroRaw<< //Raw Lumi from PLT
	","<<m_bcmflumi<< //Inst Lumi from BCMF
	","<<m_bcmflumiRaw<< //Raw Lumi from BCMF
    ","<<m_bcmfutcalumi<< //Inst Lumi from BCMFUTCA
	","<<m_bcmfutcalumiRaw<< //Raw Lumi from BCMFUTCA
	","<<m_nColliding<<",BX-Lumi";  //Number of collidable bunches from FBCT
      for (int i=0;i<maxnbx;i++){
	if(m_collmask[i]){
	  ss<<","<<i+1<<","<<m_bxdelivered[i]<<","<<m_collidingCurrents[i];
	}  // For each collidable bunch, BX# and Primary Lumi for that BX
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      publishToDip();
      m_last_publishing_todip = toolbox::TimeVal::gettimeofday();//task end  time
      return false;
    }
    
    bool LumiTopicsPublisher::publishing_todipls(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing_todipls");
      publishLumiSection();
      return false;
    }    
    
  }
}

