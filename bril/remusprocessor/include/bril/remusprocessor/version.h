// $Id$

#ifndef _bril_remusprocessor_version_h_
#define _bril_remusprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILREMUSPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILREMUSPROCESSOR_VERSION_MINOR 0
#define BRIL_BRILREMUSPROCESSOR_VERSION_PATCH 1
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILREMUSPROCESSOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILREMUSPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILREMUSPROCESSOR_VERSION_MAJOR,BRIL_BRILREMUSPROCESSOR_VERSION_MINOR,BRIL_BRILREMUSPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILREMUSPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILREMUSPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILREMUSPROCESSOR_VERSION_MAJOR,BRIL_BRILREMUSPROCESSOR_VERSION_MINOR,BRIL_BRILREMUSPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILREMUSPROCESSOR_FULL_VERSION_LIST BRIL_BRILREMUSPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILREMUSPROCESSOR_VERSION_MAJOR,BRIL_BRILREMUSPROCESSOR_VERSION_MINOR,BRIL_BRILREMUSPROCESSOR_VERSION_PATCH)
#endif

namespace brilremusprocessor
{
        const std::string project = "bril";
	const std::string package = "brilremusprocessor";
	const std::string versions = BRIL_BRILREMUSPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ remusprocessor";
	const std::string description = "REMUS processor";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
