// $Id

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"

#include "bril/pltsource/Application.h"
#include "bril/pltsource/WebUtils.h"
#include "bril/pltsource/exception/Exception.h"
#include "interface/bril/PLTTopics.hh"

#include "xdata/Properties.h"

#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/TimerFactory.h"

#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"

#include <unistd.h>
#include <zmq.hpp>

XDAQ_INSTANTIATOR_IMPL (bril::pltsource::Application)

using namespace interface::bril;

bril::pltsource::Application::Application (xdaq::ApplicationStub* s)
: xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL)
{
  xgi::framework::deferredbind(this,this,&bril::pltsource::Application::Default, "Default");
  b2in::nub::bind(this, &bril::pltsource::Application::onMessage);
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  m_classname    = getApplicationDescriptor()->getClassName();
  m_instanceid   = 0;
  if( getApplicationDescriptor()->hasInstanceNumber() ){
    m_instanceid = getApplicationDescriptor()->getInstance();        
  }     
  //	std::string memPoolName = m_classname + m_instance.toString() + std::string("_memPool");
  toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
  toolbox::net::URN urn("toolbox-mem-pool",m_classname.toString()+"_"+m_instanceid.toString()+"_memPool");
  m_memPool = m_poolFactory->createPool(urn,allocator);
  try{
    // Listen to app infospace
    getApplicationInfoSpace()->fireItemAvailable("simSource", &m_simSource);
    getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("topics",&m_topics);
    getApplicationInfoSpace()->fireItemAvailable("zmqPublisher",&m_zmqPublisher);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    m_appURN.fromString(getApplicationDescriptor()->getURN());
  }catch( xcept::Exception & e){
    XCEPT_RETHROW(xdaq::exception::Exception, "Failed to put parameters into info spaces", e);
  }

  defineMonTable();
  initializeMonTable();

  m_currentnib = 0;
  m_currentls = 0;
  m_currentrun = 12345;
  m_currentfill = 2322;

  m_nibperls = 64;
  m_lsperrun = 4;

  //Initialize active channel list.  Assume initially all channels are active.

  for (int ichan = 0; ichan < 16; ichan++){
    m_activechannels[ichan] = 1;
  }
  for (int ihalf = 0; ihalf < 2; ++ihalf) {
    m_nactivechannels[ihalf] = 8;
    m_maskdata[ihalf] = 0x3333;
  }

  try{
    
    std::string nid("PltSource_mon");
    
    std::string monurnPull = createQualifiedInfoSpace(nid).toString();
    m_monInfoSpacePull = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurnPull));
    m_monInfoSpacePull->fireItemAvailable("className", &m_classname);
    m_monItemListPull.push_back("className");
    m_monInfoSpacePull->fireItemAvailable("appURN", &m_appURN);
    m_monItemListPull.push_back("appURN");
    m_monInfoSpacePull->fireItemAvailable("simSource", &m_simSource);
    m_monItemListPull.push_back("simSource");
    m_monInfoSpacePull->fireItemAvailable("signalTopic", &m_signalTopic);
    m_monItemListPull.push_back("signalTopic");
    m_monInfoSpacePull->fireItemAvailable("zmqPublisher", &m_zmqPublisher);
    m_monItemListPull.push_back("zmqPublisher");
    	  
    m_monInfoSpacePull->addGroupRetrieveListener(this);
	  
	  
    std::string monurnPush = createQualifiedInfoSpace(nid).toString();
    m_monInfoSpacePush = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurnPush));
    m_avgValue = 0;
    m_monInfoSpacePush->fireItemAvailable("avgValue", &m_avgValue);
    m_monItemListPush.push_back("avgValue");
    m_monInfoSpacePush->fireItemAvailable("simSource", &m_simSource);
    m_monItemListPush.push_back("simSource");
    m_monInfoSpacePush->fireItemAvailable("zmqPublisher", &m_zmqPublisher);
    m_monItemListPush.push_back("zmqPublisher");

    m_monInfoSpacePush->fireItemAvailable("currentnibble",&m_currentnib);
    m_monItemListPush.push_back("currentnibble");
    m_monInfoSpacePush->fireItemAvailable("currentls",&m_currentls);
    m_monItemListPush.push_back("currentls");
    m_monInfoSpacePush->fireItemAvailable("currentrun",&m_currentrun);
    m_monItemListPush.push_back("currentrun");
    m_monInfoSpacePush->fireItemAvailable("currentfill",&m_currentfill);
    m_monItemListPush.push_back("currentfill");

    m_monInfoSpacePush->fireItemAvailable("monTable",&m_montable);
    m_monItemListPush.push_back("monTable");

    m_monRunStatus.insert(std::make_pair("current nibble", &m_currentnib));
    m_monRunStatus.insert(std::make_pair("current ls", &m_currentls));
    m_monRunStatus.insert(std::make_pair("current run", &m_currentrun));
    m_monRunStatus.insert(std::make_pair("current fill", &m_currentfill));

    m_monStatus.insert(std::make_pair("simSource",&m_simSource));
    m_monStatus.insert(std::make_pair("zmqPublisher",&m_zmqPublisher));

  }catch(xdata::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to fire monitoring items";
    LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
    XCEPT_RETHROW(bril::pltsource::exception::Exception,msg.str(),e);
  }catch(xdaq::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to create infospace";
    LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
    XCEPT_RETHROW(bril::pltsource::exception::Exception, stdformat_exception_history(e),e);
  }catch(std::exception &e){
    LOG4CPLUS_ERROR(getApplicationLogger(), "std error "+std::string(e.what()));
    XCEPT_RAISE(bril::pltsource::exception::Exception, e.what());
  }
  
}

bril::pltsource::Application::~Application ()
{  
}

void bril::pltsource::Application::actionPerformed(xdata::Event& e){
  if( e.type() == "urn:xdaq-event:setDefaultValues"){           
    //count_=0;

    std::string timername(m_appURN.toString()+"_zmqstarter");
    try{
      toolbox::TimeVal zmqStart(2,0);
      toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername);
      //      toolbox::TimeVal start =toolbox::TimeVal::gettimeofday();
      timer->schedule(this, zmqStart, 0, timername);
    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer" << timername;
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::pltsource::exception::Exception,msg.str(),e);
    }
    
       
    if(m_simSource){
      LOG4CPLUS_INFO(this->getApplicationLogger(), std::string("subscribing to ")+m_signalTopic.toString());
      try{
	this->getEventingBus(m_bus.value_).subscribe(m_signalTopic);
      }catch(eventing::api::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
      }
    }
    if(!m_topics.value_.empty()){
      m_topiclist = toolbox::parseTokenSet(m_topics.value_,",");
    }
  }
}

void bril::pltsource::Application::actionPerformed(toolbox::Event& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
  if ( e.type() == "eventing::api::BusReadyToPublish" ){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "Eventing bus '" << busname << "' is ready to publish";
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
  }
}

/*
 * This is the default web page for this XDAQ application.
 */
void bril::pltsource::Application::Default (xgi::Input * in, xgi::Output * out)
{
  *out << "XDAQ PLT_source" << std::endl;

  // if uncomment, count_ needs to be locked for thread safety
  //*out << "<input type='button' onClick=\"parent.location='sendMessage'\" value='Send next message'>" << std::endl;
  //*out << "</br>" << std::endl;
  //*out << "Last Published LN : " << count_ << std::endl;
  
  //if (m_simSource){
  //  *out << "publishing simulated data on frequency: " << m_signalTopic.toString() << std::endl;
  //}else{
  //  *out << "publishing real data" << std::endl;
  //}
  //*out << cgicc::br();

  m_monInfoSpacePush->lock();
  m_monInfoSpacePush->fireItemGroupRetrieve(m_monItemListPush,this);
  m_monInfoSpacePush->unlock();
  *out << WebUtils::mapToHTML("Run Status", m_monRunStatus,"",true);
  *out << WebUtils::mapToHTML("App Status", m_monStatus,"",true);
  *out << "<br>";
  *out << "<p> Histogram Status</p>";
  *out << WebUtils::xdatatableToHTML(&m_montable);
}

/*
 * We use this as an activator to send a message
 */
/**
void bril::pltsource::Application::sendMessage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
  count_++;
  
  // In this example we will send a message that is ONLY a property list
  xdata::Properties plist;
  plist.setProperty("messageNumber", toolbox::toString("%d", count_));
  
  // This is how to publish a message. The paramaters are { topic, buffer, propertylist }
  try
    {
      this->getEventingBus("brildata").publish("dummyplist", 0, plist);
    }
  catch (eventing::api::exception::Exception & e)
    {
      // we are not doing any error handling in these examples. We just log the error
      LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to publish");
    }
  // use the default web page
  this->Default(in, out);
}
**/
/**
void bril::pltsource::Application::do_stoptimer(){
  toolbox::task::Timer* timer = toolbox::task::TimerFactory::getInstance()->getTimer(m_timername);
  if( timer->isActive() ){
    timer->stop();
  }
}
**/
/**
void bril::pltsource::Application::do_starttimer(){

  if(!toolbox::task::TimerFactory::getInstance()->hasTimer(m_timername)){
    (void) toolbox::task::TimerFactory::getInstance()->createTimer(m_timername);
  }
  toolbox::task::Timer* timer = toolbox::task::TimerFactory::getInstance()->getTimer(m_timername);
  toolbox::TimeInterval interval(m_nibfrequencySec,m_nibfrequencyUSec);
  try{
    do_stoptimer();
    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    timer->start();
    timer->scheduleAtFixedRate( start, this, interval, 0, m_timername);
  }catch (toolbox::task::exception::InvalidListener& e){
    LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
  }catch (toolbox::task::exception::InvalidSubmission& e){
    LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
  }catch (toolbox::task::exception::NotActive& e){
    LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
  }catch (toolbox::exception::Exception& e){
    LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
  }

}
**/

void bril::pltsource::Application::do_zmqclient(){

  //std::cout << "In zmq method" << std::endl;
  uint32_t time = 0;
  uint32_t orbit = 0;
  uint32_t channel = 0;
  uint32_t tcds_data[4] = {0};
  uint32_t data[3564];
  uint32_t maskdata[2];
  uint32_t pltend = 0;
  zmq::context_t zmq_context(1);
  zmq::socket_t zmq_socket(zmq_context, ZMQ_SUB);

  std::string default_zmq_publisher = "tcp://vmepc-s2d16-07-01.cms:5556";
  if (m_zmqPublisher == "") {
    LOG4CPLUS_INFO(getApplicationLogger(), "No zmq_publisher found in XML configuration, using default of " << default_zmq_publisher);
    zmq_socket.connect(default_zmq_publisher.c_str());
  } else {
    LOG4CPLUS_INFO(getApplicationLogger(), "Connecting to zmq publisher at " << m_zmqPublisher.toString());
    zmq_socket.connect(m_zmqPublisher.toString());
  }
  zmq_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);

  //  do_stoptimer();

  for(int iii = 0; iii<3564; iii++){
    data[iii] = 0;
  }
  //std::cout << "before zmq loop" << std::endl;
  while(1){

    //std::cout << "waiting for message" << std::endl;                                                                                
    zmq::message_t zmq_message(3573*sizeof(uint32_t));
    if (zmq_socket.recv(&zmq_message)){

      char * messageBuffer[3573*sizeof(uint32_t)];
      memcpy(messageBuffer, (void*) zmq_message.data(), 3573*sizeof(uint32_t));
      memcpy(&time, messageBuffer, sizeof(uint32_t)); 
      memcpy(&orbit, (uint32_t*)messageBuffer+1, sizeof(uint32_t));
      memcpy(&channel, (uint32_t*)messageBuffer+2, sizeof(uint32_t));     
      memcpy(&tcds_data, (uint32_t*)messageBuffer+3, 4*sizeof(uint32_t));
      memcpy(&maskdata, (uint32_t*)messageBuffer+7, 2*sizeof(uint32_t));
      memcpy(&data, (uint32_t*)messageBuffer+9, 3564*sizeof(uint32_t));

      //std::cout << "Got a message with time " << time << " orbit " << orbit << " channel " << channel << " nibble " << tcds_data[0] << " data " << (data[0]&0xfff) << " " << (data[1000]&0xfff) << " " << (data[3563]&0xfff) << std::endl;

      //See which end this channel is in.  Then check if the mask data for that end
      //has changed since the last message.  If so, then change the list of active 
      //channels.
      if(channel >=8) pltend = 1;
      if(channel < 8) pltend = 0;

      if(maskdata[pltend] != m_maskdata[pltend]){
	setActiveChannels(maskdata[pltend], channel);
	m_maskdata[pltend] = maskdata[pltend];
      }


      if(m_activechannels[channel] && (tcds_data[3] > 0) && (tcds_data[2] > 0) && (tcds_data[1] > 0) && (tcds_data[0] > 0)
	 && (tcds_data[3]< 99999999) && (tcds_data[2] < 99999999) && (tcds_data[1] < 99999999) && (tcds_data[0] < 65)){
	makePLTData(tcds_data[3],tcds_data[2],tcds_data[1],tcds_data[0],messageBuffer,3568*sizeof(uint32_t));}
      else{
	std::string msg("Something is wrong with the TCDS info, not publishing");
	LOG4CPLUS_INFO(getApplicationLogger(),msg);
      }

      m_currentnib = tcds_data[0];
      m_currentls = tcds_data[1];
      m_currentrun = tcds_data[2];
      m_currentfill = tcds_data[3];

      try{
	xdata::UnsignedShort population(0);
	for(int iii=0; iii<3564; iii++){
	  population = population + ((data[iii])&0xfff);}
	m_montable.setValueAt(channel,"Population",population);
	m_monInfoSpacePush->fireItemGroupChanged(m_monItemListPush,this);
	
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to push monitoring items");
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
      }
    }
  }
}

void bril::pltsource::Application::publishData(const std::string&topic, uint32_t fill,uint32_t run, uint32_t ls,uint32_t nibble,uint32_t timeorbit, uint32_t orbit, uint32_t channel, void* payloadPtr, size_t payloadsize){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Publishing");
  toolbox::mem::Reference* bufRef = 0;
  try{      
    size_t totsize = plthistT::maxsize();
    bufRef = m_poolFactory->getFrame(m_memPool,totsize);
    bufRef->setDataSize(totsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    header->setTime(fill,run,ls,nibble,t.sec(),t.millisec());
    header->setResource(interface::bril::shared::DataSource::PLT,0,channel,interface::bril::shared::StorageType::UINT32);
    header->setTotalsize(totsize);
    header->setFrequency(1);   
    memcpy(header->payloadanchor,payloadPtr,payloadsize);
    this->getEventingBus(m_bus.value_).publish(plthistT::topicname(), bufRef, plist);
  }catch (eventing::api::exception::Exception & e){
    // we are not doing any error handling in these examples. We just log the error
    LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to publish, release message buffer");
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
  }
}

void bril::pltsource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist)
{
//  std::cout << "onMessage" << std::endl;
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if(action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    //    std::cout << "Received a message with topic: " << plist.getProperty("signalTopic") << std::endl;
    if(topic == m_signalTopic.toString()){
      //      std::cout << "about to try to publish data"<<std::endl;
      if(ref!=0){
	//parse timing signal message header to get timing info
	interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 
        uint32_t fillnum = thead->fillnum;
	uint32_t runnum = thead->runnum;
	uint32_t lsnum = thead->lsnum;
	uint32_t nbnum = thead->nbnum;
	for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
	  if(*it==plthistT::topicname()){
	    simPLTData(fillnum,runnum,lsnum,nbnum);
	  }
	}
      }
    }
  }
  if(ref!=0){
    ref->release();
    ref = 0;
  }
}
 
void bril::pltsource::Application::timeExpired(toolbox::task::TimerEvent& e){
  std::cout << "Doing zmq client" << std::endl;
  do_zmqclient();
}
void bril::pltsource::Application::simPLTData(uint32_t fillnum, uint32_t runnum,uint32_t lsnum,uint32_t nbnum){
  uint32_t time_orbit=0;
  uint32_t reg_orbit=0;
  uint32_t ij=0;
  uint32_t databuff[3564];
  for(int ii = 0; ii < 16; ii++){    
    ij = ii;
    float sum = 0;
    for(int i=0; i<3564; ++i){
      databuff[i] = i;
      sum+= databuff[i];
    }
    m_avgValue = sum/3564;
    publishData(plthistT::topicname(),fillnum,runnum,lsnum,nbnum,time_orbit,reg_orbit,ij+1,&databuff,sizeof(databuff));    
  }  
}

void bril::pltsource::Application::makePLTData(uint32_t fillnum, uint32_t runnum,uint32_t lsnum,uint32_t nbnum,void* messagePtr, size_t messageSize){
  //count_++;
  //m_currentorb.value_++;
  uint32_t time_orbit=0;
  uint32_t reg_orbit=0;
  uint32_t ij=0;
  uint32_t tcds_data[4] = {0};
  uint32_t maskdata[2] = {0};
  uint32_t databuff[3564] = {0};
  memcpy(&time_orbit, messagePtr, sizeof(uint32_t));
  memcpy(&reg_orbit, (uint32_t*)messagePtr+1, sizeof(uint32_t));
  memcpy(&ij, (uint32_t*)messagePtr+2, sizeof(uint32_t));
  memcpy(&tcds_data, (uint32_t*)messagePtr+3, 4*sizeof(uint32_t));
  memcpy(&maskdata, (uint32_t*)messagePtr+7, 2*sizeof(uint32_t));
  memcpy(&databuff, (uint32_t*)messagePtr+9, 3564*sizeof(uint32_t));

  for(int iii = 0; iii < 3564; iii++){
	databuff[iii]&=0x1fff;}
  float sum = 0;
  for(int i=0; i<3564; ++i){
    sum+= databuff[i];
  }  
  m_avgValue = sum/3564;
  
  publishData(plthistT::topicname(),fillnum, runnum,lsnum,nbnum,time_orbit,reg_orbit,ij,&databuff,sizeof(databuff)); 
}


bool bril::pltsource::Application::reading(toolbox::task::WorkLoop* wl){
//  std::string topicname=m_classname.toString()+"Topic";
   return false;
}


void bril::pltsource::Application::defineMonTable(){
  m_montable.addColumn("Channel", "unsigned short");
  m_montable.addColumn("Population", "unsigned short");
}

void bril::pltsource::Application::initializeMonTable(){
  for (size_t i = 0; i < 16; ++i){
    xdata::UnsignedShort channelid(i);
    xdata::UnsignedShort population(0);
    m_montable.setValueAt(i,"Channel",channelid);
    m_montable.setValueAt(i,"Population",population);
  }
}

void bril::pltsource::Application::setActiveChannels(uint32_t maskdata, uint32_t channelId){
  //This function sets the number of active channels and then publishes that to be picked up 
  //by the processor.

  //Channel ID 0-7 are the low end, Channel ID 8-16 are the high end.

  int pltend = 0;
  int end = 0;
  if(channelId >=8){ end = 8; pltend = 1;}
  if(channelId < 8){ end = 0; pltend = 0;}

  //Now check for presence of each channel
  //maskdata||0x1 = channel 0, maskdata||0x2 = channel 1, maskdata||0x10 = channel 2
  //up through 0x2000

  //It's tedious to check this one at a time, but it means we can compress the 
  //trigger mask for each side into a uint32_t which keeps all the zmq message components
  //the same.
  m_nactivechannels[pltend] = 0;

  if(maskdata&0x1){m_activechannels[8*end] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end] = 0;}

  if(maskdata&0x2){m_activechannels[8*end+1] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+1] = 0;}

  if(maskdata&0x10){m_activechannels[8*end+2] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+2] = 0;}

  if(maskdata&0x20){m_activechannels[8*end+3] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+3] = 0;}

  if(maskdata&0x100){m_activechannels[8*end+4] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+4] = 0;}

  if(maskdata&0x200){m_activechannels[8*end+5] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+5] = 0;}

  if(maskdata&0x1000){m_activechannels[8*end+6] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+6] = 0;}

  if(maskdata&0x2000){m_activechannels[8*end+7] = 1; m_nactivechannels[pltend]++;}
  else{m_activechannels[end+7] = 0;}

  //Publish current number of active channels to be picked up by the pltprocessor

  xdata::Properties plist;

  toolbox::mem::Reference* NRef = 0;
  NRef = m_poolFactory->getFrame(m_memPool, sizeof(uint32_t));
  NRef->setDataSize(sizeof(uint32_t));

  uint32_t tot_activechannels = m_nactivechannels[0]+m_nactivechannels[1];

  uint32_t* tchan = (uint32_t*)NRef->getDataLocation();

  memcpy(tchan, &tot_activechannels, sizeof(uint32_t));

  std::cout << "Number of active channels is " << tot_activechannels << std::endl;

  //Gotta put DATA_VERSION in the property list
  plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);

  try
    {
    this->getEventingBus("plt").publish("NActiveChannels", NRef, plist);
    }
  catch(eventing::api::exception::Exception & e)
    {
      LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to publish NActiveChannels");
      if(NRef){
	NRef->release();
	NRef = 0;
      }
    }



}
