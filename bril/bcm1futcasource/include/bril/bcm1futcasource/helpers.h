// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_HELPERS_H_
#define _BRIL_BCM1FUTCASOURCE_HELPERS_H_

#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include "xdata/Serializable.h"
#include "xdata/InfoSpace.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Output.h"
#include "bril/webutils/WebUtils.h"
#include "toolbox/Event.h"
#include "toolbox/BSem.h"

namespace bril { namespace bcm1futcasource {

template<typename T, typename... Args> std::unique_ptr<T> make_unique(Args&&... args) {
    /**
    Approved implementation of std::make_unique in C++14
    */
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

struct LockGuard
{
     LockGuard(toolbox::BSem & mutex) : _ref(mutex) 
     { 
         _ref.take();
     };

     ~LockGuard() 
     { 
          _ref.give(); 
     }
   private:

     LockGuard(const LockGuard& ); // or use c++0x ` = delete`

     toolbox::BSem & _ref;
};

class DeviceDisconnectedEvent : public toolbox::Event{
 //
 public:
    //
    DeviceDisconnectedEvent(const xcept::Exception& exp):toolbox::Event("urn:bril-bcm1futcasource-event:DeviceDisconnectedEvent",0),_exp(exp){}

    ~DeviceDisconnectedEvent(){}

     xcept::Exception _exp;
};

class DeviceConnectedEvent : public toolbox::Event{
 //
 public:
    //
    DeviceConnectedEvent(const xcept::Exception& exp):toolbox::Event("urn:bril-bcm1futcasource-event:DeviceDisconnectedEvent",0),_exp(exp){}

    ~DeviceConnectedEvent(){}

     xcept::Exception _exp;
};

}  // namespace bcm1futcasource
}  // namespace bril
#endif
