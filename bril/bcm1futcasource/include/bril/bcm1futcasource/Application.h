// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_APPLICATION_H_
#define _BRIL_BCM1FUTCASOURCE_APPLICATION_H_
#include <string>
#include <set>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "bril/bcm1futcasource/BackEndConfig.h"
#include "bril/bcm1futcasource/BackEndAcq.h"
#include "bril/bcm1futcasource/BrildaqConfig.h"
#include "bril/bcm1futcasource/Monitoring.h"
#include "bril/bcm1futcasource/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/squeue.h"

namespace bril { namespace bcm1futcasource {

extern std::mutex _mutex; // application global mutex.

class Application : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener,  public toolbox::EventDispatcher {
    XDAQ_INSTANTIATOR();

 public:
    // constructor
    explicit Application(xdaq::ApplicationStub * s);

    // destructor
    virtual ~Application();

    // xgi(web) callback
    void Default(xgi::Input * in, xgi::Output * out);

    // infospace event callback
    virtual void actionPerformed(xdata::Event& e);

    // timer callback - monitoring the hw status
    virtual void timeExpired(toolbox::task::TimerEvent& e);

    // reading continuously
    bool readingHistograms(toolbox::task::WorkLoop*);

 private:
    // some XDAQ specific information about the application
    //

    const xdaq::ApplicationDescriptor * _appDescriptor;

    std::string                         _classname;
    xdata::UnsignedInteger              _instance;

    toolbox::mem::MemoryPoolFactory * _poolFactory;
    toolbox::mem::Pool              * _memPool;

    // To fire shutdown event

    toolbox::EventDispatcher _dispatcher;

    // It's used to perform mutual exclusive access
    // to shared resources in Timers and  WorkLoops
    //
    std::mutex _mutex;

    // we also declare the class BackEndAcq as a friend 
    // to grant an access to all memebers  of the class

    friend class BackEndAcq;

    // Hardware back-end and XDAQ application configuration parameters

    std::unique_ptr<BackEndConfig>     _cfg;

    // Run-time acquisition control

    std::shared_ptr<BackEndAcq>        _acq;

    // Run-time monitoring

    std::unique_ptr<Monitoring> _monitoring;

    // waiting list of buses to get ready for publishing

    std::set<std::string> _notReadyToPublisheBuses;

    //  Histogram readout workloops, one per GLIB

    std::map<toolbox::task::WorkLoop *, std::string> _reading_hist_wl;

    // work loop termination flag
    //
    bool _terminate;

    // Current LHC BEAM MODE and BRILDAQ counters
    //
    std::string  _beam_mode;
    bool _do_store;

    struct utca::GLIBv3::tcds_info_t _tcds;


 private:
    // toolbox event callback
    virtual void actionPerformed(toolbox::Event& e);

    // b2in event callback
    void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

    // read and publish histograms

    bool histograms(toolbox::task::WorkLoop* wl);

    void startHistogramReadingWorkLoops();
    void stopHistogramReadingWorkLoops();

    // nocopy protection
    explicit Application(const Application&); Application& operator=(const Application&);

    // Callback function of hyperdaq on data request by Hicghcharts, returns JSON response

    void requestData( xgi::Input* in, xgi::Output* out );

    // Callback function of hyperdaq on data request by Hicghcharts, returns JSON response

    void requestHardwareStatus( xgi::Input* in, xgi::Output* out );

    // REST API - getJOSN input parameter to retrieve the hardware status - callback function URL

    std::string _urlToRequestHardwareStatus;

    // REST API - getJOSN input parameter to retrieve Highcharts data - callback function URL

    std::string _urlToRequestHighchartsData;

    void htmlHardwareStatusTables(xgi::Output * out);

    // permits full pages in hyperdaq. Web server is blocked until the application initialization is completed

    bool _enableHyperdaq;
};
}  // namespace bcm1futcasource
}  // namespace bril
#endif
