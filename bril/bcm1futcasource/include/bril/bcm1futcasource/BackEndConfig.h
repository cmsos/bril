// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_BACKENDCONFIG_H_
#define _BRIL_BCM1FUTCASOURCE_BACKENDCONFIG_H_
#include <string>
#include <vector>
#include <map>
#include "bcm1f_ubcm/ubcm.hpp"
#include "xdata/String.h"
#include "bril/bcm1futcasource/BrildaqConfig.h"
#include "bril/bcm1futcasource/exception/Exception.h"
#include <boost/algorithm/string.hpp>
#include "bril/bcm1futcasource/helpers.h"
#include "interface/bril/BCM1FUTCATopics.hh"

namespace bril { namespace bcm1futcasource {

class BackEndConfig : public BrildaqConfig {
 public:
    explicit BackEndConfig(xdaq::Application * application);

    void setDefaultValues();

    std::string getIPbusConfig() const;

    std::vector<std::string> getListOfDevices() const;

    std::string getIpmiHostname(const std::string & device)  const;

    std::string getAmcSlotNumber(const std::string & device) const;

    int  getOrbitPhase(const std::string & device)   const; // 78ps tap
    int  getOrbitOffsetCoarse(const std::string & device)   const; // bx (40MHz clock)
    int  getOrbitOffsetFine(const std::string & device)   const; // delay of 4 samples (300MHz clock)
    int  getOrbitOffsetUltraFine(const std::string & device)   const; // ADC sync delay (1.2GHz clock)

    utca::GLIBv3::thresholds_t getBasicAmplitudeThresholds(const std::string & device) const;         // BaT in XML configuration

    utca::GLIBv3::thresholds_t getDerivativeAmplitudeThresholds(const std::string & device) const;    // DaT in XML configuration

    utca::GLIBv3::thresholds_t getDerivativeDerivativeThresholds(const std::string & device) const;   // DdT in XML configuration

    utca::GLIBv3::thresholds_t getDerivativeIsolationThresholds(const std::string & device) const;    // DiT in XML configuration

    utca::GLIBv3::thresholds_t getTimeOverThresholds(const std::string & device) const;               // ToT in XML configuration

    unsigned int getChannelId(const std::string & device, unsigned int channel) const;

    unsigned int getAlgoId(const std::string & device, unsigned int channel) const;

    std::string getPlotLegend(const std::string & device, unsigned int channel) const;

 protected:
    //  IPbus configuration file

    xdata::String   _ipBusConfig;

    // Crates configuration

    xdata::Vector<xdata::Properties> _ubcm_cfg;
};
}  // namespace bcm1futcasource
}  // namespace bril
#endif
