// $Id$
#include "bril/bcm1futcasource/BrildaqConfig.h"
#include "interface/bril/BCM1FUTCATopics.hh"

bril::bcm1futcasource::BrildaqConfig::BrildaqConfig(xdaq::Application * application) : _application(application) {
    //
    // Infospace to pull the information about the application instance
    //
    xdata::InfoSpace * infoSpace;

    infoSpace = application->getApplicationInfoSpace();

    infoSpace->fireItemAvailable("commissioningMode",  &_commissioningMode);
    infoSpace->fireItemAvailable("rawDataSelfTrigger",  &_rawDataSelfTrigger);
    infoSpace->fireItemAvailable("rawDataSelfTriggerThreshold",  &_rawDataSelfTriggerThreshold);
    infoSpace->fireItemAvailable("rawDataPeakfinderTrigger",  &_rawDataPeakfinderTrigger);
    infoSpace->fireItemAvailable("peakfinderTriggerThresholdLow",  &_peakfinderTriggerThresholdLow);
    infoSpace->fireItemAvailable("peakfinderTriggerThresholdHigh",  &_peakfinderTriggerThresholdHigh);
    infoSpace->fireItemAvailable("eventinginput",      &_inputtopics);
    infoSpace->fireItemAvailable("eventingoutput",     &_outputtopics);
    infoSpace->fireItemAvailable("timerinterval",      &_timerInterval);
}


void bril::bcm1futcasource::BrildaqConfig::setDefaultValues() {
    //
    // Getting list of subscriptions
    //
    for ( size_t i = 0; i < _inputtopics.elements(); i++ ) {
        //
        xdata::Properties * p = dynamic_cast<xdata::Properties*>(_inputtopics.elementAt(i));

        if (p) {
            //
            std::string bus = p->getProperty("bus");

            std::string topic = p->getProperty("topic");

            _subscriptionTopics.insert(std::make_pair(topic, bus));
        }
    }
    //
    // Getting list of publications
    //
    for ( size_t i = 0; i < _outputtopics.elements(); i++ ) {
        //
        xdata::Properties * p = dynamic_cast<xdata::Properties*>(_outputtopics.elementAt(i));

        if (p) {
            //
            std::string bus = p->getProperty("bus");

            std::string topic = p->getProperty("topic");

            std::string rate = p->getProperty("rate");

            _publicationTopics.insert(std::make_pair(topic, std::make_pair(bus, rate)));
        }
    }
}


std::set<std::string> bril::bcm1futcasource::BrildaqConfig::getListOfPublicationBuses() const {
    /**
    */

    std::set<std::string> bus;

    std::multimap<std::string, std::pair<std::string, std::string> >::const_iterator it;

    for ( it= _publicationTopics.begin(); it != _publicationTopics.end(); ++it ) bus.insert(it->second.first);

    return std::move(bus);
}

std::set<std::string> bril::bcm1futcasource::BrildaqConfig::getListOfPublicationBuses(const std::string & topic) const {
    /**
    */
    std::set<std::string> bus;

    std::multimap<std::string, std::pair<std::string, std::string> >::const_iterator it;

    for ( it= _publicationTopics.begin(); it != _publicationTopics.end(); ++it ) {
        //
        if ( it->first == topic ) bus.insert(it->second.first);
    }
    return std::move(bus);
}


bool bril::bcm1futcasource::BrildaqConfig::isOrbitRequestedEveryNB1() const {
    /**
    */
    std::multimap<std::string, std::pair<std::string, std::string> >::const_iterator it;

    for ( it= _publicationTopics.begin(); it != _publicationTopics.end(); ++it ) {
        //
        if ( it->first == interface::bril::bcm1futcarawdataT::topicname() ) {
            //
            if ( it->second.second == "NB1" ) return true;
        }
    }
    return false;
}

bool bril::bcm1futcasource::BrildaqConfig::isOrbitRequestedEveryNB4() const {
    /**
    */
    std::multimap<std::string, std::pair<std::string, std::string> >::const_iterator it;

    for ( it= _publicationTopics.begin(); it != _publicationTopics.end(); ++it ) {
        //
        if ( it->first == interface::bril::bcm1futcarawdataT::topicname() ) {
            //
            if ( it->second.second == "NB4" ) return true;
        }
    }
    return false;
}

bool bril::bcm1futcasource::BrildaqConfig::isOrbitRequestedEveryNB64() const {
    /**
    */
    std::multimap<std::string, std::pair<std::string, std::string> >::const_iterator it;

    for ( it= _publicationTopics.begin(); it != _publicationTopics.end(); ++it ) {
        //
        if ( it->first == interface::bril::bcm1futcarawdataT::topicname() ) {
            //
            if ( it->second.second == "NB64" ) return true;
        }
    }
    return false;
}
