// $Id$
#include "bril/bcm1futcasource/BackEndConfig.h"

bril::bcm1futcasource::BackEndConfig::BackEndConfig(xdaq::Application * application) : bril::bcm1futcasource::BrildaqConfig(application) {
    //
    // Infospace to pull the information about the application instance
    //
    xdata::InfoSpace * infoSpace;

    infoSpace = application->getApplicationInfoSpace();

    infoSpace->fireItemAvailable("ipbusconfig",  &_ipBusConfig);

    infoSpace->fireItemAvailable("ubcm",         &_ubcm_cfg);
}

void bril::bcm1futcasource::BackEndConfig::setDefaultValues() {
    /**
    */
    bril::bcm1futcasource::BrildaqConfig::setDefaultValues();
}


std::string bril::bcm1futcasource::BackEndConfig::getIPbusConfig() const {
    /**
    */
    return _ipBusConfig.toString();
}

std::vector<std::string> bril::bcm1futcasource::BackEndConfig::getListOfDevices() const {
    /**
    */
    std::vector<std::string>  devices;

    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        devices.push_back(it->getProperty("device"));
    }
    return devices;
}


std::string bril::bcm1futcasource::BackEndConfig::getIpmiHostname(const std::string & device) const {
    /**
    */

    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) return it->getProperty("ipmi");
    }
    return "";
}

std::string bril::bcm1futcasource::BackEndConfig::getAmcSlotNumber(const std::string & device) const {
    /**
    */

    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) return it->getProperty("amc");
    }
    return "";
}

int bril::bcm1futcasource::BackEndConfig::getOrbitPhase(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            try {
                return std::stoi(it->getProperty("orbit-phase"));
                //
            } catch(std::invalid_argument & e ) {
                //
                return 0;
                //
            } catch(std::out_of_range & e) {
                //
                return 0;
            }
        }
    }
    return 0;
}

int bril::bcm1futcasource::BackEndConfig::getOrbitOffsetCoarse(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //        
            try {
                return std::stoi(it->getProperty("orbit-offset-coarse"));
                //
            } catch(std::invalid_argument & e ) {
                //
                return 0;
                //
            } catch(std::out_of_range & e) {
                //
                return 0;
            }
        }
    }
    return 0; 
}

int bril::bcm1futcasource::BackEndConfig::getOrbitOffsetFine(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            try {
                return std::stoi(it->getProperty("orbit-offset-fine"));
                //
            } catch(std::invalid_argument & e ) {
                //
                return 0;
                //
            } catch(std::out_of_range & e) {
                //
                return 0;
            }
        }
    }
    return 0;
}

int bril::bcm1futcasource::BackEndConfig::getOrbitOffsetUltraFine(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            try {
                return std::stoi(it->getProperty("orbit-offset-ultrafine"));
                //
            } catch(std::invalid_argument & e ) {
                //
                return 0;
                //
            } catch(std::out_of_range & e) {
                //
                return 0;
            }
        }
    }
    return 0;
}

utca::GLIBv3::thresholds_t bril::bcm1futcasource::BackEndConfig::getBasicAmplitudeThresholds(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    utca::GLIBv3::thresholds_t ret_thresholds;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::vector<std::string> thresholds; std::string th = it->getProperty("BaT");

            int16_t th_A = -1;
            int16_t th_B = -1;
            int16_t th_C = -1;
            int16_t th_D = -1;

            if ( !th.empty() ) {

                boost::algorithm::split(thresholds, th, boost::is_any_of(","));

                try {
                    th_A = std::stoi(thresholds[0]);
                    th_B = std::stoi(thresholds[1]);
                    th_C = std::stoi(thresholds[2]);
                    th_D = std::stoi(thresholds[3]);
                    //
                }  catch (std::exception &) {}
//
//                if ( th_A > 255 || th_A < 0 ) { th_A = 0xFF; }
//                if ( th_B > 255 || th_B < 0 ) { th_B = 0xFF; }
//                if ( th_C > 255 || th_C < 0 ) { th_C = 0xFF; }
//                if ( th_D > 255 || th_D < 0 ) { th_D = 0xFF; }
            }

            ret_thresholds = utca::GLIBv3::thresholds_t {th_A, th_B, th_C, th_D};

            return std::move(ret_thresholds);
        }
    }
    ret_thresholds = utca::GLIBv3::thresholds_t {-1, -1, -1, -1};

    return ret_thresholds;
}

utca::GLIBv3::thresholds_t bril::bcm1futcasource::BackEndConfig::getDerivativeAmplitudeThresholds(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    utca::GLIBv3::thresholds_t ret_thresholds;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::vector<std::string> thresholds; std::string th = it->getProperty("DaT");

            int16_t th_A = 0xFF;
            int16_t th_B = 0xFF;
            int16_t th_C = 0xFF;
            int16_t th_D = 0xFF;

            if ( !th.empty() ) {

                boost::algorithm::split(thresholds, th, boost::is_any_of(","));

                try {
                    th_A = std::stoi(thresholds[0]);
                    th_B = std::stoi(thresholds[1]);
                    th_C = std::stoi(thresholds[2]);
                    th_D = std::stoi(thresholds[3]);
                    //
                }  catch (std::exception &) {}

                if ( th_A > 4095 || th_A < 0 ) { th_A = 0xFFF; }
                if ( th_B > 4095 || th_B < 0 ) { th_B = 0xFFF; }
                if ( th_C > 4095 || th_C < 0 ) { th_C = 0xFFF; }
                if ( th_D > 4095 || th_D < 0 ) { th_D = 0xFFF; }
            }

            ret_thresholds = utca::GLIBv3::thresholds_t {th_A, th_B, th_C, th_D};

            return std::move(ret_thresholds);
        }
    }
    ret_thresholds = utca::GLIBv3::thresholds_t {0, 0, 0, 0}; return ret_thresholds;
}

utca::GLIBv3::thresholds_t bril::bcm1futcasource::BackEndConfig::getDerivativeDerivativeThresholds(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    utca::GLIBv3::thresholds_t ret_thresholds;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::vector<std::string> thresholds; std::string th = it->getProperty("DdT");

            int16_t th_A = 0xFF;
            int16_t th_B = 0xFF;
            int16_t th_C = 0xFF;
            int16_t th_D = 0xFF;

            if ( !th.empty() ) {

                boost::algorithm::split(thresholds, th, boost::is_any_of(","));

                try {
                    th_A = std::stoi(thresholds[0]);
                    th_B = std::stoi(thresholds[1]);
                    th_C = std::stoi(thresholds[2]);
                    th_D = std::stoi(thresholds[3]);
                    //
                }  catch (std::exception &) {}

                if ( th_A > 255 || th_A < 0 ) { th_A = 0xFF; }
                if ( th_B > 255 || th_B < 0 ) { th_B = 0xFF; }
                if ( th_C > 255 || th_C < 0 ) { th_C = 0xFF; }
                if ( th_D > 255 || th_D < 0 ) { th_D = 0xFF; }
            }

            ret_thresholds = utca::GLIBv3::thresholds_t {th_A, th_B, th_C, th_D};

            return std::move(ret_thresholds);
        }
    }
    ret_thresholds = utca::GLIBv3::thresholds_t {0, 0, 0, 0}; return ret_thresholds;
}


utca::GLIBv3::thresholds_t bril::bcm1futcasource::BackEndConfig::getDerivativeIsolationThresholds(const std::string & device)   const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    utca::GLIBv3::thresholds_t ret_thresholds;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::vector<std::string> thresholds; std::string th = it->getProperty("DiT");

            int16_t th_A = 0xFF;
            int16_t th_B = 0xFF;
            int16_t th_C = 0xFF;
            int16_t th_D = 0xFF;

            if ( !th.empty() ) {

                boost::algorithm::split(thresholds, th, boost::is_any_of(","));

                try {
                    th_A = std::stoi(thresholds[0]);
                    th_B = std::stoi(thresholds[1]);
                    th_C = std::stoi(thresholds[2]);
                    th_D = std::stoi(thresholds[3]);
                    //
                }  catch (std::exception &) {}

                if ( th_A > 255 || th_A < 0 ) { th_A = 0xFF; }
                if ( th_B > 255 || th_B < 0 ) { th_B = 0xFF; }
                if ( th_C > 255 || th_C < 0 ) { th_C = 0xFF; }
                if ( th_D > 255 || th_D < 0 ) { th_D = 0xFF; }
            }

            ret_thresholds = utca::GLIBv3::thresholds_t {th_A, th_B, th_C, th_D};

            return std::move(ret_thresholds);
        }
    }
    ret_thresholds = utca::GLIBv3::thresholds_t {0, 0, 0, 0}; return ret_thresholds;
}


 utca::GLIBv3::thresholds_t bril::bcm1futcasource::BackEndConfig::getTimeOverThresholds(const std::string & device) const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    utca::GLIBv3::thresholds_t ret_thresholds;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::vector<std::string> thresholds; std::string th = it->getProperty("ToT");

            int16_t th_A = 0x1;
            int16_t th_B = 0x1;
            int16_t th_C = 0x1;
            int16_t th_D = 0x1;

            if ( !th.empty() ) {

                boost::algorithm::split(thresholds, th, boost::is_any_of(","));

                try {
                    th_A = std::stoi(thresholds[0]);
                    th_B = std::stoi(thresholds[1]);
                    th_C = std::stoi(thresholds[2]);
                    th_D = std::stoi(thresholds[3]);
                    //
                }  catch (std::exception &) {}

                if ( th_A > 255 || th_A < 0 ) { th_A = 0x1; }
                if ( th_B > 255 || th_B < 0 ) { th_B = 0x1; }
                if ( th_C > 255 || th_C < 0 ) { th_C = 0x1; }
                if ( th_D > 255 || th_D < 0 ) { th_D = 0x1; }
            }
            ret_thresholds = utca::GLIBv3::thresholds_t {th_A, th_B, th_C, th_D};

            return std::move(ret_thresholds);
        }
    }
    ret_thresholds = utca::GLIBv3::thresholds_t {0, 0, 0, 0}; return ret_thresholds;
}


unsigned int bril::bcm1futcasource::BackEndConfig::getChannelId(const std::string & device, unsigned int channel) const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::string ids = it->getProperty("channel-ids");

            if ( !ids.empty() ) {

                std::vector<std::string> channelids; boost::algorithm::split(channelids, ids, boost::is_any_of(","));

                if ( channel < channelids.size() ) return std::stoi(channelids[channel]);
            }
            break;
        }    
    }
    return 0;
}

unsigned int bril::bcm1futcasource::BackEndConfig::getAlgoId(const std::string & device, unsigned int channel) const {
    /**
    */
    std::vector<xdata::Properties>::const_iterator it;

    for ( it = _ubcm_cfg.begin(); it != _ubcm_cfg.end(); ++it ) {
        //
        if ( device == it->getProperty("device") ) {
            //
            std::string ids = it->getProperty("algo-ids");

            if ( !ids.empty() ) {

                std::vector<std::string> algoids; boost::algorithm::split(algoids, ids, boost::is_any_of(","));

                if ( channel < algoids.size() ) return std::stoi(algoids[channel]);
            }
            break;
        }    
    }
    return interface::bril::BCM1FUTCAHistAlgos::UNDEFINED_PEAK_FINDER;
}


std::string bril::bcm1futcasource::BackEndConfig::getPlotLegend(const std::string & device, unsigned int channel) const
{
	std::stringstream ss;

	ss << "Channel ID # " << getChannelId(device,channel) << " (";

	if ( getAlgoId(device, channel) == interface::bril::BCM1FUTCAHistAlgos::BASIC_PEAK_FINDER )
	{
		ss << "basic peak finder)";
	}
	else if ( getAlgoId(device, channel) == interface::bril::BCM1FUTCAHistAlgos::DERIVATIVE_PEAK_FINDER )
	{
		ss << "derivative peak finder)";
	}
	else
	{
		ss << "algo ID # " << getAlgoId(device, channel) << ")";
	}

	return ss.str();
}
