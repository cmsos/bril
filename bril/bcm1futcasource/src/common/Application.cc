// $Id$
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xcept/tools.h"
#include "bril/bcm1futcasource/Application.h"
#include "xgi/framework/Method.h"
#include "b2in/nub/Method.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/ShutdownEvent.h"
#include <boost/timer/timer.hpp>
#include "toolbox/Runtime.h" 
#include "interface/bril/BEAMTopics.hh"
#include "xdata/InfoSpaceFactory.h"
#include "bril/bcm1futcasource/helpers.h"
#include "toolbox/mem/AutoReference.h"

#include <time.h>

XDAQ_INSTANTIATOR_IMPL(bril::bcm1futcasource::Application)

bril::bcm1futcasource::Application::Application(xdaq::ApplicationStub* s): xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this) {
  /**
     some information about this application
  */
  _appDescriptor  = getApplicationDescriptor();

  _classname      = _appDescriptor->getClassName();

  _instance       = _appDescriptor->getInstance();

  _terminate      = false;

  _enableHyperdaq = false;

  _urlToRequestHardwareStatus =  getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestHardwareStatus";

  _urlToRequestHighchartsData = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";

  _tcds.lhc_fill     = 0;
  _tcds.cms_run      = 0;
  _tcds.lumi_section = 0;
  _tcds.lumi_nibble  = 0;
  
  _beam_mode = "STABLE BEAMS"; _do_store = true;
  LOG4CPLUS_INFO(this->getApplicationLogger(), "STORE ON");

  //  Memory pool
  
  _poolFactory  = toolbox::mem::getMemoryPoolFactory();

  try
  {
    toolbox::net::URN memurn("toolbox-mem-pool",_classname+"_"+_instance.toString()+"_mem");

    toolbox::mem::HeapAllocator * allocator = new toolbox::mem::HeapAllocator();

    _memPool = _poolFactory->createPool(memurn,allocator);
  }
  catch(toolbox::task::exception::Exception & e)
  {
    std::string msg("Failed to allocate a memory pool ");

    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));

    XCEPT_RETHROW(bril::bcm1futcasource::exception::Exception,msg,e); 
  } 

  // Initialization of the application infospace

  _cfg = bril::bcm1futcasource::make_unique<bril::bcm1futcasource::BackEndConfig>(this);

  // Initialization of the run-time monioring

  _monitoring = bril::bcm1futcasource::make_unique<bril::bcm1futcasource::Monitoring>(this);

  // Initialization of the run-time acquisition

  _acq = std::make_shared<bril::bcm1futcasource::BackEndAcq>(this, *_cfg.get());

  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

  // setting shutdown handler to stop all workloops
  //
  toolbox::getRuntime()->addShutdownListener(this);
}

bril::bcm1futcasource::Application::~Application() {
  /**
   Never called when Ctrl-C The code is moved to actionPerformed on toolbox::EventShutdown
  */ 
  std::cout << "Destructor is called" << std::endl;
}

void bril::bcm1futcasource::Application::Default(xgi::Input * in, xgi::Output * out) {
	//
	if (  ! _enableHyperdaq )
	{
		*out << "<p>The application is being initialized. Refresh the page later.</p>" << std::endl;

		return;
	}

	//webutils::WebChart::pageHeader(out, true);
	*out << "<script src=\"/bril/bcm1futcasource/html/highcharts.js\"></script>";
	*out << "<script src=\"/bril/bcm1futcasource/html/modules/boost.js\"></script>";

	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
	//------------------------------------------------------------------------------------
	*out << "<div id=\"hwst\" class=\"xdaq-tab\" Title=\"Hardware status\">" << std::endl;

	htmlHardwareStatusTables(out);

	*out << "</div>" << std::endl;

	//------------------------------------------------------------------------------------
	*out << "<div id=\"amphst\" class=\"xdaq-tab\" Title=\"Amplitude histograms\">" << std::endl;

	cgicc::Cgicc cgi( in );

	std::vector<std::string> devices  = _cfg->getListOfDevices();

	for( std::string device : _cfg->getListOfDevices() ) {

		*out << "<h2>" << device << "</h2>" << std::endl;

		_acq->getUBCM(device)->getAmplitudeChart()->writeChart(out,"height:300px; width:90%;");
	}
	*out << "</div>" << std::endl;
	//------------------------------------------------------------------------------------

	*out << "<div id=\"occhst\" class=\"xdaq-tab\" Title=\"Occupancy histograms\">" << std::endl;

	devices  = _cfg->getListOfDevices();

		for( std::string device : _cfg->getListOfDevices() ) {

			*out << "<h2>" << device << "</h2>" << std::endl;

			_acq->getUBCM(device)->getOccupancyChart()->writeChart(out,"height:300px; width:90%;");
		}
	*out << "</div>" << std::endl;
	//------------------------------------------------------------------------------------
	*out << "<div class=\"xdaq-tab\" Title=\"Orbit raw data\">" << std::endl;

	devices  = _cfg->getListOfDevices();

		for( std::string device : _cfg->getListOfDevices() ) {

			*out << "<h2>" << device << "</h2>" << std::endl;

			_acq->getUBCM(device)->getRawOrbitChart()->writeChart(out,"height:300px; width:90%;");
		}
	*out << "</div>" << std::endl;
	//------------------------------------------------------------------------------------
	*out << "</div>" << std::endl;
}

void bril::bcm1futcasource::Application::actionPerformed(xdata::Event& e) {
  /*
  */
  LOG4CPLUS_INFO(this->getApplicationLogger(), "Received xdata event " << e.type());

  if ( e.type() == "urn:xdaq-event:setDefaultValues" ) {
      /**
      bind "Default" method as my web page method
      */
      xgi::framework::deferredbind(this, this, &bril::bcm1futcasource::Application::Default, "Default");

      /**
         bind "requestData" method as my web page method for Highcharts plotting

         !!! NEVER USE xgi::framework::deferredbind FOR jQUERY JSON RESPONSE !!!
         !!! It adds a hyperdaq  page standard header. Use this one instead: !!!
         !!!                        xgi::deferredbind                        !!!
      */
      xgi::deferredbind(this, this, &bril::bcm1futcasource::Application::requestData,                      "requestData");

      xgi::deferredbind( this, this, &bril::bcm1futcasource::Application::requestHardwareStatus, "requestHardwareStatus");

      _cfg->setDefaultValues();

      _notReadyToPublisheBuses = _cfg->getListOfPublicationBuses();

      for (std::set<std::string>::const_iterator it = _notReadyToPublisheBuses.begin(); it != _notReadyToPublisheBuses.end(); ++it) {
          //
          this->getEventingBus(*it).addActionListener(this);
      }
  }
}

void bril::bcm1futcasource::Application::actionPerformed(toolbox::Event& e) {
  /**
  Right method to clean up resources at termination of an XDAQ application
  */ 
  if ( e.type() == "Shutdown" ) {
    //
    LOG4CPLUS_INFO(getApplicationLogger(), "Received toolbox event [" << e.type() << "]...");

    stopHistogramReadingWorkLoops();

    LOG4CPLUS_INFO(getApplicationLogger(), "Uptime: " << _monitoring->getApplicationUptime() << " hour(s).");

    LOG4CPLUS_INFO(getApplicationLogger(), "Safe termination!!!"); return;
  }

  if ( e.type() == "eventing::api::BusReadyToPublish" ) {
    //
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();

    LOG4CPLUS_INFO(getApplicationLogger(), "Eventing bus '" << busname << "' is ready to publish");

    _notReadyToPublisheBuses.erase(busname);

    if ( !_notReadyToPublisheBuses.size() ) {

      try {
        _acq->connect(); // Initialize the hardware

        _acq->updateInfo(); // add more stuff to info dictionary

        _acq->initWebPlotting(this->_urlToRequestHighchartsData); // Create instances of the class "WebChart"
        //
      } catch(bril::bcm1futcasource::exception::ubcmException & e) {
        //
        LOG4CPLUS_FATAL(getApplicationLogger(), e.message());
        LOG4CPLUS_FATAL(getApplicationLogger(), "The exception raised at " << e.module() << ":" << e.line());

        toolbox::getRuntime()->halt(-2);
      }
      catch( std::exception & e)
      {
        LOG4CPLUS_FATAL(getApplicationLogger(), e.what()); toolbox::getRuntime()->halt(-2);        
      }

      // reset histos
      try {
        _acq->resetHistograms();
      }
      catch( std::exception & e)
      {
        LOG4CPLUS_FATAL(getApplicationLogger(), e.what()); toolbox::getRuntime()->halt(-2);
      }
            
      try {
        startHistogramReadingWorkLoops();
      }
      catch(bril::bcm1futcasource::exception::Exception & e) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "Failed to launch histogram reading workloops: " + stdformat_exception_history(e));
      }

      // starting the timer which triggers a hardware lookup

      toolbox::net::URN timerurn("toolbox-mem-pool", _classname+"_"+_instance.toString()+ "_timer");

      try {
        toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer(timerurn);

        toolbox::TimeInterval interval(_cfg->getTimerInterval(), 0);

        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

        timer->scheduleAtFixedRate(start, this, interval, NULL, timerurn.toString());
      }
      catch(toolbox::task::exception::Exception & ex) {
    	//
        std::stringstream ss;

        ss << "Failed to create a timer to monitor hardware status. " << ex.what() << std::endl;

        std::string msg = ss.str();                LOG4CPLUS_FATAL(getApplicationLogger(), msg);
      }

      /**
        bind "onMessage" with b2in actions from the eventing
      */
      b2in::nub::bind(this, &bril::bcm1futcasource::Application::onMessage);

      std::map<std::string, std::string>::const_iterator it;

      for ( it= _cfg->getListOfSubscriptions().begin(); it != _cfg->getListOfSubscriptions().end(); ++it ) {
        try {
          this->getEventingBus(it->second).subscribe(it->first);
        }
        catch(eventing::api::exception::Exception& e) {
          LOG4CPLUS_ERROR(getApplicationLogger(), "Failed to subscribe to a topic: " + stdformat_exception_history(e));
        }
      }
      _enableHyperdaq = true;
    }
  }
}

void bril::bcm1futcasource::Application::timeExpired(toolbox::task::TimerEvent& e) {
  /**
  */
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received timeExpired event from the monitor timer.");

  _acq->updateInfo();
}


void bril::bcm1futcasource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
  /**
  */
  toolbox::mem::AutoReference aotoref(ref); if ( _terminate ) return;

  std::string action = plist.getProperty("urn:b2in-eventing:action");

  if (action == "notify") {
    //
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");

    if ( topic == "NB4" ) {
      //
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received Signal Topic NB4");
      
      interface::bril::shared::DatumHead * inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());

      {
        LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received Signal Topic !!!! beam !!!");

        std::lock_guard<std::mutex> lock(_mutex);
      
        _tcds.lhc_fill     = inheader->fillnum;
        _tcds.cms_run      = inheader->runnum;
        _tcds.lumi_section = inheader->lsnum;
        _tcds.lumi_nibble  = inheader->nbnum;
      }      

      if ( _cfg->isOrbitRequestedEveryNB4() ) {
        //
        _acq->acquireOrbit();
       }      

    } else if ( topic == "NB1" ) {
      //
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received Signal Topic NB1");
      
      if ( _cfg->isOrbitRequestedEveryNB1() ) {
        //
        _acq->acquireOrbit();
      }

    } else if ( topic == "NB64" ) {
      //
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received Signal Topic !!!! NB64 !!!");

      if ( _cfg->isOrbitRequestedEveryNB64() ) {
        //
        _acq->acquireOrbit();
       }

    } else if ( topic == "beam" ) {

      try
      {
        //
        interface::bril::shared::DatumHead * inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
        
        interface::bril::shared::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());

        char beam_mode[50];

        tc.extract_field(beam_mode, "status", inheader->payloadanchor);

        {
          std::lock_guard<std::mutex> lock(_mutex);
          if (std::string(beam_mode) != _beam_mode) {
            std::stringstream ss;
            ss << "New beam mode: " << std::string(beam_mode);
            LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
          }
          _beam_mode = std::string(beam_mode);

          if (_beam_mode == "INJECTION PROBE BEAM" || _beam_mode == "INJECTION SETUP BEAM" || _beam_mode == "INJECTION PHYSICS BEAM" || _beam_mode == "PREPARE RAMP" || _beam_mode == "RAMP" || _beam_mode == "FLAT TOP" || _beam_mode == "SQUEEZE" || _beam_mode == "ADJUST" || _beam_mode == "STABLE BEAMS" || _beam_mode == "UNSTABLE BEAMS" || _beam_mode == "BEAM DUMP WARNING" || _beam_mode == "BEAM DUMP") {
            if(!_do_store) LOG4CPLUS_INFO(this->getApplicationLogger(), "STORE ON");
            _do_store = true;
          } else {
            if(_do_store) LOG4CPLUS_INFO(this->getApplicationLogger(), "STORE OFF");
            _do_store = false;
          }
        }
      }
      catch(std::exception & ex)
      {
        std::string msg = "Extracting beam mode from beam topic: "; msg += ex.what(); LOG4CPLUS_ERROR(this->getApplicationLogger(), msg);
      }
    }
  }
}


void bril::bcm1futcasource::Application::startHistogramReadingWorkLoops() {
  /**
  */
  for ( auto && d : _cfg->getListOfDevices() ) {

    toolbox::task::WorkLoop * readingwl = NULL;

    try
    {
      std::ostringstream ss;

      ss << _appDescriptor->getURN() << "_" << _instance.toString() << "_reading_hist_";

      ss << d;

      readingwl    = toolbox::task::getWorkLoopFactory()->getWorkLoop(ss.str(),"waiting");
    }
    catch(toolbox::task::exception::Exception & e)
    {
      //
      LOG4CPLUS_FATAL(getApplicationLogger(), e.message());

      LOG4CPLUS_FATAL(getApplicationLogger(), "The exception raised at " << e.module() << ":" << e.line());

      stopHistogramReadingWorkLoops(); toolbox::getRuntime()->halt(-2); return;
    }

    {
      std::lock_guard<std::mutex> lock(_mutex);

      try {
        // start workloop
        _reading_hist_wl.emplace(std::make_pair(readingwl,d));

        //
      } 
      catch( std::exception & e ) {
        //
        LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to start workloops for histogram readout");

        stopHistogramReadingWorkLoops(); toolbox::getRuntime()->halt(-2);

        return;
      }
    } // end of std::lock_guard on _mutex

    try
    {
      // 
      toolbox::task::ActionSignature* as_reading = toolbox::task::bind(this,&bril::bcm1futcasource::Application::histograms,"UBCM-UTCA");

      // activate and submit workloop "reading"
      //
      { std::lock_guard<std::mutex> lock(_mutex); readingwl->activate(); } 

      { std::lock_guard<std::mutex> lock(_mutex); readingwl->submit(as_reading); }
    }
    catch(toolbox::task::exception::Exception& e)
    {
      LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to start workloops for histogram readout");

      stopHistogramReadingWorkLoops(); toolbox::getRuntime()->halt(-2); return;
    }
  }
}   


void bril::bcm1futcasource::Application::stopHistogramReadingWorkLoops() {
  /**
  */
  {
    std::lock_guard<std::mutex> lock(_mutex); _terminate = true; 
  }
  std::map<toolbox::task::WorkLoop *, std::string>::const_iterator it;

  LOG4CPLUS_INFO(getApplicationLogger(),"Stopping histogram readout work loops...");

  try
  {
    for( it=_reading_hist_wl.begin(); it != _reading_hist_wl.end(); ++it )
    {
      if ( it->first != NULL) it->first->cancel();
    }
  }
  catch(std::exception &) {} 

  LOG4CPLUS_INFO(getApplicationLogger(),"Done.");
}


bool bril::bcm1futcasource::Application::histograms(toolbox::task::WorkLoop* w) {
  /**
  */
  {
    std::lock_guard<std::mutex> lock(_mutex);  if ( _terminate ) return false; 
  }

  try{
    //
    _acq->acquireHistograms(_reading_hist_wl[w]);
    //
  } catch(bril::bcm1futcasource::exception::ubcmException & e) {
    //
    LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());

    bril::bcm1futcasource::DeviceDisconnectedEvent divice_disconnected(e);

    fireEvent(divice_disconnected);
  }
  return true;
}


void bril::bcm1futcasource::Application::requestData( xgi::Input* in, xgi::Output* out )
{
	out->getHTTPResponseHeader().addHeader( "Content-Type", "text/json" );

	if (  ! _enableHyperdaq )
	{
		*out << "{}" << std::endl; return;
	}
	cgicc::Cgicc cgi( in );

	std::vector<std::string> devices  = _cfg->getListOfDevices();

	for( std::string device : _cfg->getListOfDevices() ) {

		_acq->getUBCM(device)->writeDataAsJSON(device,in,out);
	}
}


void bril::bcm1futcasource::Application::requestHardwareStatus( xgi::Input* in, xgi::Output* out )
{
	out->getHTTPResponseHeader().addHeader( "Content-Type", "text/json" );

	if (  ! _enableHyperdaq )
	{
		*out << "{}" << std::endl; return;
	}

	 *out << "{" << std::endl;

	 bool bPrintComma = false;

	 for( std::string device : _cfg->getListOfDevices() ) {
		 //
		 if ( bPrintComma )
		 {
			 *out << "," << std::endl;
		 }
		 else
		 {
			 *out << std::endl; bPrintComma = true;
		 }

		 *out << "   \"" << device << "\" :  {" << std::endl;

		 bool bPrintCommaNested = false;

		 // std::lock_guard<std::mutex> lock(_mutex);

		 for( auto topic : _acq->getUBCM(device)->getInfo() ) {
			 //
			 if ( bPrintCommaNested )
			 {
				 *out << "," << std::endl;
			 }
			 else
			 {
				 *out << std::endl; bPrintCommaNested = true;
			 }
			 *out << "      \"" <<  topic.first << "\" : \"" << topic.second << "\"";
		 }

		 if ( _acq->getUBCM(device)->isReady() )
		 {
			 *out << ",\n      \"" <<  "status" << "\" : \"" << "Ready" << "\"";
		 }
		 else
		 {
			 *out << ",\n      \"" <<  "status" << "\" : \"" << "Failed" << "\"";
		 }
		 *out << std::endl << "   }";
	 }
	 *out << std::endl << "}" << std::endl;
}

void bril::bcm1futcasource::Application::htmlHardwareStatusTables(xgi::Output * out) {
	//
	*out << cgicc::comment() << "------------------------------------------------------------------------" << cgicc::comment() << std::endl;

	*out << cgicc::comment() << "Building hardware status tables with automatic refresh on time intervals" << cgicc::comment() << std::endl;

	*out << cgicc::script() << std::endl;

	*out << "$(document).on( 'xdaq-post-load', function() {" << std::endl;

	*out << "console.time();" << std::endl;

	*out << "console.log('getJSON on load from " << _urlToRequestHardwareStatus << "');" << std::endl;

	*out << "$.getJSON('" << _urlToRequestHardwareStatus << "', function ( data ) { " << std::endl;

	*out << "$.each( data, function(device, info )  { " << std::endl;

	*out << "var status = info['status'];" << std::endl;

	// ---- Writing tables -----

    *out << "var table_id= 'hwst_table_' + device;" << std::endl;

    *out << "var table = '<table id=\"' + table_id + '\" ';" << std::endl;

    *out << "table += 'class=\"xdaq-table-tree xdaq-param-viewer\" style=\"min-width:400px\">';" << std::endl;

    *out << "if ( status === 'Ready' ) {" << std::endl;

    *out << "table += '<thead><tr><th>' + device + '</th><th id=\"' + table_id + '_status\" style=\"background-color:green;color:gold\">' + status + '</th></tr></thead>'; " << std::endl;

    *out << "} else { " << std::endl;

    *out << "table += '<thead><tr><th>' + device + '</th><th id=\"' + table_id + '_status\" style=\"background-color:red;color:white\">' + status + '</th></tr></thead>'; " << std::endl;

    *out << "};" << std::endl;

    *out << "table += '<tbody>';" << std::endl;

	*out << "var cell_id_counter = 0;" << std::endl;

	*out << "$.each(info, function( key, value ) { " << std::endl;

	*out << "if ( key == 'status' ) return;" << std::endl;

	*out << "var cell_id = table_id + '_' + cell_id_counter; " << std::endl;

	*out << "cell_id_counter++;" << std::endl;

	*out << "table += '<tr><td>' + key + '</td><td id=\"' + cell_id + '\">' + value + '</td></tr>';" << std::endl;

    *out << "});" << std::endl; // end of .each(info)

    *out << "table += '</tbody></table>';" << std::endl;

	*out << "$('#hwst').append(table);" << std::endl;

	*out << "});" << std::endl; // end of .each(data)

	*out << "}).fail( function () { " << std::endl;

	*out << "document.getElementById(\"hwst\").innerHTML = \"<span style='color:red'>Failed to request hardware status</span><br><br>Check the link: " << _urlToRequestHardwareStatus << "\"; " << std::endl;

	*out << "document.getElementById(\"hwst\").innerHTML += \"<br><br>The URL has to return a valid JSON document\";" << std::endl;

	*out << "console.log('getJSON failed'); } );" << std::endl;

	*out << "console.timeEnd();" << std::endl;

	*out <<	"} );" << std::endl;

	// --------------------------------------------------------------------------------------------------
	// Refresh tables
	//
	*out << "function refresh_hwst_tables() {" << std::endl;

	*out << "console.log('getJSON on timer event from " << _urlToRequestHardwareStatus << "');" << std::endl;

	*out << "$.getJSON('" << _urlToRequestHardwareStatus << "', function ( data ) { " << std::endl;

	*out << "$.each( data, function(device, info )  { " << std::endl;

	*out << "var status = info['status'];" << std::endl;

	// ---- Writing tables -----

    *out << "var table_id= 'hwst_table_' + device;" << std::endl;

    *out << "if ( status === 'Ready' ) {" << std::endl;

    *out << "document.getElementById(table_id+'_status').style.backgroundColor='green';" << std::endl;
    *out << "document.getElementById(table_id+'_status').style.color='gold';"            << std::endl;
    *out << "document.getElementById(table_id+'_status').innerHTML ='Ready';"            << std::endl;

    *out << "} else { " << std::endl;

    *out << "document.getElementById(table_id+'_status').style.backgroundColor='red';" << std::endl;
    *out << "document.getElementById(table_id+'_status').style.color='white';"         << std::endl;
    *out << "document.getElementById(table_id+'_status').innerHTML ='Failed';"         << std::endl;

    *out << "}" << std::endl; // end of if

	*out << "var cell_id_counter = 0;" << std::endl;

    *out << "$.each(info, function( key, value ) { " << std::endl;

    *out << "if ( key == 'status' ) return;" << std::endl;

    *out << "var cell_id = table_id + '_' + cell_id_counter; " << std::endl;

    *out << "cell_id_counter++;" << std::endl;

    *out << "document.getElementById(cell_id).innerHTML=value;" << std::endl;

    *out << "});" << std::endl; // end of .each(info)

    *out << "});" << std::endl; // end of .each(data...

	*out << "});" << std::endl; // end of .getJSON(...

	*out << "}" << std::endl; // end of the function refresh_hwst_tables()

	*out << "window.setInterval(refresh_hwst_tables, 10000);" << std::endl;

	*out << cgicc::script() << std::endl;

	*out << cgicc::comment() << "------------------------------------------------------------------------" << cgicc::comment() << std::endl;

	*out << std::endl;
}
