// $Id$
#include "bril/bcm1futcasource/Monitoring.h"

bril::bcm1futcasource::Monitoring::Monitoring(xdaq::Application * application) {
    _rt_start = std::chrono::system_clock::now();
}

float bril::bcm1futcasource::Monitoring::getApplicationUptime() const {
    //
    return std::chrono::duration<float, std::ratio<3600l> >(std::chrono::system_clock::now() - _rt_start).count();
}

