#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "bril/hfprocessor/HFProcessor.h" 
#include "bril/hfprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>
#include <stdio.h>
#include <string.h>

#include "interface/bril/BEAMTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::hfprocessor::HFProcessor)

using namespace interface::bril;
 
bril::hfprocessor::HFProcessor::HFProcessor (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::hfprocessor::HFProcessor::Default,"Default");
    b2in::nub::bind(this, &bril::hfprocessor::HFProcessor::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor= getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_collectnb = 4;
    m_nHistsDiscarded = 0;
    m_alignedNB4 = false;
    lumiTotal = 0;
    muTotal = 0;
    computeLumiNow = false;
    saneData = false;
    rejectHistUntilNewLN = false;
    m_vdmflagTopic.fromString("vdmflag");
    m_vdmflag = false;

    lastPubRun=1;
    lastPubLS=1;
    lastPubLN=1;
    lastPub=1;
    hfAfterGlowTotalScale=1;

    thisLumiTopic="";

    m_mon_nbMod4PerBoard.resize(36, -1);
    m_mon_lumiPerBoard.resize(36, 0);
    m_mon_bunchMask.resize(interface::bril::shared::MAX_NBX,0);
    m_mon_nActiveBX = 0;
    m_mon_nBoards   = 0;
    m_mon_nCollected=0;
    BMthresET= 0.001;
    BMthresOC= 8e-6;
    NearNoiseValue = 1e-4;
    AboveNoiseValue = 1e-5;
    BMmin = 1e-2;
    HCALMaskLow = 3481;
    HCALMaskHigh = 3499;
    MAX_NBX_noAG = 3480;
    
    m_beammode = "";
        
    afterglowBufRef=0;
    pedBufRef = 0;
    histBufRef = 0;
    bxBufRef = 0;
    
    // Bunch mask
    memset (m_iscolliding1, false, sizeof (m_iscolliding1) );
    memset (m_iscolliding2, false, sizeof (m_iscolliding2) );
    memset (m_machine_mask, false, sizeof (m_machine_mask) );

  
    debugStatement.str("");
    debugStatement.precision(3);
    
    toolbox::net::URN memurn("toolbox-mem-pool",m_classname+"_mem"); 
    try{
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool(memurn,allocator);
        m_appInfoSpace->fireItemAvailable("eventinginput",&m_datasources);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got input eventing topics");
        m_appInfoSpace->fireItemAvailable("eventingoutput",&m_outputtopics);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got output eventing topics");
        m_appInfoSpace->fireItemAvailable("calibtag",&m_calibtag);
        m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_publishing","waiting");
   
        //CP my stuff
        m_appInfoSpace->fireItemAvailable("sigmaVis",                 &sigmaVis);
        m_appInfoSpace->fireItemAvailable("lumiAlgo",                 &lumiAlgo);
        m_appInfoSpace->fireItemAvailable("quadraticCorrection",      &quadraticCorrection);
        m_appInfoSpace->fireItemAvailable("orbits_per_nibble",        &orbits_per_nibble);
        m_appInfoSpace->fireItemAvailable("integration_period_nb",    &integration_period_nb);
        m_appInfoSpace->fireItemAvailable("nibbles_per_section",      &nibbles_per_section);
        m_appInfoSpace->fireItemAvailable("applyAfterglowCorr",       &applyAfterglowCorr);
        m_appInfoSpace->fireItemAvailable("applyPedestalCorr",        &applyPedestalCorr);
        m_appInfoSpace->fireItemAvailable("useCustomBXMask",          &useCustomBXMask);
        m_appInfoSpace->fireItemAvailable("vdmflagTopic",             &m_vdmflagTopic);
        m_appInfoSpace->fireItemAvailable("BMthresET",                &BMthresET);
        m_appInfoSpace->fireItemAvailable("BMthresOC",                &BMthresOC);
        m_appInfoSpace->fireItemAvailable("NearNoiseValue",           &NearNoiseValue);
        m_appInfoSpace->fireItemAvailable("AboveNoiseValue",          &AboveNoiseValue);
        m_appInfoSpace->fireItemAvailable("BMmin",                    &BMmin);

    } catch(xcept::Exception& e){
        std::string msg("Failed to setup memory pool ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        //XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);  
    }

    SetupMonitoring();
}

bril::hfprocessor::HFProcessor::~HFProcessor (){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::hfprocessor::HFProcessor::Default (xgi::Input * in, xgi::Output * out){
  //m_applock.take();

  ///////*out << busesToHTML();
  ///////std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
  ///////*out << "URL: "<< appurl;
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::caption("input/output");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("calibtag");
  ///////*out << cgicc::td( m_calibtag );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("sigmaVis");
  ///////*out << cgicc::td( sigmaVis.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("in_topic");
  ///////*out << cgicc::td( interface::bril::hfCMS1T::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfOcc1AggT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfoclumiT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("# hists discarded on LS/run boundary");
  ///////*out << cgicc::td( m_nHistsDiscarded.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();  
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  ///////*out << "Automatic channel masking is ";
  ///////if (m_useAutomaticChannelMasking)
  ///////  {
  ///////    *out << "ON";
  ///////  }
  ///////else
  ///////  {
  ///////    *out << "OFF";
  ///////  }
  ///////*out << cgicc::br();
  ///////*out << cgicc::br();

  ///////*out << "Permanently-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!m_useChannelForLumi[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "Temporarily-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!boardAvailableThisBlock[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "The current background numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 1");
  ///////*out << cgicc::td( m_BG1_plus.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 2");
  ///////*out << cgicc::td( m_BG2_minus.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("How many times is albedo higher than background?");
  ///////*out << cgicc::td( xdata::Integer(m_countAlbedoHigherThanBackground).toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "And the current lumi average numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (calibrated)");
  ///////*out << cgicc::td( lumiTotal.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (raw)");
  ///////*out << cgicc::td( lumiTotal_raw.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "First few elements from lumi histogram: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////for (unsigned int bin = 0; bin < 20; bin++)
  ///////  {
  ///////    *out << cgicc::tr();
  ///////    *out << cgicc::th( xdata::Integer(bin).toString() );
  ///////    *out << cgicc::td( xdata::Float(lumiHistPerBX[bin]).toString() );
  ///////    *out << cgicc::tr();
  ///////  }

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  //m_applock.give();
}

void bril::hfprocessor::HFProcessor::SetupMonitoring(){
    LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace( "hfProcessorMon" ).toString();
    m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
    const int nvars = 11;
    const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", "nActiveBX", "nBoards", "statsCollected", "nbMod4PerBoard", "lumiPerBoard", "bunchMask"};
    xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_mon_nActiveBX, &m_mon_nBoards, &m_mon_nCollected, &m_mon_nbMod4PerBoard, &m_mon_lumiPerBoard, &m_mon_bunchMask };
    for ( int i = 0; i != nvars; ++i ){
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
    }
}


// Sets default values of application according to parameters in the 
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::hfprocessor::HFProcessor::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    std::stringstream msg;
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        size_t nsources = m_datasources.elements();
        try{
            for(size_t i=0;i<nsources;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
                xdata::String databus;
                xdata::String topicsStr;
                if(p){
                  databus = p->getProperty("bus");     
                  topicsStr = p->getProperty("topics");
                  std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,",");     
                  m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
                }
            }
            
            this->getEventingBus("brildata").subscribe(interface::bril::beamT::topicname());
            
            subscribeall();
            size_t ntopics = m_outputtopics.elements();
            for(size_t i=0;i<ntopics;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));
                if(p){
                  xdata::String topicname = p->getProperty("topic");
                  xdata::String outputbusStr = p->getProperty("buses");
                  std::set<std::string> outputbuses = toolbox::parseTokenSet(outputbusStr.value_,",");
                  for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){
                    m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));
                  }
                  m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
                  m_unreadybuses.insert(outputbuses.begin(),outputbuses.end());
                }
            }
            for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
                debugStatement<<"listening?"<<*it;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                this->getEventingBus(*it).addActionListener(this);
            }
    
            integration_period_orbits=integration_period_nb.value_ * orbits_per_nibble.value_;
            integration_period_secs=integration_period_orbits / 11245.6;
            
            if (lumiAlgo.value_ == "occupancy") {
                thisLumiTopic.append(interface::bril::hfoclumiT::topicname());
            } else if (lumiAlgo.value_ == "etsum") {
                thisLumiTopic.append(interface::bril::hfetlumiT::topicname());
            }
            InitializeHFSBR(); //need lumiAlgo
    
        }catch(xdata::exception::Exception& e){
            msg<<"Failed to parse application property";
            LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception, msg.str(), e);
        }      
    }
}

// When buses are ready to publish, start the publishing workloop and 
// timer to check for stale cache. Listens for toolbox::Event
void  bril::hfprocessor::HFProcessor::actionPerformed(toolbox::Event& e){
    if(e.type() == "eventing::api::BusReadyToPublish"){
        std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
        std::stringstream msg;
        msg<< "event Bus '" << busname << "' is ready to publish";
        m_unreadybuses.erase(busname);
        if(m_unreadybuses.size()!=0) return; //wait until all buses are ready
        try{    
            toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::hfprocessor::HFProcessor::publishing,"publishing");
            m_publishing->activate();
            m_publishing->submit(as_publishing);
        }catch(toolbox::task::exception::Exception& e){
            msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg.str(),e);  
        }    
        try{
            std::string appuuid = m_appDescriptor->getUUID().toString();
            // check timeExpired every 120 seconds
            // huge time right now because timeExpired is currently doing nothing
            toolbox::TimeInterval checkinterval(120,0);
            std::string timername("stalecachecheck_timer");
            toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername+"_"+appuuid);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate(start, this, checkinterval, 0, timername);
        }catch(toolbox::exception::Exception& e){
            std::string msg("failed to start timer ");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e) );
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);
        }
    }
}

// subscribe to all input buses
void bril::hfprocessor::HFProcessor::subscribeall(){
    for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
        std::string busname = bit->first; 
        for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){       
            LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit); 
            try{
                this->getEventingBus(busname).subscribe(*topicit);
            }catch(eventing::api::exception::Exception& e){
                LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
                m_in_busTotopics[busname].erase(*topicit);
            }
        }
    }
}

// Listens for reception of data on the eventing.  
// If full statistics collected, call do_process on the already-accumulated nibbles.
void bril::hfprocessor::HFProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
    debugStatement<<"In onMessage";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        curTopic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string v = plist.getProperty("DATA_VERSION");
        if(v.empty()){
            std::string msg("Received data message without version header, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }
        if(v!=interface::bril::shared::DATA_VERSION){
            std::string msg("Mismatched bril data version in received message, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }    
    
        debugStatement<<"Received data from "+topic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());

        if( topic == m_vdmflagTopic.value_ ){
            bool flag = false;
            interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
            flag = me->payload()[0];

            debugStatement<<"cur flag in prog "<<m_vdmflag<<" flag from topic "<<flag ;
            LOG4CPLUS_DEBUG(getApplicationLogger(),debugStatement.str());
            debugStatement.str("");
            
            if( m_vdmflag==true && flag==false ){
                m_vdmflag = false;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING OFF VDMFLAG--BUNCHMASK IS DYNAMIC");
            }else if(  m_vdmflag==false && flag==true ){
                m_vdmflag = true;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING ON VDMFLAG--BUNCHMASK IS FIXED");
            }
        }
        
        if (topic == interface::bril::beamT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", making bunch masks";
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
            
            std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
            interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                
            tc.extract_field(m_iscolliding1, "bxconfig1", inheader->payloadanchor);
            tc.extract_field(m_iscolliding2, "bxconfig2", inheader->payloadanchor);
        }


        if( topic == interface::bril::hfCMS1T::topicname() ){
            if (lumiAlgo == "occupancy") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS1 topic found but you aren't running occupancy lumi.");
            }
        } else if( topic == interface::bril::hfCMS_ETT::topicname()) {
            if (lumiAlgo == "etsum") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS_ET topic found but you aren't running etsum lumi.");
            }
        } else if( topic == interface::bril::hfCMS_VALIDT::topicname() ) {
            RetrieveSourceHistos(ref, inheader);
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}

// When HF data received on eventing, check and store it
void bril::hfprocessor::HFProcessor::RetrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::shared::DatumHead * inheader) {
    // assume data is guilty until proven innocent
    saneData = false;
    newLumiBlock = false;

    debugStatement<<"START:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int crate     = inheader->getDataSourceID();
    unsigned int board     = inheader->getChannelID();
    unsigned int boardID   = crate*100+board;
    unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    debugStatement<<"crate, board, boardID "<<crate<<","<<board<<","<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int runnum = inheader->runnum;
    unsigned int lsnum = inheader->lsnum;
    unsigned int nbnum = inheader->nbnum;
    debugStatement<<"Received data from run "<<runnum<<" ls "<<lsnum<<" nb "<<inheader->nbnum<<" boardID "<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    std::string topic;


    // Check if we have collected all channels for all nibbles
    // If so, then process the lot -- starting at last_header
    // (NOT current header!)
   
    debugStatement<< "process last header: run " << prevDataHeader.runnum 
               << " ls " << prevDataHeader.lsnum 
               << " nb " << prevDataHeader.nbnum
               << " (right now in run) " << runnum 
               << " ls " << lsnum 
               << " nb " << nbnum 
               << " Lumi: " << lumiTotal 
               << " Lumi raw: " << muTotal;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");



    // FIXME add consistency checks here

    // CP HACK to make lumi nibble line up.
    // MUST DO BEFORE ANALYZING
    debugStatement<<"making the 4nib line up "<<integration_period_nb.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(integration_period_nb.value_>1){
        int modnib=inheader->nbnum % integration_period_nb.value_;
        m_mon_nbMod4PerBoard[boardIndex]=m_mon_nbMod4PerBoard[boardIndex]+modnib; 
        debugStatement<<"nib "<<inheader->nbnum<<" int period "<<integration_period_nb.value_<<" mod "<<modnib;
        if(modnib!=0){
            // always advance
            inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            //democratic
            ////float offsetFrac = (float)modnib / integration_period_nb.value_;
            ////if(offsetFrac>0.5){
            ////    inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            ////} else {
            ////    inheader->nbnum=inheader->nbnum-(modnib);
            ////}
            if(inheader->nbnum==0) {
                inheader->nbnum=64;
                inheader->lsnum-=1;
                debugStatement<<" new LS "<<inheader->lsnum;
            }
            debugStatement<<" new nib "<<inheader->nbnum;
        }
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        runnum = inheader->runnum;
        lsnum = inheader->lsnum;
        nbnum = inheader->nbnum;
    }


    //SanityChecks
    if(runnum==prevDataHeader.runnum&&lsnum==prevDataHeader.lsnum&&nbnum==prevDataHeader.nbnum){
        saneData=true;
    } else if((runnum-prevDataHeader.runnum)>0){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New run");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==1)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New LS");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum>1)) {
        debugStatement<<"New LS - "<<lsnum-prevDataHeader.lsnum<<" LSs later";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        newLumiBlock = true;
        saneData=true;

    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum==4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New block, same run,ls - 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum>4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(),"New block, same run,ls - more than 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else {
        std::stringstream message;
        message<<"INSANE DATA!!\n";
        message<<"\t PREV run,ls,ln "<<prevDataHeader.runnum<<","<<prevDataHeader.lsnum<<","<<prevDataHeader.nbnum<<"\n";
        message<<"\t THIS run,ls,ln "<<runnum<<","<<lsnum<<","<<nbnum<<"\n";
        message<<"\t THIS boardID "<<boardID;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());
    }

    //if(runnum==lastPubRun&&lsnum==lastPubLS&&nbnum==lastPubLN){
    double thisLN=runnum*1e7+lsnum*1e2+nbnum;
    if(thisLN<=lastPub){
        debugStatement.precision(13);
        debugStatement<<"Last published "<<lastPub<<"  This nibble "<<thisLN
                <<"  Diff "<<lastPub-thisLN<<" dropping this histogram";
        debugStatement<<"  crate, board  "<<crate<<","<<board;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
        saneData=false;
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    double timeInProcessor=(double)timeNow.sec()+(double)timeNow.millisec()/1000.;
    double timeInHeader=(double)inheader->timestampsec+(double)inheader->timestampmsec/1000.;
    if(timeInProcessor-timeInHeader>0.3){
        debugStatement.precision(4);
        debugStatement<<"Larger than 0.2 s between InProcessor InSource Diff/InBus "<<timeInProcessor<<" "<<timeInHeader<<" "<<timeInHeader-timeInProcessor<<" "<<curTopic;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
    }
    if(!saneData) return;

    if(newLumiBlock&&NBoardsCollected>0) {
        computeLumiNow=true;
    }

    if(newLumiBlock){
        firstDataReceived = timeInProcessor;
        debugStatement<<"newLumiBlock:  NBoardsCollected "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        //rejectHistUntilNewLN = false;
    } 


    if(computeLumiNow){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "found new lumi block-going to send lumi");
        do_process(prevDataHeader);
        computeLumiNow=false;
    }


    // Only proceed if the received topic if it is the raw (occ/etsum) hist or the validity hist
    if( (curTopic == interface::bril::hfCMS1T::topicname() && lumiAlgo == "occupancy") 
     || (curTopic == interface::bril::hfCMS_ETT::topicname() &&lumiAlgo == "etsum") ){
        debugStatement<<"FOUND raw hist topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfRawHist.find(boardID);
        if(boardIter==hfRawHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfRawHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+1;
            timeHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
    
    if( curTopic == interface::bril::hfCMS_VALIDT::topicname() ) {
        debugStatement<<"FOUND hfCMS_VALIDT topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfValidHist.find(boardID);
        if(boardIter==hfValidHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfValidHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+2;
            timeValidHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
   
    if(boardAvailableThisBlock[boardIndex]==3){
        NBoardsCollected++;
        debugStatement<<"NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(NBoardsCollected==36){
            debugStatement<<"all boards collected--going to send lumi";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            do_process(prevDataHeader);
        }    
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    lastDataAcquired = timeNow.sec();
    prevDataHeader = *inheader;

    debugStatement<<"END:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}

// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
//  * luminosity numbers
//  * BX mask
void bril::hfprocessor::HFProcessor::do_process(interface::bril::shared::DatumHead& inheader){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Now entered do_process: VERY BEGINNING");
    
    std::stringstream ss;
    ss.str("");
    ss << "do_process run " << inheader.runnum 
       << " ls " << inheader.lsnum
       << " nb " <<inheader.nbnum
       << " channel " << inheader.getChannelID();
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
    
    debugStatement<<"Total:  "<<NBoardsCollected<<" \n";
    debugStatement<<" run " << prevDataHeader.runnum 
       << " ls " << prevDataHeader.lsnum 
       << " nb " << prevDataHeader.nbnum<<"\n";
    debugStatement<<"Missing ";
    for(int thisInd=0; thisInd<36; thisInd++){
        if(boardAvailableThisBlock[thisInd]!=3){
            debugStatement<<thisInd<<" "<<boardAvailableThisBlock[thisInd]<<" ";
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // 1. first make agghists, using same loop calculate total rates
    LOG4CPLUS_DEBUG(getApplicationLogger(), "About to SumHists");
    debugStatement<<"1) read histograms into local memory\n";
    debugStatement<<"  a) all boards merged \n";
    debugStatement<<"  b) each board \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    bool success=SumHists(inheader);
    debugStatement<<"  c) clear cache of hists \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(!success) {
        ClearCache();
        return;
    }
    
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "2) compute/publish data");
    ComputeLumi(inheader);
    lastPubRun=inheader.runnum;
    lastPubLS=inheader.lsnum;
    lastPubLN=inheader.nbnum;
    lastPub=lastPubRun*1e7+lastPubLS*1e2+lastPubLN;
    
    ClearCache();
}

// Loops  through  uHTR histograms,   makes the agghists for  each c hannel
// and at the same time creates total rates for each channel for monitoring 
// purposes and) making the channel mask. 
//
// Queues the agghists for publishing -->

bool bril::hfprocessor::HFProcessor::SumHists(interface::bril::shared::DatumHead& inheader) {
    // assume failure until success
    bool success=false;
    
    memset(aggHistPerBoardPerBX  ,0,sizeof(aggHistPerBoardPerBX));
    memset(aggValidHistPerBoardPerBX,0,sizeof(aggValidHistPerBoardPerBX));
    memset(muHistPerBoardPerBX   ,0,sizeof(muHistPerBoardPerBX));
    memset(lumiHistPerBoardPerBX ,0,sizeof(lumiHistPerBoardPerBX));
    memset(lumiHistPerBoard      ,0,sizeof(lumiHistPerBoard));
    memset(aggHistPerBX          ,0,sizeof(aggHistPerBX));
    memset(aggValidHistPerBX        ,0,sizeof(aggValidHistPerBX));
    memset(muHistPerBX           ,0,sizeof(muHistPerBX));
    memset(lumiHistPerBX         ,0,sizeof(lumiHistPerBX));
      
    if(!m_vdmflag){
        memset(activeBXMask         ,false,sizeof(activeBXMask));
        NActiveBX=0;
        maxBX=0;
        firstBX=-1;
    }
    
    size_t bxbins = interface::bril::shared::MAX_NBX;

    try{
        debugStatement<<"size of hfRawHist "<<hfRawHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(hfRawHist.size()==0){
            LOG4CPLUS_INFO(getApplicationLogger(), "no data?!  ugh i'm done (SumHists)");
            return success;
        }
        
        // loop over occupancy histograms; 
        // ensure there is a corresponding valid histogram;
        // extract primitive data; 
        // sum up per BX over all boards
        // save which boards were fully functional
        for(std::map<unsigned int, toolbox::mem::Reference*>::iterator boardIter=hfRawHist.begin(); boardIter!=hfRawHist.end(); ++boardIter){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Accessing this channel of histcache");
            
            unsigned int channelid = boardIter->first;
            unsigned int crate = channelid/100;
            unsigned int board = channelid-crate*100;
            unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    
            if(crate!=22&&crate!=29&&crate!=32){
                debugStatement<<"crate is not 22,29,32 "<<crate;
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
            }
    
            std::map<unsigned int, toolbox::mem::Reference*>::iterator validIter=hfValidHist.find(channelid);
            if(validIter==hfValidHist.end()){
                debugStatement<<"crate, board, boardIndex "<<crate<<" "<<board<<" "<<boardIndex;
                debugStatement<<"\nThere is no valid histogram for this occupancy hist... skipping it.";
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                continue;
            }
            
            toolbox::mem::Reference* histref = boardIter->second;
            interface::bril::hfCMS1T* histdataptr = (interface::bril::hfCMS1T*)(histref->getDataLocation());
            
            toolbox::mem::Reference* validref = validIter->second;
            interface::bril::hfCMS_VALIDT* validdataptr = (interface::bril::hfCMS_VALIDT*)(validref->getDataLocation());
            
            // extract primitive data; 
            // sum up per BX over all boards
            for(size_t bxid=0; bxid<bxbins; bxid++){
                aggHistPerBoardPerBX[boardIndex][bxid]+=histdataptr->payload()[bxid];
                aggValidHistPerBoardPerBX[boardIndex][bxid]+=validdataptr->payload()[bxid];
                aggHistPerBX[bxid]+=aggHistPerBoardPerBX[boardIndex][bxid];
                aggValidHistPerBX[bxid]+=aggValidHistPerBoardPerBX[boardIndex][bxid];
            }
            
            for(size_t bxid=0; bxid<bxbins; bxid++){
                if(aggValidHistPerBX[bxid]>0){
                    aggNormHistPerBX[bxid]=aggHistPerBX[bxid]/aggValidHistPerBX[bxid];
                } else {
                    aggNormHistPerBX[bxid]=0;
                }
            }
            if(boardAvailableThisBlock[boardIndex]!=3){
                std::stringstream message;
                message<<"I have saved the occ/valid hist for "<<boardIndex<<" but boardAvailableThisBlock is "<<boardAvailableThisBlock[boardIndex]; 
                LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            }
            success=true;
        }

        //format aggregate hist
        
        if (lumiAlgo.value_ == "occupancy") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfOcc1AggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfOcc1AggT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtSumAggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfEtSumAggT::maxsize());
        }        

        interface::bril::shared::DatumHead* aggHistHeader= (interface::bril::shared::DatumHead*)(histBufRef->getDataLocation());
        aggHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        aggHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::FLOAT);

        if (lumiAlgo.value_ == "occupancy") {
            aggHistHeader->setTotalsize(interface::bril::hfOcc1AggT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            aggHistHeader->setTotalsize(interface::bril::hfEtSumAggT::maxsize());
        }

        aggHistHeader->setFrequency(integration_period_nb.value_);

        memcpy(aggHistHeader->payloadanchor,aggNormHistPerBX,(interface::bril::shared::MAX_NBX)*sizeof(float));

        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfOcc1AggT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfEtSumAggT::topicname());
        }                

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this agghist to output queue");
            it->second->push(histBufRef); //push agghist to publishing queue
        } else if(histBufRef) { 
            LOG4CPLUS_INFO(getApplicationLogger(),"histBufRef topic is not in topic queue... releasing memory");
            histBufRef->release(); 
            histBufRef= 0;
        }        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e) {
        if(histBufRef) { histBufRef->release(); histBufRef= 0;  }
        std::string msg("Failed to process data for agghist");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    }

    return success;
}



// Calculates the luminosity numbers (full-orbit values 
// and bunch-by-bunch histograms), 
// and queues them for publishing
void bril::hfprocessor::HFProcessor::ComputeLumi(interface::bril::shared::DatumHead& inheader){
    // NOOOOOWWWW for luminosity numbers
    
    toolbox::mem::Reference* lumiHistBufRef = 0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process: lumiHistBufRef");  
    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        if (lumiAlgo.value_ == "etsum") {
          pdict = interface::bril::hfetlumiT::payloaddict();
        }
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
    

        //format aggregate hist
        toolbox::mem::Reference * lumiHistBufRef = 0;
        
        if (lumiAlgo.value_ == "occupancy") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfoclumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfoclumiT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfetlumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfetlumiT::maxsize());
        }
    
        // fill, run, ls, ln, sec, msec
        //interface::bril::hfoclumiT* lumihist = (interface::bril::hfoclumiT*)(lumiHistBufRef->getDataLocation());
        interface::bril::shared::DatumHead* lumiHistHeader= (interface::bril::shared::DatumHead*)(lumiHistBufRef->getDataLocation());
        lumiHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        lumiHistHeader->setFrequency(integration_period_nb.value_);
    
        // sourceid, algo, channel, payloadtype
        lumiHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::COMPOUND);

        if (lumiAlgo.value_ == "occupancy") {
            lumiHistHeader->setTotalsize(interface::bril::hfoclumiT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistHeader->setTotalsize(interface::bril::hfetlumiT::maxsize());    
        }
    
        // resetting values of global variables to zero
        muTotal = 0;
        lumiTotal = 0;
    
        unsigned int maskhigh = 0; // board 32
        unsigned int masklow = 0; // boards 22 and 29
    
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
                float ratio=aggHistPerBoardPerBX[boardInd][bxid];
                if( aggValidHistPerBoardPerBX[boardInd][bxid] > 0 ){
                    ratio=ratio/aggValidHistPerBoardPerBX[boardInd][bxid];
                    if (lumiAlgo.value_ == "occupancy") {
                        muHistPerBoardPerBX[boardInd][bxid]=-log(1-ratio);
                    } else if (lumiAlgo.value_ == "etsum") {
                        muHistPerBoardPerBX[boardInd][bxid]=ratio;
                    }
                } else {
                   ratio=0;
                }
            }
        }

        // compute mu's first
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            float ratio=aggHistPerBX[bxid];
            if( aggValidHistPerBX[bxid] > 0 ){
                ratio=ratio/aggValidHistPerBX[bxid];
                if (lumiAlgo.value_ == "occupancy") {
                    muHistPerBX[bxid]=-log(1-ratio);
                } else if (lumiAlgo.value_ == "etsum") {
                    muHistPerBX[bxid]=ratio;
                }
                // add sanity check to exclude spikes
                if (muHistPerBX[bxid] > 50) {
                    muHistPerBX[bxid] = 0;
                }
            } else {
                ratio=0;
            }
        }
   
        // if scan is happening, lock the mask--don't change it
        if(useCustomBXMask) {
            setMask();
        } else {
            if(!m_vdmflag){
                MakeDynamicBXMask(inheader);
            }
        }
        // correct mu's for afterglow
        if(applyAfterglowCorr.value_){
            ComputeAfterglow(inheader);
        }
        
        if (applyPedestalCorr.value_) {
            if(lumiAlgo.value_ == "etsum"){
                SubtractPedestal(inheader);
            } else if (lumiAlgo.value_ == "occupancy") {
                SubtractPedestal(inheader);
            }
        }

        // compute sum of mu's (post correction)
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){

		    debugStatement<<" bxid "<<bxid<< " isActive  "<<activeBXMask[bxid]<<" PostCorrection "<<muHistPerBX[bxid];
	            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        	    debugStatement.str("");
            if(activeBXMask[bxid]){
                muTotal+=muHistPerBX[bxid];
            }
        }

        // compute lumi with corrected mu's (if afterglow corr is on)
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
                lumiHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*11245.6/sigmaVis;
                

                // non-linearity correction
                if(activeBXMask[bxid]){
                    lumiHistPerBoardPerBX[boardInd][bxid]=lumiHistPerBoardPerBX[boardInd][bxid]+quadraticCorrection*lumiHistPerBoardPerBX[boardInd][bxid]*lumiHistPerBoardPerBX[boardInd][bxid];
                }
            }
        }

        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            lumiHistPerBX[bxid]=(muHistPerBX[bxid])*11245.6/sigmaVis;
            // non-linearity correction
            if(activeBXMask[bxid]){
                lumiHistPerBX[bxid]= lumiHistPerBX[bxid]+quadraticCorrection*lumiHistPerBX[bxid]*lumiHistPerBX[bxid];
            }
        }


        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            if(activeBXMask[bxid]){
                lumiTotal+=lumiHistPerBX[bxid];
                for(int boardInd=0; boardInd<36; boardInd++){
                    if(boardAvailableThisBlock[boardInd]==3){
                        lumiHistPerBoard[boardInd]+=lumiHistPerBoardPerBX[boardInd][bxid];
                    }
                }
            }
        }
        
        // masklow is for presence of first two crates
        // maskhigh is for the third crate
        for(int boardInd=0; boardInd<36; boardInd++){
            if(boardAvailableThisBlock[boardInd]==3){
                m_mon_lumiPerBoard[boardInd]= m_mon_lumiPerBoard[boardInd]+lumiHistPerBoard[boardInd];
                if(boardInd<24){
                    masklow  += 2^(boardInd);
                } else {
                    maskhigh += 2^(boardInd-24);
                }
            }
        }

        // Make sure lumi is above zero before publishing (useful in case of custom mask) to avoid confusion to OMS people
        if (lumiTotal < 0 || lumiTotal > 70000) {
            muTotal = 0;
            lumiTotal = 0;
        }
      
        timeNow = toolbox::TimeVal::gettimeofday();
        unsigned int now = timeNow.sec();
        if(now-lastMessageTime>90){
            debugStatement<<"fill,run,LS,LN "<<inheader.fillnum<<","<<inheader.runnum<<","<<inheader.lsnum<<","<<inheader.nbnum
              <<" NActiveBX "<<NActiveBX
              <<" NBoardsCollected "<<NBoardsCollected
              <<" hfAfterGlowTotalScale "<<hfAfterGlowTotalScale
              <<" lumi combined "<<lumiTotal<<" per board ";
            for(int boardInd=0; boardInd<36; boardInd++){
                if(boardAvailableThisBlock[boardInd]==3){
                    debugStatement<<boardInd<<" "<<lumiHistPerBoard[boardInd]<<" ";
                }else{
                    debugStatement<<boardInd<<" N/A ";
                }
            }
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            lastMessageTime=now;
        }
        m_mon_fill = inheader.fillnum;
        m_mon_run  = inheader.runnum;
        m_mon_ls   = inheader.lsnum;
        m_mon_nb   = inheader.nbnum;
        m_mon_timestamp = timeNow;
        m_mon_nActiveBX = m_mon_nActiveBX+NActiveBX;
        m_mon_nBoards   = m_mon_nBoards  +NBoardsCollected;
        m_mon_nCollected++;
       
        if(inheader.lsnum!=lastPubLS){ // monitor every new LS
            //average over the m_mon_nCollected
            m_mon_nActiveBX=m_mon_nActiveBX/m_mon_nCollected;
            m_mon_nBoards=m_mon_nBoards/m_mon_nCollected;
            for(int index=0;index<36; index++){
                m_mon_nbMod4PerBoard[index]=m_mon_nbMod4PerBoard[index]/m_mon_nCollected;
                m_mon_lumiPerBoard[index]=m_mon_lumiPerBoard[index]/m_mon_nCollected;
            }
            //send data
            m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
            //reset everything
            m_mon_nActiveBX = 0;
            m_mon_nBoards   = 0;
            m_mon_nbMod4PerBoard.resize(36, -1);
            m_mon_lumiPerBoard.resize(36, 0);
            m_mon_bunchMask.resize(interface::bril::shared::MAX_NBX,0);
            m_mon_nCollected=0;
        }

        std::stringstream ss;
        ss.str("");
        ss << "Avg lumi: " << lumiTotal << "  Avg lumi raw: " << muTotal;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc(pdict);       
        tc.insert_field( lumiHistHeader->payloadanchor, "calibtag", m_calibtag.value_.c_str());
        tc.insert_field( lumiHistHeader->payloadanchor, "avgraw",   &muTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "avg",      &lumiTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "bxraw",    muHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "bx",       lumiHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "masklow",  &masklow);
        tc.insert_field( lumiHistHeader->payloadanchor, "maskhigh", &maskhigh);

        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfoclumiT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfetlumiT::topicname());
        }

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS");
            it->second->push(lumiHistBufRef);
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS - done");
        }      
        else
        {
            if(lumiHistBufRef) { lumiHistBufRef->release(); lumiHistBufRef= 0;  }
        }
        //m_topicoutqueues[interface::bril::hfoclumiT::topicname()]->push(lumiHistBufRef);
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(lumiHistBufRef) {
            lumiHistBufRef->release();
            lumiHistBufRef= 0;
        }
    }
}


void bril::hfprocessor::HFProcessor::MakeDynamicBXMask(interface::bril::shared::DatumHead& inheader, float fracThreshold){
    
    float minv=1;
    float maxv=0;
    NActiveBX=0;
    maxBX=0;
    firstBX=-1;

    for(int bxind=0; bxind<int(MAX_NBX_noAG); bxind++){


        float thisRatio=0;
        if (lumiAlgo.value_ == "occupancy") {
            thisRatio=aggHistPerBX[bxind];
            if(aggValidHistPerBX[bxind]>0){
                thisRatio=thisRatio/aggValidHistPerBX[bxind];
            } else {
                thisRatio=0;
            }
        } else if (lumiAlgo.value_ == "etsum") {
            thisRatio=muHistPerBX[bxind];
        }
        //only look at non-zero BXs
        if(minv>thisRatio && thisRatio!=0) minv=thisRatio;
        if(maxv<thisRatio) {
            maxv=thisRatio;
            maxBX=bxind;
        }
    }

    // if still 1, all bx are 0
    if(minv==1) minv=0;

    // minimum value of min is 1e-6
    float abMin=BMmin;
    int MaskLow = int(HCALMaskLow);
    int MaskHigh = int(HCALMaskHigh);

        //if (lumiAlgo.value_ == "etsum") abMin=1e-2;
    if (lumiAlgo.value_ == "etsum") abMin=BMthresET;
    if (lumiAlgo.value_ == "occupancy") abMin=BMthresOC;

    minv=std::max(minv,abMin);
    
    //make fraction dynamic
    //at very low lumi, need to distinguish more from noise... higher frac of diff
    //need smooth transition to normal fraction
    float nearNoise=NearNoiseValue;
    float aboveNoise=AboveNoiseValue;
    if (lumiAlgo.value_ == "etsum") {
        nearNoise=0.02;
        aboveNoise=0.05;
    }
    if( maxv-minv<nearNoise) fracThreshold=1.;
    else if(maxv<aboveNoise) fracThreshold= (aboveNoise-maxv+minv)/(aboveNoise-nearNoise) + (maxv-minv-nearNoise)*fracThreshold / (aboveNoise-nearNoise);

    float dynamicThreshold=std::max((maxv-minv)*fracThreshold,abMin);
    //float dynamicThreshold=(max-minv)*fracThreshold;
    debugStatement<<"minv, maxv, fracT, dynT "<<minv<<" "<<maxv<<" "<<fracThreshold<<" "<<dynamicThreshold;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    //if the difference between max and min is small compared to noise, then it is noise
    if(maxv-minv > 0.5*minv) {
        for(int bxind=0; bxind<interface::bril::shared::MAX_NBX; bxind++){
            float thisRatio=0;
	    bool MaskLED = bxind < MaskLow || bxind > MaskHigh;

            debugStatement<<"bxind  "<<bxind<<" MaskedLED  "<<MaskLED<<" Low "<<MaskLow<<" High "<<MaskHigh;
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");

            if (lumiAlgo.value_ == "occupancy") {
                thisRatio=aggHistPerBX[bxind];
                if(aggValidHistPerBX[bxind]>0){
                    thisRatio=thisRatio/aggValidHistPerBX[bxind];
                } else {
                    thisRatio=0;
                }
            } else if (lumiAlgo.value_ == "etsum") {
                thisRatio=muHistPerBX[bxind];
            }
    
            if( thisRatio-minv > dynamicThreshold && MaskLED) {
                debugStatement<<"passing threshold "<<bxind<<" "<<thisRatio<<" "<<minv;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                activeBXMask[bxind]=true;
                m_mon_bunchMask[bxind]=m_mon_bunchMask[bxind]+1;
                NActiveBX++;
                if(firstBX==-1) firstBX=bxind;
            }
        }
    }
    debugStatement<<"NActiveBX "<<NActiveBX;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

        std::stringstream message;
       message<<"================ CHECK THIS !!!!! NActiveBX "<<NActiveBX<<"  BunchMask Thershold  hfet "<<BMthresET<<" hfoc  "<<BMthresOC;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());

    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
        
        //format bx mask hist
        bxBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::bunchmaskT::maxsize());
        bxBufRef->setDataSize(interface::bril::bunchmaskT::maxsize());

        interface::bril::shared::DatumHead* maskHistHeader= (interface::bril::shared::DatumHead*)(bxBufRef->getDataLocation());
        maskHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        maskHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::COMPOUND);
        maskHistHeader->setTotalsize(interface::bril::bunchmaskT::maxsize());
        maskHistHeader->setFrequency(integration_period_nb.value_);

        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc(pdict);       
        tc.insert_field( maskHistHeader->payloadanchor, "ncollidingbx", &NActiveBX);
        tc.insert_field( maskHistHeader->payloadanchor, "firstlumibx",  &firstBX);
        tc.insert_field( maskHistHeader->payloadanchor, "maxlumibx",    &maxBX);
        tc.insert_field( maskHistHeader->payloadanchor, "bxmask",       activeBXMask);
        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.find( interface::bril::bunchmaskT::topicname() );
        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this bx mask to output queue");
            it->second->push(bxBufRef); //push agghist to publishing queue
        } else if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref after assigning because topic not in queue's list");
            bxBufRef->release();
            bxBufRef= 0;
        }
        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref in exception");
            bxBufRef->release();
            bxBufRef= 0;
        }
    }
}

void bril::hfprocessor::HFProcessor::ComputeAfterglow(interface::bril::shared::DatumHead& inheader){

    debugStatement<<"Begin afterglow calculation";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str() );
    debugStatement.str("");
    
    //First apply SBR on mu per BX
    //Compute the hf afterglow with corrected mu / uncorrected mu

    memset(muUncorrectedPerBX,   0, sizeof(muUncorrectedPerBX));
    memset(hfafterglowPerBX,     0, sizeof(hfafterglowPerBX));

    afterglowBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfafterglowfracT::maxsize());
    afterglowBufRef->setDataSize(interface::bril::hfafterglowfracT::maxsize());
    
    interface::bril::shared::DatumHead* afterglowHeader = (interface::bril::shared::DatumHead*)(afterglowBufRef->getDataLocation());
    afterglowHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    afterglowHeader->setResource(interface::bril::shared::DataSource::HF, 0, 0, interface::bril::shared::StorageType::FLOAT);
    afterglowHeader->setTotalsize(interface::bril::hfafterglowfracT::maxsize());


    float noise = 0;
    int num_cut = 20;
    float idl = 0;
    
    debugStatement<<"The luminosity before correction is: ";
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        muUncorrectedPerBX[bxid] = muHistPerBX[bxid];
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // modified afterglow model
    int type(0);
    float SBR(0);

    float ettype1[3] = {0, 0, 0};
    float etquadratic_type1[3] = {0, 0, 0};
    float octype1[3] = {0, 0, 0};
    float ocquadratic_type1[3] = {0, 0, 0};

    for(int ibx=0; ibx<interface::bril::shared::MAX_NBX; ibx++){
        if(activeBXMask[ibx]){
            type = 0;        
            for(int jbx=ibx+1; jbx<ibx+interface::bril::shared::MAX_NBX; jbx++){
                
                // Define correction
                if (type < 3) {
                    if (lumiAlgo.value_ == "etsum") {
                        SBR = (muHistPerBX[ibx]*muHistPerBX[ibx] * etquadratic_type1[type]) + (muHistPerBX[ibx] * ettype1[type]) + HFSBR[jbx-ibx];
                    } else {
                       SBR = (muHistPerBX[ibx]*muHistPerBX[ibx] * ocquadratic_type1[type]) + (muHistPerBX[ibx] * octype1[type]) + HFSBR[jbx-ibx]; 
                    }
                } else {
                    SBR = HFSBR[jbx-ibx];
                }

                if(jbx<interface::bril::shared::MAX_NBX) {
                    muHistPerBX[jbx] -= muHistPerBX[ibx] * SBR;
                } else {
                    muHistPerBX[jbx-interface::bril::shared::MAX_NBX] -= muHistPerBX[ibx] * SBR;
                }

                type += 1;
            }
        }
    }

    for(int bxid=0; bxid<num_cut; bxid++){
        noise+=muHistPerBX[bxid];
        idl+=1;
    }

    noise = noise/num_cut;
    
    float totalMuUn=0;
    float totalMuCorr=0;

    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        if(muUncorrectedPerBX[bxid]!=0){
            hfafterglowPerBX[bxid]=muHistPerBX[bxid]/muUncorrectedPerBX[bxid];
            totalMuUn+=muUncorrectedPerBX[bxid];
            totalMuCorr+=muHistPerBX[bxid];
        }else{
            hfafterglowPerBX[bxid]=0;
        }
    }

    if(totalMuUn>0){
        hfAfterGlowTotalScale=totalMuCorr/totalMuUn;
    }

    debugStatement<<"The total correction factor is:  "<<hfAfterGlowTotalScale;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    debugStatement<<"The mus after correction are: ";
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // send off hf afterglow frac
    memcpy(afterglowHeader->payloadanchor, hfafterglowPerBX, (interface::bril::shared::MAX_NBX)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfafterglowfracT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(afterglowBufRef);
    } else if(afterglowBufRef) {
        LOG4CPLUS_INFO(getApplicationLogger(),"release afterglow ref after assigning because topic not in queue's list");
        afterglowBufRef->release();
        afterglowBufRef= 0;
    }

    LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing afterglow hist");


    // apply hf afterglow frac to the rest of the mu's
    for(int boardInd=0; boardInd<36; boardInd++){
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*hfafterglowPerBX[bxid];
        }
    }
}


void bril::hfprocessor::HFProcessor::SubtractPedestal(interface::bril::shared::DatumHead& inheader){
    
    memset(pedestal,     0, sizeof(pedestal));
    
    pedBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtPedestalT::maxsize());
    pedBufRef->setDataSize(interface::bril::hfEtPedestalT::maxsize());
    
    interface::bril::shared::DatumHead* pedHeader = (interface::bril::shared::DatumHead*)(pedBufRef->getDataLocation());
    pedHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    pedHeader->setResource(interface::bril::shared::DataSource::HF, 0, 0, interface::bril::shared::StorageType::FLOAT);
    pedHeader->setTotalsize(interface::bril::hfEtPedestalT::maxsize());

    debugStatement<<"Pedestal: ";

    int nSample=13;
    for (int i = 0; i < 4; i++){
        for (int j = i; j < 4*nSample; j+=4) {
            pedestal[i] +=  muHistPerBX[3500 + j]/nSample;
        }
        debugStatement<<" "<<i<<" "<<pedestal[i];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        debugStatement<<" "<<bxid<<" "<<muHistPerBX[bxid];
        muHistPerBX[bxid]=(muHistPerBX[bxid] - pedestal[bxid % 4]);
        debugStatement<<" "<<pedestal[bxid % 4]<<" "<<muHistPerBX[bxid]<<"\n";
        for(int boardInd=0; boardInd<36; boardInd++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid] - pedestal[bxid % 4];
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // send off hf pedestal
    memcpy(pedHeader->payloadanchor, pedestal, (4)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfEtPedestalT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(pedBufRef);
    } else if(pedBufRef){
        LOG4CPLUS_INFO(getApplicationLogger(),"release pedestal ref after assigning because topic not in queue's list");
        pedBufRef->release();
        pedBufRef=0;
    }
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::hfprocessor::HFProcessor::ClearCache(){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "ClearCache");
    
    memset(boardAvailableThisBlock  ,0,sizeof(boardAvailableThisBlock));
    memset(timeValidHist            ,0,sizeof(timeValidHist          ));
    memset(timeHist                 ,0,sizeof(timeHist            ));
    NBoardsCollected=0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "about to clear hfRawHist");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfRawHist.begin();it!=hfRawHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfRawHist.clear();

    debugStatement<<"about to clear hfValidHist";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfValidHist.begin();it!=hfValidHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfValidHist.clear();
    debugStatement<<"All cleared";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}


bool bril::hfprocessor::HFProcessor::publishing(toolbox::task::WorkLoop* wl){
    QueueStoreIt it;
    for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
        std::string topicname = it->first;
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Is there data for "+topicname);
        if(it->second->empty()) continue;
        toolbox::mem::Reference* data = it->second->pop();
    
        std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topicname);
        for(TopicStoreIt topicit = ret.first; topicit!=ret.second;++topicit){
            if(data){ 
                std::string payloaddict;
                debugStatement<<"Found topic "<<topicname<<" for publishing";
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                if(topicname=="hfOcc1Agg"){
                    payloaddict=interface::bril::hfOcc1AggT::payloaddict();
                }else if(topicname=="hfoclumi"){
                    payloaddict=interface::bril::hfoclumiT::payloaddict();
                }else if(topicname=="hfEtSumAgg"){  
                    payloaddict=interface::bril::hfEtSumAggT::payloaddict();
                }else if(topicname=="hfetlumi"){
                    payloaddict=interface::bril::hfetlumiT::payloaddict();
                }else if(topicname=="bunchmask"){
                    payloaddict=interface::bril::bunchmaskT::payloaddict();
                }else if(topicname=="hfEtPedestal"){
                    payloaddict=interface::bril::hfEtPedestalT::payloaddict();
                }else if(topicname=="hfafterglowfrac"){
                    payloaddict=interface::bril::hfafterglowfracT::payloaddict();
                } 
                else{
                    LOG4CPLUS_ERROR(getApplicationLogger(),"Wrong topic name to publish!");
                }
                do_publish(topicit->second,topicname,payloaddict,data->duplicate());
            }
        }
        if(data) {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Releasing "+topicname+" after publishing");
            data->release();
            data=0;
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Released "+topicname+" after publishing");
        }
    }

    usleep(int(integration_period_secs*1.e6/8.)); //sleep 1/8 of the length of the integration period
    return true;
}

void bril::hfprocessor::HFProcessor::do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef){
    std::stringstream msg;
    try{
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" ){
            plist.setProperty("NOSTORE","1");
        }
        msg << "publish to "<<busname<<" , "<<topicname<<", run " << prevDataHeader.runnum<<" ls "<< prevDataHeader.lsnum <<" nb "<<prevDataHeader.nbnum; 
        LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
        debugStatement<<"Publish "<<topicname<<" to "<<busname;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        this->getEventingBus(busname).publish(topicname,bufRef,plist);
        debugStatement<<"getEventingBus.publish seemed to work";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }catch(xcept::Exception& e){
        msg<<"Failed to publish "<<topicname<<" to "<<busname;    
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){
            bufRef->release();
            bufRef = 0;
        }
        //XCEPT_DECLARE_NESTED(bril::hfprocessor::exception::Exception,myerrorobj,msg.str(),e);
        //this->notifyQualified("fatal",myerrorobj);
    }  
}

// When nibble-histogram receiving times out, processor the received data 
// and/or clear the stale data
void bril::hfprocessor::HFProcessor::timeExpired(toolbox::task::TimerEvent& e){
    //if(prevDataHeader.runnum==0) return;//cache is clean, do nothing
    //toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    //unsigned int nowsec = t.sec();
    //if( (nowsec-lastDataAcquired)>5 ){
    //    std::stringstream ss;
    //    ss<<"Found stale cache. run "<<prevDataHeader.runnum<<" ls "<<prevDataHeader.lsnum;
    //    LOG4CPLUS_DEBUG(getApplicationLogger(), "Full statistics, process the cache ");
    //    do_process(prevDataHeader);
    //    ClearCache();
    //}else{
    //    LOG4CPLUS_INFO(getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
    //    prevDataHeader.runnum = 0;
    //    prevDataHeader.lsnum = 0;
    //    //prevDataHeader.cmslsnum = 0;
    //    prevDataHeader.nbnum = 0;
    //    ClearCache();
    //}
}

void bril::hfprocessor::HFProcessor::setMask() {
    
    for (unsigned int icol = 0; icol < 3564; icol++) {
        if ( m_iscolliding1[icol] && m_iscolliding2[icol] ) {
	    m_machine_mask[icol] = true;
        } else {
            m_machine_mask[icol] = false;
        }
    }
    
    memcpy(activeBXMask, m_machine_mask, sizeof(bool)*interface::bril::shared::MAX_NBX);
}

void bril::hfprocessor::HFProcessor::InitializeHFSBR()
{ 
  float sbr_oc[interface::bril::shared::MAX_NBX] = {1.0000000000, 0.0076063100000000, 0.0022055670000000, 0.0015072753000000, 0.0012536100000000, 0.0011175800000000, 0.0010035800000000, 0.0009061300000000, 0.0008319600000000, 0.0007658400000000, 0.0007090200000000, 0.0006615600000000, 0.0006278200000000, 0.0006049300000000, 0.0005759400000000, 0.0005549800000000, 0.0005292000000000, 0.0005112400000000, 0.0004973700000000, 0.0004805500000000, 0.0004637200000000, 0.0004508700000000, 0.0004393700000000, 0.0004263700000000, 0.0004116200000000, 0.0004032300000000, 0.0003928700000000, 0.0003829100000000, 0.0003719400000000, 0.0003647200000000, 0.0003580300000000, 0.0003494800000000, 0.0003425700000000, 0.0003376400000000, 0.0003344700000000, 0.0003295400000000, 0.0003231500000000, 0.0003207800000000, 0.0003165500000000, 0.0003151700000000, 0.0003139400000000, 0.0003128500000000, 0.0003119000000000, 0.0003110900000000, 0.0003104300000000, 0.0003099000000000, 0.0003095200000000, 0.0003092700000000, 0.0003091500000000, 0.0003091500000000, 0.0003092700000000, 0.0003095000000000, 0.0003098200000000, 0.0003102300000000, 0.0003107200000000, 0.0003112600000000, 0.0003118400000000, 0.0003124500000000, 0.0003130800000000, 0.0003136900000000, 0.0003142800000000, 0.0003148300000000, 0.0003153100000000, 0.0003157100000000, 0.0003160100000000, 0.0003162000000000, 0.0003162400000000, 0.0003161400000000, 0.0003158600000000, 0.0003154000000000, 0.0003147500000000, 0.0003138900000000, 0.0003128200000000, 0.0003115200000000, 0.0003100000000000, 0.0003082400000000, 0.0003062600000000, 0.0003040400000000, 0.0003016000000000, 0.0002989400000000, 0.0002960600000000, 0.0002929800000000, 0.0002897100000000, 0.0002862600000000, 0.0002826400000000, 0.0002788700000000, 0.0002749700000000, 0.0002709500000000, 0.0002668300000000, 0.0002626400000000, 0.0002583800000000, 0.0002540700000000, 0.0002497500000000, 0.0002454100000000, 0.0002410800000000, 0.0002367800000000, 0.0002325200000000, 0.0002283200000000, 0.0002241800000000, 0.0002201200000000, 0.0002161500000000, 0.0002122800000000, 0.0002085200000000, 0.0002048700000000, 0.0002013400000000, 0.0001979300000000, 0.0001946500000000, 0.0001914900000000, 0.0001884600000000, 0.0001855500000000, 0.0001827800000000, 0.0001801200000000, 0.0001775800000000, 0.0001751600000000, 0.0001728600000000, 0.0001706600000000, 0.0001685600000000, 0.0001665700000000, 0.0001646600000000, 0.0001628500000000, 0.0001611200000000, 0.0001594600000000, 0.0001578800000000, 0.0001563700000000, 0.0001549200000000, 0.0001535300000000, 0.0001521900000000, 0.0001509000000000, 0.0001496600000000, 0.0001484600000000, 0.0001473000000000, 0.0001461800000000, 0.0001450900000000, 0.0001440300000000, 0.0001429900000000, 0.0001419900000000, 0.0001410000000000, 0.0001400400000000, 0.0001391000000000, 0.0001381800000000, 0.0001372700000000, 0.0001363800000000, 0.0001355100000000, 0.0001346500000000, 0.0001338000000000, 0.0001321277740000, 0.0001311564520000, 0.0001301557620000, 0.0001292540580000, 0.0001282938660000, 0.0001273425480000, 0.0001263961570000, 0.0001255162570000, 0.0001245793420000, 0.0001236976950000, 0.0001228179370000, 0.0001219877290000, 0.0001211260100000, 0.0001203295950000, 0.0001195779720000, 0.0001187589890000, 0.0001180094240000, 0.0001172456130000, 0.0001165342140000, 0.0001158142710000, 0.0001151787530000, 0.0001145247720000, 0.0001139276930000, 0.0001133378640000, 0.0001128140890000, 0.0001122091370000, 0.0001116670030000, 0.0001111178340000, 0.0001105482090000, 0.0001100502970000, 0.0001095373360000, 0.0001090380330000, 0.0001085042670000, 0.0001080151940000, 0.0001075404960000, 0.0001070520050000, 0.0001065894880000, 0.0001061889360000, 0.0001056943660000, 0.0001053298790000, 0.0001049875870000, 0.0001045574100000, 0.0001041847120000, 0.0001037787120000, 0.0001034299810000, 0.0001030811560000, 0.0001026789030000, 0.0001023719990000, 0.0001019555310000, 0.0001015754780000, 0.0001012614470000, 0.0001008375720000, 0.0001004411590000, 0.0001000797330000, 0.0000997557305000, 0.0000994396943000, 0.0000990944449000, 0.0000987116251000, 0.0000984017254000, 0.0000980066762000, 0.0000976936673000, 0.0000973815153000, 0.0000970389798000, 0.0000967398968000, 0.0000964048565000, 0.0000961115790000, 0.0000957288523000, 0.0000954401684000, 0.0000951400969000, 0.0000948870545000, 0.0000946180622000, 0.0000943371891000, 0.0000940620854000, 0.0000937956979000, 0.0000935284033000, 0.0000932575566000, 0.0000930074981000, 0.0000927360947000, 0.0000925108602000, 0.0000922261920000, 0.0000919490204000, 0.0000916768407000, 0.0000914060471000, 0.0000911801362000, 0.0000909624093000, 0.0000906817411000, 0.0000904173072000, 0.0000901957911000, 0.0000899595897000, 0.0000897254177000, 0.0000894808576000, 0.0000892784864000, 0.0000891103334000, 0.0000888759539000, 0.0000886835992000, 0.0000884999171000, 0.0000882943532000, 0.0000880878295000, 0.0000879347127000, 0.0000876907439000, 0.0000874999576000, 0.0000873491860000, 0.0000871983309000, 0.0000869439860000, 0.0000867256615000, 0.0000865489614000, 0.0000863316101000, 0.0000861433573000, 0.0000859589819000, 0.0000858040210000, 0.0000856095400000, 0.0000854515505000, 0.0000852500587000, 0.0000850671858000, 0.0000849117381000, 0.0000848301981000, 0.0000846791155000, 0.0000845760163000, 0.0000844537792000, 0.0000842736219000, 0.0000841162639000, 0.0000839588698000, 0.0000838156502000, 0.0000836928220000, 0.0000835687702000, 0.0000834218868000, 0.0000832437034000, 0.0000830555699000, 0.0000828820029000, 0.0000826995392000, 0.0000825432357000, 0.0000823774556000, 0.0000822518699000, 0.0000820919506000, 0.0000819112601000, 0.0000817382163000, 0.0000816290995000, 0.0000815205012000, 0.0000814030041000, 0.0000812860595000, 0.0000811928946000, 0.0000810252792000, 0.0000808598785000, 0.0000807172162000, 0.0000805749754000, 0.0000804692099000, 0.0000803574174000, 0.0000802201520000, 0.0000800282768000, 0.0000798642982000, 0.0000797243040000, 0.0000795738178000, 0.0000794150842000, 0.0000793024370000, 0.0000791883806000, 0.0000790911030000, 0.0000789445748000, 0.0000788030200000, 0.0000786822902000, 0.0000785507525000, 0.0000784402870000, 0.0000782999978000, 0.0000781914834000, 0.0000780390510000, 0.0000779054388000, 0.0000777736538000, 0.0000776594189000, 0.0000775322649000, 0.0000774165330000, 0.0000772672216000, 0.0000770979559000, 0.0000769988478000, 0.0000768693163000, 0.0000767662597000, 0.0000766458261000, 0.0000765252294000, 0.0000763881924000, 0.0000762587214000, 0.0000761147198000, 0.0000759648619000, 0.0000758188403000, 0.0000757124052000, 0.0000756244051000, 0.0000755170953000, 0.0000753897724000, 0.0000752515314000, 0.0000750889687000, 0.0000749853305000, 0.0000748970021000, 0.0000747974363000, 0.0000746898885000, 0.0000745921064000, 0.0000744698893000, 0.0000743401103000, 0.0000742442600000, 0.0000741171023000, 0.0000739813447000, 0.0000738630969000, 0.0000737588527000, 0.0000736684356000, 0.0000735450249000, 0.0000734431092000, 0.0000733313270000, 0.0000732011868000, 0.0000730967978000, 0.0000730113436000, 0.0000728735409000, 0.0000727634783000, 0.0000726775762000, 0.0000725603332000, 0.0000724916870000, 0.0000723552028000, 0.0000722318988000, 0.0000720977558000, 0.0000720332960000, 0.0000719527907000, 0.0000718479319000, 0.0000717461333000, 0.0000716558349000, 0.0000715515219000, 0.0000714686960000, 0.0000714231635000, 0.0000712884443000, 0.0000712038270000, 0.0000711091677000, 0.0000710231380000, 0.0000709025393000, 0.0000707877160000, 0.0000707057439000, 0.0000705965806000, 0.0000705041369000, 0.0000704141706000, 0.0000703349907000, 0.0000702051129000, 0.0000701145354000, 0.0000699950885000, 0.0000699287911000, 0.0000697905432000, 0.0000696702392000, 0.0000695797626000, 0.0000694576194000, 0.0000694046796000, 0.0000692859724000, 0.0000691941697000, 0.0000690511238000, 0.0000689937720000, 0.0000688807073000, 0.0000688421907000, 0.0000687340358000, 0.0000686846623000, 0.0000686106676000, 0.0000685043413000, 0.0000684188690000, 0.0000683282351000, 0.0000682863110000, 0.0000682115718000, 0.0000681406934000, 0.0000680210397000, 0.0000679536249000, 0.0000678072125000, 0.0000677280060000, 0.0000676519968000, 0.0000675488165000, 0.0000674724422000, 0.0000674004592000, 0.0000672784992000, 0.0000671461700000, 0.0000670273143000, 0.0000669209704000, 0.0000668611178000, 0.0000668045971000, 0.0000667755780000, 0.0000666673428000, 0.0000666057380000, 0.0000665431654000, 0.0000664849980000, 0.0000664081293000, 0.0000663327442000, 0.0000662914637000, 0.0000662388301000, 0.0000661809680000, 0.0000660723651000, 0.0000659537457000, 0.0000658519729000, 0.0000657925245000, 0.0000656591451000, 0.0000655714758000, 0.0000654912557000, 0.0000654039834000, 0.0000653392352000, 0.0000652388990000, 0.0000651544960000, 0.0000650624678000, 0.0000649833516000, 0.0000649207384000, 0.0000648260554000, 0.0000647357270000, 0.0000646975305000, 0.0000645999992000, 0.0000645213476000, 0.0000644429312000, 0.0000643630972000, 0.0000642671049000, 0.0000642215955000, 0.0000641172587000, 0.0000640616689000, 0.0000639899890000, 0.0000639500892000, 0.0000638841084000, 0.0000638052391000, 0.0000637464832000, 0.0000636514691000, 0.0000635994731000, 0.0000635221841000, 0.0000634910418000, 0.0000633574853000, 0.0000632965984000, 0.0000631565189000, 0.0000630512248000, 0.0000629605218000, 0.0000628888328000, 0.0000627796606000, 0.0000626907634000, 0.0000625889206000, 0.0000624965675000, 0.0000623977557000, 0.0000622819411000, 0.0000622041778000, 0.0000621436653000, 0.0000621001823000, 0.0000620300793000, 0.0000619443500000, 0.0000618763517000, 0.0000618414473000, 0.0000617691987000, 0.0000617524119000, 0.0000616731041000, 0.0000616208195000, 0.0000615408213000, 0.0000614919333000, 0.0000614256765000, 0.0000613690041000, 0.0000612823103000, 0.0000611733056000, 0.0000610724734000, 0.0000609883925000, 0.0000609277608000, 0.0000608110329000, 0.0000606896260000, 0.0000606004931000, 0.0000605611390000, 0.0000604497236000, 0.0000603559774000, 0.0000602868782000, 0.0000602114575000, 0.0000597714822608, 0.0000597072909977, 0.0000596431672270, 0.0000595791108793, 0.0000595151218852, 0.0000594512001754, 0.0000593873456810, 0.0000593235583326, 0.0000592598380614, 0.0000591961847983, 0.0000591325984744, 0.0000590690790210, 0.0000590056263692, 0.0000589422404504, 0.0000588789211960, 0.0000588156685374, 0.0000587524824062, 0.0000586893627338, 0.0000586263094521, 0.0000585633224927, 0.0000585004017874, 0.0000584375472681, 0.0000583747588668, 0.0000583120365153, 0.0000582493801458, 0.0000581867896904, 0.0000581242650814, 0.0000580618062510, 0.0000579994131315, 0.0000579370856553, 0.0000578748237550, 0.0000578126273631, 0.0000577504964122, 0.0000576884308349, 0.0000576264305641, 0.0000575644955325, 0.0000575026256730, 0.0000574408209186, 0.0000573790812023, 0.0000573174064571, 0.0000572557966163, 0.0000571942516130, 0.0000571327713806, 0.0000570713558523, 0.0000570100049617, 0.0000569487186422, 0.0000568874968273, 0.0000568263394507, 0.0000567652464460, 0.0000567042177471, 0.0000566432532876, 0.0000565823530017, 0.0000565215168231, 0.0000564607446858, 0.0000564000365241, 0.0000563393922720, 0.0000562788118638, 0.0000562182952337, 0.0000561578423161, 0.0000560974530455, 0.0000560371273562, 0.0000559768651828, 0.0000559166664600, 0.0000558565311225, 0.0000557964591049, 0.0000557364503422, 0.0000556765047691, 0.0000556166223206, 0.0000555568029317, 0.0000554970465375, 0.0000554373530732, 0.0000553777224739, 0.0000553181546749, 0.0000552586496116, 0.0000551992072193, 0.0000551398274335, 0.0000550805101897, 0.0000550212554235, 0.0000549620630707, 0.0000549029330668, 0.0000548438653478, 0.0000547848598493, 0.0000547259165075, 0.0000546670352582, 0.0000546082160375, 0.0000545494587814, 0.0000544907634263, 0.0000544321299082, 0.0000543735581636, 0.0000543150481288, 0.0000542565997401, 0.0000541982129342, 0.0000541398876475, 0.0000540816238167, 0.0000540234213785, 0.0000539652802696, 0.0000539072004268, 0.0000538491817871, 0.0000537912242872, 0.0000537333278644, 0.0000536754924555, 0.0000536177179978, 0.0000535600044285, 0.0000535023516847, 0.0000534447597039, 0.0000533872284233, 0.0000533297577805, 0.0000532723477130, 0.0000532149981583, 0.0000531577090540, 0.0000531004803379, 0.0000530433119477, 0.0000529862038213, 0.0000529291558964, 0.0000528721681112, 0.0000528152404035, 0.0000527583727115, 0.0000527015649733, 0.0000526448171270, 0.0000525881291110, 0.0000525315008635, 0.0000524749323230, 0.0000524184234279, 0.0000523619741167, 0.0000523055843280, 0.0000522492540003, 0.0000521929830725, 0.0000521367714832, 0.0000520806191712, 0.0000520245260755, 0.0000519684921350, 0.0000519125172887, 0.0000518566014756, 0.0000518007446348, 0.0000517449467056, 0.0000516892076272, 0.0000516335273389, 0.0000515779057801, 0.0000515223428902, 0.0000514668386086, 0.0000514113928750, 0.0000513560056290, 0.0000513006768101, 0.0000512454063583, 0.0000511901942131, 0.0000511350403146, 0.0000510799446026, 0.0000510249070171, 0.0000509699274982, 0.0000509150059858, 0.0000508601424203, 0.0000508053367418, 0.0000507505888905, 0.0000506958988069, 0.0000506412664313, 0.0000505866917041, 0.0000505321745660, 0.0000504777149574, 0.0000504233128190, 0.0000503689680916, 0.0000503146807158, 0.0000502604506324, 0.0000502062777825, 0.0000501521621068, 0.0000500981035464, 0.0000500441020424, 0.0000499901575358, 0.0000499362699678, 0.0000498824392797, 0.0000498286654128, 0.0000497749483084, 0.0000497212879079, 0.0000496676841527, 0.0000496141369845, 0.0000495606463448, 0.0000495072121752, 0.0000494538344175, 0.0000494005130134, 0.0000493472479048, 0.0000492940390335, 0.0000492408863414, 0.0000491877897706, 0.0000491347492631, 0.0000490817647610, 0.0000490288362065, 0.0000489759635419, 0.0000489231467094, 0.0000488703856514, 0.0000488176803102, 0.0000487650306284, 0.0000487124365485, 0.0000486598980130, 0.0000486074149646, 0.0000485549873459, 0.0000485026150999, 0.0000484502981691, 0.0000483980364966, 0.0000483458300252, 0.0000482936786979, 0.0000482415824578, 0.0000481895412480, 0.0000481375550116, 0.0000480856236918, 0.0000480337472320, 0.0000479819255753, 0.0000479301586653, 0.0000478784464454, 0.0000478267888590, 0.0000477751858498, 0.0000477236373613, 0.0000476721433372, 0.0000476207037212, 0.0000475693184572, 0.0000475179874889, 0.0000474667107602, 0.0000474154882152, 0.0000473643197978, 0.0000473132054521, 0.0000472621451222, 0.0000472111387523, 0.0000471601862866, 0.0000471092876694, 0.0000470584428451, 0.0000470076517581, 0.0000469569143528, 0.0000469062305738, 0.0000468556003656, 0.0000468050236728, 0.0000467545004402, 0.0000467040306125, 0.0000466536141344, 0.0000466032509509, 0.0000465529410068, 0.0000465026842471, 0.0000464524806168, 0.0000464023300610, 0.0000463522325249, 0.0000463021879535, 0.0000462521962922, 0.0000462022574863, 0.0000461523714810, 0.0000461025382218, 0.0000460527576542, 0.0000460030297237, 0.0000459533543758, 0.0000459037315562, 0.0000458541612106, 0.0000458046432847, 0.0000457551777242, 0.0000457057644751, 0.0000456564034832, 0.0000456070946945, 0.0000455578380550, 0.0000455086335107, 0.0000454594810079, 0.0000454103804925, 0.0000453613319110, 0.0000453123352095, 0.0000452633903344, 0.0000452144972321, 0.0000451656558491, 0.0000451168661317, 0.0000450681280267, 0.0000450194414805, 0.0000449708064399, 0.0000449222228516, 0.0000448736906622, 0.0000448252098188, 0.0000447767802680, 0.0000447284019569, 0.0000446800748325, 0.0000446317988417, 0.0000445835739317, 0.0000445354000496, 0.0000444872771426, 0.0000444392051579, 0.0000443911840430, 0.0000443432137450, 0.0000442952942115, 0.0000442474253898, 0.0000441996072276, 0.0000441518396723, 0.0000441041226716, 0.0000440564561732, 0.0000440088401248, 0.0000439612744741, 0.0000439137591691, 0.0000438662941576, 0.0000438188793875, 0.0000437715148069, 0.0000437242003637, 0.0000436769360061, 0.0000436297216822, 0.0000435825573402, 0.0000435354429284, 0.0000434883783950, 0.0000434413636886, 0.0000433943987573, 0.0000433474835498, 0.0000433006180145, 0.0000432538021000, 0.0000432070357549, 0.0000431603189279, 0.0000431136515677, 0.0000430670336230, 0.0000430204650428, 0.0000429739457759, 0.0000429274757711, 0.0000428810549776, 0.0000428346833443, 0.0000427883608203, 0.0000427420873548, 0.0000426958628969, 0.0000426496873959, 0.0000426035608010, 0.0000425574830617, 0.0000425114541272, 0.0000424654739471, 0.0000424195424708, 0.0000423736596480, 0.0000423278254281, 0.0000422820397608, 0.0000422363025959, 0.0000421906138830, 0.0000421449735721, 0.0000420993816128, 0.0000420538379552, 0.0000420083425492, 0.0000419628953448, 0.0000419174962920, 0.0000418721453410, 0.0000418268424419, 0.0000417815875449, 0.0000417363806002, 0.0000416912215583, 0.0000416461103693, 0.0000416010469839, 0.0000415560313523, 0.0000415110634251, 0.0000414661431528, 0.0000414212704862, 0.0000413764453758, 0.0000413316677723, 0.0000412869376265, 0.0000412422548892, 0.0000411976195112, 0.0000411530314435, 0.0000411084906370, 0.0000410639970428, 0.0000410195506118, 0.0000409751512952, 0.0000409307990442, 0.0000408864938099, 0.0000408422355436, 0.0000407980241966, 0.0000407538597202, 0.0000407097420659, 0.0000406656711851, 0.0000406216470293, 0.0000405776695501, 0.0000405337386990, 0.0000404898544277, 0.0000404460166879, 0.0000404022254314, 0.0000403584806098, 0.0000403147821752, 0.0000402711300793, 0.0000402275242741, 0.0000401839647117, 0.0000401404513439, 0.0000400969841230, 0.0000400535630010, 0.0000400101879302, 0.0000399668588627, 0.0000399235757509, 0.0000398803385470, 0.0000398371472035, 0.0000397940016728, 0.0000397509019073, 0.0000397078478596, 0.0000396648394822, 0.0000396218767278, 0.0000395789595490, 0.0000395360878986, 0.0000394932617292, 0.0000394504809938, 0.0000394077456451, 0.0000393650556360, 0.0000393224109196, 0.0000392798114488, 0.0000392372571767, 0.0000391947480563, 0.0000391522840408, 0.0000391098650835, 0.0000390674911374, 0.0000390251621560, 0.0000389828780925, 0.0000389406389003, 0.0000388984445329, 0.0000388562949438, 0.0000388141900863, 0.0000387721299142, 0.0000387301143811, 0.0000386881434405, 0.0000386462170462, 0.0000386043351520, 0.0000385624977116, 0.0000385207046790, 0.0000384789560080, 0.0000384372516525, 0.0000383955915666, 0.0000383539757042, 0.0000383124040195, 0.0000382708764666, 0.0000382293929997, 0.0000381879535730, 0.0000381465581407, 0.0000381052066573, 0.0000380638990769, 0.0000380226353541, 0.0000379814154434, 0.0000379402392991, 0.0000378991068759, 0.0000378580181283, 0.0000378169730110, 0.0000377759714787, 0.0000377350134861, 0.0000376940989879, 0.0000376532279391, 0.0000376124002944, 0.0000375716160088, 0.0000375308750373, 0.0000374901773348, 0.0000374495228564, 0.0000374089115572, 0.0000373683433924, 0.0000373278183171, 0.0000372873362865, 0.0000372468972560, 0.0000372065011809, 0.0000371661480165, 0.0000371258377182, 0.0000370855702416, 0.0000370453455421, 0.0000370051635753, 0.0000369650242967, 0.0000369249276620, 0.0000368848736269, 0.0000368448621472, 0.0000368048931785, 0.0000367649666768, 0.0000367250825979, 0.0000366852408976, 0.0000366454415320, 0.0000366056844570, 0.0000365659696288, 0.0000365262970033, 0.0000364866665368, 0.0000364470781853, 0.0000364075319052, 0.0000363680276526, 0.0000363285653840, 0.0000362891450556, 0.0000362497666238, 0.0000362104300452, 0.0000361711352761, 0.0000361318822732, 0.0000360926709929, 0.0000360535013920, 0.0000360143734270, 0.0000359752870548, 0.0000359362422320, 0.0000358972389154, 0.0000358582770619, 0.0000358193566284, 0.0000357804775718, 0.0000357416398490, 0.0000357028434171, 0.0000356640882332, 0.0000356253742543, 0.0000355867014376, 0.0000355480697403, 0.0000355094791197, 0.0000354709295329, 0.0000354324209373, 0.0000353939532903, 0.0000353555265493, 0.0000353171406717, 0.0000352787956151, 0.0000352404913369, 0.0000352022277948, 0.0000351640049463, 0.0000351258227492, 0.0000350876811611, 0.0000350495801399, 0.0000350115196432, 0.0000349734996290, 0.0000349355200550, 0.0000348975808793, 0.0000348596820599, 0.0000348218235546, 0.0000347840053216, 0.0000347462273189, 0.0000347084895048, 0.0000346707918373, 0.0000346331342747, 0.0000345955167753, 0.0000345579392973, 0.0000345204017992, 0.0000344829042393, 0.0000344454465760, 0.0000344080287679, 0.0000343706507733, 0.0000343333125510, 0.0000342960140595, 0.0000342587552574, 0.0000342215361035, 0.0000341843565564, 0.0000341472165750, 0.0000341101161180, 0.0000340730551443, 0.0000340360336128, 0.0000339990514825, 0.0000339621087123, 0.0000339252052612, 0.0000338883410883, 0.0000338515161527, 0.0000338147304135, 0.0000337779838300, 0.0000337412763614, 0.0000337046079669, 0.0000336679786059, 0.0000336313882377, 0.0000335948368217, 0.0000335583243173, 0.0000335218506840, 0.0000334854158814, 0.0000334490198690, 0.0000334126626064, 0.0000333763440532, 0.0000333400641691, 0.0000333038229139, 0.0000332676202473, 0.0000332314561292, 0.0000331953305193, 0.0000331592433775, 0.0000331231946638, 0.0000330871843382, 0.0000330512123607, 0.0000330152786913, 0.0000329793832901, 0.0000329435261172, 0.0000329077071328, 0.0000328719262972, 0.0000328361835706, 0.0000328004789132, 0.0000327648122854, 0.0000327291836477, 0.0000326935929603, 0.0000326580401838, 0.0000326225252786, 0.0000325870482054, 0.0000325516089246, 0.0000325162073969, 0.0000324808435829, 0.0000324455174433, 0.0000324102289390, 0.0000323749780306, 0.0000323397646789, 0.0000323045888449, 0.0000322694504894, 0.0000322343495733, 0.0000321992860577, 0.0000321642599035, 0.0000321292710719, 0.0000320943195238, 0.0000320594052205, 0.0000320245281230, 0.0000319896881927, 0.0000319548853908, 0.0000319201196785, 0.0000318853910173, 0.0000318506993684, 0.0000318160446932, 0.0000317814269534, 0.0000317468461102, 0.0000317123021253, 0.0000316777949602, 0.0000316433245765, 0.0000316088909359, 0.0000315744940000, 0.0000315401337307, 0.0000315058100895, 0.0000314715230385, 0.0000314372725393, 0.0000314030585538, 0.0000313688810441, 0.0000313347399720, 0.0000313006352995, 0.0000312665669887, 0.0000312325350016, 0.0000311985393004, 0.0000311645798473, 0.0000311306566043, 0.0000310967695337, 0.0000310629185978, 0.0000310291037590, 0.0000309953249795, 0.0000309615822216, 0.0000309278754480, 0.0000308942046209, 0.0000308605697030, 0.0000308269706566, 0.0000307934074445, 0.0000307598800292, 0.0000307263883734, 0.0000306929324397, 0.0000306595121909, 0.0000306261275897, 0.0000305927785990, 0.0000305594651815, 0.0000305261873003, 0.0000304929449181, 0.0000304597379979, 0.0000304265665027, 0.0000303934303956, 0.0000303603296396, 0.0000303272641978, 0.0000302942340334, 0.0000302612391095, 0.0000302282793893, 0.0000301953548362, 0.0000301624654134, 0.0000301296110841, 0.0000300967918119, 0.0000300640075601, 0.0000300312582921, 0.0000299985439714, 0.0000299658645616, 0.0000299332200261, 0.0000299006103286, 0.0000298680354327, 0.0000298354953021, 0.0000298029899004, 0.0000297705191914, 0.0000297380831389, 0.0000297056817067, 0.0000296733148586, 0.0000296409825586, 0.0000296086847705, 0.0000295764214583, 0.0000295441925860, 0.0000295119981176, 0.0000294798380172, 0.0000294477122490, 0.0000294156207770, 0.0000293835635654, 0.0000293515405785, 0.0000293195517805, 0.0000292875971356, 0.0000292556766083, 0.0000292237901629, 0.0000291919377637, 0.0000291601193752, 0.0000291283349620, 0.0000290965844884, 0.0000290648679190, 0.0000290331852184, 0.0000290015363513, 0.0000289699212822, 0.0000289383399759, 0.0000289067923971, 0.0000288752785106, 0.0000288437982811, 0.0000288123516734, 0.0000287809386525, 0.0000287495591833, 0.0000287182132306, 0.0000286869007595, 0.0000286556217350, 0.0000286243761221, 0.0000285931638859, 0.0000285619849915, 0.0000285308394041, 0.0000284997270888, 0.0000284686480109, 0.0000284376021357, 0.0000284065894284, 0.0000283756098544, 0.0000283446633790, 0.0000283137499677, 0.0000282828695858, 0.0000282520221989, 0.0000282212077725, 0.0000281904262720, 0.0000281596776632, 0.0000281289619115, 0.0000280982789827, 0.0000280676288424, 0.0000280370114563, 0.0000280064267902, 0.0000279758748100, 0.0000279453554813, 0.0000279148687701, 0.0000278844146422, 0.0000278539930637, 0.0000278236040004, 0.0000277932474183, 0.0000277629232835, 0.0000277326315621, 0.0000277023722201, 0.0000276721452237, 0.0000276419505391, 0.0000276117881324, 0.0000275816579699, 0.0000275515600180, 0.0000275214942427, 0.0000274914606107, 0.0000274614590881, 0.0000274314896415, 0.0000274015522372, 0.0000273716468418, 0.0000273417734217, 0.0000273119319435, 0.0000272821223739, 0.0000272523446794, 0.0000272225988266, 0.0000271928847823, 0.0000271632025131, 0.0000271335519859, 0.0000271039331674, 0.0000270743460244, 0.0000270447905238, 0.0000270152666325, 0.0000269857743174, 0.0000269563135454, 0.0000269268842837, 0.0000268974864990, 0.0000268681201587, 0.0000268387852296, 0.0000268094816790, 0.0000267802094741, 0.0000267509685819, 0.0000267217589697, 0.0000266925806049, 0.0000266634334546, 0.0000266343174863, 0.0000266052326672, 0.0000265761789648, 0.0000265471563465, 0.0000265181647798, 0.0000264892042321, 0.0000264602746710, 0.0000264313760640, 0.0000264025083788, 0.0000263736715829, 0.0000263448656440, 0.0000263160905298, 0.0000262873462081, 0.0000262586326466, 0.0000262299498130, 0.0000262012976753, 0.0000261726762013, 0.0000261440853587, 0.0000261155251157, 0.0000260869954402, 0.0000260584963000, 0.0000260300276633, 0.0000260015894981, 0.0000259731817725, 0.0000259448044546, 0.0000259164575126, 0.0000258881409146, 0.0000258598546289, 0.0000258315986237, 0.0000258033728673, 0.0000257751773280, 0.0000257470119742, 0.0000257188767742, 0.0000256907716965, 0.0000256626967095, 0.0000256346517817, 0.0000256066368815, 0.0000255786519776, 0.0000255506970384, 0.0000255227720327, 0.0000254948769290, 0.0000254670116961, 0.0000254391763025, 0.0000254113707171, 0.0000253835949086, 0.0000253558488458, 0.0000253281324976, 0.0000253004458328, 0.0000252727888203, 0.0000252451614290, 0.0000252175636279, 0.0000251899953859, 0.0000251624566722, 0.0000251349474557, 0.0000251074677055, 0.0000250800173908, 0.0000250525964806, 0.0000250252049443, 0.0000249978427509, 0.0000249705098698, 0.0000249432062701, 0.0000249159319213, 0.0000248886867926, 0.0000248614708534, 0.0000248342840731, 0.0000248071264212, 0.0000247799978671, 0.0000247528983802, 0.0000247258279302, 0.0000246987864866, 0.0000246717740189, 0.0000246447904968, 0.0000246178358900, 0.0000245909101681, 0.0000245640133008, 0.0000245371452578, 0.0000245103060090, 0.0000244834955242, 0.0000244567137731, 0.0000244299607257, 0.0000244032363518, 0.0000243765406214, 0.0000243498735044, 0.0000243232349709, 0.0000242966249908, 0.0000242700435342, 0.0000242434905711, 0.0000242169660718, 0.0000241904700063, 0.0000241640023448, 0.0000241375630575, 0.0000241111521146, 0.0000240847694865, 0.0000240584151434, 0.0000240320890556, 0.0000240057911935, 0.0000239795215274, 0.0000239532800279, 0.0000239270666653, 0.0000239008814101, 0.0000238747242329, 0.0000238485951041, 0.0000238224939944, 0.0000237964208743, 0.0000237703757144, 0.0000237443584855, 0.0000237183691582, 0.0000236924077033, 0.0000236664740914, 0.0000236405682934, 0.0000236146902800, 0.0000235888400222, 0.0000235630174907, 0.0000235372226566, 0.0000235114554906, 0.0000234857159638, 0.0000234600040471, 0.0000234343197117, 0.0000234086629284, 0.0000233830336685, 0.0000233574319029, 0.0000233318576029, 0.0000233063107397, 0.0000232807912843, 0.0000232552992080, 0.0000232298344822, 0.0000232043970780, 0.0000231789869668, 0.0000231536041199, 0.0000231282485088, 0.0000231029201047, 0.0000230776188792, 0.0000230523448037, 0.0000230270978497, 0.0000230018779886, 0.0000229766851922, 0.0000229515194319, 0.0000229263806793, 0.0000229012689061, 0.0000228761840839, 0.0000228511261845, 0.0000228260951796, 0.0000228010910409, 0.0000227761137401, 0.0000227511632492, 0.0000227262395399, 0.0000227013425842, 0.0000226764723538, 0.0000226516288208, 0.0000226268119571, 0.0000226020217347, 0.0000225772581255, 0.0000225525211017, 0.0000225278106353, 0.0000225031266984, 0.0000224784692631, 0.0000224538383016, 0.0000224292337861, 0.0000224046556887, 0.0000223801039818, 0.0000223555786376, 0.0000223310796283, 0.0000223066069264, 0.0000222821605041, 0.0000222577403339, 0.0000222333463882, 0.0000222089786394, 0.0000221846370600, 0.0000221603216224, 0.0000221360322992, 0.0000221117690630, 0.0000220875318863, 0.0000220633207418, 0.0000220391356021, 0.0000220149764398, 0.0000219908432276, 0.0000219667359383, 0.0000219426545446, 0.0000219185990194, 0.0000218945693353, 0.0000218705654652, 0.0000218465873821, 0.0000218226350587, 0.0000217987084681, 0.0000217748075830, 0.0000217509323767, 0.0000217270828219, 0.0000217032588918, 0.0000216794605594, 0.0000216556877978, 0.0000216319405800, 0.0000216082188794, 0.0000215845226689, 0.0000215608519219, 0.0000215372066115, 0.0000215135867109, 0.0000214899921935, 0.0000214664230326, 0.0000214428792015, 0.0000214193606735, 0.0000213958674221, 0.0000213723994206, 0.0000213489566425, 0.0000213255390612, 0.0000213021466504, 0.0000212787793834, 0.0000212554372338, 0.0000212321201753, 0.0000212088281813, 0.0000211855612256, 0.0000211623192819, 0.0000211391023237, 0.0000211159103248, 0.0000210927432590, 0.0000210696011000, 0.0000210464838216, 0.0000210233913976, 0.0000210003238019, 0.0000209772810084, 0.0000209542629909, 0.0000209312697234, 0.0000209083011798, 0.0000208853573342, 0.0000208624381605, 0.0000208395436327, 0.0000208166737250, 0.0000207938284114, 0.0000207710076661, 0.0000207482114631, 0.0000207254397767, 0.0000207026925810, 0.0000206799698503, 0.0000206572715588, 0.0000206345976807, 0.0000206119481905, 0.0000205893230624, 0.0000205667222708, 0.0000205441457900, 0.0000205215935945, 0.0000204990656586, 0.0000204765619570, 0.0000204540824639, 0.0000204316271540, 0.0000204091960019, 0.0000203867889819, 0.0000203644060688, 0.0000203420472372, 0.0000203197124617, 0.0000202974017170, 0.0000202751149777, 0.0000202528522186, 0.0000202306134145, 0.0000202083985401, 0.0000201862075702, 0.0000201640404797, 0.0000201418972433, 0.0000201197778360, 0.0000200976822327, 0.0000200756104082, 0.0000200535623377, 0.0000200315379959, 0.0000200095373581, 0.0000199875603990, 0.0000199656070939, 0.0000199436774179, 0.0000199217713459, 0.0000198998888532, 0.0000198780299150, 0.0000198561945063, 0.0000198343826024, 0.0000198125941786, 0.0000197908292102, 0.0000197690876723, 0.0000197473695403, 0.0000197256747896, 0.0000197040033955, 0.0000196823553334, 0.0000196607305787, 0.0000196391291069, 0.0000196175508934, 0.0000195959959138, 0.0000195744641435, 0.0000195529555580, 0.0000195314701330, 0.0000195100078440, 0.0000194885686667, 0.0000194671525767, 0.0000194457595496, 0.0000194243895612, 0.0000194030425871, 0.0000193817186032, 0.0000193604175851, 0.0000193391395087, 0.0000193178843497, 0.0000192966520841, 0.0000192754426877, 0.0000192542561364, 0.0000192330924061, 0.0000192119514727, 0.0000191908333122, 0.0000191697379007, 0.0000191486652140, 0.0000191276152283, 0.0000191065879196, 0.0000190855832640, 0.0000190646012376, 0.0000190436418166, 0.0000190227049772, 0.0000190017906954, 0.0000189808989476, 0.0000189600297099, 0.0000189391829586, 0.0000189183586701, 0.0000188975568205, 0.0000188767773864, 0.0000188560203439, 0.0000188352856695, 0.0000188145733397, 0.0000187938833307, 0.0000187732156192, 0.0000187525701815, 0.0000187319469942, 0.0000187113460339, 0.0000186907672769, 0.0000186702107001, 0.0000186496762798, 0.0000186291639929, 0.0000186086738158, 0.0000185882057253, 0.0000185677596981, 0.0000185473357110, 0.0000185269337406, 0.0000185065537637, 0.0000184861957571, 0.0000184658596977, 0.0000184455455622, 0.0000184252533276, 0.0000184049829708, 0.0000183847344686, 0.0000183645077979, 0.0000183443029358, 0.0000183241198593, 0.0000183039585453, 0.0000182838189708, 0.0000182637011130, 0.0000182436049489, 0.0000182235304556, 0.0000182034776103, 0.0000181834463900, 0.0000181634367720, 0.0000181434487335, 0.0000181234822516, 0.0000181035373037, 0.0000180836138670, 0.0000180637119187, 0.0000180438314363, 0.0000180239723970, 0.0000180041347782, 0.0000179843185572, 0.0000179645237116, 0.0000179447502187, 0.0000179249980560, 0.0000179052672010, 0.0000178855576311, 0.0000178658693239, 0.0000178462022569, 0.0000178265564077, 0.0000178069317539, 0.0000177873282732, 0.0000177677459431, 0.0000177481847413, 0.0000177286446456, 0.0000177091256335, 0.0000176896276829, 0.0000176701507716, 0.0000176506948772, 0.0000176312599775, 0.0000176118460505, 0.0000175924530740, 0.0000175730810258, 0.0000175537298838, 0.0000175343996259, 0.0000175150902301, 0.0000174958016744, 0.0000174765339367, 0.0000174572869950, 0.0000174380608274, 0.0000174188554119, 0.0000173996707265, 0.0000173805067495, 0.0000173613634588, 0.0000173422408327, 0.0000173231388494, 0.0000173040574869, 0.0000172849967235, 0.0000172659565375, 0.0000172469369071, 0.0000172279378105, 0.0000172089592262, 0.0000171900011323, 0.0000171710635073, 0.0000171521463295, 0.0000171332495773, 0.0000171143732291, 0.0000170955172634, 0.0000170766816586, 0.0000170578663931, 0.0000170390714456, 0.0000170202967945, 0.0000170015424183, 0.0000169828082957, 0.0000169640944051, 0.0000169454007253, 0.0000169267272349, 0.0000169080739125, 0.0000168894407368, 0.0000168708276865, 0.0000168522347403, 0.0000168336618769, 0.0000168151090752, 0.0000167965763140, 0.0000167780635719, 0.0000167595708279, 0.0000167410980609, 0.0000167226452496, 0.0000167042123730, 0.0000166857994100, 0.0000166674063396, 0.0000166490331407, 0.0000166306797923, 0.0000166123462734, 0.0000165940325630, 0.0000165757386402, 0.0000165574644841, 0.0000165392100737, 0.0000165209753882, 0.0000165027604067, 0.0000164845651083, 0.0000164663894723, 0.0000164482334778, 0.0000164300971041, 0.0000164119803304, 0.0000163938831360, 0.0000163758055001, 0.0000163577474021, 0.0000163397088213, 0.0000163216897370, 0.0000163036901286, 0.0000162857099756, 0.0000162677492573, 0.0000162498079532, 0.0000162318860427, 0.0000162139835053, 0.0000161961003205, 0.0000161782364678, 0.0000161603919268, 0.0000161425666770, 0.0000161247606980, 0.0000161069739695, 0.0000160892064710, 0.0000160714581822, 0.0000160537290828, 0.0000160360191524, 0.0000160183283708, 0.0000160006567177, 0.0000159830041729, 0.0000159653707161, 0.0000159477563271, 0.0000159301609857, 0.0000159125846719, 0.0000158950273653, 0.0000158774890460, 0.0000158599696938, 0.0000158424692886, 0.0000158249878104, 0.0000158075252391, 0.0000157900815548, 0.0000157726567373, 0.0000157552507669, 0.0000157378636234, 0.0000157204952869, 0.0000157031457376, 0.0000156858149556, 0.0000156685029209, 0.0000156512096138, 0.0000156339350143, 0.0000156166791027, 0.0000155994418592, 0.0000155822232641, 0.0000155650232975, 0.0000155478419397, 0.0000155306791711, 0.0000155135349719, 0.0000154964093225, 0.0000154793022032, 0.0000154622135944, 0.0000154451434765, 0.0000154280918299, 0.0000154110586350, 0.0000153940438724, 0.0000153770475224, 0.0000153600695656, 0.0000153431099824, 0.0000153261687535, 0.0000153092458593, 0.0000152923412804, 0.0000152754549975, 0.0000152585869912, 0.0000152417372420, 0.0000152249057307, 0.0000152080924379, 0.0000151912973443, 0.0000151745204306, 0.0000151577616776, 0.0000151410210660, 0.0000151242985766, 0.0000151075941901, 0.0000150909078875, 0.0000150742396495, 0.0000150575894570, 0.0000150409572909, 0.0000150243431321, 0.0000150077469614, 0.0000149911687598, 0.0000149746085083, 0.0000149580661878, 0.0000149415417793, 0.0000149250352639, 0.0000149085466226, 0.0000148920758364, 0.0000148756228864, 0.0000148591877536, 0.0000148427704193, 0.0000148263708645, 0.0000148099890704, 0.0000147936250182, 0.0000147772786890, 0.0000147609500640, 0.0000147446391245, 0.0000147283458517, 0.0000147120702270, 0.0000146958122315, 0.0000146795718466, 0.0000146633490536, 0.0000146471438338, 0.0000146309561687, 0.0000146147860396, 0.0000145986334279, 0.0000145824983150, 0.0000145663806823, 0.0000145502805114, 0.0000145341977837, 0.0000145181324808, 0.0000145020845840, 0.0000144860540750, 0.0000144700409353, 0.0000144540451465, 0.0000144380666902, 0.0000144221055480, 0.0000144061617015, 0.0000143902351325, 0.0000143743258224, 0.0000143584337532, 0.0000143425589063, 0.0000143267012637, 0.0000143108608070, 0.0000142950375180, 0.0000142792313784, 0.0000142634423702, 0.0000142476704750, 0.0000142319156748, 0.0000142161779514, 0.0000142004572867, 0.0000141847536626, 0.0000141690670609, 0.0000141533974637, 0.0000141377448529, 0.0000141221092105, 0.0000141064905184, 0.0000140908887586, 0.0000140753039133, 0.0000140597359644, 0.0000140441848940, 0.0000140286506842, 0.0000140131333170, 0.0000139976327747, 0.0000139821490394, 0.0000139666820931, 0.0000139512319182, 0.0000139357984967, 0.0000139203818109, 0.0000139049818431, 0.0000138895985754, 0.0000138742319902, 0.0000138588820698, 0.0000138435487964, 0.0000138282321523, 0.0000138129321200, 0.0000137976486818, 0.0000137823818200, 0.0000137671315171, 0.0000137518977554, 0.0000137366805174, 0.0000137214797857, 0.0000137062955425, 0.0000136911277704, 0.0000136759764520, 0.0000136608415698, 0.0000136457231062, 0.0000136306210439, 0.0000136155353654, 0.0000136004660534, 0.0000135854130904, 0.0000135703764591, 0.0000135553561421, 0.0000135403521221, 0.0000135253643819, 0.0000135103929040, 0.0000134954376713, 0.0000134804986664, 0.0000134655758722, 0.0000134506692714, 0.0000134357788467, 0.0000134209045811, 0.0000134060464574, 0.0000133912044583, 0.0000133763785668, 0.0000133615687658, 0.0000133467750381, 0.0000133319973667, 0.0000133172357344, 0.0000133024901244, 0.0000132877605195, 0.0000132730469027, 0.0000132583492571, 0.0000132436675656, 0.0000132290018114, 0.0000132143519774, 0.0000131997180468, 0.0000131851000026, 0.0000131704978280, 0.0000131559115061, 0.0000131413410200, 0.0000131267863530, 0.0000131122474882, 0.0000130977244087, 0.0000130832170979, 0.0000130687255389, 0.0000130542497151, 0.0000130397896096, 0.0000130253452058, 0.0000130109164870, 0.0000129965034365, 0.0000129821060377, 0.0000129677242738, 0.0000129533581283, 0.0000129390075847, 0.0000129246726262, 0.0000129103532363, 0.0000128960493984, 0.0000128817610961, 0.0000128674883128, 0.0000128532310320, 0.0000128389892372, 0.0000128247629119, 0.0000128105520398, 0.0000127963566043, 0.0000127821765890, 0.0000127680119775, 0.0000127538627535, 0.0000127397289006, 0.0000127256104025, 0.0000127115072427, 0.0000126974194051, 0.0000126833468732, 0.0000126692896308, 0.0000126552476617, 0.0000126412209496, 0.0000126272094782, 0.0000126132132314, 0.0000125992321929, 0.0000125852663467, 0.0000125713156764, 0.0000125573801660, 0.0000125434597993, 0.0000125295545603, 0.0000125156644327, 0.0000125017894006, 0.0000124879294480, 0.0000124740845586, 0.0000124602547165, 0.0000124464399058, 0.0000124326401103, 0.0000124188553142, 0.0000124050855014, 0.0000123913306560, 0.0000123775907621, 0.0000123638658038, 0.0000123501557651, 0.0000123364606302, 0.0000123227803833, 0.0000123091150084, 0.0000122954644898, 0.0000122818288117, 0.0000122682079581, 0.0000122546019134, 0.0000122410106619, 0.0000122274341876, 0.0000122138724750, 0.0000122003255083, 0.0000121867932717, 0.0000121732757497, 0.0000121597729266, 0.0000121462847866, 0.0000121328113142, 0.0000121193524937, 0.0000121059083096, 0.0000120924787463, 0.0000120790637881, 0.0000120656634196, 0.0000120522776252, 0.0000120389063894, 0.0000120255496967, 0.0000120122075315, 0.0000119988798785, 0.0000119855667221, 0.0000119722680470, 0.0000119589838377, 0.0000119457140787, 0.0000119324587548, 0.0000119192178505, 0.0000119059913504, 0.0000118927792393, 0.0000118795815018, 0.0000118663981225, 0.0000118532290863, 0.0000118400743778, 0.0000118269339817, 0.0000118138078828, 0.0000118006960659, 0.0000117875985157, 0.0000117745152171, 0.0000117614461548, 0.0000117483913138, 0.0000117353506788, 0.0000117223242347, 0.0000117093119664, 0.0000116963138588, 0.0000116833298967, 0.0000116703600652, 0.0000116574043492, 0.0000116444627336, 0.0000116315352033, 0.0000116186217435, 0.0000116057223390, 0.0000115928369749, 0.0000115799656363, 0.0000115671083082, 0.0000115542649756, 0.0000115414356237, 0.0000115286202375, 0.0000115158188022, 0.0000115030313029, 0.0000114902577246, 0.0000114774980527, 0.0000114647522722, 0.0000114520203684, 0.0000114393023265, 0.0000114265981316, 0.0000114139077691, 0.0000114012312241, 0.0000113885684820, 0.0000113759195279, 0.0000113632843474, 0.0000113506629255, 0.0000113380552477, 0.0000113254612993, 0.0000113128810657, 0.0000113003145323, 0.0000112877616843, 0.0000112752225074, 0.0000112626969867, 0.0000112501851079, 0.0000112376868564, 0.0000112252022175, 0.0000112127311769, 0.0000112002737199, 0.0000111878298321, 0.0000111753994991, 0.0000111629827064, 0.0000111505794395, 0.0000111381896840, 0.0000111258134255, 0.0000111134506495, 0.0000111011013418, 0.0000110887654880, 0.0000110764430736, 0.0000110641340844, 0.0000110518385060, 0.0000110395563242, 0.0000110272875245, 0.0000110150320929, 0.0000110027900149, 0.0000109905612763, 0.0000109783458630, 0.0000109661437607, 0.0000109539549551, 0.0000109417794322, 0.0000109296171776, 0.0000109174681774, 0.0000109053324172, 0.0000108932098831, 0.0000108811005608, 0.0000108690044363, 0.0000108569214954, 0.0000108448517242, 0.0000108327951086, 0.0000108207516345, 0.0000108087212879, 0.0000107967040547, 0.0000107846999211, 0.0000107727088729, 0.0000107607308963, 0.0000107487659773, 0.0000107368141019, 0.0000107248752562, 0.0000107129494263, 0.0000107010365984, 0.0000106891367584, 0.0000106772498927, 0.0000106653759872, 0.0000106535150282, 0.0000106416670019, 0.0000106298318944, 0.0000106180096920, 0.0000106062003808, 0.0000105944039471, 0.0000105826203772, 0.0000105708496573, 0.0000105590917736, 0.0000105473467126, 0.0000105356144605, 0.0000105238950035, 0.0000105121883281, 0.0000105004944206, 0.0000104888132674, 0.0000104771448548, 0.0000104654891692, 0.0000104538461971, 0.0000104422159248, 0.0000104305983388, 0.0000104189934255, 0.0000104074011715, 0.0000103958215631, 0.0000103842545868, 0.0000103727002293, 0.0000103611584769, 0.0000103496293163, 0.0000103381127339, 0.0000103266087164, 0.0000103151172503, 0.0000103036383221, 0.0000102921719186, 0.0000102807180263, 0.0000102692766318, 0.0000102578477219, 0.0000102464312831, 0.0000102350273021, 0.0000102236357656, 0.0000102122566604, 0.0000102008899731, 0.0000101895356904, 0.0000101781937992, 0.0000101668642861, 0.0000101555471380, 0.0000101442423415, 0.0000101329498836, 0.0000101216697510, 0.0000101104019306, 0.0000100991464091, 0.0000100879031735, 0.0000100766722106, 0.0000100654535073, 0.0000100542470505, 0.0000100430528270, 0.0000100318708239, 0.0000100207010281, 0.0000100095434264, 0.0000099983980059, 0.0000099872647536, 0.0000099761436564, 0.0000099650347013, 0.0000099539378753, 0.0000099428531656, 0.0000099317805591, 0.0000099207200428, 0.0000099096716040, 0.0000098986352295, 0.0000098876109066, 0.0000098765986224, 0.0000098655983639, 0.0000098546101184, 0.0000098436338729, 0.0000098326696146, 0.0000098217173308, 0.0000098107770086, 0.0000097998486351, 0.0000097889321978, 0.0000097780276836, 0.0000097671350800, 0.0000097562543742, 0.0000097453855534, 0.0000097345286050, 0.0000097236835161, 0.0000097128502742, 0.0000097020288666, 0.0000096912192806, 0.0000096804215036, 0.0000096696355228, 0.0000096588613258, 0.0000096480988999, 0.0000096373482325, 0.0000096266093110, 0.0000096158821228, 0.0000096051666555, 0.0000095944628964, 0.0000095837708330, 0.0000095730904529, 0.0000095624217434, 0.0000095517646922, 0.0000095411192867, 0.0000095304855145, 0.0000095198633631, 0.0000095092528201, 0.0000094986538731, 0.0000094880665097, 0.0000094774907173, 0.0000094669264838, 0.0000094563737967, 0.0000094458326436, 0.0000094353030122, 0.0000094247848902, 0.0000094142782652, 0.0000094037831249, 0.0000093932994571, 0.0000093828272494, 0.0000093723664896, 0.0000093619171655, 0.0000093514792647, 0.0000093410527751, 0.0000093306376844, 0.0000093202339804, 0.0000093098416510, 0.0000092994606840, 0.0000092890910672, 0.0000092787327883, 0.0000092683858354, 0.0000092580501963, 0.0000092477258588, 0.0000092374128109, 0.0000092271110404, 0.0000092168205353, 0.0000092065412836, 0.0000091962732731, 0.0000091860164918, 0.0000091757709277, 0.0000091655365688, 0.0000091553134031, 0.0000091451014186, 0.0000091349006033, 0.0000091247109452, 0.0000091145324325, 0.0000091043650531, 0.0000090942087951, 0.0000090840636467, 0.0000090739295958, 0.0000090638066307, 0.0000090536947395, 0.0000090435939102, 0.0000090335041311, 0.0000090234253903, 0.0000090133576759, 0.0000090033009762, 0.0000089932552793, 0.0000089832205735, 0.0000089731968470, 0.0000089631840880, 0.0000089531822847, 0.0000089431914255, 0.0000089332114986, 0.0000089232424922, 0.0000089132843948, 0.0000089033371945, 0.0000088934008797, 0.0000088834754387, 0.0000088735608600, 0.0000088636571317, 0.0000088537642424, 0.0000088438821804, 0.0000088340109340, 0.0000088241504918, 0.0000088143008420, 0.0000088044619732, 0.0000087946338738, 0.0000087848165322, 0.0000087750099369, 0.0000087652140764, 0.0000087554289392, 0.0000087456545137, 0.0000087358907885, 0.0000087261377522, 0.0000087163953932, 0.0000087066637001, 0.0000086969426615, 0.0000086872322659, 0.0000086775325020, 0.0000086678433583, 0.0000086581648234, 0.0000086484968860, 0.0000086388395347, 0.0000086291927582, 0.0000086195565451, 0.0000086099308840, 0.0000086003157637, 0.0000085907111729, 0.0000085811171002, 0.0000085715335344, 0.0000085619604642, 0.0000085523978784, 0.0000085428457656, 0.0000085333041148, 0.0000085237729146, 0.0000085142521538, 0.0000085047418213, 0.0000084952419058, 0.0000084857523961, 0.0000084762732812, 0.0000084668045499, 0.0000084573461910, 0.0000084478981933, 0.0000084384605459, 0.0000084290332375, 0.0000084196162570, 0.0000084102095935, 0.0000084008132358, 0.0000083914271728, 0.0000083820513935, 0.0000083726858870, 0.0000083633306420, 0.0000083539856478, 0.0000083446508931, 0.0000083353263671, 0.0000083260120588, 0.0000083167079572, 0.0000083074140513, 0.0000082981303302, 0.0000082888567830, 0.0000082795933988, 0.0000082703401666, 0.0000082610970755, 0.0000082518641148, 0.0000082426412734, 0.0000082334285405, 0.0000082242259053, 0.0000082150333569, 0.0000082058508845, 0.0000081966784773, 0.0000081875161245, 0.0000081783638153, 0.0000081692215388, 0.0000081600892844, 0.0000081509670413, 0.0000081418547986, 0.0000081327525458, 0.0000081236602720, 0.0000081145779665, 0.0000081055056187, 0.0000080964432177, 0.0000080873907531, 0.0000080783482140, 0.0000080693155899, 0.0000080602928700, 0.0000080512800437, 0.0000080422771005, 0.0000080332840297, 0.0000080243008206, 0.0000080153274628, 0.0000080063639456, 0.0000079974102584, 0.0000079884663907, 0.0000079795323319, 0.0000079706080716, 0.0000079616935991, 0.0000079527889040, 0.0000079438939757, 0.0000079350088038, 0.0000079261333778, 0.0000079172676872, 0.0000079084117216, 0.0000078995654704, 0.0000078907289233, 0.0000078819020699, 0.0000078730848996, 0.0000078642774022, 0.0000078554795672, 0.0000078466913843, 0.0000078379128430, 0.0000078291439330, 0.0000078203846440, 0.0000078116349655, 0.0000078028948874, 0.0000077941643992, 0.0000077854434907, 0.0000077767321516, 0.0000077680303715, 0.0000077593381403, 0.0000077506554476, 0.0000077419822831, 0.0000077333186368, 0.0000077246644982, 0.0000077160198572, 0.0000077073847036, 0.0000076987590272, 0.0000076901428178, 0.0000076815360652, 0.0000076729387593, 0.0000076643508899, 0.0000076557724468, 0.0000076472034199, 0.0000076386437991, 0.0000076300935743, 0.0000076215527353, 0.0000076130212722, 0.0000076044991747, 0.0000075959864329, 0.0000075874830366, 0.0000075789889759, 0.0000075705042406, 0.0000075620288208, 0.0000075535627064, 0.0000075451058874, 0.0000075366583538, 0.0000075282200957, 0.0000075197911031, 0.0000075113713659, 0.0000075029608743, 0.0000074945596183, 0.0000074861675880, 0.0000074777847734, 0.0000074694111646, 0.0000074610467517, 0.0000074526915249, 0.0000074443454742, 0.0000074360085898, 0.0000074276808618, 0.0000074193622803, 0.0000074110528356, 0.0000074027525178, 0.0000073944613171, 0.0000073861792236, 0.0000073779062275, 0.0000073696423192, 0.0000073613874887, 0.0000073531417264, 0.0000073449050225, 0.0000073366773671, 0.0000073284587507, 0.0000073202491634, 0.0000073120485955, 0.0000073038570374, 0.0000072956744793, 0.0000072875009116, 0.0000072793363245, 0.0000072711807084, 0.0000072630340537, 0.0000072548963507, 0.0000072467675898, 0.0000072386477613, 0.0000072305368557, 0.0000072224348632, 0.0000072143417744, 0.0000072062575797, 0.0000071981822694, 0.0000071901158340, 0.0000071820582640, 0.0000071740095498, 0.0000071659696818, 0.0000071579386506, 0.0000071499164467, 0.0000071419030604, 0.0000071338984824, 0.0000071259027032, 0.0000071179157132, 0.0000071099375030, 0.0000071019680632, 0.0000070940073843, 0.0000070860554569, 0.0000070781122715, 0.0000070701778188, 0.0000070622520892, 0.0000070543350736, 0.0000070464267624, 0.0000070385271462, 0.0000070306362158, 0.0000070227539618, 0.0000070148803747, 0.0000070070154454, 0.0000069991591643, 0.0000069913115224, 0.0000069834725101, 0.0000069756421183, 0.0000069678203376, 0.0000069600071588, 0.0000069522025726, 0.0000069444065697, 0.0000069366191410, 0.0000069288402771, 0.0000069210699688, 0.0000069133082069, 0.0000069055549823, 0.0000068978102857, 0.0000068900741078, 0.0000068823464396, 0.0000068746272719, 0.0000068669165955, 0.0000068592144013, 0.0000068515206800, 0.0000068438354227, 0.0000068361586201, 0.0000068284902632, 0.0000068208303428, 0.0000068131788499, 0.0000068055357753, 0.0000067979011101, 0.0000067902748451, 0.0000067826569712, 0.0000067750474795, 0.0000067674463609, 0.0000067598536064, 0.0000067522692070, 0.0000067446931536, 0.0000067371254372, 0.0000067295660490, 0.0000067220149798, 0.0000067144722207, 0.0000067069377628, 0.0000066994115971, 0.0000066918937147, 0.0000066843841067, 0.0000066768827640, 0.0000066693896779, 0.0000066619048393, 0.0000066544282395, 0.0000066469598695, 0.0000066394997204, 0.0000066320477835, 0.0000066246040497, 0.0000066171685104, 0.0000066097411566, 0.0000066023219795, 0.0000065949109703, 0.0000065875081202, 0.0000065801134204, 0.0000065727268621, 0.0000065653484365, 0.0000065579781349, 0.0000065506159484, 0.0000065432618684, 0.0000065359158861, 0.0000065285779927, 0.0000065212481795, 0.0000065139264379, 0.0000065066127590, 0.0000064993071342, 0.0000064920095549, 0.0000064847200123, 0.0000064774384977, 0.0000064701650026, 0.0000064628995182, 0.0000064556420359, 0.0000064483925471, 0.0000064411510431, 0.0000064339175153, 0.0000064266919552, 0.0000064194743541, 0.0000064122647034, 0.0000064050629946, 0.0000063978692191, 0.0000063906833682, 0.0000063835054336, 0.0000063763354066, 0.0000063691732786, 0.0000063620190413, 0.0000063548726860, 0.0000063477342042, 0.0000063406035875, 0.0000063334808273, 0.0000063263659152, 0.0000063192588427, 0.0000063121596013, 0.0000063050681826, 0.0000062979845782, 0.0000062909087796, 0.0000062838407783, 0.0000062767805660, 0.0000062697281342, 0.0000062626834746, 0.0000062556465788, 0.0000062486174383, 0.0000062415960448, 0.0000062345823900, 0.0000062275764655, 0.0000062205782628, 0.0000062135877738, 0.0000062066049900, 0.0000061996299032, 0.0000061926625050, 0.0000061857027872, 0.0000061787507414, 0.0000061718063593, 0.0000061648696327, 0.0000061579405533, 0.0000061510191128, 0.0000061441053030, 0.0000061371991157, 0.0000061303005426, 0.0000061234095755, 0.0000061165262062, 0.0000061096504264, 0.0000061027822280, 0.0000060959216028, 0.0000060890685426, 0.0000060822230392, 0.0000060753850845, 0.0000060685546703, 0.0000060617317885, 0.0000060549164309, 0.0000060481085893, 0.0000060413082558, 0.0000060345154221, 0.0000060277300801, 0.0000060209522218, 0.0000060141818391, 0.0000060074189239, 0.0000060006634682, 0.0000059939154637, 0.0000059871749026, 0.0000059804417768, 0.0000059737160782, 0.0000059669977988, 0.0000059602869306, 0.0000059535834656, 0.0000059468873958, 0.0000059401987131, 0.0000059335174097, 0.0000059268434774, 0.0000059201769084, 0.0000059135176948, 0.0000059068658284, 0.0000059002213015, 0.0000058935841060, 0.0000058869542341, 0.0000058803316778, 0.0000058737164292, 0.0000058671084805, 0.0000058605078237, 0.0000058539144509, 0.0000058473283542, 0.0000058407495259, 0.0000058341779580, 0.0000058276136427, 0.0000058210565722, 0.0000058145067385, 0.0000058079641340, 0.0000058014287507, 0.0000057949005809, 0.0000057883796167, 0.0000057818658504, 0.0000057753592742, 0.0000057688598803, 0.0000057623676609, 0.0000057558826083, 0.0000057494047147, 0.0000057429339725, 0.0000057364703737, 0.0000057300139108, 0.0000057235645760, 0.0000057171223616, 0.0000057106872599, 0.0000057042592632, 0.0000056978383638, 0.0000056914245540, 0.0000056850178262, 0.0000056786181727, 0.0000056722255858, 0.0000056658400580, 0.0000056594615815, 0.0000056530901487, 0.0000056467257521, 0.0000056403683839, 0.0000056340180367, 0.0000056276747027, 0.0000056213383745, 0.0000056150090444, 0.0000056086867049, 0.0000056023713483, 0.0000055960629672, 0.0000055897615539, 0.0000055834671011, 0.0000055771796010, 0.0000055708990462, 0.0000055646254292, 0.0000055583587424, 0.0000055520989784, 0.0000055458461297, 0.0000055396001888, 0.0000055333611481, 0.0000055271290003, 0.0000055209037379, 0.0000055146853534, 0.0000055084738393, 0.0000055022691884, 0.0000054960713930, 0.0000054898804458, 0.0000054836963393, 0.0000054775190663, 0.0000054713486192, 0.0000054651849907, 0.0000054590281733, 0.0000054528781598, 0.0000054467349428, 0.0000054405985148, 0.0000054344688686, 0.0000054283459968, 0.0000054222298920, 0.0000054161205470, 0.0000054100179544, 0.0000054039221069, 0.0000053978329972, 0.0000053917506180, 0.0000053856749619, 0.0000053796060219, 0.0000053735437904, 0.0000053674882604, 0.0000053614394245, 0.0000053553972755, 0.0000053493618061, 0.0000053433330091, 0.0000053373108772, 0.0000053312954034, 0.0000053252865802, 0.0000053192844006, 0.0000053132888574, 0.0000053072999433, 0.0000053013176511, 0.0000052953419737, 0.0000052893729040, 0.0000052834104347, 0.0000052774545588, 0.0000052715052690, 0.0000052655625582, 0.0000052596264194, 0.0000052536968453, 0.0000052477738289, 0.0000052418573631, 0.0000052359474407, 0.0000052300440547, 0.0000052241471980, 0.0000052182568635, 0.0000052123730442, 0.0000052064957329, 0.0000052006249227, 0.0000051947606064, 0.0000051889027771, 0.0000051830514278, 0.0000051772065513, 0.0000051713681406, 0.0000051655361889, 0.0000051597106890, 0.0000051538916340, 0.0000051480790168, 0.0000051422728306, 0.0000051364730683, 0.0000051306797230, 0.0000051248927877, 0.0000051191122554, 0.0000051133381193, 0.0000051075703723, 0.0000051018090076, 0.0000050960540183, 0.0000050903053974, 0.0000050845631380, 0.0000050788272332, 0.0000050730976761, 0.0000050673744599, 0.0000050616575777, 0.0000050559470225, 0.0000050502427876, 0.0000050445448661, 0.0000050388532512, 0.0000050331679359, 0.0000050274889135, 0.0000050218161771, 0.0000050161497200, 0.0000050104895353, 0.0000050048356162, 0.0000049991879559, 0.0000049935465476, 0.0000049879113846, 0.0000049822824600, 0.0000049766597671, 0.0000049710432992, 0.0000049654330495, 0.0000049598290112, 0.0000049542311776, 0.0000049486395420, 0.0000049430540976, 0.0000049374748377, 0.0000049319017557, 0.0000049263348448, 0.0000049207740983, 0.0000049152195095, 0.0000049096710718, 0.0000049041287784, 0.0000048985926228, 0.0000048930625982, 0.0000048875386980, 0.0000048820209156, 0.0000048765092442, 0.0000048710036773, 0.0000048655042083, 0.0000048600108305, 0.0000048545235373, 0.0000048490423222, 0.0000048435671784, 0.0000048380980995, 0.0000048326350788, 0.0000048271781098, 0.0000048217271859, 0.0000048162823006, 0.0000048108434472, 0.0000048054106193, 0.0000047999838102, 0.0000047945630135, 0.0000047891482227, 0.0000047837394311, 0.0000047783366324, 0.0000047729398199, 0.0000047675489872, 0.0000047621641279, 0.0000047567852353, 0.0000047514123031, 0.0000047460453247, 0.0000047406842937, 0.0000047353292037, 0.0000047299800482, 0.0000047246368207, 0.0000047192995148, 0.0000047139681241, 0.0000047086426421, 0.0000047033230624, 0.0000046980093787, 0.0000046927015845, 0.0000046873996734, 0.0000046821036391, 0.0000046768134751, 0.0000046715291751, 0.0000046662507326, 0.0000046609781415, 0.0000046557113951, 0.0000046504504874, 0.0000046451954118, 0.0000046399461621, 0.0000046347027319, 0.0000046294651149, 0.0000046242333048, 0.0000046190072952, 0.0000046137870800, 0.0000046085726527, 0.0000046033640071, 0.0000045981611370, 0.0000045929640359, 0.0000045877726978, 0.0000045825871162, 0.0000045774072850, 0.0000045722331978, 0.0000045670648486, 0.0000045619022309, 0.0000045567453387, 0.0000045515941656, 0.0000045464487055, 0.0000045413089521, 0.0000045361748993, 0.0000045310465408, 0.0000045259238704, 0.0000045208068821, 0.0000045156955695, 0.0000045105899266, 0.0000045054899471, 0.0000045003956250, 0.0000044953069540, 0.0000044902239280, 0.0000044851465409, 0.0000044800747866, 0.0000044750086588, 0.0000044699481516, 0.0000044648932588, 0.0000044598439743, 0.0000044548002920, 0.0000044497622057, 0.0000044447297095, 0.0000044397027973, 0.0000044346814629, 0.0000044296657003, 0.0000044246555035, 0.0000044196508663, 0.0000044146517828, 0.0000044096582469, 0.0000044046702526, 0.0000043996877939, 0.0000043947108646, 0.0000043897394589, 0.0000043847735707, 0.0000043798131940, 0.0000043748583227, 0.0000043699089511, 0.0000043649650729, 0.0000043600266823, 0.0000043550937734, 0.0000043501663400, 0.0000043452443763, 0.0000043403278764, 0.0000043354168342, 0.0000043305112439, 0.0000043256110995, 0.0000043207163950, 0.0000043158271247, 0.0000043109432825, 0.0000043060648625, 0.0000043011918590, 0.0000042963242658, 0.0000042914620773, 0.0000042866052874, 0.0000042817538903, 0.0000042769078802, 0.0000042720672512, 0.0000042672319974, 0.0000042624021130, 0.0000042575775921, 0.0000042527584289, 0.0000042479446176, 0.0000042431361523, 0.0000042383330273, 0.0000042335352367, 0.0000042287427746, 0.0000042239556354, 0.0000042191738132, 0.0000042143973022, 0.0000042096260967, 0.0000042048601908, 0.0000042000995789, 0.0000041953442551, 0.0000041905942136, 0.0000041858494488, 0.0000041811099548, 0.0000041763757261, 0.0000041716467567, 0.0000041669230410, 0.0000041622045733, 0.0000041574913478, 0.0000041527833589, 0.0000041480806008, 0.0000041433830679, 0.0000041386907544, 0.0000041340036547, 0.0000041293217631, 0.0000041246450740, 0.0000041199735816, 0.0000041153072802, 0.0000041106461644, 0.0000041059902283, 0.0000041013394664, 0.0000040966938730, 0.0000040920534425, 0.0000040874181693, 0.0000040827880478, 0.0000040781630722, 0.0000040735432372, 0.0000040689285369, 0.0000040643189659, 0.0000040597145186, 0.0000040551151893, 0.0000040505209725, 0.0000040459318627, 0.0000040413478542, 0.0000040367689415, 0.0000040321951191, 0.0000040276263814, 0.0000040230627229, 0.0000040185041380, 0.0000040139506212, 0.0000040094021670, 0.0000040048587698, 0.0000040003204243, 0.0000039957871248, 0.0000039912588658, 0.0000039867356419, 0.0000039822174476, 0.0000039777042773, 0.0000039731961257, 0.0000039686929873, 0.0000039641948565, 0.0000039597017280, 0.0000039552135962, 0.0000039507304557, 0.0000039462523012, 0.0000039417791271, 0.0000039373109280, 0.0000039328476986, 0.0000039283894333, 0.0000039239361268, 0.0000039194877737, 0.0000039150443685, 0.0000039106059059, 0.0000039061723805, 0.0000039017437869, 0.0000038973201198, 0.0000038929013736, 0.0000038884875432, 0.0000038840786231, 0.0000038796746080, 0.0000038752754926, 0.0000038708812714, 0.0000038664919391, 0.0000038621074905, 0.0000038577279202, 0.0000038533532228, 0.0000038489833931, 0.0000038446184258, 0.0000038402583155, 0.0000038359030569, 0.0000038315526448, 0.0000038272070739, 0.0000038228663389, 0.0000038185304345, 0.0000038141993554, 0.0000038098730965, 0.0000038055516524, 0.0000038012350178, 0.0000037969231876, 0.0000037926161565, 0.0000037883139192, 0.0000037840164705, 0.0000037797238053, 0.0000037754359182, 0.0000037711528041, 0.0000037668744578, 0.0000037626008741, 0.0000037583320477, 0.0000037540679734, 0.0000037498086462, 0.0000037455540608, 0.0000037413042120, 0.0000037370590947, 0.0000037328187037, 0.0000037285830338, 0.0000037243520799, 0.0000037201258369, 0.0000037159042996, 0.0000037116874629, 0.0000037074753216, 0.0000037032678706, 0.0000036990651048, 0.0000036948670191, 0.0000036906736084, 0.0000036864848675, 0.0000036823007915, 0.0000036781213751, 0.0000036739466133, 0.0000036697765011, 0.0000036656110333, 0.0000036614502048, 0.0000036572940107, 0.0000036531424458, 0.0000036489955051, 0.0000036448531836, 0.0000036407154762, 0.0000036365823778, 0.0000036324538835, 0.0000036283299881, 0.0000036242106868, 0.0000036200959744, 0.0000036159858460, 0.0000036118802966, 0.0000036077793211, 0.0000036036829146, 0.0000035995910720, 0.0000035955037884, 0.0000035914210589, 0.0000035873428783, 0.0000035832692419, 0.0000035792001445, 0.0000035751355813, 0.0000035710755473, 0.0000035670200375, 0.0000035629690470, 0.0000035589225709, 0.0000035548806043, 0.0000035508431421, 0.0000035468101796, 0.0000035427817117, 0.0000035387577336, 0.0000035347382404, 0.0000035307232271, 0.0000035267126889, 0.0000035227066209, 0.0000035187050181, 0.0000035147078758, 0.0000035107151890, 0.0000035067269530, 0.0000035027431627, 0.0000034987638134, 0.0000034947889002, 0.0000034908184182, 0.0000034868523627, 0.0000034828907287, 0.0000034789335115, 0.0000034749807062, 0.0000034710323080, 0.0000034670883121, 0.0000034631487137, 0.0000034592135079, 0.0000034552826900, 0.0000034513562551, 0.0000034474341986, 0.0000034435165155, 0.0000034396032011, 0.0000034356942507, 0.0000034317896595, 0.0000034278894226, 0.0000034239935354, 0.0000034201019931, 0.0000034162147909, 0.0000034123319241, 0.0000034084533880, 0.0000034045791778, 0.0000034007092887, 0.0000033968437162, 0.0000033929824554, 0.0000033891255016, 0.0000033852728502, 0.0000033814244963, 0.0000033775804354, 0.0000033737406627, 0.0000033699051736, 0.0000033660739633, 0.0000033622470272, 0.0000033584243605, 0.0000033546059588, 0.0000033507918171, 0.0000033469819310, 0.0000033431762958, 0.0000033393749067, 0.0000033355777592, 0.0000033317848487, 0.0000033279961704, 0.0000033242117198, 0.0000033204314922, 0.0000033166554830, 0.0000033128836876, 0.0000033091161015, 0.0000033053527199, 0.0000033015935383, 0.0000032978385521, 0.0000032940877567, 0.0000032903411475, 0.0000032865987200, 0.0000032828604695, 0.0000032791263915, 0.0000032753964815, 0.0000032716707348, 0.0000032679491469, 0.0000032642317133, 0.0000032605184294, 0.0000032568092907, 0.0000032531042927, 0.0000032494034307, 0.0000032457067003, 0.0000032420140970, 0.0000032383256163, 0.0000032346412535, 0.0000032309610044, 0.0000032272848642, 0.0000032236128286, 0.0000032199448930, 0.0000032162810529, 0.0000032126213039, 0.0000032089656415, 0.0000032053140613, 0.0000032016665586, 0.0000031980231292, 0.0000031943837685, 0.0000031907484720, 0.0000031871172354, 0.0000031834900541, 0.0000031798669238, 0.0000031762478400, 0.0000031726327983, 0.0000031690217942, 0.0000031654148234, 0.0000031618118813, 0.0000031582129637, 0.0000031546180661, 0.0000031510271840, 0.0000031474403132, 0.0000031438574491, 0.0000031402785875, 0.0000031367037239, 0.0000031331328539, 0.0000031295659733, 0.0000031260030775, 0.0000031224441623, 0.0000031188892233, 0.0000031153382561, 0.0000031117912564, 0.0000031082482199, 0.0000031047091421, 0.0000031011740188, 0.0000030976428457, 0.0000030941156183, 0.0000030905923324, 0.0000030870729837, 0.0000030835575679, 0.0000030800460806, 0.0000030765385175, 0.0000030730348743, 0.0000030695351469, 0.0000030660393307, 0.0000030625474217, 0.0000030590594154, 0.0000030555753077, 0.0000030520950941, 0.0000030486187706, 0.0000030451463328, 0.0000030416777764, 0.0000030382130973, 0.0000030347522911, 0.0000030312953536, 0.0000030278422806, 0.0000030243930678, 0.0000030209477110, 0.0000030175062060, 0.0000030140685486, 0.0000030106347345, 0.0000030072047595, 0.0000030037786195, 0.0000030003563102, 0.0000029969378274, 0.0000029935231669, 0.0000029901123246, 0.0000029867052962, 0.0000029833020776, 0.0000029799026646, 0.0000029765070530, 0.0000029731152386, 0.0000029697272174, 0.0000029663429851, 0.0000029629625375, 0.0000029595858707, 0.0000029562129802, 0.0000029528438622, 0.0000029494785123, 0.0000029461169266, 0.0000029427591007, 0.0000029394050307, 0.0000029360547125, 0.0000029327081418, 0.0000029293653146, 0.0000029260262268, 0.0000029226908742, 0.0000029193592529, 0.0000029160313586, 0.0000029127071874, 0.0000029093867350, 0.0000029060699976, 0.0000029027569708, 0.0000028994476508, 0.0000028961420334, 0.0000028928401146, 0.0000028895418904, 0.0000028862473565, 0.0000028829565091, 0.0000028796693441, 0.0000028763858574, 0.0000028731060450, 0.0000028698299028, 0.0000028665574269, 0.0000028632886133, 0.0000028600234578, 0.0000028567619565, 0.0000028535041054, 0.0000028502499004, 0.0000028469993377, 0.0000028437524131, 0.0000028405091227, 0.0000028372694625, 0.0000028340334285, 0.0000028308010167, 0.0000028275722232, 0.0000028243470440, 0.0000028211254751, 0.0000028179075126, 0.0000028146931525, 0.0000028114823908, 0.0000028082752236, 0.0000028050716470, 0.0000028018716570, 0.0000027986752497, 0.0000027954824211, 0.0000027922931673, 0.0000027891074845, 0.0000027859253686, 0.0000027827468157, 0.0000027795718220, 0.0000027764003836, 0.0000027732324965, 0.0000027700681568, 0.0000027669073607, 0.0000027637501042, 0.0000027605963835, 0.0000027574461947, 0.0000027542995339, 0.0000027511563972, 0.0000027480167808, 0.0000027448806808, 0.0000027417480933, 0.0000027386190145, 0.0000027354934406, 0.0000027323713676, 0.0000027292527917, 0.0000027261377092, 0.0000027230261161, 0.0000027199180087, 0.0000027168133830, 0.0000027137122353, 0.0000027106145618, 0.0000027075203586, 0.0000027044296220, 0.0000027013423480, 0.0000026982585330, 0.0000026951781731, 0.0000026921012646, 0.0000026890278035, 0.0000026859577862, 0.0000026828912089, 0.0000026798280677, 0.0000026767683590, 0.0000026737120789, 0.0000026706592237, 0.0000026676097896, 0.0000026645637728, 0.0000026615211696, 0.0000026584819763, 0.0000026554461891, 0.0000026524138042, 0.0000026493848180, 0.0000026463592266, 0.0000026433370264, 0.0000026403182136, 0.0000026373027845, 0.0000026342907354, 0.0000026312820626, 0.0000026282767623, 0.0000026252748309, 0.0000026222762647, 0.0000026192810599, 0.0000026162892128, 0.0000026133007199, 0.0000026103155773, 0.0000026073337814, 0.0000026043553285, 0.0000026013802150, 0.0000025984084372, 0.0000025954399913, 0.0000025924748739, 0.0000025895130811, 0.0000025865546093, 0.0000025835994549, 0.0000025806476143, 0.0000025776990838, 0.0000025747538597, 0.0000025718119384, 0.0000025688733163, 0.0000025659379898, 0.0000025630059552, 0.0000025600772089, 0.0000025571517474, 0.0000025542295669, 0.0000025513106639, 0.0000025483950348, 0.0000025454826759, 0.0000025425735838, 0.0000025396677547, 0.0000025367651851, 0.0000025338658715, 0.0000025309698101, 0.0000025280769976, 0.0000025251874302, 0.0000025223011044, 0.0000025194180167, 0.0000025165381635, 0.0000025136615412, 0.0000025107881463, 0.0000025079179752, 0.0000025050510244, 0.0000025021872903, 0.0000024993267695, 0.0000024964694583, 0.0000024936153532, 0.0000024907644508, 0.0000024879167474, 0.0000024850722396, 0.0000024822309238, 0.0000024793927966, 0.0000024765578544, 0.0000024737260937, 0.0000024708975111, 0.0000024680721029, 0.0000024652498658, 0.0000024624307963, 0.0000024596148908, 0.0000024568021459, 0.0000024539925581, 0.0000024511861239, 0.0000024483828398, 0.0000024455827025, 0.0000024427857084, 0.0000024399918540, 0.0000024372011359, 0.0000024344135507, 0.0000024316290949, 0.0000024288477651, 0.0000024260695578, 0.0000024232944696, 0.0000024205224970, 0.0000024177536367, 0.0000024149878851, 0.0000024122252390, 0.0000024094656948, 0.0000024067092491, 0.0000024039558985, 0.0000024012056397, 0.0000023984584692, 0.0000023957143836, 0.0000023929733796, 0.0000023902354536, 0.0000023875006024, 0.0000023847688226, 0.0000023820401107, 0.0000023793144634, 0.0000023765918774, 0.0000023738723492, 0.0000023711558755, 0.0000023684424529, 0.0000023657320780, 0.0000023630247476, 0.0000023603204582, 0.0000023576192065, 0.0000023549209891, 0.0000023522258028, 0.0000023495336441, 0.0000023468445098, 0.0000023441583964, 0.0000023414753008, 0.0000023387952195, 0.0000023361181493, 0.0000023334440867, 0.0000023307730286, 0.0000023281049716, 0.0000023254399123, 0.0000023227778476, 0.0000023201187740, 0.0000023174626883, 0.0000023148095872, 0.0000023121594675, 0.0000023095123257, 0.0000023068681587, 0.0000023042269632, 0.0000023015887359, 0.0000022989534734, 0.0000022963211727, 0.0000022936918303, 0.0000022910654431, 0.0000022884420077, 0.0000022858215210, 0.0000022832039797, 0.0000022805893804, 0.0000022779777201, 0.0000022753689954, 0.0000022727632031, 0.0000022701603400, 0.0000022675604028, 0.0000022649633884, 0.0000022623692934, 0.0000022597781148, 0.0000022571898492, 0.0000022546044935, 0.0000022520220444, 0.0000022494424988, 0.0000022468658534, 0.0000022442921051, 0.0000022417212506, 0.0000022391532868, 0.0000022365882105, 0.0000022340260185, 0.0000022314667075, 0.0000022289102746, 0.0000022263567164, 0.0000022238060297, 0.0000022212582115, 0.0000022187132586, 0.0000022161711678, 0.0000022136319359, 0.0000022110955599, 0.0000022085620364, 0.0000022060313625, 0.0000022035035350, 0.0000022009785506, 0.0000021984564064, 0.0000021959370991, 0.0000021934206256, 0.0000021909069828, 0.0000021883961676, 0.0000021858881769, 0.0000021833830075, 0.0000021808806563, 0.0000021783811202, 0.0000021758843962, 0.0000021733904811, 0.0000021708993718, 0.0000021684110652, 0.0000021659255582, 0.0000021634428478, 0.0000021609629308, 0.0000021584858042, 0.0000021560114649, 0.0000021535399098, 0.0000021510711359, 0.0000021486051400, 0.0000021461419192, 0.0000021436814703, 0.0000021412237903, 0.0000021387688761, 0.0000021363167247, 0.0000021338673330, 0.0000021314206981, 0.0000021289768168, 0.0000021265356860, 0.0000021240973029, 0.0000021216616643, 0.0000021192287672, 0.0000021167986086, 0.0000021143711855, 0.0000021119464948, 0.0000021095245335, 0.0000021071052987, 0.0000021046887872, 0.0000021022749962, 0.0000020998639225, 0.0000020974555633, 0.0000020950499154, 0.0000020926469760, 0.0000020902467419, 0.0000020878492103, 0.0000020854543782, 0.0000020830622425, 0.0000020806728003, 0.0000020782860486, 0.0000020759019844, 0.0000020735206049, 0.0000020711419069, 0.0000020687658876, 0.0000020663925440, 0.0000020640218731, 0.0000020616538720, 0.0000020592885378, 0.0000020569258674, 0.0000020545658580, 0.0000020522085067, 0.0000020498538104, 0.0000020475017662, 0.0000020451523712, 0.0000020428056226, 0.0000020404615173, 0.0000020381200525, 0.0000020357812252, 0.0000020334450325, 0.0000020311114715, 0.0000020287805393, 0.0000020264522330, 0.0000020241265497, 0.0000020218034865, 0.0000020194830405, 0.0000020171652089, 0.0000020148499886, 0.0000020125373769, 0.0000020102273709, 0.0000020079199676, 0.0000020056151642, 0.0000020033129579, 0.0000020010133457, 0.0000019987163248, 0.0000019964218923, 0.0000019941300454, 0.0000019918407813, 0.0000019895540969, 0.0000019872699896, 0.0000019849884565, };

  float sbr_et[interface::bril::shared::MAX_NBX] = {1.0, 0.02980003, 4.24787103e-03, 4.27978138e-04, 0.0010356194553775566, 0.0009626241116900353, 0.0008970334680314758, 0.0008380616834822833, 0.0007850073676633778, 0.0007372445431628781, 0.0006942145784101556, 0.0006554189867067844, 0.0006204129982767821, 0.0005887998221526117, 0.0005602255235978036, 0.0005343744506971256, 0.0005109651508240438, 0.0004897467240149576, 0.00047049556592270814, 0.00045301245806180423, 0.0004371199675605084, 0.0004226601226592312, 0.0004094923337931741, 0.00039749153331683674, 0.00038654650981078694, 0.00037655841549442016, 0.0003674394275856252, 0.0003591115465289963, 0.0003515055158848093, 0.00034455985035473376, 0.0003382199599377708, 0.0003324373595792956, 0.0003271689549132067, 0.0003223763958158554, 0.0003180254905026253, 0.0003140856738140342, 0.0003105295241668479, 0.00030733232439436695, 0.0003044716623750271, 0.00030192706795496827, 0.0002996796832125872, 0.00029771196359485894, 0.0002960074078793496, 0.0002945503152848077, 0.0002933255683691422, 0.00029231844061836494, 0.00029151442784549773, 0.0002908991026863147, 0.0002904579916010366, 0.0002901764738698165, 0.00029003970210745976, 0.0002900325438220228, 0.0002901395435058388, 0.0002903449046796468, 0.00029063249121475586, 0.00029098584713889533, 0.0002913882339932551, 0.0002918226846562138, 0.00029227207238863176, 0.0002927191936917514, 0.0002931468634071777, 0.0002935380203345325, 0.00029387584150145665, 0.0002941438630976729, 0.0002943261059844082, 0.0002944072036167066, 0.0002943725301725063, 0.0002942083266715648, 0.0002939018228913769, 0.00029344135294723906, 0.0002928164624998104, 0.00029201800568521663, 0.0002910382300283409, 0.0002898708477969957, 0.0002885110924798848, 0.0002869557593206264, 0.00028520322910895857, 0.00028325347471339736, 0.00028110805013149197, 0.00027877006212859336, 0.0002762441248277899, 0.00027353629789650705, 0.000270654009243536, 0.000267605963388642, 0.00026440203689056754, 0.00026105316241395145, 0.0002575712031779164, 0.0002539688196560738, 0.00025025933048756707, 0.0002464565696104867, 0.00024257474164240427, 0.000238628277508614, 0.00023463169225849178, 0.000230599446916513, 0.0002265458160899212, 0.00022248476290341823, 0.00021842982265664486, 0.00021439399640708738, 0.00021038965547408065, 0.00020642845764358908, 0.00020252127563322907, 0.00019867813815722878, 0.0001949081837161321, 0.0001912196270301473, 0.0001876197378417973, 0.0001841148316361468, 0.00018071027166801238, 0.00017741048154730043, 0.00017421896751745365, 0.00017113834946883274, 0.00016817039965905342, 0.00016531608806562772, 0.00016257563327202562, 0.00015994855778531222, 0.00015743374670030034, 0.00015502950865981492, 0.0001527336381110956, 0.00015054347792226955, 0.00014845598149781857, 0.00014646777361559862, 0.00014457520929783466, 0.00014277443012226582, 0.00014106141747504975, 0.00013943204234210266, 0.00013788211132842066, 0.0001364074086839796, 0.0001350037341986823, 0.00013366693690639484, 0.00013239294460852454, 0.00013117778929022577, 0.000130017628556797, 0.00012890876326399078, 0.0001278476515538347, 0.00012683091953737184, 0.00012585536888783358, 0.00012491798162264376, 0.00012401592236090746, 0.00012314653834531324, 0.0001223073575143821, 0.00012149608490345451, 0.0001207105976414557, 0.00011994893879603569, 0.00011920931030283623, 0.00011849006519604232, 0.00011778969933762892, 0.00011710684282235509, 0.00011644025121506638, 0.00011578879675665434, 0.00011515145965543616, 0.00011452731956204424, 0.00011391554730837163, 0.00011331539697487463, 0.00011272619833569752, 0.00011214734971772198, 0.00011157831129777821, 0.0001110185988518769, 0.00011046777796138505, 0.00010992545867351397, 0.00010939129060722684, 0.00010886495849061215, 0.0001083461781117996, 0.00010783469266250787, 0.00010733026945119374, 0.00010683269696141112, 0.00010634178223027661, 0.00010585734852177306, 0.0001053792332699114, 0.00010490728626742203, 0.00010444136807658536, 0.0001039813486399629, 0.0001035271060700953, 0.0001030785255986393, 0.00010263549866687359, 0.00010219792214098007, 0.0001017656976369678, 0.00010133873094153062, 0.00010091693151649393, 0.00010050021207580126, 0.00010008848822520403, 9.968167815594477e-05, 9.927970238476043e-05, 9.888248353347905e-05, 9.848994614234044e-05, 9.810201651194293e-05, 9.77186225694084e-05, 9.733969375496969e-05, 9.696516092572734e-05, 9.659495627379651e-05, 9.622901325648123e-05, 9.586726653647442e-05, 9.550965193039401e-05, 9.515610636423495e-05, 9.48065678345469e-05, 9.446097537434467e-05, 9.41192690229248e-05, 9.378138979890376e-05, 9.34472796759114e-05, 9.311688156047401e-05, 9.279013927170396e-05, 9.246699752248279e-05, 9.214740190188263e-05, 9.183129885861713e-05, 9.151863568535375e-05, 9.12093605037502e-05, 9.090342225010477e-05, 9.060077066153155e-05, 9.03013562625883e-05, 9.000513035229999e-05, 8.97120449915308e-05, 8.9422052990668e-05, 8.913510789758736e-05, 8.885116398587556e-05, 8.85701762432911e-05, 8.829210036044654e-05, 8.80168927197002e-05, 8.774451038424605e-05, 8.747491108739323e-05, 8.720805322202788e-05, 8.694389583025077e-05, 8.668239859318569e-05, 8.642352182095354e-05, 8.616722644280852e-05, 8.591347399743207e-05, 8.566222662338181e-05, 8.541344704969196e-05, 8.516709858662254e-05, 8.492314511655469e-05, 8.468155108502928e-05, 8.444228149192682e-05, 8.420530188278559e-05, 8.397057834025645e-05, 8.373807747569166e-05, 8.350776642086564e-05, 8.327961281982546e-05, 8.305358482086924e-05, 8.282965106865032e-05, 8.260778069640492e-05, 8.238794331830184e-05, 8.217010902191199e-05, 8.195424836079568e-05, 8.174033234720641e-05, 8.152833244490872e-05, 8.131822056210852e-05, 8.110996904449441e-05, 8.090355066838773e-05, 8.069893863400011e-05, 8.04961065587966e-05, 8.029502847096265e-05, 8.009567880297367e-05, 7.989803238526504e-05, 7.970206444000149e-05, 7.950775057494395e-05, 7.931506677741257e-05, 7.912398940834428e-05, 7.893449519644333e-05, 7.874656123242363e-05, 7.856016496334123e-05, 7.837528418701539e-05, 7.819189704653736e-05, 7.800998202486498e-05, 7.782951793950196e-05, 7.76504839372605e-05, 7.747285948910603e-05, 7.729662438508273e-05, 7.712175872931827e-05, 7.694824293510729e-05, 7.677605772007125e-05, 7.660518410139472e-05, 7.643560339113588e-05, 7.626729719161072e-05, 7.610024739084925e-05, 7.59344361581233e-05, 7.576984593954399e-05, 7.560645945372834e-05, 7.544425968753382e-05, 7.528322989185955e-05, 7.512335357751332e-05, 7.496461451114353e-05, 7.480699671123449e-05, 7.465048444416489e-05, 7.449506222032764e-05, 7.434071479031064e-05, 7.418742714113744e-05, 7.403518449256664e-05, 7.388397229344938e-05, 7.373377621814383e-05, 7.358458216298582e-05, 7.34363762428148e-05, 7.328914478755424e-05, 7.314287433884546e-05, 7.299755164673442e-05, 7.285316366641014e-05, 7.270969755499441e-05, 7.256714066838171e-05, 7.242548055812862e-05, 7.228470496839196e-05, 7.21448018329149e-05, 7.20057592720603e-05, 7.186756558989052e-05, 7.173020927129286e-05, 7.159367897915016e-05, 7.145796355155569e-05, 7.132305199907163e-05, 7.118893350203051e-05, 7.105559740787893e-05, 7.092303322856282e-05, 7.079123063795381e-05, 7.066017946931572e-05, 7.052986971281089e-05, 7.040029151304545e-05, 7.027143516665318e-05, 7.0143291119917e-05, 7.001584996642797e-05, 6.988910244478083e-05, 6.976303943630554e-05, 6.96376519628345e-05, 6.951293118450481e-05, 6.938886839759459e-05, 6.926545503239372e-05, 6.914268265110742e-05, 6.902054294579302e-05, 6.889902773632893e-05, 6.877812896841533e-05, 6.865783871160634e-05, 6.853814915737287e-05, 6.841905261719597e-05, 6.830054152068979e-05, 6.81826084137543e-05, 6.806524595675658e-05, 6.794844692274082e-05, 6.78322041956663e-05, 6.771651076867299e-05, 6.760135974237416e-05, 6.748674432317609e-05, 6.737265782162363e-05, 6.725909365077203e-05, 6.714604532458425e-05, 6.703350645635302e-05, 6.69214707571481e-05, 6.680993203428753e-05, 6.669888418983302e-05, 6.658832121910865e-05, 6.647823720924302e-05, 6.636862633773421e-05, 6.625948287103705e-05, 6.615080116317262e-05, 6.604257565435965e-05, 6.593480086966705e-05, 6.582747141768788e-05, 6.572058198923382e-05, 6.561412735605033e-05, 6.55081023695517e-05, 6.5402501959576e-05, 6.529732113315966e-05, 6.519255497333101e-05, 6.508819863792292e-05, 6.49842473584039e-05, 6.48806964387276e-05, 6.477754125420027e-05, 6.46747772503659e-05, 6.457239994190906e-05, 6.447040491157456e-05, 6.436878780910443e-05, 6.426754435019116e-05, 6.416667031544763e-05, 6.406616154939306e-05, 6.396601395945481e-05, 6.386622351498582e-05, 6.376678624629753e-05, 6.366769824370785e-05, 6.356895565660406e-05, 6.347055469252047e-05, 6.337249161623027e-05, 6.327476274885198e-05, 6.317736446696962e-05, 6.308029320176668e-05, 6.298354543817385e-05, 6.288711771402975e-05, 6.279100661925524e-05, 6.269520879504021e-05, 6.259972093304354e-05, 6.250453977460516e-05, 6.240966210997083e-05, 6.231508477752883e-05, 6.222080466305861e-05, 6.21268186989914e-05, 6.20331238636821e-05, 6.193971718069286e-05, 6.184659571808772e-05, 6.175375658773832e-05, 6.166119694464038e-05, 6.156891398624126e-05, 6.147690495177753e-05, 6.13851671216233e-05, 6.129369781664855e-05, 6.120249439758766e-05, 6.111155426441772e-05, 6.102087485574652e-05, 6.093045364821034e-05, 6.084028815588098e-05, 6.075037592968208e-05, 6.066071455681475e-05, 6.057130166019196e-05, 6.048213489788198e-05, 6.039321196256042e-05, 6.030453058097089e-05, 6.021608851339414e-05, 6.0127883553125414e-05, 6.0039913525960104e-05, 5.9952176289687395e-05, 5.986466973359181e-05, 5.977739177796262e-05, 5.9690340373610935e-05, 5.9603513501394263e-05, 5.9516909171748694e-05, 5.943052542422817e-05, 5.934436032705121e-05, 5.92584119766545e-05, 5.9172678497253724e-05, 5.908715804041105e-05, 5.900184878460948e-05, 5.8916748934833864e-05, 5.8831856722158534e-05, 5.87471704033412e-05, 5.8662688260423495e-05, 5.857840860033748e-05, 5.849432975451846e-05, 5.841045007852394e-05, 5.832676795165834e-05, 5.824328177660375e-05, 5.815998997905638e-05, 5.8076891007368795e-05, 5.79939833321976e-05, 5.791126544615682e-05, 5.7828735863476526e-05, 5.7746393119666966e-05, 5.766423577118784e-05, 5.758226239512281e-05, 5.750047158885912e-05, 5.7418861969772114e-05, 5.733743217491487e-05, 5.725618086071256e-05, 5.717510670266165e-05, 5.7094208395033766e-05, 5.7013484650584264e-05, 5.6932934200265306e-05, 5.685255579294344e-05, 5.6772348195121564e-05, 5.669231019066539e-05, 5.661244058053397e-05, 5.6532738182514586e-05, 5.645320183096181e-05, 5.6373830376540495e-05, 5.6294622685972954e-05, 5.6215577641790015e-05, 5.613669414208599e-05, 5.605797110027744e-05, 5.597940744486578e-05, 5.5901002119203595e-05, 5.582275408126452e-05, 5.5744662303416856e-05, 5.56667257722006e-05, 5.558894348810808e-05, 5.551131446536793e-05, 5.543383773173253e-05, 5.535651232826867e-05, 5.527933730915158e-05, 5.5202311741462164e-05, 5.5125434704987364e-05, 5.504870529202366e-05, 5.4972122607183725e-05, 5.4895685767206e-05, 5.481939390076727e-05, 5.4743246148298244e-05, 5.466724166180196e-05, 5.459137960467498e-05, 5.451565915153152e-05, 5.4440079488030226e-05, 5.436463981070368e-05, 5.428933932679057e-05, 5.4214177254070565e-05, 5.413915282070164e-05, 5.4064265265060015e-05, 5.3989513835582665e-05, 5.39148977906121e-05, 5.384041639824387e-05, 5.37660689361761e-05, 5.369185469156169e-05, 5.3617772960862646e-05, 5.354382304970677e-05, 5.3470004272746526e-05, 5.339631595352019e-05, 5.332275742431512e-05, 5.324932802603316e-05, 5.317602710805819e-05, 5.310285402812573e-05, 5.302980815219455e-05, 5.2956888854320346e-05, 5.288409551653134e-05, 5.2811427528705844e-05, 5.2738884288451785e-05, 5.266646520098803e-05, 5.2594169679027574e-05, 5.252199714266268e-05, 5.244994701925166e-05, 5.23780187433075e-05, 5.230621175638823e-05, 5.2234525506988966e-05, 5.216295945043572e-05, 5.209151304878072e-05, 5.20201857706996e-05, 5.194897709138995e-05, 5.1877886492471625e-05, 5.18069134618885e-05, 5.173605749381189e-05, 5.166531808854534e-05, 5.159469475243094e-05, 5.152418699775724e-05, 5.145379434266838e-05, 5.138351631107481e-05, 5.131335243256535e-05, 5.124330224232059e-05, 5.117336528102772e-05, 5.1103541094796636e-05, 5.1033829235077344e-05, 5.0964229258578696e-05, 5.089474072718841e-05, 5.0825363207894264e-05, 5.075609627270657e-05, 5.068693949858191e-05, 5.061789246734794e-05, 5.0548954765629504e-05, 5.048012598477576e-05, 5.041140572078859e-05, 5.034279357425205e-05, 5.027428915026288e-05, 5.020589205836223e-05, 5.01376019124683e-05, 5.006941833081013e-05, 5.0001340935862446e-05, 4.99333693542814e-05, 4.9865503216841445e-05, 4.979774215837314e-05, 4.973008581770188e-05, 4.96625338375877e-05, 4.9595085864665864e-05, 4.9527741549388574e-05, 4.946050054596738e-05, 4.9393362512316646e-05, 4.932632710999785e-05, 4.925939400416471e-05, 4.919256286350921e-05, 4.9125833360208506e-05, 4.9059205169872566e-05, 4.89926779714927e-05, 4.8926251447390875e-05, 4.88599252831698e-05, 4.879369916766381e-05, 4.8727572792890534e-05, 4.866154585400328e-05, 4.859561804924418e-05, 4.852978907989806e-05, 4.846405865024703e-05, 4.8398426467525844e-05, 4.833289224187779e-05, 4.826745568631149e-05, 4.820211651665818e-05, 4.813687445152979e-05, 4.807172921227758e-05, 4.8006680522951515e-05, 4.794172811026019e-05, 4.7876871703531386e-05, 4.781211103467336e-05, 4.774744583813657e-05, 4.768287585087608e-05, 4.761840081231458e-05, 4.755402046430593e-05, 4.748973455109925e-05, 4.7425542819303714e-05, 4.736144501785365e-05, 4.729744089797444e-05, 4.723353021314875e-05, 4.716971271908342e-05, 4.710598817367681e-05, 4.7042356336986605e-05, 4.697881697119829e-05, 4.6915369840593894e-05, 4.6852014711521394e-05, 4.678875135236452e-05, 4.672557953351305e-05, 4.666249902733353e-05, 4.659950960814051e-05, 4.6536611052168225e-05, 4.647380313754259e-05, 4.6411085644253855e-05, 4.6348458354129456e-05, 4.628592105080749e-05, 4.6223473519710395e-05, 4.616111554801926e-05, 4.6098846924648364e-05, 4.603666744022015e-05, 4.597457688704069e-05, 4.591257505907539e-05, 4.5850661751925125e-05, 4.578883676280282e-05, 4.572709989051025e-05, 4.566545093541535e-05, 4.560388969942975e-05, 4.5542415985986744e-05, 4.5481029600019607e-05, 4.541973034794014e-05, 4.535851803761767e-05, 4.529739247835834e-05, 4.52363534808847e-05, 4.5175400857315576e-05, 4.5114534421146404e-05, 4.5053753987229684e-05, 4.499305937175581e-05, 4.4932450392234294e-05, 4.487192686747511e-05, 4.481148861757046e-05, 4.475113546387672e-05, 4.469086722899681e-05, 4.4630683736762674e-05, 4.45705848122181e-05, 4.451057028160188e-05, 4.445063997233112e-05, 4.439079371298482e-05, 4.433103133328781e-05, 4.427135266409484e-05, 4.421175753737488e-05, 4.415224578619582e-05, 4.409281724470927e-05, 4.4033471748135595e-05, 4.397420913274932e-05, 4.3915029235864566e-05, 4.385593189582087e-05, 4.379691695196915e-05, 4.3737984244657894e-05, 4.367913361521956e-05, 4.362036490595724e-05, 4.3561677960131446e-05, 4.3503072621947145e-05, 4.344454873654101e-05, 4.338610614996882e-05, 4.332774470919312e-05, 4.3269464262070996e-05, 4.3211264657342106e-05, 4.315314574461686e-05, 4.309510737436478e-05, 4.303714939790303e-05, 4.2979271667385226e-05, 4.2921474035790226e-05, 4.2863756356911265e-05, 4.28061184853452e-05, 4.274856027648187e-05, 4.26910815864937e-05, 4.2633682272325425e-05, 4.2576362191683994e-05, 4.251912120302855e-05, 4.2461959165560696e-05, 4.240487593921482e-05, 4.2347871384648594e-05, 4.229094536323359e-05, 4.2234097737046086e-05, 4.2177328368858024e-05, 4.212063712212802e-05, 4.2064023860992605e-05, 4.200748845025757e-05, 4.195103075538941e-05, 4.189465064250694e-05, 4.183834797837306e-05, 4.178212263038656e-05, 4.172597446657413e-05, 4.166990335558248e-05, 4.161390916667058e-05, 4.155799176970193e-05, 4.1502151035137125e-05, 4.144638683402638e-05, 4.139069903800222e-05, 4.133508751927231e-05, 4.1279552150612384e-05, 4.1224092805359206e-05, 4.116870935740377e-05, 4.11134016811845e-05, 4.105816965168062e-05, 4.100301314440553e-05, 4.094793203540041e-05, 4.089292620122786e-05, 4.08379955189656e-05, 4.078313986620032e-05, 4.072835912102162e-05, 4.0673653162016e-05, 4.061902186826099e-05, 4.056446511931938e-05, 4.050998279523344e-05, 4.045557477651933e-05, 4.040124094416159e-05, 4.034698117960765e-05, 4.029279536476245e-05, 4.0238683381983174e-05, 4.0184645114074045e-05, 4.013068044428117e-05, 4.007678925628751e-05, 4.002297143420787e-05, 3.996922686258407e-05, 3.991555542638e-05, 3.9861957010976986e-05, 3.980843150216906e-05, 3.9754978786158324e-05, 3.9701598749550454e-05, 3.9648291279350185e-05, 3.959505626295695e-05, 3.9541893588160525e-05, 3.948880314313673e-05, 3.9435784816443256e-05, 3.938283849701552e-05, 3.9329964074162545e-05, 3.927716143756299e-05, 3.922443047726114e-05, 3.917177108366304e-05, 3.911918314753267e-05, 3.906666655998809e-05, 3.9014221212497785e-05, 3.8961846996876954e-05, 3.8909543805283915e-05, 3.8857311530216524e-05, 3.880515006450867e-05, 3.875305930132684e-05, 3.870103913416669e-05, 3.8649089456849686e-05, 3.859721016351985e-05, 3.854540114864043e-05, 3.849366230699076e-05, 3.844199353366307e-05, 3.839039472405938e-05, 3.8338865773888424e-05, 3.828740657916269e-05, 3.8236017036195355e-05, 3.818469704159742e-05, 3.81334464922748e-05, 3.8082265285425496e-05, 3.803115331853678e-05, 3.798011048938242e-05, 3.792913669602001e-05, 3.7878231836788244e-05, 3.78273958103043e-05, 3.777662851546123e-05, 3.77259298514254e-05, 3.7675299717634e-05, 3.762473801379247e-05, 3.7574244639872145e-05, 3.752381949610782e-05, 3.74734624829953e-05, 3.7423173501289145e-05, 3.737295245200031e-05, 3.732279923639391e-05, 3.7272713755986916e-05, 3.722269591254601e-05, 3.717274560808538e-05, 3.712286274486456e-05, 3.707304722538631e-05, 3.702329895239462e-05, 3.6973617828872535e-05, 3.692400375804019e-05, 3.687445664335285e-05, 3.682497638849887e-05, 3.6775562897397814e-05, 3.672621607419854e-05, 3.6676935823277275e-05, 3.662772204923584e-05, 3.657857465689976e-05, 3.6529493551316484e-05, 3.6480478637753594e-05, 3.6431529821697085e-05, 3.638264700884964e-05, 3.63338301051289e-05, 3.628507901666581e-05, 3.623639364980297e-05, 3.618777391109301e-05, 3.613921970729695e-05, 3.60907309453827e-05, 3.604230753252339e-05, 3.599394937609594e-05, 3.5945656383679474e-05, 3.589742846305386e-05, 3.5849265522198224e-05, 3.58011674692895e-05, 3.5753134212701026e-05, 3.5705165661001076e-05, 3.565726172295153e-05, 3.5609422307506474e-05, 3.5561647323810827e-05, 3.5513936681199066e-05, 3.546629028919386e-05, 3.5418708057504784e-05, 3.537118989602707e-05, 3.5323735714840294e-05, 3.5276345424207215e-05, 3.522901893457244e-05, 3.518175615656132e-05, 3.51345570009787e-05, 3.5087421378807753e-05, 3.504034920120883e-05, 3.4993340379518315e-05, 3.4946394825247516e-05, 3.4899512450081487e-05, 3.485269316587801e-05, 3.480593688466648e-05, 3.475924351864684e-05, 3.471261298018851e-05, 3.466604518182941e-05, 3.461954003627484e-05, 3.457309745639656e-05, 3.4526717355231756e-05, 3.448039964598203e-05, 3.443414424201251e-05, 3.4387951056850794e-05, 3.4341820004186096e-05, 3.4295750997868225e-05, 3.424974395190677e-05, 3.420379878047009e-05, 3.415791539788452e-05, 3.411209371863339e-05, 3.4066333657356236e-05, 3.402063512884789e-05, 3.397499804805763e-05, 3.392942233008841e-05, 3.3883907890195946e-05, 3.383845464378795e-05, 3.379306250642333e-05, 3.374773139381139e-05, 3.3702461221810984e-05, 3.3657251906429865e-05, 3.361210336382382e-05, 3.356701551029596e-05, 3.352198826229596e-05, 3.3477021536419354e-05, 3.343211524940675e-05, 3.338726931814319e-05, 3.3342483659657396e-05, 3.329775819112107e-05, 3.325309282984823e-05, 3.320848749329452e-05, 3.3163942099056543e-05, 3.3119456564871155e-05, 3.307503080861491e-05, 3.303066474830329e-05, 3.2986358302090165e-05, 3.2942111388267124e-05, 3.289792392526284e-05, 3.285379583164249e-05, 3.2809727026107086e-05, 3.2765717427492965e-05, 3.272176695477111e-05, 3.267787552704662e-05, 3.2634043063558124e-05, 3.2590269483677186e-05, 3.254655470690775e-05, 3.2502898652885594e-05, 3.245930124137777e-05, 3.241576239228208e-05, 3.237228202562649e-05, 3.232886006156867e-05, 3.228549642039538e-05, 3.2242191022522026e-05, 3.2198943788492134e-05, 3.2155754638976816e-05, 3.2112623494774284e-05, 3.206955027680935e-05, 3.202653490613296e-05, 3.1983577303921676e-05, 3.1940677391477216e-05, 3.189783509022599e-05, 3.185505032171863e-05, 3.1812323007629487e-05, 3.1769653069756246e-05, 3.1727040430019436e-05, 3.168448501046195e-05, 3.164198673324869e-05, 3.1599545520666034e-05, 3.15571612951215e-05, 3.151483397914322e-05, 3.147256349537961e-05, 3.143034976659887e-05, 3.138819271568866e-05, 3.134609226565561e-05, 3.130404833962496e-05, 3.1262060860840164e-05, 3.122012975266247e-05, 3.1178254938570544e-05, 3.113643634216008e-05, 3.1094673887143405e-05, 3.105296749734914e-05, 3.101131709672181e-05, 3.09697226093214e-05, 3.092818395932312e-05, 3.088670107101694e-05, 3.084527386880725e-05, 3.080390227721257e-05, 3.0762586220865085e-05, 3.07213256245104e-05, 3.068012041300714e-05, 3.0638970511326625e-05, 3.0597875844552515e-05, 3.055683633788051e-05, 3.0515851916618015e-05, 3.0474922506183737e-05, 3.0434048032107477e-05, 3.0393228420029733e-05, 3.0352463595701392e-05, 3.0311753484983427e-05, 3.027109801384658e-05, 3.0230497108371062e-05, 3.0189950694746233e-05, 3.0149458699270294e-05, 3.0109021048350023e-05, 3.0068637668500415e-05, 3.0028308486344448e-05, 2.9988033428612764e-05, 2.9947812422143373e-05, 2.9907645393881384e-05, 2.98675322708787e-05, 2.9827472980293773e-05, 2.9787467449391262e-05, 2.974751560554182e-05, 2.9707617376221803e-05, 2.9667772689012972e-05, 2.962798147160224e-05, 2.9588243651781434e-05, 2.954855915744696e-05, 2.9508927916599626e-05, 2.946934985734432e-05, 2.9429824907889777e-05, 2.9390352996548324e-05, 2.9350934051735617e-05, 2.93115680019704e-05, 2.9272254775874226e-05, 2.9232994302171272e-05, 2.9193786509688042e-05, 2.9154631327353127e-05, 2.9115528684196998e-05, 2.9076478509351725e-05, 2.9037480732050762e-05, 2.8998535281628713e-05, 2.8959642087521114e-05, 2.892080107926414e-05, 2.8882012186494465e-05, 2.8843275338948945e-05, 2.880459046646447e-05, 2.876595749897767e-05, 2.8727376366524753e-05, 2.8688846999241242e-05, 2.8650369327361776e-05, 2.861194328121988e-05, 2.857356879124776e-05, 2.853524578797609e-05, 2.8496974202033777e-05, 2.8458753964147792e-05, 2.8420585005142902e-05, 2.8382467255941516e-05, 2.8344400647563453e-05, 2.8306385111125735e-05, 2.8268420577842387e-05, 2.823050697902423e-05, 2.819264424607871e-05, 2.8154832310509642e-05, 2.8117071103917054e-05, 2.8079360557996996e-05, 2.8041700604541305e-05, 2.800409117543744e-05, 2.7966532202668287e-05, 2.7929023618311953e-05, 2.7891565354541598e-05, 2.7854157343625225e-05, 2.78167995179255e-05, 2.7779491809899554e-05, 2.7742234152098836e-05, 2.770502647716886e-05, 2.7667868717849105e-05, 2.7630760806972765e-05, 2.75937026774666e-05, 2.755669426235074e-05, 2.7519735494738523e-05, 2.7482826307836308e-05, 2.744596663494331e-05, 2.7409156409451388e-05, 2.7372395564844926e-05, 2.7335684034700615e-05, 2.729902175268729e-05, 2.7262408652565767e-05, 2.7225844668188676e-05, 2.7189329733500287e-05, 2.7152863782536316e-05, 2.711644674942381e-05, 2.7080078568380932e-05, 2.7043759173716806e-05, 2.7007488499831383e-05, 2.6971266481215227e-05, 2.6935093052449395e-05, 2.6898968148205257e-05, 2.6862891703244333e-05, 2.6826863652418126e-05, 2.679088393066798e-05, 2.6754952473024905e-05, 2.671906921460943e-05, 2.668323409063144e-05, 2.6647447036390023e-05, 2.661170798727331e-05, 2.657601687875831e-05, 2.6540373646410792e-05, 2.6504778225885083e-05, 2.646923055292396e-05, 2.6433730563358465e-05, 2.639827819310776e-05, 2.636287337817901e-05, 2.6327516054667166e-05, 2.629220615875488e-05, 2.6256943626712343e-05, 2.6221728394897097e-05, 2.6186560399753938e-05, 2.615143957781475e-05, 2.611636586569834e-05, 2.6081339200110325e-05, 2.604635951784297e-05, 2.6011426755775055e-05, 2.5976540850871715e-05, 2.5941701740184307e-05, 2.590690936085028e-05, 2.5872163650092998e-05, 2.583746454522165e-05, 2.5802811983631054e-05, 2.5768205902801575e-05, 2.573364624029894e-05, 2.5699132933774122e-05, 2.566466592096319e-05, 2.563024513968718e-05, 2.559587052785196e-05, 2.5561542023448092e-05, 2.5527259564550688e-05, 2.5493023089319286e-05, 2.5458832535997714e-05, 2.5424687842913933e-05, 2.5390588948479943e-05, 2.5356535791191625e-05, 2.5322528309628605e-05, 2.5288566442454135e-05, 2.5254650128414973e-05, 2.5220779306341192e-05, 2.518695391514613e-05, 2.515317389382621e-05, 2.5119439181460816e-05, 2.508574971721219e-05, 2.505210544032525e-05, 2.501850629012753e-05, 2.4984952206028985e-05, 2.495144312752192e-05, 2.4917978994180813e-05, 2.488455974566224e-05, 2.4851185321704712e-05, 2.4817855662128547e-05, 2.4784570706835775e-05, 2.475133039580998e-05, 2.4718134669116193e-05, 2.4684983466900785e-05, 2.4651876729391294e-05, 2.4618814396896353e-05, 2.458579640980553e-05, 2.4552822708589234e-05, 2.451989323379857e-05, 2.448700792606522e-05, 2.4454166726101356e-05, 2.4421369574699456e-05, 2.4388616412732243e-05, 2.4355907181152535e-05, 2.4323241820993115e-05, 2.429062027336665e-05, 2.4258042479465523e-05, 2.4225508380561765e-05, 2.419301791800689e-05, 2.416057103323182e-05, 2.4128167667746708e-05, 2.4095807763140893e-05, 2.406349126108273e-05, 2.4031218103319497e-05, 2.3998988231677262e-05, 2.396680158806078e-05, 2.3934658114453387e-05, 2.3902557752916846e-05, 2.3870500445591268e-05, 2.383848613469499e-05, 2.3806514762524455e-05, 2.3774586271454093e-05, 2.3742700603936218e-05, 2.37108577025009e-05, 2.3679057509755867e-05, 2.3647299968386386e-05, 2.3615585021155143e-05, 2.358391261090214e-05, 2.355228268054459e-05, 2.3520695173076776e-05, 2.3489150031569967e-05, 2.3457647199172292e-05, 2.3426186619108644e-05, 2.3394768234680552e-05, 2.336339198926608e-05, 2.333205782631971e-05, 2.3300765689372248e-05, 2.3269515522030684e-05, 2.323830726797811e-05, 2.3207140870973608e-05, 2.3176016274852125e-05, 2.3144933423524384e-05, 2.3113892260976764e-05, 2.3082892731271167e-05, 2.3051934778544975e-05, 2.302101834701088e-05, 2.2990143380956798e-05, 2.295930982474579e-05, 2.2928517622815888e-05, 2.2897766719680073e-05, 2.2867057059926082e-05, 2.283638858821637e-05, 2.2805761249287977e-05, 2.277517498795241e-05, 2.2744629749095564e-05, 2.27141254776776e-05, 2.2683662118732827e-05, 2.2653239617369642e-05, 2.2622857918770375e-05, 2.259251696819121e-05, 2.2562216710962084e-05, 2.2531957092486568e-05, 2.2501738058241774e-05, 2.247155955377825e-05, 2.244142152471987e-05, 2.2411323916763745e-05, 2.23812666756801e-05, 2.235124974731219e-05, 2.232127307757619e-05, 2.2291336612461087e-05, 2.226144029802859e-05, 2.2231584080413007e-05, 2.2201767905821182e-05, 2.217199172053235e-05, 2.2142255470898067e-05, 2.2112559103342096e-05, 2.2082902564360282e-05, 2.2053285800520515e-05, 2.202370875846257e-05, 2.199417138489803e-05, 2.196467362661019e-05, 2.1935215430453942e-05, 2.1905796743355683e-05, 2.1876417512313227e-05, 2.1847077684395693e-05, 2.1817777206743402e-05, 2.178851602656778e-05, 2.1759294091151286e-05, 2.1730111347847274e-05, 2.1700967744079898e-05, 2.1671863227344057e-05, 2.1642797745205242e-05, 2.161377124529948e-05, 2.1584783675333227e-05, 2.155583498308323e-05, 2.1526925116396503e-05, 2.1498054023190157e-05, 2.146922165145135e-05, 2.1440427949237186e-05, 2.141167286467459e-05, 2.138295634596024e-05, 2.1354278341360468e-05, 2.1325638799211136e-05, 2.1297037667917582e-05, 2.1268474895954498e-05, 2.1239950431865826e-05, 2.12114642242647e-05, 2.11830162218333e-05, 2.1154606373322814e-05, 2.1126234627553288e-05, 2.1097900933413557e-05, 2.106960523986117e-05, 2.1041347495922263e-05, 2.1013127650691473e-05, 2.098494565333186e-05, 2.0956801453074784e-05, 2.0928694999219847e-05, 2.090062624113476e-05, 2.0872595128255297e-05, 2.0844601610085153e-05, 2.0816645636195875e-05, 2.0788727156226788e-05, 2.0760846119884844e-05, 2.07330024769446e-05, 2.070519617724808e-05, 2.0677427170704685e-05, 2.0649695407291127e-05, 2.0622000837051318e-05, 2.0594343410096258e-05, 2.0566723076603993e-05, 2.0539139786819487e-05, 2.0511593491054533e-05, 2.0484084139687675e-05, 2.0456611683164114e-05, 2.04291760719956e-05, 2.0401777256760368e-05, 2.0374415188103016e-05, 2.034708981673446e-05, 2.0319801093431787e-05, 2.0292548969038208e-05, 2.0265333394462953e-05, 2.0238154320681185e-05, 2.0211011698733887e-05, 2.0183905479727808e-05, 2.0156835614835357e-05, 2.0129802055294515e-05, 2.010280475240873e-05, 2.0075843657546865e-05, 2.004891872214306e-05, 2.002202989769669e-05, 1.9995177135772248e-05, 1.9968360387999265e-05, 1.994157960607222e-05, 1.9914834741750456e-05, 1.9888125746858083e-05, 1.9861452573283898e-05, 1.9834815172981285e-05, 1.9808213497968163e-05, 1.978164750032684e-05, 1.9755117132203977e-05, 1.972862234581048e-05, 1.9702163093421403e-05, 1.9675739327375878e-05, 1.964935100007703e-05, 1.9622998063991865e-05, 1.959668047165122e-05, 1.957039817564964e-05, 1.9544151128645322e-05, 1.951793928336e-05, 1.9491762592578884e-05, 1.9465621009150564e-05, 1.9439514485986926e-05, 1.9413442976063056e-05, 1.9387406432417174e-05, 1.9361404808150515e-05, 1.933543805642729e-05, 1.930950613047457e-05, 1.92836089835822e-05, 1.9257746569102724e-05, 1.9231918840451305e-05, 1.920612575110563e-05, 1.918036725460582e-05, 1.9154643304554354e-05, 1.9128953854615994e-05, 1.9103298858517682e-05, 1.9077678270048474e-05, 1.9052092043059436e-05, 1.902654013146359e-05, 1.9001022489235778e-05, 1.897553907041264e-05, 1.895008982909249e-05, 1.892467471943525e-05, 1.8899293695662364e-05, 1.8873946712056704e-05, 1.8848633722962493e-05, 1.882335468278523e-05, 1.8798109545991607e-05, 1.8772898267109416e-05, 1.8747720800727463e-05, 1.8722577101495513e-05, 1.869746712412417e-05, 1.8672390823384818e-05, 1.8647348154109542e-05, 1.862233907119103e-05, 1.85973635295825e-05, 1.857242148429763e-05, 1.8547512890410458e-05, 1.8522637703055285e-05, 1.849779587742665e-05, 1.84729873687792e-05, 1.8448212132427618e-05, 1.842347012374655e-05, 1.8398761298170516e-05, 1.8374085611193865e-05, 1.834944301837061e-05, 1.832483347531445e-05, 1.8300256937698617e-05, 1.8275713361255822e-05, 1.8251202701778182e-05, 1.8226724915117122e-05, 1.8202279957183293e-05, 1.8177867783946527e-05, 1.8153488351435717e-05, 1.8129141615738754e-05, 1.810482753300245e-05, 1.8080546059432453e-05, 1.8056297151293173e-05, 1.8032080764907694e-05, 1.8007896856657705e-05, 1.7983745382983415e-05, 1.7959626300383478e-05, 1.7935539565414914e-05, 1.7911485134693023e-05, 1.788746296489131e-05, 1.7863473012741417e-05, 1.7839515235033034e-05, 1.7815589588613813e-05, 1.779169603038932e-05, 1.7767834517322922e-05, 1.7744005006435724e-05, 1.7720207454806497e-05, 1.7696441819571598e-05, 1.767270805792488e-05, 1.764900612711763e-05, 1.762533598445848e-05, 1.760169758731335e-05, 1.7578090893105342e-05, 1.7554515859314678e-05, 1.7530972443478633e-05, 1.7507460603191432e-05, 1.7483980296104205e-05, 1.7460531479924888e-05, 1.7437114112418153e-05, 1.7413728151405332e-05, 1.7390373554764332e-05, 1.7367050280429585e-05, 1.7343758286391943e-05, 1.7320497530698613e-05, 1.7297267971453086e-05, 1.7274069566815064e-05, 1.7250902275000356e-05, 1.722776605428084e-05, 1.7204660862984365e-05, 1.7181586659494693e-05, 1.71585434022514e-05, 1.7135531049749827e-05, 1.711254956054097e-05, 1.7089598893231453e-05, 1.706667900648341e-05, 1.704378985901444e-05, 1.7020931409597503e-05, 1.6998103617060884e-05, 1.6975306440288085e-05, 1.695253983821776e-05, 1.6929803769843648e-05, 1.6907098194214498e-05, 1.688442307043399e-05, 1.686177835766066e-05, 1.6839164015107837e-05, 1.681658000204355e-05, 1.6794026277790472e-05, 1.677150280172585e-05, 1.67490095332814e-05, 1.6726546431943285e-05, 1.670411345725199e-05, 1.6681710568802286e-05, 1.6659337726243124e-05, 1.663699488927761e-05, 1.6614682017662876e-05, 1.6592399071210048e-05, 1.6570146009784167e-05, 1.6547922793304097e-05, 1.652572938174247e-05, 1.650356573512561e-05, 1.6481431813533458e-05, 1.645932757709951e-05, 1.643725298601072e-05, 1.6415208000507468e-05, 1.639319258088345e-05, 1.6371206687485628e-05, 1.634925028071414e-05, 1.632732332102227e-05, 1.6305425768916316e-05, 1.6283557584955563e-05, 1.6261718729752206e-05, 1.623990916397127e-05, 1.6218128848330527e-05, 1.619637774360045e-05, 1.6174655810604137e-05, 1.6152963010217228e-05, 1.6131299303367838e-05, 1.61096646510365e-05, 1.6088059014256073e-05, 1.606648235411169e-05, 1.604493463174068e-05, 1.60234158083325e-05, 1.6001925845128663e-05, 1.5980464703422664e-05, 1.5959032344559926e-05, 1.5937628729937704e-05, 1.5916253821005048e-05, 1.58949075792627e-05, 1.5873589966263056e-05, 1.585230094361007e-05, 1.58310404729592e-05, 1.5809808516017344e-05, 1.578860503454274e-05, 1.5767429990344945e-05, 1.5746283345284723e-05, 1.5725165061273998e-05, 1.5704075100275778e-05, 1.5683013424304107e-05, 1.5661979995423947e-05, 1.5640974775751164e-05, 1.561999772745244e-05, 1.5599048812745178e-05, 1.5578127993897486e-05, 1.5557235233228062e-05, 1.553637049310615e-05, 1.5515533735951472e-05, 1.549472492423414e-05, 1.5473944020474624e-05, 1.5453190987243652e-05, 1.543246578716215e-05, 1.5411768382901193e-05, 1.5391098737181902e-05, 1.5370456812775422e-05, 1.534984257250282e-05, 1.5329255979235033e-05, 1.530869699589279e-05, 1.5288165585446562e-05, 1.5267661710916483e-05, 1.5247185335372276e-05, 1.5226736421933208e-05, 1.5206314933768015e-05, 1.518592083409482e-05, 1.5165554086181093e-05, 1.5145214653343563e-05, 1.5124902498948158e-05, 1.5104617586409945e-05, 1.508435987919306e-05, 1.5064129340810648e-05, 1.5043925934824775e-05, 1.50237496248464e-05, 1.5003600374535277e-05, 1.498347814759989e-05, 1.4963382907797418e-05, 1.4943314618933639e-05, 1.4923273244862882e-05, 1.4903258749487945e-05, 1.4883271096760061e-05, 1.4863310250678784e-05, 1.4843376175291971e-05, 1.4823468834695701e-05, 1.4803588193034196e-05, 1.4783734214499783e-05, 1.4763906863332801e-05, 1.4744106103821567e-05, 1.472433190030227e-05, 1.4704584217158959e-05, 1.4684863018823431e-05, 1.4665168269775207e-05, 1.4645499934541428e-05, 1.4625857977696835e-05, 1.4606242363863654e-05, 1.4586653057711584e-05, 1.4567090023957701e-05, 1.4547553227366397e-05, 1.4528042632749335e-05, 1.4508558204965358e-05, 1.448909990892046e-05, 1.4469667709567678e-05, 1.4450261571907076e-05, 1.4430881460985656e-05, 1.4411527341897287e-05, 1.439219917978267e-05, 1.437289693982925e-05, 1.4353620587271169e-05, 1.4334370087389186e-05, 1.431514540551064e-05, 1.429594650700936e-05, 1.4276773357305629e-05, 1.4257625921866098e-05, 1.423850416620374e-05, 1.4219408055877774e-05, 1.420033755649362e-05, 1.4181292633702826e-05, 1.4162273253203004e-05, 1.4143279380737776e-05, 1.4124310982096708e-05, 1.4105368023115251e-05, 1.4086450469674671e-05, 1.4067558287702e-05, 1.4048691443169964e-05, 1.4029849902096931e-05, 1.401103363054684e-05, 1.399224259462916e-05, 1.3973476760498783e-05, 1.3954736094356022e-05, 1.393602056244651e-05, 1.3917330131061154e-05, 1.3898664766536073e-05, 1.3880024435252527e-05, 1.3861409103636875e-05, 1.38428187381605e-05, 1.3824253305339762e-05, 1.3805712771735915e-05, 1.3787197103955059e-05, 1.3768706268648102e-05, 1.3750240232510658e-05, 1.3731798962283029e-05, 1.3713382424750096e-05, 1.3694990586741323e-05, 1.367662341513063e-05, 1.3658280876836384e-05, 1.3639962938821318e-05, 1.3621669568092459e-05, 1.3603400731701113e-05, 1.3585156396742748e-05, 1.3566936530356985e-05, 1.3548741099727496e-05, 1.3530570072081975e-05, 1.3512423414692085e-05, 1.3494301094873348e-05, 1.3476203079985165e-05, 1.3458129337430676e-05, 1.3440079834656769e-05, 1.3422054539153973e-05, 1.3404053418456413e-05, 1.3386076440141784e-05, 1.3368123571831232e-05, 1.3350194781189363e-05, 1.3332290035924123e-05, 1.331440930378677e-05, 1.3296552552571836e-05, 1.3278719750117015e-05, 1.3260910864303167e-05, 1.3243125863054201e-05, 1.3225364714337078e-05, 1.3207627386161688e-05, 1.3189913846580836e-05, 1.3172224063690194e-05, 1.3154558005628191e-05, 1.3136915640576021e-05, 1.3119296936757518e-05, 1.3101701862439167e-05, 1.3084130385929992e-05, 1.3066582475581519e-05, 1.304905809978774e-05, 1.3031557226985012e-05, 1.3014079825652044e-05, 1.2996625864309804e-05, 1.2979195311521495e-05, 1.2961788135892473e-05, 1.2944404306070196e-05, 1.2927043790744182e-05, 1.2909706558645928e-05, 1.289239257854889e-05, 1.2875101819268372e-05, 1.2857834249661539e-05, 1.2840589838627292e-05, 1.2823368555106258e-05, 1.2806170368080724e-05, 1.2788995246574566e-05, 1.2771843159653218e-05, 1.2754714076423595e-05, 1.2737607966034031e-05, 1.2720524797674266e-05, 1.270346454057534e-05, 1.2686427164009572e-05, 1.266941263729048e-05, 1.2652420929772753e-05, 1.2635452010852167e-05, 1.2618505849965545e-05, 1.2601582416590714e-05, 1.2584681680246415e-05, 1.2567803610492293e-05, 1.2550948176928798e-05, 1.2534115349197166e-05, 1.2517305096979341e-05, 1.2500517389997926e-05, 1.2483752198016145e-05, 1.2467009490837753e-05, 1.2450289238307033e-05, 1.2433591410308676e-05, 1.2416915976767802e-05, 1.240026290764983e-05, 1.238363217296048e-05, 1.2367023742745702e-05, 1.2350437587091603e-05, 1.2333873676124433e-05, 1.2317331980010486e-05, 1.2300812468956075e-05, 1.2284315113207481e-05, 1.2267839883050872e-05, 1.2251386748812287e-05, 1.2234955680857545e-05, 1.2218546649592226e-05, 1.2202159625461586e-05, 1.218579457895052e-05, 1.2169451480583526e-05, 1.2153130300924607e-05, 1.213683101057727e-05, 1.2120553580184425e-05, 1.2104297980428377e-05, 1.2088064182030733e-05, 1.2071852155752367e-05, 1.2055661872393387e-05, 1.203949330279304e-05, 1.20233464178297e-05, 1.2007221188420782e-05, 1.1991117585522725e-05, 1.1975035580130903e-05, 1.1958975143279592e-05, 1.1942936246041928e-05, 1.1926918859529828e-05, 1.1910922954893969e-05, 1.1894948503323696e-05, 1.1878995476047025e-05, 1.1863063844330535e-05, 1.1847153579479343e-05, 1.1831264652837067e-05, 1.1815397035785742e-05, 1.1799550699745798e-05, 1.178372561617598e-05, 1.1767921756573317e-05, 1.1752139092473076e-05, 1.1736377595448684e-05, 1.1720637237111704e-05, 1.1704917989111764e-05, 1.1689219823136522e-05, 1.1673542710911604e-05, 1.1657886624200555e-05, 1.1642251534804793e-05, 1.1626637414563552e-05, 1.1611044235353844e-05, 1.1595471969090375e-05, 1.1579920587725549e-05, 1.1564390063249363e-05, 1.1548880367689385e-05, 1.153339147311071e-05, 1.1517923351615877e-05, 1.1502475975344868e-05, 1.1487049316475003e-05, 1.1471643347220938e-05, 1.1456258039834578e-05, 1.1440893366605045e-05, 1.1425549299858638e-05, 1.1410225811958756e-05, 1.1394922875305877e-05, 1.1379640462337479e-05, 1.1364378545528025e-05, 1.1349137097388876e-05, 1.1333916090468262e-05, 1.1318715497351248e-05, 1.1303535290659642e-05, 1.1288375443051994e-05, 1.127323592722351e-05, 1.125811671590601e-05, 1.1243017781867903e-05, 1.1227939097914103e-05, 1.1212880636886014e-05, 1.1197842371661445e-05, 1.1182824275154602e-05, 1.1167826320315995e-05, 1.1152848480132424e-05, 1.1137890727626921e-05, 1.1122953035858687e-05, 1.1108035377923072e-05, 1.1093137726951487e-05, 1.1078260056111402e-05, 1.1063402338606258e-05, 1.1048564547675433e-05, 1.1033746656594211e-05, 1.1018948638673698e-05, 1.100417046726082e-05, 1.0989412115738214e-05, 1.0974673557524253e-05, 1.0959954766072934e-05, 1.0945255714873858e-05, 1.09305763774522e-05, 1.0915916727368619e-05, 1.0901276738219256e-05, 1.088665638363564e-05, 1.0872055637284678e-05, 1.08574744728686e-05, 1.0842912864124885e-05, 1.082837078482626e-05, 1.0813848208780605e-05, 1.0799345109830942e-05, 1.078486146185537e-05, 1.0770397238767007e-05, 1.0755952414513988e-05, 1.0741526963079361e-05, 1.0727120858481083e-05, 1.0712734074771941e-05, 1.0698366586039543e-05, 1.0684018366406232e-05, 1.0669689390029059e-05, 1.0655379631099748e-05, 1.064108906384462e-05, 1.0626817662524573e-05, 1.0612565401435014e-05, 1.0598332254905843e-05, 1.0584118197301375e-05, 1.0569923203020297e-05, 1.0555747246495654e-05, 1.0541590302194762e-05, 1.0527452344619192e-05, 1.0513333348304703e-05, 1.0499233287821219e-05, 1.0485152137772756e-05, 1.0471089872797391e-05, 1.0457046467567231e-05, 1.0443021896788327e-05, 1.0429016135200682e-05, 1.0415029157578153e-05, 1.0401060938728435e-05, 1.0387111453493018e-05, 1.0373180676747127e-05, 1.0359268583399692e-05, 1.0345375148393275e-05, 1.0331500346704068e-05, 1.0317644153341806e-05, 1.030380654334974e-05, 1.0289987491804615e-05, 1.0276186973816568e-05, 1.0262404964529144e-05, 1.0248641439119213e-05, 1.023489637279694e-05, 1.0221169740805739e-05, 1.0207461518422213e-05, 1.0193771680956146e-05, 1.018010020375041e-05, 1.0166447062180977e-05, 1.0152812231656808e-05, 1.013919568761988e-05, 1.0125597405545076e-05, 1.0112017360940187e-05, 1.0098455529345854e-05, 1.008491188633551e-05, 1.0071386407515362e-05, 1.0057879068524325e-05, 1.004438984503398e-05, 1.0030918712748554e-05, 1.0017465647404837e-05, 1.0004030624772183e-05, 9.990613620652417e-06, 9.977214610879845e-06, 9.963833571321165e-06, 9.950470477875442e-06, 9.937125306474074e-06, 9.92379803308073e-06, 9.910488633691326e-06, 9.897197084333954e-06, 9.883923361068876e-06, 9.870667439988452e-06, 9.857429297217093e-06, 9.844208908911257e-06, 9.83100625125936e-06, 9.817821300481765e-06, 9.804654032830714e-06, 9.791504424590317e-06, 9.778372452076476e-06, 9.765258091636855e-06, 9.752161319650857e-06, 9.739082112529544e-06, 9.726020446715633e-06, 9.712976298683417e-06, 9.69994964493876e-06, 9.686940462019018e-06, 9.673948726493019e-06, 9.660974414961024e-06, 9.648017504054666e-06, 9.63507797043693e-06, 9.622155790802092e-06, 9.609250941875678e-06, 9.596363400414448e-06, 9.583493143206315e-06, 9.57064014707034e-06, 9.557804388856659e-06, 9.544985845446468e-06, 9.532184493751959e-06, 9.519400310716289e-06, 9.50663327331355e-06, 9.493883358548697e-06, 9.481150543457547e-06, 9.468434805106692e-06, 9.455736120593504e-06, 9.44305446704605e-06, 9.430389821623086e-06, 9.417742161514002e-06, 9.405111463938765e-06, 9.392497706147921e-06, 9.379900865422495e-06, 9.36732091907401e-06, 9.3547578444444e-06, 9.342211618905989e-06, 9.329682219861463e-06, 9.317169624743788e-06, 9.304673811016227e-06, 9.29219475617224e-06, 9.279732437735497e-06, 9.26728683325979e-06, 9.254857920329028e-06, 9.242445676557187e-06, 9.230050079588247e-06, 9.217671107096199e-06, 9.205308736784951e-06, 9.192962946388326e-06, 9.180633713670012e-06, 9.168321016423509e-06, 9.156024832472113e-06, 9.143745139668847e-06, 9.131481915896455e-06, 9.119235139067326e-06, 9.107004787123481e-06, 9.094790838036525e-06, 9.082593269807605e-06, 9.070412060467368e-06, 9.058247188075932e-06, 9.046098630722838e-06, 9.03396636652701e-06, 9.021850373636713e-06, 9.009750630229538e-06, 8.99766711451232e-06, 8.985599804721137e-06, 8.973548679121248e-06, 8.96151371600707e-06, 8.949494893702125e-06, 8.937492190559001e-06, 8.925505584959334e-06, 8.913535055313738e-06, 8.901580580061794e-06, 8.889642137671994e-06, 8.877719706641697e-06, 8.865813265497122e-06, 8.853922792793267e-06, 8.842048267113909e-06, 8.83018966707153e-06, 8.818346971307315e-06, 8.806520158491077e-06, 8.794709207321243e-06, 8.782914096524814e-06, 8.771134804857313e-06, 8.759371311102766e-06, 8.747623594073637e-06, 8.735891632610823e-06, 8.724175405583587e-06, 8.712474891889534e-06, 8.70079007045458e-06, 8.689120920232885e-06, 8.67746742020686e-06, 8.66582954938708e-06, 8.654207286812288e-06, 8.64260061154933e-06, 8.631009502693125e-06, 8.619433939366643e-06, 8.607873900720832e-06, 8.596329365934624e-06, 8.584800314214854e-06, 8.573286724796267e-06, 8.561788576941437e-06, 8.55030584994076e-06, 8.538838523112408e-06, 8.527386575802282e-06, 8.515949987384001e-06, 8.504528737258827e-06, 8.493122804855657e-06, 8.48173216963098e-06, 8.470356811068832e-06, 8.45899670868077e-06, 8.447651842005822e-06, 8.436322190610466e-06, 8.425007734088575e-06, 8.413708452061398e-06, 8.402424324177512e-06, 8.39115533011278e-06, 8.379901449570345e-06, 8.368662662280548e-06, 8.357438948000931e-06, 8.346230286516174e-06, 8.33503665763807e-06, 8.323858041205497e-06, 8.312694417084357e-06, 8.301545765167577e-06, 8.290412065375025e-06, 8.279293297653522e-06, 8.268189441976767e-06, 8.257100478345324e-06, 8.246026386786587e-06, 8.23496714735472e-06, 8.223922740130657e-06, 8.212893145222028e-06, 8.201878342763148e-06, 8.19087831291499e-06, 8.179893035865107e-06, 8.16892249182765e-06, 8.157966661043285e-06, 8.1470255237792e-06, 8.136099060329023e-06, 8.125187251012826e-06, 8.11429007617708e-06, 8.103407516194599e-06, 8.092539551464537e-06, 8.08168616241232e-06, 8.07084732948964e-06, 8.060023033174396e-06, 8.049213253970674e-06, 8.038417972408712e-06, 8.027637169044846e-06, 8.016870824461509e-06, 8.006118919267153e-06, 7.995381434096262e-06, 7.984658349609275e-06, 7.973949646492565e-06, 7.963255305458428e-06, 7.952575307245005e-06, 7.941909632616288e-06, 7.931258262362055e-06, 7.92062117729786e-06, 7.909998358264974e-06, 7.899389786130368e-06, 7.88879544178668e-06, 7.878215306152159e-06, 7.867649360170665e-06, 7.857097584811603e-06, 7.846559961069896e-06, 7.83603646996597e-06, 7.825527092545692e-06, 7.815031809880365e-06, 7.804550603066663e-06, 7.794083453226624e-06, 7.783630341507597e-06, 7.773191249082214e-06, 7.762766157148366e-06, 7.752355046929155e-06, 7.741957899672867e-06, 7.731574696652935e-06, 7.721205419167912e-06, 7.710850048541428e-06, 7.70050856612216e-06, 7.69018095328381e-06, 7.679867191425042e-06, 7.66956726196949e-06, 7.659281146365682e-06, 7.649008826087038e-06, 7.638750282631822e-06, 7.628505497523108e-06, 7.61827445230876e-06, 7.608057128561378e-06, 7.5978535078782865e-06, 7.587663571881485e-06, 7.577487302217615e-06, 7.567324680557948e-06, 7.557175688598325e-06, 7.54704030805914e-06, 7.5369185206853005e-06, 7.526810308246206e-06, 7.516715652535692e-06, 7.5066345353720184e-06, 7.496566938597835e-06, 7.4865128440801325e-06, 7.4764722337102345e-06, 7.466445089403735e-06, 7.4564313931004955e-06, 7.446431126764595e-06, 7.436444272384293e-06, 7.426470811972024e-06, 7.416510727564326e-06, 7.406564001221847e-06, 7.39663061502928e-06, 7.386710551095356e-06, 7.376803791552797e-06, 7.36691031855828e-06, 7.357030114292431e-06, 7.347163160959754e-06, 7.33730944078864e-06, 7.3274689360312925e-06, 7.317641628963741e-06, 7.307827501885767e-06, 7.298026537120896e-06, 7.288238717016369e-06, 7.278464023943087e-06, 7.268702440295611e-06, 7.258953948492101e-06, 7.2492185309743006e-06, 7.239496170207508e-06, 7.229786848680526e-06, 7.2200905489056575e-06, 7.210407253418645e-06, 7.200736944778664e-06, 7.191079605568276e-06, 7.1814352183933986e-06, 7.17180376588329e-06, 7.162185230690488e-06, 7.152579595490813e-06, 7.142986842983307e-06, 7.133406955890226e-06, 7.123839916956989e-06, 7.114285708952158e-06, 7.104744314667413e-06, 7.0952157169175e-06, 7.085699898540228e-06, 7.076196842396408e-06, 7.066706531369854e-06, 7.05722894836732e-06, 7.04776407631849e-06, 7.038311898175948e-06, 7.0288723969151335e-06, 7.019445555534325e-06, 7.010031357054595e-06, 7.000629784519797e-06, 6.991240820996521e-06, 6.98186444957406e-06, 6.972500653364404e-06, 6.9631494155021725e-06, 6.953810719144623e-06, 6.944484547471589e-06, 6.935170883685466e-06, 6.925869711011182e-06, 6.916581012696152e-06, 6.907304772010275e-06, 6.898040972245872e-06, 6.888789596717686e-06, 6.8795506287628245e-06, 6.870324051740749e-06, 6.86110984903324e-06, 6.851908004044363e-06, 6.842718500200443e-06, 6.833541320950027e-06, 6.824376449763874e-06, 6.815223870134897e-06, 6.8060835655781495e-06, 6.796955519630806e-06, 6.787839715852105e-06, 6.778736137823346e-06, 6.76964476914784e-06, 6.760565593450899e-06, 6.751498594379783e-06, 6.742443755603692e-06, 6.7334010608137325e-06, 6.724370493722868e-06, 6.7153520380659246e-06, 6.706345677599531e-06, 6.697351396102098e-06, 6.6883691773738055e-06, 6.679399005236545e-06, 6.670440863533917e-06, 6.6614947361311834e-06, 6.6525606069152505e-06, 6.6436384597946305e-06, 6.634728278699414e-06, 6.625830047581256e-06, 6.61694375041332e-06, 6.608069371190278e-06, 6.599206893928253e-06, 6.5903563026648215e-06, 6.581517581458954e-06, 6.572690714391007e-06, 6.563875685562687e-06, 6.555072479097021e-06, 6.546281079138335e-06, 6.537501469852211e-06, 6.528733635425478e-06, 6.5199775600661645e-06, 6.511233228003477e-06, 6.502500623487786e-06, 6.49377973079057e-06, 6.485070534204413e-06, 6.476373018042955e-06, 6.467687166640887e-06, 6.4590129643538975e-06, 6.450350395558659e-06, 6.441699444652805e-06, 6.433060096054883e-06, 6.42443233420435e-06, 6.415816143561521e-06, 6.407211508607557e-06, 6.398618413844436e-06, 6.390036843794911e-06, 6.381466783002507e-06, 6.372908216031462e-06, 6.3643611274667335e-06, 6.355825501913938e-06, 6.347301323999342e-06, 6.338788578369841e-06, 6.330287249692904e-06, 6.321797322656582e-06, 6.313318781969443e-06, 6.304851612360582e-06, 6.29639579857956e-06, 6.287951325396396e-06, 6.279518177601539e-06, 6.27109634000583e-06, 6.262685797440487e-06, 6.254286534757065e-06, 6.245898536827446e-06, 6.237521788543788e-06, 6.2291562748185184e-06, 6.220801980584302e-06, 6.212458890794005e-06, 6.20412699042068e-06, 6.19580626445753e-06, 6.18749669791788e-06, 6.179198275835164e-06, 6.170910983262882e-06, 6.162634805274584e-06, 6.154369726963833e-06, 6.146115733444191e-06, 6.137872809849179e-06, 6.129640941332257e-06, 6.121420113066802e-06, 6.113210310246069e-06, 6.105011518083176e-06, 6.096823721811068e-06, 6.088646906682504e-06, 6.080481057970011e-06, 6.07232616096587e-06, 6.064182200982095e-06, 6.0560491633503886e-06, 6.047927033422135e-06, 6.039815796568356e-06, 6.031715438179703e-06, 6.023625943666412e-06, 6.015547298458287e-06, 6.007479488004682e-06, 5.999422497774453e-06, 5.991376313255957e-06, 5.983340919957001e-06, 5.975316303404842e-06, 5.9673024491461355e-06, 5.959299342746923e-06, 5.951306969792613e-06, 5.943325315887935e-06, 5.935354366656936e-06, 5.9273941077429336e-06, 5.919444524808502e-06, 5.911505603535451e-06, 5.903577329624784e-06, 5.895659688796692e-06, 5.8877526667905035e-06, 5.8798562493646894e-06, 5.8719704222968075e-06, 5.864095171383494e-06, 5.85623048244044e-06, 5.8483763413023495e-06, 5.840532733822936e-06, 5.8326996458748745e-06, 5.824877063349795e-06, 5.817064972158245e-06, 5.809263358229667e-06, 5.801472207512382e-06, 5.793691505973547e-06, 5.785921239599145e-06, 5.77816139439395e-06, 5.770411956381514e-06, 5.762672911604125e-06, 5.754944246122789e-06, 5.74722594601722e-06, 5.739517997385783e-06, 5.731820386345505e-06, 5.724133099032015e-06, 5.716456121599554e-06, 5.708789440220921e-06, 5.701133041087457e-06, 5.693486910409034e-06, 5.685851034414009e-06, 5.6782253993492135e-06, 5.6706099914799245e-06, 5.663004797089834e-06, 5.65540980248104e-06, 5.647824993973999e-06, 5.640250357907526e-06, 5.632685880638747e-06, 5.625131548543096e-06, 5.6175873480142705e-06, 5.610053265464219e-06, 5.602529287323119e-06, 5.5950154000393385e-06, 5.587511590079429e-06, 5.580017843928084e-06, 5.572534148088133e-06, 5.565060489080498e-06, 5.557596853444183e-06, 5.550143227736245e-06, 5.54269959853177e-06, 5.535265952423853e-06, 5.527842276023558e-06, 5.5204285559599205e-06, 5.513024778879901e-06, 5.5056309314483616e-06, 5.4982470003480666e-06, 5.490872972279623e-06, 5.483508833961489e-06, 5.476154572129925e-06, 5.468810173538981e-06, 5.461475624960479e-06, 5.454150913183976e-06, 5.4468360250167516e-06, 5.439530947283769e-06, 5.4322356668276755e-06, 5.424950170508751e-06, 5.417674445204904e-06, 5.410408477811645e-06, 5.403152255242053e-06, 5.395905764426764e-06, 5.3886689923139374e-06, 5.381441925869244e-06, 5.3742245520758275e-06, 5.367016857934294e-06, 5.359818830462686e-06, 5.352630456696448e-06, 5.3454517236884245e-06, 5.338282618508813e-06, 5.3311231282451615e-06, 5.3239732400023265e-06, 5.316832940902465e-06, 5.3097022180850035e-06, 5.302581058706615e-06, 5.295469449941203e-06, 5.288367378979863e-06, 5.281274833030882e-06, 5.274191799319694e-06, 5.267118265088863e-06, 5.260054217598073e-06, 5.252999644124085e-06, 5.2459545319607325e-06, 5.238918868418882e-06, 5.231892640826422e-06, 5.224875836528239e-06, 5.217868442886186e-06, 5.210870447279072e-06, 5.203881837102627e-06, 5.19690259976949e-06, 5.189932722709181e-06, 5.1829721933680735e-06, 5.176020999209388e-06, 5.169079127713147e-06, 5.162146566376175e-06, 5.155223302712056e-06, 5.148309324251128e-06, 5.1414046185404486e-06, 5.134509173143774e-06, 5.1276229756415485e-06, 5.120746013630863e-06, 5.113878274725451e-06, 5.107019746555649e-06, 5.100170416768394e-06, 5.093330273027183e-06, 5.086499303012056e-06, 5.079677494419585e-06, 5.072864834962834e-06, 5.0660613123713536e-06, 5.0592669143911455e-06, 5.052481628784643e-06, 5.045705443330705e-06, 5.0389383458245635e-06, 5.032180324077834e-06, 5.0254313659184695e-06, 5.018691459190754e-06, 5.011960591755268e-06, 5.005238751488875e-06, 4.9985259262847e-06, 4.991822104052103e-06, 4.985127272716661e-06, 4.978441420220142e-06, 4.97176453452049e-06, 4.965096603591797e-06, 4.95843761542428e-06, 4.951787558024272e-06, 4.945146419414182e-06, 4.9385141876324915e-06, 4.931890850733717e-06, 4.925276396788399e-06, 4.918670813883078e-06, 4.912074090120269e-06, 4.9054862136184515e-06, 4.8989071725120255e-06, 4.892336954951322e-06, 4.88577554910255e-06, 4.8792229431478e-06, 4.872679125285007e-06, 4.866144083727928e-06, 4.859617806706145e-06, 4.853100282465005e-06, 4.846591499265637e-06, 4.840091445384905e-06, 4.833600109115396e-06, 4.827117478765403e-06, 4.8206435426588915e-06, 4.814178289135496e-06, 4.807721706550482e-06, 4.801273783274736e-06, 4.7948345076947425e-06, 4.788403868212555e-06, 4.781981853245788e-06, 4.775568451227586e-06, 4.769163650606614e-06, 4.762767439847016e-06, 4.75637980742842e-06, 4.750000741845899e-06, 4.7436302316099525e-06, 4.7372682652464975e-06, 4.7309148312968325e-06, 4.7245699183176306e-06, 4.718233514880904e-06, 4.711905609574e-06, 4.705586190999567e-06, 4.699275247775536e-06, 4.692972768535115e-06, 4.6866787419267415e-06, 4.680393156614091e-06, 4.674116001276034e-06, 4.667847264606625e-06, 4.661586935315088e-06, 4.655335002125782e-06, 4.649091453778194e-06, 4.642856279026908e-06, 4.636629466641598e-06, 4.630411005406992e-06, 4.624200884122859e-06, 4.617999091603998e-06, 4.6118056166801985e-06, 4.60562044819624e-06, 4.599443575011855e-06, 4.593274986001724e-06, 4.587114670055446e-06, 4.580962616077515e-06, 4.574818812987317e-06, 4.568683249719087e-06, 4.56255591522191e-06, 4.556436798459689e-06, 4.550325888411127e-06, 4.544223174069711e-06, 4.538128644443683e-06, 4.532042288556036e-06, 4.525964095444477e-06, 4.519894054161423e-06, 4.513832153773963e-06, 4.507778383363862e-06, 4.501732732027515e-06, 4.495695188875948e-06, 4.48966574303479e-06, 4.48364438364425e-06, 4.477631099859112e-06, 4.471625880848694e-06, 4.465628715796843e-06, 4.459639593901916e-06, 4.4536585043767506e-06, 4.447685436448659e-06, 4.441720379359394e-06, 4.435763322365143e-06, 4.429814254736499e-06, 4.423873165758443e-06, 4.417940044730334e-06, 4.412014880965872e-06, 4.4060976637931e-06, 4.400188382554364e-06, 4.394287026606313e-06, 4.38839358531986e-06, 4.382508048080181e-06, 4.376630404286687e-06, 4.370760643353003e-06, 4.364898754706956e-06, 4.359044727790547e-06, 4.353198552059944e-06, 4.34736021698545e-06, 4.341529712051489e-06, 4.335707026756595e-06, 4.32989215061338e-06, 4.324085073148526e-06, 4.318285783902754e-06, 4.312494272430822e-06, 4.306710528301492e-06, 4.300934541097511e-06, 4.295166300415608e-06, 4.2894057958664545e-06, 4.283653017074663e-06, 4.277907953678758e-06, 4.272170595331155e-06, 4.26644093169816e-06, 4.260718952459925e-06, 4.255004647310454e-06, 4.24929800595756e-06, 4.243599018122876e-06, 4.237907673541804e-06, 4.232223961963518e-06, 4.226547873150948e-06, 4.22087939688074e-06, 4.215218522943263e-06, 4.209565241142569e-06, 4.203919541296394e-06, 4.198281413236123e-06, 4.192650846806778e-06, 4.1870278318670066e-06, 4.181412358289053e-06, 4.175804415958747e-06, 4.170203994775478e-06, 4.1646110846521915e-06, 4.159025675515352e-06, 4.153447757304936e-06, 4.147877319974418e-06, 4.142314353490737e-06, 4.1367588478343e-06, 4.131210792998941e-06, 4.125670178991916e-06, 4.120136995833889e-06, 4.1146112335589e-06, 4.109092882214365e-06, 4.103581931861033e-06, 4.098078372573e-06, 4.09258219443766e-06, 4.08709338755571e-06, 4.081611942041122e-06, 4.076137848021123e-06, 4.070671095636187e-06, 4.0652116750400026e-06, 4.059759576399476e-06, 4.054314789894691e-06, 4.048877305718903e-06, 4.043447114078525e-06, 4.038024205193098e-06, 4.032608569295286e-06, 4.0272001966308486e-06, 4.021799077458631e-06, 4.01640520205054e-06, 4.011018560691527e-06, 4.00563914367958e-06, 4.000266941325692e-06, 3.994901943953856e-06, 3.989544141901036e-06, 3.984193525517164e-06, 3.978850085165106e-06, 3.973513811220653e-06, 3.968184694072515e-06, 3.9628627241222765e-06, 3.9575478917844076e-06, 3.9522401874862285e-06, 3.946939601667897e-06, 3.941646124782395e-06, 3.936359747295506e-06, 3.931080459685804e-06, 3.925808252444628e-06, 3.920543116076073e-06, 3.915285041096971e-06, 3.910034018036864e-06, 3.904790037438009e-06, 3.8995530898553325e-06, 3.894323165856443e-06, 3.889100256021585e-06, 3.8838843509436485e-06, 3.878675441228133e-06, 3.873473517493137e-06, 3.8682785703693486e-06, 3.863090590500012e-06, 3.857909568540931e-06, 3.852735495160431e-06, 3.84756836103936e-06, 3.842408156871062e-06, 3.837254873361359e-06, 3.832108501228547e-06, 3.826969031203361e-06, 3.8218364540289735e-06, 3.816710760460969e-06, 3.8115919412673303e-06, 3.806479987228425e-06, 3.8013748891369796e-06, 3.7962766377980764e-06, 3.791185224029123e-06, 3.786100638659848e-06, 3.781022872532275e-06, 3.7759519165007074e-06, 3.7708877614317227e-06, 3.7658303982041404e-06, 3.760779817709017e-06, 3.7557360108496226e-06, 3.750698968541431e-06, 3.7456686817120966e-06, 3.7406451413014412e-06, 3.735628338261441e-06, 3.730618263556203e-06, 3.7256149081619585e-06, 3.7206182630670328e-06, 3.715628319271846e-06, 3.7106450677888826e-06, 3.7056684996426796e-06, 3.7006986058698185e-06, 3.6957353775188927e-06, 3.690778805650512e-06, 3.6858288813372644e-06, 3.680885595663719e-06, 3.675948939726398e-06, 3.6710189046337637e-06, 3.666095481506208e-06, 3.6611786614760255e-06, 3.6562684356874113e-06, 3.65136479529643e-06, 3.6464677314710114e-06, 3.6415772353909312e-06, 3.636693298247791e-06, 3.6318159112450114e-06, 3.6269450655978027e-06, 3.622080752533166e-06, 3.6172229632898617e-06, 3.6123716891184013e-06, 3.607526921281035e-06, 3.602688651051726e-06, 3.597856869716145e-06, 3.5930315685716476e-06, 3.5882127389272635e-06, 3.5834003721036755e-06, 3.5785944594332055e-06, 3.573794992259807e-06, 3.569001961939034e-06, 3.5642153598380413e-06, 3.5594351773355565e-06, 3.554661405821874e-06, 3.5498940366988323e-06, 3.545133061379801e-06, 3.5403784712896683e-06, 3.5356302578648194e-06, 3.530888412553131e-06, 3.526152926813942e-06, 3.521423792118051e-06, 3.516700999947693e-06, 3.5119845417965262e-06, 3.507274409169622e-06, 3.502570593583437e-06, 3.497873086565813e-06, 3.4931818796559507e-06, 3.488496964404397e-06, 3.483818332373034e-06, 3.479145975135058e-06, 3.47447988427497e-06, 3.469820051388552e-06, 3.465166468082866e-06, 3.4605191259762215e-06, 3.4558780166981734e-06, 3.451243131889505e-06, 3.4466144632022035e-06, 3.441992002299461e-06, 3.437375740855643e-06, 3.4327656705562865e-06, 3.428161783098076e-06, 3.4235640701888325e-06, 3.4189725235475002e-06, 3.414387134904127e-06, 3.409807895999855e-06, 3.405234798586899e-06, 3.4006678344285397e-06, 3.3961069952991018e-06, 3.3915522729839405e-06, 3.387003659279433e-06, 3.382461145992953e-06, 3.377924724942868e-06, 3.3733943879585137e-06, 3.368870126880184e-06, 3.36435193355912e-06, 3.3598397998574875e-06, 3.3553337176483712e-06, 3.350833678815749e-06, 3.3463396752544894e-06, 3.3418516988703285e-06, 3.337369741579856e-06, 3.3328937953105087e-06, 3.328423852000543e-06, 3.3239599035990346e-06, 3.319501942065849e-06, 3.3150499593716434e-06, 3.310603947497836e-06, 3.3061638984366013e-06, 3.3017298041908575e-06, 3.2973016567742426e-06, 3.292879448211111e-06, 3.2884631705365072e-06, 3.284052815796166e-06, 3.279648376046483e-06, 3.27524984335451e-06, 3.27085720979794e-06, 3.2664704674650885e-06, 3.262089608454884e-06, 3.2577146248768484e-06, 3.253345508851091e-06, 3.2489822525082834e-06, 3.244624847989655e-06, 3.240273287446976e-06, 3.2359275630425367e-06, 3.231587666949147e-06, 3.227253591350109e-06, 3.2229253284392066e-06, 3.2186028704207e-06, 3.2142862095092978e-06, 3.209975337930155e-06, 3.2056702479188493e-06, 3.201370931721376e-06, 3.1970773815941274e-06, 3.1927895898038804e-06, 3.188507548627787e-06, 3.1842312503533517e-06, 3.179960687278428e-06, 3.1756958517111937e-06, 3.1714367359701494e-06, 3.1671833323840918e-06, 3.162935633292106e-06, 3.158693631043557e-06, 3.1544573179980654e-06, 3.150226686525502e-06, 3.1460017290059668e-06, 3.1417824378297847e-06, 3.1375688053974826e-06, 3.133360824119778e-06, 3.1291584864175737e-06, 3.1249617847219287e-06, 3.1207707114740606e-06, 3.11658525912532e-06, 3.112405420137182e-06, 3.1082311869812343e-06, 3.104062552139157e-06, 3.0998995081027206e-06, 3.0957420473737575e-06, 3.0915901624641625e-06, 3.087443845895869e-06, 3.083303090200841e-06, 3.0791678879210604e-06, 3.0750382316085077e-06, 3.070914113825156e-06, 3.0667955271429503e-06, 3.0626824641438027e-06, 3.0585749174195704e-06, 3.0544728795720443e-06, 3.050376343212944e-06, 3.0462853009638913e-06, 3.0421997454564086e-06, 3.038119669331896e-06, 3.0340450652416284e-06, 3.0299759258467314e-06, 3.025912243818174e-06, 3.0218540118367576e-06, 3.0178012225930956e-06, 3.0137538687876096e-06, 3.009711943130505e-06, 3.0056754383417705e-06, 3.0016443471511533e-06, 2.997618662298152e-06, 2.993598376532006e-06, 2.989583482611675e-06, 2.985573973305833e-06, 2.9815698413928515e-06, 2.977571079660785e-06, 2.973577680907366e-06, 2.9695896379399795e-06, 2.965606943575663e-06, 2.9616295906410825e-06, 2.9576575719725298e-06, 2.9536908804158997e-06, 2.9497295088266828e-06, 2.9457734500699546e-06, 2.941822697020355e-06, 2.9378772425620857e-06, 2.933937079588885e-06, 2.9300022010040303e-06, 2.9260725997203097e-06, 2.9221482686600167e-06, 2.9182292007549434e-06, 2.9143153889463545e-06, 2.910406826184986e-06, 2.9065035054310254e-06, 2.902605419654104e-06, 2.8987125618332805e-06, 2.894824924957029e-06, 2.8909425020232307e-06, 2.887065286039152e-06, 2.883193270021445e-06, 2.8793264469961198e-06, 2.8754648099985468e-06, 2.8716083520734326e-06, 2.867757066274811e-06, 2.8639109456660377e-06, 2.8600699833197643e-06, 2.856234172317938e-06, 2.852403505751783e-06, 2.848577976721786e-06, 2.844757578337693e-06, 2.8409423037184847e-06, 2.837132145992377e-06, 2.8333270982967947e-06, 2.8295271537783728e-06, 2.8257323055929346e-06, 2.821942546905481e-06, 2.8181578708901844e-06, 2.8143782707303663e-06, 2.8106037396184958e-06, 2.8068342707561675e-06, 2.8030698573540968e-06, 2.799310492632103e-06, 2.7955561698190983e-06, 2.7918068821530786e-06, 2.7880626228811046e-06, 2.7843233852592986e-06, 2.7805891625528232e-06, 2.7768599480358774e-06, 2.773135734991677e-06, 2.7694165167124465e-06, 2.7657022864994107e-06, 2.7619930376627724e-06, 2.7582887635217125e-06, 2.7545894574043676e-06, 2.750895112647823e-06, 2.7472057225981026e-06, 2.743521280610151e-06, 2.739841780047828e-06, 2.7361672142838907e-06, 2.7324975766999865e-06, 2.7288328606866386e-06, 2.725173059643232e-06, 2.7215181669780094e-06, 2.7178681761080484e-06, 2.714223080459261e-06, 2.71058287346637e-06, 2.706947548572909e-06, 2.703317099231201e-06, 2.699691518902351e-06, 2.6960708010562372e-06, 2.6924549391714896e-06, 2.688843926735491e-06, 2.685237757244353e-06, 2.6816364242029142e-06, 2.678039921124722e-06, 2.674448241532023e-06, 2.670861378955753e-06, 2.6672793269355217e-06, 2.6637020790196067e-06, 2.6601296287649335e-06, 2.6565619697370746e-06, 2.6529990955102277e-06, 2.649440999667208e-06, 2.6458876757994414e-06, 2.642339117506945e-06, 2.638795318398322e-06, 2.6352562720907454e-06, 2.631721972209948e-06, 2.628192412390215e-06, 2.624667586274365e-06, 2.621147487513746e-06, 2.617632109768218e-06, 2.6141214467061457e-06, 2.6106154920043854e-06, 2.607114239348271e-06, 2.603617682431611e-06, 2.6001258149566654e-06, 2.5966386306341456e-06, 2.5931561231831935e-06, 2.589678286331379e-06, 2.5862051138146807e-06, 2.582736599377479e-06, 2.579272736772547e-06, 2.5758135197610316e-06, 2.5723589421124513e-06, 2.5689089976046773e-06, 2.5654636800239297e-06, 2.5620229831647575e-06, 2.5585869008300346e-06, 2.5551554268309474e-06, 2.55172855498698e-06, 2.5483062791259075e-06, 2.544888593083782e-06, 2.5414754907049224e-06, 2.5380669658419044e-06, 2.5346630123555465e-06, 2.531263624114903e-06, 2.527868794997248e-06, 2.524478518888071e-06, 2.5210927896810597e-06, 2.5177116012780886e-06, 2.514334947589217e-06, 2.5109628225326666e-06, 2.5075952200348194e-06, 2.5042321340301984e-06, 2.5008735584614674e-06, 2.4975194872794094e-06, 2.4941699144429213e-06, 2.4908248339190038e-06, 2.4874842396827463e-06, 2.4841481257173226e-06, 2.48081648601397e-06, 2.477489314571991e-06, 2.4741666053987313e-06, 2.4708483525095744e-06, 2.4675345499279334e-06, 2.4642251916852324e-06, 2.460920271820904e-06, 2.4576197843823716e-06, 2.4543237234250463e-06, 2.4510320830123077e-06, 2.447744857215498e-06, 2.444462040113914e-06, 2.441183625794788e-06, 2.437909608353288e-06, 2.4346399818924958e-06, 2.431374740523405e-06, 2.428113878364908e-06, 2.4248573895437814e-06, 2.4216052681946823e-06, 2.4183575084601316e-06, 2.415114104490508e-06, 2.4118750504440346e-06, 2.4086403404867677e-06, 2.4054099687925908e-06, 2.4021839295431985e-06, 2.398962216928091e-06, 2.395744825144559e-06, 2.392531748397678e-06, 2.3893229809002936e-06, 2.3861185168730125e-06, 2.3829183505441945e-06, 2.3797224761499384e-06, 2.376530887934075e-06, 2.3733435801481523e-06, 2.370160547051431e-06, 2.3669817829108693e-06, 2.363807282001114e-06, 2.360637038604492e-06, 2.3574710470109966e-06, 2.3543093015182815e-06, 2.351151796431645e-06, 2.3479985260640274e-06, 2.3448494847359923e-06, 2.3417046667757206e-06, 2.3385640665190023e-06, 2.3354276783092224e-06, 2.332295496497353e-06, 2.3291675154419425e-06, 2.3260437295091028e-06, 2.3229241330725073e-06, 2.3198087205133685e-06, 2.3166974862204414e-06, 2.31359042459e-06, 2.31048753002584e-06, 2.3073887969392576e-06, 2.3042942197490452e-06, 2.3012037928814838e-06, 2.2981175107703255e-06, 2.2950353678567913e-06, 2.291957358589553e-06, 2.2888834774247336e-06, 2.2858137188258853e-06, 2.282748077263988e-06, 2.2796865472174393e-06, 2.2766291231720373e-06, 2.273575799620981e-06, 2.270526571064849e-06, 2.2674814320116017e-06, 2.2644403769765598e-06, 2.2614034004824023e-06, 2.2583704970591554e-06, 2.2553416612441776e-06, 2.252316887582159e-06, 2.249296170625101e-06, 2.246279504932314e-06, 2.2432668850704065e-06, 2.24025830561327e-06, 2.2372537611420792e-06, 2.23425324624527e-06, 2.2312567555185414e-06, 2.2282642835648368e-06, 2.225275824994339e-06, 2.2222913744244606e-06, 2.2193109264798307e-06, 2.2163344757922914e-06, 2.2133620170008792e-06, 2.210393544751826e-06, 2.2074290536985397e-06, 2.2044685385016e-06, 2.2015119938287495e-06, 2.1985594143548792e-06, 2.1956107947620253e-06, 2.1926661297393522e-06, 2.1897254139831517e-06, 2.1867886421968247e-06, 2.1838558090908765e-06, 2.180926909382909e-06, 2.1780019377976046e-06, 2.1750808890667256e-06, 2.1721637579290946e-06, 2.169250539130595e-06, 2.166341227424154e-06, 2.163435817569735e-06, 2.160534304334333e-06, 2.1576366824919573e-06, 2.15474294682363e-06, 2.1518530921173685e-06, 2.1489671131681827e-06, 2.146085004778064e-06, 2.1432067617559726e-06, 2.1403323789178335e-06, 2.137461851086521e-06, 2.1345951730918578e-06, 2.1317323397705954e-06, 2.1288733459664123e-06, 2.1260181865299027e-06, 2.1231668563185664e-06, 2.1203193501968017e-06, 2.1174756630358913e-06, 2.1146357897140002e-06, 2.11179972511616e-06, 2.1089674641342613e-06, 2.10613900166705e-06, 2.1033143326201075e-06, 2.100493451905854e-06, 2.0976763544435275e-06, 2.0948630351591845e-06, 2.0920534889856834e-06, 2.0892477108626784e-06, 2.086445695736613e-06, 2.083647438560706e-06, 2.080852934294947e-06, 2.078062177906082e-06, 2.075275164367609e-06, 2.0724918886597686e-06, 2.069712345769531e-06, 2.066936530690593e-06, 2.0641644384233615e-06, 2.0613960639749532e-06, 2.058631402359178e-06, 2.0558704485965323e-06, 2.053113197714195e-06, 2.0503596447460095e-06, 2.0476097847324836e-06, 2.0448636127207736e-06, 2.0421211237646806e-06, 2.039382312924638e-06, 2.0366471752677025e-06, 2.0339157058675513e-06, 2.031187899804463e-06, 2.0284637521653196e-06, 2.0257432580435883e-06, 2.0230264125393186e-06, 2.0203132107591315e-06, 2.0176036478162096e-06, 2.014897718830292e-06, 2.0121954189276604e-06, 2.0094967432411347e-06, 2.006801686910061e-06, 2.004110245080307e-06, 2.0014224129042466e-06, 1.9987381855407572e-06, 1.996057558155211e-06, 1.9933805259194596e-06, 1.9907070840118345e-06, 1.9880372276171316e-06, 1.9853709519266026e-06, 1.982708252137954e-06, 1.9800491234553277e-06, 1.977393561089302e-06, 1.974741560256873e-06, 1.972093116181458e-06, 1.9694482240928756e-06, 1.9668068792273437e-06, 1.96416907682747e-06, 1.9615348121422412e-06, 1.9589040804270172e-06, 1.956276876943519e-06, 1.953653196959826e-06, 1.9510330357503604e-06, 1.9484163885958834e-06, 1.9458032507834866e-06, 1.94319361760658e-06, 1.940587484364889e-06, 1.9379848463644387e-06, 1.9353856989175545e-06, 1.9327900373428448e-06, 1.9301978569651966e-06, 1.9276091531157704e-06, 1.9250239211319847e-06, 1.922442156357514e-06, 1.9198638541422757e-06, 1.9172890098424235e-06, 1.9147176188203423e-06, 1.9121496764446322e-06, 1.909585178090109e-06, 1.907024119137789e-06, 1.9044664949748845e-06, 1.901912300994794e-06, 1.8993615325970926e-06, 1.8968141851875292e-06, 1.8942702541780092e-06, 1.8917297349865963e-06, 1.889192623037496e-06, 1.8866589137610521e-06, 1.8841286025937359e-06, 1.8816016849781417e-06, 1.8790781563629726e-06, 1.8765580122030381e-06, 1.8740412479592404e-06, 1.871527859098576e-06, 1.8690178410941137e-06, 1.8665111894249978e-06, 1.8640078995764337e-06, 1.861507967039681e-06, 1.8590113873120516e-06, 1.856518155896891e-06, 1.8540282683035768e-06, 1.8515417200475094e-06, 1.8490585066501019e-06, 1.8465786236387796e-06, 1.8441020665469602e-06, 1.8416288309140543e-06, 1.8391589122854534e-06, 1.836692306212527e-06, 1.8342290082526076e-06, 1.8317690139689863e-06, 1.8293123189309055e-06, 1.8268589187135478e-06, 1.824408808898035e-06, 1.821961985071411e-06, 1.8195184428266395e-06, 1.8170781777625942e-06, 1.8146411854840507e-06, 1.812207461601684e-06, 1.8097770017320503e-06, 1.807349801497587e-06, 1.8049258565266026e-06, 1.8025051624532662e-06, 1.800087714917608e-06, 1.7976735095655005e-06, 1.7952625420486577e-06, 1.7928548080246224e-06, 1.7904503031567679e-06, 1.7880490231142776e-06, 1.7856509635721456e-06, 1.783256120211166e-06, 1.7808644887179238e-06, 1.7784760647847947e-06, 1.776090844109926e-06, 1.7737088223972365e-06, 1.7713299953564068e-06, 1.7689543587028694e-06, 1.7665819081578083e-06, 1.764212639448141e-06, 1.761846548306518e-06, 1.7594836304713103e-06, 1.7571238816866108e-06, 1.7547672977022144e-06, 1.7524138742736182e-06, 1.750063607162011e-06, 1.7477164921342665e-06, 1.745372524962939e-06, 1.7430317014262481e-06, 1.740694017308078e-06, 1.738359468397966e-06, 1.736028050491096e-06, 1.7336997593882948e-06, 1.7313745908960173e-06, 1.7290525408263432e-06, 1.7267336049969695e-06, 1.7244177792312008e-06, 1.722105059357947e-06, 1.7197954412117094e-06, 1.7174889206325758e-06, 1.715185493466212e-06, 1.7128851555638597e-06, 1.7105879027823215e-06, 1.7082937309839564e-06, 1.7060026360366739e-06, 1.7037146138139235e-06, 1.7014296601946928e-06, 1.6991477710634935e-06, 1.6968689423103563e-06, 1.6945931698308258e-06, 1.692320449525949e-06, 1.6900507773022743e-06, 1.6877841490718364e-06, 1.685520560752154e-06, 1.683260008266219e-06, 1.6810024875424965e-06, 1.6787479945149075e-06, 1.6764965251228267e-06, 1.674248075311076e-06, 1.6720026410299142e-06, 1.6697602182350355e-06, 1.6675208028875541e-06, 1.6652843909540025e-06, 1.6630509784063224e-06, 1.6608205612218561e-06, 1.658593135383346e-06, 1.6563686968789177e-06, 1.6541472417020786e-06, 1.6519287658517095e-06, 1.6497132653320558e-06, 1.6475007361527266e-06, 1.645291174328678e-06, 1.6430845758802123e-06, 1.6408809368329677e-06, 1.6386802532179171e-06, 1.6364825210713519e-06, 1.634287736434881e-06, 1.632095895355423e-06, 1.629906993885195e-06, 1.6277210280817147e-06, 1.6255379940077822e-06, 1.623357887731479e-06, 1.6211807053261615e-06, 1.6190064428704493e-06, 1.6168350964482263e-06, 1.6146666621486235e-06, 1.6125011360660198e-06, 1.6103385143000295e-06, 1.6081787929555028e-06, 1.6060219681425095e-06, 1.6038680359763378e-06, 1.6017169925774857e-06, 1.5995688340716534e-06, 1.597423556589741e-06, 1.5952811562678333e-06, 1.5931416292471991e-06, 1.5910049716742819e-06, 1.5888711797006927e-06, 1.5867402494832069e-06, 1.5846121771837512e-06, 1.5824869589694004e-06, 1.5803645910123708e-06, 1.5782450694900097e-06, 1.576128390584796e-06, 1.5740145504843251e-06, 1.5719035453813054e-06, 1.569795371473551e-06, 1.567690024963979e-06, 1.5655875020605955e-06, 1.5634877989764932e-06, 1.5613909119298437e-06, 1.5592968371438892e-06, 1.5572055708469414e-06, 1.5551171092723663e-06, 1.5530314486585827e-06, 1.5509485852490548e-06, 1.548868515292283e-06, 1.5467912350418032e-06, 1.5447167407561725e-06, 1.5426450286989664e-06, 1.5405760951387706e-06, 1.538509936349179e-06, 1.5364465486087796e-06, 1.5343859282011522e-06, 1.5323280714148617e-06, 1.5302729745434483e-06, 1.5282206338854281e-06, 1.5261710457442768e-06, 1.5241242064284293e-06, 1.5220801122512718e-06, 1.520038759531133e-06, 1.5180001445912834e-06, 1.5159642637599207e-06, 1.5139311133701686e-06, 1.5119006897600682e-06, 1.5098729892725713e-06, 1.5078480082555372e-06, 1.5058257430617203e-06, 1.5038061900487678e-06, 1.50178934557921e-06, 1.4997752060204602e-06, 1.497763767744799e-06, 1.4957550271293743e-06, 1.4937489805561923e-06, 1.4917456244121107e-06, 1.4897449550888364e-06, 1.487746968982912e-06, 1.4857516624957142e-06, 1.483759032033446e-06, 1.4817690740071288e-06, 1.4797817848326016e-06, 1.477797160930506e-06, 1.4758151987262858e-06, 1.473835894650179e-06, 1.4718592451372093e-06, 1.4698852466271857e-06, 1.4679138955646883e-06, 1.4659451883990671e-06, 1.463979121584432e-06, 1.4620156915796534e-06, 1.4600548948483463e-06, 1.4580967278588702e-06, 1.4561411870843204e-06, 1.4541882690025224e-06, 1.4522379700960278e-06, 1.4502902868521033e-06, 1.4483452157627269e-06, 1.4464027533245821e-06, 1.444462896039049e-06, 1.4425256404122043e-06, 1.4405909829548066e-06, 1.4386589201822954e-06, 1.436729448614782e-06, 1.4348025647770494e-06, 1.4328782651985366e-06, 1.4309565464133388e-06, 1.4290374049601999e-06, 1.4271208373825046e-06, 1.4252068402282766e-06, 1.4232954100501662e-06, 1.4213865434054483e-06, 1.4194802368560145e-06, 1.4175764869683664e-06, 1.415675290313615e-06, 1.413776643467465e-06, 1.411880543010216e-06, 1.4099869855267532e-06, 1.4080959676065412e-06, 1.406207485843622e-06, 1.4043215368366024e-06, 1.4024381171886516e-06, 1.4005572235074938e-06, 1.3986788524054058e-06, 1.3968030004992052e-06, 1.3949296644102475e-06, 1.3930588407644193e-06, 1.3911905261921317e-06, 1.3893247173283188e-06, 1.3874614108124232e-06, 1.3856006032883972e-06, 1.3837422914046924e-06, 1.3818864718142555e-06, 1.3800331411745254e-06, 1.3781822961474197e-06, 1.376333933399335e-06, 1.3744880496011368e-06, 1.3726446414281597e-06, 1.3708037055601934e-06, 1.3689652386814815e-06, 1.3671292374807148e-06, 1.3652956986510242e-06, 1.3634646188899785e-06, 1.3616359948995726e-06, 1.3598098233862255e-06, 1.3579861010607734e-06, 1.3561648246384626e-06, 1.3543459908389487e-06, 1.3525295963862828e-06, 1.3507156380089106e-06, 1.348904112439666e-06, 1.347095016415763e-06, 1.345288346678796e-06, 1.3434840999747244e-06, 1.3416822730538742e-06, 1.3398828626709282e-06, 1.338085865584925e-06, 1.3362912785592462e-06, 1.3344990983616164e-06, 1.332709321764094e-06, 1.330921945543066e-06, 1.329136966479246e-06, 1.3273543813576622e-06, 1.3255741869676553e-06, 1.3237963801028719e-06, 1.322020957561258e-06, 1.3202479161450576e-06, 1.3184772526607994e-06, 1.3167089639192972e-06, 1.3149430467356394e-06, 1.313179497929191e-06, 1.3114183143235778e-06, 1.3096594927466879e-06, 1.307903030030663e-06, 1.3061489230118923e-06, 1.304397168531012e-06, 1.3026477634328912e-06, 1.3009007045666323e-06, 1.2991559887855633e-06, 1.2974136129472315e-06, 1.2956735739134015e-06, 1.2939358685500442e-06, 1.2922004937273346e-06, 1.2904674463196447e-06, 1.288736723205538e-06, 1.2870083212677676e-06, 1.2852822373932639e-06, 1.2835584684731335e-06, 1.2818370114026516e-06, 1.2801178630812602e-06, 1.278401020412557e-06, 1.276686480304293e-06, 1.2749742396683667e-06, 1.2732642954208167e-06, 1.2715566444818219e-06, 1.2698512837756875e-06, 1.2681482102308452e-06, 1.266447420779846e-06, 1.2647489123593543e-06, 1.2630526819101454e-06, 1.261358726377095e-06, 1.2596670427091763e-06, 1.2579776278594544e-06, 1.2562904787850834e-06, 1.2546055924472956e-06, 1.2529229658113992e-06, 1.251242595846773e-06, 1.2495644795268588e-06, 1.2478886138291614e-06, 1.246214995735235e-06, 1.2445436222306845e-06, 1.2428744903051561e-06, 1.2412075969523332e-06, 1.2395429391699338e-06, 1.2378805139597002e-06, 1.2362203183273957e-06, 1.2345623492828005e-06, 1.2329066038397027e-06, 1.2312530790159002e-06, 1.2296017718331859e-06, 1.2279526793173493e-06, 1.2263057984981664e-06, 1.2246611264094008e-06, 1.2230186600887912e-06, 1.2213783965780494e-06, 1.2197403329228559e-06, 1.2181044661728512e-06, 1.2164707933816366e-06, 1.2148393116067617e-06, 1.2132100179097232e-06, 1.2115829093559594e-06, 1.2099579830148421e-06, 1.208335235959678e-06, 1.2067146652676952e-06, 1.2050962680200426e-06, 1.2034800413017842e-06, 1.2018659822018917e-06, 1.200254087813245e-06, 1.198644355232619e-06, 1.1970367815606836e-06, 1.1954313639019963e-06, 1.1938280993650003e-06, 1.1922269850620146e-06, 1.1906280181092318e-06, 1.189031195626712e-06, 1.1874365147383766e-06, 1.1858439725720075e-06, 1.1842535662592356e-06, 1.1826652929355403e-06, 1.181079149740242e-06, 1.1794951338164971e-06, 1.1779132423112962e-06, 1.1763334723754539e-06, 1.1747558211636062e-06, 1.1731802858342046e-06, 1.1716068635495147e-06, 1.1700355514756047e-06, 1.1684663467823446e-06, 1.1668992466434003e-06, 1.165334248236227e-06, 1.1637713487420682e-06, 1.1622105453459455e-06, 1.160651835236656e-06, 1.1590952156067676e-06, 1.1575406836526122e-06, 1.1559882365742853e-06, 1.1544378715756338e-06, 1.1528895858642561e-06, 1.1513433766514957e-06, 1.1497992411524349e-06, 1.1482571765858943e-06, 1.1467171801744217e-06, 1.14517924914429e-06, 1.143643380725492e-06, 1.142109572151738e-06, 1.1405778206604457e-06, 1.1390481234927388e-06, 1.137520477893441e-06, 1.13599488111107e-06, 1.1344713303978366e-06, 1.1329498230096348e-06, 1.1314303562060383e-06, 1.1299129272502974e-06, 1.1283975334093313e-06, 1.126884171953728e-06, 1.1253728401577324e-06, 1.123863535299247e-06, 1.1223562546598234e-06, 1.120850995524662e-06, 1.1193477551826022e-06, 1.1178465309261195e-06, 1.1163473200513204e-06, 1.1148501198579378e-06, 1.113354927649328e-06, 1.1118617407324622e-06, 1.110370556417923e-06, 1.108881372019901e-06, 1.1073941848561869e-06, 1.105908992248173e-06, 1.1044257915208397e-06, 1.102944580002757e-06, 1.1014653550260771e-06, 1.09998811392653e-06, 1.0985128540434204e-06, 1.0970395727196204e-06, 1.0955682673015648e-06, 1.0940989351392476e-06, 1.0926315735862196e-06, 1.0911661799995777e-06, 1.0897027517399642e-06, 1.0882412861715613e-06, 1.086781780662086e-06, 1.0853242325827873e-06, 1.0838686393084377e-06, 1.0824149982173315e-06, 1.0809633066912786e-06, 1.0795135621156002e-06, 1.0780657618791259e-06, 1.0766199033741854e-06, 1.0751759839966064e-06, 1.0737340011457083e-06, 1.0722939522243009e-06, 1.0708558346386748e-06, 1.0694196457986003e-06, 1.0679853831173208e-06, 1.066553044011549e-06, 1.0651226259014643e-06, 1.0636941262107036e-06, 1.06226754236636e-06, 1.0608428717989768e-06, 1.0594201119425433e-06, 1.0579992602344925e-06, 1.0565803141156915e-06, 1.0551632710304404e-06, 1.053748128426467e-06, 1.0523348837549216e-06, 1.0509235344703745e-06, 1.0495140780308086e-06, 1.048106511897616e-06, 1.046700833535593e-06, 1.0452970404129381e-06, 1.043895130001243e-06, 1.042495099775492e-06, 1.0410969472140549e-06, 1.0397006697986826e-06, 1.0383062650145065e-06, 1.036913730350028e-06, 1.0355230632971175e-06, 1.0341342613510097e-06, 1.032747322010297e-06, 1.0313622427769296e-06, 1.0299790211562057e-06, 1.0285976546567696e-06, 1.0272181407906062e-06, 1.02584047707304e-06, 1.0244646610227252e-06, 1.0230906901616447e-06, 1.0217185620151046e-06, 1.0203482741117291e-06, 1.0189798239834604e-06, 1.017613209165547e-06, 1.0162484271965444e-06, 1.0148854756183092e-06, 1.013524351975994e-06, 1.0121650538180464e-06, 1.0108075786961991e-06, 1.0094519241654695e-06, 1.0080980877841533e-06, 1.0067460671138213e-06, 1.0053958597193163e-06, 1.004047463168745e-06, 1.0027008750334756e-06, 1.001356092888134e-06, 1.0000131143106003e-06, 9.98671936882001e-07, 9.973325581867076e-07, 9.95994975812331e-07, 9.946591873497173e-07, 9.933251903929453e-07, 9.91992982539319e-07, 9.906625613893647e-07, 9.893339245468271e-07, 9.880070696186642e-07, 9.866819942150455e-07, 9.853586959493434e-07, 9.84037172438132e-07, 9.827174213011817e-07, 9.813994401614546e-07, 9.80083226645103e-07, 9.787687783814605e-07, 9.774560930030415e-07, 9.76145168145534e-07, 9.748360014477996e-07, 9.735285905518644e-07, 9.722229331029177e-07, 9.709190267493064e-07, 9.69616869142531e-07, 9.683164579372447e-07, 9.670177907912422e-07, 9.65720865365462e-07, 9.644256793239786e-07, 9.631322303339988e-07, 9.618405160658605e-07, 9.605505341930237e-07, 9.592622823920694e-07, 9.579757583426936e-07, 9.566909597277072e-07, 9.55407884233026e-07, 9.541265295476702e-07, 9.528468933637596e-07, 9.515689733765081e-07, 9.502927672842232e-07, 9.490182727882971e-07, 9.477454875932055e-07, 9.46474409406503e-07, 9.452050359388172e-07, 9.439373649038496e-07, 9.426713940183643e-07, 9.414071210021898e-07, 9.401445435782116e-07, 9.388836594723687e-07, 9.376244664136526e-07, 9.36366962134098e-07, 9.351111443687819e-07, 9.338570108558185e-07, 9.326045593363576e-07, 9.313537875545767e-07, 9.301046932576786e-07, 9.288572741958883e-07, 9.276115281224466e-07, 9.263674527936106e-07, 9.251250459686438e-07, 9.238843054098159e-07, 9.226452288823977e-07, 9.21407814154656e-07, 9.201720589978539e-07, 9.189379611862403e-07, 9.177055184970505e-07, 9.164747287105001e-07, 9.152455896097838e-07, 9.140180989810676e-07, 9.127922546134868e-07, 9.115680542991421e-07, 9.103454958330947e-07, 9.09124577013365e-07, 9.079052956409244e-07, 9.066876495196944e-07, 9.05471636456542e-07, 9.042572542612741e-07, 9.030445007466381e-07, 9.01833373728312e-07, 9.006238710249042e-07, 8.994159904579489e-07, 8.982097298519008e-07, 8.970050870341353e-07, 8.958020598349387e-07, 8.946006460875084e-07, 8.93400843627947e-07, 8.922026502952612e-07, 8.91006063931354e-07, 8.898110823810234e-07, 8.886177034919579e-07, 8.874259251147318e-07, 8.862357451028045e-07, 8.850471613125118e-07, 8.838601716030655e-07, 8.826747738365481e-07, 8.814909658779091e-07, 8.803087455949634e-07, 8.791281108583836e-07, 8.779490595416984e-07, 8.767715895212881e-07, 8.755956986763827e-07, 8.744213848890551e-07, 8.73248646044219e-07, 8.720774800296245e-07, 8.709078847358541e-07, 8.697398580563219e-07, 8.685733978872647e-07, 8.674085021277413e-07, 8.662451686796288e-07, 8.650833954476168e-07, 8.639231803392077e-07, 8.627645212647081e-07};

  if (lumiAlgo.value_ == "occupancy") {
      memcpy(HFSBR,sbr_oc,sizeof(float)*interface::bril::shared::MAX_NBX);
  } else if (lumiAlgo.value_ == "etsum") {
      memcpy(HFSBR,sbr_et,sizeof(float)*interface::bril::shared::MAX_NBX);
  }
}

