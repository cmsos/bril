// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_pltprocessor_Application_h_
#define _bril_pltprocessor_Application_h_

#include <string>

#include "xdaq/Application.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"

#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"

#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/squeue.h"
#include "toolbox/BSem.h"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BEAMTopics.hh"

namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}
namespace bril
{
  namespace pltprocessor
  {
    static const unsigned int NBX = 3564;

    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member, public xdata::ActionListener,public toolbox::ActionListener
    {
      public:
	
	XDAQ_INSTANTIATOR();
	
	Application (xdaq::ApplicationStub* s);
	~Application ();
	
	void Default (xgi::Input * in, xgi::Output * out);
	
	void onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist);
	
	virtual void actionPerformed(xdata::Event& e);
	
	virtual void actionPerformed(toolbox::Event& e);
        void automask(float totdata[16]);
    private:
	bool publishing(toolbox::task::WorkLoop* wl);
	void do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef);
	void subscribeall();
        void DoBackground(interface::bril::shared::DatumHead * inheader);
	void init_Flashlist();
	void declare_Flashlist();
    protected:
	
	toolbox::mem::MemoryPoolFactory *m_poolFactory;
	std::string m_classname;
	xdata::UnsignedInteger32 m_instanceid;
	toolbox::mem::Pool* m_memPool;
	
	xdata::UnsignedInteger32 m_currentfill;
	xdata::UnsignedInteger32 m_currentnib;
	xdata::UnsignedInteger32 m_prevnib;	
	xdata::UnsignedInteger32 m_currentls;
	xdata::UnsignedInteger32 m_currentrun;
	xdata::UnsignedInteger32 m_prevls;
	xdata::UnsignedInteger32 m_currentcha;
	xdata::UnsignedInteger32 m_prevcha;

        xdata::Vector<xdata::UnsignedInteger32> m_activechannels;
	xdata::UnsignedInteger32 m_usemask;
	xdata::String m_excludechannels;

	bool m_vdmflag;
	std::string m_beamstatus;
	uint32_t m_aggchannels;
        uint32_t m_aggnibbles;
	uint32_t m_aggdata[16][NBX]; //agg hist per channel 
        uint32_t m_aggzero[16][NBX]; //agg zeroes per channel
        float m_logzero[16][NBX]; //mu value per channel
        float m_totdata[16];
	bool m_automasks[16];      // true=this channel is behaving normally, false=this channel disabled by automasker
	bool m_includechannel[16]; // false=this channel is explicitly removed from lumi calculation in XML config
	float m_rawbxlumi[NBX];  //bxrawlumi averaged over 16 channels
        float m_bxlumi_chan[16][NBX];
	float m_bxlumi[NBX];     //bxrawlumi*calib	
        float m_rawbxzero[NBX];  
	float m_bxzero_chan[16][NBX];
        float m_bxzero[NBX];
        float m_bxlogzero[NBX];
	float m_chan_rawlumizero[16];  // raw lumi by channel (summed over BX)
	float m_chan_lumizero[16];     // calibrated lumi by channel (summed over BX)
	float m_chan_bxzero[16][NBX];  // ALMOST the same as m_bxzero_chan except the former includes the normalization
	// over channels. In a sensible world we wouldn't have these both lying around, but since I don't want to risk breaking
	// anything in the main lumi I'll just keep the two paths separate. Sorry!
	float m_pltbkgA1;
	float m_pltbkgA2;
	float m_pltbkgB1;
	float m_pltbkgB2;
	xdata::Float m_avgrawlumi;       //rawbxlumi sum over bx
	xdata::Float m_avglumi;          //bxlumi sum over bx
	xdata::Float m_avgrawzero;
	xdata::Float m_avgzero;

	float m_bx1[NBX];
	float m_bx2[NBX];
	bool m_iscolliding1[NBX];
	bool m_iscolliding2[NBX];

	//eventinginput, eventingoutput configuration  
	xdata::Vector<xdata::Properties> m_datasources;
	xdata::Vector<xdata::Properties> m_outputtopics;
	xdata::String m_calibtag;
	xdata::String m_calibzerotag;
	xdata::Vector<xdata::Float> m_sigmavis;
	xdata::Vector<xdata::Float>m_pltdimensions;
	xdata::Float m_accidentalspzero;
	xdata::Float m_accidentalspone;
	std::map<std::string, std::set<std::string> > m_in_busTotopics;
	typedef std::multimap< std::string, std::string > TopicStore;
	typedef std::multimap< std::string, std::string >::iterator TopicStoreIt;
	TopicStore m_out_topicTobuses;
	//count how many outgoing buses not ready
	std::set<std::string> m_unreadybuses;
	
	// outgoing queue
	typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* > QueueStore;
	typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* >::iterator QueueStoreIt;
	QueueStore m_topicoutqueues;

	std::map<std::string,std::string> m_outtopicdicts;
	std::map<std::string, xdata::Serializable*> m_monRunStatus;
	std::map<std::string, xdata::Serializable*> m_monLumiStatus;
	toolbox::task::WorkLoop* m_publishing;
	
	// flaslist monitoring
	xdata::InfoSpace *m_monInfoSpace; 
	uint32_t m_tot_hit[16];
	uint32_t m_tot_hit_zero[16];
	std::list<std::string> m_flashfields;
	std::vector<xdata::Serializable*> m_flashcontents;
	toolbox::BSem m_applock;
      };
  }
}

#endif
