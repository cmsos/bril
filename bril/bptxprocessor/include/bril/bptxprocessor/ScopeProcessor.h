#ifndef _bril_bptxprocessor_ScopeProcessor_h_
#define _bril_bptxprocessor_ScopeProcessor_h_

#include "xdaq/Application.h"

#include <string>

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/ActionListener.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/TimeVal.h"

#include "eventing/api/Member.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/BSem.h"
#include "toolbox/squeue.h"

#include "b2in/nub/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/BPTXTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include "log4cplus/logger.h"
#include <unistd.h>
#include "xcept/tools.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"


/*
namespace toolbox{
  namespace task{
    class WorkLoop;
    // class ActionSignature;
  }
}
*/

namespace bril{
  namespace bptxprocessor{
    class ScopeProcessor :  public xdaq::Application,
      public xgi::framework::UIManager,
      public eventing::api::Member,
      public xdata::ActionListener,
      public toolbox::ActionListener,
      public toolbox::task::TimerListener{

    public:
      struct InData{
      InData(unsigned short n,toolbox::mem::Reference* d):nbcount(n),dataref(d){}
      InData():nbcount(0),dataref(0){}
	unsigned short nbcount;
	toolbox::mem::Reference* dataref;
      };

    public:
      XDAQ_INSTANTIATOR();
      // constructor
      ScopeProcessor (xdaq::ApplicationStub* s) noexcept(false);
      // destructor
      ~ScopeProcessor ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out) noexcept(false);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) noexcept(false);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);

      std::string getImg64code(std::ifstream& img);
      std::string myreplace(std::string &s, std::string s1, std::string s2);

      virtual void reOpenDataFile(std::ifstream& ifs, const std::string& fname);
      virtual void checkFileRolover(std::ifstream& ifs, const std::string& fname, const toolbox::TimeInterval& lastReadTime);
      virtual void updateTime( interface::bril::shared::DatumHead* hdr );


    protected:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;

      // Infospace for flashlist:
      xdata::InfoSpace *m_monInfoSpace;
      std::list< std::string > m_flashfields;
      std::vector< xdata::Serializable* > m_flashvalues;
      xdata::Boolean m_fireflash;
      
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      std::string m_classname;


      toolbox::task::Timer *timer;
      toolbox::task::Timer *timer2;

      //
      // monitorables
      //
      //xdata::Table m_montable;
      //std::list<std::string> m_monItemList;
      // void defineMonTable();
      // void initializeMonTable(size_t nchannels);
      // Note: monitorables must be of xdata types, headers can be found in daq/xdata
      //they should have the same names and type as the ones described in my flashlist (xml directory)
      //xdata::String m_beamstatus;
      /*
      xdata::String mon_lid;
      xdata::String mon_context;
      */
      xdata::UnsignedInteger cms_fill;
      xdata::UnsignedInteger cms_run;
      xdata::UnsignedInteger cms_ls;
      xdata::UnsignedInteger cms_nb;
      xdata::UnsignedInteger cms_timestamp;

      // To track the last time the data files were read out:
      toolbox::TimeVal lastReadTime1, lastReadTime2;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      float beam_TotInt1, beam_TotInt2;
      std::vector<unsigned int> beam_LHC_BX_B1, beam_LHC_BX_B2;
      std::vector<unsigned int> beam_FBCT_BX_B1, beam_FBCT_BX_B2;
      bool beam_BXCONF_B1[3564], beam_BXCONF_B2[3564];
      float beam_tot_int_b1, beam_tot_int_b2, beam_avg_int_b1, beam_avg_int_b2;
      //xdata::Vector<xdata::UnsignedInteger> beam_BX_B1, beam_BX_B2;
      //xdata::Vector<xdata::Float> beam_int_B1, beam_int_B2, beam_len_B1, beam_len_B2;
      char beam_mode[50];

      // application lock, it's useful to always include one
      toolbox::BSem m_applock;
      //xdata::Boolean m_simSource;
      //xdata::String m_signalTopic;

      xdata::String m_docroot;
      xdata::String m_file1, m_file2;
      xdata::Float m_ItoIfactor_B1, m_ItoIfactor_B2;
      xdata::UnsignedInteger m_AtoIfactor_B1, m_AtoIfactor_B2;

      xdata::String bptx_script_vesrion;
      xdata::UnsignedInteger bptxB_status;
      xdata::UnsignedInteger bptxB_nB1, bptxB_nB2, bptxT_nB1, bptxT_nB2;
      xdata::UnsignedInteger bptxB_nCol, bptxT_nCol;
      xdata::String bptxB_BX1_str, bptxB_BX2_str;
      xdata::String LHC_BX1_str, LHC_BX2_str;
      xdata::String FBCT_BX1_str, FBCT_BX2_str;
      //bptxB_B1_int, bptxB_B2_int, bptxB_B1_amp, bptxB_B2_amp, bptxB_B1_len, bptxB_B2_len;
      //xdata::String bptxB_B1_pos_zc, bptxB_B2_pos_zc;
      xdata::String bptxT_acq_time, bptxB_acq_time;
      xdata::Float bptxT_dT, bptxB_dT;
      xdata::Table bptx_table_B1, bptx_table_B2;
      xdata::Float bptxB_TOT_INT_B1, bptxB_TOT_INT_B2;
      xdata::Float bptxB_AVG_INT_B1, bptxB_AVG_INT_B2;
      xdata::Float bptxB_AVG_LEN_B1, bptxB_AVG_LEN_B2;
      xdata::Float bptxB_AVG_PHA_B1, bptxB_AVG_PHA_B2;
      float LHC_AVG_LEN_B1, LHC_AVG_LEN_B2;
      //xdata::Float LHC_AVG_LEN_B1, LHC_AVG_LEN_B2;

      std::vector<unsigned int> colliding;

      xdata::Vector<xdata::Properties> m_datasources;
      xdata::Vector<xdata::Properties> m_outputtopics;
      std::map<std::string, std::set<std::string> > m_in_busTotopics;
      typedef std::multimap< std::string, std::string > TopicStore;
      typedef std::multimap< std::string, std::string >::iterator TopicStoreIt;
      TopicStore m_out_topicTobuses;
      std::set<std::string> m_unreadybuses;
      typedef std::string TopicName;
      typedef std::string BusName;

      // incoming data cache
      typedef std::map<unsigned int,InData*> InDataCache;

      //unsigned int m_lastreceivedsec;

      // outgoing data cache
      interface::bril::shared::DatumHead m_lastheader;
      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* > QueueStore;
      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      QueueStore m_topicoutqueues;

      toolbox::task::WorkLoop* m_readingData;


    private:
      // methods
      //void do_process(void* dataptr,size_t buffersize);
      //void do_process(interface::bril::shared::DatumHead& inheader);
      //void clear_cache();
      bool readDataFile(toolbox::task::WorkLoop* wl);
      void doPublish( const TopicName& t, toolbox::mem::Reference *r, const std::string &d);

      void subscribeall();

      void InitBptxFlashlist();
      void CleanFlashlist();
      
      ScopeProcessor(const ScopeProcessor&);
      ScopeProcessor& operator=(const ScopeProcessor&);
    };
  }
}
#endif
