c#include "bril/bptxprocessor/ScopeProcessor2.h"
#include "bril/bptxprocessor/exception/Exception.h"
#include <map>

#include <iostream>
#include <string>
#include <fstream>
#include <locale>
#include "xercesc/util/Base64.hpp"

#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
//#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "toolbox/mem/AutoReference.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "toolbox/mem/AutoReference.h"

//struct tm* myclock;// create a time structure
//struct stat attrib;// create a file attribute structure
//std::ifstream html;

XDAQ_INSTANTIATOR_IMPL (bril::bptxprocessor::ScopeProcessor2)

const xdata::Float BeginOffset = 6606.6;
const xdata::Float ORB = 88924.796;
const xdata::Float BXlength = ORB/3564;
  
bril::bptxprocessor::ScopeProcessor2::ScopeProcessor2 (xdaq::ApplicationStub* s):
xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this), m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&bril::bptxprocessor::ScopeProcessor2::Default,"Default");
  b2in::nub::bind(this, &bril::bptxprocessor::ScopeProcessor2::onMessage);

  LOG4CPLUS_INFO(getApplicationLogger(),"Enter Constructor");

  m_appInfoSpace = getApplicationInfoSpace();
  m_appDescriptor= getApplicationDescriptor();
  m_classname    = m_appDescriptor->getClassName();

  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  m_dipbusName.fromString("brildip");
  m_diptopic.fromString("brildipdata");
  m_dipPubRoot.fromString("dip/CMS/");
  //Set default values
  bptx_script_vesrion = "xx";
  bptxB_nB1=bptxB_nB2=bptxT_nB1=bptxT_nB2=bptxB_nCol=bptxT_nCol=0;
  bptxT_acq_time=bptxB_acq_time="long long time ago";
  bptxT_dT=bptxB_dT=-0.99;


  toolbox::net::URN memurn("toolbox-mem-pool",m_classname+"_mem");
  try{
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool = m_poolFactory->createPool(memurn,allocator);
    m_appInfoSpace->fireItemAvailable("dipPubRoot",&m_dipPubRoot);
    m_appInfoSpace->fireItemAvailable("dipbusName",&m_dipbusName);
    m_appInfoSpace->fireItemAvailable("docroot", &m_docroot);
    m_appInfoSpace->fireItemAvailable("eventinginput", &m_datasources);
    m_appInfoSpace->fireItemAvailable("eventingoutput",&m_outputtopics);


    //m_appInfoSpace->fireItemAvailable("",&m_);
    //m_appInfoSpace->fireItemAvailable("simSource",  &m_simSource);
    //m_appInfoSpace->fireItemAvailable("signalTopic",&m_signalTopic);
    //m_appInfoSpace->fireItemAvailable("bus",&m_bus);

    m_appInfoSpace->fireItemAvailable("dataFile1", &m_file1);
    m_appInfoSpace->fireItemAvailable("dataFile2", &m_file2);


    m_appInfoSpace->fireItemAvailable("ItoIfactor_B1", &m_ItoIfactor_B1);
    m_appInfoSpace->fireItemAvailable("ItoIfactor_B2", &m_ItoIfactor_B2);

    m_appInfoSpace->fireItemAvailable("AtoIfactor_B1", &m_AtoIfactor_B1);
    m_appInfoSpace->fireItemAvailable("AtoIfactor_B2", &m_AtoIfactor_B2);


    /* These do not print anyathing.. Guess they are not filled here yet
    LOG4CPLUS_INFO(getApplicationLogger(),  __func__ <<std::endl
		   <<" file name = "<<m_file1.toString()
		   <<" AtoIfactor_B1 = "<<m_AtoIfactor_B1.toString()
		   <<std::endl);
    */

    m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");

    //m_publishing  = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_publishing", "waiting");
    m_readingData = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_readingData","waiting");
    //std::cout<<"managed to setup memory pool" <<std::endl;


  }catch(xcept::Exception& e){
    std::stringstream msg;
    msg<<"Failed to setup memory pool "<<stdformat_exception_history(e);
    LOG4CPLUS_FATAL(getApplicationLogger(),msg.str());
    XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg.str(),e);
  }


  
  const std::string monurn = createQualifiedInfoSpace("bptx_scope_flash").toString();
  m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));
  InitBptxFlashlist();


  LOG4CPLUS_INFO(getApplicationLogger(),"Hello the worlds");
}

void bril::bptxprocessor::ScopeProcessor2::InitBptxFlashlist (){

  std::string fieldname = "fillnum";
  m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_flashfields.push_back( fieldname );

  fieldname = "runnum";
  m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_flashfields.push_back( fieldname );
  
  fieldname = "lsnum";
  m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_flashfields.push_back( fieldname );
  
  fieldname = "timestamp";
  m_flashvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
  m_flashfields.push_back( fieldname );

  fieldname = "deltaT";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );

  fieldname = "avg_phase_b1";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );
  fieldname = "avg_phase_b2";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );

  fieldname = "avg_intensity_b1";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );
  fieldname = "avg_intensity_b2";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );

  fieldname = "avg_length_b1";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );
  fieldname = "avg_length_b2";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );

  fieldname = "tot_intensity_b1";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );
  fieldname = "tot_intensity_b2";
  m_flashvalues.push_back( new xdata::Float(0) );
  m_flashfields.push_back( fieldname );

  
  // Per BX flashlists below:
  fieldname = "phase_b1";
  m_flashvalues.push_back( new xdata::Vector<xdata::Integer>() );
  m_flashfields.push_back( fieldname );
  fieldname = "phase_b2";
  m_flashvalues.push_back( new xdata::Vector<xdata::Integer>() );
  m_flashfields.push_back( fieldname );

  fieldname = "intensity_b1";
  m_flashvalues.push_back( new xdata::Vector<xdata::UnsignedInteger>() );
  m_flashfields.push_back( fieldname );
  fieldname = "intensity_b2";
  m_flashvalues.push_back( new xdata::Vector<xdata::UnsignedInteger>() );
  m_flashfields.push_back( fieldname );
  
  fieldname = "length_b1";
  m_flashvalues.push_back( new xdata::Vector<xdata::UnsignedInteger>() );
  m_flashfields.push_back( fieldname );
  fieldname = "length_b2";
  m_flashvalues.push_back( new xdata::Vector<xdata::UnsignedInteger>() );
  m_flashfields.push_back( fieldname );

  try{
    std::vector<xdata::Serializable*>::const_iterator vIt = m_flashvalues.begin();
    std::list<std::string>::const_iterator nameIt=m_flashfields.begin(); 
    for(; nameIt!=m_flashfields.end(); ++nameIt, ++vIt ){
      m_monInfoSpace->fireItemAvailable(*nameIt,*vIt);
    }
  } catch(xdata::exception::Exception& e){
    std::string msg( "Failed to declare flashlist for BPTX scope data" );
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
    XCEPT_RETHROW(xdaq::exception::Exception, msg, e);
  }
  
  
}

void bril::bptxprocessor::ScopeProcessor2::CleanFlashlist(){
  for(std::vector< xdata::Serializable* >::iterator it=m_flashvalues.begin(); it!=m_flashvalues.end(); ++it){
    delete *it;
  }
  m_flashvalues.clear();
}

bril::bptxprocessor::ScopeProcessor2::~ScopeProcessor2 (){
  //QueueStoreIt it;
  //for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
  //delete it->second;
  //}
  //m_topicoutqueues.clear();
  
  CleanFlashlist();
}


std::string bril::bptxprocessor::ScopeProcessor2::myreplace(std::string &s, std::string toReplace, std::string replaceWith){
  if (s.find(toReplace)!=std::string::npos)
    return s.replace(s.find(toReplace), toReplace.length(), replaceWith);
  else
    return s;
}


void bril::bptxprocessor::ScopeProcessor2::Default (xgi::Input * in, xgi::Output * out){

  LOG4CPLUS_DEBUG(getApplicationLogger(), "In the default.. I.e. accessing the Web page");

  //std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();

  //html.open("/nfshome0/andrey/xDAQ/daq/bril/bptxprocessor/html/html_template.html", std::ifstream::in);

  //std::cout<<"docroot path = "<<m_docroot.toString()<<std::endl;
  std::string htmltemplate = m_docroot.toString()+"/bril/bptxprocessor/html/html_template.html";
  std::ifstream html;
  html.open(htmltemplate.c_str(), std::ifstream::in);
  //std::cout<<"template path = "<<htmltemplate<<std::endl;


  std::stringstream  html_mod;
  html_mod<<html.rdbuf();
  std::string html_mod_str  =  html_mod.str();
  html.close();

  m_applock.take();

  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%BUSES%", busesToHTML());
  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%BEAM_MODE%", beam_mode);

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_timestamp%",  bptxT_acq_time);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_timestamp%", bptxB_acq_time);

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_nB1%",  bptxT_nB1.toString());
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_nB2%",  bptxT_nB2.toString());
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_nB1%", bptxB_nB1.toString());
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_nB2%", bptxB_nB2.toString());


  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_dT%",  toolbox::toString("%.3f",(float)bptxT_dT));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_dT%", toolbox::toString("%.3f",(float)bptxB_dT));

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_ver%", bptx_script_vesrion);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_ver%",  bptx_script_vesrion);


  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_BX_B1%",   bptxB_BX1_str);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_BX_B2%",   bptxB_BX2_str);

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%LHC_BX_B1%",   LHC_BX1_str);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%LHC_BX_B2%",   LHC_BX2_str);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_BX_B1%",  FBCT_BX1_str);
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_BX_B2%",  FBCT_BX2_str);


  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_TOT_INT_B1%", toolbox::toString("%.1f",(float)bptxB_TOT_INT_B1));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_TOT_INT_B2%", toolbox::toString("%.1f",(float)bptxB_TOT_INT_B2));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_INT_B1%", toolbox::toString("%.2f",(float)bptxB_AVG_INT_B1));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_INT_B2%", toolbox::toString("%.2f",(float)bptxB_AVG_INT_B2));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_PHA_B1%", toolbox::toString("%.3f",(float)bptxB_AVG_PHA_B1/1000));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_PHA_B2%", toolbox::toString("%.3f",(float)bptxB_AVG_PHA_B2/1000));

  // Calculated from the sum of per-bunch FBCT numbers:
  //html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_TOT_INT_B1%", toolbox::toString("%.1f",(float)beam_tot_int_b1));
  //html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_TOT_INT_B2%", toolbox::toString("%.1f",(float)beam_tot_int_b2));
  // Directly from the  publication
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_TOT_INT_B1%", toolbox::toString("%.1f",(float)(1E-11*beam_TotInt1)));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_TOT_INT_B2%", toolbox::toString("%.1f",(float)(1E-11*beam_TotInt2)));

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_AVG_INT_B1%", toolbox::toString("%.2f",(float)beam_avg_int_b1));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%FBCT_AVG_INT_B2%", toolbox::toString("%.2f",(float)beam_avg_int_b2));

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_LEN_B1%", toolbox::toString("%.3f",(float)bptxB_AVG_LEN_B1));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_AVG_LEN_B2%", toolbox::toString("%.3f",(float)bptxB_AVG_LEN_B2));

  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%LHC_AVG_LEN_B1%", toolbox::toString("%.3f",1E9*(float)LHC_AVG_LEN_B1));
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%LHC_AVG_LEN_B2%", toolbox::toString("%.3f",1E9*(float)LHC_AVG_LEN_B2));


  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%FILL%", cms_fill.toString());
  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%FILL%", cms_fill.toString());
  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%FILL%", cms_fill.toString());

  //std::cout<<"Before the for loop"<<std::endl;

  std::stringstream table_b1, table_b2, table_both;

  table_b1 << cgicc::tbody();

  std::vector<unsigned int> bptx_BX_B1, bptx_BX_B2;

  xdata::Serializable * s;
  for (size_t j = 0; j < bptx_table_B1.getRowCount(); j++ ){
    table_b1 << cgicc::tr();
    s = bptx_table_B1.getValueAt(j, "BX");
    if (std::find(colliding.begin(), colliding.end(), (unsigned int)toolbox::toLong(s->toString())) != colliding.end())
      table_b1 << cgicc::td(cgicc::b(s->toString())); // This is a colliding bunch. Make it BOLD
    else
      table_b1 << cgicc::td(s->toString());
    bptx_BX_B1.push_back(toolbox::toLong(s->toString()));
    s = bptx_table_B1.getValueAt(j, "INT");
    table_b1 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B1.getValueAt(j, "LEN");
    table_b1 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B1.getValueAt(j, "AMP");
    table_b1 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B1.getValueAt(j, "PHASE");
    table_b1 << cgicc::td(s->toString());
    table_b1 << cgicc::tr();
  }

  table_b1 << cgicc::tbody();

  table_b2 << cgicc::tbody();
  for (size_t j = 0; j < bptx_table_B2.getRowCount(); j++ ){
    table_b2 << cgicc::tr();
    s = bptx_table_B2.getValueAt(j, "BX");
    if (std::find(colliding.begin(), colliding.end(), (unsigned int)toolbox::toLong(s->toString())) != colliding.end())
      table_b2 << cgicc::td(cgicc::b(s->toString()));
    else
      table_b2 << cgicc::td(s->toString());
    bptx_BX_B2.push_back(toolbox::toLong(s->toString()));
    s = bptx_table_B2.getValueAt(j, "INT");
    table_b2 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B2.getValueAt(j, "LEN");
    table_b2 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B2.getValueAt(j, "AMP");
    table_b2 << cgicc::td(toolbox::toString("%.3f", toolbox::toDouble(s->toString())));
    s = bptx_table_B2.getValueAt(j, "PHASE");
    table_b2 << cgicc::td(s->toString());
    table_b2 << cgicc::tr();
  }

  table_b2 << cgicc::tbody();

  //std::cout<<"After the table "<<std::endl;

  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%TABLE_B1%", table_b1.str());
  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%TABLE_B2%", table_b2.str());


  //std::cout<<"Lets compare some length now"<<std::endl;
  //std::cout<<"B1  ==>  bptx = "<<bptx_BX_B1.size()<<"  fbct = "<<beam_FBCT_BX_B1.size()<<"  LHC"<<beam_FBCT_BX_B1.size()<<std::endl;
  //std::cout<<"B2  ==>  bptx = "<<bptx_BX_B2.size()<<"  fbct = "<<beam_FBCT_BX_B2.size()<<"  LHC"<<beam_FBCT_BX_B1.size()<<std::endl;

  std::vector<unsigned int> vec;
  std::set_intersection(bptx_BX_B1.begin(), bptx_BX_B1.end(), bptx_BX_B2.begin(), bptx_BX_B2.end(),  std::back_inserter(vec));

  //std::cout<<"Colliding bptx = "<<vec.size()<<"   from file: "<<bptxB_nCol<<std::endl;
  //bptxB_nCol = vec.size();

  colliding = vec;

  vec.clear();
  //std::set_difference(bptx_BX_B1.begin(), bptx_BX_B1.end(), beam_LHC_BX_B1.begin(), beam_LHC_BX_B1.end(),  std::back_inserter(vec));
  //std::cout<<" \n\n\n \t\t TEST TEST \n"<<std::endl;
  //std::cout<<"Extra bunches in bptx = "<<vec.size()<<std::endl;
  std::string miss("None");
  std::stringstream myBXs;

  // Missing bunches
  vec.clear();
  std::set_difference(beam_LHC_BX_B1.begin(), beam_LHC_BX_B1.end(), bptx_BX_B1.begin(), bptx_BX_B1.end(), std::back_inserter(vec));
  //std::cout<<"Missing bunches in bptx = "<<vec.size()<<std::endl;
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(myBXs,", "));
  if (vec.size()>0) miss = myBXs.str();
  else miss="None";
  myBXs.str("");
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_MISS_B1%", miss);

  vec.clear();
  std::set_difference(beam_LHC_BX_B2.begin(), beam_LHC_BX_B2.end(), bptx_BX_B2.begin(), bptx_BX_B2.end(), std::back_inserter(vec));
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(myBXs,", "));
  if (vec.size()>0) miss = myBXs.str();
  else miss="None";
  myBXs.str("");
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_MISS_B2%", miss);


  // Unexpeced extra bunches
  vec.clear();
  std::set_difference(bptx_BX_B1.begin(), bptx_BX_B1.end(), beam_LHC_BX_B1.begin(), beam_LHC_BX_B1.end(), std::back_inserter(vec));
  //std::cout<<"Missing bunches in bptx = "<<vec.size()<<std::endl;
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(myBXs,", "));
  if (vec.size()>0) miss = myBXs.str();
  else miss="None";
  myBXs.str("");
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_EXTRA_B1%", miss);

  vec.clear();
  std::set_difference(bptx_BX_B2.begin(), bptx_BX_B2.end(), beam_LHC_BX_B2.begin(), beam_LHC_BX_B2.end(), std::back_inserter(vec));
  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(myBXs,", "));
  if (vec.size()>0) miss = myBXs.str();
  else miss="None";
  myBXs.str("");
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_EXTRA_B2%", miss);



  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Timing_nCol%",  bptxT_nCol.toString());
  html_mod_str = ScopeProcessor2::myreplace(html_mod_str, "%BPTX_Bunches_nCol%", bptxB_nCol.toString());

  m_applock.give();

  // Insert an image (scope screen shot with base 64 trick)
  std::ifstream img1, img2;
  img1.open("/brildata/bptx/scopes/bunches-snapshots/scope_screenshot.png", std::ios::binary);
  img2.open("/brildata/bptx/scopes/timing-snapshots/scope_screenshot.png",  std::ios::binary);

  std::string img1SRC =  "data:image/png;base64,"+getImg64code(img1);
  std::string img2SRC =  "data:image/png;base64,"+getImg64code(img2);
  std::stringstream htmlImage1, htmlImage2;
  htmlImage1 << cgicc::a(cgicc::img().set("src",img1SRC.c_str()).set("width", "500").set("alt", "Scope screenshot of Bunches measurement")).set("href",img1SRC.c_str());
  htmlImage2 << cgicc::a(cgicc::img().set("src",img2SRC.c_str()).set("width", "500").set("alt", "Scope screenshot of  Timing measurement")).set("href",img2SRC.c_str());

  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%SCREEN_Bunches%", htmlImage1.str());
  html_mod_str =  ScopeProcessor2::myreplace(html_mod_str, "%SCREEN_Timing%",  htmlImage2.str());


  //*out << cgicc::img().set("src",codedImage.str().c_str()).set("width", "50").set("alt", "Scope scrinshot of Bunches measurement");


  //std::cout<<"After the images"<<std::endl;

  *out<<html_mod_str;

  //*out << cgicc::body() << std::endl;
  LOG4CPLUS_DEBUG(getApplicationLogger(), "\t END OF Default.");

}


std::string bril::bptxprocessor::ScopeProcessor2::getImg64code(std::ifstream& img){
  if (img.is_open()){
    img.seekg(0, std::ios::end);
    const long length = img.tellg();
    img.seekg(0);

    char* buffer = new char[length];
    img.read(buffer, length);

    //std::cout<<buffer;

    XMLSize_t outputLength;
    XMLByte *dd;
    dd = Base64::encode(reinterpret_cast<const XMLByte*> (buffer), length, &outputLength);

    //std::cout<<outputLength <<std::endl;
    //std::string s( reinterpret_cast<char const*>(dd), outputLength) ;
    //std::cout<<dd<<std::endl;
    //codedImage<<"data:image/png;base64,"<<dd;

    std::stringstream codedImage;
    codedImage<<dd;

    delete[] buffer;
    delete[] dd;
    img.close();

    return codedImage.str();
  }
  else
    return "";

}

void bril::bptxprocessor::ScopeProcessor2::reOpenDataFile(std::ifstream& ifs, const std::string& fname){

  LOG4CPLUS_INFO(getApplicationLogger(),"Re-Open the file: "<< fname);
  // First of all let's cancel the reading workloop:
  LOG4CPLUS_INFO(getApplicationLogger(), "Canceling the Reading workloop");

  if (m_readingData->isActive())
    m_readingData->cancel();

  ifs.open(fname.c_str());
  if (ifs.is_open()) {
    std::string line;
    while (std::getline(ifs, line)) {
      //std::cout << line.substr(0, 100) << "\n";
      if (ifs.eof()) break;
    }
    ifs.clear();
  }

  LOG4CPLUS_INFO(getApplicationLogger(),"Read it all --> "<< fname);
  // Now, submit the workloop again
  m_readingData->activate();
  LOG4CPLUS_INFO(getApplicationLogger(), "Reading workloop is re-activated!");
}


void bril::bptxprocessor::ScopeProcessor2::actionPerformed(xdata::Event& e){

  //LOG4CPLUS_INFO(getApplicationLogger(), "Received xdata event " << e.type());

  if( e.type() == "urn:xdaq-event:setDefaultValues" ){

    reOpenDataFile(ifs1, m_file1.toString());
    sleep(1);
    reOpenDataFile(ifs2, m_file2.toString());

    /*
    try{
      timer = toolbox::task::getTimerFactory()->createTimer(timername);
      toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
      toolbox::TimeInterval i1;  i1.fromString ("PT24H0M");
      toolbox::TimeVal nextDay = now + i1;
      //toolbox::TimeVal midnight; midnight.fromString("2015-07-09 12:14:00","%F %T", toolbox::TimeVal::loc);
      // 2am is a midnight in UTC time:
      toolbox::TimeVal midnight; midnight.fromString(nextDay.toString("%F", toolbox::TimeVal::loc)+" 00:00:01","%F %T", toolbox::TimeVal::gmt);
      std::cout << "Now is: \n"
		<< now.toString ("%F %T", toolbox::TimeVal::loc) << std::endl;
      std::cout << "Next day is: \n"
		<< nextDay.toString ("%F %T", toolbox::TimeVal::loc) << std::endl;
      std::cout << "And the midnight is: \n"
		<< midnight.toString("%F %T", toolbox::TimeVal::loc) << std::endl;
      timer->schedule(this, midnight, 0, timername);

      LOG4CPLUS_DEBUG(getApplicationLogger(), "Timer is set up: "<<timername);

    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer "<<timername;
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg.str(),e);
    }
    */

    std::string timername("fileCheck");
    try{
      // Second timer to cach file rolover or stack
      toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
      timer = toolbox::task::getTimerFactory()->createTimer( timername );
      toolbox::TimeInterval xSec(59,0);
      timer->scheduleAtFixedRate(now, this, xSec, 0, timername);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Time is set up: "+timername);

    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer ";
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg.str(),e);
    }




    timername = "fireInfoSpace";
    try{
      // Timer for firing flashlist
      toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
      timer = toolbox::task::getTimerFactory()->createTimer( timername );
      toolbox::TimeInterval xSec(11,0);
      timer->scheduleAtFixedRate(now, this, xSec, 0, timername);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Time is set up: "+ timername);

    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer "<<timername;
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg.str(),e);
    }



    try{
      toolbox::task::ActionSignature* as_readingData = toolbox::task::bind(this,&bril::bptxprocessor::ScopeProcessor2::readDataFile,"readDataFile");
      m_readingData->submit(as_readingData);
      if (!m_readingData->isActive()) m_readingData->activate();
      LOG4CPLUS_INFO(getApplicationLogger()," Activated reading data WorkLoop");
    }catch(toolbox::task::exception::Exception& e){
      std::stringstream msg;
      msg<<"Failed to start read data workloop "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception, msg.str(),e);
    }


    //this->getEventingBus(m_bus.value_).addActionListener(this);
    //if(this->getEventingBus(m_bus.value_).canPublish()){
    //std::string msg("Eventing bus is ready at setDefaultValues");
    //LOG4CPLUS_DEBUG(getApplicationLogger(),msg);
    //}

    const size_t nsources = m_datasources.elements();

    std::cout<<"nsources =   "<<nsources<<std::endl;
    try{
      for(size_t i=0;i<nsources;++i){
	xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
	
	if(p){
	  const xdata::String topicStr = p->getProperty("topics");
	  const xdata::String databus  = p->getProperty("bus");

	  std::cout<<i<<"  bus =   "<<databus.toString()<<std::endl;

	  const std::set<std::string> topics = toolbox::parseTokenSet(topicStr.value_,",");
	  m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
	}
      }

      subscribeall();


      size_t ntopics = m_outputtopics.elements();
      std::cout<<"ntopics =   "<<ntopics<<std::endl;

      for(size_t i=0;i<ntopics;++i){
	xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));

	if(p){
	  const xdata::String topicname = p->getProperty("topic");
	  const xdata::String outBusStr = p->getProperty("buses");

	  const std::set<std::string> outputbuses = toolbox::parseTokenSet(outBusStr.value_,",");
	  for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){

	    m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));

	    LOG4CPLUS_INFO(getApplicationLogger(), "Topic: "+topicname.value_);

	  }
	  //m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
	  m_unreadybuses.insert(outputbuses.begin(), outputbuses.end());
	}
      }

      for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
	this->getEventingBus(*it).addActionListener(this);
	//if(this->getEventingBus(*it).canPublish()) continue;
      }
    }catch(xdata::exception::Exception& e){
      std::stringstream msg;
      msg<<"Failed to parse application property";
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception, msg.str(), e);
    }
  }

}


void  bril::bptxprocessor::ScopeProcessor2::actionPerformed(toolbox::Event& e){

  LOG4CPLUS_INFO(getApplicationLogger(), " Tool box event occured, it is: "<<e.type());

  if(e.type() == "eventing::api::BusReadyToPublish"){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "event Bus '" << busname << "' is ready to publish";
    m_unreadybuses.erase(busname);
    if(m_unreadybuses.size()!=0) return; //wait until all buses are ready

    /* Don't need a publishing loop for now
       try{
       toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::bptxprocessor::ScopeProcessor2::publishing,"publishing");
       m_publishing->submit(as_publishing);
       m_publishing->activate();
       LOG4CPLUS_INFO(getApplicationLogger()," Activated publishing WorkLoop");
       }catch(toolbox::task::exception::Exception& e){
       msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
       LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg.str(),e);
      }
    */

    /* This timer is not used yet
    try{
      std::string appuuid = m_appDescriptor->getUUID().toString();
      toolbox::TimeInterval checkinterval(60,0);// check every 10 seconds
      //std::string timername("stalecachecheck_timer");
      toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername+"_"+appuuid);
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
      timer->scheduleAtFixedRate(start, this, checkinterval, 0, timername);
    }catch(toolbox::exception::Exception& e){
      std::string msg("failed to start timer ");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e) );
      XCEPT_RETHROW(bril::bptxprocessor::exception::Exception,msg,e);
    }
    */

  }
}

void bril::bptxprocessor::ScopeProcessor2::subscribeall(){
  LOG4CPLUS_INFO(getApplicationLogger(), "Subscibe all");

 for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
    std::string busname = bit->first;
    for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){
      LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit);
      try{
	this->getEventingBus(busname).subscribe(*topicit);
      }catch(eventing::api::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
	m_in_busTotopics[busname].erase(*topicit);
      }
    }
  }
}

void bril::bptxprocessor::ScopeProcessor2::updateTime( interface::bril::DatumHead* hdr )
{ // stealing this from bhm processor
  if ( !hdr->fillnum ) //sometimes the information is missing...
    return;
  m_applock.take();
  cms_fill = hdr->fillnum;
  cms_run  = hdr->runnum;
  cms_ls   = hdr->lsnum;
  cms_nb   = hdr->nbnum;
  cms_timestamp = hdr->timestampsec;
  m_applock.give();
}

void bril::bptxprocessor::ScopeProcessor2::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope

  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    const std::string topic = plist.getProperty("urn:b2in-eventing:topic");

    LOG4CPLUS_DEBUG(getApplicationLogger(), "\n Received data from "+topic);

    const std::string v = plist.getProperty("DATA_VERSION");
    if(v.empty()){
      const std::string msg("Received data message without version header, do nothing.");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+" Topic = "+topic);

      return;
    }

    //if (topic != "beam") //Because it publishes too often
    //LOG4CPLUS_DEBUG(getApplicationLogger(), "\n On message, topic="<<topic);


    const std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
    interface::bril::CompoundDataStreamer tc(payload_dict);
    interface::bril::DatumHead* inheader = (interface::bril::DatumHead*)(ref->getDataLocation());

    if(v!=interface::bril::DATA_VERSION){
      const std::string msg("Mismatched bril data version in received message, do nothing");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);

      return;
    }

    if( payload_dict.empty() ){
      const std::string msg("Received brildaq message has no dictionary, do not process.");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      return;
    }

    /*
    if(inheader->datasourceid==9){
      // THis is all the stuff from BPTX source

      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__
		       <<"\n TOPIC = "<<topic
		       <<"\n Fill= "<<cms_fill<<"  Run= "<<cms_run<<"  LS= "<<cms_ls<<"  NB= "<<cms_nb<<"   times= "<<cms_timestamp<<std::endl
		       <<"sourceId= "<<(int)inheader->datasourceid<<"  algoId= "<<(int)inheader->algoid<<"  cha-id="<<(int)inheader->channelid);
    }
    */

    if( topic == interface::bril::ScopeDataT::topicname()){

      // This subscription is for TESTS
      // Scope Data topic should be commented out for production

      /*
      std::string data_version = plist.getProperty("DATA_VERSION");
      std::string ttt = plist.getProperty("ABC");

      uint16_t n1, n2, nCol, aLen;
      tc.extract_field( &n1, "nB1", inheader->payloadanchor );
      tc.extract_field( &n2, "nB2", inheader->payloadanchor );
      tc.extract_field( &nCol, "nCol", inheader->payloadanchor );
      tc.extract_field( &aLen, "AVG_LEN_B1", inheader->payloadanchor );

      LOG4CPLUS_DEBUG(getApplicationLogger(),  __func__ <<std::endl<<topic<<" topic.   data_version="<<data_version
		      <<"   nB1 = "<<n1<<"   nB2 = "<<n2<<"  nCol="<<nCol<<
		      "\n B1 avg len =  "<<aLen<<std::endl);

      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__
		       <<":\n Fill= "<<cms_fill<<"  Run= "<<cms_run<<"  LS= "<<cms_ls<<"  NB= "<<cms_nb<<"   times= "<<cms_timestamp<<std::endl
		       <<"sourceId= "<<(int)inheader->datasourceid<<"  algoId= "<<(int)inheader->algoid<<"  cha-id="<<(int)inheader->channelid);
      */


      //uint16_t tmp1[3564],tmp2[3564];
      //for (int i=0; i<3564; ++i) { tmp1[i]=0; tmp2[i]=0;}
      //tc.extract_field(tmp1,"LEN_B1", inheader->payloadanchor);
      //tc.extract_field(tmp2,"LEN_B2", inheader->payloadanchor);
      
      //int16_t tmp1[3564],tmp2[3564];
      //for (int i=0; i<3564; ++i) { tmp1[i]=0; tmp2[i]=0;}
      //tc.extract_field(tmp1,"PHASE_B1", inheader->payloadanchor);
      //tc.extract_field(tmp2,"PHASE_B2", inheader->payloadanchor);

      /*
      std::cout<<" Start loop over tmps"<<" B1 (0) = "<<tmp1[0]<<std::endl;
      for (int i=0; i<20; ++i) {
        if (tmp1[i]!=0)
	  std::cout<<i<<" B1  Val = "<<tmp1[i]<<std::endl;
        if (tmp2[i]!=0)
	  std::cout<<i<<" B2  Val = "<<tmp2[i]<<std::endl;
      }
      std::cout<<" End loop over tmps"<<std::endl;
      */

      /*
      float tmp1, tmp2;
      tc.extract_field( &tmp1, "TOT_INT_B1", inheader->payloadanchor );
      tc.extract_field( &tmp2, "TOT_INT_B2", inheader->payloadanchor );

      std::cout<<"TOT intens B1 = "<<tmp1<<std::endl;
      std::cout<<"TOT intens B2 = "<<tmp2<<std::endl;


      */

      /*
      int16_t itmp1, itmp2;
      tc.extract_field( &itmp1, "AVG_PHASE_B1", inheader->payloadanchor );
      tc.extract_field( &itmp2, "AVG_PHASE_B2", inheader->payloadanchor );

      std::cout<<"AVG PHASES. Beam 1 = "<<itmp1
	       <<"ps   Beam 2 = "<<itmp2<<"ps.   DeltaT(B1-B2) = "<<itmp1-itmp2<<std::endl;

      float ftmp;
      tc.extract_field( &ftmp, "DeltaT", inheader->payloadanchor );
      std::cout<<"\t DeltaT = "<<ftmp<<std::endl;
      */
      
    }


    if( topic == interface::bril::tcdsT::topicname()){
      LOG4CPLUS_DEBUG(getApplicationLogger(),  __func__ <<std::endl
		      <<"\n Received data from TCDS in BrilDAQ");
      updateTime(inheader);
    }

    if( topic == interface::bril::bunchlengthT::topicname()){

      //updateTime(inheader); //Do not update time from this source. It's often missing.

      LOG4CPLUS_DEBUG(getApplicationLogger(),  __func__ <<std::endl
		      <<"\n Received data from Bunch LENGTHES!!!");

      //float b1_len, b2_len;
      tc.extract_field( &LHC_AVG_LEN_B1, "b1mean", inheader->payloadanchor );
      tc.extract_field( &LHC_AVG_LEN_B2, "b2mean", inheader->payloadanchor );
    }


    if( topic == interface::bril::beamT::topicname()){

      updateTime(inheader);

      //interface::bril::DatumHead* inheader = (interface::bril::DatumHead*)(ref->getDataLocation());

      // Beam data is probably the most reliable,
      // So let's get out run number, etc from here

      //LOG4CPLUS_DEBUG( getApplicationLogger(), __func__
      //	       <<":\n Fill= "<<cms_fill<<"  Run= "<<cms_run<<"  LS= "<<cms_ls<<"  NB= "<<cms_nb<<"   times= "<<cms_timestamp<<std::endl
      //	       <<"sourceId= "<<(int)inheader->datasourceid<<"  algoId= "<<(int)inheader->algoid<<"  cha-id="<<(int)inheader->channelid);


      tc.extract_field( &beam_mode,    "status",     inheader->payloadanchor );
      tc.extract_field( &beam_TotInt1, "intensity1", inheader->payloadanchor );
      tc.extract_field( &beam_TotInt2, "intensity2", inheader->payloadanchor );

      //ss.str("\n");
      //ss << "Read beam topics. Beam mode is "<<beam_mode <<"  beam 1 has avg charge of "<<beam_TotInt1;
      //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //std::cout<<"Beam mode = "<<beam_mode_x<<std::endl;

      float beam_bxint_B1[3564], beam_bxint_B2[3564];
      for (int i=0; i<3564; ++i) {
	beam_bxint_B1[i]=0;
	beam_bxint_B2[i]=0;
      }

      tc.extract_field( beam_bxint_B1,  "bxintensity1", inheader->payloadanchor );
      tc.extract_field( beam_bxint_B2,  "bxintensity2", inheader->payloadanchor );

      tc.extract_field( beam_BXCONF_B1,  "bxconfig1", inheader->payloadanchor );
      tc.extract_field( beam_BXCONF_B2,  "bxconfig2", inheader->payloadanchor );

      beam_tot_int_b1=beam_tot_int_b2=beam_avg_int_b1=beam_avg_int_b2=0;
      beam_LHC_BX_B1.clear();  beam_LHC_BX_B2.clear();
      beam_FBCT_BX_B1.clear(); beam_FBCT_BX_B2.clear();
      for (int i=0; i<3564; i++){

	if (beam_BXCONF_B1[i]) beam_LHC_BX_B1.push_back(i+1);
	if (beam_BXCONF_B2[i]) beam_LHC_BX_B2.push_back(i+1);

	if (beam_bxint_B1[i] > 5E9) {
	  beam_tot_int_b1 += beam_bxint_B1[i]*1E-11;
	  beam_FBCT_BX_B1.push_back(i+1);
	  //std::cout<<beam_BX_B1.size()<<"  BX="<<i<<"   b1="<<beam_bxint_B1[i]<<"  b2="<<beam_bxint_B2[i]<<std::endl;;
	}
	if (beam_BXCONF_B2[i]) {
	  beam_tot_int_b2 += beam_bxint_B2[i]*1E-11;
	  beam_FBCT_BX_B2.push_back(i+1);
	}
      }

      if (beam_FBCT_BX_B1.size()!=0) beam_avg_int_b1 = beam_tot_int_b1/beam_FBCT_BX_B1.size();
      else beam_avg_int_b1=0;
      if (beam_FBCT_BX_B2.size()!=0) beam_avg_int_b2 = beam_tot_int_b2/beam_FBCT_BX_B2.size();
      else beam_avg_int_b2=0;

    }


    std::stringstream myBXs;
    std::copy(beam_LHC_BX_B1.begin(), beam_LHC_BX_B1.end(), std::ostream_iterator<int>(myBXs,", "));
    LHC_BX1_str = myBXs.str(); myBXs.str("");
    std::copy(beam_LHC_BX_B2.begin(), beam_LHC_BX_B2.end(), std::ostream_iterator<int>(myBXs,", "));
    LHC_BX2_str = myBXs.str(); myBXs.str("");

    std::copy(beam_FBCT_BX_B1.begin(), beam_FBCT_BX_B1.end(), std::ostream_iterator<int>(myBXs,", "));
    FBCT_BX1_str = myBXs.str(); myBXs.str("");
    std::copy(beam_FBCT_BX_B2.begin(), beam_FBCT_BX_B2.end(), std::ostream_iterator<int>(myBXs,", "));
    FBCT_BX2_str = myBXs.str(); myBXs.str("");

    //std::cout<<myBXs.str()<<" \n"<<std::endl;

    //LOG4CPLUS_DEBUG(getApplicationLogger(), "End of topic: "+topic);

    //toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    //unsigned int nowsec = t.sec();
    //m_lastreceivedsec = nowsec;


  }

  return;
}


/*
void bril::bptxprocessor::ScopeProcessor2::do_process(interface::bril::DatumHead& inheader)
{

  LOG4CPLUS_DEBUG(getApplicationLogger(), "Now entered do_process");

  //std::cout<<"Still in do_process: bptxbkgsums"<<std::endl;
  // now for BG numbers
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process");
}

void bril::bptxprocessor::ScopeProcessor2::clear_cache(){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "clear_cache");

}
*/


bool bril::bptxprocessor::ScopeProcessor2::readDataFile(toolbox::task::WorkLoop* wl){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "We are in readDataFile workLoop");

  if (ifs1.is_open())
    {
      // Following suggestions from here: http://stackoverflow.com/questions/11757304/how-to-read-a-growing-text-file-in-c

      //LOG4CPLUS_DEBUG(getApplicationLogger(), "Yes, the file is open!");
      std::string line;
      while (std::getline(ifs1, line))
	{
	  if (line.substr(0,1)=="#") continue;

	  // Lock this thread from Default() method to avoid segfaults

	  lastReadTime1 = toolbox::TimeVal::gettimeofday();

	  LOG4CPLUS_DEBUG(getApplicationLogger(), "\n Bunches data. This line is to be processed and published:\n \t" << line.substr(0,100));
	  //std::cout << line.substr(0, 100) << "\n";

	  // Here will submit into another Workloop which will parse the 'line' and create the Data structure in BRIL format
	  // ... or maybe not... will see
	  std::list<std::string> dataScope1 = toolbox::parseTokenList(line," ");
	  //std::cout<<toolbox::printTokenList(dataScope1, " -- ")<<std::endl;


	  // Check that we have all data in parsed input:
	  LOG4CPLUS_DEBUG(getApplicationLogger(), "\n Number of parsed entries:  "<<dataScope1.size());
	  if (dataScope1.size()!=32){
	    LOG4CPLUS_WARN(getApplicationLogger(), "The line exists, but we have to skip it because it does not contain the correct number of entries: "
			   <<dataScope1.size());
	    continue;
	  }

	  // Lock it from Web method (Default)
	  m_applock.take();

	  std::list<std::string>::iterator it = dataScope1.begin();
	  bptx_script_vesrion.fromString(*it);
	  it++;


	  bptxB_acq_time.fromString(*it);
	  toolbox::TimeVal ScopeACQ;
	  ScopeACQ.fromString(bptxB_acq_time,"%FT%T", toolbox::TimeVal::gmt);
	  //std::cout<<"\n\n *** Orig Time = "<<bptxB_acq_time.toString()
	  //   <<"  TimeVal="<<ScopeACQ.toString("%F %T", toolbox::TimeVal::gmt)<<std::endl;


	  it++;


	  bptxB_status.fromString(*it);
	  std::advance(it,6);

	  bptxB_nB1.fromString(*it);
	  it++;

	  std::list<std::string>  parsed;
	  // Clear the tables before filling it:
	  bptx_table_B1.clear();
	  bptx_table_B2.clear();


	  bptx_table_B1.addColumn("BX","unsigned int");
	  bptx_table_B2.addColumn("BX","unsigned int");

	  bptx_table_B1.addColumn("INT","float");
	  bptx_table_B2.addColumn("INT","float");

	  bptx_table_B1.addColumn("LEN","float");
	  bptx_table_B2.addColumn("LEN","float");

	  bptx_table_B1.addColumn("AMP","float");
	  bptx_table_B2.addColumn("AMP","float");

	  bptx_table_B1.addColumn("POS","float");
	  bptx_table_B2.addColumn("POS","float");

	  bptx_table_B1.addColumn("PHASE","int");
	  bptx_table_B2.addColumn("PHASE","int");

	  // Bunch crossing numbers
	  parsed = toolbox::parseTokenList(*it,",");
	  bptxB_BX1_str = toolbox::printTokenList(parsed, ", ");

	  if ((unsigned int)parsed.size()!=bptxB_nB1  && bptxB_BX1_str!="0"){
	      std::stringstream msg;
	      msg<<"Big problem while parsing the input files! \n Number of Bunches don't match up!   b1=" <<bptxB_nB1<<"  b1="<<parsed.size();
	      LOG4CPLUS_WARN(getApplicationLogger(),msg.str());
	    }

	  //LOG4CPLUS_DEBUG(getApplicationLogger(), bptx_table_B1.toString());
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::UnsignedInteger xx(toolbox::toLong(*p));
	    bptx_table_B1.setValueAt(row,"BX",xx);
	  }
	  it++;


	  bptxB_nB2.fromString(*it);
	  it++;

	  parsed = toolbox::parseTokenList(*it,",");
	  bptxB_BX2_str = toolbox::printTokenList(parsed, ", ");

	  if ((unsigned int)parsed.size()!=bptxB_nB2  && bptxB_BX2_str!="0"){
	      std::stringstream msg;
	      msg<<"Big problem while parsing the input files! \nNumber of Bunches don't match up!   b2=" <<bptxB_nB2<<"  b2="<<parsed.size();
	      LOG4CPLUS_WARN(getApplicationLogger(),msg.str());
	    }

	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::UnsignedInteger xx(toolbox::toLong(*p));
	    bptx_table_B2.setValueAt(row,"BX",xx);
	  }

	  std::advance(it,3);

	  // Absolute time position measurement on the scope (from zero-crossing)
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B1_pos_zc = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p));
	    bptx_table_B1.setValueAt(row,"POS",xx);
	  }

	  it++;

	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B2_pos_zc = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p));
	    bptx_table_B2.setValueAt(row,"POS",xx);
	  }

	  it++;

	  // Pulse aplitudes:
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B1_amp = toolbox::printTokenList(parsed, ", ");

	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p));
	    bptx_table_B1.setValueAt(row,"AMP",xx);
	  }

	  it++;

	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B2_amp = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p));
	    bptx_table_B2.setValueAt(row,"AMP",xx);
	  }

	  it++;


	  /*
	  // If this is commented out,
	  // then the integrals are calculated as Amp*Len, see below
	  // ---
	  bptxB_TOT_INT_B1 = 0;
	  // Half integrals. To be transformed to intensity
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B1_int = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p)); xx=xx*m_ItoIfactor_B1;; //Correction factor to get from pulse integral to Intensity
	    bptx_table_B1.setValueAt(row,"INT",xx);
	    bptxB_TOT_INT_B1 = bptxB_TOT_INT_B1 + xx;
	    //std::cout<<"ItoI factor = "<<m_ItoIfactor_B1<<bptxB_TOT_INT_B1<<"  "<<xx.toString()<<std::endl;
	  }
	  bptxB_AVG_INT_B1 = bptxB_TOT_INT_B1/bptxB_nB1;

	  it++;

	  bptxB_TOT_INT_B2 = 0;
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B2_int = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p)); xx=xx*m_ItoIfactor_B2;
	    bptx_table_B2.setValueAt(row,"INT",xx);
	    bptxB_TOT_INT_B2 = bptxB_TOT_INT_B2 + xx;
	  }
	  bptxB_AVG_INT_B2 = bptxB_TOT_INT_B2/bptxB_nB2;

	  it++;
	  */

	  // if the INT calculation above is used, this needs to be commented out:
	  std::advance(it,2);

	  // Length of the pulses

	  xdata::Float bptxB_TOT_LEN_B1 = 0;
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B1_len = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p)); xx=xx*1E9; //Get it in nanoseconds
	    bptx_table_B1.setValueAt(row,"LEN",xx);
	    bptxB_TOT_LEN_B1 = bptxB_TOT_LEN_B1 + xx;
	  }

	  if ((int)bptxB_nB1!=0) bptxB_AVG_LEN_B1 = bptxB_TOT_LEN_B1/bptxB_nB1;
	  else bptxB_AVG_LEN_B1=0;

	  it++;

	  xdata::Float bptxB_TOT_LEN_B2 = 0;
	  parsed = toolbox::parseTokenList(*it,",");
	  //bptxB_B2_len = toolbox::printTokenList(parsed, ", ");
	  for (std::list<std::string>::iterator p = parsed.begin(); p!=parsed.end(); p++){
	    size_t row = std::distance(parsed.begin(),p);
	    xdata::Float xx(toolbox::toDouble(*p)); xx=xx*1E9;
	    bptx_table_B2.setValueAt(row,"LEN",xx);
	    bptxB_TOT_LEN_B2 = bptxB_TOT_LEN_B2 + xx;
	  }
	  if ((int)bptxB_nB2!=0) bptxB_AVG_LEN_B2 = bptxB_TOT_LEN_B2/bptxB_nB2;
	  else bptxB_AVG_LEN_B2=0;


	  // ---
	  // INTegral calcualation as a simple product: AMP*LEN
	  // ---
	  xdata::Serializable * s;

          bptxB_TOT_INT_B1 = 0;
          for (size_t j = 0; j < bptx_table_B1.getRowCount(); j++ ){
	    s = bptx_table_B1.getValueAt(j, "LEN"); xdata::Float l = toolbox::toDouble(s->toString());
            s = bptx_table_B1.getValueAt(j, "AMP"); xdata::Float a = toolbox::toDouble(s->toString());

	    //std::cout<<"j = "<<j<<"   value = "<<s->toString()<<std::endl;
	    xdata::Float protons = a*l*m_AtoIfactor_B1*1E-4;
            bptx_table_B1.setValueAt(j,"INT",protons);
            bptxB_TOT_INT_B1 = bptxB_TOT_INT_B1 + protons;

            //std::cout<<"AtoI factor = "<<m_AtoIfactor_B1<<" TOT INT = "<<bptxB_TOT_INT_B1<<"  "<<protons.toString()
	    //	     <<" ampl = "<<a<<"  len="<<l<<std::endl;

          }
          if ((int)bptxB_nB1!=0) bptxB_AVG_INT_B1 = bptxB_TOT_INT_B1/bptxB_nB1;
	  else bptxB_AVG_INT_B1=0;

          bptxB_TOT_INT_B2 = 0;
          for (size_t j = 0; j < bptx_table_B2.getRowCount(); j++ ){
	    s = bptx_table_B2.getValueAt(j, "LEN"); xdata::Float l = toolbox::toDouble(s->toString());
            s = bptx_table_B2.getValueAt(j, "AMP"); xdata::Float a = toolbox::toDouble(s->toString());

	    xdata::Float protons = a*l*m_AtoIfactor_B2*1E-4;
            bptx_table_B2.setValueAt(j,"INT",protons);
            bptxB_TOT_INT_B2 = bptxB_TOT_INT_B2 + protons;
            //std::cout<<"AtoI factor = "<<m_AtoIfactor_B2<<bptxB_TOT_INT_B2<<"  "<<protons.toString()<<std::endl;
          }
          if ((int)bptxB_nB2!=0) bptxB_AVG_INT_B2 = bptxB_TOT_INT_B2/bptxB_nB2;
	  else bptxB_AVG_INT_B2=0;

	  // END of INTegral calculation



	  std::advance(it,8);
	  bptxB_nCol.fromString(*it);
	  it++;
	  bptxB_dT.fromString(*it);



	  LOG4CPLUS_DEBUG(getApplicationLogger(), "Processed the line. \n Bunches Script version ="
			  << bptx_script_vesrion.toString() <<"  status = "<<bptxB_status);
	  LOG4CPLUS_DEBUG(getApplicationLogger(), "Number of bunches, B1 =" << bptxB_nB1 <<"    B2 = "<<bptxB_nB2);

	  //std::cout<<"table initial"<<bptx_table_B1.toString()<<std::endl;


	  //it++;
	  //bptx_status.fromString(*it);

	  // Payloads. Used for publishing to eventing bus.
	  uint16_t paylo_bptx_BX_B1[3564], paylo_bptx_INT_B1[3564], paylo_bptx_LEN_B1[3564];
	  uint16_t paylo_bptx_BX_B2[3564], paylo_bptx_INT_B2[3564], paylo_bptx_LEN_B2[3564];
	  int16_t paylo_bptx_PHASE_B1[3564], paylo_bptx_PHASE_B2[3564];
	  bool paylo_bxconfig1[3564], paylo_bxconfig2[3564];
	  
	  for (size_t z=0; z<3564; z++){
	    // why do I even have to do this... C++ is stupid++.
	    paylo_bptx_BX_B1[z]=paylo_bptx_BX_B2[z]=0;
	    paylo_bptx_INT_B1[z]=paylo_bptx_INT_B2[z]=0;
	    paylo_bptx_LEN_B1[z]=paylo_bptx_LEN_B2[z]=0;
	    paylo_bptx_PHASE_B1[z]=paylo_bptx_PHASE_B2[z]=0;
	    paylo_bxconfig1[z]=paylo_bxconfig2[z]=0;
	  }

	  // Beam 1 payloads:
	  bptxB_AVG_PHA_B1=0;
	  for (size_t j = 0; j < bptx_table_B1.getRowCount(); j++ ){
	    s = bptx_table_B1.getValueAt(j, "BX");
	    const uint16_t tmp_bx = toolbox::toLong(s->toString());
	    if (tmp_bx<1 || tmp_bx>3564) continue; //just a precaution. this should never happen
	    paylo_bptx_BX_B1[tmp_bx-1] = tmp_bx;
	    paylo_bxconfig1[tmp_bx-1] = true;
	    s = bptx_table_B1.getValueAt(j, "INT");
	    xdata::Float tmp_val = toolbox::toDouble(s->toString());
	    paylo_bptx_INT_B1[tmp_bx-1] = round(tmp_val*1E4);

	    s = bptx_table_B1.getValueAt(j, "LEN");
	    tmp_val = toolbox::toDouble(s->toString());
            paylo_bptx_LEN_B1[tmp_bx-1] = int(tmp_val*1000); // This will be in picoseconds
	    //s = bptx_table_B1.getValueAt(j, "AMP");

	    // Putting the Phases in the table (need to do it here, and not where we set "POS"
	    // becasue we need BX number for calculation)
	    s = bptx_table_B1.getValueAt(j, "POS");
	    xdata::Double tmp_pos = toolbox::toDouble(s->toString());
	    if (tmp_bx > 3300)
	      tmp_pos = tmp_pos + ORB*1e-9;
	    
	    xdata::Integer phase = 1e12*tmp_pos - 1e3*(BeginOffset  +  (tmp_bx-1)*BXlength) +   40;
            bptx_table_B1.setValueAt(j,"PHASE",phase);
	    bptxB_AVG_PHA_B1 = bptxB_AVG_PHA_B1 + phase;
	    
	    paylo_bptx_PHASE_B1[tmp_bx-1] = int(phase); // This will be in picoseconds
	    
	    //if (j<10)
	    //std::cout<<"B1, BaX="<<tmp_bx<<"  tmp_pos="<<tmp_pos<<"  phase="<<phase<<std::endl;
	  }
	  if ((int)bptxB_nB1!=0) bptxB_AVG_PHA_B1 = bptxB_AVG_PHA_B1/bptxB_nB1;
	  else bptxB_AVG_PHA_B1 = 0;
	  
	  // Beam 2 payloads:
	  bptxB_AVG_PHA_B2=0;
	  for (size_t j = 0; j < bptx_table_B2.getRowCount(); j++ ){
	    s = bptx_table_B2.getValueAt(j, "BX");
	    const uint16_t tmp_bx = toolbox::toLong(s->toString());
	    if (tmp_bx<1 || tmp_bx>3564) continue; //just a precaution. this should never happen
	    paylo_bptx_BX_B2[tmp_bx-1] = tmp_bx;
	    paylo_bxconfig2[tmp_bx-1] = true;
	    s = bptx_table_B2.getValueAt(j, "INT");
	    xdata::Float tmp_val = toolbox::toDouble(s->toString());
	    paylo_bptx_INT_B2[tmp_bx-1] = round(tmp_val*1E4);

	    s = bptx_table_B2.getValueAt(j, "LEN");
	    tmp_val = toolbox::toDouble(s->toString());
            paylo_bptx_LEN_B2[tmp_bx-1] = int(tmp_val*1000); // This will be in picoseconds

	    s = bptx_table_B2.getValueAt(j, "POS");
	    xdata::Double tmp_pos = toolbox::toDouble(s->toString());
	    if (tmp_bx > 3300)
	      tmp_pos = tmp_pos + ORB*1e-9;

	    xdata::Integer phase = 1e12*tmp_pos - 1e3*(BeginOffset  +  (tmp_bx-1)*BXlength) + 2600;
            bptx_table_B2.setValueAt(j,"PHASE",phase);
	    bptxB_AVG_PHA_B2 = bptxB_AVG_PHA_B2 + phase;

            paylo_bptx_PHASE_B2[tmp_bx-1] = int(phase); // This will be in picoseconds

	    //if (j<10)
	    //std::cout<<"B2, BX="<<tmp_bx<<"  tmp_pos="<<tmp_pos<<"  phase="<<phase<<std::endl;

	  }

	  if ((int)bptxB_nB2!=0) bptxB_AVG_PHA_B2 = bptxB_AVG_PHA_B2/bptxB_nB2;
	  else bptxB_AVG_PHA_B2 = 0;
	  
	  for (int i=0; i<3564; ++i) {
	    if (paylo_bptx_BX_B1[i]!=0)
	      paylo_bptx_PHASE_B1[i] -= bptxB_AVG_PHA_B1;
	    if (paylo_bptx_BX_B2[i]!=0)
	      paylo_bptx_PHASE_B2[i] -= bptxB_AVG_PHA_B2;
	  }
	  /*
	  std::cout<<" Start loop over payloads"<<std::endl;
	  for (int i=0; i<3564; ++i) {
	    if (paylo_bptx_INT_B1[i]!=0)
	      std::cout<<i<<" B1  Val = "<<paylo_bptx_INT_B1[i]<<std::endl;
	    if (paylo_bptx_INT_B2[i]!=0)
	      std::cout<<i<<" B2  Val = "<<paylo_bptx_INT_B2[i]<<std::endl;
	  }
	  std::cout<<" End loop over payloads"<<std::endl;
	  */


	  toolbox::mem::Reference* myMemoryREF = m_poolFactory->getFrame(m_memPool,interface::bril::ScopeDataT::maxsize());
	  myMemoryREF->setDataSize(interface::bril::ScopeDataT::maxsize());
	  interface::bril::ScopeDataT* bunchess_data_paylo = (interface::bril::ScopeDataT*)(myMemoryREF->getDataLocation());

	  bunchess_data_paylo->setTime(cms_fill, cms_run, cms_ls, cms_nb, ScopeACQ.sec(), 0);
          bunchess_data_paylo->setFrequency(64);
	  bunchess_data_paylo->setResource(interface::bril::DataSource::BPTX,0,0,interface::bril::StorageType::COMPOUND);
	  bunchess_data_paylo->setTotalsize(interface::bril::ScopeDataT::maxsize());

	  std::string pdict = interface::bril::ScopeDataT::payloaddict();
	  
	  const uint16_t n1 = bptxB_nB1, n2 = bptxB_nB2, n1and2=bptxB_nCol;
	  interface::bril::CompoundDataStreamer cds( pdict );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "nB1", &n1);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "nB2", &n2);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "nCol", &n1and2);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "BXID_B1", paylo_bptx_BX_B1 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "BXID_B2", paylo_bptx_BX_B2 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "INT_B1", paylo_bptx_INT_B1 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "INT_B2", paylo_bptx_INT_B2 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "LEN_B1", paylo_bptx_LEN_B1 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "LEN_B2", paylo_bptx_LEN_B2 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "PHASE_B1", paylo_bptx_PHASE_B1 );
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "PHASE_B2", paylo_bptx_PHASE_B2 );

	  const uint16_t meanLen1 = round(bptxB_AVG_LEN_B1*1E3);
	  const uint16_t meanLen2 = round(bptxB_AVG_LEN_B2*1E3);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AVG_LEN_B1", &meanLen1);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AVG_LEN_B2", &meanLen2);

	  const uint16_t atoi_1 = m_AtoIfactor_B1;
	  const uint16_t atoi_2 = m_AtoIfactor_B2;
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AtoIcorB1", &atoi_1);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AtoIcorB2", &atoi_2);

	  const int16_t meanPhase1 = round(bptxB_AVG_PHA_B1);
	  const int16_t meanPhase2 = round(bptxB_AVG_PHA_B2);
	  // std::cout<<" Phases before loading: b1="<<meanPhase1 <<"  b2="<<meanPhase2<<std::endl;
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AVG_PHASE_B1", &meanPhase1);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "AVG_PHASE_B2", &meanPhase2);
	  
	  const float totInt1 = bptxB_TOT_INT_B1*1E11;
	  const float totInt2 = bptxB_TOT_INT_B2*1E11;
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "TOT_INT_B1", &totInt1);
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "TOT_INT_B2", &totInt2);

	  const float dT = bptxB_dT;
	  cds.insert_field(bunchess_data_paylo->payloadanchor, "DeltaT", &dT);

	  // Memory will be released insede the publish method:
	  doPublish( interface::bril::ScopeDataT::topicname(), myMemoryREF, pdict );
	  xdata::Table::Reference dipmessage( new xdata::Table );
	  dipmessage->addColumn("ACQ_TIMESTAMPSEC","int 64");
	  dipmessage->addColumn("TOT_INT_B1","float");
	  dipmessage->addColumn("TOT_INT_B2","float");
	  dipmessage->addColumn("AVG_LEN_B1","float");
	  dipmessage->addColumn("AVG_LEN_B2","float");
	  dipmessage->addColumn("LEN_B1","vector float");
	  dipmessage->addColumn("LEN_B2","vector float");
	  dipmessage->addColumn("INT_B1","vector float");
	  dipmessage->addColumn("INT_B2","vector float");
	  dipmessage->addColumn("AVG_PHASE_B1","float");
	  dipmessage->addColumn("AVG_PHASE_B2","float");
	  dipmessage->addColumn("PHASE_B1","vector float");
	  dipmessage->addColumn("PHASE_B2","vector float");
	  //ticket JIRA CMSBRIL-50
	  //divide the values of m_avg_len_b1 and m_avg_len_b2 by 1000 before publication
	  xdata::Integer64 acq_timestampsec = (long long)cms_timestamp;
	  dipmessage->setValueAt(0,"ACQ_TIMESTAMPSEC",acq_timestampsec);
	  xdata::Float tot_int_b1 = totInt1;
	  xdata::Float tot_int_b2 = totInt2;
	  dipmessage->setValueAt(0,"TOT_INT_B1",tot_int_b1);
	  dipmessage->setValueAt(0,"TOT_INT_B2",tot_int_b2);

	  xdata::Float avg_len_b1 = float(meanLen1)/1000.;
	  xdata::Float avg_len_b2 = float(meanLen2)/1000.;
	  dipmessage->setValueAt(0,"AVG_LEN_B1",avg_len_b1);
	  dipmessage->setValueAt(0,"AVG_LEN_B2",avg_len_b2);
	  xdata::Vector<xdata::Float> len_b1;
	  xdata::Vector<xdata::Float> len_b2;
	  xdata::Vector<xdata::Float> int_b1;	  
	  xdata::Vector<xdata::Float> int_b2;
	  xdata::Vector<xdata::Float> phase_b1;
	  xdata::Vector<xdata::Float> phase_b2;
	  for( int i=0; i<3564; ++i ){
	    xdata::Float v;
	    v =  float(paylo_bptx_LEN_B1[i])/1e3;
	    len_b1.push_back(v);
	    v =  float(paylo_bptx_LEN_B2[i])/1e3;
	    len_b2.push_back(v);
	    v = float(paylo_bptx_INT_B1[i])*1e7;
	    int_b1.push_back(v);
	    v = float(paylo_bptx_INT_B2[i])*1e7;
	    int_b2.push_back(v);
	    v = float(paylo_bptx_PHASE_B1[i])/1e3;
	    phase_b1.push_back(v);
	    v = float(paylo_bptx_PHASE_B1[i])/1e3;
	    phase_b2.push_back(v);
	  }
	  dipmessage->setValueAt(0,"LEN_B1",len_b1);
	  dipmessage->setValueAt(0,"LEN_B2",len_b2);
	  dipmessage->setValueAt(0,"INT_B1",int_b1);
	  dipmessage->setValueAt(0,"INT_B2",int_b2);
	  dipmessage->setValueAt(0,"PHASE_B1",phase_b1);
	  dipmessage->setValueAt(0,"PHASE_B2",phase_b2);

	  xdata::Float avg_phase_b1 = float(meanPhase1)/1000.;
	  xdata::Float avg_phase_b2 = float(meanPhase2)/1000.;
	  dipmessage->setValueAt(0,"AVG_PHASE_B1",avg_phase_b1);
	  dipmessage->setValueAt(0,"AVG_PHASE_B2",avg_phase_b2);
	  std::string dipname = m_dipPubRoot.value_+"LHC/BPTX-BunchData";
	  do_publish_to_dipbus(dipname,dipmessage);  
	 
	  myMemoryREF = m_poolFactory->getFrame(m_memPool,interface::bril::bxconfigT::maxsize());
	  myMemoryREF->setDataSize(interface::bril::bxconfigT::maxsize());
	  interface::bril::bxconfigT* bxconfig_data_paylo = (interface::bril::bxconfigT*)(myMemoryREF->getDataLocation());

          bxconfig_data_paylo->setTime(cms_fill, cms_run, cms_ls, cms_nb, cms_timestamp, 0);
          bxconfig_data_paylo->setFrequency(64);
	  bxconfig_data_paylo->setResource(interface::bril::DataSource::BPTX,0,0,interface::bril::StorageType::COMPOUND);
	  bxconfig_data_paylo->setTotalsize(interface::bril::bxconfigT::maxsize());

	  const uint16_t sou = 5;
	  pdict = interface::bril::bxconfigT::payloaddict();
	  interface::bril::CompoundDataStreamer cds2( pdict );
	  cds2.insert_field(bxconfig_data_paylo->payloadanchor, "beam1", paylo_bxconfig1);
	  cds2.insert_field(bxconfig_data_paylo->payloadanchor, "beam2", paylo_bxconfig2);
	  cds.insert_field(bxconfig_data_paylo->payloadanchor, "datasource", &sou);
	  doPublish( interface::bril::bxconfigT::topicname(), myMemoryREF, pdict );

	  // -----------------------------------
	  // Prepare monitoring infoSpace here |
	  // ---------  -------     -----------
	  if (m_fireflash) { // This is eneabled by the timer

	    xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("fillnum") );
	    p_fillnum->value_ = cms_fill;
	    xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("runnum") );
	    p_runnum->value_ = cms_run;
	    xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("lsnum") );
	    p_lsnum->value_ = cms_ls;
	    xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_monInfoSpace->find("timestamp") );
	    p_timestamp->value_.sec( ScopeACQ.sec() );
	    p_timestamp->value_.usec( 0 ); // zero, because who cares about usec

	    xdata::Float* p_deltaT = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("deltaT") );
	    p_deltaT->value_ = bptxB_dT;
	    
	    xdata::Float* p_avg_b1 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_phase_b1") );
	    p_avg_b1->value_ = bptxB_AVG_PHA_B1;
	    xdata::Float* p_avg_b2 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_phase_b2") );
	    p_avg_b2->value_ = bptxB_AVG_PHA_B2;

	    xdata::Float* i_avg_b1 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_intensity_b1") );
	    i_avg_b1->value_ = bptxB_AVG_INT_B1;
	    xdata::Float* i_avg_b2 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_intensity_b2") );
	    i_avg_b2->value_ = bptxB_AVG_INT_B2;

	    xdata::Float* l_avg_b1 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_length_b1") );
	    l_avg_b1->value_ = bptxB_AVG_LEN_B1;
	    xdata::Float* l_avg_b2 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("avg_length_b2") );
	    l_avg_b2->value_ = bptxB_AVG_LEN_B2;

	    xdata::Float* i_tot_b1 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("tot_intensity_b1") );
	    i_tot_b1->value_ = bptxB_TOT_INT_B1;
	    xdata::Float* i_tot_b2 = dynamic_cast< xdata::Float* >( m_monInfoSpace->find("tot_intensity_b2") );
	    i_tot_b2->value_ = bptxB_TOT_INT_B2;


	    xdata::Vector<xdata::Integer>* p_b1 =  dynamic_cast< xdata::Vector<xdata::Integer>* >( m_monInfoSpace->find("phase_b1") );
	    xdata::Vector<xdata::Integer>* p_b2 =  dynamic_cast< xdata::Vector<xdata::Integer>* >( m_monInfoSpace->find("phase_b2") );
	    p_b1->clear();
	    p_b2->clear();

	    xdata::Vector<xdata::UnsignedInteger>* i_b1 =  dynamic_cast< xdata::Vector<xdata::UnsignedInteger>* >( m_monInfoSpace->find("intensity_b1") );
	    xdata::Vector<xdata::UnsignedInteger>* i_b2 =  dynamic_cast< xdata::Vector<xdata::UnsignedInteger>* >( m_monInfoSpace->find("intensity_b2") );
	    i_b1->clear();
	    i_b2->clear();

	    xdata::Vector<xdata::UnsignedInteger>* l_b1 =  dynamic_cast< xdata::Vector<xdata::UnsignedInteger>* >( m_monInfoSpace->find("length_b1") );
	    xdata::Vector<xdata::UnsignedInteger>* l_b2 =  dynamic_cast< xdata::Vector<xdata::UnsignedInteger>* >( m_monInfoSpace->find("length_b2") );
	    l_b1->clear();
	    l_b2->clear();

	    for (size_t b=0; b<3564; ++b){
	      if (paylo_bxconfig1[b]==0){
		p_b1->push_back(0);
		i_b1->push_back(0);
		i_b1->push_back(0);
	      }
	      else {
		p_b1->push_back(paylo_bptx_PHASE_B1[b]);
		i_b1->push_back(paylo_bptx_INT_B1[b]);
		l_b1->push_back(paylo_bptx_LEN_B1[b]);
	      }
	      if (paylo_bxconfig2[b]==0){
		p_b2->push_back(0);
		i_b2->push_back(0);
		l_b2->push_back(0);
	      }
	      else{
		p_b2->push_back(paylo_bptx_PHASE_B2[b]);
		i_b2->push_back(paylo_bptx_INT_B2[b]);
		l_b2->push_back(paylo_bptx_LEN_B2[b]);
	      }
	    }

	    
	    // Here we will fire into monitoring info space
	    LOG4CPLUS_DEBUG(getApplicationLogger(), "Firing Info into Space! Elon Musk is our god.");
	    try{
	      m_monInfoSpace->fireItemGroupChanged( m_flashfields, this);
	    }catch(xdata::exception::Exception& e){
	      std::string msg("Failed to fire BPTX flashlist");
	      LOG4CPLUS_ERROR(getApplicationLogger(), msg+xcept::stdformat_exception_history(e));
	      
	    }
	    
	    m_fireflash = false;
	  }

	  // End of Flash, end of List, end of Space
	  // ---------------
	  
      	  // Un-lock this thread
	  m_applock.give();

	}

      if (!ifs1.eof())
	LOG4CPLUS_FATAL(getApplicationLogger(), "The END of File is NOT EOF?! Don't know whats going on here...");

      ifs1.clear();
    }


  if (ifs2.is_open())
    {
      //LOG4CPLUS_DEBUG(getApplicationLogger(), "Timing data file open");
      std::string line;
      while (std::getline(ifs2, line))
	{
	  if (line.substr(0,1)=="#") continue;
	  lastReadTime2 = toolbox::TimeVal::gettimeofday();

	  LOG4CPLUS_DEBUG(getApplicationLogger(), "\n Timing data. This line is to be processed and published:\n \t" << line.substr(0,100));
	  std::list<std::string> dataScope2 = toolbox::parseTokenList(line," ");
	  //std::cout<<toolbox::printTokenList(dataScope2, " -- ")<<std::endl;

	  // Check that we have all data in parsed input:
	  LOG4CPLUS_DEBUG(getApplicationLogger(), "\n Number of parsed entries:  "<<dataScope2.size());
	  if (dataScope2.size()!=27){
	    LOG4CPLUS_WARN(getApplicationLogger(), "The line exists, but we have to skip it because it does not have correct number of entries: "
			   <<dataScope2.size());
	    continue;
	  }

	  std::list<std::string>::iterator it = dataScope2.begin();
	  //bptx_script_vesrion.fromString(*it);
	  it++;

	  // Lock it from Web method (Default)
	  m_applock.take();

	  bptxT_acq_time.fromString(*it);


	  std::advance(it,7);

	  bptxT_nB1.fromString(*it);
	  it++;

	  bptxT_nB2.fromString(*it);
	  it++;

	  std::advance(it,10);

	  if ( (*it) == "-")
	    bptxT_dT = -0.99;
	  else
	    bptxT_dT.fromString(*it);
	  //std::cout<<"deltaT? = "<<*it<<std::endl;

	  std::advance(it,5);
	  bptxT_nCol.fromString(*it);

	  m_applock.give();


      }
      if (!ifs2.eof())
	LOG4CPLUS_FATAL(getApplicationLogger(), "The END of File is NOT EOF?! Don't know whats going on here...");
      ifs2.clear();
    }

  //std::cout<<"Last time 1 = "<<lastReadTime1.toString(toolbox::TimeVal::loc)<<std::endl;
  //std::cout<<"Last time 2 = "<<lastReadTime2.toString(toolbox::TimeVal::loc)<<std::endl;

  sleep(5);

  return true;
}


// I don't use publishing work loop, every publication is made right after reading the input files
/*
bool bril::bptxprocessor::ScopeProcessor2::publishing(toolbox::task::WorkLoop* wl){
  //QueueStoreIt it;
  LOG4CPLUS_DEBUG(getApplicationLogger(), "We are in publishing workLoop");

  // 1. Check if there is something to publish and if all data is ready (unlocked)
  // 2. Do publish that instance
  // 3. Release the flags


  std::stringstream msg;
  try{
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plist.setProperty("ABC", "test of ABC");
    //plist.setProperty("PAYLOAD_DICT",pdict);
    msg << "publish to brildata, ScopeData";
    //msg << "publish to "<<busname<<" , "<<topicname<<", run " << m_lastheader.runnum<<" ls "<< m_lastheader.lsnum <<" nb "<<m_lastheader.nbnum;
    LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
    //this->
    getEventingBus("brildata").publish("ScopeData", 0, plist);

    //getEventingBus("brildata").publish("bptx2", 0, plist);

  }catch(xcept::Exception& e){
    msg<<"Failed to publish to ";
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));

    //this->notifyQualified("fatal",myerrorobj);
  }

  sleep(25);
  return true;
}

*/


void bril::bptxprocessor::ScopeProcessor2::doPublish( const TopicName& topic, toolbox::mem::Reference* memref, const std::string& payloaddict ) {
  toolbox::mem::AutoReference refguard(memref); //guarantee ref is released when refguard is out of scope
  xdata::Properties plist;
  plist.setProperty( "DATA_VERSION", interface::bril::DATA_VERSION );
  if ( !payloaddict.empty() )
    plist.setProperty( "PAYLOAD_DICT", payloaddict );

  LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ":\n Trying to publish "<<topic );

  const std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topic);
  //if ( ret == m_out_topicTobuses.end() )
  //XCEPT_RAISE( exception::EventingError, "Topic is not in the list of known topics" );

  for(TopicStoreIt topicit = ret.first; topicit!=ret.second; ++topicit){
    const BusName bus = topicit->second;

    LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ":\n publishing topic "<<topic<<" to BUS:" << bus
		     << ", a message of size " << memref->getDataSize() );

    toolbox::mem::Reference* ref = memref->duplicate();
    getEventingBus( bus ).publish( topic, ref, plist );

  }
}


void bril::bptxprocessor::ScopeProcessor2::timeExpired(toolbox::task::TimerEvent& e){

  std::stringstream tmsg;
  tmsg << "\n My timer Name: "  << e.getTimerTask()->name << std::endl;
  tmsg << "Actual time: " << e.getTimerTask()->lastExecutionTime.toString(toolbox::TimeVal::loc) << std::endl;
  tmsg << "Schedule time: "  << e.getTimerTask()->schedule.toString(toolbox::TimeVal::loc) << std::endl;

  const toolbox::TimeVal delta = e.getTimerTask()->lastExecutionTime-e.getTimerTask()->schedule;

  // tmsg << "Delta: " << delta.fmt_ss_mls() << std::endl;
  tmsg << "Delta: " << delta.usec() << " usec" << std::endl;
  tmsg << "-------------------------------------------------------" << std::endl;
  tmsg << std::endl;

  LOG4CPLUS_DEBUG(getApplicationLogger(), tmsg.str());

  if (e.getTimerTask()->name=="Cinderella"){
    // New data files are created every midnight. Need to re-attach their links to new files.
    // Close the files and re-open them:
    const std::string timername("Cinderella");
    ifs1.close();
    ifs2.close();

    reOpenDataFile(ifs1, m_file1.toString());
    sleep(1);
    reOpenDataFile(ifs2, m_file2.toString());

    // Now reschedule the timer for the next day:
    LOG4CPLUS_INFO(getApplicationLogger(), "New "+timername+" is to be scheduled");
    const toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
    toolbox::TimeInterval i1;  i1.fromString ("PT24H0M");
    const toolbox::TimeVal nextDay = now + i1;
    //toolbox::TimeVal midnight; midnight.fromString("2015-07-09 11:58:40","%F %T", toolbox::TimeVal::loc);
    toolbox::TimeVal midnight; midnight.fromString(nextDay.toString("%F", toolbox::TimeVal::loc)+" 00:00:05","%F %T", toolbox::TimeVal::gmt);
    timer->schedule(this, midnight, 0, timername);

    LOG4CPLUS_INFO(getApplicationLogger(), "New "+timername+" is scheduled at "+midnight.toString(toolbox::TimeVal::loc));
  }

  if (e.getTimerTask()->name=="fileCheck"){
    bril::bptxprocessor::ScopeProcessor2::checkFileRolover(ifs1, m_file1.toString(), lastReadTime1);
    bril::bptxprocessor::ScopeProcessor2::checkFileRolover(ifs2, m_file2.toString(), lastReadTime2);
  }

  if (e.getTimerTask()->name=="fireInfoSpace"){
    m_fireflash = true;
  }
}


void bril::bptxprocessor::ScopeProcessor2::checkFileRolover(std::ifstream& ifs, const std::string& fname, const toolbox::TimeInterval& lastReadTime){

  const toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
  const toolbox::TimeInterval readInterval(now - lastReadTime);
  const toolbox::TimeInterval deadline(101,0); // A bit bigger than the time between scope acquisitons

  if (readInterval < deadline)
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Checking file: "<<fname<<"\nLooks like nothing rolled over, everything is good, continue your way!");
  else {
    // Need to check if the file on disk was actually updated
    struct stat attrib;
    const int can_stat = stat(fname.c_str(), &attrib);

    if (can_stat==0){
      struct tm* myclock;// create a time structure
      myclock = localtime(&(attrib.st_mtime));
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Stat the file: "<<fname);
      //std::cout<<"Stat the file: "<<fname<<std::endl;
      //std::cout<<"Date:  = "<<myclock->tm_mday<<"-"<<myclock->tm_mon<<"-"<<myclock->tm_year<<std::endl;
      //std::cout<<"Time = "<<myclock->tm_hour<<":"<<myclock->tm_min<<":"<<myclock->tm_sec<<":"<<std::endl;
      std::stringstream modTime;
      modTime<<1900+myclock->tm_year<<"-"<<myclock->tm_mon+1<<"-"<<myclock->tm_mday<<" "<<myclock->tm_hour<<":"<<myclock->tm_min<<":"<<myclock->tm_sec;

      toolbox::TimeVal lastModFileTime; lastModFileTime.fromString(modTime.str(), "%F %T", toolbox::TimeVal::loc);
      toolbox::TimeInterval modInterval(now - lastModFileTime);

      if (modInterval > deadline){
	std::stringstream msg; msg<<std::endl;
	msg<<"Well, the file on disk was not modified for some time. So, nothing to do"<<std::endl;
	msg<<"lastModTime: "<<lastModFileTime.toString(toolbox::TimeVal::loc)<<std::endl;
	msg<<"Time now:    "<<now.toString(toolbox::TimeVal::loc)<<std::endl;
	msg<<"Delta t:     "<<modInterval.toString()<<std::endl;
	LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
      }
      else{
        std::stringstream msg;
	msg<<"Okay, I guess there was a rolovwer... -> Let's re-read the file: "<<fname;
	LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
	ifs.close();
	reOpenDataFile(ifs, fname);
      }

    }
    else{
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Sorry, can't stat the file: "<<fname);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Well, something is wrong. But there is nothing I can do: fix the scope-monitoring scripts first!");
    }
  }
}

void bril::bptxprocessor::ScopeProcessor2::do_publish_to_dipbus(const std::string& dipname, xdata::Table::Reference dipmessage){
  std::stringstream ss;
  xdata::exdr::AutoSizeOutputStreamBuffer outBuffer; 
  xdata::exdr::Serializer serializer; 
  toolbox::mem::Reference* bufRef=0;
  xdata::Properties plist;
  plist.setProperty( "urn:dipbridge:dipname" , dipname );
  try{
    serializer.exportAll(&(*dipmessage),&outBuffer);
    char* buf = outBuffer.getBuffer();
    size_t bufsize = outBuffer.tellp();    
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,bufsize);
    bufRef->setDataSize(bufsize);	
    memcpy( bufRef->getDataLocation(), buf, bufsize );
    this->getEventingBus(m_dipbusName.value_).publish(m_diptopic.value_,bufRef,plist);
  }catch(xcept::Exception& e){
    ss<<"Failed to publish dip "<<dipname<<" to "<<m_diptopic.value_<<" on bus "<<m_dipbusName.toString();
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef) bufRef->release(); 
    //XCEPT_DECLARE_NESTED(bril::bcmlprocessor::exception::BCMLProcessor,myerrorobj,ss.str(),e);
    //this->notifyQualified("fatal",myerrorobj);
  }
}
