#include <cmath>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include "bril/bhmprocessor/Algorithms.h"
#include "bril/bhmprocessor/exception/Exception.h"

namespace bril
 {

  namespace bhmprocessor
   {

/**
 * The default Alogrithm is a simple sum of enabled channels on each side
 */
    class DefaultAggregator : public AggregateAlgorithm
     {

      public:
      DefaultAggregator( /*const*/ xdata::Properties& props ) :
       m_enabled( 40 ), m_online_quality( true )
       {
        for ( int ch = 0; ch != 10; ++ch )
         {
          m_enabled[ch + 0] = bhm::ChannelId( bhm::Beam1, bhm::Near, ch );
          m_enabled[ch + 10] = bhm::ChannelId( bhm::Beam1, bhm::Far, ch );
          m_enabled[ch + 20] = bhm::ChannelId( bhm::Beam2, bhm::Near, ch );
          m_enabled[ch + 30] = bhm::ChannelId( bhm::Beam2, bhm::Far, ch );
         }
        auto masklist = toolbox::parseTokenList( props.getProperty( "masked" ), "," );
        for ( auto it = masklist.begin(); it != masklist.end(); ++it )
         {
          auto toerase = std::find( m_enabled.begin(), m_enabled.end(), bhm::ChannelId( *it ) );
          if ( toerase != m_enabled.end() )
            m_enabled.erase( toerase );
         }
        if ( props.hasProperty( "useonlinequaqlity" ) )
          std::istringstream( props.getProperty( "useonlinequality" ) ) >> m_online_quality;
       }

      void aggregate( const Application::OccupancyCache& cache, Application::AggregatedData& plus, Application::AggregatedData& minus )
       {
        const float charea = 21.237f; //cm^2
        const float luminibblelength = 0.364225f; //s
        plus.m_occupancy.clear();
        plus.m_exposure = 0.f;
        minus.m_occupancy.clear();
        minus.m_exposure = 0.f;
        for ( auto it = cache.begin(); it != cache.end(); ++it )
         {
          if ( std::find( m_enabled.begin(), m_enabled.end(), it->first ) == m_enabled.end() )
            continue;
          if ( m_online_quality && it->second.m_quality[0] > 1 )
            continue;
          if ( it->first.beam() == bhm::Beam1 )
           {
            plus.m_occupancy += it->second.m_histogram[0];
            plus.m_exposure += charea * luminibblelength * it->second.m_nibbles[0];
           }
          else
           {
            minus.m_occupancy += it->second.m_histogram[0];
            minus.m_exposure += charea * luminibblelength * it->second.m_nibbles[0];
           }
         }
        //perfect quality only if 20/20 channels and 64/64 nibbles
        const float totalexposure = charea * 20 * luminibblelength * 64 - 0.0001; //to account for rounding errors
        plus.m_quality = plus.m_exposure < totalexposure ? 2 : 1;
        minus.m_quality = minus.m_exposure < totalexposure ? 2 : 1;
       }

      Application::ChannelIdVector desired_channels() const
       {
        return m_enabled;
       }

      std::string name() const
       {
        return "DefaultAggregator";
       }

      protected:
      Application::ChannelIdVector m_enabled;
      bool m_online_quality;

     };

/**
 * Simple Background algorithm counts events in the background bins.
 * The background from a BX is measured ~2.75 BXs earlier, due to the distance of BHM from the IP.
 */
    class SimpleBackground : public BackgroundAlgorithm
     {

      public:
      SimpleBackground( /*const*/ xdata::Properties& props ) :
       m_bkgd_bin( 0 ), m_normalization( true )
       {
        //depends on the chosen TDC bin for MIB
        if ( props.hasProperty( "bkgdbin" ) )
          std::istringstream( props.getProperty( "bkgdbin" ) ) >> m_bkgd_bin;
        //normalize == true gives Hz/cm^2/10^11; false gives raw counts/LS
        if ( props.hasProperty( "normalize" ) )
          std::istringstream( props.getProperty( "normalize" ) ) >> m_normalization;
       }

      void compute( const Application::AggregatedData& data, const Application::BeamData& beamdata, Application::BackgroundOutput& bkgd )
       {
        bkgd.m_perbx.clear();
        bkgd.m_perbx_error.clear();
        bkgd.m_total = 0.f;
        bkgd.m_total_error = 0.f;
        bkgd.m_quality = 0;
        for ( size_t bx = 0; bx != bkgd.m_perbx.size(); ++bx )
         {
          int mib_sbx = bx * 4 - 12 + m_bkgd_bin; //depending on TDC tuning, bkgd_bin can be 0-2, which means 3-2.5 BXs before the collision
          float rate = data.m_occupancy.at( mib_sbx );
          float relerr = rate > 0.f ? 1.f / std::sqrt( rate ) : 0.f;
          if ( m_normalization )
            rate *= 1.e11f / ( beamdata.m_fbct_incoming[bx] * data.m_exposure );
          bkgd.m_total += rate;
          bkgd.m_perbx[bx] = rate;
          float err = rate * ( relerr + ( m_normalization ? 0.02f : 0.f ) );
          bkgd.m_total_error += err;
          bkgd.m_perbx_error[bx] = err;
         }
       }

      std::string name() const
       {
        return "SimpleBackground";
       }

      protected:
      int m_bkgd_bin;
      bool m_normalization;

     };

/**
 * Leading Bunch Background algorithm counts events in the background bins that are known to have no contamination from collisions
 * These bunches are selected based on the filling scheme.
 * The absence of contamination is defined as a period of no collisions of a certain length prior to the measurement.
 * For a BX, the background contribution is measured ~2.75 BXs earlier, and the collison contribution comes from 6 BXs earlier
 *
 * CHANGED FOR HEAVY IONS: ignoring sub bx, using whole bx
 *
 */
    class LeadingBunchBackground : public SimpleBackground
     {

      public:
      LeadingBunchBackground( /*const*/ xdata::Properties& props ) :
       SimpleBackground( props ), m_min_quiet_bx( 30 )
       {
        if ( props.hasProperty( "minquiet" ) )
          std::istringstream( props.getProperty( "minquiet" ) ) >> m_min_quiet_bx;
       }

      void compute( const Application::AggregatedData& data, const Application::BeamData& beamdata, Application::BackgroundOutput& bkgd )
       {
        bkgd.m_perbx.clear();
        bkgd.m_perbx_error.clear();
        bkgd.m_total = 0.f;
        bkgd.m_total_error = 0.f;
        bkgd.m_quality = 0;
        float nactive = 0.f;
        float count = 0.f;
        for ( size_t bx = 0; bx != bkgd.m_perbx.size(); ++bx )
         {
          if ( !beamdata.m_mask_incoming[bx] ) //expect 0, every count would be noise...
            continue;
          nactive += 1.f;
          //Compute quiet period
          bool coll = false;
          for ( int d = bx - 6; d > int(bx) - 6 - m_min_quiet_bx; --d )
           {
	     int idx = d >= 0 ? d : d + interface::bril::shared::MAX_NBX;
	     coll = coll || ( beamdata.m_mask_incoming[idx] && beamdata.m_mask_outgoing[idx] );
           }
          //Simply ignore non quiet bins
          if ( coll )
            continue;
          count += 1.f;
//        CHANGED FOR HEAVY IONS
//          int mib_sbx = bx * 4 - 12 + m_bkgd_bin; //depending on TDC tuning, bkgd_bin can be 0-2, which means 3-2.5 BXs before the collision
//          float rate = data.m_occupancy.at( mib_sbx );
//        CHANGED FOR HEAVY IONS
          int mib_sbx = bx * 4 - 12;
          float rate = data.m_occupancy.at( mib_sbx ) +
                       data.m_occupancy.at( mib_sbx + 1 ) +
                       data.m_occupancy.at( mib_sbx + 2 ) +
                       data.m_occupancy.at( mib_sbx + 3 );
          float relerr = rate > 0.f ? 1.f / std::sqrt( rate ) : 0.f;
          if ( m_normalization )
            rate *= 1.e11f / ( beamdata.m_fbct_incoming[bx] * data.m_exposure );
          bkgd.m_total += rate;
          bkgd.m_perbx[bx] = rate;
          float err = rate * ( relerr + ( m_normalization ? 0.02f : 0.f ) );
          bkgd.m_total_error += err;
          bkgd.m_perbx_error[bx] = err;
         }
        float scale = nactive / count;
        bkgd.m_total *= scale;
        bkgd.m_total_error *= scale;
       }

      std::string name() const
       {
        return "LeadingBunchBackground";
       }

      protected:
      int m_min_quiet_bx;

     };
/**
 * The Full Background algorithm counts events in the background bins that are either MIB only or MIB + pp.
 * These bunches are selected based on the filling scheme.
 * If they are MIB + pp, the contribution from pp is calculated using bins that should have pp only.
 * This contribution is calculated separately for each train and adjusted with a tuning factor (default 1).
 * The absence of contamination is defined as a period of no collisions of a certain length prior to the measurement.
 * For a BX, the background contribution is measured ~2.75 BXs earlier, and the collison contribution comes from 6 BXs earlier
 */
    class FullBackground : public SimpleBackground
     {

      protected:
      struct train
       {
        size_t m_begin;
        size_t m_end;
        float m_correction;
        float m_correction_relerr;
       };

      public:
      FullBackground( /*const*/ xdata::Properties& props ) :
       SimpleBackground( props ), m_correction_coefficient( 1.0 )
       {
        if ( props.hasProperty( "correctioncoeff" ) )
          std::istringstream( props.getProperty( "correctioncoeff" ) ) >> m_correction_coefficient;
       }

      std::vector<train> find_trains( const Application::AggregatedData& data, const Application::BeamData& beamdata )
       {
        std::vector<train> ret;
        int startoftrain = 0;
        bool intrain = false;
        for ( int bx = 0; bx != interface::bril::shared::MAX_NBX; ++bx )
         {
          if ( beamdata.m_mask_incoming[bx] && beamdata.m_mask_outgoing[bx] )
           {
            if ( !intrain )
             {
              startoftrain = bx;
              intrain = true;
             }
           }
          else
           {
            if ( intrain )
             {
              train tr;
              tr.m_begin = startoftrain;
              tr.m_end = bx;
              tr.m_correction = 0.f;
              for ( int i = bx - 6; i != bx; ++i )
               {
                int mib_sbx = i * 4 + 12 + m_bkgd_bin; //depending on TDC tuning, bkgd_bin can be 0-2, which means 3-2.5 BXs before the collision
                tr.m_correction += data.m_occupancy.at( mib_sbx ) / ( beamdata.m_fbct_incoming[i] * beamdata.m_fbct_outgoing[i] );
               }
              tr.m_correction /= 6.f;
              tr.m_correction_relerr = 0.04f;
              ret.push_back( tr );
//              std::cerr << __func__ << ": Found train starting at " << startoftrain << " and ending at " << bx << ". Computed correction is " << tr.m_correction << std::endl;
//              for ( int b = startoftrain; b != bx; ++b )
//                std::cerr << "Protons for bunch " << b << " - Incoming: " << beamdata.m_fbct_incoming[b] << ", Outgoing: " << beamdata.m_fbct_outgoing[b] << std::endl;
              intrain = false;
             }
           }
         }
        return ret;
       }

      void compute( const Application::AggregatedData& data, const Application::BeamData& beamdata, Application::BackgroundOutput& bkgd )
       {
        bkgd.m_perbx.clear();
        bkgd.m_perbx_error.clear();
        bkgd.m_total = 0.f;
        bkgd.m_total_error = 0.f;
        bkgd.m_quality = 0;
        const std::vector<train> trains = find_trains( data, beamdata );
//        if ( trains.empty() ) //no beam
//          return;
        for ( size_t bx = 0; bx != bkgd.m_perbx.size(); ++bx )
         {
          if ( !beamdata.m_mask_incoming[bx] ) //expect 0, every count would be noise...
            continue;
          int contbx = bx - 6; //The bx that contaminates the current
          contbx = contbx > 0 ? contbx : contbx + interface::bril::shared::MAX_NBX;
          int mib_sbx = bx * 4 - 12 + m_bkgd_bin;
          float rate = data.m_occupancy.at( mib_sbx );
          float relerr = rate > 0.f ? 1.f / std::sqrt( rate ) : 0.f;
          relerr += 0.02f;
//          std::cerr << "BX " << bx << " - raw rate = " << rate;
          if ( beamdata.m_mask_outgoing[contbx] && beamdata.m_mask_outgoing[bx] ) //remove contamination from pp
           {
            auto currenttrain = trains.begin();
            while ( currenttrain != trains.end() )
              if ( currenttrain->m_begin <= bx && bx < currenttrain->m_end )
                break;
              else
                currenttrain++;
            if ( currenttrain == trains.end() )
              XCEPT_RAISE( exception::AlgorithmError, "Train calculation incorrect for BX " + std::to_string( (long long) bx ) );
            float effective_correction = currenttrain->m_correction * beamdata.m_fbct_incoming[contbx] * beamdata.m_fbct_outgoing[contbx];
//            std::cerr << ", effective correction = " << effective_correction;
            if ( rate > 0.f )
              rate -= effective_correction * m_correction_coefficient;
            relerr += currenttrain->m_correction_relerr;
           }
          rate *= 1.e11f / ( beamdata.m_fbct_incoming[bx] * data.m_exposure );
//          std::cerr << ", normalized rate = " << rate << '\n';
          bkgd.m_total += rate;
          bkgd.m_perbx[bx] = rate;
          float err = rate * relerr;
          bkgd.m_total_error += err;
          bkgd.m_perbx_error[bx] = err;
         }
//        std::cerr << "Total rate = " << bkgd.m_total << std::endl;
       }

      std::string name() const
       {
        return "FullBackground";
       }

      protected:
      float m_correction_coefficient;

     };

/**
 * Rnd algo returns random histograms, to test downstream stuff
 */
    class RndBackground : public BackgroundAlgorithm
     {

      public:
      RndBackground()
       {}

      void compute( const Application::AggregatedData&, const Application::BeamData&, Application::BackgroundOutput& bkgd )
       {
        bkgd.m_perbx.clear();
        bkgd.m_perbx_error.clear();
        for ( int i = 0; i != 100; ++i )
          bkgd.m_perbx[std::rand()] = std::rand() / 1.e6f;
        bkgd.m_total = std::rand() / 1.e6f;
        bkgd.m_total_error = std::sqrt( bkgd.m_total );
        bkgd.m_quality = 99;
       }

      std::string name() const
       {
        return "RndBackground";
       }

     };

/**
 * Default Amplitude does nothing
 */
    class DefaultAmplitude : public AmplitudeAlgorithm
     {

      public:
      DefaultAmplitude( /*const*/ xdata::Properties& props ) :
       m_enabled( 40 )
       {
        for ( int ch = 0; ch != 10; ++ch )
         {
          m_enabled[ch + 0] = bhm::ChannelId( bhm::Beam1, bhm::Near, ch );
          m_enabled[ch + 10] = bhm::ChannelId( bhm::Beam1, bhm::Far, ch );
          m_enabled[ch + 20] = bhm::ChannelId( bhm::Beam2, bhm::Near, ch );
          m_enabled[ch + 30] = bhm::ChannelId( bhm::Beam2, bhm::Far, ch );
         }
        auto masklist = toolbox::parseTokenList( props.getProperty( "masked" ), "," );
        for ( auto it = masklist.begin(); it != masklist.end(); ++it )
         {
          auto toerase = std::find( m_enabled.begin(), m_enabled.end(), bhm::ChannelId( *it ) );
          if ( toerase != m_enabled.end() )
            m_enabled.erase( toerase );
         }
       }

      void compute( const Application::AmplitudeCache& cache, Application::AmplitudeOutput& out )
       {
        for ( auto it = cache.begin(); it != cache.end(); ++it )
         {
          if ( std::find( m_enabled.begin(), m_enabled.end(), it->first ) == m_enabled.end() )
            continue;
          Application::AggregateAmplitudeHistogram& h = out[it->first];
          for ( int i = 0; i != 256; ++i )
            for ( int tdc = 0; tdc != 4; ++tdc )
              h[i * 4 + tdc] = it->second.m_histogram[tdc][i];
         }
       }

      Application::ChannelIdVector desired_channels() const
       {
        return m_enabled;
       }

      std::string name() const
       {
        return "DefaultAmplitude";
       }

      protected:
      Application::ChannelIdVector m_enabled;

     };

/**
 * Calibration Amplitude computes relative gain, by using a channel as reference for a group of other channels
 */
    class CalibrationAmplitude : public AmplitudeAlgorithm
     {

      public:
      CalibrationAmplitude()
       {}

      void compute( const Application::AmplitudeCache&, Application::AmplitudeOutput& )
       {}

      Application::ChannelIdVector desired_channels() const
       {
        return Application::ChannelIdVector(); //FIXME
       }

      std::string name() const
       {
        return "CalibrationAmplitude";
       }

     };

    AggregateAlgorithm::~AggregateAlgorithm()
     {}

    AmplitudeAlgorithm::~AmplitudeAlgorithm()
     {}

    BackgroundAlgorithm::~BackgroundAlgorithm()
     {}

    AggregateAlgorithm* AlgorithmFactory::aggregate( /*const*/ xdata::Properties& props )
     {
      std::string name = props.getProperty( "class" );
      if ( name == "DefaultAggregator" )
        return new DefaultAggregator( props );
      XCEPT_RAISE( exception::ConfigurationError, "Aggregation algorithm \"" + name + "\" is not available." );
      return 0;
     }

    AmplitudeAlgorithm* AlgorithmFactory::amplitude( /*const*/ xdata::Properties& props )
     {
      std::string name = props.getProperty( "class" );
      if ( name == "DefaultAmplitude" )
        return new DefaultAmplitude( props );
      if ( name == "CalibrationAmplitude" )
        return new CalibrationAmplitude();
      XCEPT_RAISE( exception::ConfigurationError, "Amplitude algorithm \"" + name + "\" is not available." );
      return 0;
     }

    BackgroundAlgorithm* AlgorithmFactory::background( /*const*/ xdata::Properties& props )
     {
      std::string name = props.getProperty( "class" );
      if ( name == "RndBackground" )
        return new RndBackground();
      if ( name == "SimpleBackground" )
        return new SimpleBackground( props );
      if ( name == "LeadingBunchBackground" )
        return new LeadingBunchBackground( props );
      if ( name == "FullBackground" )
        return new FullBackground( props );
      XCEPT_RAISE( exception::ConfigurationError, "Background algorithm \"" + name + "\" is not available." );
      return 0;
     }

   }

 }
