#include <algorithm>
#include <numeric>

#include "interface/bril/shared/CommonDataFormat.h"

namespace bril
 {

  namespace bhmprocessor
   {

    template <typename BinT, unsigned int N>
    class Histogram
     {

      public:
      typedef BinT value_type;
      typedef BinT* iterator;
      typedef const BinT* const_iterator;

      public:
      Histogram( BinT init = BinT( 0 ) )
       {
        clear( init );
       }

      size_t size() const
       {
        return N;
       }

      void clear( BinT init = BinT( 0 ) )
       {
        for ( iterator it = begin(); it != end(); ++it )
          *it = init;
       }

      iterator begin()
       {
        return m_bins;
       }

      iterator end()
       {
        return m_bins + N;
       }

      const_iterator begin() const
       {
        return m_bins;
       }

      const_iterator end() const
       {
        return m_bins + N;
       }

      value_type& operator [] ( size_t idx )
       {
        return m_bins[idx];
       }

      value_type operator [] ( size_t idx ) const
       {
        return m_bins[idx];
       }

      value_type& at( size_t idx )
       {
        return m_bins[( idx + N ) % N];
       }

      const value_type& at( size_t idx ) const
       {
        return m_bins[( idx + N ) % N];
       }

      Histogram<BinT, N>& operator += ( const Histogram<BinT, N>& rhs )
       {
        const_iterator rhit = rhs.begin();
        for ( iterator it = begin(); it != end(); ++it )
          *it += *rhit++;
        return *this;
       }

      Histogram<BinT, N> operator + ( const Histogram<BinT, N>& rhs ) const
       {
        Histogram<BinT, N> ret( *this );
        return ret += rhs;
       }

      template <typename RhsT>
      Histogram<BinT, N>& operator += ( const interface::bril::shared::Datum<RhsT>& rhs )
       {
        //this const cast is necessary because Datum does not have a const-qualified accessor ...
        interface::bril::shared::Datum<RhsT>& ncrhs = const_cast<interface::bril::shared::Datum<RhsT>&>( rhs );
        for ( size_t idx = 0; idx != size(); ++idx )
          m_bins[idx] += ncrhs.payload()[idx];
        return *this;
       }

      template <typename RhsT>
      Histogram<BinT, N> operator + ( const interface::bril::shared::Datum<RhsT>& rhs ) const
       {
        Histogram<BinT, N> ret( *this );
        return ret += rhs;
       }

      template <unsigned int M>
      Histogram<BinT, N / M> rebin() const
       {
        static_assert( N % M == 0, "Rebinning requires exact division" );
        Histogram<BinT, N / M> ret;
        for ( size_t idx = 0; idx != ret.size(); ++idx )
          for ( size_t j = 0; j != M; ++j )
            ret[idx] += m_bins[idx * M + j];
        return ret;
       }

      template <unsigned int M>
	Histogram<BinT, N / M> resample( unsigned int offset = 0 ) const
       {
        static_assert( N % M == 0, "Resampling requires exact division" );
        offset = offset % M;
        Histogram<BinT, N / M> ret;
        for ( size_t idx = 0; idx <= ret.size(); ++idx )
          ret[idx] += m_bins[idx * M + offset];
        return ret;
       }

      double average() const
       {
        return std::accumulate( begin(), end(), 0.0 ) / N;
       }

      private:
      BinT m_bins[N];

     };

    template <typename Stream, typename BinT, unsigned int N>
    Stream& operator << ( Stream& out, const Histogram<BinT, N>& h )
     {
      auto it = h.begin();
      while ( it != h.end() - 1 )
        out << *it++ << ',';
      out << *it;
      return out;
     }

   }

 }

