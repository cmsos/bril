
#ifndef _bril_bcm1fprocessor_Application_h_
#define _bril_bcm1fprocessor_Application_h_
#include <string>
#include <list>
#include <array>
#include <deque>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xgi/framework/UIManager.h"

#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/EventDispatcher.h"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include "toolbox/BSem.h"
#include "log4cplus/logger.h"
namespace toolbox {
    namespace task {
        class WorkLoop;
    }
}
namespace bril {
    namespace webutils {
        class WebChart;
    }
    namespace bcm1fprocessor {
        enum class Tab {DEFAULT, PLOTTING, RHU, BPTX};

        class Application : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener
        {
          public:
            struct InData
            {
                InData (unsigned short n, toolbox::mem::Reference* d) : nbcount (n), dataref (d) {}
                InData() : nbcount (0), dataref (0) {}
                unsigned short nbcount;
                toolbox::mem::Reference* dataref;
            };

            static const int s_nBcm1fChannels = 48;
            static const int s_nAllChannels = 64;
            static const int s_maxnbx = 3564;
            static const int s_num_nb_to_collect = 4;
            static const int s_minimum_present_channels = 4;
            static const int s_cache_lifetime_sec = 60;

            static constexpr float s_areaSilicon = 0.0289; // cm^2. 1.7mm*1.7mm = 0.17cm * 0.17cm = 0.0289 cm^2
            static constexpr float s_sec_per_nibble = 0.3649536;

          public:
            XDAQ_INSTANTIATOR();
            // constructor
            Application (xdaq::ApplicationStub* s);
            // destructor
            ~Application ();
            // xgi(web) callback
            void createTabBar (/*xgi::Input* in,*/ xgi::Output* out, Tab tab);
            void Default (xgi::Input* in, xgi::Output* out);
            // plotting webpage
            void plotting (xgi::Input* in, xgi::Output* out);
            void RawRHUhists (xgi::Input* in, xgi::Output* out);
            void RawBPTXhists (xgi::Input* in, xgi::Output* out);
            // infospace event callback
            virtual void actionPerformed (xdata::Event& e);
            // toolbox event callback
            virtual void actionPerformed (toolbox::Event& e);
            // b2in message callback
            void onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist) ;
            // timer callback
            virtual void timeExpired (toolbox::task::TimerEvent& e);

          private:
            // members
            Tab m_tab;
            // some information about this application
            xdata::InfoSpace* m_appInfoSpace;
#ifdef x86_64_centos7
            const
#endif
            xdaq::ApplicationDescriptor* m_appDescriptor;
            std::string m_classname;

            toolbox::mem::MemoryPoolFactory* m_poolFactory;
            toolbox::mem::Pool* m_memPool;
            unsigned int m_collectnb;
            unsigned int m_collectchannel;
            unsigned int m_numberOfNibbles;
            toolbox::TimeVal m_last_process_time;

            // application lock, it's useful to always include one
            toolbox::BSem m_applock;

            //eventinginput, eventingoutput configuration
            xdata::Vector<xdata::Properties> m_datasources;
            xdata::Vector<xdata::Properties> m_outputtopics;
            std::map<std::string, std::set<std::string> > m_in_busTotopics;
            typedef std::multimap< std::string, std::string > TopicStore;
            typedef std::multimap< std::string, std::string >::iterator TopicStoreIt;
            TopicStore m_out_topicTobuses;
            //count how many outgoing buses not ready
            std::set<std::string> m_unreadybuses;

            // incoming data cache
            typedef std::map<unsigned int, InData*> InDataCache;
            InDataCache m_bcm1fhistcache;
            unsigned int m_lastreceivedsec;

            // outgoing data cache
            interface::bril::shared::DatumHead m_lastheader;

            typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* > QueueStore;
            typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* >::iterator QueueStoreIt;
            QueueStore m_topicoutqueues;
            toolbox::task::WorkLoop* m_publishing;


            // data cache for albedo
            typedef std::deque< std::array<double, s_maxnbx> > AlbedoCache;
            typedef std::deque< std::array<double, s_maxnbx> >::iterator AlbedoIt;
            AlbedoCache m_albedoqueuesi;
            toolbox::task::WorkLoop* m_albedo_calculation;
            double m_albedo_fraction_si[s_maxnbx];
            double m_raw_noise_si;

            // hotfix for bunch mask info being off by 1 bcid sometimes
            xdata::Boolean m_bunchmask_timing_fix;
            // option to turn off bunch collision mask
            // USE ONLY IN EMERGENCY
            xdata::Boolean m_use_collision_mask;


            // counters for number of nibbles received
            //xdata::UnsignedInteger
            xdata::UnsignedInteger m_nHistsDiscarded;

            // background numbers
            xdata::Float m_BG1_plus;
            xdata::Float m_BG2_minus;
            xdata::Float m_BG1_nc;
            xdata::Float m_BG2_nc;
            xdata::Float m_BG1_lead;
            xdata::Float m_BG2_lead;
            xdata::Float m_last4_bkg1[4];
            xdata::Float m_last4_bkg2[4];
            xdata::Float m_last4_bkg1_lead[4];
            xdata::Float m_last4_bkg2_lead[4];
            xdata::Float m_last4_bkg1_nc[4];
            xdata::Float m_last4_bkg2_nc[4];
            /* float m_BG1_plus; */
            /* float m_BG2_minus; */

            // lumi calibration and numbers to publish
            xdata::String m_calibtag;
            xdata::Float m_sigma_vis_si;
            xdata::Float m_calib_at_SBIL_si;
            xdata::Float m_nonlinearity_si;

            xdata::Float m_trip_threshold; //lumi*n_channels threshold for spike and trip automasking
            xdata::Float m_lower_spike_ratio; //maximum lumi_channel/lumi_avg ratio to be considered a trip
            xdata::Float m_upper_spike_ratio; //minimum lumi_channel/lumi_avg ratio to be considered a spike
            xdata::UnsignedInteger m_time_spike; //number of nb4s before a channel can be unmasked after a spike

            //to mask the test pulses per RHU
            xdata::Vector<xdata::Properties> m_tp_mask;
            //to be parsed from the array
            //key: RHU number, value: start/end time
            std::map<unsigned int, unsigned int> m_tp_mask_start;
            std::map<unsigned int, unsigned int> m_tp_mask_end;

            xdata::UnsignedInteger m_albedo_calculation_time;
            xdata::UnsignedInteger m_albedo_queue_length;
            xdata::UnsignedInteger m_noise_calc_start;
            xdata::UnsignedInteger m_noise_calc_end;


            xdata::Float m_lumi_avg;
            xdata::Float m_lumi_avg_raw;
            float m_lumi_bx[s_maxnbx];
            float m_lumi_bx_raw[s_maxnbx];
            float m_raw_uncor_si[s_maxnbx];
            float m_albedoqueue_hist_si_uncor[s_maxnbx];
            float m_albedoqueue_hist_si_cor[s_maxnbx];
            float m_devel_hist1[s_maxnbx];
            float m_devel_hist2[s_maxnbx];
            float m_devel_hist3[s_maxnbx];
            float m_devel_hist4[s_maxnbx];
            float m_fourbinsperbx[56][14265];
            float m_BPTXhist[8][s_maxnbx];
            float m_rates_perchannel_percollidingbunches[48];
            float m_lumi_percshape[4];
            float m_lumi_percshapeRaw[4];
            float m_lumi_perchannel_bx[48][s_maxnbx];
            float m_lumi_perchannel_bxRaw[48][s_maxnbx];
            xdata::String m_albedo_model_si_str;
            std::vector<float> m_albedo_model_si;

            // channel configuration
            xdata::String m_excluded_channels_lumi;
            xdata::String m_excluded_channels_bkgd;
            //bool m_useChannelForLumi[48];
            bool m_useChannelForLumi[64];
            //      bool m_useChannelForBkgd[48];
            // bool m_sensorConnected[64];
            bool m_useChannelForBkgd[64];
            bool m_channel_spike_mask[64];
            unsigned int m_channel_count_nospike[64];
            //double m_channelSigmaVis[48];
            //float m_channelSigmaVis[48];
            double m_channelScaleFactor[48];
            xdata::UnsignedInteger m_nRHUs;

            xdata::Boolean m_verbose;

            xdata::String m_beammode;

            /* xdata::Boolean m_useChannelCalibrations; */

            /* xdata::Vector<xdata::Float> m_channelCalibrations; */
            xdata::Vector<xdata::Float> m_channelAcceptances;
            xdata::Vector<xdata::Float> m_channelEfficiencies;
            xdata::Vector<xdata::Float> m_channelCorrections;

            struct ChannelCounts
            {
                unsigned int channel_number;
                double hit_count;
                bool operator< (const ChannelCounts& rhs) const
                {
                    return (hit_count < rhs.hit_count);
                }
            };

            // storage of total channel rates (excl. TP)
            //double m_totalChannelRate[48];
            //double m_totalChannelRate[64];
            xdata::Vector<xdata::Float> m_totalChannelRate;
            xdata::Vector<xdata::Float> m_lumiChannel;
            xdata::Vector<xdata::Float> m_lumiChannelRaw;

            // bunch masks for background and collisions
            bool m_bkgd1mask[s_maxnbx];
            bool m_bkgd2mask[s_maxnbx];
            bool m_bkgd1leadmask[s_maxnbx];
            bool m_bkgd2leadmask[s_maxnbx];
            bool m_bkgd1NCmask[s_maxnbx];
            bool m_bkgd2NCmask[s_maxnbx];
            bool m_collmask[s_maxnbx];

            // True during vdm scans
            bool m_vdmflag;

            // beam currents for normalization of background
            float m_iTot1;
            float m_iTot2;
            float m_iBunch1[s_maxnbx];
            float m_iBunch2[s_maxnbx];

            // albedo subtraction of background
            unsigned long m_countAlbedoHigherThanBackground;

            //plotting
            bril::webutils::WebChart* m_chart_lumi_raw_si;
            bril::webutils::WebChart* m_chart_albedoqueue_si;
            bril::webutils::WebChart* m_chart_devel;
            bril::webutils::WebChart* m_chart_fourbinsperbx1;
            bril::webutils::WebChart* m_chart_fourbinsperbx2;
            bril::webutils::WebChart* m_chart_fourbinsperbx3;
            bril::webutils::WebChart* m_chart_fourbinsperbx4;
            bril::webutils::WebChart* m_chart_fourbinsperbx5;
            bril::webutils::WebChart* m_chart_fourbinsperbx6;
            bril::webutils::WebChart* m_chart_BPTX;
            bril::webutils::WebChart* m_chart_BPTX_perBX;
          private: //flashlist stuff
            xdata::InfoSpace* m_mon_infospace;
            std::list<std::string> m_mon_varlist;
            std::vector<xdata::Serializable*> m_mon_vars;
            //xdata::String m_mon_beammode;
            xdata::UnsignedInteger m_mon_fill;
            xdata::UnsignedInteger m_mon_run;
            xdata::UnsignedInteger m_mon_ls;
            xdata::UnsignedInteger m_mon_nb;
            xdata::TimeVal m_mon_timestamp;

          private:
            // methods
            //      void do_process(void* dataptr,size_t buffersize);
            void do_process (interface::bril::shared::DatumHead& inheader);
            void clear_cache();
            void hist_accumulateInChannel (InDataCache::iterator channelit, interface::bril::bcm1fhistT* indata);
            bool check_nbstatistics (unsigned short nnbs);
            bool publishing (toolbox::task::WorkLoop* wl);
            bool albedo_calculation (toolbox::task::WorkLoop* wl);
            void do_publish (const std::string& busname, const std::string& topicname, const std::string& pdict, toolbox::mem::Reference* bufRef);
            void subscribeall();
            void makeAggHistsAndTotalRates (interface::bril::shared::DatumHead& inheader);
            void makeBkgNumbers (interface::bril::shared::DatumHead& inheader);
            void makeLumiNumbers (interface::bril::shared::DatumHead& inheader);
            void makeCollidingRates (interface::bril::shared::DatumHead& inheader);
            void makebxhistnumbers (interface::bril::shared::DatumHead& inheader);
            void retrieveSourceHistos (toolbox::mem::Reference* ref, interface::bril::shared::DatumHead* inheader);
            void makeBunchMasks (interface::bril::shared::DatumHead* inheader);
            void setupMonitoring();
            void setupCharts();
            void requestData ( xgi::Input* in, xgi::Output* out );
            template <typename T>
            std::vector<float> filterHistForPlot (T in, int len);

            //assuming 0-indexed channels
            unsigned int get_RHU (unsigned int channel)
            {
                if(channel <= 0 && channel < 8) return 1;
                else if (channel >7 && channel < 16) return 2;
                else if (channel >15 && channel < 24) return 3;
                else if (channel >23 && channel < 32) return 4;
                else if(channel > 31 && channel < 40) return 5;
                else if (channel >39 && channel < 48) return 6;
                else return 999;
            }

            void get_RHU_tp_mask(unsigned int channel, unsigned int& start, unsigned int& end)
            {
                unsigned int RHU = this->get_RHU(channel);
                if (RHU != 999)
                {
                std::map<unsigned int, unsigned int>::iterator start_it = m_tp_mask_start.find(RHU);
                if(start_it != std::end(m_tp_mask_start))
                {
                    start = start_it->second;
                    std::map<unsigned int, unsigned int>::iterator end_it = m_tp_mask_end.find(RHU);
                    if(end_it != std::end(m_tp_mask_end))
                        end = end_it->second;
                    else end = 14232;
                }
                else start = 14136;
                }
                else
                {
                    start = 14136;
                    end = 14232;
                }
            }



            // nocopy protection
            Application (const Application&);
            Application& operator= (const Application&);
        };
    }
}
#endif
