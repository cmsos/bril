// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

// This version is the first update of pltslinkprocessor to work with the
// new 2017 dataflow. This skips the pltslinksource entirely and just sets
// up a zmq listener to listen to the data coming directly from FEDStreamReader.
// A zmq poll is used to listen to both the workloop zmq publisher (to get the
// TCDS data) and the slink zmq publisher.
//
//  -- Paul Lujan, 24 April 2017

// c++ stuff
#include <string>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sys/stat.h>
#include <sys/types.h>
#include <math.h>

// xdaq stuff
#include "cgicc/CgiDefs.h"

#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "eventing/api/exception/Exception.h"
#include "xdata/Properties.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/AutoReference.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"

#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "bril/pltslinkprocessor/zmq.hpp"

// PLT stuff
#include "bril/pltslinkprocessor/PLTEvent.h"
#include "bril/pltslinkprocessor/Application.h"
#include "bril/pltslinkprocessor/exception/Exception.h"
#include "interface/bril/PLTSlinkTopics.hh"

// Sentinel Alarms
#include "sentinel/utils/Alarm.h"
#include "bril/pltslinkprocessor/exception/PLTAlarm.h"

// Nico's stuff for web pages with charts
#include "bril/webutils/WebUtils.h"

using boost::property_tree::ptree;
using boost::property_tree::write_json;

XDAQ_INSTANTIATOR_IMPL (bril::pltslinkprocessor::Application) 

using namespace interface::bril;
using namespace std;

bril::pltslinkprocessor::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this)
{
    this->getEventingBus("brildata").addActionListener(this);
    xgi::framework::deferredbind(this,this,&bril::pltslinkprocessor::Application::Default, "Default");
    xgi::deferredbind( this, this, &bril::pltslinkprocessor::Application::requestData, "requestData" ); //naked data
    b2in::nub::bind(this, &bril::pltslinkprocessor::Application::onMessage);
    m_poolFactory   = toolbox::mem::getMemoryPoolFactory();
    m_appDescriptor = getApplicationDescriptor();
    m_classname     = m_appDescriptor->getClassName();
    m_instanceid    = m_appDescriptor->getInstance();

    std::string memPoolName = m_classname + m_instanceid.toString() + std::string("_memPool");
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
    m_memPool   = m_poolFactory->createPool(urn, allocator);

    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("workloopHost",&m_workloopHost);
    getApplicationInfoSpace()->fireItemAvailable("slinkHost",&m_slinkHost);  
    getApplicationInfoSpace()->fireItemAvailable("activeFEDChannels", &m_activeChannelList);
    getApplicationInfoSpace()->fireItemAvailable("gcFile",&m_gcFile);  
    getApplicationInfoSpace()->fireItemAvailable("alFile",&m_alFile);  
    getApplicationInfoSpace()->fireItemAvailable("trackFile", &m_trackFile);
    getApplicationInfoSpace()->fireItemAvailable("pixMask",&m_pixMask);  
    getApplicationInfoSpace()->fireItemAvailable("baseDir",&m_baseDir);  
    getApplicationInfoSpace()->fireItemAvailable("calibVal", &m_calibVal);
    getApplicationInfoSpace()->fireItemAvailable("MakeLogs", &m_MakeLogs);
    getApplicationInfoSpace()->fireItemAvailable("AlwaysFlash", &m_AlwaysFlash);
    getApplicationInfoSpace()->fireItemAvailable("err_thresh", &m_err_thresh);

    try{
        getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    }
    catch(xcept::Exception & e){
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to add Listener", e);
    }
   
    // output
    //m_outtopicdicts.insert( std::make_pair(pltslinklumiT::topicname(),pltslinklumiT::payloaddict()) );

    nPulseBins = 50;
    pulseMax = 50000.0;
    
    m_acc_perchannel.setSize(16);
    m_eff_perchannel.setSize(16);   
    m_rate_perchannel.setSize(16);
    m_acc_sum.setSize(16);
    m_eff_sum.setSize(16);
    m_rate_sum.setSize(16);
    m_error_perchannel.setSize(16);

    // Set default values for flash list
    m_fill_flash = 0;
    m_run_flash = 0;
    m_ls_flash = 0;
    m_timestamp = toolbox::TimeVal::gettimeofday();
    m_acc_avg = 0;
    m_eff_avg = 0;
    m_error_sum = 0;
    for (unsigned i = 0; i < 16; ++i) {
        m_acc_perchannel.at(i) = 0;
        m_eff_perchannel.at(i) = 0;
        m_error_perchannel.at(i) = 0;
    }
    
    // flashlist 
    std::string nid("pltslink_flash");
    std::string flashurn = createQualifiedInfoSpace(nid).toString();
    m_flashInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(flashurn));
    // Add items to infospace
    m_flashInfoSpace->fireItemAvailable("fill", &m_fill_flash);
    m_flashInfoSpace->fireItemAvailable("run", &m_run_flash);
    m_flashInfoSpace->fireItemAvailable("timestamp", &m_timestamp);
    m_flashInfoSpace->fireItemAvailable("acc_avg", &m_acc_avg);
    m_flashInfoSpace->fireItemAvailable("acc_perchannel", &m_acc_perchannel);
    m_flashInfoSpace->fireItemAvailable("eff_avg", &m_eff_avg);
    m_flashInfoSpace->fireItemAvailable("eff_perchannel", &m_eff_perchannel);

    m_publish_count = 0;

    // flashlist publishing workloop
    toolbox::net::URN flashwlurn("Flashlist_wl", getApplicationDescriptor()->getURN());
    m_publish_flash = toolbox::task::getWorkLoopFactory()->getWorkLoop(flashwlurn.toString(), "waiting");
    m_publish_flash_as = toolbox::task::bind(this, &bril::pltslinkprocessor::Application::publish_flash,"publish_flash");

    // Sentinel alarm
    m_sentinelInfoSpace = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
}

bril::pltslinkprocessor::Application::~Application (){}

void bril::pltslinkprocessor::Application::setupCharts(){
    std::string cb = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";
    webutils::WebChart::property_map channel_propmap;
    webutils::WebChart::property_map this_propmap;
    this_propmap[""] = webutils::WebChart::series_properties();
    
    float binVals[nPulseBins]; 
    float bin_width = (pulseMax)/(nPulseBins);
    std::stringstream binNames;
    binNames << "[";
    for (unsigned int i = 0; i < nPulseBins; ++i) {
        binVals[i] = i*bin_width;
        binNames << binVals[i] << ", ";
    }
    binNames << "]";
    const std::string Binstr = binNames.str();
    
    const unsigned nRows = PLTU::LASTROW;
    const unsigned nCols = PLTU::LASTCOL;
   
    std::stringstream rowNames;
    std::stringstream colNames;
    rowNames << "["; 
    for (unsigned int row = 0; row < nRows; ++row) {
        rowNames << row << ", ";
    }
    rowNames << "]";
    colNames << "["; 
    for (unsigned int col = 0; col < nCols; ++col) {
        colNames << col << ", ";
    }
    colNames << "]";
    const std::string Rowstr = rowNames.str();
    const std::string Colstr = colNames.str();
    
    for (unsigned int i = 0; i < m_nChannels; i++) {
        channel_propmap["Channel "+std::to_string(m_activeChannels[i])] = webutils::WebChart::series_properties();
        m_error_history["Channel "+std::to_string(m_activeChannels[i])] = std::deque<std::pair<unsigned long, int>>();

        m_pulse_vals[std::make_pair(m_activeChannels[i], 0)][""] = std::vector<int>();
        m_pulse_vals[std::make_pair(m_activeChannels[i], 1)][""] = std::vector<int>();
        m_pulse_vals[std::make_pair(m_activeChannels[i], 2)][""] = std::vector<int>();

        m_occ_vals[std::make_pair(m_activeChannels[i], 0)][""] = std::vector<webutils::heatmap_point<int, int, int>>();
        m_occ_vals[std::make_pair(m_activeChannels[i], 1)][""] = std::vector<webutils::heatmap_point<int, int, int>>();
        m_occ_vals[std::make_pair(m_activeChannels[i], 2)][""] = std::vector<webutils::heatmap_point<int, int, int>>();
        
        m_pulse_hist[std::make_pair(m_activeChannels[i], 0)] = new webutils::WebChart( "pulseHist"+std::to_string(m_activeChannels[i])+"ROC0", cb, "type: 'column', zoomType: 'xy', animation: false", "plotOptions: { column: { animation: false, enableMouseTracking: false } }, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 0' },"
           "yAxis: { min: 0, title: { text: 'Entries/bin', useHTML: true } }, xAxis: { type: 'float', categories: "+Binstr+", title: { text: 'Charge (electrons)', useHTML: true } }", this_propmap );
        m_pulse_hist[std::make_pair(m_activeChannels[i], 1)] = new webutils::WebChart( "pulseHist"+std::to_string(m_activeChannels[i])+"ROC1", cb, "type: 'column', zoomType: 'xy', animation: false", "plotOptions: { column: { animation: false, enableMouseTracking: false } }, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 1' },"
           "yAxis: { min: 0, title: { text: 'Entries/bin', useHTML: true } }, xAxis: { type: 'float', categories: "+Binstr+", title: { text: 'Charge (electrons)', useHTML: true } }", this_propmap );
        m_pulse_hist[std::make_pair(m_activeChannels[i], 2)] = new webutils::WebChart( "pulseHist"+std::to_string(m_activeChannels[i])+"ROC2", cb, "type: 'column', zoomType: 'xy', animation: false", "plotOptions: { column: { animation: false, enableMouseTracking: false } }, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 2' },"
           "yAxis: { min: 0, title: { text: 'Entries/bin', useHTML: true } }, xAxis: { type: 'float', categories: "+Binstr+", title: { text: 'Charge (electrons)', useHTML: true } }", this_propmap );
        m_occ_chart[std::make_pair(m_activeChannels[i], 0)] = new webutils::WebChart( "occChart"+std::to_string(m_activeChannels[i])+"ROC0", cb, "type: 'heatmap', zoomType: 'xy', animation: false", "plotOptions: { heatmap: { animation: false, enableMouseTracking: false } }, boost: {useGPUTranslations: true}, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 0' },"
           "yAxis: {categories: "+Colstr+", title: { text: '', useHTML: true } }, xAxis: {categories: "+Rowstr+", title: { text: '', useHTML: true } }, colorAxis: {min: 0}", this_propmap );
        m_occ_chart[std::make_pair(m_activeChannels[i], 1)] = new webutils::WebChart( "occChart"+std::to_string(m_activeChannels[i])+"ROC1", cb, "type: 'heatmap', zoomType: 'xy', animation: false", "plotOptions: { heatmap: { animation: false, enableMouseTracking: false } }, boost: {useGPUTranslations: true}, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 1' },"
           "yAxis: {categories: "+Colstr+", title: { text: '', useHTML: true } }, xAxis: {categories: "+Rowstr+", title: { text: '', useHTML: true } }, colorAxis: {min: 0}", this_propmap );
        m_occ_chart[std::make_pair(m_activeChannels[i], 2)] = new webutils::WebChart( "occChart"+std::to_string(m_activeChannels[i])+"ROC2", cb, "type: 'heatmap', zoomType: 'xy', animation: false", "plotOptions: { heatmap: { animation: false, enableMouseTracking: false } }, boost: {useGPUTranslations: true}, title: { text: 'Channel "+std::to_string(m_activeChannels[i])+", ROC 2' },"
           "yAxis: {categories: "+Colstr+", title: { text: '', useHTML: true } }, xAxis: {categories: "+Rowstr+", title: { text: '', useHTML: true } }, colorAxis: {min: 0}", this_propmap );
    } 

    m_error_chart_main = new webutils::WebChart( "errorChart", cb, "type: 'line', zoomType: 'xy', animation: false", "plotOptions: { line: { animation: false } }, title: { text: 'Channel Error Counts' },"
       "yAxis: { min: 0, title: { text: 'Error Counts', useHTML: true } }, xAxis: { type: 'datetime' }", channel_propmap );
} 

std::vector<int> bril::pltslinkprocessor::Application::vectorToHist(std::vector<float> &vecIn, unsigned int nBins, float minVal, float maxVal) 
{
    std::vector<int> vecOut(nBins, 0);
    float bin_width = (maxVal - minVal)/nBins; 
    for (unsigned int i = 0; i < vecIn.size(); i++) {
        float val = vecIn.at(i);
        for (unsigned int j = 0; j < nBins; j++) {
            if (val > j*bin_width && val < (j+1)*bin_width)
                ++vecOut.at(j);
        }
    }
    return vecOut;    
}


void bril::pltslinkprocessor::Application::actionPerformed(xdata::Event& e){

    if(e.type() == "urn:xdaq-event:setDefaultValues"){

	// Parse the active channels string and read it into the array. Borrowed from pltprocessor
	std::string actch = m_activeChannelList;
	if (actch.length() > 0) {
	  size_t pos = 0; // position within FULL string that we're at so far
	  size_t newpos = 0; // position within SUBSTRING that comma was found
	  while (actch[pos] == ',') pos++; // in case we started with commas for some reason

	  while (newpos != std::string::npos) {
	    newpos = actch.substr(pos).find(',');
	    int i = atoi(actch.substr(pos).c_str());
	    if (i<1 || i>23) {
	      LOG4CPLUS_WARN(getApplicationLogger(), "Bad FED channel number " << i << " in active channel list");
	    } else {
	      LOG4CPLUS_DEBUG(getApplicationLogger(), "FED channel " << i << " set to active");
	      m_activeChannels.push_back(i);
	    }
	    pos += newpos+1;
	    while (actch[pos] == ',') pos++; // in case we have multiple commas in a row somehow
	    if (pos == actch.length()) break; // in case somehow we managed to end the string with a comma
	  }
	}
	// we need some active channels otherwise we're in trouble!
	m_nChannels = m_activeChannels.size();
	LOG4CPLUS_INFO(getApplicationLogger(), "Total of " << m_nChannels << " active channels");
	if (m_nChannels == 0) {
	  LOG4CPLUS_ERROR(getApplicationLogger(), "No active channels specified in XDAQ configuration");
	  XCEPT_RAISE(xgi::exception::Exception, "No active channels found; cannot start");
	}

	for (unsigned i = 0; i < m_nChannels; ++i) {
	  m_error_perchannel_map[m_activeChannels[i]] = 0;
	  m_channelMask[i] = false; // used for dropout alarms
	} 

        setupCharts();
       
        if (m_AlwaysFlash) { 
            m_publish_flash->activate();
            m_publish_flash->submit(m_publish_flash_as);
        }
        if (m_MakeLogs) { 
            std::stringstream fname;
            string baseDir = m_baseDir.value_;
            fname << baseDir << "/flash_log.csv";
            string fname_str = fname.str();
            flashFile.open(fname_str.c_str(), ios::out);
            
            flashFile << "channel,eff,acc,rate,time";
            flashFile << "\n";
        }

        try {this->getEventingBus("brildata").subscribe("beam");}
        catch (eventing::api::exception::Exception &e) {
            LOG4CPLUS_ERROR(getApplicationLogger(), "Failed to subscribe - " + stdformat_exception_history(e));
        }

        std::string timername("zmqstarter");
        try{
            toolbox::TimeVal zmqStart(2,0);
            toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer(timername);
            timer->schedule(this, zmqStart, 0, timername);
        }catch(xdata::exception::Exception& e){
            std::stringstream msg;
            msg << "failed to start timer" << timername;
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::pltslinkprocessor::exception::Exception,msg.str(),e);
        }
    }
}

void bril::pltslinkprocessor::Application::actionPerformed(toolbox::Event& e){}

/*
 * This is the default web page for this XDAQ application.
 */
void bril::pltslinkprocessor::Application::Default (xgi::Input * in, xgi::Output * out) {
    using namespace cgicc;

    // Send resize event
    *out << "<script>";
    *out << "setInterval(function() {window.dispatchEvent(new Event('resize'));}, 200);";
    *out << "</script>";
    webutils::WebChart::pageHeader(out, true);
    *out << h1() << "PLT Slink Processor" << h1() << endl;
    *out << "<div class=\"xdaq-tab-wrapper\">" << endl;
    *out << "<div class=\"xdaq-tab\" title=\"Summary\">" << endl;
    *out << busesToHTML() << br() << endl;

    *out << cgicc::table().set("class","xdaq-table-vertical");
    *out << cgicc::tbody();
    
    *out << cgicc::tr();
    *out << cgicc::th("Active channels");
    *out << cgicc::td(m_activeChannelList);
    *out << cgicc::tr();
    
    *out << cgicc::tbody();  
    *out << cgicc::table();
    
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Errors\">" << endl;
    m_error_chart_main->writeChart(out);
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Pulse Heights\">" << endl;
    
    // Make pulldown for pulse height plots
    *out << "Channel selector: ";
    *out << cgicc::select().set("id", "pulsePulldown") << endl;
    *out << option().set("value", to_string(m_activeChannels[0])) << to_string(m_activeChannels[0]) << option() << endl;
    for (unsigned int i = 1; i < m_nChannels; ++i) {
        *out << option().set("value", to_string(m_activeChannels[i])) << to_string(m_activeChannels[i]) << option() << endl;
    }
    *out << cgicc::select() << endl << "<br> <br>"; 
    *out << "<script>";
    *out << "$( '#pulsePulldown').change(function() {\n"
            "var selected_ch = Number( $( '#pulsePulldown option:selected').index() );\n"
            "var i; \n"
            "for (i = 0; i < " << m_nChannels << " ; ++i) {\n"
            "    var divId = '#pulse' + i;\n"
            "    if (i === selected_ch) { \n"
            "         $(divId).show(); \n"
            "         console.log(divId);\n"
            "    } else { \n"
            "         $(divId).hide(); \n"
            "    } \n"
            "} \n"
            "});";
    *out << "</script>" << endl;

    for (unsigned int i = 0; i < m_nChannels; ++i) {
        *out << "<div id=\"pulse" << i << "\" style=\"display:none; width:100%\">";
        // ROC 0
        *out << "<div style = \"float:left; width:33%;\">";
        m_pulse_hist[std::make_pair(m_activeChannels[i], 0)]->writeChart(out);
        *out << "</div>";
        // ROC 1
        *out << "<div style = \"float:left; width:33%;\">";
        m_pulse_hist[std::make_pair(m_activeChannels[i], 1)]->writeChart(out);
        *out << "</div>";
        // ROC 2
        *out << "<div style = \"float:left; width:33%;\">";
        m_pulse_hist[std::make_pair(m_activeChannels[i], 2)]->writeChart(out);
        *out << "</div>";

        *out << "</div>" << endl;
    }
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Occupancies\">" << endl;
    
    // Make pulldown for occupancy plots
    *out << "Channel selector: ";
    *out << cgicc::select().set("id", "occupancyPulldown") << endl;
    *out << option().set("value", to_string(m_activeChannels[0])) << to_string(m_activeChannels[0]) << option() << endl;
    for (unsigned int i = 1; i < m_nChannels; ++i) {
        *out << option().set("value", to_string(m_activeChannels[i])) << to_string(m_activeChannels[i]) << option() << endl;
    }
    *out << cgicc::select() << endl << "<br> <br>"; 
    *out << "<script>";
    *out << "$( '#occupancyPulldown').change(function() {\n"
            "var selected_ch = Number( $( '#occupancyPulldown option:selected').index() );\n"
            "var i; \n"
            "for (i = 0; i < " << m_nChannels << " ; ++i) {\n"
            "    var divId = '#occ' + i;\n"
            "    if (i === selected_ch) { \n"
            "         $(divId).show(); \n"
            "         console.log(divId);\n"
            "    } else { \n"
            "         $(divId).hide(); \n"
            "    } \n"
            "} \n"
            "});";
    *out << "</script>" << endl;

    for (unsigned int i = 0; i < m_nChannels; ++i) {
        *out << "<div id=\"occ" << i << "\" style=\"display:none; width:100%\">";
        //ROC 0
        *out << "<div style = \"float:left; width:33%;\">";
        m_occ_chart[std::make_pair(m_activeChannels[i], 0)]->writeChart(out);
        *out << "</div>";
        // ROC 1
        *out << "<div style = \"float:left; width:33%;\">";
        m_occ_chart[std::make_pair(m_activeChannels[i], 1)]->writeChart(out);
        *out << "</div>";
        // ROC 2
        *out << "<div style = \"float:left; width:33%;\">";
        m_occ_chart[std::make_pair(m_activeChannels[i], 2)]->writeChart(out);
        *out << "</div>";
        *out << "</div>" << endl;
    }
    *out << "</div>";
    *out << "</div>";
}

void bril::pltslinkprocessor::Application::requestData(xgi::Input* in, xgi::Output* out) {
    using namespace cgicc;
    try {
        webutils::WebChart::dataHeader(out);
        Cgicc cgi(in);
        m_error_chart_main->writeDataForQuery(cgi, out, m_error_history);
        webutils::heatmap_point<int, int, int> test(1,2,3);
        webutils::to_string(test);
        for (unsigned int i = 0; i < m_nChannels; ++i) {
            m_pulse_hist[std::make_pair(m_activeChannels[i], 0)]->writeDataForQuery(cgi, out, m_pulse_vals[std::make_pair(m_activeChannels[i], 0)]);
            m_pulse_hist[std::make_pair(m_activeChannels[i], 1)]->writeDataForQuery(cgi, out, m_pulse_vals[std::make_pair(m_activeChannels[i], 1)]);
            m_pulse_hist[std::make_pair(m_activeChannels[i], 2)]->writeDataForQuery(cgi, out, m_pulse_vals[std::make_pair(m_activeChannels[i], 2)]);
            m_occ_chart[std::make_pair(m_activeChannels[i], 0)]->writeDataForQuery(cgi, out, m_occ_vals[std::make_pair(m_activeChannels[i], 0)]);
            m_occ_chart[std::make_pair(m_activeChannels[i], 1)]->writeDataForQuery(cgi, out, m_occ_vals[std::make_pair(m_activeChannels[i], 1)]);
            m_occ_chart[std::make_pair(m_activeChannels[i], 2)]->writeDataForQuery(cgi, out, m_occ_vals[std::make_pair(m_activeChannels[i], 2)]);
        }
    } catch (const std::exception& e) {
        XCEPT_RAISE( xgi::exception::Exception, e.what() );
    }
}

void bril::pltslinkprocessor::Application::onMessage(toolbox::mem::Reference* ref, xdata::Properties& plist) { 
    
    toolbox::mem::AutoReference refguard(ref); //IMPORTANT: guarantee ref is released when refguard is out of scope
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if ( action == "notify" && ref!=0 ){      
	    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received topic " +topic);
	    if( topic == "beam" ){
            interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());   
            std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
            interface::bril::shared::CompoundDataStreamer tc(payloaddict);
            char bstatus[28];
            tc.extract_field( bstatus , "status", thead->payloadanchor );
            m_beamstatus = std::string(bstatus);
            if (!m_AlwaysFlash) {
                if (m_beamstatus == "STABLE BEAMS" && !m_publish_flash->isActive()) {
                    m_publish_flash->activate();
                    m_publish_flash->submit(m_publish_flash_as);
                }
                else if (m_beamstatus != "STABLE BEAMS" && m_publish_flash->isActive()) {
                    m_publish_flash->cancel();
                }
            }
        }
    }
}
            
bool bril::pltslinkprocessor::Application::publish_flash(toolbox::task::WorkLoop* wl) {
    sleep(300);
    LOG4CPLUS_INFO(getApplicationLogger(), "Publishing flashlist");
    xdata::Float acc_sum = 0;
    xdata::Float eff_sum = 0;
    m_timestamp = toolbox::TimeVal::gettimeofday();
    for (unsigned i = 0; i < m_nChannels; ++i) {
        m_acc_perchannel.at(i) = m_acc_sum.at(i)/m_publish_count;
        m_eff_perchannel.at(i) = m_eff_sum.at(i)/m_publish_count;
        m_rate_perchannel.at(i) = m_rate_sum.at(i)/m_publish_count;
        acc_sum = acc_sum + m_acc_perchannel.at(i);
        eff_sum = eff_sum + m_eff_perchannel.at(i);
        m_acc_sum.at(i) = 0;
        m_eff_sum.at(i) = 0;
        m_rate_sum.at(i) = 0;
        if (m_MakeLogs) {
            flashFile << m_activeChannels[i] << "," << m_eff_perchannel.at(i) << "," << m_acc_perchannel.at(i) << "," << m_rate_perchannel.at(i) << "," << m_timestamp.toString();
            flashFile << "\n" << std::flush; 
        }
    }
    m_acc_avg = acc_sum/m_nChannels;
    m_eff_avg = eff_sum/m_nChannels;
    std::list<std::string> flashitemlist; 
    flashitemlist.push_back("fill");
    flashitemlist.push_back("run");
    flashitemlist.push_back("timestamp");
    flashitemlist.push_back("acc_avg");
    flashitemlist.push_back("acc_perchannel");
    flashitemlist.push_back("eff_avg");
    flashitemlist.push_back("eff_perchannel");
    m_flashInfoSpace->fireItemGroupChanged(flashitemlist, this); 
    m_publish_count = 0;
    catchDropouts();
    return true;
}

void bril::pltslinkprocessor::Application::catchDropouts(){
    const float offsets[] = {0.0087, 0.0323, 0.0292, -0.0034, 0.0049, -0.0059, 0.0485, 0.0247, 0.0110, 0.0044, 0.0104, -0.0356, -0.1646, 0.0356};
    const float sigma = 0.05;
    for (unsigned i = 0; i < m_nChannels; ++i) {
        float this_eff = m_eff_perchannel.at(i);
        float this_eff_sum = 0;
        int n_unmasked = 0;
        for (unsigned j = 0; j < m_nChannels; ++j) {
            if (j != i && !m_channelMask[j]) {
                this_eff_sum += m_eff_perchannel.at(j); 
                ++n_unmasked;
            }
        }
        float this_eff_avg = this_eff_sum/(n_unmasked - 1);
        bool hasAlarm = m_sentinelInfoSpace->hasItem("Channel_"+std::to_string(m_activeChannels[i])+"_dropout_alarm");
        if (fabs(this_eff - offsets[i] - this_eff_avg)/sigma > 4) {
            if (!hasAlarm) { 
                std::stringstream msg;
                msg << "Channel "+std::to_string(m_activeChannels[i])+" dropout.";
                XCEPT_DECLARE(bril::pltslinkprocessor::exception::PLTAlarm, ex, msg.str());
                sentinel::utils::Alarm *alarm = new sentinel::utils::Alarm("warning", ex, this);  
                m_sentinelInfoSpace->fireItemAvailable("Channel_"+std::to_string(m_activeChannels[i])+"_dropout_alarm", alarm);
                if (n_unmasked > 7)  // Avoid masking too many channels
                    m_channelMask[i] = true;
            }
        }
        else {
            if (hasAlarm) {
                sentinel::utils::Alarm *alarm = dynamic_cast<sentinel::utils::Alarm*>(m_sentinelInfoSpace->find("Channel_"+std::to_string(m_activeChannels[i])+"_dropout_alarm"));
                m_sentinelInfoSpace->fireItemRevoked("Channel_"+std::to_string(m_activeChannels[i])+"_dropout_alarm", this);
                delete alarm;
                m_channelMask[i] = false;
            }
        }
    }                    
}

void bril::pltslinkprocessor::Application::timeExpired(toolbox::task::TimerEvent& e){
    LOG4CPLUS_INFO(getApplicationLogger(), "Beginning zmq listener");
    zmqClient();
}

void bril::pltslinkprocessor::Application::zmqClient()
{
    // Use zmq_poll to get zmq messages from the multiple sources.
    zmq::context_t zmq_context(1);

    // Workloop zmq listener: used for getting TCDS info from workloop
    zmq::socket_t workloop_socket(zmq_context, ZMQ_SUB);
    workloop_socket.connect(m_workloopHost.c_str());
    workloop_socket.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    uint32_t EventHisto[3573];
    uint32_t tcds_info[4];
    uint32_t channel;
    int old_run = 1; // Set these high so we don't immediately publish
    int old_ls  = 999999;   // the first LS

    // Slink zmq listener: used for getting actual data from slink
    zmq::socket_t slink_socket(zmq_context, ZMQ_SUB);
    slink_socket.connect(m_slinkHost.c_str());
    slink_socket.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    uint32_t slinkBuffer[1024];
    // Translation of pixel FED channel number to readout channel number.
    int nevents = 0; // counter of slink items received

    const unsigned nRows = PLTU::LASTROW;
    const unsigned nCols = PLTU::LASTCOL;

    // Set up poll object
    zmq::pollitem_t pollItems[2] = {
        {workloop_socket, 0, ZMQ_POLLIN, 0},
        {slink_socket, 0, ZMQ_POLLIN, 0}
    };

    // Set up the PLTEvent object to do the event decoding. Note: for testing
    // purposes we don't have a gaincal, alignment, or tracking, but we will
    // want to implement these in order to do more interesting things.

    PLTEvent *event = new PLTEvent("", m_gcFile.value_, m_alFile.value_, kBuffer);
    event->ReadOnlinePixelMask(m_pixMask.value_);
    vector<PLTError> plt_errors;
    
    // Initialize tools
    EventAnalyzer *eventAnalyzer = new EventAnalyzer(event, m_alFile.value_, m_trackFile.value_, m_activeChannels);

    // Loop and receive messages
    while (1) {

        zmq_poll(&pollItems[0],  2,  -1);

        // Check for workloop message
        if (pollItems[0].revents & ZMQ_POLLIN) {
            zmq::message_t zmq_EventHisto(3573*sizeof(uint32_t));
            workloop_socket.recv(&zmq_EventHisto);

            memcpy(EventHisto, zmq_EventHisto.data(), 3573*sizeof(uint32_t));
            memcpy(&channel, (uint32_t*)EventHisto+2, sizeof(uint32_t));
            memcpy(&tcds_info, (uint32_t*)EventHisto+3, 4*sizeof(uint32_t));

            m_nibble = tcds_info[0];
            m_ls     = tcds_info[1];
            m_run    = tcds_info[2];
            m_fill   = tcds_info[3];

            if (m_MakeLogs) {
                if (old_run == 1) {
                    this->initializeOutputFiles(m_run, m_activeChannels);
                } else if (m_run > old_run) {
                    effFile.close();
                    accFile.close();
                    lumiFile.close();
                    publishFile.close();
                    this->initializeOutputFiles(m_run, m_activeChannels);
                }
            }

            // If we've started a new run or a new lumisection, then go ahead
            // and publish what we have.
            if (m_ls > old_ls or m_run > old_run) {  
                
                m_fill_flash = m_fill;
                m_run_flash = m_run;
                m_ls_flash = m_ls;
                
                if (m_MakeLogs) {
                    effFile << m_ls;
                    accFile << m_ls;
                    lumiFile << m_ls;
                    publishFile << m_fill << "," << m_run << "," << m_ls;
                }

                vector<float> eff;
                float avgRateNum = 0.;
                float avgRateDen = 0.;
                float avgRateRaw = 0.;
                for (unsigned i = 0; i < m_nChannels; ++i) {
                    eff   = eventAnalyzer->GetTelescopeEfficiency(m_activeChannels[i]);
                    float channelEff = eff[0]*eff[1]*eff[2];
                    float acc  = eventAnalyzer->GetTelescopeAccidentals(m_activeChannels[i]);
                    float rate = eventAnalyzer->GetZeroCounting(m_activeChannels[i]); 
    
                    vector<float> pulse_heights_0, pulse_heights_1, pulse_heights_2;
                    pulse_heights_0 = eventAnalyzer->PulseHeights(m_activeChannels[i], 0);
                    pulse_heights_1 = eventAnalyzer->PulseHeights(m_activeChannels[i], 1);
                    pulse_heights_2 = eventAnalyzer->PulseHeights(m_activeChannels[i], 2);

                    m_pulse_vals[std::make_pair(m_activeChannels[i], 0)][""] = vectorToHist(pulse_heights_0, nPulseBins, 0., pulseMax);
                    m_pulse_vals[std::make_pair(m_activeChannels[i], 1)][""] = vectorToHist(pulse_heights_1, nPulseBins, 0., pulseMax);
                    m_pulse_vals[std::make_pair(m_activeChannels[i], 2)][""] = vectorToHist(pulse_heights_2, nPulseBins, 0., pulseMax);

                    m_occ_vals[std::make_pair(m_activeChannels[i], 0)][""].clear();
                    m_occ_vals[std::make_pair(m_activeChannels[i], 1)][""].clear();
                    m_occ_vals[std::make_pair(m_activeChannels[i], 2)][""].clear();
                    
                    // Occupancies
                    for (unsigned int row = 0; row < nRows; ++row) {
                        for (unsigned int col = 0; col < nCols; ++col) {
                            int occ0 = eventAnalyzer->Occupancies(m_activeChannels[i], 0, row, col);
                            int occ1 = eventAnalyzer->Occupancies(m_activeChannels[i], 1, row, col);
                            int occ2 = eventAnalyzer->Occupancies(m_activeChannels[i], 2, row, col);
                            m_occ_vals[std::make_pair(m_activeChannels[i], 0)][""].emplace_back(col, row, occ0);
                            m_occ_vals[std::make_pair(m_activeChannels[i], 1)][""].emplace_back(col, row, occ1);
                            m_occ_vals[std::make_pair(m_activeChannels[i], 2)][""].emplace_back(col, row, occ2);
                        }
                    }

                   
                    //flashlist 
                    m_acc_sum.at(i) = m_acc_sum.at(i) + acc;
                    m_eff_sum.at(i) = m_eff_sum.at(i) + channelEff;
                    m_rate_sum.at(i) = m_rate_sum.at(i) + rate;

                    avgRateRaw += rate/m_nChannels;
                    
                    if (channelEff == 1.) { 
                        avgRateNum += rate/m_nChannels;
                        avgRateDen += 1.;
                    }
                    
                    else if (channelEff == 0.) {
                        avgRateNum += 0.;
                        avgRateDen += 1.;
                    }

                    else {
                        avgRateNum += 1./(channelEff*(1 - channelEff));
                        if (rate > 0.) 
                            avgRateDen += 1./(rate*(1 - channelEff));
                        else 
                            avgRateDen += 1.; 
                        }
                        
 
                    if (m_MakeLogs) {
                        effFile  << "," << eff[0] << "," << eff[1] << "," << eff[2];
                        accFile  << "," << acc;
                        lumiFile << "," << rate;
                    }
                }
                
                if (m_MakeLogs) {
                    effFile  << "\n" << std::flush;
                    accFile  << "\n" << std::flush;
                    lumiFile << "\n" << std::flush;
                }

                float pZero    = 1. - avgRateNum/avgRateDen;
                float pZeroRaw = 1. - avgRateRaw;
                //float lumi = -m_calibVal*log(pZero);
                float lumiRaw = -m_calibVal*log(pZeroRaw);

                if (m_MakeLogs) {
                    publishFile << "," << pZero << "," << pZeroRaw;
                    publishFile << "\n" << std::flush;
                }

                toolbox::TimeVal timeStamp = toolbox::TimeVal::gettimeofday();

                // prepare output buffer and publish
                xdata::Properties plist;
                plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty("PAYLOAD_DICT", pltslinklumiT::payloaddict());
	            if( m_beamstatus!="STABLE BEAMS"&&m_beamstatus!="SQUEEZE"&&m_beamstatus!="FLAT TOP"&&m_beamstatus!="ADJUST"){
	                plist.setProperty("NOSTORE","1");
	            }

                size_t bufferSize = pltslinklumiT::maxsize();
                toolbox::mem::Reference* bufferRef = m_poolFactory->getFrame(m_memPool, bufferSize);
                bufferRef->setDataSize(bufferSize);

                pltslinklumiT* payload = (pltslinklumiT*) (bufferRef->getDataLocation());
                payload->setTime(m_fill, m_run, m_ls, m_nibble, timeStamp.sec(), timeStamp.millisec());
                payload->setResource(interface::bril::shared::DataSource::PLT, 0, 1, interface::bril::shared::StorageType::COMPOUND);
                payload->setTotalsize(pltslinklumiT::maxsize());

		interface::bril::shared::CompoundDataStreamer streamer(pltslinklumiT::payloaddict()); 
		        std::string calibtag("default");
                streamer.insert_field( payload->payloadanchor, "calibtag" , calibtag.c_str() );
                streamer.insert_field(payload->payloadanchor, "avgraw", &lumiRaw);
                streamer.insert_field(payload->payloadanchor, "avg", &lumiRaw); // Temporarily use avgraw calculated value as output to avg
                std::stringstream ss;
                ss << "Publishing to brildata::pltslinklumi: fill " << m_fill << " run " << m_run << " LS " << m_ls << " events " << nevents;
                LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
                
                try{
                    this->getEventingBus("brildata").publish("pltslinklumi", bufferRef, plist);
                } 
                catch (xcept::Exception& e) {
                    bufferRef->release();
	                LOG4CPLUS_ERROR( getApplicationLogger(),stdformat_exception_history(e) );
                } 
            
                LOG4CPLUS_INFO(getApplicationLogger(), "Done sending publishing data to 'brildata'");
                m_publish_count++; 
                    
                for (unsigned int i = 0; i < m_nChannels; ++i) {
                    m_occ_chart[std::make_pair(m_activeChannels[i], 0)]->newDataReady(); 
                    m_occ_chart[std::make_pair(m_activeChannels[i], 1)]->newDataReady(); 
                    m_occ_chart[std::make_pair(m_activeChannels[i], 2)]->newDataReady(); 
                }

                
                eventAnalyzer->ClearCounters();
                eventAnalyzer->ClearOccupancies();
               
                for (unsigned int i = 0; i < plt_errors.size(); i++) {
                    int channel = plt_errors.at(i).GetChannel();
                    ++m_error_perchannel_map[channel];
                }
                
                m_error_sum = 0;
                m_timestamp = toolbox::TimeVal::gettimeofday();
                auto ms = static_cast<toolbox::TimeVal>(m_timestamp).sec()*1000;
                for (unsigned i = 0; i < m_nChannels; ++i) {
                    m_error_perchannel.at(i) = m_error_perchannel_map[m_activeChannels[i]];
                    m_error_sum = m_error_sum + m_error_perchannel.at(i);
                    bool hasAlarm = m_sentinelInfoSpace->hasItem("Channel_"+std::to_string(m_activeChannels[i])+"_alarm"); 
                    if (m_error_perchannel.at(i) > m_err_thresh) {
                        if (!hasAlarm) { 
                            std::stringstream msg;
                            msg << "Channel "+std::to_string(m_activeChannels[i])+" has high error count.";
                            XCEPT_DECLARE(bril::pltslinkprocessor::exception::PLTAlarm, ex, msg.str());
                            sentinel::utils::Alarm *alarm = new sentinel::utils::Alarm("warning", ex, this);    
                            m_sentinelInfoSpace->fireItemAvailable("Channel_"+std::to_string(m_activeChannels[i])+"_alarm", alarm);
                        }
                    }
                    else {
                        if (hasAlarm) {
                            sentinel::utils::Alarm *alarm = dynamic_cast<sentinel::utils::Alarm*>(m_sentinelInfoSpace->find("Channel_"+std::to_string(m_activeChannels[i])+"_alarm"));
                            m_sentinelInfoSpace->fireItemRevoked("Channel_"+std::to_string(m_activeChannels[i])+"_alarm", this);
                            delete alarm;
                        }
                    }
                int this_error = m_error_perchannel_map[m_activeChannels[i]];
                m_error_history["Channel "+std::to_string(m_activeChannels[i])].push_back(std::make_pair(ms, this_error));
                if (m_error_history["Channel "+std::to_string(m_activeChannels[i])].size() > 1000)
                    m_error_history["Channel "+std::to_string(m_activeChannels[i])].pop_front();
                m_error_perchannel_map[m_activeChannels[i]] = 0; // reset the error counter
                } 
                m_error_chart_main->newDataReady(); 
                for (unsigned int i = 0; i < m_nChannels; ++i) {
                    m_pulse_hist[std::make_pair(m_activeChannels[i], 0)]->newDataReady(); 
                    m_pulse_hist[std::make_pair(m_activeChannels[i], 1)]->newDataReady(); 
                    m_pulse_hist[std::make_pair(m_activeChannels[i], 2)]->newDataReady(); 
                }
                eventAnalyzer->ClearPulses();
                plt_errors.clear();
            }
            old_ls  = m_ls;
            old_run = m_run;
        }

        // Check for slink message
        if (pollItems[1].revents & ZMQ_POLLIN) {
            int eventSize = slink_socket.recv((void*)slinkBuffer, sizeof(slinkBuffer)*sizeof(uint32_t), 0);

            // feed it to Event.GetNextEvent()
            event->GetNextEvent(slinkBuffer, eventSize/sizeof(uint32_t));
            plt_errors.insert(plt_errors.begin(), event->GetErrors().begin(), event->GetErrors().end());

            // Now the event is fully processed and we can do things with the
            // processed information.  For now all we do is put the hits in the
            // occupancy plots, but we can do all sorts of other things (work
            // with tracks, pulse heights, etc.)

            // Calculate efficiency and accidental rate per telescope
            eventAnalyzer->AnalyzeEvent();
            nevents++;
        } // slink message
    } // message loop  
}

void bril::pltslinkprocessor::Application::initializeOutputFiles(unsigned runNumber, vector<unsigned> channels)
{
    std::stringstream fname;
    string baseDir = m_baseDir.value_;

    fname << baseDir << "/efficiencies_" << m_run << ".csv";
    string fname_str = fname.str();
    effFile.open(fname_str.c_str(), ios::out);
    fname.str("");

    fname << baseDir << "/accidentals_" << m_run << ".csv";
    fname_str = fname.str();
    accFile.open(fname_str.c_str(), ios::out);
    fname.str("");
 
    fname << baseDir << "/lumi_rates_" << m_run << ".csv";
    fname_str = fname.str();
    lumiFile.open(fname_str.c_str(), ios::out);
    fname.str("");
    
    fname << baseDir << "/publish_log_" << m_run << ".csv";
    fname_str = fname.str();
    publishFile.open(fname_str.c_str(), ios::out);
    fname.str(""); 
    
    effFile  << "lumi_section";
    accFile  << "lumi_section";
    lumiFile << "lumi_section";
    publishFile << "fill,run,lumi,avg,avgraw";

    for (unsigned i = 0; i < channels.size(); ++i) {
        unsigned ch = channels[i];
        effFile  << ",ch" << ch << "_0,ch" << ch << "_1,ch" << ch << "_2";
        accFile  << ",ch" << ch;
        lumiFile << ",ch" << ch;
    }
    effFile  << "\n";
    accFile  << "\n";
    lumiFile << "\n";
    publishFile << "\n";
}

