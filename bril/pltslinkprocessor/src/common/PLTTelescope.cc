#include "bril/pltslinkprocessor/PLTTelescope.h"


PLTTelescope::PLTTelescope ()
{
  // Con me
}


PLTTelescope::~PLTTelescope ()
{
  // Telescopes own Tracks in them.
  for (size_t itrack = 0; itrack != fTracks.size(); ++itrack) {
    delete fTracks[itrack];
  }

  // Byebye
}


void PLTTelescope::AddPlane (PLTPlane* Plane)
{
  // Add a plane
  fPlanes.push_back(Plane);
  fChannel = Plane->Channel();
  return;
}


int PLTTelescope::Channel ()
{
  // Get the channel
  return fChannel;
}


PLTPlane* PLTTelescope::Plane(size_t i)
{
  // Get a specific plane
  return fPlanes[i];
}

size_t PLTTelescope::NPlanes ()
{
  // Number of planes in this telescope with at least one hit
  return fPlanes.size();
}


size_t PLTTelescope::NHits ()
{
  // Get the total number of hits in all planes in this telescope
  size_t nhits = 0;
  for (std::vector<PLTPlane*>::iterator it = fPlanes.begin(); it != fPlanes.end(); ++it) {
    nhits += (*it)->NHits();
  }

  return nhits;
}


size_t PLTTelescope::NClusters ()
{
  // Get the total number of clusters in all planes in this telescope
  size_t nclusters = 0;
  for (std::vector<PLTPlane*>::iterator it = fPlanes.begin(); it != fPlanes.end(); ++it) {
    nclusters += (*it)->NClusters();
  }

  return nclusters;
}



size_t PLTTelescope::NTracks ()
{
  return fTracks.size();
}

PLTTrack* PLTTelescope::Track (size_t i)
{
  return fTracks[i];
}

int PLTTelescope::HitPlaneBits ()
{
  // This function return a binary representation of hit planes
  int HitPlanes = 0x0;

  for (std::vector<PLTPlane*>::iterator it = fPlanes.begin(); it != fPlanes.end(); ++it) {
    if ((*it)->NHits() > 0) {
      HitPlanes |= (0x1 << (*it)->ROC());
    }
  }

  return HitPlanes;
}



int PLTTelescope::NHitPlanes ()
{
  // This function returns the number of planes with at least 1 hit
  int HitPlanes = 0;

  for (std::vector<PLTPlane*>::iterator it = fPlanes.begin(); it != fPlanes.end(); ++it) {
    if ((*it)->NHits() > 0) {
      ++HitPlanes;
    }
  }

  return HitPlanes;
}


void PLTTelescope::AddTrack (PLTTrack* T)
{
  fTracks.push_back(T);
  return;
}


void PLTTelescope::FillAndOrderTelescope ()
{
  // This functino takes forces the size of a telescope to be 3 ROCs
  // It then orders the ROCs which exist and makes a new plane for missing ones =)

  std::vector<PLTPlane*> Ordered(3, (PLTPlane*) 0x0);

  for (size_t i = 0; i != fPlanes.size(); ++i) {
    Ordered[ fPlanes[i]->ROC() ] = fPlanes[i];
  }

  fPlanes = Ordered;
  return;
}


