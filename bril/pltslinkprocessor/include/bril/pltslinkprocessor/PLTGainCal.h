#ifndef GUARD_PLTGainCal_h
#define GUARD_PLTGainCal_h

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "bril/pltslinkprocessor/PLTHit.h"
#include "bril/pltslinkprocessor/PLTCluster.h"
#include "bril/pltslinkprocessor/PLTPlane.h"
#include "bril/pltslinkprocessor/PLTU.h"


class PLTGainCal
{
  public:
    PLTGainCal ();
    PLTGainCal (const std::string&, int const NParams = 5);
    ~PLTGainCal ();

    static int const DEBUGLEVEL = 0;
    void  SetCharge (PLTHit&);
    float GetCharge(int const ch, int const roc, int const col, int const row, int adc);
    void  ReadGainCalFile (const std::string& GainCalFileName);
    void  ReadGainCalFile5 (const std::string& GainCalFileName);
    void  ReadTesterGainCalFile (const std::string& GainCalFileName);

    void CheckGainCalFile (const std::string& GainCalFileName, int const Channel);

    void PrintGainCal5 ();

    static int RowIndex (int const);
    static int ColIndex (int const);
    static int ChIndex (int const);
    static int RocIndex (int const);

    float GetCoef(int const, int const, int const, int const, int const);

    bool IsGood () { return fIsGood; }

    int GetHardwareID (int const);
    int GetFEDChannel(int mFec, int mFecCh, int hubId) const;

  private:
    bool fIsGood;
    bool fIsExternalFunction;

    int  fNParams; // how many parameters for this gaincal
    //TF1 fFitFunction;

    static int const MAXCHNS =   36;
    static int const MAXROWS =   80;
    static int const MAXCOLS =   52;
    static int const MAXROCS =    3;

    static int const NCHNS =  36;
    static int const NROWS =  PLTU::NROW;
    static int const NCOLS =  PLTU::NCOL;
    static int const NROCS =   3;


    // ch,roc,col,row [3]
    //float GC[MAXCHNS][MAXROCS][MAXCOLS][MAXROWS][3];
    typedef float GCOnTheHeap[NROCS][NCOLS][NROWS][6];
    GCOnTheHeap* GC;

    // Map for hardware locations by fed channel
    std::map<int, int> fHardwareMap;

};















#endif
