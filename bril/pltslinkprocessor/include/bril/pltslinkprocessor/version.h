// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_pltslinkprocessor_version_h_
#define _bril_pltslinkprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILPLTSLINKPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILPLTSLINKPROCESSOR_VERSION_MINOR 1
#define BRIL_BRILPLTSLINKPROCESSOR_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILPLTSLINKPROCESSOR_PREVIOUS_VERSIONS "1.0.0,1.0.1,1.0.2,4.0.0"

//
// Template macros
//
#define BRIL_BRILPLTSLINKPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILPLTSLINKPROCESSOR_VERSION_MAJOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_MINOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILPLTSLINKPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILPLTSLINKPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILPLTSLINKPROCESSOR_VERSION_MAJOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_MINOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILPLTSLINKPROCESSOR_FULL_VERSION_LIST BRIL_BRILPLTSLINKPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILPLTSLINKPROCESSOR_VERSION_MAJOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_MINOR,BRIL_BRILPLTSLINKPROCESSOR_VERSION_PATCH)
#endif

namespace brilpltslinkprocessor
{
        const std::string project = "bril";
	const std::string package = "brilpltslinkprocessor";
	const std::string versions = BRIL_BRILPLTSLINKPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ pltslinkprocessor";
	const std::string description = "collect and process histograms from plt slink";
	const std::string authors = "K. J. Rose, A. Gomez, J. Bueghly, P. Lujan";
	const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/PLTSlinkProcessor";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
