#ifndef _bril_bcm1f_utcaprocessor_Utilities_h_
#define _bril_bcm1f_utcaprocessor_Utilities_h_

//std library
#include <map>
#include <deque>
#include <array>
#include <set>
#include <string>
//mine
#include "interface/bril/BCM1FUTCATopics.hh"

//#include "bril/bcm1futcaprocessor/HistogramBuffer.h"
#include "bril/bcm1futcaprocessor/HistogramCache.h"

namespace bril {

    namespace bcm1futcaprocessor {

        static const size_t s_nBinOcc = interface::bril::BCM1FUTCA_BINPERBX_OCC * interface::bril::shared::MAX_NBX;
        static const size_t s_nBinAmp =  interface::bril::BCM1FUTCA_BINAMP;
        static const size_t s_nBinRaw =  interface::bril::BCM1FUTCA_BINPERBX_RAW * interface::bril::shared::MAX_NBX;

        using OccupancyHistogram = Histogram < uint16_t, s_nBinOcc >;
        using AmplitudeHistogram = Histogram < uint32_t, s_nBinAmp >;
        using RawHistogram = Histogram < uint8_t, s_nBinRaw >;

        using OccupancyHistogramVector = std::vector<HistogramBuffer < uint16_t, s_nBinOcc >>;
        using AmplitudeHistogramVector = std::vector<HistogramBuffer < uint32_t, s_nBinAmp >>;
        using RawHistogramVector = std::vector<HistogramBuffer < uint8_t, s_nBinRaw >>;

        using OccupancyHistogramVectorLS = std::vector<HistogramBuffer < uint32_t, s_nBinOcc >>;
        using AmplitudeHistogramVectorLS = std::vector<HistogramBuffer < uint32_t, s_nBinAmp >>;
        using RawHistogramVectorLS = std::vector<HistogramBuffer < uint32_t, s_nBinRaw >>;

        using OccupancyHistogramBuffer = HistogramBuffer < uint16_t, s_nBinOcc >;
        using AmplitudeHistogramBuffer = HistogramBuffer < uint32_t, s_nBinAmp >;
        using RawHistogramBuffer = HistogramBuffer < uint8_t, s_nBinRaw >;

        using OccupancyHistogramBufferLS = HistogramBuffer < uint32_t, s_nBinOcc >;
        using AmplitudeHistogramBufferLS = HistogramBuffer < uint32_t, s_nBinAmp >;
        using RawHistogramBufferLS = HistogramBuffer < uint32_t, s_nBinRaw >;

        using AlbedoQueue = std::deque < std::map < HistogramIdentifier, std::vector<float> >>;
        /////////////////////////
        //some constants for calculations
        /////////////////////////

        //const float s_areaDiamond = 0.0722; // cm^2.  EFFECTIVE AREA (incl edge effects): 3.8mm*1.9mm = 0.38 cm * 0.19 cm = 0.0722 cm^2
        const float s_areaSilicon = 0.0289; // cm^2. 1.7mm*1.7mm = 0.17cm * 0.17cm = 0.0289 cm^2
    } //namespace bcm1futcaprocessor
} //namespace bril

#endif
