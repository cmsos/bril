#ifndef _bril_bcm1f_utcaprocessor_histogramcache_h_
#define _bril_bcm1f_utcaprocessor_histogramcache_h_

//std c++
#include <algorithm>
#include <numeric>
#include <malloc.h>

//BRIL
#include "interface/bril/shared/CommonDataFormat.h"

//XDAQ
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Guard.h"

//mine
#include "bril/bcm1futcaprocessor/Histogram.h"
#include "bril/bcm1futcaprocessor/HistogramBuffer.h"
#include "bril/bcm1futcaprocessor/Utils/ConsoleColor.h"

namespace bril {

    namespace bcm1futcaprocessor {

        typedef toolbox::task::Guard<toolbox::BSem> Locker;

        template<typename DataT, size_t nBin, typename DatumT>
        class HistogramCache
        {
	    //This should be a Unique manager class type and as such should have a default constructor, one with params, a destructor defined and implement the Move members
          private:
            std::map<NB4Identifier, HistogramBuffer<DataT, nBin>>* m_nibbleBuffer;
            std::map<LSIdentifier, HistogramBuffer<uint32_t, nBin>>* m_lsBuffer;

            std::vector<unsigned int> m_goodChannelList{};
	    unsigned int m_algorithm;

            unsigned int m_lastAdditionTime;
            std::string m_histName;
	    //only if this is true will the ls buffer be used
	    bool m_aggregatable;
            //for thread safety
            toolbox::BSem m_appLock;

          public:
	    //defautl constructor
	    HistogramCache() : // = default;
		m_nibbleBuffer(nullptr),
		m_lsBuffer(nullptr),
                m_goodChannelList (),
		m_algorithm(0),
                m_histName (""),
		m_aggregatable(false),
                m_appLock (toolbox::BSem::FULL)
            {
                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }
		
	    //Parameter constructor
            HistogramCache (std::vector<unsigned int> goodChannels, unsigned int algorithm = 0, bool aggregatable = false) :
		m_nibbleBuffer(nullptr),
		m_lsBuffer(nullptr),
                m_goodChannelList (goodChannels),
		m_algorithm(algorithm),
                m_histName (""),
		m_aggregatable(aggregatable),
                m_appLock (toolbox::BSem::FULL)
            {
                //first, sort the good channel list
                std::sort (m_goodChannelList.begin(), m_goodChannelList.end() );
                //clear the map that is used to store the nibble4s
                m_nibbleBuffer = new std::map<NB4Identifier, HistogramBuffer<DataT, nBin>>;

                if(m_aggregatable)
		    m_lsBuffer = new std::map<LSIdentifier, HistogramBuffer<uint32_t, nBin>>; 

                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }

	    ~HistogramCache() noexcept
	    {
		this->release();
	    }

	    void release() noexcept
	    {
		if(m_nibbleBuffer != nullptr)
		{
		    m_nibbleBuffer->clear();
		    delete m_nibbleBuffer;
		}
		if(m_aggregatable)
		{
		    if(m_lsBuffer != nullptr)
		    {
		        m_lsBuffer->clear();
		        delete m_lsBuffer;
		    }
		}
		m_algorithm = 0;
		m_goodChannelList.clear();
		m_histName.clear();
		m_lastAdditionTime = 0;
	    }

	    //Move C'tor
	    HistogramCache(HistogramCache && rhs) noexcept :
		m_nibbleBuffer(std::move(rhs.m_nibbleBuffer)),
		m_lsBuffer(std::move(rhs.m_lsBuffer)),
		m_goodChannelList(std::move(rhs.m_goodChannelList)),
		m_algorithm(std::move(rhs.m_algorithm)),
		m_lastAdditionTime(std::move(rhs.m_lastAdditionTime)),
		m_histName(std::move(rhs.m_histName)),
		m_aggregatable(std::move(rhs.m_aggregatable)),
		m_appLock(toolbox::BSem::FULL)
	    {
		rhs.release();
	    }

	    HistogramCache& operator = (HistogramCache && rhs) & noexcept
	    {
		if (this != & rhs)
		{
			this->release();
			std::swap(this->m_nibbleBuffer, rhs.m_nibbleBuffer);
			std::swap(this->m_lsBuffer, rhs.m_lsBuffer);
			std::swap(this->m_goodChannelList, rhs.m_goodChannelList);
			std::swap(this->m_algorithm, rhs.m_algorithm);
			std::swap(this->m_lastAdditionTime, rhs.m_lastAdditionTime);
			std::swap(this->m_histName, rhs.m_histName);
			std::swap(this->m_aggregatable, rhs.m_aggregatable);

			rhs.release();
		}
		return *this;
	    }

            void printLSBuffer() const 
            {
                std::cout << "LS buffer looks as following:" << std::endl;

                for (const auto& ls : *m_lsBuffer)
                    std::cout << "Type " << m_histName << " Fill " << ls.first.m_fillnum << " Run " << ls.first.m_runnum << " LS " << ls.first.m_lsnum << std::endl;// " aggregate operations " << ls.second.getAggregateOperations() << std::endl;
            }


            void add (toolbox::mem::Reference* ref, std::string pHistName, std::ostream& logstream = std::cout) &
            {
                m_histName = pHistName;
                //here cast the reference into the correct type and decode the nb number
                //get the b2in message header with all the run number, nibble, NB4 etc info, this also includes the channel id
                //this const cast is necessary because Datum does not have a const-qualified accessor ...
                const interface::bril::shared::Datum<DatumT>* t_datum = (interface::bril::shared::Datum<DatumT>*) (ref->getDataLocation() );

                //get the specific info about run, fill, ls, nb, channel
                unsigned int fillnum = t_datum->fillnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int runnum = t_datum->runnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int lsnum = t_datum->lsnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int nbnum = t_datum->nbnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int mod = nbnum % 4;

                unsigned int channelID = t_datum->getChannelID();
                unsigned int algoID = t_datum->getAlgoID();

                /*std::stringstream msg;
                this->print(msg);
                this->printLS(msg);
                logstream << msg.str() << std::endl;*/

		if(nbnum == 0)
		{
		    nbnum = 64; //this should never be the case
		    lsnum-=1;
		    //logstream << RED << "Attention, received NB number  = 0 for Channel " << channelID << " Algorithm " << algoID << " -- this should never happen. I corrected to LS = " << lsnum << " NB " << nbnum << std::endl;
		}

                if (mod != 0)
                {
                    logstream << MAGENTA << "Attention, pulling dirty trick since I had to correct the NB4 number from " << nbnum << " to " << nbnum - mod << RESET << " for channel " << channelID << " algo " << algoID << " ls " << lsnum <<  std::endl;
                    nbnum -= mod;
                }

                unsigned int timestampsec = t_datum->timestampsec;
                unsigned int timestampmsec = t_datum->timestampmsec;
                NB4Identifier t_nibble4 (fillnum, runnum, lsnum, nbnum, timestampsec, timestampmsec);

                //only add if the channel is accepted
                if (( std::find (m_goodChannelList.begin(), m_goodChannelList.end(), channelID) != m_goodChannelList.end() ) && algoID == m_algorithm)
                {
                    //logstream << "Received message on topic " << pHistName << " for Channel " << channelID << " Algo " << algoID << " LS " << lsnum << " NB4 " << nbnum << std::endl;

                    HistogramIdentifier t_identifier (algoID, channelID);

                    //convert the reference into a histogram to start with!
                    Histogram<DataT, nBin> t_hist (*t_datum);

                    //from here on need to protect the m_nibblebuffer with a BSEM
                    Locker t_locker (m_appLock);

                    //now check if that NB4Identifier already exists in the nibble4 map
                    auto nb4 = m_nibbleBuffer->find (t_nibble4);

                    //if it does, call the histogramBuffer.add method with the histogram as argument
                    if (nb4 != std::end (*m_nibbleBuffer) )
                    {
                        nb4->second.add (t_identifier, t_hist);

			if(this->m_aggregatable)
			{
	                        //now check if the nb4 is complete and put it in the ls buffer
                        	if (nb4->second.is_complete() )
                        	{
                        	    LSIdentifier t_LS (fillnum, runnum, lsnum);
                        	    //check if the LS already exists in the cache
                        	    auto t_lsBuffer = m_lsBuffer->find (t_LS);

                        	    //if it does not, then add an empty Buffer object in the map
                        	    if (t_lsBuffer == std::end (*m_lsBuffer) )
                        	    {
                        	        //create a "dumb" NB4 identifier for the histogram cache object to  hold the LS histogram
                        	        //and set the timestampsec to creation time
                        	        toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                        	        unsigned int now = t.sec();
                        	        NB4Identifier t_nb4forLS = t_nibble4;
                        	        t_nb4forLS.m_nbnum = 0;
                        	        t_nb4forLS.m_timestampsec = now;
                        	        t_nb4forLS.m_timestampmsec = 0;
                        	        //this puts an empty histogram buffer for a given lumisection
                        	        m_lsBuffer->emplace (t_LS, HistogramBuffer<uint32_t, nBin> (m_goodChannelList.size(), nb4->second.getHistType(), t_nb4forLS ) );
                        	        t_lsBuffer = m_lsBuffer->find (t_LS); //put the iterator there since now it exists for sure!
                        	    }

                        	    //now the LS exists and I can just call aggregate
                        	    t_lsBuffer->second.aggregate (nb4->second);
                        	    //this->printLSBuffer();
                        	}
			}
                    }
                    else // as in the NB4Identifier does not yet exist in the map
                    {
                        //create a histogram buffer object at location t_nibble4 with no mapped data yet, this happens in add
                        m_nibbleBuffer->emplace (t_nibble4, HistogramBuffer<DataT, nBin> (m_goodChannelList.size() , pHistName, t_nibble4) );
                        //and add the histogram
                        //first put the iterator to the newly created object
                        nb4 = m_nibbleBuffer->find (t_nibble4);
                        nb4->second.add (t_identifier, t_hist);

                        //I just created a new NB4 buffer, so it can be by no means complete and thus can not go to the LS buffer yet
                    }

                    //update last received time
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    m_lastAdditionTime = t.sec();
                }
		else
		{
		    logstream << MAGENTA << "WARNING, received a message for channel " << channelID << " algorithm " << algoID << " which is either not in the good channel list or is not of algorithm ID " << m_algorithm << RESET << std::endl;
		}
            }

            bool is_clean() 
            {
                Locker t_locker (m_appLock);

                if (m_nibbleBuffer->size() == 0) return true;
                else return false;
            }

            unsigned int getLastUpdate() const
            {
                return m_lastAdditionTime;
            }

            void get_completedNB4 (std::vector<HistogramBuffer<DataT, nBin>>& retvec, unsigned int timeout_sec, std::ostream& logstream = std::cout ) &
            {
                Locker t_locker (m_appLock);

                for (auto nb4It = m_nibbleBuffer->begin(); nb4It != m_nibbleBuffer->end();)
                {
                    if (nb4It->second.is_complete() )
                    {
                        retvec.emplace_back (std::move(nb4It->second));
                        nb4It = m_nibbleBuffer->erase (nb4It);
                    }

                    else nb4It++;
                }

            }

            void get_completedLS (std::vector<HistogramBuffer<uint32_t, nBin>>& retvec, unsigned int timeout_sec, std::ostream& logstream = std::cout) &
            {
                Locker t_locker (m_appLock);

		if(this->m_aggregatable)
		{	
                	for (auto buffer = m_lsBuffer->begin(); buffer != m_lsBuffer->end();)
                	{
                	    //first check if the LSbuffer is complete
                	    //this may also be if it timed-out so I need to reScale per channel
                	    buffer->second.check_complete (timeout_sec, logstream);

                	    if (buffer->second.is_complete() )
                	    {
                	        buffer->second.reScale();
                	        retvec.emplace_back (std::move(buffer->second ));
                	        buffer = m_lsBuffer->erase (buffer);
                	    }

                	    else
                	        buffer++;
                	}
		}

	    }


            void print (std::stringstream& msg)
            {
                Locker t_locker (m_appLock);
                //msg << "Histogram Cache currently containing data from  " << m_nibbleBuffer->size() << " NB4: " << std::endl;

                for (const auto& nb4 : *m_nibbleBuffer)
                    msg << nb4.first.m_lsnum << "-" << nb4.first.m_nbnum << ": from  " << nb4.second.size() << " Channels - is complete: " << nb4.second.is_complete() << std::endl;
            }

            void printLS (std::stringstream& msg)
            {
                Locker t_locker (m_appLock);
                msg << RED << "Lumi Section Histogram Cache currently containing data from " << m_lsBuffer->size() << " Lumi Sections" << RESET << std::endl;

                //iterate the lumi sections
                for (const auto& ls : *m_lsBuffer)
                {
                    //iterate the histograms
                    for (const auto& entry : ls.second.m_channelMap)
                    {
                        unsigned int algo;
                        unsigned int channel;
                        entry.first.decode (algo, channel);
                        msg << ls.first.m_lsnum << ": containing " << ls.second.getAggregateOperations (entry.first) << " nb4s for Channel " << channel << " Algorithm " << algo << RESET << std::endl;
                    }
                }

            }


        }; // class Histogram Cache

    } // namespace bcm1futcaprocessor

} // namespace bril
#endif
