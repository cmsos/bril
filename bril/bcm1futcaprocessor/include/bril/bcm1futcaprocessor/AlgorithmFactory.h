#ifndef _bril_bcm1f_utcaprocessor_algorithm_h_
#define _bril_bcm1f_utcaprocessor_algorithm_h_

#include <cmath>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <deque>

//XDATA
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger16.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Serializable.h"

//XDAQ
#include "xdata/Properties.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/Guard.h"
#include "toolbox/squeue.h"

//BRIL
#include "bril/bcm1futcaprocessor/Definition.h"
#include "bril/bcm1futcaprocessor/Channel.h"
#include "bril/bcm1futcaprocessor/Beam.h"
#include "bril/bcm1futcaprocessor/exception/Exception.h"
#include "interface/bril/shared/CommonDataFormat.h"
//ALGORITHMS

namespace bril {
    namespace bcm1futcaprocessor {

        struct MonitoringVariables
        {
            //names of the monitoring variables
            std::list<std::string> m_monVarlist;
            //monitoring variables for fireItemGroupChanged
            std::vector<xdata::Serializable*> m_monVars;
            //beam mode and run infor
            xdata::String m_monBeammode;
            xdata::UnsignedInteger m_monFill;
            xdata::UnsignedInteger m_monRun;
            xdata::UnsignedInteger m_monLs;
            xdata::UnsignedInteger m_monNb;
            xdata::TimeVal m_monTimestamp;
            //lumi numbers
            xdata::Float m_lumiAvg;
            xdata::Float m_lumiAvg_raw;
            //total channel rates
            xdata::Vector<xdata::Float> m_totalChannelRate;
            //background
            xdata::Float m_bg1Plus;
            xdata::Float m_bg2Minus;
	    // amplitude analysis
            xdata::Vector<xdata::UnsignedInteger> m_monMPV;
            xdata::Vector<xdata::UnsignedInteger> m_monTP;
            xdata::Vector<xdata::UnsignedInteger> m_monTotal;
            xdata::Vector<xdata::UnsignedInteger> m_monMinBin;

        };

        class AmplitudeAlgorithm
        {

          public:
            virtual ~AmplitudeAlgorithm();
            virtual void compute (  bril::bcm1futcaprocessor::AmplitudeHistogramBuffer*, const Beam&, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring/*, Application::AmplitudeOutput&*/ ) = 0;

            //virtual Application::ChannelIdVector desired_channels() const = 0;

            virtual std::string name() const = 0;

        };

        class LumiAlgorithm
        {

          public:
            virtual ~LumiAlgorithm();

            virtual void compute (  bril::bcm1futcaprocessor::OccupancyHistogramBuffer*, const Beam&, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, AlbedoQueue& albedoQueue, MonitoringVariables& monitoring, bool doAlbedo = false) = 0;

            //virtual Application::ChannelIdVector desired_channels() const = 0;

            virtual std::string name() const = 0;

        };

        class AggregateAlgorithm
        {

          public:
            virtual ~AggregateAlgorithm();

            virtual void compute (  bril::bcm1futcaprocessor::OccupancyHistogramBuffer*, const Beam&, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring) = 0;

            virtual void compute (  bril::bcm1futcaprocessor::OccupancyHistogramBufferLS*, const Beam&, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring) = 0;

            //virtual Application::ChannelIdVector desired_channels() const = 0;

            virtual std::string name() const = 0;

        };

        class BackgroundAlgorithm
        {

          public:
            virtual ~BackgroundAlgorithm();

            virtual void compute (  bril::bcm1futcaprocessor::OccupancyHistogramBuffer*, const Beam&, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring) = 0;

            virtual std::string name() const = 0;

        };

        class AlbedoAlgorithm
        {

          public:
            virtual ~AlbedoAlgorithm();

            virtual void compute (  const Beam&, AlbedoQueue& albedoQueue, toolbox::BSem& appLock, std::map<std::string, std::vector<float>>& chart_data, std::map<std::string, std::vector<float>>& fraction_data) = 0;

            virtual std::string name() const = 0;

        };

        class AlgorithmFactory
        {

          public:
            static LumiAlgorithm* lumi (xdata::Properties&, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool );

            static AggregateAlgorithm* aggregator (xdata::Properties&, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool );

            static AmplitudeAlgorithm* amplitude ( /*const*/ xdata::Properties&, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool );

            static BackgroundAlgorithm* background ( xdata::Properties&, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool );

            static AlbedoAlgorithm* albedo ( xdata::Properties&, ChannelInfo* channels);

        };
    } //namespace bcm1futcaprocessor
} //namespace bril
#endif
