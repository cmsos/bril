#ifndef _bril_bcm1f_utcaprocessor_timer_h_
#define _bril_bcm1f_utcaprocessor_timer_h_

#include "toolbox/TimeVal.h"
#include <iostream>
#include <chrono>
#include <ctime>

using namespace std::chrono;

namespace bril {

    namespace bcm1futcaprocessor {

        class Timer
        {
          public:
            Timer() : startval(), endval() {}
            virtual ~Timer() {}
            void start()
            {
                startval = toolbox::TimeVal::gettimeofday();
            }
            void stop()
            {
                endval = toolbox::TimeVal::gettimeofday();
            }
            void show (const std::string& label, std::ostream& stream = std::cout)
            {
                double startstamp = startval.sec() * 1e6 + startval.usec();
                double endstamp = endval.sec() * 1e6 + endval.usec();
                stream << label
                       << " started at: " << startval.toString (toolbox::TimeVal::TimeZone::loc)
                       << " finished at: " << endval.toString (toolbox::TimeVal::TimeZone::loc)
                       << "\telapsed time: " << endstamp - startstamp << " microseconds" << std::endl;
            }
            double getElapsedTime()
            {
                double startstamp = startval.sec() * 1e6 + startval.usec();
                double endstamp = endval.sec() * 1e6 + endval.usec();
                return endstamp - startstamp;
            }
            double getCurrentTime()
            {
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                double nowstamp = now.sec() * 1e6 + now.usec();
                double startstamp = startval.sec() * 1e6 + startval.usec();
                return nowstamp - startstamp;
            }
            void reset()
            {
                startval = endval;
            }

          private:
            toolbox::TimeVal startval, endval;

        };
    }
}
#endif
