#ifndef _bril_bcm1f_utcaprocessor_amplitudealgorithm_h_
#define _bril_bcm1f_utcaprocessor_amplitudealgorithm_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"

namespace bril {

    namespace bcm1futcaprocessor {

        class DefaultAmplitude : public AmplitudeAlgorithm
        {
          public:
            DefaultAmplitude (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;
            }

            ~DefaultAmplitude() = default;

	    void compute (  bril::bcm1futcaprocessor::AmplitudeHistogramBuffer* histogramBuffer, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring/*, Application::AmplitudeOutput&*/ )
            {
                if(histogramBuffer != nullptr)
                {
                    //now I have a histogram buffer object
                    //histBuffer.first is the NB4 identifier in case I need it later
                    //histBuffer.second is the Map<HistogramIdentifier, Histogram>
                    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        //now looping the acutal channel histograms
                        //hist.first is the Histogram Identifier
                        //hist.second is the histogram object
                        //ok, here check if the NB4 identifier of the histogram has NB4num ==0 / then it's a lumiscecion hist  
                        if (histogramBuffer->m_nb4Identifier.m_nbnum == 0) 
                        {
                            std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_amp_section_histT::topicname(), this->createAmpLSHist (hist.second, histogramBuffer->m_nb4Identifier, hist.first) );
                            publishQueue.push (t_pair) ;
                            t_pair = std::make_pair (interface::bril::bcm1futca_amp_analysisT::topicname(), this->createAmpAnalysis (hist.second, histogramBuffer->m_nb4Identifier, hist.first, beam, monitoring) );
                            publishQueue.push (t_pair) ;

                        }
                    }

                }

            }

            std::string name() const
            {
                return "DefaultAmplitude";
            }


          private:
            toolbox::mem::Reference* createAmpLSHist (bril::bcm1futcaprocessor::Histogram < uint32_t, interface::bril::BCM1FUTCA_BINAMP> hist, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);
                Histogram < uint32_t, s_nBinAmp/2 > section_hist = hist.rebin<2>();  // conversion to ADC units (512 -> 256)

                toolbox::mem::Reference* t_agghist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_amp_section_histT::maxsize() );
                t_agghist->setDataSize (interface::bril::bcm1futca_amp_section_histT::maxsize() );
                interface::bril::bcm1futca_amp_section_histT* agghist = (interface::bril::bcm1futca_amp_section_histT*) (t_agghist->getDataLocation() );
                agghist->setTime (nb4.m_fillnum, nb4.m_runnum, 0, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                agghist->setFrequency (64);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                agghist->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                agghist->setTotalsize (interface::bril::bcm1futca_amp_section_histT::maxsize() );

                uint32_t summedNibbles = 64;

                std::string pdict = interface::bril::bcm1futca_amp_section_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (agghist->payloadanchor, "summedNibbles", &summedNibbles);
                tc.insert_field (agghist->payloadanchor, "amp_section_hist", section_hist.data() );

                return t_agghist;
            }
            toolbox::mem::Reference* createAmpAnalysis (bril::bcm1futcaprocessor::Histogram < uint32_t, interface::bril::BCM1FUTCA_BINAMP> hist, NB4Identifier nb4, HistogramIdentifier histIdentifier, const Beam& beam, MonitoringVariables& monitoring)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);
                Histogram < uint32_t, s_nBinAmp/2 > adc_hist = hist.rebin<2>();  // conversion to ADC units (512 -> 256)

                toolbox::mem::Reference* t_ampAna = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_amp_analysisT::maxsize() );
                t_ampAna->setDataSize (interface::bril::bcm1futca_amp_analysisT::maxsize() );
                interface::bril::bcm1futca_amp_analysisT* ampAnaData = (interface::bril::bcm1futca_amp_analysisT*) (t_ampAna->getDataLocation() );
                ampAnaData->setTime (nb4.m_fillnum, nb4.m_runnum, 0, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                ampAnaData->setFrequency (64);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                ampAnaData->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                ampAnaData->setTotalsize (interface::bril::bcm1futca_amp_analysisT::maxsize() );

		// what metrics do we need ?
                uint32_t totalhits = 0;
		uint32_t mpvbin = 0;
		uint32_t minbin = 0; 
		uint32_t testpulse = 0;  
		uint32_t maxAmp = 0;
                for (size_t iBin = 0; iBin < adc_hist.size(); iBin++ ) if ( adc_hist[iBin] > 0 )
		{
		    minbin = iBin;
		    break;
		}
                for (size_t iBin = minbin; iBin < adc_hist.size(); iBin++ )
                {
                    
                    totalhits += adc_hist[iBin];
		    if ( adc_hist[iBin] > maxAmp && iBin < 0.9*adc_hist.size() )   // adding condition that mip bin has to be smaller number than 90% of hist size to exclude saturation amps
		    {
			maxAmp = adc_hist[iBin];
			mpvbin = iBin;
		    }
                }
		if (beam.getBeamMode() == "RAMP DOWN") testpulse = mpvbin;  
		
                std::string pdict = interface::bril::bcm1futca_amp_analysisT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);
                interface::bril::shared::CompoundDataStreamer tc (pdict);
		// here need to insert the calculated metrics instead
                tc.insert_field (ampAnaData->payloadanchor, "totalhits", &totalhits);
                tc.insert_field (ampAnaData->payloadanchor, "mpvbin", &mpvbin);
                tc.insert_field (ampAnaData->payloadanchor, "minbin", &minbin);
                tc.insert_field (ampAnaData->payloadanchor, "testpulse", &testpulse);

                monitoring.m_monMPV[t_channel - 1] = mpvbin;
                monitoring.m_monTP[t_channel - 1]= testpulse;
                monitoring.m_monTotal[t_channel - 1] = totalhits;
                monitoring.m_monMinBin[t_channel - 1] = minbin;

                return t_ampAna;
	    }

          private:
            ChannelInfo* m_channelInfo;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;


        };
    } //bcm1futcaprocessor
} //bril
#endif

// structure for reference:
//bcm1futca_amp_analysis, "totalhits:uint32:1 mipbin:uint32:1 meanbin:uint32:1 testpulse:uint32:1", "bcm1futca amplitude histogram with 256 bins", "hits, ADC"
