//TODO:
//does the agghist need to be shifted left so the MIB bin of BX0 appears at the end of the abort gap?
//find a proper handling of the summed nibbles for the occ section hist
#ifndef _bril_bcm1f_utcaprocessor_aggregatealgorithm_h_
#define _bril_bcm1f_utcaprocessor_aggregatealgorithm_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class DefaultAggregator : public AggregateAlgorithm
        {
          public:
            DefaultAggregator (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;
            }

	    ~DefaultAggregator() = default;

            void compute (  OccupancyHistogramBuffer* histogramBuffer, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring)
            {

                //first, loop the Histogram Vector
                //for (auto& histBuffer : histogramVector)
                if(histogramBuffer != nullptr)
                {
                    m_totalRateMap.clear();
                    //now I have a histogram buffer object
                    //histBuffer.first is the NB4 identifier in case I need it later
                    //histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    //here i should instantiate the perSensor type arrays

                    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        //now looping the acutal channel histograms
                        //hist.first is the Histogram Identifier
                        //hist.second is the histogram object

                        //ok, here check if the NB4 identifier of the histogram has NB4num ==0 / then it's a lumiscecion hist
                        if (histogramBuffer->m_nb4Identifier.m_nbnum != 0)
                        {
                            std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_agg_histT::topicname(), this->createAggHist (hist.second, histogramBuffer->m_nb4Identifier, hist.first) );
                            publishQueue.push (t_pair) ;
                        }
                    }


                    //now fill the total channel rates vector
                    for (auto ch : m_totalRateMap)
                    {
                        unsigned int algo = 0;
                        unsigned int channel = 0;
                        ch.first.decode (algo, channel);
                        monitoring.m_totalChannelRate[channel - 1] = ch.second;

                    }
                }
            }

            void compute (  OccupancyHistogramBufferLS* histogramBuffer, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring)
            {
                //first, loop the Histogram Vector
                //for (auto& histBuffer : histogramVector)
                if(histogramBuffer != nullptr)
                {
                    //now I have a histogram buffer object
                    //histBuffer.first is the NB4 identifier in case I need it later
                    //histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    //here i should instantiate the perSensor type arrays
                    //lumi_bx, lumi_bx_raw, inst_lumi, inst_lumi_raw
                    //to be fair, I need these split up by AlgoIDs too

                    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        //now looping the acutal channel histograms
                        //hist.first is the Histogram Identifier
                        //hist.second is the histogram object

                        //ok, here check if the NB4 identifier of the histogram has NB4num ==0 / then it's a lumiscecion hist

                        if (histogramBuffer->m_nb4Identifier.m_nbnum == 0)
                        {
                            // this is a lumi section hist
                            std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_occ_section_histT::topicname(), this->createOccSectionHist (hist.second, histogramBuffer->m_nb4Identifier, hist.first) );
                            publishQueue.push (t_pair) ;
                        }
                    }
                }
            }


            std::string name() const
            {
                return "DefaultAggregator";
            }

          private:

            toolbox::mem::Reference* createAggHist (bril::bcm1futcaprocessor::Histogram < uint16_t, interface::bril::shared::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);
                //first, get a rebinned histogram per bx that I can publish as agghist
                Histogram < uint16_t, interface::bril::shared::MAX_NBX> agg_hist = hist.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                //agg_hist.shift (-1); 
                agg_hist.maskAbortGap();

                //compute total rates??
                uint32_t totalrate = agg_hist.integral();

                m_totalRateMap[histIdentifier] = totalrate;

                toolbox::mem::Reference* t_agghist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_agg_histT::maxsize() );
                t_agghist->setDataSize (interface::bril::bcm1futca_agg_histT::maxsize() );
                interface::bril::bcm1futca_agg_histT* agghist = (interface::bril::bcm1futca_agg_histT*) (t_agghist->getDataLocation() );
                agghist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                agghist->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                agghist->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                agghist->setTotalsize (interface::bril::bcm1futca_agg_histT::maxsize() );

                std::string pdict = interface::bril::bcm1futca_agg_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (agghist->payloadanchor, "totalhits", &totalrate );
                tc.insert_field (agghist->payloadanchor, "agghist", agg_hist.data() );

                return t_agghist;
            }

            toolbox::mem::Reference* createOccSectionHist (bril::bcm1futcaprocessor::Histogram < uint32_t, interface::bril::shared::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);
                Histogram < uint32_t, s_nBinOcc > section_hist = static_cast<Histogram<uint32_t, s_nBinOcc>> (hist); //rebin by a factor of 6 to get the per BX histogram

                toolbox::mem::Reference* t_agghist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_occ_section_histT::maxsize() );
                t_agghist->setDataSize (interface::bril::bcm1futca_occ_section_histT::maxsize() );
                interface::bril::bcm1futca_occ_section_histT* agghist = (interface::bril::bcm1futca_occ_section_histT*) (t_agghist->getDataLocation() );
                agghist->setTime (nb4.m_fillnum, nb4.m_runnum, 0, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                agghist->setFrequency (64);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                agghist->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                agghist->setTotalsize (interface::bril::bcm1futca_occ_section_histT::maxsize() );

                uint32_t summedNibbles = 64;

                std::string pdict = interface::bril::bcm1futca_occ_section_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (agghist->payloadanchor, "summedNibbles", &summedNibbles);
                tc.insert_field (agghist->payloadanchor, "occ_section_hist", section_hist.data() );

                return t_agghist;
            }

          private:
            ChannelInfo* m_channelInfo;
            std::map < HistogramIdentifier, uint32_t> m_totalRateMap;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
