#ifndef _bril_bcm1f_utcaprocessor_zerocounting_h_
#define _bril_bcm1f_utcaprocessor_zerocounting_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class ZeroCounting : public LumiAlgorithm
        {
          public:
            ZeroCounting (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool ) :
                m_commissioning (false)
            {
                m_channelInfo = channels;
	        m_channelInfoAutoMask = *channels;

                m_memPoolFactory = factory;
                m_memPool = pool;

                m_calibtag = props.getProperty ("calibtag");

                std::string mode = props.getProperty ("commissioningmode");
                if (mode.find ("true") != std::string::npos) m_commissioning = 1;
                else m_commissioning = 0;

                std::string automask = props.getProperty ("automasking");
                if (automask.find ("true") != std::string::npos) m_autoMasking = 1;
                else m_autoMasking = 0;

            }

	    ~ZeroCounting() = default;

            void compute (  OccupancyHistogramBuffer* histogramBuffer, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, AlbedoQueue& albedoQueue, MonitoringVariables& monitoring, bool doAlbedo = false)
            {
                float nb4Orbits = 4096.*4.;
		unsigned int t_algo = m_channelInfo->getAlgo();

                //first, loop the Histogram Vector
                //for (auto& histBuffer : histogramVector)
                if(histogramBuffer!=nullptr)
                {
                    //first, the References for the lumi numbers
                    toolbox::mem::Reference* bcm1flumi = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futcalumiT::maxsize() );
                    interface::bril::bcm1futcalumiT* lumihist = this->getHistPtr<interface::bril::bcm1futcalumiT> (bcm1flumi, histogramBuffer->m_nb4Identifier, 0, t_algo);

                    //For the lumi topic
                    std::string pdict = interface::bril::bcm1futcalumiT::payloaddict();
                    xdata::Properties plist;
                    plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                    plist.setProperty ("PAYLOAD_DICT", pdict);

                    //now I have a histogram buffer object
                    //histBuffer.first is the NB4 identifier in case I need it later
                    //histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    //to be fair, I need these split up by AlgoIDs too

                    //channel maks for lumi publishing
                    uint32_t masklow = 0;
                    uint32_t maskhigh = 0;

                    //these are for the global average
                    float avg_lumi = 0;

                    float avg_lumi_raw = 0; 

                    std::vector<float> avg_lumi_bx(interface::bril::shared::MAX_NBX, 0.0);
                    std::vector<float> avg_lumi_bx_raw(interface::bril::shared::MAX_NBX, 0.0);


                    int total_count = 0;

                    //to temporarily store all the uncorrected muBX for the albedo queue
                    std::map<HistogramIdentifier, std::vector<float>> t_albedoQueueMap{};


		    // loop over channels to create average lumi for reference
		    if( m_autoMasking==1 ){
		    float refLumi = 0;
                    int temp_count = 0;
		    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        Histogram < uint16_t, interface::bril::shared::MAX_NBX> t_hist = hist.second.rebin<6>(); 
			if (m_channelInfoAutoMask.useLumi (t_channel) )
                        {

			    Corrections t_corr = m_channelInfo->getCorrections (hist.first);
                            for (size_t iBX = 0; iBX < interface::bril::shared::MAX_NBX; iBX++ )
                            {
				if (beam.isColliding (iBX) )
                                {
				    float muBX_uncorr = 0;
				    float rate0 = 1. - ( (float) t_hist.at (iBX) / ( (float) m_channelInfo->getFactor (hist.first) * (float) nb4Orbits ) );
				    if (rate0 <= 0.)
				      muBX_uncorr= 0;
				    else
                                      muBX_uncorr= -1. * log (rate0);

				    refLumi += muBX_uncorr * 11246 / t_corr.m_sigmaVis + pow (muBX_uncorr, 2) * pow ( (11246 / t_corr.m_sigmaVis), 2) * t_corr.m_nonLinearity * (1 + t_corr.m_nonLinearity * t_corr.m_calibSBIL);
				}
			    }
        	            temp_count += 1;
			}
		    }
		    refLumi = refLumi /( (float) temp_count );
		    // new mask
		    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
			if (m_channelInfo->useLumi (t_channel) )
                        {
        	                Histogram < uint16_t, interface::bril::shared::MAX_NBX> t_hist = hist.second.rebin<6>(); 
	    		        Corrections t_corr = m_channelInfo->getCorrections (hist.first);
				float lumi_temp = 0;
                                for (size_t iBX = 0; iBX < interface::bril::shared::MAX_NBX; iBX++ )
                                {
				    if (beam.isColliding (iBX) )
                                    {
				        float muBX_uncorr = 0;
				        float rate0 = 1. - ( (float) t_hist.at (iBX) / ( (float) m_channelInfo->getFactor (hist.first) * (float) nb4Orbits ) );
  				        if (rate0 <= 0.)
				          muBX_uncorr= 0;
				        else
                                          muBX_uncorr = -1. * log (rate0);

				        lumi_temp += muBX_uncorr * 11246 / t_corr.m_sigmaVis + pow (muBX_uncorr, 2) * pow ( (11246 / t_corr.m_sigmaVis), 2) * t_corr.m_nonLinearity * (1 + t_corr.m_nonLinearity * t_corr.m_calibSBIL);

				   }
			     }
  			     if ( (beam.getBeamMode()=="STABLE BEAMS") && ( (lumi_temp-refLumi)/refLumi < -0.2 ) ) {  
				m_channelInfoAutoMask.setMask(t_channel);
				std::cout << "WARNING: channel " << t_channel << " excluded from lumi automatically due to rate < 0.8*average, av.: " << refLumi << " " << abs(lumi_temp-refLumi)/refLumi << std::endl; 
			    }

			}

		    }
		    } // if auto masking on
                    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        //now looping the acutal channel histograms
                        //hist.first is the Histogram Identifier
                        //hist.second is the histogram object


                        //then, rebin - this is now the agghist after masking the abort gap
                        Histogram < uint16_t, interface::bril::shared::MAX_NBX> t_hist = hist.second.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                        t_hist.maskAbortGap();//maks the abort gap first before integrating for total rate


                        //now test if we want this channel for lumi calculations
                        if (m_channelInfoAutoMask.useLumi (t_channel) )
                        {
                            total_count += 1;

                            //for the per channel lumi publication
                            uint32_t t_masklow = 0;
                            uint32_t t_maskhigh = 0;

                            if (t_channel < 32)
                            {
                                masklow |= (1 << (t_channel - 1) );
                                t_masklow |= (1 << (t_channel - 1) );
                            }
                            else
                            {
                                maskhigh |= (1 << (t_channel - 33) ); //equivalent to t_channel-1-32
                                t_maskhigh |= (1 << (t_channel - 33) ); //equivalent to t_channel-1-32
                            }

                            int t_nonZeroBunches = 0;
                            float muBX[interface::bril::shared::MAX_NBX];
                            memset (muBX, 0, sizeof (muBX) );
                            float muBX_uncorrected[interface::bril::shared::MAX_NBX];
                            memset (muBX_uncorrected, 0, sizeof (muBX_uncorrected) );
                            float lumiBX[interface::bril::shared::MAX_NBX];
                            memset (lumiBX, 0, sizeof (lumiBX) );
                            float lumiChannel = 0;
                            float lumiChannelRaw = 0;

                            //NOW the actual lumi claculation
                            //but first, get the albedo fraction for this channel and algorithm
                            //first we need the correction factors
                            Corrections t_corr = m_channelInfo->getCorrections (hist.first);
                            //t_corr.print();

                            //for (size_t iBX = 0; iBX < t_hist.size(); iBX++ )
                            //TODO: this is also looping over the abort gap -- need to be sure with test pulses
                            for (size_t iBX = 0; iBX < interface::bril::shared::MAX_NBX; iBX++ )
                            {
                                //TODO: this just leaves out the abort gap, except for the last bin since we shifted MIB from BX0 there - not sure if this is viable
                                if (iBX > 3533 && iBX < interface::bril::shared::MAX_NBX) continue;

                                //calculate 1 - probability of not having a zero (so # of non-zero/# of orbits)
                                float rate0 = 1. - ( (float) t_hist.at (iBX) / ( (float) m_channelInfo->getFactor (hist.first) * (float) nb4Orbits ) );

                                //if negative, set 0
                                if (rate0 <= 0.)
                                     muBX_uncorrected[iBX]= 0;

                                //calculate the pileup parameter as per 0counting formula
                                //first the uncorrected
                                else
                                    muBX_uncorrected[iBX] = -1. * log (rate0);

                                //and now the albedo corrected version
                                muBX[iBX] = muBX_uncorrected[iBX] * m_channelInfo->getAlbedoFraction (hist.first, iBX);//TODO - m_channelInfo->getNoise (hist.first);

                                //if(iBX == 0)std::cout << YELLOW << "Debug: Channel: " << t_channel << " Algo: " << t_algo << " SigmaVis: "<<t_corr.m_sigmaVis << RESET << std::endl;
                                //now calculate some lumi
                                lumiBX[iBX] = muBX[iBX] * 11246 / t_corr.m_sigmaVis + pow (muBX[iBX], 2) * pow ( (11246 / t_corr.m_sigmaVis), 2) * t_corr.m_nonLinearity * (1 + t_corr.m_nonLinearity * t_corr.m_calibSBIL);

                                //the ones that distinguish algoID
                                avg_lumi_bx.at (iBX) += lumiBX[iBX];
                                avg_lumi_bx_raw.at (iBX) += muBX[iBX];

                                //test if the bunch is colliding
                                if (beam.isColliding (iBX) )
                                {
                                    lumiChannel += lumiBX[iBX];
                                    lumiChannelRaw += muBX[iBX];
                                    t_nonZeroBunches++;
                                }
                            } // end of BX loop

                            //now can insert uncorrected muBX to albedo queue
                            if(doAlbedo)
			    {
                            	std::vector<float> t_tmpVec (muBX_uncorrected, muBX_uncorrected + interface::bril::shared::MAX_NBX );
                            	t_albedoQueueMap[hist.first] = std::move(t_tmpVec);
			    }

                            avg_lumi += lumiChannel;
                            avg_lumi_raw += lumiChannelRaw;

                            if (m_commissioning || (beam.m_vdmFlag/* && beam.m_vdmIP5*/) )
                                //only publish by channel histograms if the comissioning mode is enabled or the VdM flag is set
                            {
                                //create a new Refernce for this channel and algo
                                toolbox::mem::Reference* channellumi = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futcalumiT::maxsize() );
                                interface::bril::bcm1futcalumiT* channelhist = this->getHistPtr<interface::bril::bcm1futcalumiT> (channellumi, histogramBuffer->m_nb4Identifier, t_channel, t_algo);
                                //now need to fill the compound data streamer with the per channel lumi data
                                this->fillDataChannel<interface::bril::bcm1futcalumiT> (channelhist, pdict, lumiChannelRaw, lumiChannel, muBX, lumiBX, t_masklow, t_maskhigh);

                                //and push to the queue
                                //needed to change the topic name for per channel data
                                //make topicname
                                std::stringstream ss;
                                ss << "bcm1futcalumi" << t_channel;
                                std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (ss.str(), channellumi);
                                publishQueue.push (t_pair);

                            } // end of commissioning mode
                        } // end of if(useLumi)
                    } // end of channel / algo loop

                    //now push the albedoqueuemap to the albedo queue
                    if(doAlbedo)
			albedoQueue.push_back (std::move(t_albedoQueueMap));


                    //here normalize the inst lumi by the number of channels and normalize the lumi_bx by the number of channels
                    //i.e. build the average
                    if (total_count)
                    {
                        avg_lumi /= (float) total_count;
                        avg_lumi_raw /= (float) total_count;

                        for (size_t iBX = 0; iBX < interface::bril::shared::MAX_NBX; iBX++)
                        {
                            avg_lumi_bx[iBX] /= (float) total_count;
                            avg_lumi_bx_raw[iBX] /= (float) total_count;
                        } // end of iBX loop
                    } // end of if(counter)


                    //////////////////////////////////////////////////////////
                    //Fill the data for the different types in the toolbox::mem::Reference and push to queue
                    //////////////////////////////////////////////////////////
                    std::stringstream topicname;

                    this->fillData<interface::bril::bcm1futcalumiT> (lumihist, pdict, avg_lumi_raw, avg_lumi, avg_lumi_bx_raw, avg_lumi_bx, masklow, maskhigh);
                    std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futcalumiT::topicname(), bcm1flumi);
                    publishQueue.push (t_pair);

                    //////////////////////////////////////////////////////////
                    //Fill the monitoring variables
                    //////////////////////////////////////////////////////////
                    monitoring.m_monBeammode = beam.getBeamMode();
                    monitoring.m_monFill = histogramBuffer->m_nb4Identifier.m_fillnum;
                    monitoring.m_monRun = histogramBuffer->m_nb4Identifier.m_runnum;
                    monitoring.m_monLs = histogramBuffer->m_nb4Identifier.m_lsnum;
                    monitoring.m_monNb = histogramBuffer->m_nb4Identifier.m_nbnum;
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    monitoring.m_monTimestamp = t;
                    monitoring.m_lumiAvg = avg_lumi;
                    monitoring.m_lumiAvg_raw = avg_lumi_raw;
                } // end of histogrambuffer != nullptr
            }

            template<class DataT>
            DataT* getHistPtr (toolbox::mem::Reference* ref, NB4Identifier nb4, unsigned int channel, unsigned int algo)
            {
                ref->setDataSize (DataT::maxsize() );
                DataT* hist = (DataT*) (ref->getDataLocation() );

                hist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                hist->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                hist->setResource (bestsource, algo, channel, interface::bril::shared::StorageType::COMPOUND);
                hist->setTotalsize (DataT::maxsize() );

                return hist;
            }

            template<class DataT>
            void fillDataChannel (DataT* hist, std::string dict, float avgraw, float avg, float* bxraw, float* bx, uint32_t masklow, uint32_t maskhigh )
            {
                //now need to fill the compound data streamer with the lumi data
                interface::bril::shared::CompoundDataStreamer streamer (dict);
                streamer.insert_field (hist->payloadanchor, "calibtag", m_calibtag.c_str() );
                streamer.insert_field (hist->payloadanchor, "avgraw", &avgraw );
                streamer.insert_field (hist->payloadanchor, "avg", &avg );
                streamer.insert_field (hist->payloadanchor, "bxraw", bxraw );
                streamer.insert_field (hist->payloadanchor, "bx", bx );
                streamer.insert_field (hist->payloadanchor, "masklow", &masklow );
                streamer.insert_field (hist->payloadanchor, "maskhigh", &maskhigh );
            }

            template<class DataT>
            void fillData (DataT* hist, std::string dict, float avgraw, float avg, std::vector<float> bxraw, std::vector<float> bx, uint32_t masklow, uint32_t maskhigh )
            {
                //copy the contents of the vector into an array
                float lumi_bx_array[interface::bril::shared::MAX_NBX];
                float lumi_bx_raw_array[interface::bril::shared::MAX_NBX];

                std::copy (bx.begin(), bx.end(), lumi_bx_array);
                std::copy (bxraw.begin(), bxraw.end(), lumi_bx_raw_array);
                //now need to fill the compound data streamer with the lumi data
                interface::bril::shared::CompoundDataStreamer streamer (dict);
                streamer.insert_field (hist->payloadanchor, "calibtag", m_calibtag.c_str() );
                streamer.insert_field (hist->payloadanchor, "avgraw", &avgraw );
                streamer.insert_field (hist->payloadanchor, "avg", &avg );
                streamer.insert_field (hist->payloadanchor, "bxraw", lumi_bx_raw_array );
                streamer.insert_field (hist->payloadanchor, "bx", lumi_bx_array );
                streamer.insert_field (hist->payloadanchor, "masklow", &masklow );
                streamer.insert_field (hist->payloadanchor, "maskhigh", &maskhigh );
            }

            std::string name() const
            {
                return "ZeroCounting";
            }
	 

          private:
            ChannelInfo* m_channelInfo;
	    ChannelInfo m_channelInfoAutoMask;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
            std::string m_calibtag;
            bool m_commissioning;
	    bool m_autoMasking;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
