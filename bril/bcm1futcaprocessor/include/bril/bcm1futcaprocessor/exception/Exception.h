#ifndef _bril_bcm1f_utcaprocessor_exception_Exception_h_
#define _bril_bcm1f_utcaprocessor_exception_Exception_h_

#include "xcept/Exception.h"

namespace bril {

    namespace bcm1futcaprocessor {

        namespace exception {

            class SetupException: public xcept::Exception
            {
              public:
                SetupException ( std::string name, std::string message, std::string module, int line, std::string function ) : xcept::Exception (name, message, module, line, function) {}
                SetupException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : xcept::Exception (name, message, module, line, function, e) {}
            }; //class SetupException

            class EventingException: public xcept::Exception
            {
              public:
                EventingException ( std::string name, std::string message, std::string module, int line, std::string function ) : xcept::Exception (name, message, module, line, function) {}
                EventingException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : xcept::Exception (name, message, module, line, function, e) {}
            }; //class EventingException
            class AlgorithmException: public xcept::Exception
            {
              public:
                AlgorithmException ( std::string name, std::string message, std::string module, int line, std::string function ) : xcept::Exception (name, message, module, line, function) {}
                AlgorithmException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : xcept::Exception (name, message, module, line, function, e) {}
            }; //class UnknownException

            class UnknownException: public xcept::Exception
            {
              public:
                UnknownException ( std::string name, std::string message, std::string module, int line, std::string function ) : xcept::Exception (name, message, module, line, function) {}
                UnknownException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : xcept::Exception (name, message, module, line, function, e) {}
            }; //class UnknownException
            class PointerException: public xcept::Exception
            {
              public:
                PointerException ( std::string name, std::string message, std::string module, int line, std::string function ) : xcept::Exception (name, message, module, line, function) {}
                PointerException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : xcept::Exception (name, message, module, line, function, e) {}
            }; //class UnknownException

        } //namespace exception
    } //namespace bcm1futcaprocessor

    XCEPT_DEFINE_EXCEPTION (bcm1fprocessor, HardwareError)

} //namespace bril
#endif
