#ifndef _bril_bcm1f_utcaprocessor_channel_h_
#define _bril_bcm1f_utcaprocessor_channel_h_

#include <algorithm>
#include <numeric>
#include <bitset>

#include "xdata/Properties.h"
#include "xdata/Vector.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "bril/bcm1futcaprocessor/HistogramCache.h"

namespace bril {

    namespace bcm1futcaprocessor {

        enum class SensorType {Si, Disconnected, All};

        enum class SensorPosition {plus, minus, undefined};

        struct SensorIdentifier
        {
            SensorType m_type;
            SensorIdentifier (SensorType type, unsigned int algo) :
                m_type (type)
            {}
            bool operator < (const SensorIdentifier& rhs) const
            {
                return this->m_type < rhs.m_type;
            }
        };
        struct Corrections
        {
            float m_sigmaVis;
            float m_calibSBIL;
            float m_nonLinearity;
            float m_factor;

            Corrections (float sigmavis, float calibsbil, float nonlinearity, float otherfactor) :
                m_sigmaVis (sigmavis),
                m_calibSBIL (calibsbil),
                m_nonLinearity (nonlinearity),
                m_factor (otherfactor)
            {}

            const void print()
            {
                std::cout << "Sigma visible: " << m_sigmaVis << " nonlinearity: " << m_nonLinearity << " calib SBIL: " << m_calibSBIL << std::endl;
            }
        };

        struct AlbedoFraction
        {
            std::vector<float> m_fraction;
            //float m_fraction[interface::bril::MAX_NBX];
            float m_noise;

            AlbedoFraction() :
                m_fraction (interface::bril::shared::MAX_NBX, 1.),
                m_noise (0)
            {
            }

            const float getFraction (size_t iBX) const
            {
                if (iBX < interface::bril::shared::MAX_NBX)
                    return m_fraction.at (iBX);
                else return 1.;
            }

            const float& getNoise() const
            {
                return m_noise;
            }
            void setFraction (size_t& iBX, float& value) &
            {
                if (iBX < interface::bril::shared::MAX_NBX)
                    m_fraction.at (iBX) = value;
            }

            void setNoise (float& value) &
            {
                m_noise = value;
            }
        };

        class ChannelInfo
        {
          private:

	    unsigned int m_algo;
            std::vector<unsigned int> m_goodChannelList;

            std::bitset<interface::bril::BCM1FUTCA_NCHANNELS> m_channelForLumi;
            std::bitset<interface::bril::BCM1FUTCA_NCHANNELS> m_channelForBkg;

            using ChannelDelayMap = std::map<HistogramIdentifier, int>;
            ChannelDelayMap m_delayMap;

            using CorrectionMap = std::map<HistogramIdentifier, Corrections>;
            CorrectionMap m_corrections;

            using AlbedoMap = std::map<HistogramIdentifier, AlbedoFraction>;
            AlbedoMap m_albedo;

            std::vector<HistogramIdentifier> m_histIdentifierVector;

          public:
            ChannelInfo() :
		m_algo(0),
                m_goodChannelList (0)
            {
                m_goodChannelList.clear();
                m_channelForLumi.reset();
                m_channelForBkg.reset();
                m_delayMap.clear();
                m_corrections.clear();
                m_albedo.clear();
                m_histIdentifierVector.clear();
            }

            ChannelInfo& operator = (const ChannelInfo& rhs)
            {
                this->m_goodChannelList = rhs.m_goodChannelList;
                this->m_channelForLumi = rhs.m_channelForLumi;
                this->m_channelForBkg = rhs.m_channelForBkg;
                this->m_delayMap = rhs.m_delayMap;
                this->m_corrections = rhs.m_corrections;
                this->m_albedo = rhs.m_albedo;
                this->m_histIdentifierVector = rhs.m_histIdentifierVector;
                return *this;
            }

            ~ChannelInfo();

            void setupChannels (xdata::Vector<xdata::Properties>& channelConfig, unsigned int algorithm, std::stringstream& logstream) &
            {
		this->m_algo = algorithm;

                logstream << BOLDGREEN << std::endl << "######################################################" << RESET << std::endl;
                logstream << BOLDGREEN << "Channel Configuration (Algorithm " << m_algo << "): " << RESET << std::endl;

                //first the input
                for (size_t channel = 0; channel < channelConfig.elements(); channel++)
                {
                    unsigned int t_channel = std::stoul (channelConfig[channel].getProperty ("channel") );
                    m_goodChannelList.push_back (t_channel );

                    bool t_useLumi = (channelConfig[channel].getProperty ("uselumi") == "true") ? true : false;

                    if (t_useLumi) m_channelForLumi.set (t_channel - 1);

                    bool t_useBkg = (channelConfig[channel].getProperty ("usebkg") == "true") ? true : false;

                    if (t_useBkg) m_channelForBkg.set (t_channel - 1);

                    int t_delay = std::stoi (channelConfig[channel].getProperty ("delay") );
                    m_delayMap.emplace (HistogramIdentifier (1, t_channel), t_delay);

                    //corrections on  a per channel / algo basis
                    float t_sigmaVis = std::stof (channelConfig[channel].getProperty ("sigmavis") );
                    float t_calibSBIL = std::stof (channelConfig[channel].getProperty ("calibSBIL") );
                    float t_nonLinearity = std::stof (channelConfig[channel].getProperty ("nonlinearity") );
                    float t_factor = std::stof (channelConfig[channel].getProperty ("factor") );

                    //initialize the albedo fraction
                    m_albedo.emplace (HistogramIdentifier (m_algo, t_channel), AlbedoFraction() );

                    m_corrections.emplace (HistogramIdentifier (m_algo, t_channel), Corrections (t_sigmaVis, t_calibSBIL, t_nonLinearity, t_factor) );

                    //for convenience
                    m_histIdentifierVector.push_back (HistogramIdentifier (m_algo, t_channel) );

                    logstream << BLUE << "Channel: " << GREEN << t_channel << BLUE << " - use Lumi: " << YELLOW << t_useLumi << BLUE << " use BGK: " << YELLOW << t_useBkg << BLUE << " delay: " << CYAN << t_delay << RESET << std::endl;
                    logstream << CYAN << "Corrections: " ;
                    logstream << BLUE << " SigmaVis: " << t_sigmaVis; 
                    logstream << MAGENTA << " CalibSBIL: " << t_calibSBIL;
                    logstream << YELLOW << " Nonlinearity: " << t_nonLinearity;
                    logstream << GREEN << " OtherFactor: " << t_factor << RESET << std::endl;
                }

                logstream << BOLDGREEN << "######################################################" << RESET << std::endl;
            }

            const unsigned int& getAlgo() const
	    {
		return m_algo;
	    }

            const std::vector<HistogramIdentifier>& getHistIdentifierVector() const
            {
                return m_histIdentifierVector;
            }

            const std::vector<unsigned int>& getChannelVector() const
            {
                return m_goodChannelList;
            }

            const int getChannelDelay (const HistogramIdentifier& identifier)
            {
                auto hist_delay = m_delayMap.find (identifier);

                if (hist_delay != std::end (m_delayMap) )
                    return hist_delay->second;
                else
                    return 0;
            }

            const float getAlbedoFraction (const HistogramIdentifier& identifier, size_t& iBX)
            {
                auto albedo = m_albedo.find (identifier);
                float fraction = 1.;

                if (albedo != std::end (m_albedo) )
                    fraction = albedo->second.getFraction (iBX);

                return fraction;
            }

            const float getNoise (const HistogramIdentifier& identifier) const
            {
                auto albedo = m_albedo.find (identifier);
                float noise = 0.;

                if (albedo != std::end (m_albedo) )
                    noise = albedo->second.getNoise ();

                return noise;
            }

            const float getAlbedoFraction (unsigned int& algo, unsigned int& channel, size_t& iBX) const
            {
                HistogramIdentifier t_identifier (algo, channel);
                auto albedo = m_albedo.find (t_identifier);

                if (albedo != std::end (m_albedo) )
                    return albedo->second.getFraction (iBX);

                else
                    return 1.;
            }

            void setAlbedoFraction (const HistogramIdentifier& identifier, size_t& iBX, float value) &
            {
                auto albedo = m_albedo.find (identifier);

                if (albedo != std::end (m_albedo) )
                    albedo->second.setFraction (iBX, value);
            }

            void setNoise (const HistogramIdentifier& identifier, float& value) &
            {
                auto albedo = m_albedo.find (identifier);

                if (albedo != std::end (m_albedo) )
                    albedo->second.setNoise (value);
            }

            void setMask (unsigned int& channel) &
            {
		m_channelForLumi.reset (channel - 1);
		m_channelForBkg.reset (channel - 1);
            }


            const float getFactor (const HistogramIdentifier& identifier) const
            {
                auto factor = m_corrections.find (identifier);

                if (factor != std::end (m_corrections) ) return factor->second.m_factor;
                else return 0;
            }

            const Corrections getCorrections (const HistogramIdentifier& identifier) const
            {
                auto correction = m_corrections.find (identifier);

                if (correction != std::end (m_corrections) ) return correction->second;
                //if not found return just ones
                else return Corrections (1, 1, 1, 1);
            }

            const bool useLumi (unsigned int& channel) const
            {
                return m_channelForLumi.test (channel - 1);
            }

            const bool useBkg (unsigned int& channel) const
            {
                return m_channelForBkg.test (channel - 1);
            }

            const SensorType getSensorType (unsigned int& channel) const
            {
                //to account for the fact that channels start counting from 1 - is this necessary?
                channel -= 1;


                //these are the 2021 channels without signal
                if (channel == 28 || channel == 29 || channel == 30 || channel == 31 || channel == 42 || channel == 43) return SensorType::Disconnected;

                else if (channel >= 0 && channel <= 47) return SensorType::Si;

                return SensorType::Disconnected;

            }

            const SensorPosition getSensorPosition (unsigned int& channel) const
            {
                if (channel > 0 && channel < 25) return SensorPosition::plus;
                else if (channel > 24 && channel < 49) return SensorPosition::minus;
                else return SensorPosition::undefined;

            }


        };

    } //namespace bcm1futcaprocessor
} //namespace bril

#endif
