// $Id$
#ifndef _bril_bcm1futcaprocessor_version_h_
#define _bril_bcm1futcaprocessor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MINOR 7
#define BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_PATCH 7
#define BRILBCM1FUTCAPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MAJOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MINOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILBCM1FUTCAPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILBCM1FUTCAPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MAJOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MINOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILBCM1FUTCAPROCESSOR_FULL_VERSION_LIST BRIL_BRILBCM1FUTCAPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MAJOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_MINOR,BRIL_BRILBCM1FUTCAPROCESSOR_VERSION_PATCH)
#endif

namespace brilbcm1futcaprocessor {
    const std::string project = "bril";
    const std::string package = "brilbcm1futcaprocessor";
    const std::string versions = BRIL_BRILBCM1FUTCAPROCESSOR_FULL_VERSION_LIST;
    const std::string summary = "BRIL DAQ bcm1futcaprocessor";
    const std::string description = "collect and process bcm1f histograms from bcm1futcasource";
    const std::string authors = "G. Auzinger";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo ();
    void checkPackageDependencies ();
    std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
