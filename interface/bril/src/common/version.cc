#include "interface/bril/version.h"
#include "config/version.h"
GETPACKAGEINFO(interfacebril)
void interfacebril::checkPackageDependencies() {
  CHECKDEPENDENCY(config);
}
std::set<std::string, std::less<std::string> > interfacebril::getPackageDependencies(){
  std::set<std::string, std::less<std::string> > dependencies;
  
  ADDDEPENDENCY(dependencies,config); 
	 
  return dependencies;
}       
