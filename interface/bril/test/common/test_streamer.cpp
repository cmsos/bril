#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/shared/LUMITopics.hh"
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>
/**
 * unit tests of some helpers defined in the interface header files
 */
//using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<"  === data version: "<<interface::bril::shared::DATA_VERSION<<std::endl;
  std::cout<<"  === head dictionary: "<<interface::bril::shared::bestlumiT::headdict()<<std::endl;
  std::cout<<"  === payload dictionary: "<<interface::bril::shared::bestlumiT::payloaddict()<<std::endl;
  std::cout<<"  === topic description: "<<interface::bril::shared::bestlumiT::description()<<std::endl;
  std::cout<<"  === topic unit: "<<interface::bril::shared::bestlumiT::unit()<<std::endl;
  std::cout<<"  ===testing data type in/out dataholder === "<<std::endl;
  size_t totalsize = interface::bril::shared::bestlumiT::maxsize();
  unsigned char *buffer=(unsigned char*)malloc(totalsize);
  if (buffer==NULL) exit (1);
  ((interface::bril::shared::DatumHead*)buffer)->setTime(2364,123450,12,1,34556,0);
  ((interface::bril::shared::DatumHead*)buffer)->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
  ((interface::bril::shared::DatumHead*)buffer)->setTotalsize(totalsize);
  ((interface::bril::shared::DatumHead*)buffer)->setFrequency(4);
  std::cout<<((interface::bril::shared::DatumHead*)buffer)->totalsize()<<std::endl;

  std::cout<<"  === payload stream in === "<<std::endl;
  interface::bril::shared::CompoundDataStreamer cdata(interface::bril::shared::bestlumiT::payloaddict());
  const char* lumiprovider = "bcm1f";
  const char* calibtag = "bcm1flumicalib_v1";
  float delivered=0.5;
  float recorded=0.45;
  float avgpu=0.1;
  float bxdelivered[3564];
  std::fill(bxdelivered,bxdelivered+3564,0.);
  for(int i=0;i<3564;++i){bxdelivered[i]=0.5*i;}
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "provider", (void*)lumiprovider );
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "calibtag", (void*)calibtag );
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "recorded", (void*)(&recorded) );
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "delivered", (void*)(&delivered) );
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "avgpu", (void*)(&avgpu) );
  cdata.insert_field( ((interface::bril::shared::DatumHead*)buffer)->payloadanchor, "bxdelivered", (void*)bxdelivered );

  std::cout<<"  === payload stream out === "<<std::endl;
  char out_lumiprovider[] = "";
  char out_calibtag[] = "";
  float out_delivered = 0;
  float out_recorded = 0;
  float out_avgpu = 0;
  float out_bxdelivered[3564];
  std::fill(out_bxdelivered,out_bxdelivered+3564,0.);
  cdata.extract_field(out_lumiprovider, "provider", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(out_calibtag, "calibtag", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_delivered, "delivered", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_recorded, "recorded", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_avgpu, "avgpu", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_bxdelivered, "bxdelivered", ((interface::bril::shared::DatumHead*)buffer)->payloadanchor);
  std::cout<<"fillnum "<<((interface::bril::shared::DatumHead*)buffer)->fillnum<<std::endl;
  std::cout<<"runnum "<<((interface::bril::shared::DatumHead*)buffer)->runnum<<std::endl;
  std::cout<<"lumiprovider "<<out_lumiprovider<<std::endl;
  std::cout<<"calibtag "<<out_calibtag<<std::endl;
  std::cout<<"delivered "<<out_delivered<<std::endl;
  std::cout<<"recorded "<<out_recorded<<std::endl;
  std::cout<<"avgpu "<<out_avgpu<<std::endl;
  for(int i=0;i<3564;++i){ std::cout<<i<<" "<<out_bxdelivered[i]<<" ";}
  std::cout<<std::endl;
}
