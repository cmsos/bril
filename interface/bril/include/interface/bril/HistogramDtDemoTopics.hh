#ifndef _interface_bril_HistogramDtDemoTopics_hh_
#define _interface_bril_HistogramDtDemoTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/HistogramGenericTopics.hh"

namespace interface{
    namespace bril{

        // dt demo parameters
        constexpr histogramgeneric::HistogramGenericParametersT HISTOGRAMDTDEMO_PARAMETERS = {.fHeaderSize = 9,
                                                                                            .fNbins = 3564,
                                                                                            .fCounterWidth = 16,
                                                                                            .fIncrementWidth = 1,
                                                                                            .fNunits = 1,
                                                                                            .fUnitIncrementWidth = 3,
                                                                                            .fErrorCounterWidth = 3};

        //
        // DtDemoSource topics
        //
        DEFINE_COMPOUND_TOPIC (dtdemo_rawhist, HISTOGRAMDTDEMO_PARAMETERS.fPayloadDict, "histogram dt demo", "counts");
    }
}

#endif
