#ifndef _interface_bril_REMUSTopics_hh_
#define _interface_bril_REMUSTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
 
  DEFINE_COMPOUND_TOPIC(remuslumi,"calibtag:str32:1 avgraw:float:1 avg:float:1","remus lumi","");

  }}//ns interface/bril

#endif
