#ifndef _interface_bril_RADMONTopics_hh_
#define _interface_bril_RADMONTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"

namespace interface{ namespace bril
{  
  // total number of RADMON detectors (channels)
  // 
  const unsigned int NRADMONCHANNELS  =  16; 

  #define RADMONAlgos_ENUM(XX) XX(SIMPLE_CALIBRATION,=101) XX(COMPLEX_CALIBRATION,)

  DEFINE_LOOKUPTABLE(RADMONAlgos,RADMONAlgos_ENUM)
  //
  // RADMONProcessor output topics
  //
  
  DEFINE_COMPOUND_TOPIC(radmonraw, "rate:float:16 voltage:float:16 current:float:16 status:uint16:16 readouttime:uint16:1","raw data", "vary");
  
  DEFINE_SIMPLE_TOPIC(radmonflux,float,NRADMONCHANNELS,NULL,"flux","n/cm^2*s");
  
  DEFINE_COMPOUND_TOPIC(radmonlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1","instantaneous luminosity", "Hz/ub");
  
}}//ns interface/bril 

#endif
