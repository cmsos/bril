#ifndef _interface_bril_CalibData_h_
#define _interface_bril_CalibData_h_
#include <cmath>
#include <cstring>
namespace interface{ namespace bril {
      const size_t MAX_CALIBCOEFS = 5;
      const size_t MAX_AFTERGLOWTH = 50;

      struct Validity{
      Validity():since(0),till(0){}
	int since;
	int till;
      };

      struct AfterglowThreshold{
      AfterglowThreshold():first(0),second(0){}
	unsigned int first;
	float second;
      };
    
      class CalibData{     
      public:      
        CalibData():m_ncoefs(0),m_nafterglows(0){
	  memset( m_coefs,0,sizeof(float)*MAX_CALIBCOEFS );
	}   
	void setValidity(int validSince, int validTill){
	  m_v.since = validSince;
	  m_v.till = validTill;
	}
	void setValidity(const Validity& v){
	  setValidity(v.since,v.till);
	}
        void setCoefs(float* c, size_t n){
	  m_ncoefs = n;
	  memcpy( m_coefs, c, n*sizeof(float) );
        }
	void setAfterglowThresholds(AfterglowThreshold* a, size_t n){
	  m_nafterglows = n;
	  memcpy( m_afterglowthresholds, a, n*sizeof(AfterglowThreshold) );
	}
	Validity getValidity() const{
	  return m_v;
	}
	const float* getCoefs() const { return m_coefs; }
	const AfterglowThreshold* getAfterglowThreshold() const { return m_afterglowthresholds; }
	size_t nCoefs() const{ return m_ncoefs; }
	size_t nAfterglowThresholds() const{ return m_nafterglows; }		
    public:  
      Validity m_v;
      float m_coefs[MAX_CALIBCOEFS]; 
      AfterglowThreshold m_afterglowthresholds[MAX_AFTERGLOWTH];  
      size_t m_ncoefs;
      size_t m_nafterglows;
    };//CalibData    
    }}//ns

#endif
