#ifndef _interface_bril_BCM1FUTCATopics_hh_
#define _interface_bril_BCM1FUTCATopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface {
    namespace bril {
        const unsigned int BCM1FUTCA_BINPERBX_RAW =  30; // bins per BX in raw  orbit
        const unsigned int BCM1FUTCA_BINPERBX_OCC =   6; // bins per BX in occ  histogram
        const unsigned int BCM1FUTCA_BINAMP   = 512; // bins per amplitude histogram
        const unsigned int BCM1FUTCA_NCHANNELS    =  48; // total number of BCM1FUTCA detectors (channels)
        const unsigned int BPTX_CHANNELID_BC1 = 101;
        const unsigned int BPTX_CHANNELID_BC2 = 102;

        // Occupancy histrograms -------------------------------------------------------------------------------------------------------------------
#define BCM1FUTCAHistAlgos_ENUM(XX) XX(BASIC_PEAK_FINDER,=100) XX(DERIVATIVE_PEAK_FINDER,) XX(UNDEFINED_PEAK_FINDER,)
        DEFINE_LOOKUPTABLE (BCM1FUTCAHistAlgos, BCM1FUTCAHistAlgos_ENUM)
        DEFINE_SIMPLE_TOPIC (bcm1futcaocchist, uint16_t, BCM1FUTCA_BINPERBX_OCC*interface::bril::shared::MAX_NBX, BCM1FUTCAHistAlgos::lookuptable, "bcm1futca occ histograms", "");
        // -----------------------------------------------------------------------------------------------------------------------------------------

        // Amplitude histrograms --------------------------------------------------------------------------------------------------------------------
        DEFINE_SIMPLE_TOPIC (bcm1futcaamphist, uint32_t, BCM1FUTCA_BINAMP, BCM1FUTCAHistAlgos::lookuptable, "bcm1futca amp histograms", "hits");
        // -----------------------------------------------------------------------------------------------------------------------------------------

        // Raw orbit data ------------------------------------------------------------------------------------------------------------------------
        DEFINE_SIMPLE_TOPIC (bcm1futcarawdata, uint8_t, BCM1FUTCA_BINPERBX_RAW*interface::bril::shared::MAX_NBX, BCM1FUTCAHistAlgos::lookuptable, "bcm1futca adc raw data", "");
        // ---------------------------------------------------------------------------------------------------------------------------------------
        //
        //
        //
        // BCM1FUTCAProcessor output topics
        //
        // Timing processr outputs
        DEFINE_COMPOUND_TOPIC (bcm1futca_agg_hist, "totalhits:uint32:1 agghist:uint16:3564", "bcm1futca aggregate histogram with bx per bin and total number of hits", "hits");

        DEFINE_COMPOUND_TOPIC (bcm1futca_col_hist, "totalhits:uint32:1 colhist:uint16:3564", "bcm1futca aggregate collision histogram with bx per bin and total number of hits", "hits");

        DEFINE_COMPOUND_TOPIC (bcm1futca_bkg_hist, "totalhits:uint32:1 bkghist:uint16:3564", "bcm1futca aggregate MIB histogram with bx per bin and total number of hits", "hits");

        DEFINE_COMPOUND_TOPIC (bcm1futca_bkg12, "bkgd1:float:1 bkgd2:float:1", "bcm1futca rate/current", "");

        DEFINE_COMPOUND_TOPIC (bcm1futca_background, "bkgd1:float:1 bkgd2:float:1 bkgd1_nc:float:1 bkgd2_nc:float:1 bkgd1histsum:float:1 bkgd2histsum:float:1 bkgd1hist:float:3564 bkgd2hist:float:3564", "bcm1f rate/current per bx", "Hz/current");

        DEFINE_COMPOUND_TOPIC (bcm1futcalumi, "calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1", "bcm1futca instantaneous lumi raw and calibrated", "Hz/ub");

        DEFINE_COMPOUND_TOPIC (bcm1futca_pcvdlumi, "calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1", "bcm1futca instantaneous lumi raw and calibrated", "Hz/ub");

        DEFINE_COMPOUND_TOPIC (bcm1futca_silumi, "calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1", "bcm1futca instantaneous lumi raw and calibrated", "Hz/ub");

        DEFINE_COMPOUND_TOPIC (bcm1futca_occ_section_hist, "summedNibbles:uint32:1 occ_section_hist:uint32:21384", "bcm1futca occupancy histogram aggregated over a full lumisection", "hits");

        //Ampitude processor output
        DEFINE_COMPOUND_TOPIC (bcm1futca_amp_analysis, "totalhits:uint32:1 mpvbin:uint32:1 minbin:uint32:1 testpulse:uint32:1", "bcm1futca amplitude histogram analysis results", "hits, ADC");

        DEFINE_COMPOUND_TOPIC (bcm1futca_amp_section_hist, "summedNibbles:uint32:1 amp_section_hist:uint32:256", "bcm1futca amplitude histogram aggregated over a full lumisection", "hits");

        // Raw data processor output
        DEFINE_COMPOUND_TOPIC (bcm1futca_baseline_tp_info, "baseline:float:1 testpulse_height:float:1 testpulse_position:uint32:1", "Baseline, Testpulse height and position", "ADC, ADC, time-bin")

        DEFINE_COMPOUND_TOPIC (bcm1futca_ADC_peaks, "height:float:1 timing:float:1 timing_wrt_tp:float:1 FWHM:float:1", "Peaks found in RAW ADC data", "ADC, time-bin")
    }
}//ns interface/bril
#endif
