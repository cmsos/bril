#ifndef _interface_bril_TCDSTopics_hh_
#define _interface_bril_TCDSTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{

    DEFINE_SIGNAL_TOPIC(NB1,"software nibble clock");  
    DEFINE_SIGNAL_TOPIC(NB4,"software 4 nibble clock");  
    DEFINE_SIGNAL_TOPIC(NB64,"software 64 nibble clock");
 
    DEFINE_COMPOUND_TOPIC(tcds,"norb:uint32:1 nbperls:uint32:1 cmson:bool:1 deadfrac:float:1 ncollidingbx:uint32:1","deadtime beam active, cmsalive, ncolliding bx","deadfrac(%)");
    
  }}//ns interface/bril

#endif
