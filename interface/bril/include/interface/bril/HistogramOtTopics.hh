#ifndef _interface_bril_HistogramOtTopics_hh_
#define _interface_bril_HistogramOtTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/HistogramGenericTopics.hh"

namespace interface{
    namespace bril{

        // ot parameters
        constexpr histogramgeneric::HistogramGenericParametersT HISTOGRAMOT_PARAMETERS = {.fHeaderSize = 9,
                                                                                            .fNbins = 3564,
                                                                                            .fCounterWidth = 30,
                                                                                            .fIncrementWidth = 6,
                                                                                            .fNunits = 4,
                                                                                            .fUnitIncrementWidth = 6,
                                                                                            .fErrorCounterWidth = 3};

        //
        // OtSource topics
        //
	DEFINE_COMPOUND_TOPIC (ot_rawhist, HISTOGRAMOT_PARAMETERS.fPayloadDict, "histogram ot", "counts");

    }
}

#endif
