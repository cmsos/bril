#ifndef _interface_bril_BEAMTopics_hh_
#define _interface_bril_BEAMTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
    const unsigned short bxconfig_bptx_rhu=0;
    const unsigned short bxconfig_bptx_scope=1;
    const unsigned short bxconfig_fbct=2;
    const unsigned short bxconfig_bptx_utca=3;
    const unsigned short bxconfig_lhcconifg=4;

  ///dip/acc/LHC/Beam/IntensityPerBunch/Beam%d
  ///dip/acc/LHC/RunControl/BeamMode
  ///dip/acc/LHC/Beam/Energy
  ///dip/acc/LHC/RunControl/CirculatingBunchConfig/Beam%d
  ///dip/acc/LHC/RunControl/RunConfiguration/
  DEFINE_COMPOUND_TOPIC(beam,"status:str28:1 egev:float:1 targetegev:uint16:1 bxconfig1:bool:3564 bxconfig2:bool:3564 intensity1:float:1 intensity2:float:1 bxintensity1:float:3564 bxintensity2:float:3564 machinemode:str20:1 fillscheme:str64:1 ncollidable:int32:1 collidable:bool:3564","beam parameters","intensity(charge)");

  DEFINE_COMPOUND_TOPIC(bxconfig,"beam1:bool:3564 beam2:bool:3564 datasource:uint16:1","bxconfig","");

  DEFINE_COMPOUND_TOPIC(bestbxconfig,"ncollidingbx:uint16:1 beam1:bool:3564 beam2:bool:3564","best bxconfig","");

  DEFINE_COMPOUND_TOPIC(bunchmask,"ncollidingbx:uint16:1 firstlumibx:int16:1 maxlumibx:int16:1 bxmask:bool:3564 datasource:uint16:1","bunch mask with nbx","");

  ///dip/acc/LHC/Beam/AutomaticScan
  DEFINE_COMPOUND_TOPIC(vdmscan,"step:int32:1 progress:int32:1 beam:uint8:1 ip:uint8:1 plane:str16:1 nominal_sep_plane:str16:1 stat:str16:1 sep:float:1 r_sepP1:float:1 r_xingP1:float:1 r_sepP2:float:1 r_xingP2:float:1 s_sepP1:float:1 s_xingP1:float:1 s_sepP2:float:1 s_xingP2:float:1 5ldoros_b1names:str16:1 5ldoros_b1hpos:float:1 5ldoros_b1vpos:float:1 5ldoros_b1herr:float:1 5ldoros_b1verr:float:1 5rdoros_b1names:str16:1 5rdoros_b1hpos:float:1 5rdoros_b1vpos:float:1 5rdoros_b1herr:float:1 5rdoros_b1verr:float:1 5ldoros_b2names:str16:1 5ldoros_b2hpos:float:1 5ldoros_b2vpos:float:1 5ldoros_b2herr:float:1 5ldoros_b2verr:float:1 5rdoros_b2names:str16:1 5rdoros_b2hpos:float:1 5rdoros_b2vpos:float:1 5rdoros_b2herr:float:1 5rdoros_b2verr:float:1 bstar5:int32:1 xingHmurad:int32:1 vdmname:str64:1","vdmscan parameters","");

  ///dip/acc/LHC/Beam/BunchLengths
  DEFINE_COMPOUND_TOPIC(bunchlength,"b1mean:float:1 b2mean:float:1 b1length:float:3564 b2length:float:3564","bunch lengths","");

  ///dip/TNproxy/ATLAS/LHC/BPTX1/IntensitiesPerBunch
  ///dip/TNproxy/ATLAS/LHC/BPTX2/IntensitiesPerBunch  
  DEFINE_COMPOUND_TOPIC(atlasbeam,"intensity1:float:1 intensity2:float:1 bxintensity1:float:3564 bxintensity2:float:3564","atlas beam during vdm scan","");

  DEFINE_COMPOUND_TOPIC(vdmstep,"provider:str32:1 stepid:int32:1 plane:str16:1 separation:float:1 rate:float:3564 rateerror:float:3564 beam1intensity:float:3564 beam2intensity:float:3564","vdm accumulated data per step per provider","");
    
  DEFINE_COMPOUND_TOPIC(bsrlfracBunched,"postfracghostBunched:float:2 postfracsatBunched:float:2","BSRL post frac ghost sat bunched","");

  DEFINE_SIMPLE_TOPIC(bsrlqBunched,float,11880,0,"BSRL qBunched with channelid=11,12,13 for beam1 or 21,22,23 for beam2","");
  
  DEFINE_COMPOUND_TOPIC(luminousregion,"tilt:float:2 centroid:float:3 size:float:3","luminous region","");

  DEFINE_COMPOUND_TOPIC(vdmoutput,"provider:str32:1 mean_sep:float:3564 meanerr_sep:float:3564 sigma_sep:float:3564 sigmaerr_sep:float:3564 avgsigma_sep:float:1 avgsigmaerr_sep:float:1 mean_cross:float:3564 meanerr_cross:float:3564 sigma_cross:float:3564 sigmaerr_cross:float:3564 avgsigma_cross:float:1 avgsigmaerr_cross:float:1 pileup:float:3564 avgpileup:float:1 bxemittanceX:float:3564 avgemittanceX:float:1 bxemittanceY:float:3564 avgemittanceY:float:1 bxemittanceX_err:float:3564 avgemittanceX_err:float:1 bxemittanceY_err:float:3564 avgemittanceY_err:float:1","vdmmonitor results",""); 

     
  DEFINE_SIMPLE_TOPIC(vdmflag,bool,1,NULL,"vdm acquisition flag","");
    
  DEFINE_SIMPLE_TOPIC(vdmscanflag,bool,1,NULL,"vdm scan active flag","");

  }}//ns interface/bril

#endif
