#ifndef _interface_bril_BHMTopics_hh_
#define _interface_bril_BHMTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
  const unsigned int BHM_NRU = 20;
  const unsigned int BHM_BINPERBX = 4;
  const unsigned int BHM_BINAMP = 256;
  
#define BHMBkgAlgos_ENUM(XX) XX(BEAM1,=1) XX(BEAM2,)
DEFINE_LOOKUPTABLE(BHMBkgAlgos,BHMBkgAlgos_ENUM)
  
#define BHMQualityAlgos_ENUM(XX) XX(NotAvailable,=1) XX(Normal,) XX(Noisy,)
DEFINE_LOOKUPTABLE(BHMQualityAlgos,BHMQualityAlgos_ENUM)

#define BHMAmpAlgos_ENUM(XX) XX(TDCBIN1,=1) XX(TDCBIN2,) XX(TDCBIN3,) XX(TDCBIN4,)
DEFINE_LOOKUPTABLE(BHMAmpAlgos,BHMAmpAlgos_ENUM)


  //
  // BHMSource output topics
  //

  DEFINE_SIMPLE_TOPIC(bhmocchist,unsigned short,BHM_BINPERBX*interface::bril::shared::MAX_NBX,BHMQualityAlgos::lookuptable,"bhm occupancy histogram","");

  DEFINE_SIMPLE_TOPIC(bhmamphist,unsigned int,BHM_BINAMP,BHMAmpAlgos::lookuptable,"bhm amplitude histogram","");

  //
  // BHMProcessor output topics
  //
  DEFINE_SIMPLE_TOPIC(bhmagghist,unsigned short,interface::bril::shared::MAX_NBX,BHMBkgAlgos::lookuptable,"bhm aggregated occupancy histogram accumulation in time with larger bins","");

  DEFINE_SIMPLE_TOPIC(bhmampagghist,unsigned int,BHM_BINAMP,BHMAmpAlgos::lookuptable,"bhm aggregated amplitude histogram accumulation in time with larger bins","");

  DEFINE_SIMPLE_TOPIC(bhmagghistmib,unsigned short,interface::bril::shared::MAX_NBX,BHMBkgAlgos::lookuptable,"bhm aggregated mib occupancy histogram accumulation in time with larger bins","");
 
  DEFINE_SIMPLE_TOPIC(bhmagghistpp,unsigned short,interface::bril::shared::MAX_NBX,BHMBkgAlgos::lookuptable,"bhm aggregated pp occupancy histogram accumulation in time with larger bins","");

  DEFINE_SIMPLE_TOPIC(bhmsumhistmib,unsigned short,interface::bril::shared::MAX_NBX,BHMBkgAlgos::lookuptable,"bhm aggregated mib occupancy histogram accumulation in time with larger bins","");

  DEFINE_SIMPLE_TOPIC(bhmsumhistpp,unsigned short,interface::bril::shared::MAX_NBX,BHMBkgAlgos::lookuptable,"bhm aggregated pp occupancy histogram accumulation in time with larger bins","");

  DEFINE_COMPOUND_TOPIC(bhmbkg,"beam1:float:1 beam2:float:1 bxbeam1:float:3564 bxbeam2:float:3564 bxerror1:float:3564 bxerror2:float:3564","beam background","Hz/cm2/10^11");
  
  }}//ns interface/bril

#endif
